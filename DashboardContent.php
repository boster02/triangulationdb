<?php session_start();
      include("analytics/fncAnalytics.inc.php");
      $summary=fncGetSummaryData(fncGetLastMonthCycle());
      $startmonth=date("Y")."01"; //start of the year
      $endmonth=date("Ym"); //this month
      $sales=fncGetTotalSalesByMonth($startmonth,$endmonth);
      $households_percent=floatval($summary["previousmonth_households"])>0?floatval(($summary["thismonth_households"]/$summary["previousmonth_households"])*100):0;
      $households_arrow=$households_percent>=100?"fa-long-arrow-up":"fa-long-arrow-down";
      $beneficiaries_percent=floatval($summary["previousmonth_beneficiaries"])>0?intval($summary["thismonth_beneficiaries"])/intval($summary["previousmonth_beneficiaries"])*100:0;
      $beneficiaries_arrow=$beneficiaries_percent>=100?"fa-long-arrow-up":"fa-long-arrow-down";
      $reload_percent=floatval($summary["previousmonth_reload_value"])>0?floatval(($summary["thismonth_reload_value"]/$summary["previousmonth_reload_value"])*100):0;
      $reload_arrow=$reload_percent>=100?"fa-long-arrow-up":"fa-long-arrow-down";
      $sales_percent=floatval($summary["previousmonth_sales_value"])>0?floatval(($summary["thismonth_sales_value"]/$summary["previousmonth_sales_value"])*100):0;
      $sales_arrow=$sales_percent>=100?"fa-long-arrow-up":"fa-long-arrow-down";
      fncLogAccess2("Viewed the dashboard");
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Dashboard</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <!-- Boostrap Theme -->
    <link href="css/boostrap-overrides.css" rel="stylesheet" />
    <link id="themeCss" href="css/theme.css" rel="stylesheet" />

    <!-- Plugins -->
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <style>
        .map-frame {
              overflow-y: hidden;
              overflow-x: hidden;
            }
        .row {
            margin-top: 40px;
            padding: 0 10px;
        }

        .clickable {
            cursor: pointer;
        }

        .panel-heading span {
            margin-top: -20px;
            font-size: 15px;
        }
    </style>

    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/canvasjs.min.js"></script>

    <script type="text/javascript">
        $(document).on('click', '.panel-heading span.clickable', function (e) {
            var $this = $(this);
            if (!$this.hasClass('panel-collapsed')) {
                $this.parents('.panel').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            } else {
                $this.parents('.panel').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
        })
        window.onload = function () {
            var chart = new CanvasJS.Chart("SalesTrendChartContainer", {
                theme: "theme1",//theme1
                axisY:{
                    title:"Sales Amount (JOD)",
                },
                animationEnabled: true,   // change to true
                data: [
                {
                    // Change type to "bar", "splineArea", "area", "spline", "pie",etc.
                    type: "line",
                    dataPoints: [
                       <?php     for($i=1;$i<=$sales[0][0];$i++){ ?>
                        { label: "<?php echo substr(fncGetMonthName($sales[$i]["Month"]),0,1) ?>", y: <?php echo floatval($sales[$i]["Amount"]) ?> },
                        <?php } ?>
                    ]
               
                }
                ]
            });
            chart.render();
            var chart = new CanvasJS.Chart("UploadAuthenticationChartContainer", {
                theme: "theme1",//theme1
                animationEnabled: true,   // change to true
                data: [
                {
                    // Change type to "bar", "splineArea", "area", "spline", "pie",etc.
                    type: "column",
                    dataPoints: [
                                    { label: "Progr", y: +globaldata.wfp_reload.value },
                                    { label: "MEPS", y: +globaldata.meps_reload.value },
                                    { label: "JAB", y: +globaldata.jab_reload.value },
                                    { label: "LOA", y: +globaldata.loa_reload.value },
                    ]
                }
                ]
            });
            chart.render();

            //canvas initialization
            var canvas = document.getElementById("CardUsageChartContainer");
            var ctx = canvas.getContext("2d");
            //dimensions
            var W = canvas.width;
            var H = canvas.height;
            //Variables
            var degrees = 0;
            var new_degrees = 0;
            var difference = 0;
            var color = "red"; //green looks better to me
            var bgcolor = "#222";
            var text;
            var animation_loop, redraw_loop;

            function init() {
                //Clear the canvas everytime a chart is drawn
                ctx.clearRect(0, 0, W, H);

                //Background 360 degree arc
                ctx.beginPath();
                ctx.strokeStyle = bgcolor;
                ctx.lineWidth = 30;
                ctx.arc(W / 2, H / 2, 100, 0, Math.PI * 2, false); //you can see the arc now
                ctx.stroke();

                //gauge will be a simple arc
                //Angle in radians = angle in degrees * PI / 180
                var radians = degrees * Math.PI / 180;
                ctx.beginPath();
                ctx.strokeStyle = color;
                ctx.lineWidth = 30;
                //The arc starts from the rightmost end. If we deduct 90 degrees from the angles
                //the arc will start from the topmost end
                ctx.arc(W / 2, H / 2, 100, 0 - 90 * Math.PI / 180, radians - 90 * Math.PI / 180, false);
                //you can see the arc now
                ctx.stroke();

                //Lets add the text
                ctx.fillStyle = color;
                ctx.font = "50px bebas";
                text = Math.floor(degrees / 360 * 100) + "%";
                //Lets center the text
                //deducting half of text width from position x
                text_width = ctx.measureText(text).width;
                //adding manual value to position y since the height of the text cannot
                //be measured easily. There are hacks but we will keep it manual for now.
                ctx.fillText(text, W / 2 - text_width / 2, H / 2 + 15);
            }

            function draw(percent) {
                //Cancel any movement animation if a new chart is requested
                if (typeof animation_loop != undefined) clearInterval(animation_loop);
                var arcdegrees = (percent + 1) / 100 * 360;

                //random degree from 0 to 360
                new_degrees = Math.round(arcdegrees);
                difference = new_degrees - degrees;
                //This will animate the gauge to new positions
                //The animation will take 1 second
                //time for each frame is 1sec / difference in degrees
                animation_loop = setInterval(animate_to, 1000 / difference);
            }

            //function to make the chart move to new degrees
            function animate_to() {
                //clear animation loop if degrees reaches to new_degrees
                if (degrees == new_degrees)
                    clearInterval(animation_loop);

                if (degrees < new_degrees)
                    degrees++;
                else
                    degrees--;

                init();
            }

            //Lets add some animation for fun
            draw(globaldata.usage_percent.value);
            //redraw_loop = setInterval(draw, 2000); //Draw a new chart every 2 seconds

        };
    </script>
</head>
<body style="overflow-x: hidden">
    <form name="globaldata" id="globaldata">
        <input type="hidden" name="usage_percent" value="<?php echo $summary["cumulative_reload_value"]>0?(($summary["cumulative_sales_value"]/$summary["cumulative_reload_value"])*100):0 ?>"/>
        <input type="hidden" name="cumulative_sales_value" value="<?php echo $summary["cumulative_sales_value"] ?>" />
        <input type="hidden" name="cumulative_reload_value" value="<?php echo $summary["cumulative_reload_value"] ?>" />
        <input type="hidden" name="total_households" value="<?php echo $summary["cumulative_households"] ?>" />
        <input type="hidden" name="no_of_reloads" value="<?php echo $summary["cumulative_no_of_reloads"] ?>" />
        <input type="hidden" name="sales_transactions" value="<?php echo intval($summary["cumulative_sales_transactions"]) ?>" />
        <input type="hidden" name="reload_cycles" value="<?php echo intval($summary["cumulative_reload_cycles"]) ?>" />
        <input type="hidden" name="wfp_reload" value="<?php echo intval($summary["wfp_reload"]) ?>" />
        <input type="hidden" name="meps_reload" value="<?php echo intval($summary["meps_reload"]) ?>" />
        <input type="hidden" name="jab_reload" value="<?php echo intval($summary["jab_reload"]) ?>" />
        <input type="hidden" name="loa_reload" value="<?php echo intval($summary["loa_reload"]) ?>" />
    </form>
    <table style="width: 100%">
        <tr style="background-color: #ebebeb">
            <td>
                <div class="panel">
                    <div class="panel-body brand-secondary-bg text-inverse">
                        <div class="pull-left">
                            <div class="badge-circle daily-stat-left">
                                <div class="big-text"><i class="fa fa-users"></i></div>
                            </div>
                        </div>
                        <span class="count big-text"><?php echo intval($summary["cumulative_households"]) ?></span>
                        <div class="stat text-transparent">
                            <h5>No. of Households/Cases </h5>
                        </div>
                    </div>
                    <div class="panel-footer white-bg">
                        <div class="pull-left">
                            <h6 class="text-transparent">Last Month</h6>
                            <div class="daily-stats-compare number">
                                <span class="text-secondary"><strong><?php echo number_format($summary["thismonth_households"]) ?></strong></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <span class="fa <?php echo $households_arrow ?> text-transparent daily-stats-compare"></span>
                            <span class="daily-stats-compare"><?php echo number_format($households_percent,2) ?>%</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </td>
            <td>
                <div class="panel">
                    <div class="panel-body brand-warning-bg text-inverse">
                        <div class="pull-left">
                            <div class="badge-circle daily-stat-left">
                                <div class="big-text"><i class="fa fa-upload"></i></div>
                            </div>
                        </div>
                        <span class="count big-text"><?php echo intval($summary["cumulative_beneficiaries"]) ?></span>
                        <div class="stat text-transparent">
                            <h5>Beneficiaries</h5>
                        </div>
                    </div>
                    <div class="panel-footer white-bg">
                        <div class="pull-left">
                            <h6 class="text-transparent">Last Month</h6>
                            <div class="daily-stats-compare number">
                                <span class="text-warning"><strong><?php echo number_format($summary["thismonth_beneficiaries"]) ?></strong></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <span class="fa <?php echo $beneficiaries_arrow ?> text-transparent daily-stats-compare"></span>
                            <span class="daily-stats-compare"><?php echo number_format($beneficiaries_percent,2) ?>%</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </td>
            <td>
                <div class="panel">
                    <div class="panel-body brand-primary-bg text-inverse">
                        <div class="pull-left">
                            <div class="badge-circle daily-stat-left">
                                <div class="big-text"><i class="fa fa-shopping-cart"></i></div>
                            </div>
                        </div>
                        <span class="count big-text"><?php echo intval($summary["cumulative_reload_value"]) ?></span>
                        <div class="stat text-transparent">
                            <h5>Reload Amount (JOD)</h5>
                        </div>
                    </div>
                    <div class="panel-footer white-bg">
                        <div class="pull-left">
                            <h6 class="text-transparent">Last Month</h6>
                            <div class="daily-stats-compare number">
                                <span class="text-primary"><strong><?php echo number_format($summary["thismonth_reload_value"]) ?></strong></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <span class="fa <?php echo $reload_arrow ?> text-transparent daily-stats-compare"></span>
                            <span class="daily-stats-compare"><?php echo number_format($reload_percent,2) ?>%</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </td>
            <td>
                <div class="panel">
                    <div class="panel-body brand-info-bg text-inverse">
                        <div class="pull-left">
                            <div class="badge-circle daily-stat-left">
                                <div class="big-text"><i class="fa fa-refresh"></i></div>
                            </div>
                        </div>
                        <span class="count big-text"><?php echo intval(floatval($summary["cumulative_sales_value"])) ?></span>
                        <div class="stat text-transparent">
                            <h5>Sales Amount (JOD)</h5>
                        </div>
                    </div>
                    <div class="panel-footer white-bg">
                        <div class="pull-left">
                            <h6 class="text-transparent">Last Month</h6>
                            <div class="daily-stats-compare number">
                                <span class="text-info"><strong><?php echo number_format(floatval($summary["thismonth_sales_value"])) ?></strong></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <span class="fa <?php echo $sales_arrow ?> text-transparent daily-stats-compare"></span>
                            <span class="daily-stats-compare">
                                <?php echo number_format($sales_percent,2) ?>%
                            </span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!--/ row-->
    <div style="margin: 10px">
        <div class="row" style="margin-top: 20px">
            <div class="col-sm-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-line-chart"></i>Sales Trends for 2016</h3>
                        <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                    </div>
                    <div class="panel-body" style="height: 280px">
                        <div id="SalesTrendChartContainer" style="height: 250px"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>Total Sales as Percent of Total Reloads (Overall)</h3>
                        <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                    </div>
                    <div class="panel-body">
                        <canvas id="CardUsageChartContainer" width="400" height="250" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-sm-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>Upload Authentication for Last Month</h3>
                        <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                    </div>
                    <div class="panel-body" style="height: 280px">
                        <div id="UploadAuthenticationChartContainer" style="height: 250px"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>Map of Merchants</h3>
                        <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                    </div>
                    <div class="panel-body">
                        <div id="PurchaseByCategoryChartContainer" style="height: 250px; text-align: center; vertical-align: middle">
                            <iframe name="iframe" src="retailer_map.php" class="map-frame"
                                style="width:500px;height:250px" frameborder="0" scrolling="no" seamless="seamless"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <script src="js/script.js"></script>
</body>
</html>
