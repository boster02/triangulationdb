<!DOCTYPE html>
<html>
<head>
    <title>DataTables Caller</title>
    <script src="js/jquery-1.12.0.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="css/jquery.dataTables.css" />
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "testPagingDataTables.php?year=2015&month=01",
                "sPaginationType": "full_numbers"
            });
        });
    </script>
</head>
<body>
    <div style="width:400px">
        <table id="example" class="display">
            <thead>
                <tr>
                    <th>Customer ID</th>
                    <th>Amount</th>
                </tr>
            </thead>
        </table>
     </div>
</body>
</html>
