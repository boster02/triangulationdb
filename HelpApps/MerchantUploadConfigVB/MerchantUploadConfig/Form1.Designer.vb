﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.groupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtShopId = New System.Windows.Forms.TextBox()
        Me.txtViewName = New System.Windows.Forms.TextBox()
        Me.label23 = New System.Windows.Forms.Label()
        Me.txtDBName = New System.Windows.Forms.TextBox()
        Me.label22 = New System.Windows.Forms.Label()
        Me.txtDBPassword = New System.Windows.Forms.TextBox()
        Me.label21 = New System.Windows.Forms.Label()
        Me.txtDBHost = New System.Windows.Forms.TextBox()
        Me.label20 = New System.Windows.Forms.Label()
        Me.label18 = New System.Windows.Forms.Label()
        Me.txtDBUserName = New System.Windows.Forms.TextBox()
        Me.label19 = New System.Windows.Forms.Label()
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtRemotePassword = New System.Windows.Forms.TextBox()
        Me.label17 = New System.Windows.Forms.Label()
        Me.txtRemoteUserName = New System.Windows.Forms.TextBox()
        Me.label16 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.epUserName = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.epPassword = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.epDbHost = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.epDbUser = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.epDBPass = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.epDBName = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.epViewName = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.epShopId = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblMaxReceiptId = New System.Windows.Forms.Label()
        Me.groupBox2.SuspendLayout()
        Me.groupBox1.SuspendLayout()
        CType(Me.epUserName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epPassword, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epDbHost, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epDbUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epDBPass, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epDBName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epViewName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epShopId, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'groupBox2
        '
        Me.groupBox2.Controls.Add(Me.txtShopId)
        Me.groupBox2.Controls.Add(Me.txtViewName)
        Me.groupBox2.Controls.Add(Me.label23)
        Me.groupBox2.Controls.Add(Me.txtDBName)
        Me.groupBox2.Controls.Add(Me.label22)
        Me.groupBox2.Controls.Add(Me.txtDBPassword)
        Me.groupBox2.Controls.Add(Me.label21)
        Me.groupBox2.Controls.Add(Me.txtDBHost)
        Me.groupBox2.Controls.Add(Me.label20)
        Me.groupBox2.Controls.Add(Me.label18)
        Me.groupBox2.Controls.Add(Me.txtDBUserName)
        Me.groupBox2.Controls.Add(Me.label19)
        Me.groupBox2.Location = New System.Drawing.Point(6, 112)
        Me.groupBox2.Name = "groupBox2"
        Me.groupBox2.Size = New System.Drawing.Size(319, 219)
        Me.groupBox2.TabIndex = 1
        Me.groupBox2.TabStop = False
        Me.groupBox2.Text = "Local Settings"
        '
        'txtShopId
        '
        Me.txtShopId.Location = New System.Drawing.Point(110, 178)
        Me.txtShopId.Name = "txtShopId"
        Me.txtShopId.Size = New System.Drawing.Size(183, 20)
        Me.txtShopId.TabIndex = 11
        '
        'txtViewName
        '
        Me.txtViewName.Location = New System.Drawing.Point(111, 147)
        Me.txtViewName.Name = "txtViewName"
        Me.txtViewName.Size = New System.Drawing.Size(183, 20)
        Me.txtViewName.TabIndex = 10
        '
        'label23
        '
        Me.label23.AutoSize = True
        Me.label23.Location = New System.Drawing.Point(14, 181)
        Me.label23.Name = "label23"
        Me.label23.Size = New System.Drawing.Size(49, 13)
        Me.label23.TabIndex = 1
        Me.label23.Text = "Shop ID:"
        '
        'txtDBName
        '
        Me.txtDBName.Location = New System.Drawing.Point(111, 118)
        Me.txtDBName.Name = "txtDBName"
        Me.txtDBName.Size = New System.Drawing.Size(183, 20)
        Me.txtDBName.TabIndex = 9
        '
        'label22
        '
        Me.label22.AutoSize = True
        Me.label22.Location = New System.Drawing.Point(15, 150)
        Me.label22.Name = "label22"
        Me.label22.Size = New System.Drawing.Size(65, 13)
        Me.label22.TabIndex = 1
        Me.label22.Text = "View/Table:"
        '
        'txtDBPassword
        '
        Me.txtDBPassword.Location = New System.Drawing.Point(111, 87)
        Me.txtDBPassword.Name = "txtDBPassword"
        Me.txtDBPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtDBPassword.Size = New System.Drawing.Size(183, 20)
        Me.txtDBPassword.TabIndex = 8
        Me.txtDBPassword.UseSystemPasswordChar = True
        '
        'label21
        '
        Me.label21.AutoSize = True
        Me.label21.Location = New System.Drawing.Point(15, 121)
        Me.label21.Name = "label21"
        Me.label21.Size = New System.Drawing.Size(87, 13)
        Me.label21.TabIndex = 1
        Me.label21.Text = "Database Name:"
        '
        'txtDBHost
        '
        Me.txtDBHost.Location = New System.Drawing.Point(111, 26)
        Me.txtDBHost.Name = "txtDBHost"
        Me.txtDBHost.Size = New System.Drawing.Size(183, 20)
        Me.txtDBHost.TabIndex = 6
        '
        'label20
        '
        Me.label20.AutoSize = True
        Me.label20.Location = New System.Drawing.Point(15, 90)
        Me.label20.Name = "label20"
        Me.label20.Size = New System.Drawing.Size(56, 13)
        Me.label20.TabIndex = 1
        Me.label20.Text = "Password:"
        '
        'label18
        '
        Me.label18.AutoSize = True
        Me.label18.Location = New System.Drawing.Point(15, 29)
        Me.label18.Name = "label18"
        Me.label18.Size = New System.Drawing.Size(81, 13)
        Me.label18.TabIndex = 1
        Me.label18.Text = "Database Host:"
        '
        'txtDBUserName
        '
        Me.txtDBUserName.Location = New System.Drawing.Point(111, 58)
        Me.txtDBUserName.Name = "txtDBUserName"
        Me.txtDBUserName.Size = New System.Drawing.Size(183, 20)
        Me.txtDBUserName.TabIndex = 7
        '
        'label19
        '
        Me.label19.AutoSize = True
        Me.label19.Location = New System.Drawing.Point(15, 61)
        Me.label19.Name = "label19"
        Me.label19.Size = New System.Drawing.Size(63, 13)
        Me.label19.TabIndex = 1
        Me.label19.Text = "User Name:"
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.txtRemotePassword)
        Me.groupBox1.Controls.Add(Me.label17)
        Me.groupBox1.Controls.Add(Me.txtRemoteUserName)
        Me.groupBox1.Controls.Add(Me.label16)
        Me.groupBox1.Location = New System.Drawing.Point(6, 15)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(319, 91)
        Me.groupBox1.TabIndex = 0
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Remote Settings"
        '
        'txtRemotePassword
        '
        Me.txtRemotePassword.Location = New System.Drawing.Point(76, 52)
        Me.txtRemotePassword.Name = "txtRemotePassword"
        Me.txtRemotePassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtRemotePassword.Size = New System.Drawing.Size(218, 20)
        Me.txtRemotePassword.TabIndex = 1
        Me.txtRemotePassword.UseSystemPasswordChar = True
        '
        'label17
        '
        Me.label17.AutoSize = True
        Me.label17.Location = New System.Drawing.Point(15, 55)
        Me.label17.Name = "label17"
        Me.label17.Size = New System.Drawing.Size(56, 13)
        Me.label17.TabIndex = 1
        Me.label17.Text = "Password:"
        '
        'txtRemoteUserName
        '
        Me.txtRemoteUserName.Location = New System.Drawing.Point(76, 24)
        Me.txtRemoteUserName.Name = "txtRemoteUserName"
        Me.txtRemoteUserName.Size = New System.Drawing.Size(218, 20)
        Me.txtRemoteUserName.TabIndex = 0
        '
        'label16
        '
        Me.label16.AutoSize = True
        Me.label16.Location = New System.Drawing.Point(15, 26)
        Me.label16.Name = "label16"
        Me.label16.Size = New System.Drawing.Size(63, 13)
        Me.label16.TabIndex = 1
        Me.label16.Text = "User Name:"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(181, 349)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(108, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(23, 349)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(110, 23)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'epUserName
        '
        Me.epUserName.ContainerControl = Me
        '
        'epPassword
        '
        Me.epPassword.ContainerControl = Me
        '
        'epDbHost
        '
        Me.epDbHost.ContainerControl = Me
        '
        'epDbUser
        '
        Me.epDbUser.ContainerControl = Me
        '
        'epDBPass
        '
        Me.epDBPass.ContainerControl = Me
        '
        'epDBName
        '
        Me.epDBName.ContainerControl = Me
        '
        'epViewName
        '
        Me.epViewName.ContainerControl = Me
        '
        'epShopId
        '
        Me.epShopId.ContainerControl = Me
        '
        'lblMaxReceiptId
        '
        Me.lblMaxReceiptId.AutoSize = True
        Me.lblMaxReceiptId.Location = New System.Drawing.Point(147, 9)
        Me.lblMaxReceiptId.Name = "lblMaxReceiptId"
        Me.lblMaxReceiptId.Size = New System.Drawing.Size(0, 13)
        Me.lblMaxReceiptId.TabIndex = 2
        Me.lblMaxReceiptId.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(327, 390)
        Me.Controls.Add(Me.lblMaxReceiptId)
        Me.Controls.Add(Me.groupBox2)
        Me.Controls.Add(Me.groupBox1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Merchant Upload Config"
        Me.groupBox2.ResumeLayout(False)
        Me.groupBox2.PerformLayout()
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        CType(Me.epUserName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epPassword, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epDbHost, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epDbUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epDBPass, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epDBName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epViewName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epShopId, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents groupBox2 As GroupBox
    Private WithEvents txtShopId As TextBox
    Private WithEvents txtViewName As TextBox
    Private WithEvents label23 As Label
    Private WithEvents txtDBName As TextBox
    Private WithEvents label22 As Label
    Private WithEvents txtDBPassword As TextBox
    Private WithEvents label21 As Label
    Private WithEvents txtDBHost As TextBox
    Private WithEvents label20 As Label
    Private WithEvents label18 As Label
    Private WithEvents txtDBUserName As TextBox
    Private WithEvents label19 As Label
    Private WithEvents groupBox1 As GroupBox
    Private WithEvents txtRemotePassword As TextBox
    Private WithEvents label17 As Label
    Private WithEvents txtRemoteUserName As TextBox
    Private WithEvents label16 As Label
    Private WithEvents btnCancel As Button
    Private WithEvents btnSave As Button
    Friend WithEvents epUserName As ErrorProvider
    Friend WithEvents epPassword As ErrorProvider
    Friend WithEvents epDbHost As ErrorProvider
    Friend WithEvents epDbUser As ErrorProvider
    Friend WithEvents epDBPass As ErrorProvider
    Friend WithEvents epDBName As ErrorProvider
    Friend WithEvents epViewName As ErrorProvider
    Friend WithEvents epShopId As ErrorProvider
    Private WithEvents lblMaxReceiptId As Label
End Class
