﻿Imports System.IO
Imports System.Xml.Serialization

Public Class Form1
    Dim enc As New EncLib.DataEncryption("")
    Dim fileName As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "config.xml")

    Private Sub button1_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If String.IsNullOrWhiteSpace(txtRemoteUserName.Text) Then
            epUserName.SetError(txtRemoteUserName, "User name is mandatory")
            Return
        Else
            epUserName.Clear()
        End If

        If String.IsNullOrWhiteSpace(txtRemotePassword.Text) Then
            epPassword.SetError(txtRemotePassword, "Password is mandatory")
            Return
        Else
            epPassword.Clear()
        End If


        If String.IsNullOrWhiteSpace(txtDBHost.Text) Then
            epDbHost.SetError(txtDBHost, "Database host name is mandatory")
            Return
        Else
            epDbHost.Clear()
        End If

        If String.IsNullOrWhiteSpace(txtDBName.Text) Then
            epDBName.SetError(txtDBName, "Database name is mandatory")
            Return
        Else
            epDBName.Clear()
        End If

        If String.IsNullOrWhiteSpace(txtDBUserName.Text) Then
            epDbUser.SetError(txtDBUserName, "Database User name is mandatory")
            Return
        Else
            epDbUser.Clear()
        End If


        If String.IsNullOrWhiteSpace(txtDBPassword.Text) Then
            epDBPass.SetError(txtDBPassword, "Password is mandatory")
            Return
        Else
            epDBPass.Clear()
        End If


        If String.IsNullOrWhiteSpace(txtViewName.Text) Then
            epViewName.SetError(txtViewName, "View / Table name is mandatory")
            Return
        Else
            epViewName.Clear()
        End If

        If String.IsNullOrWhiteSpace(txtShopId.Text) Then
            epShopId.SetError(txtShopId, "Shop Id is mandatory")
            Return
        Else
            epShopId.Clear()
        End If

        GenerateXMLFile()

        MessageBox.Show("Configurations saved successfully")
    End Sub

    Private Sub GenerateXMLFile()
        Dim uploadsettings As New UploadConfig

        uploadsettings.DatabaseHost = txtDBHost.Text
        uploadsettings.DatabaseName = txtDBName.Text
        uploadsettings.Password = txtDBPassword.Text
        uploadsettings.ShopId = txtShopId.Text
        uploadsettings.UserName = txtDBUserName.Text
        uploadsettings.ViewName = txtViewName.Text

        uploadsettings.RemotePassword = txtRemotePassword.Text
        uploadsettings.RemoteUserName = txtRemoteUserName.Text
        uploadsettings.MaxRecieptId = lblMaxReceiptId.Text

        Dim xmlHelperClass As New XMLHelper
        Dim xmlString As String

        xmlString = xmlHelperClass.ToXml(uploadsettings, uploadsettings.GetType())

        xmlHelperClass.WriteXMLToFile(enc.Encrypt(xmlString), fileName)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadConfiguration()

    End Sub

    Private Sub LoadConfiguration()

        If Not IO.File.Exists(fileName) Then
            Return
        End If

        Dim encConfig As String = IO.File.ReadAllText(fileName)
        Dim decryptedConfig As String = enc.Decrypt(encConfig)

        Dim serializer As New XmlSerializer(GetType(UploadConfig))
        Dim rdr As New StringReader(decryptedConfig)
        Dim objConfig As UploadConfig = DirectCast(serializer.Deserialize(rdr), UploadConfig)

        If objConfig IsNot Nothing Then
            txtRemoteUserName.Text = objConfig.RemoteUserName
            txtRemotePassword.Text = objConfig.RemotePassword
            txtDBHost.Text = objConfig.DatabaseHost
            txtDBName.Text = objConfig.DatabaseName
            txtViewName.Text = objConfig.ViewName
            txtShopId.Text = objConfig.ShopId
            txtDBUserName.Text = objConfig.UserName
            txtDBPassword.Text = objConfig.Password
            If objConfig.MaxRecieptId > 0 Then
                lblMaxReceiptId.Text = objConfig.MaxRecieptId
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If MessageBox.Show("Changes will be ignored, are you sure?", "Warning", MessageBoxButtons.YesNoCancel) Then
            txtRemoteUserName.Text = Nothing
            txtRemotePassword.Text = Nothing
            txtDBHost.Text = Nothing
            txtDBName.Text = Nothing
            txtViewName.Text = Nothing
            txtShopId.Text = Nothing
            txtDBUserName.Text = Nothing
            txtDBPassword.Text = Nothing

            LoadConfiguration()
        End If
    End Sub
End Class
