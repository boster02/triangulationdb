﻿Imports System.Xml.Serialization

<Serializable>
Public Class UploadConfig

    Public RemoteUserName As String
    Public RemotePassword As String
    Public DatabaseHost As String
    Public UserName As String
    Public Password As String
    Public DatabaseName As String
    Public ViewName As String
    Public ShopId As String
    Public MaxRecieptId As Int32
End Class


