﻿Imports System.IO
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Text
Imports System
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary

Public Class XMLHelper

#Region " Write to file "
    ''' <summary>
    ''' Writes XML to file
    ''' </summary>
    ''' <param name="_object"></param>
    ''' <param name="Location"></param>
    ''' <param name="Overwrite"></param>
    ''' <remarks></remarks>
    Public Sub WriteXMLSeri_ToFile(ByVal _object As Object, ByVal Location As String, Optional ByVal Overwrite As Boolean = True)
        If IO.File.Exists(Location) Then
            If Overwrite = True Then
                IO.File.Delete(Location)
            End If
        End If

        'Serialize object to a text file.
        Dim objStreamWriter As New StreamWriter(Location)
        Dim x As New XmlSerializer(_object.GetType)
        x.Serialize(objStreamWriter, Location)
        objStreamWriter.Close()
    End Sub

    Public Sub WriteXMLToFile(ByVal xmlString As Object, ByVal Location As String, Optional ByVal Overwrite As Boolean = True)
        If IO.File.Exists(Location) Then
            If Overwrite = True Then
                IO.File.Delete(Location)
            End If
        End If

        File.WriteAllText(Location, xmlString)
    End Sub
#End Region

#Region " Read from files "
    ''' <summary>
    ''' Reads XML from a file
    ''' </summary>
    ''' <param name="location"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetXML_File(ByVal location As String) As Object
        'Deserialize text file to a new object.
        Dim objStreamReader As New StreamReader(location)
        Dim p2 As New UploadConfig
        Dim x As Xml.Serialization.XmlSerializer
        p2 = x.Deserialize(objStreamReader)
        objStreamReader.Close()
        Return p2
    End Function
#End Region

#Region " Write + Read to XML string"

    ''' <summary>
    ''' Returns the set of included namespaces for the serializer.
    ''' </summary>
    ''' <returns>
    ''' The set of included namespaces for the serializer.
    ''' </returns>
    Public Shared Function GetNamespaces() As XmlSerializerNamespaces
        Dim ns As XmlSerializerNamespaces
        ns = New XmlSerializerNamespaces()
        ns.Add("xs", "http://www.w3.org/2001/XMLSchema")
        ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance")
        Return ns
    End Function


    Public Shared ReadOnly Property TargetNamespace() As String
        Get
            Return "http://www.w3.org/2001/XMLSchema"
        End Get
    End Property



    Public Function ToXml(ByVal Obj As Object, ByVal ObjType As System.Type) As String
        Dim ser As XmlSerializer
        ser = New XmlSerializer(ObjType, String.Empty)
        Dim memStream As MemoryStream
        memStream = New MemoryStream()
        Dim xmlWriter As XmlTextWriter
        xmlWriter = New XmlTextWriter(memStream, Encoding.UTF8)
        xmlWriter.Namespaces = True

        Dim ns As New XmlSerializerNamespaces()
        ns.Add("", "")

        ser.Serialize(xmlWriter, Obj, ns)
        xmlWriter.Close()
        memStream.Close()
        Dim xml As String
        xml = Encoding.UTF8.GetString(memStream.GetBuffer())
        xml = xml.Substring(xml.IndexOf(Convert.ToChar(60)))
        xml = xml.Substring(0, (xml.LastIndexOf(Convert.ToChar(62)) + 1))
        Return xml
    End Function


    Public Shared Function FromXml(ByVal Xml As String, ByVal ObjType As System.Type) As Object
        Dim ser As XmlSerializer
        ser = New XmlSerializer(ObjType)
        Dim stringReader As StringReader
        stringReader = New StringReader(Xml)
        Dim xmlReader As XmlTextReader
        xmlReader = New XmlTextReader(stringReader)
        Dim obj As Object
        obj = ser.Deserialize(xmlReader)
        xmlReader.Close()
        stringReader.Close()
        Return obj
    End Function
#End Region

End Class

