﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Reflection
Imports System.Text
Imports System.Xml
Imports System.Xml.Serialization

Module Module1
    Dim Log As String
    Dim fileName As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "config.xml")
    Dim enc As New EncLib.DataEncryption("")

    Sub Main()
        Try
            Log = ""
            Dim config As New UploadConfig()
            config = LoadConfiguration()

            If config Is Nothing Then
                Console.WriteLine("Error: Configuration settings not found!")
                Return
            End If

            Console.WriteLine(("Uploading data started at " + DateTime.Now.ToString("H:mm:ss")))
            Console.WriteLine("Please wait ...")
            Dim dt As DataTable = GetShopSales(config)

            If dt.Rows.Count = 0 Then
                Console.WriteLine("Error: No new transactions found!")
                Return
            End If

            Dim maxReceipt As Int32 = dt.Compute("Max(receipt_number)", "")

            Dim csvData As String = String.Format("merchant={0}&data={1}", 3, dt.DataTableToCSV(","))

            Dim responseObject As HttpWebResponse = Send(csvData, config.RemoteUserName, config.RemotePassword)

            If responseObject.StatusCode = 200 Then
                Dim xmlString As String
                config.MaxRecieptId = maxReceipt
                xmlString = ToXml(config, config.GetType())
                WriteXMLToFile(enc.Encrypt(xmlString), fileName)
            End If

            Dim statusContent = String.Format("Merchant ID: {3}{2}date: {0}{2}Status: {1}{2}Max Receipt Number: {4}{2}Total Records: {5}{2}", DateTime.Now, responseObject.StatusDescription, vbNewLine, config.ShopId, maxReceipt, dt.Rows.Count)

            File.AppendAllText("log.txt", statusContent)

            sendMail("Retailers Data Upload Status", statusContent)

            Console.WriteLine(("Sending data complete at " + DateTime.Now.ToString("H:mm:ss")))
            Console.WriteLine("Press any key to continue")
            Console.ReadLine()
        Catch ex As Exception
            File.AppendAllText("log.txt", String.Format("date: {0}  Status: Process Failed: {1} {2}", DateTime.Now, ex.Message, vbNewLine))
        End Try

    End Sub


    Private Function GetShopSales(config As UploadConfig) As DataTable
        Dim dt As System.Data.DataTable = New DataTable
        Dim query As String = String.Format("select * from {0}", config.ViewName)
        If config.MaxRecieptId > 0 Then
            query = String.Format("select * from {0} WHERE receipt_number > {1}", config.ViewName, config.MaxRecieptId)
        End If
        Dim con As System.Data.SqlClient.SqlConnection = New SqlConnection(String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", config.DatabaseHost, config.DatabaseName, config.UserName, config.Password))
        Dim cmd As SqlCommand = New SqlCommand(query, con)
        Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
        con.Open()
        da.Fill(dt)
        Return dt
    End Function

    <Obsolete>
    Private Sub SendData(ByVal parameters As String)
        Dim url As String = ("https://cbttriangulation.jor.wfp.org/api?" + parameters)
        Dim req = WebRequest.Create(url)
        Dim enc = New UTF8Encoding(False)
        Dim res = req.GetResponse
        Dim rd = (New StreamReader(res.GetResponseStream)).ReadToEnd()
        Console.WriteLine(rd.ToString)
    End Sub

    Friend Function Send(XMLdata As String, RequestUsername As String, RequestPassword As String) As HttpWebResponse
        Dim requestData As String = XMLdata
        Dim responseData As String = ""
        Dim URL As String = "https://cbttriangulation.jor.wfp.org/api/post.php"

        Dim myWriter As StreamWriter = Nothing
        Dim objRequest As HttpWebRequest = DirectCast(WebRequest.Create(URL), HttpWebRequest)

        objRequest.Method = "POST"
        objRequest.PreAuthenticate = True
        objRequest.ContentType = "application/x-www-form-urlencoded"
        objRequest.Credentials = New NetworkCredential(RequestUsername, RequestPassword)


        objRequest.ContentLength = requestData.Length

        Try
            myWriter = New StreamWriter(objRequest.GetRequestStream())
            myWriter.Write(requestData)
        Finally
            myWriter.Close()
        End Try

        Dim sr As StreamReader = Nothing
        Dim objResponse As HttpWebResponse
        Try
            objResponse = DirectCast(objRequest.GetResponse(), HttpWebResponse)
            sr = New StreamReader(objResponse.GetResponseStream())
            responseData = sr.ReadToEnd()

        Finally
            If sr IsNot Nothing Then
                sr.Close()
            End If
        End Try

        Return objResponse
    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function DataTableToCSV(datatable As DataTable, seperator As Char) As String
        Dim sb As New StringBuilder()
        For i As Integer = 0 To datatable.Columns.Count - 1
            sb.Append(datatable.Columns(i))
            If i < datatable.Columns.Count - 1 Then
                sb.Append(seperator)
            End If
        Next
        sb.AppendLine()

        For Each dr As DataRow In datatable.Rows
            For i As Integer = 0 To datatable.Columns.Count - 1
                sb.Append(Uri.EscapeDataString(StringToCSVCell(dr(i).ToString())))

                If i < datatable.Columns.Count - 1 Then
                    sb.Append(seperator)
                End If
            Next
            sb.AppendLine()
        Next
        Return sb.ToString()
    End Function

    Public Function StringToCSVCell(str As String) As String
        Dim mustQuote As Boolean = (str.Contains(",") OrElse str.Contains("""") OrElse str.Contains(vbCr) OrElse str.Contains(vbLf))
        If mustQuote Then
            Dim sb As New StringBuilder()
            sb.Append("""")
            For Each nextChar As Char In str
                sb.Append(nextChar)
                If nextChar = """"c Then
                    sb.Append("""")
                End If
            Next
            sb.Append("""")
            Return sb.ToString()
        End If

        Return str
    End Function

    Private Function LoadConfiguration() As UploadConfig

        If Not IO.File.Exists(fileName) Then
            Return Nothing
        End If

        Dim encConfig As String = IO.File.ReadAllText(fileName)
        Dim decryptedConfig As String = enc.Decrypt(encConfig)

        Dim serializer As New XmlSerializer(GetType(UploadConfig))
        Dim rdr As New StringReader(decryptedConfig)
        Dim objConfig As UploadConfig = DirectCast(serializer.Deserialize(rdr), UploadConfig)

        Return objConfig

    End Function

    Public Sub WriteXMLToFile(ByVal xmlString As Object, ByVal Location As String, Optional ByVal Overwrite As Boolean = True)
        If IO.File.Exists(Location) Then
            If Overwrite = True Then
                IO.File.Delete(Location)
            End If
        End If

        File.WriteAllText(Location, xmlString)
    End Sub

    Public Function ToXml(ByVal Obj As Object, ByVal ObjType As System.Type) As String
        Dim ser As XmlSerializer
        ser = New XmlSerializer(ObjType, String.Empty)
        Dim memStream As MemoryStream
        memStream = New MemoryStream()
        Dim xmlWriter As XmlTextWriter
        xmlWriter = New XmlTextWriter(memStream, Encoding.UTF8)
        xmlWriter.Namespaces = True

        Dim ns As New XmlSerializerNamespaces()
        ns.Add("", "")

        ser.Serialize(xmlWriter, Obj, ns)
        xmlWriter.Close()
        memStream.Close()
        Dim xml As String
        xml = Encoding.UTF8.GetString(memStream.GetBuffer())
        xml = xml.Substring(xml.IndexOf(Convert.ToChar(60)))
        xml = xml.Substring(0, (xml.LastIndexOf(Convert.ToChar(62)) + 1))
        Return xml
    End Function

    Sub sendMail(ByVal title As String, ByVal content As String)
        Dim SmtpServer As New SmtpClient("smtp.gmail.com", 587)
        SmtpServer.EnableSsl = True
        SmtpServer.Credentials = New Net.NetworkCredential("Jordan.RetailersData@gmail.com", "RetailersData1")
        Dim mail As New MailMessage("Jordan.RetailersData@gmail.com", "omar.khalidi@wfp.org,boster.sibande@wfp.org", title, content)
        mail.From = New MailAddress("Jordan.RetailersData@gmail.com", "Retailers Sales Data", System.Text.Encoding.UTF8)

        SmtpServer.Send(mail)
    End Sub

End Module
