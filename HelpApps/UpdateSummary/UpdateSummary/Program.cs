﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace UpdateSummary
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;
            int month;
            string stmonth;
            string cycle;
            for(year=2014;year<=2016;year++){
                for(month=1;month<=12;month++){
                    stmonth=month<10?"0"+month.ToString():month.ToString();
                    cycle=year.ToString()+stmonth.ToString();
                    string previousmonth=GetLastMonth(cycle);
                    string cumulative_households="Update summary set cumulative_households=(select COUNT(distinct customer_id) from prepaid_reload_meps where left(Trans_Date,6)<="+cycle+") where cycle="+cycle+"";
                    RunCommand(cumulative_households);

                    string cumulative_reload_cycles="Update summary set cumulative_reload_cycles=(select Count(Distinct left(Trans_Date,6)) from prepaid_reload_meps where left(Trans_Date,6)<="+cycle+") where cycle="+cycle+"";
                    RunCommand(cumulative_reload_cycles);
        
                    string cumulative_reload_value="Update summary set cumulative_reload_value=(select SUM(Trans_Amount) from prepaid_reload_meps where left(Trans_Date,6)<="+cycle+") where cycle="+cycle+"";
                    RunCommand(cumulative_reload_value);
        
                    string cumulative_no_of_reloads="Update summary set cumulative_no_of_reloads=(select Count(*) from prepaid_reload_meps where left(Trans_Date,6)<="+cycle+") where cycle="+cycle+"";
                    RunCommand(cumulative_no_of_reloads);

                    string cumulative_sales_transactions="Update summary set cumulative_sales_transactions=(select Count(*) from sales_draft where left(Trans_Date,6)<="+cycle+") where cycle="+cycle+"";
                    RunCommand(cumulative_sales_transactions);

                    string cumulative_sales_value="Update summary set cumulative_sales_value=(select Sum(Trans_Amount) from sales_draft where left(Trans_Date,6)<="+cycle+") where cycle="+cycle+"";
                    RunCommand(cumulative_sales_value);

                    string thismonth_households="Update summary set thismonth_households=(select Count(distinct Customer_ID) from pre_paid_reload_meps_"+cycle+") where cycle="+cycle+"";
                    RunCommand(thismonth_households);

                    string thismonth_reload_value="Update summary set thismonth_reload_value=(select Sum(Trans_Amount) from pre_paid_reload_meps_"+cycle+") where cycle="+cycle+"";
                    RunCommand(thismonth_reload_value);

                    string thismonth_sales_transactions="Update summary set thismonth_sales_transactions=(select count(*) from sales_draft_"+cycle+") where cycle="+cycle+"";
                    RunCommand(thismonth_sales_transactions);

                    string thismonth_reload_cycles="Update summary set thismonth_reload_cycles=1 where cycle="+cycle+"";
                    RunCommand(thismonth_reload_cycles);

                    if (month > 201401)
                    {
                        string previousmonth_households = "Update summary set previousmonth_households=(select Count(distinct Customer_ID) from pre_paid_reload_meps_" + previousmonth + ") where cycle=" + cycle + "";
                        RunCommand(previousmonth_households);

                        string previousmonth_reload_value = "Update summary set previousmonth_reload_value=(select Sum(Trans_Amount) from pre_paid_reload_meps_" + previousmonth + ") where cycle=" + cycle + "";
                        RunCommand(previousmonth_reload_value);

                        string previousmonth_sales_transactions = "Update summary set previousmonth_sales_transactions=(select count(*) from sales_draft_" + previousmonth + ") where cycle=" + cycle + "";
                        RunCommand(previousmonth_sales_transactions);

                        string previousmonth_reload_cycles = "Update summary set previousmonth_reload_cycles=1 where cycle=" + cycle + "";
                        RunCommand(previousmonth_reload_cycles);
                    }
                    string loa_reload="Update summary set loa_reload=(select Sum(Trans_Amount) from pre_paid_reload_loa where cycle="+cycle+") where cycle="+cycle+"";
                    RunCommand(loa_reload);
        
                    string wfp_reload="Update summary set wfp_reload=(select Sum(Trans_Amount) from pre_paid_reload_wfp_"+cycle+") where cycle="+cycle+"";
                    RunCommand(wfp_reload);

                    string jab_reload="Update summary set jab_reload=(select Sum(Amount) from dbo.jab_statement_"+cycle+") where cycle="+cycle+"";
                    RunCommand(jab_reload);

                    string meps_reload="Update summary set meps_reload=(select Sum(Trans_Amount) from pre_paid_reload_meps_"+cycle+") where cycle="+cycle+"";
                    RunCommand(meps_reload);

                    string jab_pressed="Update summary set jab_pressed=(select count(*) from jab_vrl where substring(Creation_Date,7,4)+substring(Creation_Date,4,2)='cycle') where cycle="+cycle+"";
                    RunCommand(jab_pressed);

                    string programme_listing="Update summary set programme_listing=(select count(*) from vrl) where cycle="+cycle+"";
                    RunCommand(programme_listing);

                    string distributed="Update summary set distributed_cards=(select count(*) from vrl where customer_id in (select customer_id from prepaid_reload_meps) AND customer_id NOT IN (select customer_id from Replaced_cards)) where cycle="+cycle+"";
                    RunCommand(distributed);

                    string destroyed="Update summary set destroyed_cards=(select count(*) from Destroyed_Cards) where cycle="+cycle+"";
                    RunCommand(destroyed);

                    string undistributed="Update summary set undistributed_cards=(select count(*) from vrl where customer_id not in (select customer_id from prepaid_reload_meps) and customer_id not in (select customer_id from destroyed_cards)) where cycle="+cycle+"";
                    RunCommand(undistributed);

                    string total_unused="Update summary set total_unused=(select sum(unused) as unused from UnusedResidualOverdraft where Month="+cycle+" and unused>0) where cycle="+cycle+"";
                    RunCommand(total_unused);

                    string total_residual="Update summary set total_Residual=(select sum(Residual) as Residual from UnusedResidualOverdraft where Month="+cycle+" and Residual>0) where cycle="+cycle+"";
                    RunCommand(total_residual);

                    string total_overdraft="Update summary set total_Overdraft=(select sum(Overdraft) as Overdraft from UnusedResidualOverdraft where Month="+cycle+") where cycle="+cycle+"";
                    RunCommand(total_overdraft);

                    string odd_sale_times="Update summary set odd_sale_times=(select COUNT(*)  from sales_draft_"+cycle+" where left(Trans_Time,2)>='00' and left(Trans_Time,2)<='05') where cycle="+cycle+"";
                    RunCommand(odd_sale_times);

                    string duplicate_auth="Update summary set duplicate_auth=(select COUNT(*)  from (select auth_no,count(*) as trans from dbo.sales_draft_"+cycle+" group by auth_no having COUNT(*)>1) as table1) where cycle="+cycle+"";
                    RunCommand(duplicate_auth);

                    string merchants_shopped="Update summary set merchants_shopped=(select COUNT(distinct merchant) from sales_draft_"+cycle+") where cycle="+cycle+"";
                    RunCommand(merchants_shopped);

                    string processing_transaction_date_variation="Update summary set processing_transaction_date_variation=(SELECT max(datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))) as Days FROM sales_draft_"+cycle+") where cycle="+cycle+"";
                    RunCommand(processing_transaction_date_variation);

                    string cumulative_sales_by_merchant="Update summary set cumulative_sales_by_merchant=(select Count(*) from sales_draft where left(Trans_Date,6)<="+cycle+") where cycle="+cycle+"";
                    RunCommand(cumulative_sales_by_merchant);

                    string most_purchased_items="Update summary set most_purchased_items=(SELECT TOP (1) Item_Description FROM dbo.merchant_sales_"+cycle+" GROUP BY Item_Description ORDER BY COUNT(*) DESC) where cycle="+cycle+"";
                    RunCommand(most_purchased_items);

                    string nfi="Update summary set nfi=(SELECT top(1) Item_Description FROM dbo.merchant_sales_201511 INNER JOIN dbo.merchant_listing ON dbo.merchant_sales_201511.Item_Description = dbo.merchant_listing.Brand_Name + dbo.merchant_listing.Package_Measure WHERE (dbo.merchant_listing.Generic_Name = 'NFI')) where cycle="+cycle+"";
                    RunCommand(nfi);

                    //food_basket_pricing_contracted_merchants
                    //food_quality
                    //shop_inspection
                    //dos_pricing
                    //food_basket_pricing_average
                    //food_basket_pricing_transfer_value

                }
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
  

        private static string GetLastMonth(string thismonth){
            string date=thismonth.Substring(0,4)+"-"+thismonth.Substring(4,2)+"-01";
            DateTime monthdate=DateTime.Parse(date).AddMonths(-1);
            return monthdate.ToString("yyyyMM");
        }


        private static void RunCommand(string CommandName)
        {
            int rows = 0;
            string conString = "Server=(local);Database=jordandb;User Id=sa;Password=P@ssword1";
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(CommandName, con);
                cmd.CommandTimeout = 600;
                rows = cmd.ExecuteNonQuery();
            }
        }
    }
}
