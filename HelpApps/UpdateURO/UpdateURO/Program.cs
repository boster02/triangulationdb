﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace UpdateURO
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection("Data Source=127.0.0.1;Initial Catalog=jordandb;User ID=sa;Password=P@ssword1"))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select Customer_ID,cycle,Reload,Sales from Reload_Sales_by_Month order by Customer_ID,cycle", conn);
                SqlDataReader reader = cmd.ExecuteReader();
                int i = 0;
                int c = 0;
                int u = 0;
                float[] salesarr = new float[100];
                float[] reloadarr = new float[100];
                string[] customer_arr = new string[5000000];
                float unused = 0;
                float p_unused = 0;
                float residual = 0;
                float overdraft = 0;
                float cummulative_reload = 0;
                float cummulative_sales = 0;
                float variance = 0;
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        i++;
                        c++;
                        u++;
                        customer_arr[c] = reader["Customer_ID"].ToString();
                        if (i > 1 && customer_arr[c] != customer_arr[c - 1])
                        {
                            //different customer, initialise values
                            unused = 0;
                            residual = 0;
                            overdraft = 0;
                            cummulative_reload = 0;
                            cummulative_sales = 0;
                            variance = 0;
                            i = 1;
                        }
                        string customer_id = reader["Customer_ID"].ToString();
                        int month = Int32.Parse(reader["cycle"].ToString());
                        float reload = Val(reader["Reload"].ToString());
                        float sales = Val(reader["Sales"].ToString());
                        reloadarr[i] = reload;
                        salesarr[i] = sales;
                        if (month < 201506)
                        {
                            if (sales == 0)
                            {
                                unused = reload;
                                p_unused = 0;
                            }
                            else
                            {
                                cummulative_reload += Val(reader["Reload"].ToString());
                                cummulative_sales += Val(reader["Sales"].ToString());
                                variance = cummulative_reload - cummulative_sales;
                            }
                            if (variance > 0)
                                residual = variance;
                            else if (variance < 0)
                                overdraft = variance;
                        }
                        else
                        {
                            if (i > 1 && sales == 0)
                            {
                                if (salesarr[i - 1] == 0 && month > 201506 & u > 1){
                                    unused = reload + reloadarr[i - 1];
                                    u = 0;
                                    p_unused = 0;
                                }
                            }
                            else if (i == 1 && sales==0)
                            {
                                p_unused = reload;
                            }
                            else if (month == 201506 && sales == 0)
                            {
                                p_unused = reload;
                            }
                            else
                            {
                                cummulative_reload += Val(reader["Reload"].ToString());
                                cummulative_sales += Val(reader["Sales"].ToString());
                                variance = cummulative_reload - cummulative_sales;
                            }
                            if (variance > 0)
                                residual = variance;
                            else if (variance < 0)
                                overdraft = variance;
                        }

                        string sql = string.Format("INSERT INTO [jordandb].[dbo].[UnusedResidualOverdraft] "
                            + "([Customer_ID],[Month],[Reload],[Sales],[Punused],[Unused],[Residual],[Overdraft])"
                            + "VALUES('{0}','{1}', '{2}', '{3}','{4}','{5}','{6}','{7}');", customer_id, month, reload, sales,p_unused, unused, residual, overdraft);
                        using (SqlConnection conn2 = new SqlConnection("Data Source=127.0.0.1;Initial Catalog=jordandb;User ID=sa;Password=P@ssword1"))
                        {
                            conn2.Open();
                            Console.WriteLine(c);
                            SqlCommand cmd2 = new SqlCommand(sql, conn2);
                            cmd2.ExecuteNonQuery();
                            unused = 0;
                        }
                    }
                }
            }
        }
        public static float Val(string Expression)
        {
            float dblValue;
            if (float.TryParse(Expression, out dblValue))
                return dblValue;
            else
                return 0;
        }
    }
}


