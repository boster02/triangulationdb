﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace retailer_data
{
    [ServiceContract]
    public interface IMerchantData
    {
        [OperationContract]
        bool GetSalesData(string DATE, string TIME, string RECEIPT_NO, string SKU_CODE, string BARCODE, 
            string SKU_DESCRIPTION, string QUANTITY, string UNIT_PRICE, string LINE_TOTAL, string TAX, 
            string BENEFICIARY_ID, string PAYMENT_TYPE, string PACKAGING);

        [OperationContract]
        bool GetPurchasesData(string DATE, string SKU_CODE, string BARCODE, string PACKAGING, 
            string SKU_DESCRIPTION, string COST_PRICE, string Supplier);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

    }


    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
