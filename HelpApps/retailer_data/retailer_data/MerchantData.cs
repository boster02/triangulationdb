﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace retailer_data
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class MerchantData : IMerchantData
    {
        public bool GetSalesData(string DATE, string TIME, string RECEIPT_NO, string SKU_CODE, string BARCODE,
            string SKU_DESCRIPTION, string QUANTITY, string UNIT_PRICE, string LINE_TOTAL, string TAX,
            string BENEFICIARY_ID, string PAYMENT_TYPE, string PACKAGING)
        {
            return true;
        }
        public bool GetPurchasesData(string DATE, string SKU_CODE, string BARCODE, string PACKAGING,
            string SKU_DESCRIPTION, string COST_PRICE, string Supplier)
        {
            return true;
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
