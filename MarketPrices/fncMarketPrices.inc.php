<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('MAX_EXECUTION_TIME', 900);
ini_set('memory_limit', '-1');
include("../config.inc.php");
include("../mails/fncMailSender.inc.php");
function fncSendMonthlyReport($year,$month,$office){
    if($office==3){
        $sql1="    Select * from
		    (SELECT  dbo.Price_Monitoring_Offices.Office,
		    concat(dbo.Retailer_Types.ID,'. ',dbo.Retailer_Types.Retailer_Type) as Retailer_Type,
		    COUNT(DISTINCT dbo.price_monitoring.Retailer) AS No_of_Retailers
		    FROM            dbo.price_monitoring INNER JOIN
		    dbo.Retailer_Types ON dbo.price_monitoring.Retailer_Type = dbo.Retailer_Types.ID INNER JOIN
		    dbo.Price_Monitoring_Offices ON dbo.price_monitoring.Data_Gatherer_Office = dbo.Price_Monitoring_Offices.id
		    WHERE        (dbo.price_monitoring.Year = $year) AND (dbo.price_monitoring.Month = $month)
		    GROUP BY concat(dbo.Retailer_Types.ID,'. ',dbo.Retailer_Types.Retailer_Type), dbo.Price_Monitoring_Offices.Office) as PivotData
	        PIVOT (SUM(No_of_Retailers) for Office in ([SCI]))
	        as pivotresult";
       $sql2="SELECT        TOP (100) PERCENT dbo.Price_Monitoring_Offices.Office, dbo.price_monitoring.Data_Gatherer,
            dbo.Retailer_Types.Retailer_Type, COUNT(DISTINCT dbo.price_monitoring.Retailer) AS No_of_Retailers,
            COUNT(DISTINCT dbo.price_monitoring.Commodity) AS No_of_Commodities
            FROM            dbo.price_monitoring INNER JOIN
            dbo.Retailer_Types ON dbo.price_monitoring.Retailer_Type = dbo.Retailer_Types.ID INNER JOIN
            dbo.Price_Monitoring_Offices ON dbo.price_monitoring.Data_Gatherer_Office = dbo.Price_Monitoring_Offices.id
            WHERE        (dbo.price_monitoring.Year = $year) AND (dbo.price_monitoring.Month = $month) and Data_Gatherer_Office=3
            GROUP BY dbo.price_monitoring.Data_Gatherer, dbo.Retailer_Types.Retailer_Type, dbo.Price_Monitoring_Offices.Office
            ORDER BY dbo.Price_Monitoring_Offices.Office, dbo.price_monitoring.Data_Gatherer";
    }
    elseif($office==4){
        $sql1="    Select * from
		    (SELECT  dbo.Price_Monitoring_Offices.Office,
		    concat(dbo.Retailer_Types.ID,'. ',dbo.Retailer_Types.Retailer_Type) as Retailer_Type,
		    COUNT(DISTINCT dbo.price_monitoring.Retailer) AS No_of_Retailers
		    FROM            dbo.price_monitoring INNER JOIN
		    dbo.Retailer_Types ON dbo.price_monitoring.Retailer_Type = dbo.Retailer_Types.ID INNER JOIN
		    dbo.Price_Monitoring_Offices ON dbo.price_monitoring.Data_Gatherer_Office = dbo.Price_Monitoring_Offices.id
		    WHERE        (dbo.price_monitoring.Year = $year) AND (dbo.price_monitoring.Month = $month)
		    GROUP BY concat(dbo.Retailer_Types.ID,'. ',dbo.Retailer_Types.Retailer_Type), dbo.Price_Monitoring_Offices.Office) as PivotData
	        PIVOT (SUM(No_of_Retailers) for Office in ([ACTED]))
	        as pivotresult";
        $sql2="SELECT        TOP (100) PERCENT dbo.Price_Monitoring_Offices.Office, dbo.price_monitoring.Data_Gatherer,
            dbo.Retailer_Types.Retailer_Type, COUNT(DISTINCT dbo.price_monitoring.Retailer) AS No_of_Retailers,
            COUNT(DISTINCT dbo.price_monitoring.Commodity) AS No_of_Commodities
            FROM            dbo.price_monitoring INNER JOIN
            dbo.Retailer_Types ON dbo.price_monitoring.Retailer_Type = dbo.Retailer_Types.ID INNER JOIN
            dbo.Price_Monitoring_Offices ON dbo.price_monitoring.Data_Gatherer_Office = dbo.Price_Monitoring_Offices.id
            WHERE        (dbo.price_monitoring.Year = $year) AND (dbo.price_monitoring.Month = $month) and Data_Gatherer_Office=4
            GROUP BY dbo.price_monitoring.Data_Gatherer, dbo.Retailer_Types.Retailer_Type, dbo.Price_Monitoring_Offices.Office
            ORDER BY dbo.Price_Monitoring_Offices.Office, dbo.price_monitoring.Data_Gatherer";
    }
    else{
        $sql1="    Select * from
		    (SELECT  dbo.Price_Monitoring_Offices.Office,
		    concat(dbo.Retailer_Types.ID,'. ',dbo.Retailer_Types.Retailer_Type) as Retailer_Type,
		    COUNT(DISTINCT dbo.price_monitoring.Retailer) AS No_of_Retailers
		    FROM            dbo.price_monitoring INNER JOIN
		    dbo.Retailer_Types ON dbo.price_monitoring.Retailer_Type = dbo.Retailer_Types.ID INNER JOIN
		    dbo.Price_Monitoring_Offices ON dbo.price_monitoring.Data_Gatherer_Office = dbo.Price_Monitoring_Offices.id
		    WHERE        (dbo.price_monitoring.Year = $year) AND (dbo.price_monitoring.Month = $month)
		    GROUP BY concat(dbo.Retailer_Types.ID,'. ',dbo.Retailer_Types.Retailer_Type), dbo.Price_Monitoring_Offices.Office) as PivotData
	        PIVOT (SUM(No_of_Retailers) for Office in ([Amman SO],[Mafraq SO],[SCI],[ACTED]))
	        as pivotresult";
        $sql2="SELECT        TOP (100) PERCENT dbo.Price_Monitoring_Offices.Office, dbo.price_monitoring.Data_Gatherer,
            dbo.Retailer_Types.Retailer_Type, COUNT(DISTINCT dbo.price_monitoring.Retailer) AS No_of_Retailers,
            COUNT(DISTINCT dbo.price_monitoring.Commodity) AS No_of_Commodities
            FROM            dbo.price_monitoring INNER JOIN
            dbo.Retailer_Types ON dbo.price_monitoring.Retailer_Type = dbo.Retailer_Types.ID INNER JOIN
            dbo.Price_Monitoring_Offices ON dbo.price_monitoring.Data_Gatherer_Office = dbo.Price_Monitoring_Offices.id
            WHERE        (dbo.price_monitoring.Year = $year) AND (dbo.price_monitoring.Month = $month)
            GROUP BY dbo.price_monitoring.Data_Gatherer, dbo.Retailer_Types.Retailer_Type, dbo.Price_Monitoring_Offices.Office
            ORDER BY dbo.Price_Monitoring_Offices.Office, dbo.price_monitoring.Data_Gatherer";
    }
    $month_name=fncGetMonthName($year.str_pad($month,2,"0",STR_PAD_LEFT));
    $txt="<div style='font-family:Calibri'><h1>Price monitoring data collection update for $month_name</h1>";
    $txt.="<strong>Summary by Office</strong> <br />";
    $txt.=createTable($sql1);
    $txt.="<br /><br /><strong>Summary by Office and Data Gatherer</strong> <br />";
    $txt.=createTable($sql2);
    $txt.="</div>";
    $success=fncSendEmailToAdmins("Price monitoring data collection update for $month_name",$txt,$office);
    return $success;
}
function fncSendEmailToAdmins($subject,$message,$office){
    $dbh=fncOpenDBConn();
    $sql ="select name,email from price_monitoring_admins where status='Y' and office=".$office;
    $res = mssql_query($sql,$dbh);
    $to[0][0]=0;
    $cc[0][0]=0;
    $bcc[0][0]=0;
    $to[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$to[0][0];$i++){
        $row = mssql_fetch_array($res);
        $to[$i][0]=$row["name"];
        $to[$i][1]=$row["email"];
	}
    mssql_close($dbh);
    return authMail($to, $cc, $bcc, $subject, $message);
}
function createTable($sql) {
    $dbh=fncOpenDBConn();
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $flag = false;
    $txt="";
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);
        if(!$flag) {
            $txt="<table border='1' cellpadding='3' style='border-collapse:collapse'>";
            $style='style="background-color: #003366;color: white;text-align: left"';
            $txt.="<tr><th $style>".implode("</th><th $style>", array_keys($row)) . "</th></tr>";
            $txt=str_replace("_"," ",$txt);
            $flag = true;
        }
        $txt.="\r\n<tr><td>".implode("</td><td>", array_values($row)) . "</td></tr>";
    }
    $txt.="    </table>";
    return $txt;
}
function fncGetMonthName($month){
    return strtoupper(date('M - Y',strtotime($month."01")));
}