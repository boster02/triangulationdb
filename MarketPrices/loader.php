<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            getData("content.html", getURLParameter("month"), getURLParameter("year"))
        });
        function getData(p, pmonth, pyear) {
            var page = p;
            $('#loadingmessage').show();  // show the loading message.
            $.ajax({
                url: p,
                data: { month: pmonth, year: pyear },
                type: "POST",
                cache: false,
                success: function (html) {
                    $(".content").html(html);
                    $('#loadingmessage').hide(); // hide the loading message
                },
                error: function () {
                    $(".content").html("<h1>Error. The report could not be retrieved</h1>");
                    $('#loadingmessage').hide();
                }
            });
        }
        function getURLParameter(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null
        }
    </script>
</head>
<body>
    <div id='loadingmessage' style='display: none; width: 100%; text-align: center'>
        <img src='images/download.gif' />
    </div>
    <div class="content"></div>
</body>
</html>
