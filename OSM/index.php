<?php session_start();
  if (isset($_GET["order"])) $order = @$_GET["order"];
  if (isset($_GET["type"])) $ordtype = @$_GET["type"];

  if (isset($_POST["filter"])) $filter = @$_POST["filter"];
  if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
  $wholeonly = false;
  if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

  if (!isset($order) && isset($_SESSION["order"])) $order = $_SESSION["order"];
  if (!isset($ordtype) && isset($_SESSION["type"])) $ordtype = $_SESSION["type"];
  if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
  if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"];

?>

<html>
<head>
<title>market_prices -- market_prices</title>
<meta name="generator" http-equiv="content-type" content="text/html">
<style type="text/css">
  body {
    background-color: #FFFFFF;
    color: #004080;
    font-family: Calibri;
    font-size: 12px;
  }
  .bd {
    background-color: #FFFFFF;
    color: #004080;
    font-family: Calibri;
    font-size: 12px;
  }
  .tbl {
    background-color: #FFFFFF;
  }
  a:link { 
    background-color: #FFFFFF01;
    color: #008080;
    font-family: Calibri;
    font-size: 12px;
  }
  a:active { 
    background-color: #FFFFFF01;
    color: #0000FF;
    font-family: Calibri;
    font-size: 12px;
  }
  a:visited { 
    background-color: #FFFFFF01;
    color: #800080;
    font-family: Calibri;
    font-size: 12px;
  }
  .hr {
    background-color: #00BFFF;
    color: #FFFFFF;
    font-family: Calibri;
    font-size: 12px;
  }
  a.hr:link {
    color: #FFFFFF;
    font-family: Calibri;
    font-size: 12px;
  }
  a.hr:active {
    color: #FFFFFF;
    font-family: Calibri;
    font-size: 12px;
  }
  a.hr:visited {
    color: #FFFFFF;
    font-family: Calibri;
    font-size: 12px;
  }
  .dr {
    background-color: #FFFFFF;
    color: #000000;
    font-family: Calibri;
    font-size: 12px;
  }
  .sr {
    background-color: #EEEEEE;
    color: #000000;
    font-family: Calibri;
    font-size: 12px;
  }
</style>
</head>
<body>
<table class="bd" width="100%"><tr><td class="hr"><h2>Market Price Monitoring System</h2></td></tr></table>
<?php
  $conn = connect();
  $showrecs = 25;
  $pagerange = 10;

  $a = @$_GET["a"];
  $recid = @$_GET["recid"];
  $page = @$_GET["page"];
  if (!isset($page)) $page = 1;

  $sql = @$_POST["sql"];

  switch ($sql) {
    case "insert":
      sql_insert();
      break;
    case "update":
      sql_update();
      break;
    case "delete":
      sql_delete();
      break;
  }

  switch ($a) {
    case "add":
      addrec();
      break;
    case "view":
      viewrec($recid);
      break;
    case "edit":
      editrec($recid);
      break;
    case "del":
      deleterec($recid);
      break;
    default:
      select();
      break;
  }

  if (isset($order)) $_SESSION["order"] = $order;
  if (isset($ordtype)) $_SESSION["type"] = $ordtype;
  if (isset($filter)) $_SESSION["filter"] = $filter;
  if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;
  if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

  mysql_close($conn);
?>
</body>
</html>

<?php function select()
  {
  global $a;
  global $showrecs;
  global $page;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $order;
  global $ordtype;


  if ($a == "reset") {
    $filter = "";
    $filterfield = "";
    $wholeonly = "";
    $order = "";
    $ordtype = "";
  }

  $checkstr = "";
  if ($wholeonly) $checkstr = " checked";
  if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
  $res = sql_select();
  $count = sql_getrecordcount();
  if ($count % $showrecs != 0) {
    $pagecount = intval($count / $showrecs) + 1;
  }
  else {
    $pagecount = intval($count / $showrecs);
  }
  $startrec = $showrecs * ($page - 1);
  if ($startrec < $count) {mysql_data_seek($res, $startrec);}
  $reccount = min($showrecs * $page, $count);
?>
<hr size="1" noshade>
<form action="index.php" method="post">
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><b>Custom Filter</b>&nbsp;</td>
<td><input type="text" name="filter" value="<?php echo $filter ?>"></td>
<td><select name="filter_field">
<option value="">All Fields</option>
<option value="<?php echo "year" ?>"<?php if ($filterfield == "year") { echo "selected"; } ?>><?php echo htmlspecialchars("year") ?></option>
<option value="<?php echo "lp_month" ?>"<?php if ($filterfield == "lp_month") { echo "selected"; } ?>><?php echo htmlspecialchars("month") ?></option>
<option value="<?php echo "week" ?>"<?php if ($filterfield == "week") { echo "selected"; } ?>><?php echo htmlspecialchars("week") ?></option>
<option value="<?php echo "data_collection_date" ?>"<?php if ($filterfield == "data_collection_date") { echo "selected"; } ?>><?php echo htmlspecialchars("data_collection_date") ?></option>
<option value="<?php echo "data_gatherer" ?>"<?php if ($filterfield == "data_gatherer") { echo "selected"; } ?>><?php echo htmlspecialchars("data_gatherer") ?></option>
<option value="<?php echo "data_gatherer_phone" ?>"<?php if ($filterfield == "data_gatherer_phone") { echo "selected"; } ?>><?php echo htmlspecialchars("data_gatherer_phone") ?></option>
<option value="<?php echo "lp_state" ?>"<?php if ($filterfield == "lp_state") { echo "selected"; } ?>><?php echo htmlspecialchars("state") ?></option>
<option value="<?php echo "lp_county" ?>"<?php if ($filterfield == "lp_county") { echo "selected"; } ?>><?php echo htmlspecialchars("county") ?></option>
<option value="<?php echo "lp_market" ?>"<?php if ($filterfield == "lp_market") { echo "selected"; } ?>><?php echo htmlspecialchars("market") ?></option>
<option value="<?php echo "lp_commodity" ?>"<?php if ($filterfield == "lp_commodity") { echo "selected"; } ?>><?php echo htmlspecialchars("commodity") ?></option>
<option value="<?php echo "lp_unit" ?>"<?php if ($filterfield == "lp_unit") { echo "selected"; } ?>><?php echo htmlspecialchars("unit") ?></option>
<option value="<?php echo "lp_price_type" ?>"<?php if ($filterfield == "lp_price_type") { echo "selected"; } ?>><?php echo htmlspecialchars("price_type") ?></option>
<option value="<?php echo "price" ?>"<?php if ($filterfield == "price") { echo "selected"; } ?>><?php echo htmlspecialchars("price") ?></option>
<option value="<?php echo "lp_currency" ?>"<?php if ($filterfield == "lp_currency") { echo "selected"; } ?>><?php echo htmlspecialchars("currency") ?></option>
<option value="<?php echo "lp_product_source" ?>"<?php if ($filterfield == "lp_product_source") { echo "selected"; } ?>><?php echo htmlspecialchars("product_source") ?></option>
<option value="<?php echo "lp_supply_ratings" ?>"<?php if ($filterfield == "lp_supply_ratings") { echo "selected"; } ?>><?php echo htmlspecialchars("supply_ratings") ?></option>
<option value="<?php echo "minimum_volume" ?>"<?php if ($filterfield == "minimum_volume") { echo "selected"; } ?>><?php echo htmlspecialchars("minimum_volume") ?></option>
<option value="<?php echo "maximum_volume" ?>"<?php if ($filterfield == "maximum_volume") { echo "selected"; } ?>><?php echo htmlspecialchars("maximum_volume") ?></option>
<option value="<?php echo "lp_direction_of_flows" ?>"<?php if ($filterfield == "lp_direction_of_flows") { echo "selected"; } ?>><?php echo htmlspecialchars("direction_of_flows") ?></option>
</select></td>
<td><input type="checkbox" name="wholeonly"<?php echo $checkstr ?>>Whole words only</td>
</td></tr>
<tr>
<td>&nbsp;</td>
<td><input type="submit" name="action" value="Apply Filter"></td>
<td><a href="index.php?a=reset">Reset Filter</a></td>
</tr>
</table>
</form>
<hr size="1" noshade>
<br>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="100%">
<tr>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "year" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("year") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_month" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("month") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "week" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("week") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "data_collection_date" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("data_collection_date") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "data_gatherer" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("data_gatherer") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "data_gatherer_phone" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("data_gatherer_phone") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_state" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("state") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_county" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("county") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_market" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("market") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_commodity" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("commodity") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_unit" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("unit") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_price_type" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("price_type") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "price" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("price") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_currency" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("currency") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_product_source" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("product_source") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_supply_ratings" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("supply_ratings") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "minimum_volume" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("minimum_volume") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "maximum_volume" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("maximum_volume") ?></a></td>
<td class="hr"><a class="hr" href="index.php?order=<?php echo "lp_direction_of_flows" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("direction_of_flows") ?></a></td>
</tr>
<?php
  for ($i = $startrec; $i < $reccount; $i++)
  {
    $row = mysql_fetch_assoc($res);
    $style = "dr";
    if ($i % 2 != 0) {
      $style = "sr";
    }
?>
<tr>
<td class="<?php echo $style ?>"><a href="index.php?a=view&recid=<?php echo $i ?>">View</a></td>
<td class="<?php echo $style ?>"><a href="index.php?a=edit&recid=<?php echo $i ?>">Edit</a></td>
<td class="<?php echo $style ?>"><a href="index.php?a=del&recid=<?php echo $i ?>">Delete</a></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["year"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_month"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["week"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["data_collection_date"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["data_gatherer"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["data_gatherer_phone"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_state"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_county"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_market"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_commodity"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_unit"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_price_type"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["price"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_currency"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_product_source"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_supply_ratings"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["minimum_volume"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["maximum_volume"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_direction_of_flows"]) ?></td>
</tr>
<?php
  }
  mysql_free_result($res);
?>
</table>
<br>
<?php showpagenav($page, $pagecount); ?>
<?php } ?>

<?php function showrow($row, $recid)
  {
?>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("year")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["year"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("month")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_month"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("week")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["week"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("data_collection_date")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["data_collection_date"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("data_gatherer")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["data_gatherer"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("data_gatherer_phone")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["data_gatherer_phone"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("state")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_state"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("county")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_county"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("market")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_market"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("commodity")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_commodity"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("unit")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_unit"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("price_type")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_price_type"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("price")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["price"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("currency")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_currency"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("product_source")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_product_source"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("supply_ratings")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_supply_ratings"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("minimum_volume")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["minimum_volume"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("maximum_volume")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["maximum_volume"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("direction_of_flows")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_direction_of_flows"]) ?></td>
</tr>
</table>
<?php } ?>

<?php function showroweditor($row, $iseditmode)
  {
  global $conn;
?>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("id")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="id" value="<?php echo str_replace('"', '&quot;', trim($row["id"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("year")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="year" value="<?php echo str_replace('"', '&quot;', trim($row["year"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("month")."&nbsp;" ?></td>
<td class="dr"><select name="month">
<option value=""></option>
<?php
  $sql = "select `id`, `month` from `months`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["month"];
  if ($row["month"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("week")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="week" value="<?php echo str_replace('"', '&quot;', trim($row["week"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("data_collection_date")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="data_collection_date" maxlength="50" value="<?php echo str_replace('"', '&quot;', trim($row["data_collection_date"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("data_gatherer")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="data_gatherer" maxlength="50" value="<?php echo str_replace('"', '&quot;', trim($row["data_gatherer"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("data_gatherer_phone")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="data_gatherer_phone" maxlength="50" value="<?php echo str_replace('"', '&quot;', trim($row["data_gatherer_phone"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("state")."&nbsp;" ?></td>
<td class="dr"><select name="state">
<option value=""></option>
<?php
  $sql = "select `id`, `state` from `states`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["state"];
  if ($row["state"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("county")."&nbsp;" ?></td>
<td class="dr"><select name="county">
<option value=""></option>
<?php
  $sql = "select `id`, `county` from `counties`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["county"];
  if ($row["county"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("market")."&nbsp;" ?></td>
<td class="dr"><select name="market">
<option value=""></option>
<?php
  $sql = "select `id`, `market` from `markets`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["market"];
  if ($row["market"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("commodity")."&nbsp;" ?></td>
<td class="dr"><select name="commodity">
<option value=""></option>
<?php
  $sql = "select `id`, `Commodity` from `commodities`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["Commodity"];
  if ($row["commodity"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("unit")."&nbsp;" ?></td>
<td class="dr"><select name="unit">
<option value=""></option>
<?php
  $sql = "select `id`, `unit` from `units`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["unit"];
  if ($row["unit"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("price_type")."&nbsp;" ?></td>
<td class="dr"><select name="price_type">
<option value=""></option>
<?php
  $sql = "select `id`, `Price_type` from `price_type`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["Price_type"];
  if ($row["price_type"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("price")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="price" value="<?php echo str_replace('"', '&quot;', trim($row["price"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("currency")."&nbsp;" ?></td>
<td class="dr"><select name="currency">
<option value=""></option>
<?php
  $sql = "select `id`, `currency` from `currencies`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["currency"];
  if ($row["currency"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("product_source")."&nbsp;" ?></td>
<td class="dr"><select name="product_source">
<option value=""></option>
<?php
  $sql = "select `id`, `product_source` from `product_sources`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["product_source"];
  if ($row["product_source"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("supply_ratings")."&nbsp;" ?></td>
<td class="dr"><select name="supply_ratings">
<option value=""></option>
<?php
  $sql = "select `id`, `stock_rating` from `stock_ratings`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["stock_rating"];
  if ($row["supply_ratings"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("minimum_volume")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="minimum_volume" value="<?php echo str_replace('"', '&quot;', trim($row["minimum_volume"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("maximum_volume")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="maximum_volume" value="<?php echo str_replace('"', '&quot;', trim($row["maximum_volume"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("direction_of_flows")."&nbsp;" ?></td>
<td class="dr"><select name="direction_of_flows">
<option value=""></option>
<?php
  $sql = "select `id`, `direction` from `direction_of_flows`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["id"];
  $caption = $lp_row["direction"];
  if ($row["direction_of_flows"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
</table>
<?php } ?>

<?php function showpagenav($page, $pagecount)
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="index.php?a=add">Add Record</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="index.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Prev</a>&nbsp;</td>
<?php } ?>
<?php
  global $pagerange;

  if ($pagecount > 1) {

  if ($pagecount % $pagerange != 0) {
    $rangecount = intval($pagecount / $pagerange) + 1;
  }
  else {
    $rangecount = intval($pagecount / $pagerange);
  }
  for ($i = 1; $i < $rangecount + 1; $i++) {
    $startpage = (($i - 1) * $pagerange) + 1;
    $count = min($i * $pagerange, $pagecount);

    if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
      for ($j = $startpage; $j < $count + 1; $j++) {
        if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="index.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php } } } else { ?>
<td><a href="index.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php } } } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="index.php?page=<?php echo $page + 1 ?>">Next&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="index.php">Index Page</a></td>
<?php if ($recid > 0) { ?>
<td><a href="index.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Prior Record</a></td>
<?php } if ($recid < $count - 1) { ?>
<td><a href="index.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Next Record</a></td>
<?php } ?>
</tr>
</table>
<hr size="1" noshade>
<?php } ?>

<?php function addrec()
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="index.php">Index Page</a></td>
</tr>
</table>
<hr size="1" noshade>
<form enctype="multipart/form-data" action="index.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
$row = array(
  "id" => "",
  "year" => "",
  "month" => "",
  "week" => "",
  "data_collection_date" => "",
  "data_gatherer" => "",
  "data_gatherer_phone" => "",
  "state" => "",
  "county" => "",
  "market" => "",
  "commodity" => "",
  "unit" => "",
  "price_type" => "",
  "price" => "",
  "currency" => "",
  "product_source" => "",
  "supply_ratings" => "",
  "minimum_volume" => "",
  "maximum_volume" => "",
  "direction_of_flows" => "");
showroweditor($row, false);
?>
<p><input type="submit" name="action" value="Post"></p>
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysql_data_seek($res, $recid);
  $row = mysql_fetch_assoc($res);
  showrecnav("view", $recid, $count);
?>
<br>
<?php showrow($row, $recid) ?>
<br>
<hr size="1" noshade>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="index.php?a=add">Add Record</a></td>
<td><a href="index.php?a=edit&recid=<?php echo $recid ?>">Edit Record</a></td>
<td><a href="index.php?a=del&recid=<?php echo $recid ?>">Delete Record</a></td>
</tr>
</table>
<?php
  mysql_free_result($res);
} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysql_data_seek($res, $recid);
  $row = mysql_fetch_assoc($res);
  showrecnav("edit", $recid, $count);
?>
<br>
<form enctype="multipart/form-data" action="index.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xid" value="<?php echo $row["id"] ?>">
<?php showroweditor($row, true); ?>
<p><input type="submit" name="action" value="Post"></p>
</form>
<?php
  mysql_free_result($res);
} ?>

<?php function deleterec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysql_data_seek($res, $recid);
  $row = mysql_fetch_assoc($res);
  showrecnav("del", $recid, $count);
?>
<br>
<form action="index.php" method="post">
<input type="hidden" name="sql" value="delete">
<input type="hidden" name="xid" value="<?php echo $row["id"] ?>">
<?php showrow($row, $recid) ?>
<p><input type="submit" name="action" value="Confirm"></p>
</form>
<?php
  mysql_free_result($res);
} ?>

<?php function connect()
{
  $conn = mysql_connect("10.67.67.130", "root", "joomla");
  mysql_select_db("market_prices");
  return $conn;
}

function sqlvalue($val, $quote)
{
  if ($quote)
    $tmp = sqlstr($val);
  else
    $tmp = $val;
  if ($tmp == "")
    $tmp = "NULL";
  elseif ($quote)
    $tmp = "'".$tmp."'";
  return $tmp;
}

function sqlstr($val)
{
  return str_replace("'", "''", $val);
}

function sql_select()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT * FROM (SELECT t1.`id`, t1.`year`, t1.`month`, lp2.`month` AS `lp_month`, t1.`week`, t1.`data_collection_date`, t1.`data_gatherer`, t1.`data_gatherer_phone`, t1.`state`, lp7.`state` AS `lp_state`, t1.`county`, lp8.`county` AS `lp_county`, t1.`market`, lp9.`market` AS `lp_market`, t1.`commodity`, lp10.`Commodity` AS `lp_commodity`, t1.`unit`, lp11.`unit` AS `lp_unit`, t1.`price_type`, lp12.`Price_type` AS `lp_price_type`, t1.`price`, t1.`currency`, lp14.`currency` AS `lp_currency`, t1.`product_source`, lp15.`product_source` AS `lp_product_source`, t1.`supply_ratings`, lp16.`stock_rating` AS `lp_supply_ratings`, t1.`minimum_volume`, t1.`maximum_volume`, t1.`direction_of_flows`, lp19.`direction` AS `lp_direction_of_flows` FROM `market_prices` AS t1 LEFT OUTER JOIN `months` AS lp2 ON (t1.`month` = lp2.`id`) LEFT OUTER JOIN `states` AS lp7 ON (t1.`state` = lp7.`id`) LEFT OUTER JOIN `counties` AS lp8 ON (t1.`county` = lp8.`id`) LEFT OUTER JOIN `markets` AS lp9 ON (t1.`market` = lp9.`id`) LEFT OUTER JOIN `commodities` AS lp10 ON (t1.`commodity` = lp10.`id`) LEFT OUTER JOIN `units` AS lp11 ON (t1.`unit` = lp11.`id`) LEFT OUTER JOIN `price_type` AS lp12 ON (t1.`price_type` = lp12.`id`) LEFT OUTER JOIN `currencies` AS lp14 ON (t1.`currency` = lp14.`id`) LEFT OUTER JOIN `product_sources` AS lp15 ON (t1.`product_source` = lp15.`id`) LEFT OUTER JOIN `stock_ratings` AS lp16 ON (t1.`supply_ratings` = lp16.`id`) LEFT OUTER JOIN `direction_of_flows` AS lp19 ON (t1.`direction_of_flows` = lp19.`id`)) subq";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`year` like '" .$filterstr ."') or (`lp_month` like '" .$filterstr ."') or (`week` like '" .$filterstr ."') or (`data_collection_date` like '" .$filterstr ."') or (`data_gatherer` like '" .$filterstr ."') or (`data_gatherer_phone` like '" .$filterstr ."') or (`lp_state` like '" .$filterstr ."') or (`lp_county` like '" .$filterstr ."') or (`lp_market` like '" .$filterstr ."') or (`lp_commodity` like '" .$filterstr ."') or (`lp_unit` like '" .$filterstr ."') or (`lp_price_type` like '" .$filterstr ."') or (`price` like '" .$filterstr ."') or (`lp_currency` like '" .$filterstr ."') or (`lp_product_source` like '" .$filterstr ."') or (`lp_supply_ratings` like '" .$filterstr ."') or (`minimum_volume` like '" .$filterstr ."') or (`maximum_volume` like '" .$filterstr ."') or (`lp_direction_of_flows` like '" .$filterstr ."')";
  }
  if (isset($order) && $order!='') $sql .= " order by `" .sqlstr($order) ."`";
  if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
  $res = mysql_query($sql, $conn) or die(mysql_error());
  return $res;
}

function sql_getrecordcount()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT COUNT(*) FROM (SELECT t1.`id`, t1.`year`, t1.`month`, lp2.`month` AS `lp_month`, t1.`week`, t1.`data_collection_date`, t1.`data_gatherer`, t1.`data_gatherer_phone`, t1.`state`, lp7.`state` AS `lp_state`, t1.`county`, lp8.`county` AS `lp_county`, t1.`market`, lp9.`market` AS `lp_market`, t1.`commodity`, lp10.`Commodity` AS `lp_commodity`, t1.`unit`, lp11.`unit` AS `lp_unit`, t1.`price_type`, lp12.`Price_type` AS `lp_price_type`, t1.`price`, t1.`currency`, lp14.`currency` AS `lp_currency`, t1.`product_source`, lp15.`product_source` AS `lp_product_source`, t1.`supply_ratings`, lp16.`stock_rating` AS `lp_supply_ratings`, t1.`minimum_volume`, t1.`maximum_volume`, t1.`direction_of_flows`, lp19.`direction` AS `lp_direction_of_flows` FROM `market_prices` AS t1 LEFT OUTER JOIN `months` AS lp2 ON (t1.`month` = lp2.`id`) LEFT OUTER JOIN `states` AS lp7 ON (t1.`state` = lp7.`id`) LEFT OUTER JOIN `counties` AS lp8 ON (t1.`county` = lp8.`id`) LEFT OUTER JOIN `markets` AS lp9 ON (t1.`market` = lp9.`id`) LEFT OUTER JOIN `commodities` AS lp10 ON (t1.`commodity` = lp10.`id`) LEFT OUTER JOIN `units` AS lp11 ON (t1.`unit` = lp11.`id`) LEFT OUTER JOIN `price_type` AS lp12 ON (t1.`price_type` = lp12.`id`) LEFT OUTER JOIN `currencies` AS lp14 ON (t1.`currency` = lp14.`id`) LEFT OUTER JOIN `product_sources` AS lp15 ON (t1.`product_source` = lp15.`id`) LEFT OUTER JOIN `stock_ratings` AS lp16 ON (t1.`supply_ratings` = lp16.`id`) LEFT OUTER JOIN `direction_of_flows` AS lp19 ON (t1.`direction_of_flows` = lp19.`id`)) subq";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`year` like '" .$filterstr ."') or (`lp_month` like '" .$filterstr ."') or (`week` like '" .$filterstr ."') or (`data_collection_date` like '" .$filterstr ."') or (`data_gatherer` like '" .$filterstr ."') or (`data_gatherer_phone` like '" .$filterstr ."') or (`lp_state` like '" .$filterstr ."') or (`lp_county` like '" .$filterstr ."') or (`lp_market` like '" .$filterstr ."') or (`lp_commodity` like '" .$filterstr ."') or (`lp_unit` like '" .$filterstr ."') or (`lp_price_type` like '" .$filterstr ."') or (`price` like '" .$filterstr ."') or (`lp_currency` like '" .$filterstr ."') or (`lp_product_source` like '" .$filterstr ."') or (`lp_supply_ratings` like '" .$filterstr ."') or (`minimum_volume` like '" .$filterstr ."') or (`maximum_volume` like '" .$filterstr ."') or (`lp_direction_of_flows` like '" .$filterstr ."')";
  }
  $res = mysql_query($sql, $conn) or die(mysql_error());
  $row = mysql_fetch_assoc($res);
  reset($row);
  return current($row);
}

function sql_insert()
{
  global $conn;
  global $_POST;

  $sql = "insert into `market_prices` (`id`, `year`, `month`, `week`, `data_collection_date`, `data_gatherer`, `data_gatherer_phone`, `state`, `county`, `market`, `commodity`, `unit`, `price_type`, `price`, `currency`, `product_source`, `supply_ratings`, `minimum_volume`, `maximum_volume`, `direction_of_flows`) values (" .sqlvalue(@$_POST["id"], false).", " .sqlvalue(@$_POST["year"], false).", " .sqlvalue(@$_POST["month"], false).", " .sqlvalue(@$_POST["week"], false).", " .sqlvalue(@$_POST["data_collection_date"], true).", " .sqlvalue(@$_POST["data_gatherer"], true).", " .sqlvalue(@$_POST["data_gatherer_phone"], true).", " .sqlvalue(@$_POST["state"], false).", " .sqlvalue(@$_POST["county"], false).", " .sqlvalue(@$_POST["market"], false).", " .sqlvalue(@$_POST["commodity"], false).", " .sqlvalue(@$_POST["unit"], false).", " .sqlvalue(@$_POST["price_type"], false).", " .sqlvalue(@$_POST["price"], false).", " .sqlvalue(@$_POST["currency"], false).", " .sqlvalue(@$_POST["product_source"], false).", " .sqlvalue(@$_POST["supply_ratings"], false).", " .sqlvalue(@$_POST["minimum_volume"], false).", " .sqlvalue(@$_POST["maximum_volume"], false).", " .sqlvalue(@$_POST["direction_of_flows"], false).")";
  mysql_query($sql, $conn) or die(mysql_error());
}

function sql_update()
{
  global $conn;
  global $_POST;

  $sql = "update `market_prices` set `id`=" .sqlvalue(@$_POST["id"], false).", `year`=" .sqlvalue(@$_POST["year"], false).", `month`=" .sqlvalue(@$_POST["month"], false).", `week`=" .sqlvalue(@$_POST["week"], false).", `data_collection_date`=" .sqlvalue(@$_POST["data_collection_date"], true).", `data_gatherer`=" .sqlvalue(@$_POST["data_gatherer"], true).", `data_gatherer_phone`=" .sqlvalue(@$_POST["data_gatherer_phone"], true).", `state`=" .sqlvalue(@$_POST["state"], false).", `county`=" .sqlvalue(@$_POST["county"], false).", `market`=" .sqlvalue(@$_POST["market"], false).", `commodity`=" .sqlvalue(@$_POST["commodity"], false).", `unit`=" .sqlvalue(@$_POST["unit"], false).", `price_type`=" .sqlvalue(@$_POST["price_type"], false).", `price`=" .sqlvalue(@$_POST["price"], false).", `currency`=" .sqlvalue(@$_POST["currency"], false).", `product_source`=" .sqlvalue(@$_POST["product_source"], false).", `supply_ratings`=" .sqlvalue(@$_POST["supply_ratings"], false).", `minimum_volume`=" .sqlvalue(@$_POST["minimum_volume"], false).", `maximum_volume`=" .sqlvalue(@$_POST["maximum_volume"], false).", `direction_of_flows`=" .sqlvalue(@$_POST["direction_of_flows"], false) ." where " .primarykeycondition();
  mysql_query($sql, $conn) or die(mysql_error());
}

function sql_delete()
{
  global $conn;

  $sql = "delete from `market_prices` where " .primarykeycondition();
  mysql_query($sql, $conn) or die(mysql_error());
}
function primarykeycondition()
{
  global $_POST;
  $pk = "";
  $pk .= "(`id`";
  if (@$_POST["xid"] == "") {
    $pk .= " IS NULL";
  }else{
  $pk .= " = " .sqlvalue(@$_POST["xid"], false);
  };
  $pk .= ")";
  return $pk;
}
 ?>
