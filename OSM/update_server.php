<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');

include("../config.inc.php");
$dbh=fncOpenDBConn();
if(isset($_GET["commodity"]) && isset($_GET["price"])){
    $year=$_GET["year"];
    $month=$_GET["month"];
    $week=$_GET["week"];
    $data_collection_date=$_GET["data_collection_date"];
    $data_gatherer=$_GET["data_gatherer"];
    $data_gatherer_phone=$_GET["data_gatherer_phone"];
    $data_gatherer_office=$_GET["data_gatherer_office"];
    $data_gatherer_email=$_GET["data_gatherer_email"];
    $gps_lat=floatval($_GET["gps_lat"]);
    $gps_long=floatval($_GET["gps_long"]);
    $governorate=$_GET["governorate"];
    $retailerid=intval($_GET["retailerid"]);
    $retailer=$_GET["retailer"];
    $retailer_type=$_GET["retailer_type"];
    $commodity=$_GET["commodity"];
    $unit=intval($_GET["unit"]);
    $price_type=floatval($_GET["price_type"]);
    $price=floatval($_GET["price"]);
    $currency=intval($_GET["currency"]);
    $supply_ratings=intval($_GET["supply_ratings"]);
    $brand_name=$_GET["brand_name"];
    $comments=$_GET["comments"];
    $sql="INSERT INTO price_monitoring "
        ."(year,month,week,data_collection_date,data_gatherer,data_gatherer_phone,data_gatherer_office,data_gatherer_email,"
        ."gps_lat,gps_long,governorate,retailerid,retailer,retailer_type,commodity,unit,price_type,price,"
        ."currency,supply_ratings,brand_name,comments) VALUES ('$year',
                            $month,
                            $week,
                            '$data_collection_date',".
                            sqlvalue2($data_gatherer,true).",
                            $data_gatherer_phone,
                            $data_gatherer_office,
                            '$data_gatherer_email',
                            $gps_lat,
                            $gps_long,
                            $governorate,
                            $retailerid,".
                            sqlvalue2($retailer,true).",
                            $retailer_type,
                            $commodity,
                            $unit,
                            $price_type,
                            $price,
                            $currency,
                            $supply_ratings,".
                            sqlvalue2($brand_name,true).",".
                            sqlvalue2($comments,true).")";
    $res=mssql_query($sql,$dbh);

    if($res){
        fncDeliverResponse("200","The price has been succcessfully updated",$_GET["id"]);
    }
    else{
        fncDeliverResponse("400","An error occured while uploading the file",$_GET["id"]);
    }
}
else{
    fncDeliverResponse("400","Error: incomplete data","");
}