﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SummaryReport.aspx.cs" Inherits="TriangulationSummaryReport.SummaryReport" %>

<%@ Register Assembly="Telerik.ReportViewer.Html5.WebForms, Version=10.2.16.914, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.Html5.WebForms" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-1.10.2.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <telerik:ReportViewer ID="summaryReportViewer" runat="server" ParametersAreaVisible="False">
                <ReportSource Identifier="SummaryReport.trdp" IdentifierType="TypeReportSource">
                </ReportSource>
            </telerik:ReportViewer>
        </div>
    </form>
</body>
</html>
