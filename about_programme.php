<div id="my_connectivity_panel" class="pathwindow connectivity active">
    <div class="pathwindow_title">
        <div class="icon"></div>
            <a class="btn_back"><span></span><p>Back</p></a>
            <h1>About the Programme</h1>
        </div>
        <div class="pathwindow_content">
            <img src="images/4_30.jpg" width="400" style="margin-right:6px;float:left" />
            <p>As the unrest in Syria enters its sixth year, Jordan is currently hosting nearly 640,000 Syrian refugees in camps and communities; WFP provides food assistance to around 85 percent of this population. Depending on location, assistance includes food vouchers (mostly electronic), dates, and fresh bread distributed on a daily basis, in addition to school feeding in Za'atari and Azraq camps. WFP began assisting Syrian refugees in 2012 initially by providing hot meals in Za'atari camp and transit centres. With the expansion of its operations, WFP now covers refugees in both Azraq and Za'atari camps, King Abdullah Park and Cyber City transit centers, and communities in all 12 governorates. WFP assistance can be redeemed at 88 partner shops throughout the country. Since the beginning of its emergency operation in Jordan, WFP has injected over USD 460 million into the national economy.</p>
        <div class="clear">
            </div>
    </div>
</div>
