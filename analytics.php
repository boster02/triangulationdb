<?php session_start();
      include("fncCashAnalyzer.inc.php");
?>
<div id="my_connectivity_panel" class="pathwindow connectivity active">
    <div class="pathwindow_title">
        <div class="icon"></div>
        <a class="btn_back">
            <span></span>
            <p>Back</p>
        </a>
        <h1>Reports</h1>
    </div>
    <div class="pathwindow_content">
        <style type="text/css">
            #analyticsmenu ul {
                margin-left: 20px;
                list-style-image: url('images/report.png');
            }

                #analyticsmenu ul li {
                    font-size: 14px;
                    font-family: 'Trebuchet MS', Geneva, sans-serif;
                    color: #003366;
                }

                    #analyticsmenu ul li a {
                        color: #003366;
                    }

            .roundtable table {
                border-collapse: collapse;
                border-radius: 6px;
                -moz-border-radius: 6px;
            }

            .roundtable td, th {
                border-left: solid white 1px;
            }

            .roundtable th {
                background-color: blue;
                border-top: none;
            }

            .roundtable td:first-child {
                border-left: none;
            }

            .roundtable a {
                color: white;
            }

                .roundtable a:hover {
                    color: white;
                    font-weight: bold;
                }

            hr.style13 {
                height: 5px;
                border: 0;
                box-shadow: 0 5px 5px -5px #8c8b8b inset;
            }
        </style>
        <div class="roundtable" id="report_header">
            <table style="width: 100%;background-color:#ebebeb">
                <tr>
                    <td style="text-align:center;width:60px;background-color:yellow"><a style="color:black" href="javascript:toggleMenu()"><img src="images/toc.png" alt="Toc" /><br /> Table of Contents</a></td>
                    <td style="text-align:center;width:60px;background-color:red"><a href="javascript:movePrevious()" )"><img src="images/previous.png" alt="Back" /><br /> Previous <br /> Report</a></td>
                    <td style="text-align:left;padding-left:10px;padding-right:10px;color:#003366">
                        <span style="font-size:24px;font-family:'Trebuchet MS';color:#003366" id="report_title">My Title</span><hr class="style13" />
                        <span id="description" style="color:#003366">This query compares monthly record of upload per WFP, MEPS and JAB in order to detect variances</span>
                    </td>
                    <td style="padding-left:10px;width:120px;color:#003366">
                        Month:<br />
                        <select name="month" id="month" onchange="showQuery(currentid)">
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04" selected="selected">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </td>
                    <td style="padding-left:10px;width:120px;color:#003366">
                        Year:<br />
                        <select name="year" id="year" onchange="showQuery(currentid)">
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017" selected="selected">2017</option>
                        </select>
                    </td>
                    <td style="text-align:center;width:60px;background-color:darkgreen"><a href="javascript:moveNext()"><img src="images/next.png" alt="Next" /><br /> Next <br /> Report</a></td>
                </tr>
            </table>
        </div>
        <div id="analyticsresults" style="height:100%">
        </div>
        <div class="listholder ">
            <div class="sectionholder">
                <script type="text/javascript" src="//code.jquery.com/jquery-1.10.1.min.js"></script>
                <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
                <link rel="stylesheet" type="text/css" href="css/smk-accordion.css">
                <style type="text/css">
                    body {
                        margin: 0;
                        padding: 0;
                        font-family: 'Helvetica', Arial, sans-serif;
                        font-size: 12px;
                    }

                    .container_demo {
                        max-width: 900px;
                        margin: 30px auto 100px;
                    }

                    h2 {
                        margin: 25px 0 20px;
                        text-align: center;
                        color: #555;
                    }

                    table {
                        border: 1px solid black;
                        border-collapse: collapse;
                    }

                        table td {
                            border: 1px solid black;
                        }
          #analyticsmenu ul {
              margin-left: 20px;
              list-style-image: url('images/report.png');
          }
          #analyticsmenu ul li {
                  font-size: 14px;
                  font-family: 'Trebuchet MS', Geneva, sans-serif;
                  color:#003366;
              }
          #analyticsmenu ul li a {
                  color:#003366;
              }
       .roundtable table {
            border-collapse:separate;
            border-radius:6px;
            -moz-border-radius:6px;
        }

      .roundtable  td, th {
            border-left:solid white 1px;
        }

       .roundtable th {
            background-color: blue;
            border-top: none;
        }

       .roundtable td:first-child {
             border-left: none;
        }
          .roundtable a {
              color:white;
          }
          .roundtable a:hover {
              color:white;
              font-weight:bold;
          }
hr.style13 {
	height: 5px;
	border: 0;
	box-shadow: 0 5px 5px -5px #8c8b8b inset;
}
    </style>
                <div id="analyticsmenu">
                    <div class="accordion_example2">
                        <div class="accordion_in">
                            <div class="acc_head">Finance - CBT reports</div>
                            <div class="acc_content">
                                <ul>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(2)","RC","acqueries") ?>">Verification of Reload Instructions</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(3)","RC","acqueries") ?>">Verification of Reload Instructions - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(4)","RC","acqueries") ?>">Verification of Reload per Beneficiary</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(5)","RC","acqueries") ?>">Occurrence of Duplicate Authorization Numbers</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(6)","RC","acqueries") ?>">Occurrence of Duplicate Authorization Numbers - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(7)","RC","acqueries") ?>">Occurrence of Duplicate RRN</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(8)","RC","acqueries") ?>">Occurrence of Duplicate RRN - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(9)","CO","acqueries") ?>">Merchants Shopped</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(10)","CO","acqueries") ?>">Merchants Shopped - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(11)","RC","acqueries") ?>">VRL Report</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(12)","CO","acqueries") ?>">Time of Sales</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(13)","CO","acqueries") ?>">Time of Sales - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(14)","RC","acqueries") ?>">Processing vs.Transaction Date</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(15)","RC","acqueries") ?>">Processing vs. Transaction Date Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(16)","MO","acqueries") ?>">Replication of the same amounts by cashier</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(17)","MO","acqueries") ?>">Replication of the same amounts by Cashier - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(18)","RC","acqueries") ?>">Overdraft</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(19)","RC","acqueries") ?>">Overdraft - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(20)","RC","acqueries") ?>">Residual Balance</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(21)","RC","acqueries") ?>">Residual Balance - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(22)","RC","acqueries") ?>">Unused e-Cards</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(23)","RC","acqueries") ?>">Unused e-Cards - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(24)","RC","acqueries") ?>">Reconciliation</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(25)","RC","acqueries") ?>">Reconciliation - Historical & Cum</a></li>
                                </ul>
                             </div>
                        </div>
                        <div class="accordion_in">
                            <div class="acc_head">Programme - Beneficiary Reports</div>
                            <div class="acc_content">
                                <ul>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(0)","RC","acqueries") ?>">Beneficiary Statistics</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(1)","RC","acqueries") ?>">Beneficiary Statistics - Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(62)","RC","acqueries") ?>">Beneficiary Verification</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(9)","CO","acqueries") ?>">Merchants Shopped</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(10)","CO","acqueries") ?>">Merchants Shopped - Historical & Cum</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion_in">
                            <div class="acc_head">Programme - CBT Reports</div>
                            <div class="acc_content">
                                <ul>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(4)","RC","acqueries") ?>">Verification of Reload per Beneficiary</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(5)","RC","acqueries") ?>">Occurrence of Duplicate Authorization Numbers</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(6)","RC","acqueries") ?>">Occurrence of Duplicate Authorization Numbers - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(7)","RC","acqueries") ?>">Occurrence of Duplicate RRN</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(8)","RC","acqueries") ?>">Occurrence of Duplicate RRN - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(12)","CO","acqueries") ?>">Time of Sales</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(13)","CO","acqueries") ?>">Time of Sales - Historical & Cum</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion_in">
                            <div class="acc_head">Programme - Price Reports</div>
                            <div class="acc_content">
                                <ul>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(52)","MO","acqueries") ?>">Price Monitoring (Programme)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(53)","MO","acqueries") ?>">Price Monitoring - Horizontal (Programme)</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion_in">
                            <div class="acc_head">Retail - Data Reports</div>
                            <div class="acc_content">
                                <ul>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(56)","CO","acqueries") ?>">Shop Profiles</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(28)","CO","acqueries") ?>">Merchants WFP vs MEPS</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(42)","CO","acqueries") ?>">Merchant discounts</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(43)","CO","acqueries") ?>">Retailer Data Collection and Upload Report</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(44)","CO","acqueries") ?>">Merchant Sales Summary (Retailer Data)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(55)","CO","acqueries") ?>">Data Collection Summary by Retailer</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(63)","CO","acqueries") ?>">Categorisation Status</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion_in">
                            <div class="acc_head">Retail - Sales Reports</div>
                            <div class="acc_content">
                                <ul>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(56)","CO","acqueries") ?>">Shop Profiles</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(31)","MO","acqueries") ?>">Merchant Sales by category - Food</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(32)","MO","acqueries") ?>">Merchant Sales by category - Food Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(33)","CO","acqueries") ?>">Merchant Sales - NFI</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(34)","CO","acqueries") ?>">Merchant Sales - NFI - Historical & Cum</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(35)","CO","acqueries") ?>">Sales by Merchant (MEPS)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(36)","CO","acqueries") ?>">Sales by Merchant (MEPS - Retailer Groups)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(37)","CO","acqueries") ?>">Cumulative Sales by Merchant</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(38)","CO","acqueries") ?>">Find Beneficiary Purchases</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(39)","CO","acqueries") ?>">Find Beneficiary Purchases by Ben ID</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(58)","RC","acqueries") ?>">Sales Volume to Value Ratio</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(59)","RC","acqueries") ?>">Sales Volume to Value Ratio - Historical and Cumulative</a></li>
                                </ul>
                             </div>
                        </div>
                        <div class="accordion_in">
                            <div class="acc_head">Retail - Price Reports</div>
                            <div class="acc_content">
                                <ul>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(56)","CO","acqueries") ?>">Shop Profiles</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(40)","CO","acqueries") ?>">Top 100 Most Sold Items</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(41)","CO","acqueries") ?>">Top 20 Most Sold Items Per Merchant</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(42)","CO","acqueries") ?>">Merchant discounts</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(43)","CO","acqueries") ?>">Retailer Data Collection and Upload Report</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(44)","CO","acqueries") ?>">Merchant Sales Summary (Retailer Data)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(45)","CO","acqueries") ?>">Food Basket Analysis</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(46)","CO","acqueries") ?>">Camp vs. Community Sales (Retailer Data)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(47)","CO","acqueries") ?>">Price Comparison of Common Items (Individual Shops)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(48)","CO","acqueries") ?>">Price Comparison of Common Items (Retailer Groups)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(49)","CO","acqueries") ?>">Price Comparison of Common Items (WFP vs. Non-WFP)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(57)","CO","acqueries") ?>">Weighted Price Index (WFP vs. Non-WFP)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(60)","CO","acqueries") ?>">Weighted Price Index (WFP Only)</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(50)","CO","acqueries") ?>">SKU List by Shop</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(51)","CO","acqueries") ?>">SKU List by Chain</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(61)","CO","acqueries") ?>">Price Changes Within the Month</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion_in">
                            <div class="acc_head">Summary Report</div>
                            <div class="acc_content">
                                <ul>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(54)","RC","acqueries") ?>">Management Summary Report</a></li>
                                    <li><a href="<?php echo fncPermLink("javascript:showQuery(64)","AN","acqueries") ?>">Anomalies Dashboard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
    <script type="text/javascript" src="js/smk-accordion.js"></script>
    <script type="text/javascript">
		$(".accordion_example2").smk_Accordion({
			closeAble: true, //boolean
		});
    </script>
    <script type="text/javascript">
    $("#report_header").hide();
    var currentid;
function toggleMenu(){
	$(".listholder").show();
	$("#analyticsmenu").show();
	$("#analyticsresults").hide();
	$("#report_header").hide();
}
function showQuery(qid){
	$(".listholder").hide();
	$("#analyticsresults").show();
	$("#report_header").show();
	if (queryname[qid] == "find_purchases" || queryname[qid] == "find_purchases_by_customer") {
	    $("#analyticsresults").html('<iframe src="analytics/'
            + queryname[qid] + '.php" width="100%" class="resultframe" id="yourframe"></iframe>');
	}
	else {
	    $("#analyticsresults").html('<iframe class="resultframe" src="analytics/loader.php?page='
            + queryname[qid] + '.php&month='+$("#month").val()+'&year='+$("#year").val()+'" width="100%" id="yourframe"></iframe>');
	}
	$("#report_title").html(title[qid]);
	$("#description").html(description[qid]);
	currentid = qid;
}
function moveNext() {
    if (currentid < queryname.length)
        showQuery(currentid + +1);
    else
        alert("You have reached the end");
}
function movePrevious() {
    if (currentid > 0)
        showQuery(currentid - 1);
    else
        alert("You are at the beginning of queries");
}
    </script>