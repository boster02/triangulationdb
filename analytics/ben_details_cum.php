<?php
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "ID";
$year=$_GET["year"];
$month=$_GET["month"];

/* DB table to use */
$sTable =  "(SELECT summary_reload.Customer_ID, summary_wfp_reload.Highest_Household_Size,summary_wfp_reload.Lowest_Household_Size,"
        . "summary_wfp_reload.Average_Household_Size, summary_reload.Reload_Amount, "
        . "sales_summary.Sales_Amount,ROUND(summary_reload.Reload_Amount-sales_summary.Sales_Amount,2) as Variance,"
        . "summary_reload.Average_Reload,sales_summary.Highest_Amount, "
        . "sales_summary.Lowest_Amount, sales_summary.Average_Amount, sales_summary.No_of_Merchants,"
        . "summary_wfp_reload.Governorates, summary_reload.Customer_ID as ID "
        . "FROM (SELECT     Customer_ID, round(AVG(Trans_Amount),2) AS Average_Reload, SUM(Trans_Amount) AS Reload_Amount "
        . "FROM dbo.prepaid_reload_meps WHERE LEFT(Trans_Date,6)<=".$year . $month
        . " GROUP BY Customer_ID) as summary_reload INNER JOIN "
        . "(SELECT Customer_ID, COUNT(*) AS No_of_Sales, ROUND(Sum(Trans_Amount),2) as Sales_Amount, ROUND(MAX(Trans_Amount),2) "
        . "AS Highest_Amount,"
        . "ROUND(MIN(Trans_Amount),2) AS Lowest_Amount, ROUND(AVG(Trans_Amount),2) AS Average_Amount, COUNT(DISTINCT Merchant) "
        . "AS No_of_Merchants "
        . "FROM sales_draft WHERE LEFT(Trans_Date,6)<=" . $year . $month
        . " GROUP BY Customer_ID) as  sales_summary ON summary_reload.Customer_ID = sales_summary.Customer_ID INNER JOIN "
        . "(SELECT     Beneficiary_ID, MAX(Household_Size) as Highest_Household_Size,MIN(Household_Size) as Lowest_Household_Size,AVG(Household_Size) as Average_Household_Size, COUNT(DISTINCT Location) AS Governorates "
        . "FROM prepaid_reload_wfp WHERE LEFT(Trans_Date,6)<=" . $year . $month
        . " GROUP BY Beneficiary_ID) as summary_wfp_reload "
        . "ON summary_reload.Customer_ID = summary_wfp_reload.Beneficiary_ID)";

/* Database connection information */
$gaSql['user']       = "sa";
$gaSql['password']   = "P@ssword1";
$gaSql['db']         = "jordandb";
$gaSql['server']     = "10.67.67.130";

/*
 * Columns
 * If you don't want all of the columns displayed you need to hardcode $aColumns array with your elements.
 * If not this will grab all the columns associated with $sTable
 */
$fields=explode(",",$_GET["fields"]);
$aColumns = $fields;

$connectionInfo = array("UID" => $gaSql['user'], "PWD" => $gaSql['password'], "Database"=>$gaSql['db'],"ReturnDatesAsStrings"=>true,"CharacterSet" => "UTF-8");
$gaSql['link'] = sqlsrv_connect( $gaSql['server'], $connectionInfo);
$params = array();
$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );


/* Ordering */
$sOrder = "";
if ( isset( $_GET['iSortCol_0'] ) ) {
    $sOrder = "ORDER BY  ";
    for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ) {
        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ) {
            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                    ".addslashes( $_GET['sSortDir_'.$i] ) .", ";
        }
    }
    $sOrder = substr_replace( $sOrder, "", -2 );
    if ( $sOrder == "ORDER BY" ) {
        $sOrder = "";
    }
}

/* Filtering */
$sWhere = "";
if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
    $sWhere = "WHERE (";
    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
        $sWhere .= $aColumns[$i]." LIKE '%".addslashes( $_GET['sSearch'] )."%' OR ";
    }
    $sWhere = substr_replace( $sWhere, "", -3 );
    $sWhere .= ')';
}
/* Individual column filtering */
for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
    if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )  {
        if ( $sWhere == "" ) {
            $sWhere = "WHERE ";
        } else {
            $sWhere .= " AND ";
        }
        $sWhere .= $aColumns[$i]." LIKE '%".addslashes($_GET['sSearch_'.$i])."%' ";
    }
}

/* Paging */
$top = (isset($_GET['iDisplayStart']))?((int)$_GET['iDisplayStart']):0 ;
$limit = (isset($_GET['iDisplayLength']))?((int)$_GET['iDisplayLength'] ):10;
$sQuery = "SELECT TOP $limit ".implode(",",$aColumns)."
        FROM $sTable as t1
        $sWhere ".(($sWhere=="")?" WHERE ":" AND ")." $sIndexColumn NOT IN
        (
            SELECT $sIndexColumn FROM
            (
                SELECT TOP $top ".implode(",",$aColumns)."
                FROM $sTable as t2
                $sWhere
                $sOrder
            )
            as [virtTable]
        )
        $sOrder";

$rResult = sqlsrv_query($gaSql['link'],$sQuery) or die("$sQuery: " . sqlsrv_errors());

$sQueryCnt = "SELECT * FROM $sTable as t3 $sWhere";
$rResultCnt = sqlsrv_query( $gaSql['link'], $sQueryCnt ,$params, $options) or die (" $sQueryCnt: " . sqlsrv_errors());
$iFilteredTotal = sqlsrv_num_rows( $rResultCnt );

$sQuery = " SELECT * FROM $sTable as t4 ";
$rResultTotal = sqlsrv_query( $gaSql['link'], $sQuery ,$params, $options) or die(sqlsrv_errors());
$iTotal = sqlsrv_num_rows( $rResultTotal );

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ( $aRow = sqlsrv_fetch_array( $rResult ) ) {
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
        if ( $aColumns[$i] != ' ' ) {
            $v = $aRow[ $aColumns[$i] ];
            $v = mb_check_encoding($v, 'UTF-8') ? $v : utf8_encode($v);
            $row[]=$v;
        }
    }
    If (!empty($row)) { $output['aaData'][] = $row; }
}
echo json_encode( $output );
?>