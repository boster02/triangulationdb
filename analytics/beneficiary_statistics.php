<?php session_start();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Beneficiary Statistics</title>
    <link rel="stylesheet" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <a style="float:right" class="green-btn" href="javascript:exportDetails()">Export to Excel</a><br />
    <div id="detailscontainer" class="csstable-details">
        <table id="detailstable" class="display compact">
            <thead>
                <tr>
                    <th>Customer ID</th>
                    <th>Household Size</th>
                    <th>Reload</th>
                    <th>Sales</th>
                    <th>Variance</th>
                    <th>No. of Reloads</th>
                    <th>No. of Sale Transactions</th>
                    <th>Highest Sales Value</th>
                    <th>Lowest Sales Value</th>
                    <th>Average Sales Value</th>
                    <th>No. of Merchants Shopped</th>
                    <th>No. of Governorates</th>
                </tr>
            </thead>
        </table>
    </div>

    <script type="text/javascript">
        var year = <?php echo $_POST["year"] ?>;
        var month = <?php echo $_POST["month"] ?>;
        if (month < 10) month = "0" + month;
        fields = "Customer_ID,Household_Size,Reload_Amount,Sales_Amount,Variance,No_of_Reloads,No_of_Sales,Highest_Amount,Lowest_Amount,Average_Amount,No_of_Merchants,Governorates,ID";
        $("#detailscontainer").show();
        $("#detailstable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "ben_details.php?year=" + year + "&month=" + month + "&fields=" + fields,
            "sPaginationType": "full_numbers"
        });


        function exportDetails() {
            var year = <?php echo $_POST["year"] ?>;
            var month = <?php echo $_POST["month"] ?>;
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            $.ajax({
                url: "export_ben_details.php?year=" + year + "&month="+month,
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
