<?php session_start();
      include("fncAnalytics.inc.php");
      $uploaddata=fncGetBeneficiaryUploadAuthentication($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Beneficiary Statistics</title>
    <link rel="stylesheet" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table style="width: 500px">
                        <tr><td>Description</td><td>Number</td></tr>
                        <tr>
                            <td>
                                Number of Cases in Programme Reload
                            </td>
                            <td>
                                <a href="javascript:showDetails(<?php echo "1,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <?php echo number_format($uploaddata["programme_reload"]) ?>
                                </a>
                                <a style="text-decoration:none" href="javascript:exportDetails(<?php echo "1,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Number of Cases in MEPS Reload
                            </td>
                            <td>
                                <a href="javascript:showDetails(<?php echo "2,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <?php echo number_format($uploaddata["meps_reload"]) ?>
                                </a>
                                <a style="text-decoration:none" href="javascript:exportDetails(<?php echo "2,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cases in Programme and Not in MEPS Reload
                            </td>
                            <td>
                                <a href="javascript:showDetails(<?php echo "3,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <?php echo number_format($uploaddata["programme_not_meps"]) ?>
                                </a>
                                <a style="text-decoration:none" href="javascript:exportDetails(<?php echo "3,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cases in MEPS Reload Not in Programme Reload
                            </td>
                            <td>
                                <a href="javascript:showDetails(<?php echo "4,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <?php echo number_format($uploaddata["meps_not_programme"]) ?>
                                </a>
                                <a style="text-decoration:none" href="javascript:exportDetails(<?php echo "4,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cases with Different Reload Amounts
                            </td>
                            <td>
                                <a href="javascript:showDetails(<?php echo "5,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <?php echo number_format($uploaddata["different_reloads"]) ?>
                                </a>
                                <a style="text-decoration:none" href="javascript:exportDetails(<?php echo "5,".$_POST["year"].",".$_POST["month"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>Case ID</th>
                                <th>Programme Reload</th>
                                <th>MEPS Reload</th>
                                <th>Variance</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(tableid, year, month) {
            debugger;
            if (month < 10) month = "0" + month;
            var tablename = "("+getTableName(tableid,year,month)+")";
            fields = "ID,Programme_Reload,MEPS_Reload,Variance";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }

        function exportDetails(tableid, year, month) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = getTableName(tableid,year,month);
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=beneficiary_upload_authentication",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
        function getTableName(tableid,year,month) {
            var tablename = "";
            if (tableid == 1) {
                tablename = "SELECT pre_paid_reload_wfp_" + year + month + ".Beneficiary_ID as ID,pre_paid_reload_wfp_" + year + month + ".Trans_Amount"
                + " as Programme_Reload,pre_paid_reload_meps_" + year + month + ".Trans_Amount as MEPS_Reload, "
                + " pre_paid_reload_wfp_" + year + month + ".Trans_Amount-pre_paid_reload_meps_" + year + month + ".Trans_Amount AS Variance "
                + " FROM     pre_paid_reload_wfp_" + year + month + " Left JOIN "
                + " pre_paid_reload_meps_" + year + month + " ON pre_paid_reload_wfp_" + year + month + ".Beneficiary_ID = pre_paid_reload_meps_" + year + month + ".Customer_ID";
            }
            else if (tableid == 2) {
                tablename = "SELECT pre_paid_reload_meps_" + year + month + ".Customer_ID as ID, pre_paid_reload_wfp_" + year + month + ".Trans_Amount "
                + " AS Programme_Reload,"
                + " pre_paid_reload_meps_" + year + month + ".Trans_Amount AS MEPS_Reload,"
                + " pre_paid_reload_wfp_" + year + month + ".Trans_Amount - pre_paid_reload_meps_" + year + month + ".Trans_Amount AS Variance"
                + " FROM     pre_paid_reload_wfp_" + year + month + " RIGHT OUTER JOIN"
                + " pre_paid_reload_meps_" + year + month + " ON pre_paid_reload_wfp_" + year + month + ".Beneficiary_ID = pre_paid_reload_meps_" + year + month + ".Customer_ID";
            }
            else if (tableid == 3) {
                tablename = "SELECT pre_paid_reload_wfp_" + year + month + ".Beneficiary_ID as ID,pre_paid_reload_wfp_" + year + month + ".Trans_Amount"
                + " as Programme_Reload,pre_paid_reload_meps_" + year + month + ".Trans_Amount as MEPS_Reload, "
                + " pre_paid_reload_wfp_" + year + month + ".Trans_Amount-pre_paid_reload_meps_" + year + month + ".Trans_Amount AS Variance "
                + " FROM     pre_paid_reload_wfp_" + year + month + " Left JOIN "
                + " pre_paid_reload_meps_" + year + month + " ON pre_paid_reload_wfp_" + year + month + ".Beneficiary_ID = pre_paid_reload_meps_" + year + month + ".Customer_ID "
                + " WHERE  (pre_paid_reload_meps_" + year + month + ".Customer_ID IS NULL)";
            }
            else if (tableid == 4) {
                tablename = "SELECT pre_paid_reload_meps_" + year + month + ".Customer_ID as ID, pre_paid_reload_wfp_" + year + month + ".Trans_Amount "
                + " AS Programme_Reload,"
                + " pre_paid_reload_meps_" + year + month + ".Trans_Amount AS MEPS_Reload,"
                + " pre_paid_reload_wfp_" + year + month + ".Trans_Amount - pre_paid_reload_meps_" + year + month + ".Trans_Amount AS Variance"
                + " FROM     pre_paid_reload_wfp_" + year + month + " RIGHT OUTER JOIN"
                + " pre_paid_reload_meps_" + year + month + " ON pre_paid_reload_wfp_" + year + month + ".Beneficiary_ID = pre_paid_reload_meps_" + year + month + ".Customer_ID "
                + " WHERE  (pre_paid_reload_wfp_" + year + month + ".Beneficiary_ID IS NULL)";
            }
            else if (tableid == 5) {
                tablename = "SELECT pre_paid_reload_meps_" + year + month + ".Customer_ID as ID, pre_paid_reload_wfp_" + year + month + ".Trans_Amount "
                + " AS Programme_Reload,"
                + " pre_paid_reload_meps_" + year + month + ".Trans_Amount AS MEPS_Reload,"
                + " pre_paid_reload_wfp_" + year + month + ".Trans_Amount - pre_paid_reload_meps_" + year + month + ".Trans_Amount AS Variance"
                + " FROM     pre_paid_reload_wfp_" + year + month + " INNER JOIN"
                + " pre_paid_reload_meps_" + year + month + " ON pre_paid_reload_wfp_" + year + month + ".Beneficiary_ID = pre_paid_reload_meps_" + year + month + ".Customer_ID "
                + " WHERE  (pre_paid_reload_wfp_" + year + month + ".Trans_Amount-pre_paid_reload_meps_" + year + month + ".Trans_Amount<>0)";
            }
            return tablename;
        }
    </script>

</body>
</html>
