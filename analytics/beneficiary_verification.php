<?php session_start();
      include("fncAnalytics.inc.php");
      $bendetails=array();
      if(isset($_POST["ben_id"])){
          $bendetails=fncGetBeneficiaryVerification($_POST["ben_id"],$_POST["year"].$_POST["month"]);
      }
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Beneficiary Verification</title>
    <link rel="stylesheet" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="css/nice-form.css" />
</head>

<body>
<form id="form1" name="form1" method="get" onsubmit="return validateForm()">
    <label for="ben_id">
        <span>
            Please Enter Beneficiary UNHCR ID:
            <span class="required">*</span>
        </span>
        <input name="ben_id" id="ben_id" type="text" class="input-field" placeholder="UNHCR ID" />
        <input type="hidden" name ="page" value="beneficiary_verification.php" />
        <input type="hidden" name="month" value="<?php $_POST["month"] ?>" />
        <input type="hidden" name="year" value="<?php $_POST["year"] ?>" />
    </label>
    <label>
        <span>&nbsp;</span>
        <input type="submit" name="submit" value="Search" id="submit" />
    </label>
</form>
<div class="csstable">
    <?php if(count($bendetails)==0){
              echo "<p style='color:red'><em>No information is available for the selected beneficiary for the selected month</em></p>";
          }
    else{?>
    <table>
            <tr>
                <td>Description</td>
                <td>Value</td>
            </tr>
            <tr>
                <td>UNHCR ID:</td>
                <td><?php echo $_GET["ben_id"] ?></td>
            </tr>
            <tr>
                <td>Family size:</td>
                <td class="number"><?php echo $bendetails["family_size"] ?></td>
            </tr>
            <tr>
                <td>Card Numbers:</td>
                <td class="number"><?php echo $bendetails["card_numbers"] ?></td>
            </tr>
            <tr>
                <td>Amount on the last transaction:</td>
                <td class="number"><?php echo $bendetails["last_amount"] ?></td>
            </tr>
            <tr>
                <td>Remaining Balance on the card:</td>
                <td class="number"><?php echo $bendetails["remaining_balance"] ?></td>
            </tr>
            <tr>
                <td>Shops visited:</td>
                <td><?php echo $bendetails["shops_visited"] ?></td>
            </tr>
    </table>
    <?php } ?>
</div>
<script type="text/javascript">
    function validateForm(){
        if (document.getElementById("ben_id").value == '' || document.getElementById("ben_id").value == null) {
            alert("Please enter beneficiary ID");
            return false;
        }
        else {
            $("#loadingmessage").show();
            return true;
        }
    }
</script>
</body>
</html>
