<?php session_start();
      include("fncAnalytics.inc.php");
      $sales=fncGetCampVsCommunity($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Camp vs. Community</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <style>
        .group-title{
            background-color:#ebebeb;
            font-weight:bold;
            vertical-align:top;
        }
    </style>
</head>

<body>
    <div class="csstable">
        <table>
            <tr>
                <td class="number">
                    <strong>Category</strong>
                </td>
                <td class="number">
                    <strong>Type of Shops</strong>
                </td>
                <td class="number">
                    <strong>Products</strong>
                </td>
                <td class="number">
                    <strong>Number of beneficaries</strong>
                </td>
                <td class="number">
                    <strong>Amount Spent (JOD)</strong>
                </td>
                <td class="number">
                    <strong>Cost of Goods (JOD)</strong>
                </td>
                <td class="number">
                    <strong>COG %</strong>
                </td>
                <td class="number">
                    <strong>Number of Shops</strong>
                </td>
                <td class="number">
                    <strong>Locations</strong>
                </td>
            </tr>
            <tr>
                <td class="number group-title" rowspan="3">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",6"?>)">
                        All camp stores
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",6" ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    Safeway Camp
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["safeway_camp"]["desc_id"] ?>)">
                        <?php echo $sales["safeway_camp"]["products"] ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["safeway_camp"]["desc_id"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_camp"]["bens"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_camp"]["amount_spent"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_camp"]["cog"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_camp"]["cog_percent"],1) ?>%
                </td>
                <td class="number">
                    <?php echo $sales["safeway_camp"]["no_of_shops"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showLocations(1)">
                        <?php echo $sales["safeway_camp"]["locations"] ?>
                    </a>
                    <div id="loc1" style="display:none">
                        <?php echo $sales["safeway_camp"]["location_names"] ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="number">
                    Tazweed Camp
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["tazweed_camp"]["desc_id"] ?>)">
                        <?php echo $sales["tazweed_camp"]["products"] ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["tazweed_camp"]["desc_id"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <?php echo number_format($sales["tazweed_camp"]["bens"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["tazweed_camp"]["amount_spent"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["tazweed_camp"]["cog"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["tazweed_camp"]["cog_percent"],1) ?>%
                </td>
                <td class="number">
                    <?php echo $sales["tazweed_camp"]["no_of_shops"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showLocations(2)">
                        <?php echo $sales["tazweed_camp"]["locations"] ?>
                    </a>
                    <div id="loc2" style="display:none">
                        <?php echo $sales["tazweed_camp"]["location_names"] ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="number">
                    Sameh Mall Camp
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["sameh_mall_camp"]["desc_id"] ?>)">
                        <?php echo $sales["sameh_mall_camp"]["products"] ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["sameh_mall_camp"]["desc_id"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <?php echo number_format($sales["sameh_mall_camp"]["bens"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["sameh_mall_camp"]["amount_spent"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["sameh_mall_camp"]["cog"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["sameh_mall_camp"]["cog_percent"],1) ?>%
                </td>
                <td class="number">
                    <?php echo $sales["sameh_mall_camp"]["no_of_shops"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showLocations(2)">
                        <?php echo $sales["sameh_mall_camp"]["locations"] ?>
                    </a>
                    <div id="loc2" style="display:none">
                        <?php echo $sales["sameh_mall_camp"]["location_names"] ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="number group-title" rowspan="2">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",7" ?>)">
                        Safeway (all stores community and camp)
                        </a>
                        <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",7" ?>)">
                            <img src="../images/exportxls.png" alt="Export" />
                        </a>
</td>
                <td class="number">
                 Safeway Camp
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["safeway_camp"]["desc_id"] ?>)">
                        <?php echo $sales["safeway_camp"]["products"] ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["safeway_camp"]["desc_id"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_camp"]["bens"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_camp"]["amount_spent"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_camp"]["cog"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_camp"]["cog_percent"],1) ?>%
                </td>
                <td class="number">
                    <?php echo $sales["safeway_camp"]["no_of_shops"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showLocations(3)">
                        <?php echo $sales["safeway_camp"]["locations"] ?>
                    </a>
                    <div id="loc3" style="display:none">
                        <?php echo $sales["safeway_camp"]["location_names"] ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="number">
                   Safeway Community
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["safeway_community"]["desc_id"] ?>)">
                        <?php echo $sales["safeway_community"]["products"] ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales["safeway_community"]["desc_id"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_community"]["bens"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_community"]["amount_spent"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_community"]["cog"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales["safeway_community"]["cog_percent"],1) ?>%
                </td>
                <td class="number">
                    <?php echo $sales["safeway_community"]["no_of_shops"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showLocations(4)">
                        <?php echo $sales["safeway_community"]["locations"] ?>
                    </a>
                    <div id="loc4" style="display:none">
                        <?php echo $sales["safeway_community"]["location_names"] ?>
                    </div>
                </td>
            </tr>
            <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
            <tr>
                <?php if($i==1){ ?>
                <td class="number group-title" rowspan="<?php echo $sales[0][0] ?>">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",5" ?>)">
                        All community stores
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",5" ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <?php } ?>
                <td class="number">
                    <?php echo $sales[$i]["shop_name"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales[$i]["desc_id"]
                                                          .",'".$sales[$i]["Retailer_ID"]."'"?>)">
                        <?php echo $sales[$i]["products"] ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales[$i]["desc_id"] 
                                                            .",'".$sales[$i]["Retailer_ID"]."'"?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <?php echo number_format($sales[$i]["bens"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales[$i]["amount_spent"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales[$i]["cog"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales[$i]["cog_percent"],1) ?>%
                </td>
                <td class="number">
                    <?php echo $sales[$i]["no_of_shops"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showLocations('<?php echo $sales[$i]["Retailer_ID"] ?>')">
                        <?php echo $sales[$i]["locations"] ?>
                    </a>
                    <div id="loc<?php echo $sales[$i]["Retailer_ID"] ?>" style="display:none">
                        <?php echo $sales[$i]["location_names"] ?>
                    </div>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div id="detailscontainer" class="details csstable-details">
        <table id="detailstable" class="display compact">
            <thead>
                <tr>
                    <th>Retailer_ID</th>
                    <th>Shop_Name</th>
                    <th>Category</th>
                    <th>Barcode</th>
                    <th>Material_Code</th>
                    <th>Item_Description</th>
                    <th>Weight</th>
                    <th>Volume</th>
                    <th>Amount_Spent</th>
                    <th>Avg_Price</th>
                    <th>COG</th>
                </tr>
            </thead>
        </table>
    </div>
    <script type="text/javascript">
        function showLocations(id) {
            $("#loc" + id).show();
        }
        function showDetails(year, month, desc_id, retailer_id) {
            alert("Your results will appear at the bottom of the page");
            if (month < 10) month = "0" + month;
            var criteria = ["in (63)", "in (53,54,55,56,57,58,59,61,62,64,99,100,101,102)",
            "in (86)", "in (68,69)", "not in (3,63,68,69,86,219)", "in (63,86)", "in (53,54,55,56,57,58,59,61,62,63,64,99,100,101,102)"];
            var criteria_str = "";
            if (retailer_id != null) {
                criteria_str = "where Retailer_ID='" + retailer_id + "'";
            }
            else {
                criteria_str = "Where merchant " + criteria[desc_id - 1];
            }
            tablename = "(SELECT Retailer_ID, concat(wfp_name,' - ',branch, ' - ',[address]) as Shop_Name,Category,Barcode, Material_Code, MIN(Item_Description) AS Item_Description, SUM(Quantity) AS Weight, COUNT(*) "
                + "AS Volume, SUM(Total_Amount) AS Amount_Spent, AVG(Unit_Price) AS Avg_Price, "
                + "SUM(total_cost) AS COG,ROW_NUMBER() OVER (ORDER BY Item_Description) as ID  "
                + "FROM  merchant_sales_" + year + month + " LEFT OUTER JOIN "
                + "Open_Book_Items ON Material_Code = SKU_Code "
                + "INNER JOIN merchants_wfp ON merchant=merchants_wfp.id "
                + criteria_str
                + " GROUP BY Category,Barcode, Material_Code,Item_Description,Retailer_ID, concat(wfp_name,' - ',branch, ' - ',[address]))";
            fields = "Retailer_ID,Shop_Name,Category,Barcode,Material_Code,Item_Description,Weight,Volume,Amount_Spent,Avg_Price,COG,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month, desc_id, retailer_id) {
            $('#loadingmessage').show();
            if (month < 10) month = "0" + month;
            var criteria = ["in (63)", "in (53,54,55,56,57,58,59,61,62,64,99,100,101,102)",
                "in (86)", "in (68,69)", "not in (3,63,68,69,86)", "in (63,86)", "in (53,54,55,56,57,58,59,61,62,63,64,99,100,101,102)"];
            var criteria_str = "";
            if (retailer_id != null) {
                criteria_str = "where Retailer_ID='" + retailer_id + "'";
            }
            else {
                criteria_str = "Where merchant " + criteria[desc_id - 1];
            }
            var sql = "SELECT Retailer_ID, concat(wfp_name,' - ',branch, ' - ',[address]) as Shop_Name,Category,Barcode, Material_Code, min(Item_Description) AS Item_Description, SUM(Quantity) AS Weight, COUNT(*) "
                + "AS Volume, SUM(Total_Amount) AS Amount_Spent, AVG(Unit_Price) AS Avg_Price, "
                + "SUM(total_cost) AS COG,ROW_NUMBER() OVER (ORDER BY Item_Description) as ID  "
                + "FROM  merchant_sales_" + year + month + " LEFT OUTER JOIN "
                + "Open_Book_Items ON Material_Code = SKU_Code "
                + "INNER JOIN merchants_wfp ON merchant=merchants_wfp.id "
                + criteria_str
                + " GROUP BY Category, Barcode,Material_Code,Item_Description,Retailer_ID, concat(wfp_name,' - ',branch, ' - ',[address])";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=camp_vs_community",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
