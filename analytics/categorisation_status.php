<?php session_start();
      include("fncAnalytics.inc.php");
      $data=fncGetCategorisationStatus();
      $totals=fncGetCategorisationTotals();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Categorisation</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>

    <div class="csstable">
        <a class="green-btn" href="javascript:exportSummary()">Export this Report to Excel</a>
        <br />
        <img style="height:1px" />
        <div id="report">
            <table>
                <tr class="toprow">
                    <td>Retailer ID</td>
                    <td>Branch ID</td>
                    <td>Shop (WFP Name)</td>
                    <td>Sales Value</td>
                    <td>All Barcodes</td>
                    <td>Categorised Barcodes</td>
                    <td>Remaining Barcodes</td>
                    <td>Percent Categorised</td>
                </tr>
                <?php for($i=1;$i<=$data[0][0];$i++){ ?>
                <tr>
                    <td>
                        <?php echo $data[$i]["retailer_id"] ?>
                    </td>
                    <td>
                        <?php echo $data[$i]["merchant"] ?>
                    </td>
                    <td>
                        <?php echo $data[$i]["shop_name"] ?>
                    </td>
                    <td>
                        <?php echo number_format($data[$i]["sales_value"]) ?>
                    </td>
                    <td class="number">
                        <a href="javascript:showDetails(<?php echo $data[$i]["merchant"].",1"?>)">
                            <?php echo number_format($data[$i]["all_barcodes"]) ?>
                        </a>
                        <a href="javascript:exportDetails(<?php echo $data[$i]["merchant"].",1"?>)">
                            <img src="../images/exportxls.png" alt="Export" />
                        </a>
                    </td>
                    <td class="number">
                        <a href="javascript:showDetails(<?php echo $data[$i]["merchant"].",2"?>)">
                            <?php echo number_format($data[$i]["categorised_barcodes"]) ?>
                        </a>
                        <a href="javascript:exportDetails(<?php echo $data[$i]["merchant"].",2"?>)">
                            <img src="../images/exportxls.png" alt="Export" />
                        </a>
                    </td>
                    <td class="number">
                        <a href="javascript:showDetails(<?php echo $data[$i]["merchant"].",3"?>)">
                            <?php echo number_format($data[$i]["remainder"]) ?>
                        </a>
                        <a href="javascript:exportDetails(<?php echo $data[$i]["merchant"].",3"?>)">
                            <img src="../images/exportxls.png" alt="Export" />
                        </a>
                    </td>
                    <td class="number">
                        <?php echo number_format($data[$i]["categorisation_percent"]) ?>%
                    </td>
                </tr>
                <?php } ?>
                <tr style="background-color:silver">
                    <td>
                        <?php echo $totals["total"] ?>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td class="number">
                        <?php echo number_format($totals["total_sales"]) ?>
                    </td>
                    <td class="number">
                        <?php echo number_format($totals["total_barcodes"]) ?>
                    </td>
                    <td class="number">
                        <?php echo number_format($totals["total_categorised"]) ?>
                    </td>
                    <td class="number">
                        <?php echo number_format($totals["total_barcodes"]-$totals["total_categorised"]) ?>
                    </td>
                    <td class="number">
                        <?php echo number_format($totals["total_barcodes"]>0?$totals["total_categorised"]/$totals["total_barcodes"]*100:"0") ?>%
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="dialog" title="Sale details">
        <div id="detailscontainer" class="csstable-details">
            <table id="detailstable" class="display compact">
                <thead>
                    <tr>
                        <th>Barcodes</th>
                        <th>Item Description</th>
                        <th>English Description</th>
                        <th>Brand</th>
                        <th>Family</th>
                        <th>Sub Category</th>
                        <th>Category</th>
                        <th>Food Group</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.js"></script>

    <script type="text/javascript">
        $(function () {
            $("#dialog").dialog({
                autoOpen: false,
                maxWidth: 800,
                maxHeight: 500,
                width: 800,
                height: 500,
                modal: true
            });
        });
        function showDetails(merchant,type) {
            debugger;
            $("#dialog").dialog("open");
            var criteria = "";
            if (type == 1) {
                criteria = "";
            }
            else if (type == 2) {
                criteria = " and a03family_id is not null and a03brand_id is not null "
            }
            else if (type == 3) {
                criteria = " and (FAMILY is null or a03brand_id is null or sku_code is null) "
            }
            var fields = "barcode,item_description,item_description_english,brand,family,sub_category,category,food_group,ID";
            $("#detailscontainer").show();
            var url = "categorisation_details.php?criteria=" + criteria + "&fields=" + fields + "&merchant="+merchant;
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": url,
                "sPaginationType": "full_numbers"
            });
        }

        function exportDetails(merchant,type) {
            $('#loadingmessage').show();
            var criteria = "";
            if (type == 1) {
                criteria = "";
            }
            else if (type == 2) {
                criteria = " and a03family_id is not null and a03brand_id is not null "
            }
            else if (type == 3) {
                criteria = " and (FAMILY is null or a03brand_id is null or sku_code is null) "
            }
            $.ajax({
                url: "export_categorisation.php?criteria="+ criteria + "&merchant=" + merchant,
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
        function exportSummary() {
            $('#loadingmessage').show();
            var content = $("#report").html();
            debugger;
            $.ajax({
                url: "export_html.php",
                type: "POST",
                dataType: 'json',
                data: { tablename: 'categorisation_status_summary', html: content },
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>