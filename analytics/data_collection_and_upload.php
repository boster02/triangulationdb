<?php session_start();
      include("fncAnalytics.inc.php");
      $retailers=fncGetAllMerchants();
      $startmonth=$_POST["year"]."01";
      $endmonth=$_POST["year"].$_POST["month"];
      $uploads=fncGetUploadStatusCum($startmonth,$endmonth);
      $infolder=fncGetFilesInFolder($startmonth,$endmonth);
      $sales=fncGetLatestSales($endmonth);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Data Collection and Upload Report</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <style>
        body{
            font-family:Calibri, 'Trebuchet MS', sans-serif;
        }
        .uploaded{
            font-family:Wingdings;
            color:green;
            font-weight:bold;
            font-size:150%;
        }
        .in-folder {
            font-family: Wingdings;
            color: darkgoldenrod;
            font-weight: bold;
            font-size: 150%;
        }
        .not-uploaded {
            font-family: Wingdings;
            color: red;
            font-weight: bold;
            font-size: 150%;
        }
    </style>
</head>

<body>
    <div class="csstable">
        <a class="green-btn" href="export_data_collection.php?year=<?php echo $_POST["year"]?>&month=<?php echo $_POST["month"] ?>">Export this Report to Excel</a>
    <img style="height:1px" />

    <h3>Retailer Data Upload Status</h3>
    <strong>KEY:</strong>
    <span class="uploaded">ü</span> Uploaded
    <span class="in-folder">1</span> Data sent but not uploaded
    <span class="not-uploaded">û</span> Data not sent and not uploaded
    <table id="tabela">
        <tr>
            <td>Retailer ID</td>
            <td>Branch ID</td>
            <td>
                <strong>Retailer Name</strong>
            </td>
            <td>
                <strong><?php echo fncGetMonthName($endmonth)." Sales"?></strong>
            </td>
            <?php for($month=$startmonth;$month<=$endmonth;$month++){ ?>
            <td>
                <?php echo fncGetMonthName($month) ?>
            </td>
            <?php } ?>
        </tr>
        <?php for($i=1;$i<=$retailers[0][0];$i++){ ?>
        <tr>
            <td>
                <?php echo $retailers[$i]["retailer_id"] ?>
            </td>
            <td><?php echo $retailers[$i]["id"] ?></td>
            <td>
                <?php echo $retailers[$i]["fullname"] ?>
            </td>
            <td class="number">
                <?php echo number_format(isset($sales[$retailers[$i]["id"]])?$sales[$retailers[$i]["id"]]:0,2)?>
            </td>
            <?php for($month=$startmonth;$month<=$endmonth;$month++){ ?>
            <td style="text-align:center">
                <?php
                     if(isset($uploads[$month][$retailers[$i]["id"]])){
                                                  $class="uploaded";
                                                  $icon="ü";
                                              }
                     elseif(isset($infolder[$month][$retailers[$i]["id"]])){
                              $class="in-folder";
                              $icon="1";
                          }
                          else{
                              $class="not-uploaded";
                              $icon="û";
                          }
                ?>
                <span class="<?php echo $class ?>"><?php echo $icon ?></span>
            </td>
            <?php } ?>
        </tr>
        <?php } ?>
    </table>
</div>
</body>
</html>
