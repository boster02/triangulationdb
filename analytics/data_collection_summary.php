<?php session_start();
      header('Content-Type: text/html; charset=utf-8');
      include("fncAnalytics.inc.php");
      $sales=fncGetDataCollectionSummary($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Merchant Sales Summary</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
</head>

<body>
    <h1>
        Summary for month <?php echo fncGetMonthName($_POST["year"].$_POST["month"]) ?>
    </h1>
    <h3>
        <a style="margin-top:10px" class="green-btn" href="javascript:exportSummary(<?php echo $_POST["year"].",".$_POST["month"] ?>)">Export this Report to Excel</a>
    </h3>
    <div class="csstable">
        <table>
            <tr>
                <td>
                    <strong>Retailer ID</strong>
                </td>
                <td class="number">
                    <strong>Retailer Name</strong>
                </td>
                <td class="number">
                    <strong>All Branches</strong>
                </td>
                <td class="number">
                    <strong>Branches with Sales</strong>
                </td>
                <td class="number">
                    <strong>Branches Providing Sales Data</strong>
                </td>
                <td class="number">
                    <strong>Branches Not Providing Data</strong>
                </td>
            </tr>
            <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $sales[$i]["retailer_id"] ?>
                </td>
                <td>
                    <?php echo $sales[$i]["retailer_name"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",'All_Branches'"
                        .",'".$sales[$i]["retailer_id"]."'"?>)">
                        <?php echo intval($sales[$i]["All_Branches"]) ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",'All_Branches'"
                        .",'".$sales[$i]["retailer_id"]."'"?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",'branches_with_sales'"
                        .",'".$sales[$i]["retailer_id"]."'"?>)">
                        <?php echo intval($sales[$i]["branches_with_sales"]) ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",'branches_with_sales'"
                        .",'".$sales[$i]["retailer_id"]."'"?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",'Submitting'"
                        .",'".$sales[$i]["retailer_id"]."'"?>)">
                        <?php echo intval($sales[$i]["Submitting"]) ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",'Submitting'"
                        .",'".$sales[$i]["retailer_id"]."'"?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",'Not_Submitting'"
                        .",'".$sales[$i]["retailer_id"]."'"?>)">   
                        <?php echo $sales[$i]["Not_Submitted"] ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",'Not_Submitting'"
                        .",'".$sales[$i]["retailer_id"]."'"?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div id="dialog" title="Sale details">
        <div id="detailscontainer" class="csstable-details">
            <table id="detailstable" class="display compact">
                <thead>
                    <tr>
                        <th>Retailer ID</th>
                        <th>Triangulation ID</th>
                        <th>Retailer Name</th>
                        <th>Sales Amount</th>
                        <th>Address</th>
                        <th>Branch</th>
                        <th>Governorate</th>
                        <th>City</th>
                        <th>Owner</th>
                        <th>Focal Point</th>
                        <th>Tel</th>
                        <th>Email</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.js"></script>

    <script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            autoOpen: false,
            maxWidth: 1000,
            maxHeight: 500,
            width: 800,
            height: 500,
            modal: true
        });
    });
    function showDetails(year, month, status, merchant) {
        $("#dialog").dialog("open");
        if (month < 10) month = "0" + month;
        var criteria = "";
        switch (status) {
            case "All_Branches":
                break;
            case "branches_with_sales":
                criteria = " and sales_amount is not null";
                break;
            case "Submitting":
                criteria = " and id in (select triangulation_id from retailers_submitting where month=" + year + month + ")";
                break;
            case "Not_Submitting":
                criteria = " and sales_amount>0 and id not in (select triangulation_id from retailers_submitting where month=" + year + month + ")";
                break;
        }
        tablename = "(select retailer_id,id as triangulation_id,wfp_name,sales_amount,address, "
            + "branch,governorate, city,Owner,focal_point,tel,email,id from merchants_wfp "
            + "left outer join (SELECT merchants_wfp.id as branch_id, round(SUM(Trans_Amount),2) as sales_amount "
            + "from sales_draft_"+year+month+" "
            + "INNER JOIN "
            + "merchants_meps ON sales_draft_"+year+month+".terminal_id = merchants_meps.Terminal_ID INNER JOIN "
            + "merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id "
            + "group by merchants_wfp.id) as sales_table on merchants_wfp.id=sales_table.branch_id where retailer_id='"+merchant+"' "
            + criteria +")";
        fields = "retailer_id,triangulation_id,wfp_name,sales_amount,address,branch,governorate,city,Owner,focal_point,tel,email,ID";
        $("#detailscontainer").show();
        $("#detailstable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
            "sPaginationType": "full_numbers"
        });
    }
    function exportSummary(year, month) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var sql = "SELECT retailer_branches.retailer_id,retailer_branches.retailer_name, "
        + "retailer_branches.All_Branches, Retailer_Groups_with_Sales.branches_with_sales, "
                + "retailers_submitting_grouped.Submitting "
                + "FROM     retailer_branches LEFT OUTER JOIN "
                + "                  (SELECT retailer_id, Month, COUNT(*) AS Submitting "
                + "FROM     dbo.retailers_submitting where month="+year+month+" "
                + "GROUP BY retailer_id, Month) as retailers_submitting_grouped ON "
                + "retailer_branches.retailer_id = retailers_submitting_grouped.retailer_id LEFT OUTER JOIN "
                + "                 (SELECT dbo.merchants_wfp.retailer_id, "
        + "COUNT(DISTINCT dbo.merchants_meps.Triangulation_ID) AS branches_with_sales "
        + "FROM     dbo.sales_draft_"+year+month+" AS sales_draft_"+year+month+" INNER JOIN "
         + "                 dbo.merchants_meps ON sales_draft_"+year+month+".retailer_id = dbo.merchants_meps.MEPS_Merchant_ID INNER JOIN "
               + "           dbo.merchants_wfp ON dbo.merchants_meps.Triangulation_ID = dbo.merchants_wfp.id "
        + "WHERE  (dbo.merchants_wfp.retailer_id IS NOT NULL) "
        + "GROUP BY dbo.merchants_wfp.retailer_id) as Retailer_Groups_with_Sales "
        + "ON retailer_branches.retailer_id = Retailer_Groups_with_Sales.retailer_id";
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=merchant_sales",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }

    function exportDetails(year, month, status, merchant) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var criteria = "";
        switch (status) {
            case "All_Branches":
                break;
            case "branches_with_sales":
                criteria = " and sales_amount is not null";
                break;
            case "Submitting":
                criteria = " and id in (select triangulation_id from retailers_submitting)";
                break;
            case "Not_Submitting":
                criteria = " and sales_amount>0 and id not in (select triangulation_id from retailers_submitting)";
                break;
        }
        var sql = "select retailer_id,id as triangulation_id,wfp_name,sales_amount,address, "
            + "branch,governorate, city,Owner,focal_point,tel,email,id from merchants_wfp "
            + "left outer join (SELECT merchants_wfp.id as branch_id, round(SUM(Trans_Amount),2) as sales_amount "
            + "from sales_draft_" + year + month + " "
            + "INNER JOIN "
            + "merchants_meps ON sales_draft_" + year + month + ".terminal_id = merchants_meps.Terminal_ID INNER JOIN "
            + "merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id "
            + "group by merchants_wfp.id) as sales_table on merchants_wfp.id=sales_table.branch_id where retailer_id='" + merchant + "' "
            + criteria;
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=merchant_sales",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
</body>
</html>
