<?php session_start();
      include("fncAnalytics.inc.php");
      $duplicates=fncGetDuplicateRRN($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Duplicate RRN</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table style="width: 300px">
                        <tr>
                            <td class="number"><strong>Occurrences</strong></td>
                            <td class="number"><strong>Number of Times</strong></td>
                        </tr>
                        <?php for($i=1;$i<=$duplicates[0][0];$i++){ ?>
                        <tr>
                            <td class="number"><?php echo $duplicates[$i]["occurences"] ?></td>
                            <td class="number"><a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$duplicates[$i]["occurences"] ?>)">
                                <?php echo $duplicates[$i]["no_of_times"] ?>
                                </a>*
                                <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$duplicates[$i]["occurences"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                    <em>* No. of unique RRNs</em>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>RRN</th>
                                <th>Trans. Date</th>
                                <th>Customer ID</th>
                                <th>Merchant</th>
                                <th>Cashier ID</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month, duplicates) {
            if (month < 10) month = "0" + month;
            tablename = "(select RRN,Trans_Date,Customer_ID,Merchant,terminal_id,ROUND(Trans_Amount,2) AS Trans_Amount,ID from sales_draft_" + year + month
            + " where RRN in (SELECT RRN FROM sales_draft_" + year + month + " as tmp "
            + " GROUP BY RRN HAVING COUNT(RRN)=" + duplicates + "))";
            fields = "RRN,Trans_Date,Customer_ID,Merchant,terminal_id,Trans_Amount,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month, duplicates) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "select RRN,Trans_Date,Customer_ID,Merchant,ROUND(Trans_Amount,2) AS Trans_Amount from sales_draft_" + year + month
            + " where RRN in (SELECT RRN FROM sales_draft_" + year + month + " as tmp "
            + " GROUP BY RRN HAVING COUNT(RRN)=" + duplicates + ") Order by RRN";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=duplicate_rrn",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
