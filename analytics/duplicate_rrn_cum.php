<?php session_start();
      include("fncAnalytics.inc.php");
      $startmonth=$_POST["year"]."01";
      $endmonth=$_POST["year"].$_POST["month"];
      $duplicates=fncGetDuplicateRRNCum($startmonth,$endmonth);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Duplicate RRN Cum</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table style="width: 400px" id="tabela">
                        <tr>
                            <th style="text-align: left">Month</th>
                            <th style="text-align: left"><strong>Occurrences</strong></th>
                            <th style="text-align: right"><strong>Number of Times</strong></th>
                        </tr>
                        <?php for($i=1;$i<=$duplicates[0][0];$i++){ 
                                  $pyear=substr($duplicates[$i]["Month"],0,4);
                                  $pmonth=substr($duplicates[$i]["Month"],4,2);?>
                        <tr>
                            <td rowspan="1"><?php echo fncGetMonthName($duplicates[$i]["Month"]) ?></td>
                            <td rowspan="1"><?php echo $duplicates[$i]["occurences"] ?></td>
                            <td rowspan="1" style="text-align: right">
                                <a href="javascript:showDetails(<?php echo $pyear.",".$pmonth.",".$duplicates[$i]["occurences"] ?>)">
                                    <?php echo $duplicates[$i]["no_of_times"] ?>
                                </a>*
                                <a href="javascript:exportDetails(<?php echo $pyear.",".$pmonth.",".$duplicates[$i]["occurences"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
                <script src="../js/tabletools.js"></script>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>RRN</th>
                                <th>Trans. Date</th>
                                <th>Customer ID</th>
                                <th>Merchant</th>
                                <th>Cashier ID</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <em>*Number of unique auth numbers</em>
    <script type="text/javascript">
        function showDetails(year, month, duplicates) {
            if (month < 10) month = "0" + month;
            tablename = "(select RRN,Trans_Date,Customer_ID,Merchant,terminal_id,ROUND(Trans_Amount,2) as Trans_Amount,ID from sales_draft_" + year + month
            + " where RRN in (SELECT RRN FROM sales_draft_" + year + month + " as tmp "
            + " GROUP BY RRN HAVING COUNT(RRN)=" + duplicates + "))";
            fields = "RRN,Trans_Date,Customer_ID,Merchant,terminal_id,Trans_Amount,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month, duplicates) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "select RRN,Trans_Date,Customer_ID,Merchant,ROUND(Trans_Amount,2) as Trans_Amount from sales_draft_" + year + month
            + " where RRN in (SELECT RRN FROM sales_draft_" + year + month + " as tmp "
            + " GROUP BY RRN HAVING COUNT(RRN)=" + duplicates + ") Order by RRN";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=duplicate_rrn_cum",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
