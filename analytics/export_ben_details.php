<?php session_start();
      include("fncAnalytics.inc.php");
      if(isset($_GET["year"]) && isset($_GET["month"])){      
          $year=$_GET["year"];
          $month=$_GET["month"];

          $sql =  "SELECT summary_reload.Customer_ID, summary_wfp_reload.Household_Size, summary_reload.Reload_Amount, "
            . "sales_summary.Sales_Amount,ROUND(summary_reload.Reload_Amount-sales_summary.Sales_Amount,2) as Variance,"
            . "summary_reload.No_of_Reloads,sales_summary.No_of_Sales, sales_summary.Highest_Amount, "
            . "sales_summary.Lowest_Amount, sales_summary.Average_Amount, sales_summary.No_of_Merchants,"
            . "summary_wfp_reload.Governorates, summary_reload.Customer_ID as ID "
            . "FROM (SELECT     Customer_ID, COUNT(*) AS No_of_Reloads, SUM(Trans_Amount) AS Reload_Amount "
            . "FROM dbo.pre_paid_reload_meps_". $year . $month
            . " GROUP BY Customer_ID) as summary_reload INNER JOIN "
            . "(SELECT Customer_ID, COUNT(*) AS No_of_Sales, ROUND(Sum(Trans_Amount),2) as Sales_Amount, ROUND(MAX(Trans_Amount),2) AS Highest_Amount,"
            . "ROUND(MIN(Trans_Amount),2) AS Lowest_Amount, ROUND(AVG(Trans_Amount),2) AS Average_Amount, COUNT(DISTINCT Merchant) "
            . "AS No_of_Merchants "
            . "FROM sales_draft_" . $year . $month
            . " GROUP BY Customer_ID) as  sales_summary ON summary_reload.Customer_ID = sales_summary.Customer_ID INNER JOIN "
            . "(SELECT     Beneficiary_ID, Household_Size, COUNT(DISTINCT Location) AS Governorates "
            . "FROM pre_paid_reload_wfp_" . $year . $month
            . " GROUP BY Beneficiary_ID, Household_Size) as summary_wfp_reload "
            . "ON summary_reload.Customer_ID = summary_wfp_reload.Beneficiary_ID";

          $file=exportXLS($sql,"beneficiary_statistics");
          if(substr($file,-3)=="xls"){
              echo json_encode(array('xls' => $file));
          }
          else{
              echo 'An error occurred. The file could not be exported';
          }
      }
      else{
          echo 'Invalid input data';
      }
?>