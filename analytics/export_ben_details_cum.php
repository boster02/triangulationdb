<?php session_start();
      include("fncAnalytics.inc.php");
      if(isset($_GET["year"]) && isset($_GET["month"])){      
          $year=$_GET["year"];
          $month=$_GET["month"];

          $sql =  "SELECT summary_reload.Customer_ID, summary_wfp_reload.Highest_Household_Size,summary_wfp_reload.Lowest_Household_Size,"
                  . "summary_wfp_reload.Average_Household_Size, summary_reload.Reload_Amount, "
                  . "sales_summary.Sales_Amount,ROUND(summary_reload.Reload_Amount-sales_summary.Sales_Amount,2) as Variance,"
                  . "summary_reload.Average_Reload,sales_summary.Highest_Amount, "
                  . "sales_summary.Lowest_Amount, sales_summary.Average_Amount, sales_summary.No_of_Merchants,"
                  . "summary_wfp_reload.Governorates, summary_reload.Customer_ID as ID "
                  . "FROM (SELECT     Customer_ID, round(AVG(Trans_Amount),2) AS Average_Reload, SUM(Trans_Amount) AS Reload_Amount "
                  . "FROM dbo.prepaid_reload_meps WHERE LEFT(Trans_Date,6)<=".$year . $month
                  . " GROUP BY Customer_ID) as summary_reload INNER JOIN "
                  . "(SELECT Customer_ID, COUNT(*) AS No_of_Sales, ROUND(Sum(Trans_Amount),2) as Sales_Amount, ROUND(MAX(Trans_Amount),2) "
                  . "AS Highest_Amount,"
                  . "ROUND(MIN(Trans_Amount),2) AS Lowest_Amount, ROUND(AVG(Trans_Amount),2) AS Average_Amount, COUNT(DISTINCT Merchant) "
                  . "AS No_of_Merchants "
                  . "FROM sales_draft WHERE LEFT(Trans_Date,6)<=" . $year . $month
                  . " GROUP BY Customer_ID) as  sales_summary ON summary_reload.Customer_ID = sales_summary.Customer_ID INNER JOIN "
                  . "(SELECT     Beneficiary_ID, MAX(Household_Size) as Highest_Household_Size,MIN(Household_Size) as Lowest_Household_Size,AVG(Household_Size) as Average_Household_Size, COUNT(DISTINCT Location) AS Governorates "
                  . "FROM prepaid_reload_wfp WHERE LEFT(Trans_Date,6)<=" . $year . $month
                  . " GROUP BY Beneficiary_ID) as summary_wfp_reload "
                  . "ON summary_reload.Customer_ID = summary_wfp_reload.Beneficiary_ID";


          $file=exportXLS($sql,"beneficiary_statistics_cum");
          if(substr($file,-3)=="xls"){
              echo json_encode(array('xls' => $file));
          }
          else{
              echo 'An error occurred. The file could not be exported';
          }
      }
      else{
          echo 'Invalid input data';
      }
?>