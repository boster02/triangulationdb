<?php session_start();
      include("fncAnalytics.inc.php");
      if(isset($_GET["criteria"]) && isset($_GET["merchant"])){
          $sCriteria=$_GET["criteria"];
          $sMerchant=$_GET["merchant"];
          $sql = "select distinct t1.barcode,t2.item_description,t2.item_description_english,
            brand,family,sub_category,category,food_group from merchant_sales t1
            left join t03barcodes t2 on t1.sku_code=t2.a03code
            left join [dbo].[v_categoriesv2] t3 on t2.a03family_id=t3.family_id
            where merchant=$sMerchant and t1.barcode is not null $sCriteria";

          $file=exportXLS($sql,"categorisation_status");
          if(substr($file,-3)=="xls"){
              echo json_encode(array('xls' => $file));
          }
          else{
              echo 'An error occurred. The file could not be exported';
          }
      }
      else{
          echo 'Invalid input data. Merchant or criteria missing';
      }
?>