<?php session_start();
      include("fncAnalytics.inc.php");
      
      if(isset($_GET["sql"]) && isset($_GET["tablename"])){
          $file=exportXLS($_GET["sql"],$_GET["tablename"]);
          if(substr($file,-3)=="xls"){
              echo json_encode(array('xls' => $file));
          }
          else{
              echo 'An error occurred. The file could not be exported';
          }
      }
      else{
          echo 'The selected table could not be found';
      }
?>