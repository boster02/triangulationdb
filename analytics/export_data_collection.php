<?php session_start();
      include("fncAnalytics.inc.php");
      $retailers=fncGetAllMerchants();
      $startmonth=$_GET["year"]."01";
      $endmonth=$_GET["year"].$_GET["month"];
      $uploads=fncGetUploadStatusCum($startmonth,$endmonth);
      $infolder=fncGetFilesInFolder($startmonth,$endmonth);
      $sales=fncGetLatestSales($endmonth);
      $d=date('YmdHis');
      $filename="../downloads/data_collection_report$d.xls";
      $myfile = fopen($filename, "w") or die("Unable to open file!");

      $txt="<!doctype html>
<html>
<head>
    <meta charset='utf-8' />
    <title>Data Collection and Upload Report</title>
    <style>
        body {
            font-family: Calibri, 'Trebuchet MS', sans-serif;
        }
        .uploaded{
            font-family:Wingdings;
            color:green;
            font-weight:bold;
            font-size:150%;
        }
        .in-folder {
            font-family: Wingdings;
            color: darkgoldenrod;
            font-weight: bold;
            font-size: 150%;
        }
        .not-uploaded {
            font-family: Wingdings;
            color: red;
            font-weight: bold;
            font-size: 150%;
        }
    </style>
</head>

<body>
    <div class='csstable'>
        <h3>Retailer Data Upload Status</h3>
        <strong>KEY:</strong>
        <table border='1'>
            <tr style='font-weight:bold;background-color:darkblue;color:white'>
                <td>Retailer ID</td>
                <td>Branch ID</td>
                <td>
                    <strong>Retailer Name</strong>
                </td>
                <td>
                    <strong>
                        ".fncGetMonthName($endmonth)." Sales
                    </strong>
                </td>";
      $months="";
      for($month=$startmonth;$month<=$endmonth;$month++){
          $months.="<td>".fncGetMonthName($month)."</td>";
      }
      $txt.=$months."</tr>";
      $txt_retailers="";
      for($i=1;$i<=$retailers[0][0];$i++){
          $txt_retailers.="<tr>
                <td>
                    ". $retailers[$i]["retailer_id"] ."
                </td>
                <td>
                    ".$retailers[$i]["id"] ."
                </td>
                <td>
                    ". $retailers[$i]["fullname"] ."
                </td>
                <td class='number'>
                    ".number_format(isset($sales[$retailers[$i]["id"]])?$sales[$retailers[$i]["id"]]:0,2)
              ."</td>";
          $txt_uploads="";
          for($month=$startmonth;$month<=$endmonth;$month++){
              if(isset($uploads[$month][$retailers[$i]["id"]])){
                  $class="uploaded";
                  $icon="ü";
              }
              elseif(isset($infolder[$month][$retailers[$i]["id"]])){
                  $class="in-folder";
                  $icon="1";
              }
              else{
                  $class="not-uploaded";
                  $icon="ý";
              }
              $txt_uploads.="<td><span class='$class'>$icon</span></td>";
          }
          $txt_retailers.=$txt_uploads."</tr>";
      }
      $txt.=$txt_retailers."</table>
    </div>
</body>
</html>";
      fwrite($myfile, $txt);
      fclose($myfile);
      header("Location: $filename");
      die();
