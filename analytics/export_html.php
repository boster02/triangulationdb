<?php session_start();
      header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
      include("fncAnalytics.inc.php");

      if(isset($_POST["html"]) && isset($_POST["tablename"])){
          $file=exportHTML2XLS($_POST["html"],$_POST["tablename"]);
          if(substr($file,-3)=="xls"){
              echo json_encode(array('xls' => $file));
          }
          else{
              echo 'An error occurred. The file could not be exported';
          }
      }
      else{
          echo 'No content to export';
      }
?>