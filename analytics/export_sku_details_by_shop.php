<?php
header('Content-Type: text/html; charset=utf-8');
include("fncAnalytics.inc.php");
$year=isset($_GET["year"])?$_GET["year"]:0;
$month=isset($_GET["month"])?$_GET["month"]:0;
$month=str_pad($month,2,"0",STR_PAD_LEFT);
$merchant=isset($_GET["merchant"])?$_GET["merchant"]:0;
if($_GET["changed"]=="true"){
    $result=fncGetChangedSKUDailyPricesSQL($year.$month,$merchant);
}
else{
    $result=fncGetSKUDailyPricesSQL($year.$month,$merchant);
}
$sql=$result[0];
$days=$result[1];
$file=exportXLS($sql,"sku_details_by_shop");
if(substr($file,-3)=="xls"){
    echo json_encode(array('xls' => $file));
}
else{
    echo 'An error occurred. The file could not be exported';
}