<?php session_start();
header('Content-Type: text/html; charset=utf-8');
include('fncAnalytics.inc.php');
$year=isset($_GET['year'])?$_GET['year']:0;
$month=isset($_GET['month'])?$_GET['month']:0;
$month=str_pad($month,2,'0',STR_PAD_LEFT);
$merchant=isset($_GET['merchant'])?$_GET['merchant']:0;
$prices=fncGetWeightedPriceIndexDetails($year.$month,$merchant);
$style='style="background-color: black;color: white;text-align: left;font-weight:bold"';
$txt="<table border='1'>
        <tr>
            <td $style>Barcode</td>
            <td $style>SKU DESCRIPTION</td>
            <td $style>Weighted Quantity</td>
            <td $style>Shop Price</td>
            <td $style>Weighted Shop Price</td>
            <td $style>Non WFP Price</td>
            <td $style>Weighted Market Price</td>
            <td $style>Shop Items</td>
        </tr>";
        for($i=1;$i<=$prices[0][0];$i++){
        $txt.="<tr>
            <td>
            ". $prices[$i]['alt_Barcode'] ."
            </td>
            <td>
                ". $prices[$i]['SKU_DESCRIPTION'] ."
            </td>
            <td class='number'>
                ". $prices[$i]['Weighted_Quantity'] ."
            </td>
            <td class='number'>
                ". number_format($prices[$i]['Unit_Price'],2) ."
            </td>
            <td class='number'>
                ". number_format($prices[$i]['Weighted_Shop_Price'],2) ."
            </td>
            <td class='number'>
                ". number_format($prices[$i]['Non_WFP_Price'],2) ."
            </td>
            <td class='number'>
                ". number_format($prices[$i]['Weighted_Market_Price'],2) ."
            </td>
            <td>
            ". $prices[$i]['originals'] ."
            </td>
        </tr>";
        }
        $txt.="
        <tr style='background-color:#ccc'>
            <td>
                <strong>TOTAL</strong>
            </td>
            <td>
                &nbsp;
            </td>
            <td class='number'>
                &nbsp;
            </td>
            <td class='number'>
                &nbsp;
            </td>
            <td class='number'>
                ". number_format($prices[0]['sum_wfp'],2) ."
            </td>
            <td class='number'>
                &nbsp;
            </td>
            <td class='number'>
                ". number_format($prices[0]['sum_non_wfp'],2) ."
            </td>
        </tr>
    </table>
    <h2>
        Price Index = ". number_format($prices[0]['sum_wfp'],2) ."/". number_format($prices[0]['sum_non_wfp'],2) ."
         x 100=". number_format($prices[0]['price_index'],2) ."%
    </h2>
";
$file=exportHTML2XLS($txt,"weighted_price_index_details");
if(substr($file,-3)=="xls"){
    echo json_encode(array('xls' => $file));
}
else{
    echo 'An error occurred. The file could not be exported';
}