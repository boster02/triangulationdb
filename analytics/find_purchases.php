<?php session_start();
      include("fncAnalytics.inc.php");
      if(isset($_POST["submitted"])){
          $sales=fncGetSalesReport($_POST["merchant_data"],"",$_POST["governorate_data"],
              $_POST["date_data"],$_POST["month_data"],$_POST["year_data"],$_POST["category_data"],$_POST["brand_data"]);
      }
      $merchants=fncGetMerchantsWithSales();
      $governorates=fncGetGovernoratesWithSales();
      //$beneficiaries=fncGetbeneficiariesWithSales();
      $days=fncGetWeekDays();
      $months=fncGetMonthsWithSales();
      $years=fncGetYearsWithsales();
      $categories=fncGetCategories();
      $brands=fncGetBrands();
      $totalamount=0;
      $totalweight=0;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Sales Report</title>
    <script src="../js/jquery-1.11.1.min.js"></script>
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../css/csstable.css" rel="stylesheet" type="text/css" />
    <script src="../js/bootstrap.js"></script>
    <link href="../css/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../js/bootstrap-multiselect.js"></script>
</head>
<body>
    <script type="text/javascript">
        $(function () {
            $('#merchant').multiselect({ includeSelectAllOption: true });
            $('#governorate').multiselect({ includeSelectAllOption: true });
            $('#date').multiselect({ includeSelectAllOption: true });
            $('#month').multiselect({ includeSelectAllOption: true });
            $('#year').multiselect({ includeSelectAllOption: true });
            $('#category').multiselect({ includeSelectAllOption: true });
            $('#brand').multiselect({ includeSelectAllOption: true });
            $('#btnSelected').click(function () {
                $('#loadingb').show();
                var selected = $("#merchant option:selected");
                var data = "";
                selected.each(function () {
                    data += $(this).val() + "|";
                });
                setData("#merchant");
                setData("#governorate");
                setData("#date");
                setData("#month");
                setData("#year");
                setData("#category");
                setData("#brand");
                document.getElementById("form1").submit();
            });
        });
        function setData(selId) {
            var selected = $(selId+" option:selected");
            var data = "";
            i = 0;
            selected.each(function () {
                var delimiter = (i == 0) ? "" : ",";
                data += delimiter+sqlvalue($(this).val());
                i++;
            });
            $(selId+"_data").val(data);
        }
        function sqlvalue(val)
        {
            tmp = val.replace("'","''");
            tmp = "'"+tmp+"'";
            return tmp;
        }
    </script>

    <form id="form1" name="form1" method="post">
        <input type="hidden" name="merchant_data" id="merchant_data" />
        <input type="hidden" name="governorate_data" id="governorate_data" />
        <input type="hidden" name="date_data" id="date_data" />
        <input type="hidden" name="month_data" id="month_data" />
        <input type="hidden" name="year_data" id="year_data" />
        <input type="hidden" name="category_data" id="category_data" />
        <input type="hidden" name="brand_data" id="brand_data" />
        <input type="hidden" name="submitted" id="submitted" value="true" />
        <table class="table-condensed">
            <tr>
                <td>Merchant</td>
                <td>
                    <select id="merchant" name="merchant" style="width: 100px" multiple="multiple">
                        <?php for($i=1;$i<=$merchants[0][0];$i++){ ?>
                        <option value="<?php echo $merchants[$i][0] ?>"><?php echo $merchants[$i][1] ?></option>
                        <?php } ?>
                    </select></td>
                <td>Governorate:</td>
                <td>
                    <select class="form-control" id="governorate" name="governorate" multiple="multiple">
                        <?php for($i=1;$i<=$governorates[0][0];$i++){ ?>
                        <option value="<?php echo $governorates[$i][0] ?>"><?php echo $governorates[$i][0] ?></option>
                        <?php } ?>
                    </select></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Date</td>
                <td>
                    <select class="form-control" id="date" name="date" style="width: 150px" multiple="multiple">
                        <?php for($i=1;$i<=30;$i++){ ?>
                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                        <?php } ?>
                    </select></td>
                <td>Month:</td>
                <td>
                    <select class="form-control" id="month" name="month" style="width: 150px" multiple="multiple">
                        <?php for($i=1;$i<=$months[0][0];$i++){ ?>
                        <option value="<?php echo $months[$i][0] ?>"><?php echo $months[$i][1] ?></option>
                        <?php } ?>
                    </select></td>
                <td>Year:</td>
                <td>
                    <select class="form-control" id="year" name="year" multiple="multiple">
                        <?php for($i=1;$i<=$years[0][0];$i++){ ?>
                        <option value="<?php echo $years[$i][0] ?>"><?php echo $years[$i][0] ?></option>
                        <?php } ?>
                    </select></td>
            </tr>
            <tr>
                <td>Generic category</td>
                <td>
                    <select class="form-control" id="category" name="category" style="width: 150px" multiple="multiple">
                        <?php for($i=1;$i<=$categories[0][0];$i++){ ?>
                        <option value="<?php echo $categories[$i][0] ?>"><?php echo $categories[$i][0] ?></option>
                        <?php } ?>
                    </select></td>
                <td>Brand Name</td>
                <td>
                    <select class="form-control" id="brand" name="brand" style="width: 150px" multiple="multiple">
                        <?php for($i=1;$i<=$brands[0][0];$i++){ ?>
                        <option value="<?php echo $brands[$i][0] ?>"><?php echo $brands[$i][0] ?></option>
                        <?php } ?>
                    </select></td>
                <td></td>
                <td>
                    <input type="button" id="btnSelected" value="View Report" />
                    <!--<input type="submit" id="submit" class="btn btn-primary" value="View Report" />-->
                </td>
            </tr>
        </table>
        <div style="width: 100%; text-align: center">
            <img id="loadingb" src="../images/download.gif" height="100" width="100" style="display: none" /></div>
        <?php if(isset($sales[0][0])) { ?>
        <div class="csstable">
            <table>
                <tr>
                    <td>Generic Name</td>
                    <td>Brand Name</td>
                    <td>Amount (JOD)</td>
                    <td>VAT Category</td>
                    <td>Qty</td>
                    <td>Merchant</td>
                    <td>Governorate</td>
                </tr>
                <?php for($i=1;$i<=$sales[0][0];$i++){ 
                          $totalamount+=$sales[$i]["Total_Amount"];
                          $totalweight+=$sales[$i]["weight_in_kgs"];
                ?>
                <tr>
                    <td style="font-size: 12px"><?php echo $sales[$i]["Generic_Name"]; ?></td>
                    <td style="font-size: 12px"><?php echo $sales[$i]["Item_Description"]; ?></td>
                    <td style="text-align: right; font-size: 12px"><?php echo number_format(floatval($sales[$i]["Total_Amount"]),2); ?></td>
                    <td style="font-size: 12px"><?php echo $sales[$i]["VAT"]; ?>%</td>
                    <td style="text-align: right; font-size: 12px"><?php echo number_format(floatval($sales[$i]["weight_in_kgs"]),2); ?></td>
                    <td style="font-size: 12px"><?php echo $sales[$i]["acc_name"]; ?></td>
                    <td style="font-size: 12px"><?php echo $sales[$i]["governorate"]; ?></td>
                </tr>
                <?php } ?>
                <tr style="background-color: #ccc">
                    <td style="font-size: 12px; font-weight: bold">Total</td>
                    <td style="font-size: 12px; font-weight: bold"></td>
                    <td style="text-align: right; font-size: 12px; font-weight: bold"><?php echo number_format(floatval($totalamount),2); ?></td>
                    <td style="font-size: 12px; font-weight: bold"></td>
                    <td style="text-align: right; font-size: 12px; font-weight: bold"><?php echo number_format(floatval($totalweight),2); ?></td>
                    <td style="font-size: 12px; font-weight: bold"></td>
                    <td style="font-size: 12px; font-weight: bold"></td>
                </tr>
            </table>
            <p>
                <?php } ?>
        </div>
    </form>
    <script type="text/javascript">
        $(function () {
            $("#submit").click(function () {
                $('#loadingb').show();
            });
        });
    </script>
</body>
</html>
