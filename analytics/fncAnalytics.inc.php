<?php header('Content-Type: text/html; charset=utf-8');
ini_set('MAX_EXECUTION_TIME', 900);
ini_set('memory_limit', '-1');
include "config.inc.php";

function fncGetUploadAuthentication($month){
	$dbh=fncOpenDBConn();
	$sql = "SELECT 1 as ID, 'WFP Progr.' AS Description, COUNT(*) AS Trans,SUM(Trans_Amount) "
	."AS Amount,'#' as file_path FROM pre_paid_reload_wfp_$month "
	."UNION SELECT 2 as ID, 'MEPS' AS Description, COUNT(*) AS Trans,SUM(Trans_Amount) "
	."AS Amount,'#' as file_path FROM pre_paid_reload_meps_$month "
	."UNION SELECT 3 as ID, 'JAB' AS Description, COUNT(*) AS Trans,SUM(amount) "
	."AS Amount,'#' as file_path FROM jab_statement_$month "
    ."UNION SELECT 4 as ID, 'WFP LOA' AS Description, COUNT(*) AS Trans,SUM(Trans_Amount) "
	."AS Amount,min(file_path) as file_path FROM pre_paid_reload_loa Where cycle=$month Order by ID";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetUploadAuthenticationCum($startmonth,$endmonth){
	$dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql = "SELECT $month as Month, 1 as ID, 'WFP Progr.' AS Description, COUNT(*) AS Trans,SUM(Trans_Amount) "
        ."AS Amount FROM pre_paid_reload_wfp_$month "
        ."UNION SELECT $month as Month, 2 as ID, 'MEPS' AS Description, COUNT(*) AS Trans,SUM(Trans_Amount) "
        ."AS Amount FROM pre_paid_reload_meps_$month "
        ."UNION SELECT $month as Month, 3 as ID, 'JAB' AS Description, COUNT(*) AS Trans,SUM(amount) "
        ."AS Amount FROM jab_statement_$month "
        ."UNION SELECT $month as Month, 4 as ID, 'WFP LOA' AS Description, COUNT(*) AS Trans,SUM(Trans_Amount) "
        ."AS Amount FROM pre_paid_reload_loa Where cycle=$month Order by ID";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetDuplicateAuthorization($month){
    $dbh=fncOpenDBConn();
    $sql="Select duplicate_auths.occurences, COUNT(*) as no_of_times from (SELECT auth_no, COUNT(auth_no) AS occurences FROM sales_draft_$month
            GROUP BY auth_no HAVING (((COUNT(auth_no))>1))) as duplicate_auths
            Group by occurences";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}

function fncGetDuplicateAuthorizationCum($startmonth,$endmonth){
	$dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="Select duplicate_auths.occurences, COUNT(*) as no_of_times from (SELECT auth_no, COUNT(auth_no) AS occurences FROM sales_draft_$month
            GROUP BY auth_no HAVING (((COUNT(auth_no))>1))) as duplicate_auths
            Group by occurences";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
            $data[$c]["Month"]=$month;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantVisits($month){
    $dbh=fncOpenDBConn();
    $sql="select COUNT(*) as no_of_customers,no_of_merchants from
        (SELECT merchant_visits.Customer_ID,
        COUNT(merchant_visits.Merchant) AS no_of_merchants
        FROM
        (SELECT DISTINCT Customer_ID, Merchant
        FROM sales_draft_$month) AS merchant_visits
        GROUP BY merchant_visits.Customer_ID) as t
        group by no_of_merchants";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantVisitsCum($startmonth,$endmonth){
	$dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="select $month as Month, COUNT(*) as no_of_customers,no_of_merchants from
        (SELECT merchant_visits.Customer_ID,
        COUNT(merchant_visits.Merchant) AS no_of_merchants
        FROM
        (SELECT DISTINCT Customer_ID, Merchant
        FROM sales_draft_$month) AS merchant_visits
        GROUP BY merchant_visits.Customer_ID) as t
        group by no_of_merchants";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetTimeOfSales($month){
    $dbh=fncOpenDBConn();
    $sql="select '00:00 a.m. to 05:59 a.m.' as period, COUNT(*) as Trans,sum(trans_amount) as Amount
        from sales_draft_$month
        where left(Trans_Time,2)>='00' and left(Trans_Time,2)<='05'
        union all select '06:00 a.m. to 11:59 a.m.' as period, COUNT(*) as Trans,sum(trans_amount) as Amount
        from sales_draft_$month
        where left(Trans_Time,2)>='06' and left(Trans_Time,2)<='11'
        union all select '12:00 p.m. to 05:59 p.m ' as period, COUNT(*) as Trans,sum(trans_amount) as Amount
        from sales_draft_$month
        where left(Trans_Time,2)>='12' and left(Trans_Time,2)<='17'
        union all select '06:00 p.m. to 11:59 p.m' as period, COUNT(*) as Trans,sum(trans_amount) as Amount
        from sales_draft_$month
        where left(Trans_Time,2)>='18' and left(Trans_Time,2)<='23'";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetTimeOfSalesCum($startmonth,$endmonth){
	$dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="select $month as Month, '00:00 a.m. to 05:59 a.m.' as period, COUNT(*) as Trans,sum(trans_amount) as Amount
            from sales_draft_$month
            where left(Trans_Time,2)>='00' and left(Trans_Time,2)<='05'
            union all select $month as Month,  '06:00 a.m. to 11:59 a.m.' as period, COUNT(*) Trans,sum(trans_amount) as Amount
            from sales_draft_$month
            where left(Trans_Time,2)>='06' and left(Trans_Time,2)<='11'
            union all select $month as Month, '12:00 p.m. to 05:59 p.m ' as period, COUNT(*) as Trans,sum(trans_amount) as Amount
            from sales_draft_$month
            where left(Trans_Time,2)>='12' and left(Trans_Time,2)<='17'
            union all select $month as Month, '06:00 p.m. to 11:59 p.m' as period, COUNT(*) as Trans,sum(trans_amount) as Amount
            from sales_draft_$month
            where left(Trans_Time,2)>='18' and left(Trans_Time,2)<='23'";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetProcessingDateVariance($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date)))) as Days,
            COUNT(*) as Trans, SUM(Trans_Amount) as Amount, COUNT(distinct merchant) as Merchants
            FROM sales_draft_$month
            where datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))>0
            group by datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))
            Order by datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetProcessingDateVarianceCum($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="SELECT $month as Month,datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date)))) as Days,
            COUNT(*) as Trans, SUM(Trans_Amount) as Amount, COUNT(distinct merchant) as Merchants
            FROM sales_draft_$month
            where datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))>0
            group by datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))
            Order by datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
            $data[$c]["Month"]=$month;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetOverdrafts($month){
    $dbh=fncOpenDBConn();
    $sql="select count(*) as no_of_cards,sum(ClosingBalance) as overdraft
            from GeneralAccountBalances where cycle=$month and closingbalance<0";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data=$row;

    $sql="SELECT SUM(Trans_Amount) AS Amount FROM sales_draft_$month";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data["sales_amount"]=$row["Amount"];

    $sql="SELECT SUM(Trans_Amount) AS Amount FROM pre_paid_reload_meps_$month";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data["reload_meps"]=$row["Amount"];

    mssql_close($dbh);
    return $data;
}
function fncGetOverdraftsCum($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $c++;
        $sql="select $month as Month, COUNT(*) as no_of_cards, sum(convert(float,overdraft))*-1 as overdraft
        from UnusedResidualOverdraft where Month=$month
        and convert(float,overdraft)*-1>0";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]=$row;

        $sql="SELECT SUM(Trans_Amount) AS Amount FROM sales_draft_$month";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]["sales_amount"]=$row["Amount"];

        $sql="SELECT SUM(Trans_Amount) AS Amount FROM pre_paid_reload_meps_$month";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]["reload_meps"]=$row["Amount"];
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetResidual($month){
    $dbh=fncOpenDBConn();
    $sql="select count(*) as no_of_cards,sum(ClosingBalance) as residual
            from GeneralAccountBalances where cycle=$month and closingbalance>0";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data=$row;

    $sql="SELECT SUM(Trans_Amount) AS Amount FROM sales_draft_$month";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data["sales_amount"]=$row["Amount"];

    $sql="SELECT SUM(Trans_Amount) AS Amount FROM pre_paid_reload_meps_$month";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data["reload_meps"]=$row["Amount"];

    mssql_close($dbh);
    return $data;
}
function fncGetResidualCum($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $c++;
        $sql="select $month as Month, COUNT(*) as no_of_cards, sum(convert(float,residual)) as residual
        from UnusedResidualOverdraft where Month=$month
        and convert(float,residual)>0";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]=$row;

        $sql="SELECT SUM(Trans_Amount) AS Amount FROM sales_draft_$month";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]["sales_amount"]=$row["Amount"];

        $sql="SELECT SUM(Trans_Amount) AS Amount FROM pre_paid_reload_meps_$month";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]["reload_meps"]=$row["Amount"];
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetUnused($month){
    $dbh=fncOpenDBConn();
    $onemonthbefore=fncGetPreviousMonth($month);
    $twomonthsbefore=fncGet2MonthsBefore($month);
    $sql="select count(Customer_ID) CardsNumber ,sum(CurrentBalance) UnusedBalance from
        (
        select
        r.Customer_ID,
        max(CONVERT(DATETIME,r.Trans_Date,112)) ReloadDate,
        min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)) LastTransaction,
        DATEADD(day,60,max(CONVERT(DATETIME,cast(r.Trans_Date as char(8)),112))) CreditExpiryDate,
        DATEDIFF(day, max(CONVERT(DATETIME,r.Trans_Date,112)),isnull(min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)),GETDATE())) UnusedPeriodInDays,
        Location,
        CurrentBalance
        from (select * from [dbo].[sales_draft]  where substring(cast(trans_date as char(8)),0,7) in ($twomonthsbefore, $onemonthbefore, $month) ) S
        right join pre_paid_reload_meps_$twomonthsbefore R on r.Customer_ID = s.Customer_ID
        left join pre_paid_reload_wfp_$twomonthsbefore on Beneficiary_ID = r.Customer_ID
        left join (
        select t.Customer_ID,CurrentBalance from (
        select *,
        ROW_NUMBER() over (partition by customer_id order by trans_date desc, serial desc) rn
            from OpeningBalanceArchive
        ) t
        where rn = 1
        ) Balances on Balances.Customer_ID = R.Customer_ID
        where

            r.Customer_ID not in (select Customer_ID from [MEPS_Adjustment_Transactions] c where c.cycle
        in ($twomonthsbefore, $onemonthbefore, $month) and transaction_type in ('Pre-Paid Redemption','Cash ATM'))
        group by r.Customer_ID,Location,CurrentBalance,r.Prepaid_Program
        having
        min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)) is null
        and DATEDIFF(day, max(CONVERT(DATETIME,r.Trans_Date,112)),isnull(min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)),GETDATE()))  > 60
        and CurrentBalance > 0
        and r.Prepaid_Program in('AL ZAATARI CARD','WFP NORMAL CARD SALES')
        ) t";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data=$row;

    mssql_close($dbh);
    return $data;
}
function fncGetUnusedCum($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $c++;
        $sql="select $month as Month, COUNT(*) as no_of_cards, sum(convert(float,unused)) as unused
        from UnusedResidualOverdraft where Month=$month
        and convert(float,unused)>0";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]=$row;

        $sql="SELECT SUM(Trans_Amount) AS Amount FROM sales_draft_$month";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]["sales_amount"]=$row["Amount"];

        $sql="SELECT SUM(Trans_Amount) AS Amount FROM pre_paid_reload_meps_$month";
        $res = mssql_query($sql,$dbh);
        $row = mssql_fetch_array($res);
        $data[$month]["reload_meps"]=$row["Amount"];
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetReconciliation($month){
    $dbh=fncOpenDBConn();

    $sql="select * from (
        select CustomersNumber,NonTrxCustomers - CustomersNumber as NonTransacted,Credit,Debit,Balance,OpeningBalance, r1.ClosingBalance,r1.cycle,
        sum(cast(trans_amount as decimal(18,3))) as trans_amount ,Transaction_Type,p2.[Sales Draft],p3.[Pre-Paid Reload]
        from [dbo].[ReconciliationTotals] r1 left join MEPS_Adjustment_Transactions m on m.Cycle = r1.cycle
          outer apply
        (
        select cast(debit - isnull(sum(trans_amount),0) as decimal(18,3)) as [Sales Draft] from MEPS_Adjustment_Transactions m2 inner join MEPS_TransactionTypes on Transaction_Type = TransactionType where m2.Cycle = r1.cycle and Category = 'Debit'
        )p2
        outer apply
        (
        select cast(Credit - isnull(sum(trans_amount),0) as decimal(18,3)) as [Pre-Paid Reload] from MEPS_Adjustment_Transactions m3 inner join MEPS_TransactionTypes on Transaction_Type = TransactionType where m3.Cycle = r1.cycle and Category = 'Credit'
        )p3
        group by CustomersNumber,NonTrxCustomers,Credit,Debit,Balance , r1.ClosingBalance,r1.cycle,
        Transaction_Type,p2.[Sales Draft],p3.[Pre-Paid Reload],OpeningBalance
        )src
        PIVOT
        (
        sum(trans_amount)
        FOR Transaction_Type IN ([Credit Voucher],[Credit Voucher (Sale)],[Pre-Paid Miscellaneous Credit],
        [Pre-Paid Miscellaneous Debit],
        [Pre-Paid Redemption],
        [Cash ATM])

        ) AS pvt
        where cycle = $month
        ";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data=$row;

    mssql_close($dbh);
    return $data;
}
function fncGetReconciliationCum($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
        $c=0;
        for($month=$startmonth;$month<=$endmonth;$month++){
            $c++;

            $sql="SELECT SUM(Trans_Amount) AS Amount FROM sales_draft_$month";
            $res = mssql_query($sql,$dbh);
            $row = mssql_fetch_array($res);
            $data[$month]["sales_amount"]=$row["Amount"];

            $sql="SELECT count(*) as no_of_cards, SUM(Trans_Amount) AS Amount FROM pre_paid_reload_meps_$month";
            $res = mssql_query($sql,$dbh);
            $row = mssql_fetch_array($res);
            $data[$month]["no_of_cards"]=$row["no_of_cards"];
            $data[$month]["reload_meps"]=$row["Amount"];

            $sql="select sum(overdraft) as overdraft from unusedResidualOverdraft where Month=$month";
            $res = mssql_query($sql,$dbh);
            $row = mssql_fetch_array($res);
            $data[$month]["overdraft"]=$row["overdraft"];

            $sql="select sum(unused) as unused from UnusedResidualOverdraft where Month=$month";
            $res = mssql_query($sql,$dbh);
            $row = mssql_fetch_array($res);
            $data[$month]["unused"]=$row["unused"];

            $sql="select sum(residual) as residual from UnusedResidualOverdraft where Month=$month";
            $res = mssql_query($sql,$dbh);
            $row = mssql_fetch_array($res);
            $data[$month]["residual"]=$row["residual"];
        }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetSalesValueVolumeRatio($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT t3.retailer_id as retailer_id,t3.id as branch_id,concat(wfp_name, ' - ', branch, ' - ', address) as wfp_name,
            count(*) as Trans, SUM(round(Trans_Amount,2)) as Amount, ROUND(SUM(Trans_Amount)/count(*),2) AS Ratio,
			count(distinct customer_id) as Bens,round(cast(count(*) as float)/cast(count(distinct customer_id) as float),2) as Trans_Per_Ben
            FROM Sales_Draft_$month as t1
            Inner join Merchants_MEPS as t2 on t1.terminal_id=t2.terminal_id
            inner join merchants_wfp as t3 on t2.triangulation_id=t3.id
            Group by t3.retailer_id,t3.id,concat(wfp_name, ' - ', branch, ' - ', address)
            Order by Ratio desc";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $row["position"]=$i;
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetSalesValueVolumeRatioCum($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="SELECT $month as Month, t3.retailer_id as retailer_id,t3.id as branch_id,concat(wfp_name, ' - ', branch, ' - ', address) as wfp_name,
            count(*) as Trans, SUM(round(Trans_Amount,2)) as Amount, ROUND(SUM(Trans_Amount)/count(*),2) AS Ratio,
			count(distinct customer_id) as Bens,round(cast(count(*) as float)/cast(count(distinct customer_id) as float),2) as Trans_Per_Ben
            FROM Sales_Draft_$month as t1
            Inner join Merchants_MEPS as t2 on t1.terminal_id=t2.terminal_id
            inner join merchants_wfp as t3 on t2.triangulation_id=t3.id
            Group by t3.retailer_id,t3.id,concat(wfp_name, ' - ', branch, ' - ', address)
            Order by Ratio desc";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $row["position"]=$i;
            $data[$c]=$row;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantsWFPMEPS($month){
    $dbh=fncOpenDBConn();
    $sql="Select Retailer_ID,Triangulation_ID,Shop_Name,MEPS_Sales_Value,Retailer_Sales_Value,
        MEPS_Trans,Retailer_Receipts  from (
        SELECT merchants_wfp.retailer_id,merchants_meps.Triangulation_ID,concat(wfp_name,' - ',branch,' - ',address) as shop_name,
        Sum(Trans_Amount) as MEPS_Sales_Value,COUNT(*) as MEPS_Trans
         from sales_draft_$month
		        sales_draft_$month INNER JOIN
                                 merchants_meps ON sales_draft_$month.terminal_id = merchants_meps.Terminal_ID INNER JOIN
                                 merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id
		        group by concat(wfp_name,' - ',branch,' - ',address),merchants_wfp.retailer_id,merchants_meps.Triangulation_ID) as t1
		        left join (SELECT Merchant, SUM(Total_Amount) AS Retailer_Sales_Value,count(distinct Receipt_No) as Retailer_Receipts
        FROM     merchant_sales_$month
        GROUP BY Merchant) as t2 on t1.Triangulation_ID=t2.Merchant
        order by Retailer_ID,Triangulation_ID
        ";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantPurchasesFood($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT food_group as description,count(*) as trans,count(distinct customer_id) as bens,sum(quantity) as qty,
            sum(Total_Amount) as amount
            FROM  merchant_sales_$month t1 INNER JOIN
                         t03barcodes t2 ON t1.sku_code = t2.a03Code INNER JOIN
                         v_categoriesV2 t3 ON t2.a03family_id = t3.family_id
                         where food_group is not null
						 group by food_group";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantPurchasesFoodCum($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="SELECT '$month' as Month, food_group as description,count(*) as trans,count(distinct customer_id) as bens,sum(quantity) as qty,
            sum(Total_Amount) as amount
            FROM  merchant_sales_$month t1 INNER JOIN
                         t03barcodes t2 ON t1.sku_code = t2.a03Code INNER JOIN
                         v_categoriesV2 t3 ON t2.a03family_id = t3.family_id
						 group by food_group";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantPurchasesNFI($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT ITEM_DESCRIPTION_ENGLISH as description,count(*) as trans,count(distinct customer_id) as bens,sum(quantity) as qty,
            sum(Total_Amount) as amount
            FROM  merchant_sales_$month t1 INNER JOIN
                         t03barcodes t2 ON t1.sku_code = t2.a03Code INNER JOIN
                         v_categoriesV2 t3 ON t2.a03family_id = t3.family_id
                         where family_id=57 and ITEM_DESCRIPTION_ENGLISH is not null
						 group by ITEM_DESCRIPTION_ENGLISH
                         order by description";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantPurchasesNFICum($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="SELECT '$month' as Month,ITEM_DESCRIPTION_ENGLISH as description,count(*) as trans,count(distinct customer_id) as bens,sum(quantity) as qty,
            sum(Total_Amount) as amount
            FROM  merchant_sales_$month t1 INNER JOIN
                         t03barcodes t2 ON t1.sku_code = t2.a03Code INNER JOIN
                         v_categoriesV2 t3 ON t2.a03family_id = t3.family_id
                         where family_id=57 and ITEM_DESCRIPTION_ENGLISH is not null
						 group by ITEM_DESCRIPTION_ENGLISH
                         order by description";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetTotalSalesPerMerchant($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT Triangulation_ID, Governorate,concat(wfp_name,' - ',branch,' - ',address) as shop_name,merchants_wfp.retailer_id,Count(Distinct Customer_ID) as Bens,Sum(Trans_Amount) as SalesValue,COUNT(*) as SalesVolume,
        convert(float,Count(*))/(select Count(*) from sales_draft_$month)*100 as SalesVolumePercent,
        Sum(Trans_Amount)/(select SUM(Trans_Amount) from sales_draft_$month)*100 as
        SalesValuePercent from sales_draft_$month
		sales_draft_$month INNER JOIN
                         merchants_meps ON sales_draft_$month.terminal_id = merchants_meps.Terminal_ID INNER JOIN
                         merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id
		group by Triangulation_ID,Governorate,concat(wfp_name,' - ',branch,' - ',address),merchants_wfp.retailer_id order by SalesVolumePercent DESC";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
    }
    mssql_close($dbh);
    return $data;
}
function fncGetTotalSalesPerMerchantGrouped($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT Governorate,min(wfp_name) as shop_name,min(merchant) as meps_name,min(sales_draft_$month.retailer_id) as meps_id,merchants_wfp.retailer_id,Count(Distinct Customer_ID) as Bens,Sum(Trans_Amount) as SalesValue,COUNT(*) as SalesVolume,
        convert(float,Count(*))/(select Count(*) from sales_draft_$month)*100 as SalesVolumePercent,
        Sum(Trans_Amount)/(select SUM(Trans_Amount) from sales_draft_$month)*100 as
        SalesValuePercent from sales_draft_$month
		sales_draft_$month left outer JOIN
                         merchants_meps ON sales_draft_$month.retailer_id = merchants_meps.MEPS_Merchant_ID left outer JOIN
                         merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id
		group by Governorate,merchants_wfp.retailer_id order by SalesVolumePercent DESC";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
    }
    mssql_close($dbh);
    return $data;
}
function fncGetTotalSalesPerMerchant_Cum($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT Merchant,Sum(Trans_Amount) as SalesValue,COUNT(*) as SalesVolume,
        convert(float,Count(*))/(select Count(*) from sales_draft)*100 as SalesVolumePercent,
        Sum(Trans_Amount)/(select SUM(Trans_Amount) from sales_draft)*100 as
        SalesValuePercent from sales_draft where LEFT(Trans_Date,6)<=$month group by merchant order by SalesVolumePercent DESC";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetSalesReport($merchant,$beneficiary,$governorate,$date,$month,$year,$category,$brand){
    $dbh=fncOpenDBConn();
    $merchantflt="";
    $beneficiaryflt="";
    $governorateflt="";
    $dateflt="";
    $monthflt="";
    $yearflt="";
    $categoryflt="";
    $brandflt="";
    if($merchant!="") $merchantflt=" AND Merchant in ($merchant)";
    if($beneficiary!="ALL" && $beneficiary!="") $beneficiaryflt=" AND Customer_ID ='$beneficiary'";
    if($governorate!="") $governorateflt=" AND governorate in ($governorate)";
    if($date!="") $dateflt=" AND day(trans_date) in ($date)";
    if($month!="") $monthflt=" AND month(trans_date) in ($month)";
    if($year!="") $yearflt=" AND year(trans_date) in ($year)";
    if($category!="") $categoryflt=" AND generic_name in ($category)";
    if($brand!="") $brandflt=" AND brand_name+' '+package_measure in ($brand)";

    $sql="SELECT   Merchant,Item_Description,VAT, Generic_Name, SUM([quantity]) as weight_in_kgs, Sum(Total_Amount) as Total_Amount,
                   merchants_wfp.acc_name,merchants_wfp.governorate
      FROM          merchant_sales INNER JOIN
      merchants_wfp ON Merchant = merchants_wfp.id INNER JOIN
      merchant_listing ON Item_Description = merchant_listing.Original_Name where Customer_ID is not null
      $merchantflt$beneficiaryflt$governorateflt$dateflt$monthflt$yearflt$categoryflt$brandflt
      group by Merchant,Item_Description,VAT, Generic_Name,Item_Description,
      merchants_wfp.acc_name,merchants_wfp.governorate ORDER BY Generic_Name";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}

function fncGetMerchantsWithSales(){
    $dbh=fncOpenDBConn();
    $sql="Select id,acc_name from merchants_wfp where id in (select distinct merchant from merchant_sales)";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetGovernoratesWithSales(){
    $dbh=fncOpenDBConn();
    $sql="Select distinct governorate from "
        ."(Select governorate from merchants_wfp where id in "
        ."(select distinct merchant from merchant_sales)) as tb2 ";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetBeneficiariesWithSales(){
    $dbh=fncOpenDBConn();
    $sql="select distinct customer_id from merchant_sales";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMonthsWithSales(){
    $dbh=fncOpenDBConn();
    $sql="select distinct month(Trans_date) as Monthid, datename(month,Trans_Date) as monthname from merchant_sales";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetYearsWithSales(){
    $dbh=fncOpenDBConn();
    $sql="select distinct year(Trans_date) from merchant_sales";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetCategories(){
    $dbh=fncOpenDBConn();
    $sql="select distinct Generic_Name from merchant_listing where len(generic_name)>0";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetBrands(){
    $dbh=fncOpenDBConn();
    $sql="select distinct brand_name+' '+package_measure as Brand_Name from merchant_listing where len(ltrim(rtrim(brand_name+' '+package_measure)))>0";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMonthName($month){
    return strtoupper(date('M - Y',strtotime($month."01")));
}
function fncGetWeekDays(){
    return array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
}
function fncGetMEPSReloadDetails(){
    $dbh=fncOpenDBConn();
    $sql="select Customer_ID,Trans_Amount from pre_paid_reload_meps_$month";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetJABUploadDetails($type,$year,$month,$start=1,$end=10){
    $dbh=fncOpenDBConn();
    $sql = "SELECT * FROM
			(SELECT ROW_NUMBER() OVER(ORDER BY ID)
			AS RowNumber, Customer_ID, Trans_Amount FROM jab_statement_$year$month)
		AS TEST
		WHERE RowNumber BETWEEN ? AND ? + 1";

    $params = array(&$start, &$end);

    $res = sqlsrv_query($dbh, $sql, $params, array( "Scrollable" => "keyset" ));
    $data[0][0] = sqlsrv_num_rows($res);
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res);
        $data[$i]=$row;
    }
    mssql_close($dbh);
    return $data;
}
//function fncGetVRLData(){
//    $dbh=fncOpenDBConn();
//    $sql="Select Top 10 Customer_ID,Card_Number,Partner,Governorate,Status from VRL";
//    $res = mssql_query($sql,$dbh);
//    $data[0][0] = mssql_num_rows($res);
//    for ($i=1;$i<=$data[0][0];$i++){
//        $row = mssql_fetch_array($res);
//        $data[$i]=$row;
//        $data[$i]["Original_Dist"]="";//fncGetOriginalDist($row["Customer_ID"],$row["Card_Number"]);
//         $data[$i]["In"]="";
//        $data[$i]["Out"]=($data[$i]["Original_Dist"]!="None");
//        $data[$i]["Times_Used_Reload"]=""; //fncGetTimesUsed($row["Customer_ID"],$row["Card_Number"]);
//        $data[$i]["Times_Used_Sales"]="";
//        $data[$i]["Date_Returned"]="";
//        $data[$i]["Date_Destroyed"]="";
//        $data[$i]["Reason_Replaced"]="";
//        $data[$i]["Date_Replaced"]="";
//        $data[$i]["Replacement_Card_ Number"]="";
//    }
//    mssql_close($dbh);
//    return $data;
//}
function fncGetVRLSummary(){
    $dbh=fncOpenDBConn();
    $data["jab_pressed"]=fncGetJABPressed($dbh);
    $data["program_listing"]=fncGetProgrammeListing($dbh);
    $data["distributed"]=fncDistributedCards($dbh);
    $data["replaced"]=fncGetReplacedCards($dbh);
    $data["undistributed"]=fncGetUndistributedCards($dbh);
    $data["destroyed"]=fncGetDestroyedCards($dbh);
    $data["lsd"]=fncGetLsdCards($dbh);
    return $data;
}
function fncGetJABPressed($dbh){
    $sql="select count(1) from (
        select distinct Customer_ID,Card_Number from jab_vrl
        where
        card_program in ('AL ZAATARI CARD','WFP NORMAL CARD SALES')
        ) t";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_row($res);
    return $row[0];
}
function fncGetProgrammeListing($dbh){
    $sql="select count(*) from vrl as t1";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_row($res);
    return $row[0];
}
function fncDistributedCards($dbh){
    $sql="select count(1) from (
        select distinct Customer_ID,Card_Number from jab_vrl J
        where exists (select 1 from prepaid_reload_meps r where j.Customer_ID = r.Customer_ID and j.Card_Number = r.Card_Number)
        AND not exists(
        select 1 from Replaced_cards RC where j.Customer_ID = rc.Customer_ID --and j.Card_Number = rc.Replacement_Card_Number
        )
        and
        card_program in ('AL ZAATARI CARD','WFP NORMAL CARD SALES')
        ) t
        ";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_row($res);
    return $row[0];
}
function fncGetReplacedCards($dbh){
    $sql="select count(*) from Replaced_Cards";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_row($res);
    return $row[0];
}
function fncGetUndistributedCards($dbh){
    $sql="select count(1) from (
            select distinct Customer_ID,Card_Number from jab_vrl J
            where
            not exists
            (select 1 from prepaid_reload_meps r where j.Customer_ID = r.Customer_ID and right(j.Card_Number,4) = right(r.Card_Number,4))
            AND
            not exists
            (select 1 from Destroyed_Cards D where j.Customer_ID = d.Customer_ID and right(j.Card_Number,4) = right(d.Card_Number,4))
            and
            card_program in ('AL ZAATARI CARD','WFP NORMAL CARD SALES')
            ) t";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_row($res);
    return $row[0];
}
function fncGetDestroyedCards($dbh){
    $sql="select count(*) from Destroyed_Cards";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_row($res);
    return $row[0];
}
function fncGetLsdCards($dbh){
    $sql="select count(*) from Replaced_Cards where customer_id in (select customer_id from prepaid_reload_meps)
    and customer_id not in (select customer_id from destroyed_cards)";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_row($res);
    return $row[0];
}
function fncGetMEPSReloadTables(){
    $i=0;
    $data=array();
    for($year=2014;$year<=2017;$year++){
        for($month=1;$month<=12;$month++){
            $i++;
            $cmonth=$month<10?"0".$month:$month;
            $data[$i]="pre_paid_reload_meps_".$year.$cmonth;
        }
    }
    return $data;
}
function fncGetProgramMonths(){
    $i=0;
    $data=array();
    for($year=2014;$year<=2017;$year++){
        for($month=1;$month<=12;$month++){
            $i++;
            $cmonth=$month<10?"0".$month:$month;
            $data[$i]=$year.$cmonth;
        }
    }
    return $data;
}

function fncUpdateURO(){
    $dbh=fncOpenDBConn();
    $res2=false;
    $sql="Select Customer_ID,cycle,Reload,Sales from Reload_Sales_by_Month order by Customer_ID,cycle";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    //if ( sqlsrv_begin_transaction( $dbh ) === false ) {
    //    die( print_r( sqlsrv_errors(), true ));
    //}
    $filename="uro.sql";
    $myfile = fopen($filename, "w") or die("Unable to open file!");
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
        $unused=0;
        $residual=0;
        $overdraft=0;
        $cumulative_reload=0;
        $cumulative_sales=0;
        $variance=0;
        $customer_id=$data[$i]["Customer_ID"];
        $month=$data[$i]["cycle"];
        $reload=$data[$i]["Reload"];
        $sales=$data[$i]["Sales"];
        $cumulative_reload+=$data[$i]["Reload"];
        $cumulative_sales+=$data[$i]["Sales"];
        if($month<"201506"){
            if(floatval($data[$i]["Sales"])==0){
                $unused=floatval($data[$i]["Reload"]);
            }
            else{
                $variance=$cumulative_reload-$cumulative_sales;
            }
            if($variance>0)
                $residual=$variance;
            elseif($variance<0)
                $overdraft=$variance;
        }
        else{
            if(floatval($data[$i]["Sales"])==0 && isset($data[$i-1]["Sales"]) && floatval($data[$i-1]["Sales"])==0){
                $unused=floatval($data[$i]["Reload"]);
            }
            else{
                $variance=$cumulative_reload-$cumulative_sales;
            }
            if($variance>0)
                $residual=$variance;
            elseif($variance<0)
                $overdraft=$variance;
        }

        $sql="INSERT INTO [UnusedResidualOverdraft]
            ([Customer_ID],[Month],[Reload],[Sales],[Unused],[Residual],[Overdraft])
            VALUES('$customer_id', $month, $reload, $sales,$unused,$residual,$overdraft);";
        fwrite($myfile, $sql."\n");
    }
    fclose($myfile);
    //sqlsrv_commit($dbh);
    if($res2)
        echo "Successful";
    else
        echo "Not successful";
    mssql_close($dbh);
}
function fncGetCustomers(){
    $dbh=fncOpenDBConn();
    $sql="select Customer_ID from prepaid_reload_meps group by customer_id";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function exportXLS($sql,$tablename){
    $dbh=fncOpenDBConn();
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $flag = false;
    $d=date('YmdHis');
    $filename="../downloads/$tablename$d.xls";
    $myfile = fopen($filename, "w") or die("Unable to open file!");
    $txt="";
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);
        if(!$flag) {
            $txt="<!DOCTYPE html>
                    <html>
                    <head>
                        <title>Exported data</title>
                        <meta charset='UTF-8'>
                    </head>
                    <body>
                        <table border=1>";
            fwrite($myfile, $txt);
            $style='style="background-color: #003366;color: white;text-align: left"';
            // display field/column names as first row
            $txt="<tr><th $style>".implode("</th><th $style>", array_keys($row)) . "</th></tr>";
            $txt=str_replace("_"," ",$txt);
            $flag = true;
            fwrite($myfile, $txt);
        }
        $txt="\r\n<tr><td>".implode("</td><td>", array_values($row)) . "</td></tr>";

        fwrite($myfile, $txt);
    }
    $txt="    </table>
                </body>
                </html>";
    fwrite($myfile, $txt);
    fclose($myfile);
    mssql_close($dbh);
    return $filename;
}
function exportHTML2XLS($html,$tablename){
    $d=date('YmdHis');
    $filename="../downloads/$tablename$d.xls";
    $myfile = fopen($filename, "w") or die("Unable to open file!");
    $txt="<!DOCTYPE html>
                    <html>
                    <head>
                        <title>Exported data</title>
                        <meta charset='UTF-8'>
                        <style>
                            .toprow td{
                                background-color: #003366;
                                color: white;
                                text-align: left
                            }
                        </style>
                    </head>
                    <body>
                $html
                </body>
                </html>";
    fwrite($myfile, $txt);
    fclose($myfile);
    return $filename;
}
function exportDataCollectionXLS($sql,$tablename){
    $dbh=fncOpenDBConn();
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $flag = false;
    $d=date('YmdHis');
    $filename="../downloads/$tablename$d.xls";
    $myfile = fopen($filename, "w") or die("Unable to open file!");
    $txt="";
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);

        if(!$flag) {
            $txt="<!DOCTYPE html>
                    <html>
                    <head>
                        <title>Exported data</title>
                        <meta charset='UTF-8'>
                        <style>
                        .uploaded{
                            font-family:Wingdings;
                            color:green;
                        }
                        .in-folder {
                            font-family: Wingdings;
                            color:darkgoldenrod;
                        }
                        .not-uploaded {
                            font-family: Wingdings;
                            color: red;
                        }
                        </style>
                    </head>
                    <body>
                        <table border=1>";
            fwrite($myfile, $txt);
            $style='style="background-color: #003366;color: white;text-align: left"';
            // display field/column names as first row
            $txt="<tr><th $style>".implode("</th><th $style>", array_keys($row)) . "</th></tr>";
            $txt=str_replace("_"," ",$txt);
            $flag = true;
            fwrite($myfile, $txt);
        }
        $txt="\r\n<tr><td>".implode("</td><td>", array_values($row)) . "</td></tr>";

        fwrite($myfile, $txt);
    }
    $txt="    </table>
                </body>
                </html>";
    fwrite($myfile, $txt);
    fclose($myfile);
    mssql_close($dbh);
    return $filename;
}
function fncGetMostPurchasedItems($month,$aggregate="",$rank=""){
    $dbh=fncOpenDBConn();
    $aggregate=$aggregate==""?"ITEM_DESCRIPTION_ENGLISH":$aggregate;
    $rank=$rank==""?"trans":$rank;
    $sql="SELECT Top 100 $aggregate as description,count(*) as trans,count(distinct customer_id) as bens,sum(quantity) as qty,
            sum(Total_Amount) as amount
            FROM  merchant_sales_$month t1 INNER JOIN
                         t03barcodes t2 ON t1.sku_code = t2.a03Code INNER JOIN
                         v_categoriesV2 t3 ON t2.a03family_id = t3.family_id
						 group by $aggregate
						 order by $rank desc";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}

function fncGetSummaryData($month){
    $dbh=fncOpenDBConn();
    $sql="select Top(1) * from summary where cycle=$month";
    $res = mssql_query($sql,$dbh);
    $row=mssql_fetch_array($res);
    return $row;
}
function fncGetTotalSalesByMonth($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $sql="";
    for($i=$startmonth;$i<=$endmonth;$i++){
        if($i==$startmonth)
            $sql="select $i as Month, sum(trans_amount) as Amount from sales_draft_$i ";
        else
            $sql.="union all select $i as Month, sum(trans_amount) as Amount from sales_draft_$i ";
    }
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetDuplicateRRN($month){
    $dbh=fncOpenDBConn();
    $sql="Select duplicate_rrns.occurences, COUNT(*) as no_of_times from (SELECT rrn, COUNT(rrn) AS occurences FROM sales_draft_$month
            GROUP BY rrn HAVING (((COUNT(rrn))>1))) as duplicate_rrns
            Group by occurences";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetDuplicateRRNCum($startmonth,$endmonth){
	$dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="Select duplicate_rrns.occurences, COUNT(*) as no_of_times from (SELECT rrn, COUNT(rrn) AS occurences FROM sales_draft_$month
            GROUP BY rrn HAVING (((COUNT(rrn))>1))) as duplicate_rrns
            Group by occurences";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
            $data[$c]["Month"]=$month;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetLastMonth(){
    $date=date("Y-m-d");
    $datestring="$date first day of last month";
    $datestring="$datestring first day of last month";
    $dt=date_create($datestring);
    return $dt->format('Ym'); //201102
}
function fncGetLastMonthCycle(){
    $date=date("Y-m-d");
    $datestring="$date first day of last month";
    $dt=date_create($datestring);
    return $dt->format('Ym'); //201102
}
function fncGetMerchantDiscounts(){
    $dbh=fncOpenDBConn();
    $sql="select discount_rate,count(*) as no_of_merchants from [merchants_wfp] group by discount_rate";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetPreviousMonth($thismonth){
    $date=substr($thismonth,0,4)."-".substr($thismonth,4,2)."-01";
    $datestring="$date first day of last month";
    $dt=date_create($datestring);
    return $dt->format('Ym'); //201102
}
function fncGet2MonthsBefore($thismonth){
    $date=substr($thismonth,0,4)."-".substr($thismonth,4,2)."-01";
    $dt=date("Ym", strtotime("-2 month", strtotime($date)));
    return $dt;
}
function fncGetPriceMonitoring(){
    $dbh=fncOpenDBConn();
    $sql="select * from price_monitoring_view";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantSalesSummary($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT  merchant,concat(merchant, '. ',wfp_name,' - ',branch, ' - ',[address]) as merchant_name,count(*) as no_of_sales,
	count(distinct customer_id) as no_of_customers, sum(Total_Amount) as total_amount
    from [merchant_sales_$month]
	inner join merchants_wfp on Merchant=merchants_wfp.id
	group by merchant,wfp_name,branch,[address]
	order by merchant_name";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetPriceMonitoringHorizontal(){
    $dbh=fncOpenDBConn();
    $sql="select * from price_monitoring_horizontal_view";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetTop20PerMerchantCummulative(){
   $dbh=fncOpenDBConn();
   $sql="SELECT merchants_wfp.ID, merchants_wfp.wfp_name, merchants_wfp.branch, merchants_wfp.address, Item_Description,
        Translation, Trans, AvgUnitPrice
        FROM            Top20PerMerchantTranslated INNER JOIN
        merchants_wfp ON Merchant = merchants_wfp.id order by ID ASC, Trans DESC";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantSalesTotalSummary($month){
    $dbh=fncOpenDBConn();
    $sql="select count(distinct merchant) as shops,count(distinct customer_id) as bens,count(*) as trans,
        sum(Total_Amount) as jod from merchant_sales_$month";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetFoodBasketData(){
	$dbh=fncOpenDBConn();
    $sql = "SELECT        TOP (100) PERCENT dbo.total_sale_values.Month, dbo.total_sale_values.Sales_Value,
            dbo.all_categories.all_categories, dbo.ex_egg_produce.ex_egg_produce, dbo.dairy_grocery_frozen.dairy_grocery_frozen,
            dbo.grocery.grocery, dbo.all_categories.all_categories / dbo.total_sale_values.Sales_Value * 100 AS all_sale_percent,
            dbo.all_categories.all_categories / dbo.all_categories.all_categories * 100 AS all_categories_percent
            FROM dbo.total_sale_values INNER JOIN
            dbo.dairy_grocery_frozen ON dbo.total_sale_values.Month = dbo.dairy_grocery_frozen.ShortMonth INNER JOIN
            dbo.all_categories ON dbo.total_sale_values.Month = dbo.all_categories.ShortMonth INNER JOIN
            dbo.ex_egg_produce ON dbo.total_sale_values.Month = dbo.ex_egg_produce.ShortMonth INNER JOIN
            dbo.grocery ON dbo.total_sale_values.Month = dbo.grocery.ShortMonth
            ORDER BY dbo.total_sale_values.Month";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $january_all_categories=1;
    $january_ex_egg_produce=1;
    $january_dairy_grocery_frozen=1;
    $january_grocery=1;

    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
        if($data[$i]["Month"]=="2016-01"){
            $january_all_categories=$data[$i]["all_categories"];
            $january_ex_egg_produce=$data[$i]["ex_egg_produce"];
            $january_dairy_grocery_frozen=$data[$i]["dairy_grocery_frozen"];
            $january_grocery=$data[$i]["grocery"];
        }
        $data[$i]["all_categories_percent"]=$data[$i]["all_categories"]/$january_all_categories*100;
        $data[$i]["ex_egg_produce_percent"]=$data[$i]["ex_egg_produce"]/$january_ex_egg_produce*100;
        $data[$i]["dairy_grocery_frozen_percent"]=$data[$i]["dairy_grocery_frozen"]/$january_dairy_grocery_frozen*100;
        $data[$i]["grocery_percent"]=$data[$i]["grocery"]/$january_grocery*100;
    }
    mssql_close($dbh);
    return $data;
}
function fncGetCampVsCommunity($month){
    $dbh=fncOpenDBConn();
    $sql="select 1 as desc_id,'safeway_camp' as type_of_shop,
        count(distinct barcode) as products,count(distinct customer_id) as bens, sum(total_amount) as amount_spent,
        sum(total_cost) as cog,sum(total_cost)/sum(total_amount)*100 as cog_percent,
        count(distinct merchant) as no_of_shops
		from [merchant_sales_$month] where merchant in (63)
union select 2 as desc_id,'safeway_community' as type_of_shop,
        count(distinct barcode) as products,count(distinct customer_id) as bens, sum(total_amount) as amount_spent,
        sum(total_cost) as cog,sum(total_cost)/sum(total_amount)*100 as cog_percent,
        count(distinct merchant) as no_of_shops
		from [merchant_sales_$month] where merchant in (53,54,55,56,57,58,59,61,62,64,99,100,101,102)
union select 3 as desc_id,'tazweed_camp' as type_of_shop,
        count(distinct barcode) as products,count(distinct customer_id) as bens, sum(total_amount) as amount_spent,
        sum(total_cost) as cog,sum(total_cost)/sum(total_amount)*100 as cog_percent,
        count(distinct merchant) as no_of_shops
		from [merchant_sales_$month] where merchant in (86)
union select 4 as desc_id,'sameh_mall_camp' as type_of_shop,
        count(distinct barcode) as products,count(distinct customer_id) as bens, sum(total_amount) as amount_spent,
        sum(total_cost) as cog,sum(total_cost)/sum(total_amount)*100 as cog_percent,
        count(distinct merchant) as no_of_shops
		from [merchant_sales_$month] where merchant in (68,69)";
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    $criteria=array("in (63)","in (53,54,55,56,57,58,59,61,62,64,99,100,101,102)",
        "in (86)","in (68,69)","not in (3,63,68,69,86)");
    $data=array();
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_array($res);
        $data[$row["type_of_shop"]]=$row;
        $locations=fncGetLocations($month,$criteria[$i-1]);
        $data[$row["type_of_shop"]]["locations"]=$locations[0];
        $data[$row["type_of_shop"]]["location_names"]=$locations[1];
	}
    $sql="select 5 as desc_id,'all_community' as type_of_shop,Retailer_ID, min(wfp_name) as shop_name,
        count(distinct barcode) as products,count(distinct customer_id) as bens, sum(total_amount) as amount_spent,
        sum(total_cost) as cog,sum(total_cost)/sum(total_amount)*100 as cog_percent,
        count(distinct merchant) as no_of_shops
		from [merchant_sales_$month] INNER JOIN merchants_wfp ON merchant=merchants_wfp.id
        where merchant not in (3,63,68,69,86)
        Group By Retailer_ID";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
        $locations=fncGetLocations($month,"",$row["Retailer_ID"]);
        $data[$i]["locations"]=$locations[0];
        $data[$i]["location_names"]=$locations[1];
	}
    mssql_close($dbh);
    return $data;
}
function fncGetLocations($month,$criteria,$criteria2=""){
    $dbh=fncOpenDBConn();
    if($criteria2!=""){
        $sql="select count(distinct concat(branch,[address])), (SELECT distinct concat(branch,' ',[address]) + '|' AS 'data()'
        FROM merchants_wfp where retailer_id='$criteria2' and id
        in (select merchant from merchant_sales_$month)
        FOR XML PATH('')) as t1 from merchants_wfp where retailer_id='$criteria2' and id
        in (select merchant from merchant_sales_$month)";
    }
    else{
        $sql="select count(distinct concat(branch,[address])), (SELECT distinct concat(branch,' ',[address]) + '|' AS 'data()'
        FROM merchants_wfp where id $criteria and id
        in (select merchant from merchant_sales_$month)
        FOR XML PATH('')) as t1 from merchants_wfp where id $criteria and id
        in (select merchant from merchant_sales_$month)";
    }
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    $data=array();
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_array($res);
        $data=$row;
        $data[1]=trim($data[1]);
        $data[1]=str_replace("|","<br />",$data[1]);
	}
    return $data;
}
function fncGetAveragePrices($month){
    $dbh=fncOpenDBConn();
    $retailers=fncGetRetailerString($month);
    $avgstring=str_replace("],[","]),avg([",$retailers);
    $avgstring="avg(".$avgstring.")";
    $sql="Select Barcode,Min(Item_Description) as Item_Description,$avgstring from
        (SELECT Barcode,Item_Description,$retailers
        FROM [dbo].[merchant_sales_$month]
            AS table1
        PIVOT
        (
            avg(unit_price)
        FOR
        [Merchant] in ($retailers)) AS ptable) as table2
        where Barcode in (SELECT [Barcode]
        FROM (select distinct barcode,merchant from merchant_sales_$month) as table1
        GROUP BY [Barcode]
        HAVING count(*)>1)
        Group by Barcode";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $data[0][1]=$sql;
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_row($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetAveragePricesGrouped($month){
    $dbh=fncOpenDBConn();
    $retailers=fncGetRetailerStringGrouped($month);
    $avgstring=str_replace("],[","]),avg([",$retailers);
    $avgstring="avg(".$avgstring.")";
    $sql="Select Barcode,Min(Item_Description) as Item_Description,$avgstring from
        (SELECT Barcode,Item_Description,unit_price,retailer_id
        FROM [dbo].[merchant_sales_$month] INNER JOIN merchants_wfp ON merchant=merchants_wfp.id)
            AS table1
        PIVOT
        (
            avg(unit_price)
        FOR
        [retailer_id] in ($retailers)) AS ptable
        where Barcode in (SELECT [Barcode]
        FROM (select distinct barcode,retailer_id from merchant_sales_$month inner join merchants_wfp on merchant=merchants_wfp.id) as table1
        GROUP BY [Barcode]
        HAVING count(*)>1)
        Group by Barcode";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $data[0][1]=$sql;
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_row($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetRetailerString($month){
    $dbh=fncOpenDBConn();
    $sql="select merchant from merchant_sales_$month where merchant is not null group by merchant";
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    $data="";
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_row($res);
        $data=$data.$row[0]."],[";
	}
    $data=trim($data,",[");
    $data="[".$data;
    return $data;
}
function fncGetRetailerStringGrouped($month){
    $dbh=fncOpenDBConn();
    $sql="select retailer_id from merchant_sales_$month INNER JOIN merchants_wfp
        ON merchant=merchants_wfp.id where merchant is not null group by retailer_id";
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    $data="";
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_row($res);
        $data=$data.$row[0]."],[";
	}
    $data=trim($data,",[");
    $data="[".$data;
    return $data;
}
function fncGetRetailers($month){
    $dbh=fncOpenDBConn();
    $sql="select merchant,concat(wfp_name,' - ',branch, ' - ',[address])
        as merchant_name from merchant_sales_$month
        inner join merchants_wfp on Merchant=merchants_wfp.id
        where merchant is not null group by merchant,concat(wfp_name,' - ',branch, ' - ',[address])";
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    $data=array();
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_row($res);
        $data[$row[0]]=$row[1];
	}
    return $data;
}
function fncGetRetailerGroups($month){
    $dbh=fncOpenDBConn();
    $sql="select retailer_id,min(wfp_name)
        as merchant_name from merchant_sales_$month
        inner join merchants_wfp on Merchant=merchants_wfp.id
        where merchant is not null
        group by retailer_id";
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    $data=array();
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_row($res);
        $data[$row[0]]=$row[1];
	}
    return $data;
}

function fncGetUploadStatusCum($startmonth,$endmonth){
	$dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql = "SELECT distinct $month as Month, merchant from merchant_sales_$month";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$month][$row["merchant"]]="upload";
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetFilesInfolder($startmonth,$endmonth){
	$dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql = "select distinct $month as Month, Triangulation_ID as merchant from retailer_uploads where Month=$month and status=1";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$month][$row["merchant"]]="infolder";
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetLatestSales($month){
	$dbh=fncOpenDBConn();
    $data=array();
    $sql = "SELECT Merchants_MEPS.Triangulation_ID, SUM(sales_draft_$month.Trans_Amount) AS sales_amount
            FROM     merchants_wfp INNER JOIN
                              Merchants_MEPS ON merchants_wfp.id = Merchants_MEPS.Triangulation_ID INNER JOIN
                              sales_draft_$month AS sales_draft_$month ON Merchants_MEPS.Terminal_ID = sales_draft_$month.terminal_id
                              where merchants_wfp.status=1
            GROUP BY Merchants_MEPS.Triangulation_ID";
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_array($res);
        $data[$row["Triangulation_ID"]]=$row["sales_amount"];
    }
    mssql_close($dbh);
    return $data;
}
function fncGetAllMerchants(){
    $dbh=fncOpenDBConn();
    $sql="select retailer_id,id,concat(wfp_name,' - ',branch, ' - ',[address]) as fullname from merchants_wfp where status=1 order by retailer_id";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetAveragePricesWFPNonWFP($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT *
    FROM (
        SELECT
            unit_price, RETAILER_SKU_CODE,
                  BARCODE, SKU_DESCRIPTION, [WFP Desc],
                  [AMMAN CHAIN], [AMMAN MEDIUM],
                  [AMMAN SMALL], [AMMAN SOUK], [IRBID CHAIN],
                  [IRBID MEDIUM], [IRBID SMALL],[IRBID SOUK],[MAFRAQ CHAIN],[MAFRAQ MEDIUM],
                  [MAFRAQ SMALL], [MAFRAQ SOUK], [ZARQA CHAIN],
                  [ZARQA MEDIUM], [ZARQA SMALL], [ZARQA SOUK],Merchant
    FROM (
            SELECT merchant_sales_$month.Merchant, AVG(merchant_sales_$month.Unit_Price) AS unit_price, RETAILER_SKU_CODE,
                  price_monitoring_non_wfp_crosstab_$month.BARCODE, SKU_DESCRIPTION, [WFP Desc],
                  [AMMAN CHAIN], [AMMAN MEDIUM],
                  [AMMAN SMALL], [AMMAN SOUK], [IRBID CHAIN],
                  [IRBID MEDIUM], [IRBID SMALL],[IRBID SOUK],[MAFRAQ CHAIN],[MAFRAQ MEDIUM],
                  [MAFRAQ SMALL], [MAFRAQ SOUK], [ZARQA CHAIN],
                  [ZARQA MEDIUM], [ZARQA SMALL], [ZARQA SOUK]
                FROM     price_monitoring_non_wfp_crosstab_$month INNER JOIN
                                  merchant_sales_$month ON price_monitoring_non_wfp_crosstab_$month.BARCODE = merchant_sales_$month.Barcode
                GROUP BY merchant_sales_$month.Merchant, RETAILER_SKU_CODE, price_monitoring_non_wfp_crosstab_$month.BARCODE,
                                  SKU_DESCRIPTION, [WFP Desc],
                  [AMMAN CHAIN], [AMMAN MEDIUM], [AMMAN SMALL],
                  [AMMAN SOUK], [IRBID CHAIN], [IRBID MEDIUM],
                  [IRBID SMALL],[IRBID SOUK], [MAFRAQ MEDIUM],[MAFRAQ CHAIN],[MAFRAQ SMALL],
                  [MAFRAQ SOUK], [ZARQA CHAIN], [ZARQA MEDIUM],
                  [ZARQA SMALL], [ZARQA SOUK]) as sourcetable
				  ) as s
            PIVOT
            (
    AVG(Unit_Price)
    FOR [Merchant] IN (".fncGetStringForWfpMerchantsVsNonWfp($month).")) AS pvt";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    $data[0][1]=$sql;
    return $data;
}
function fncGetStringForWfpMerchantsVsNonWfp($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT distinct Merchant
        FROM     price_monitoring_non_wfp_crosstab_$month INNER JOIN
         merchant_sales_$month ON price_monitoring_non_wfp_crosstab_$month.BARCODE
         = merchant_sales_$month.Barcode
        Order by Merchant";
    $res = mssql_query($sql,$dbh);
    $txt="[";
    $rows = mssql_num_rows($res);
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_row($res);
        $txt.=$row[0];
        if($i<$rows)
            $txt.="],[";
	}
    $txt.="]";
    mssql_close($dbh);
    return $txt;
}

function fncGetWfpMerchantsVsNonWfp($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT distinct Merchant,concat(retailer_id,': ',wfp_name,' - ',branch, ' - ',[address]) as fullname
        FROM     price_monitoring_non_wfp_crosstab_$month INNER JOIN
         merchant_sales_$month ON price_monitoring_non_wfp_crosstab_$month.BARCODE
         = merchant_sales_$month.Barcode
         inner join merchants_wfp
         on merchants_wfp.id=merchant_sales_$month.merchant
        Order by Merchant";
    $res = mssql_query($sql,$dbh);
    $data=array();
    $rows = mssql_num_rows($res);
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_array($res);
        $data[$row["Merchant"]]=$row["fullname"];
	}
    mssql_close($dbh);
    return $data;
}

function createTable($sql) {
    $dbh=fncOpenDBConn();
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $flag = false;
    $txt="";
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);
        if(!$flag) {
            $txt="<table>";
            $style='style="background-color: #003366;color: white;text-align: left"';
            $txt.="<tr><th $style>".implode("</th><th $style>", array_keys($row)) . "</th></tr>";
            $txt=str_replace("_"," ",$txt);
            $flag = true;
        }
        $txt.="\r\n<tr><td>".implode("</td><td>", array_values($row)) . "</td></tr>";
    }
    $txt.="    </table>";
    mssql_close($dbh);
    return $txt;
}
function fncGetMerchantSKUs($month){
    $dbh=fncOpenDBConn();
    $sql="select merchant,concat(wfp_name,' - ',branch, ' - ',[address]) as fullname,
    count(distinct item_description) as no_of_items from merchants_wfp inner join merchant_sales_$month
    on merchants_wfp.id=merchant_sales_$month.merchant
    group by merchant,concat(wfp_name,' - ',branch, ' - ',[address])
    order by merchant";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetMerchantChangedSKUs($month){
    $dbh=fncOpenDBConn();
    $sql="select t1.retailer_id,t2.merchant,concat(wfp_name,' - ',branch, ' - ',[address]) as fullname,
        count(*) as no_of_items from merchants_wfp t1 inner join
        (select merchant,barcode,item_description,min(unit_price) as min_price, max(unit_price) as max_price
        from merchant_sales_$month
        group by merchant,barcode,item_description
        having max(unit_price)-min(unit_price)<>0) t2 on t1.id=t2.merchant
        group by t1.retailer_id,t2.merchant,concat(wfp_name,' - ',branch, ' - ',[address])
        order by t1.retailer_id,t2.merchant";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetSKUDailyPricesSQL($month,$merchant){
    $daystring=fncGetDaysString($month,$merchant);
    $days=$daystring[0];
    $avgstring=$daystring[1];
    $sql="Select min(barcode) as Barcode,min(material_code) as SKU_Code,t1.item_description,round(avg(unit_price),2) "
        ." as Average_Price,sum(quantity) as Quantity,round(sum(total_amount),2) as Total_sale, t1.item_description as ID,$avgstring"
        ." from merchant_sales_$month as t1"
        ." inner join (SELECT Item_Description,$days"
        ." FROM [dbo].[merchant_sales_$month]"
        ." AS table1"
        ."    PIVOT"
        ."    ("
        ."        avg(unit_price)"
        ."    FOR"
        ."    [Trans_date] in ($days)) AS ptable"
        ."    where merchant=$merchant and Item_Description is not null) as t3 on t1.Item_Description=t3.Item_Description"
        ."     where merchant=$merchant and t1.item_description is not null"
        ."     group by t1.item_description";
    $result[0]=$sql;
    $result[1]=$days;
    return $result;
}

function fncGetChangedSKUDailyPricesSQL($month,$merchant){
    $daystring=fncGetDaysString($month,$merchant);
    $days=$daystring[0];
    $avgstring=$daystring[1];
    $sql="Select t1.Barcode,min(material_code) as SKU_Code,t1.item_description,round(avg(unit_price),2) "
        ." as Average_Price,sum(quantity) as Quantity,round(sum(total_amount),2) as Total_sale,$avgstring"
        ." from merchant_sales_$month as t1"
        ." inner join (SELECT Item_Description,$days"
        ." FROM [dbo].[merchant_sales_$month]"
        ." AS table1"
        ."    PIVOT"
        ."    ("
        ."        avg(unit_price)"
        ."    FOR"
        ."    [Trans_date] in ($days)) AS ptable"
        ."    where merchant=$merchant and Item_Description is not null) as t3 on t1.Item_Description=t3.Item_Description"
        ."     where merchant=$merchant and t1.item_description is not null"
        ."     group by t1.barcode, t1.item_description having max(unit_price)-min(Unit_Price)<>0";
    $result[0]=$sql;
    $result[1]=$days;
    return $result;
}


function fncGetDaysString($month,$merchant){
    $dbh=fncOpenDBConn();
    $sql="select distinct trans_date from merchant_sales_$month where merchant=$merchant order by trans_date";
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    $data="";
    $data2="";
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_row($res);
        $data=$data.$row[0]."],[";
        $data2=$data2.$row[0]."]),2) as [".$row[0]."],round(avg([";
	}
    $data=trim($data,",[");
    $data2=trim($data2,",round(avg([");
    $data="[".$data;
    $data2="round(avg([".$data2;
    $result[0]=$data;
    $result[1]=$data2;
    return $result;
}
function fncGetChainSKUs($month){
    $dbh=fncOpenDBConn();
    $sql="select Retailer_ID,min(wfp_name) as fullname,
    count(distinct item_description) as no_of_items from merchants_wfp inner join merchant_sales_$month
    on merchants_wfp.id=merchant_sales_$month.merchant
    group by Retailer_ID order by Retailer_ID";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetDataCollectionSummary($month){
    $dbh=fncOpenDBConn();
    $sql="SELECT retailer_branches.retailer_id,retailer_branches.retailer_name,
            retailer_branches.All_Branches, Retailer_Groups_with_Sales.branches_with_sales,
            retailers_submitting_grouped.Submitting
            FROM     retailer_branches LEFT OUTER JOIN
                              (SELECT retailer_id, Month, COUNT(*) AS Submitting
            FROM     dbo.retailers_submitting where month=$month
            GROUP BY retailer_id, Month) as retailers_submitting_grouped ON
            retailer_branches.retailer_id = retailers_submitting_grouped.retailer_id LEFT OUTER JOIN
                             (SELECT dbo.merchants_wfp.retailer_id, COUNT(DISTINCT dbo.merchants_meps.Triangulation_ID) AS branches_with_sales
            FROM     dbo.merchants_wfp INNER JOIN
                              dbo.merchants_meps ON dbo.merchants_wfp.id = dbo.merchants_meps.Triangulation_ID INNER JOIN
                              dbo.sales_draft_$month AS sales_draft_$month ON dbo.merchants_meps.Terminal_ID = sales_draft_$month.terminal_id
            GROUP BY dbo.merchants_wfp.retailer_id) as Retailer_Groups_with_Sales
            ON retailer_branches.retailer_id = Retailer_Groups_with_Sales.retailer_id";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
        $data[$i]["Not_Submitted"]=fncGetNotSubmitted($row["retailer_id"],$month);
	}
    mssql_close($dbh);
    return $data;
}
function fncGetNotSubmitted($retailer_id,$month){
    $dbh=fncOpenDBConn();
    $sql="select count(*) as number from merchants_wfp left outer join
        (SELECT merchants_wfp.id as branch_id, round(SUM(Trans_Amount),2) as sales_amount
        from sales_draft_$month INNER JOIN merchants_meps
        ON sales_draft_$month.terminal_id = merchants_meps.Terminal_ID INNER JOIN merchants_wfp
        ON merchants_meps.Triangulation_ID = merchants_wfp.id group by merchants_wfp.id)
        as sales_table on merchants_wfp.id=sales_table.branch_id where retailer_id='$retailer_id'
        and sales_amount>0 and id not in (select triangulation_id
        from retailers_submitting where month=$month)";
    $res = mssql_query($sql,$dbh);
    $row=mssql_fetch_array($res);
    return $row[0];
}
function fncGetShopProfile($id){
    $dbh=fncOpenDBConn();
    $sql="select WFP_Name,shop_profiles.* from shop_profiles inner join merchants_wfp on branch_id=merchants_wfp.id where Branch_ID=".$id;
    $res = mssql_query($sql,$dbh);
    $row=mssql_fetch_array($res);
    return $row;
}
function fncGetSameAmountReplication($month){
    $dbh=fncOpenDBConn();
    $sql="select Customer_ID,Trans_Amount,Trans_Date,
        terminal_id,count(*) as no_of_times from sales_draft_$month group by Customer_ID,
        Trans_Amount,Trans_Date,terminal_id having count(*)>1";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetWeightedPriceIndex($month){
    $dbh=fncOpenDBConn();
    $sql="Select retailer_id,Merchant,Concat(wfp_name,' - ',branch,' - ',address) as shop_name, Count(*) as Matching_Barcodes,
        Sum(Weighted_Shop_Price)*100.0/sum(Weighted_Market_Price) as Price_Index from (
        SELECT t1.alt_Barcode, price_monitoring_non_wfp_crosstab_$month.SKU_DESCRIPTION, t2.Weighted_Quantity, t1.Unit_Price,
                  t2.Weighted_Quantity * t1.Unit_Price AS Weighted_Shop_Price, price_monitoring_non_wfp_crosstab_$month.Non_WFP_Price,
                  price_monitoring_non_wfp_crosstab_$month.Non_WFP_Price * t2.Weighted_Quantity AS Weighted_Market_Price, t1.Merchant
        FROM     (SELECT $month as Month,alt_Barcode,Merchant, AVG(Unit_Price) AS Unit_Price
        FROM     dbo.merchant_sales_$month as s1
		inner join price_monitoring_non_wfp_crosstab_$month as s2 on s1.alt_barcode=s2.barcode
		where unit_price>minimum and unit_price<maximum
        GROUP BY alt_Barcode,Merchant) as t1 INNER JOIN
        price_monitoring_non_wfp_crosstab_$month ON t1.alt_Barcode = price_monitoring_non_wfp_crosstab_$month.BARCODE
		Inner join Weighted_Quantities as t2 on t1.Month=t2.Month and t1.alt_barcode=t2.barcode
        ) as summary
        inner join merchants_wfp on summary.Merchant=merchants_wfp.id
        group by retailer_id,Merchant,Concat(wfp_name,' - ',branch,' - ',address)
        Order by retailer_id";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetWeightedPriceIndexWFP($month){
    $dbh=fncOpenDBConn();
    $sql="Select retailer_id,Merchant,Concat(wfp_name,' - ',branch,' - ',address) as shop_name, Count(*) as Matching_Barcodes,
        Sum(Weighted_Shop_Price)*100.0/sum(Weighted_Market_Price) as Price_Index from (
        SELECT t1.alt_Barcode, t3.SKU_DESCRIPTION, t2.Weighted_Quantity, t1.Unit_Price,
                  t2.Weighted_Quantity * t1.Unit_Price AS Weighted_Shop_Price, t3.Avg_Price,
                  t3.Avg_Price * t2.Weighted_Quantity AS Weighted_Market_Price, t1.Merchant
        FROM     (SELECT $month as Month,alt_Barcode,Merchant, AVG(Unit_Price) AS Unit_Price
        FROM     dbo.merchant_sales_$month
        GROUP BY alt_Barcode,Merchant) as t1 INNER JOIN
        (select * from price_monitoring_wfp_prices where month=$month) as t3 ON t1.alt_Barcode = t3.BARCODE
		Inner join Weighted_Quantities as t2 on t1.Month=t2.Month and t1.alt_barcode=t2.barcode
        ) as summary
        inner join merchants_wfp on summary.Merchant=merchants_wfp.id
        group by retailer_id,Merchant,Concat(wfp_name,' - ',branch,' - ',address)
        Order by retailer_id";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetWeightedPriceIndexDetails($month,$merchant){
    $dbh=fncOpenDBConn();
    $sql="SELECT t1.alt_Barcode, price_monitoring_non_wfp_crosstab_$month.SKU_DESCRIPTION, t2.Weighted_Quantity, t1.Unit_Price,
                  t2.Weighted_Quantity * t1.Unit_Price AS Weighted_Shop_Price, price_monitoring_non_wfp_crosstab_$month.Non_WFP_Price,
                  price_monitoring_non_wfp_crosstab_$month.Non_WFP_Price * t2.Weighted_Quantity AS Weighted_Market_Price, t1.Merchant
            FROM     (SELECT $month as Month,alt_Barcode,Merchant, AVG(Unit_Price) AS Unit_Price
            FROM     dbo.merchant_sales_$month as s1
			inner join price_monitoring_non_wfp_crosstab_$month as s2 on s1.alt_barcode=s2.barcode
			where unit_price>minimum and unit_price<maximum
            and Merchant = $merchant
            GROUP BY alt_Barcode,Merchant) as t1 INNER JOIN
            price_monitoring_non_wfp_crosstab_$month ON t1.alt_Barcode = price_monitoring_non_wfp_crosstab_$month.BARCODE
			Inner join Weighted_Quantities as t2 on t1.Month=t2.Month and t1.alt_barcode=t2.barcode";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $sum_wfp=0;
    $sum_non_wfp=0;
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
        $barcode=$row["alt_Barcode"];
        $originalsSql="Select s1.Barcode,Item_Description, sum(quantity) as Shop_Quantity, round(avg(unit_price),2) as Average_Price
            from merchant_sales_$month  as s1
            inner join price_monitoring_non_wfp_crosstab_$month as s2 on s1.alt_barcode=s2.barcode
			where unit_price>minimum and unit_price<maximum
            and alt_barcode='$barcode' and merchant='$merchant'
            group by s1.barcode,item_description";
        $data[$i]["originals"]=createTable($originalsSql);
        $sum_wfp+=floatval($row["Weighted_Shop_Price"]);
        $sum_non_wfp+=floatval($row["Weighted_Market_Price"]);
	}
    $data[0]["sum_wfp"]=$sum_wfp;
    $data[0]["sum_non_wfp"]=$sum_non_wfp;
    $data[0]["price_index"]=$sum_wfp*100/$sum_non_wfp;
    mssql_close($dbh);
    return $data;
}
function fncGetWeightedPriceIndexDetailsWFP($month,$merchant){
    $dbh=fncOpenDBConn();
    $sql="SELECT t1.alt_Barcode, t3.SKU_DESCRIPTION, t2.Weighted_Quantity, t1.Unit_Price,
                  t2.Weighted_Quantity * t1.Unit_Price AS Weighted_Shop_Price, t3.Avg_Price,
                  t3.Avg_Price * t2.Weighted_Quantity AS Weighted_Market_Price, t1.Merchant
            FROM     (SELECT $month as Month,alt_Barcode,Merchant, AVG(Unit_Price) AS Unit_Price
            FROM     dbo.merchant_sales_$month
            WHERE  (Merchant = $merchant)
            GROUP BY alt_Barcode,Merchant) as t1
			INNER JOIN (select * from price_monitoring_wfp_prices where month=$month) as t3
            ON t1.alt_Barcode = t3.BARCODE
			Inner join Weighted_Quantities as t2 on t1.Month=t2.Month and t1.alt_barcode=t2.barcode";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $sum_wfp=0;
    $sum_all_wfp=0;
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
        $barcode=$row["alt_Barcode"];
        $originalsSql="Select Barcode,Item_Description, sum(quantity) as Shop_Quantity, round(avg(unit_price),2) as Average_Price
            from merchant_sales_$month where alt_barcode='$barcode' and merchant='$merchant'
            group by barcode,item_description";
        $data[$i]["originals"]=createTable($originalsSql);
        $sum_wfp+=floatval($row["Weighted_Shop_Price"]);
        $sum_all_wfp+=floatval($row["Weighted_Market_Price"]);
	}
    $data[0]["sum_wfp"]=$sum_wfp;
    $data[0]["sum_all_wfp"]=$sum_all_wfp;
    $data[0]["price_index"]=$sum_wfp*100/$sum_all_wfp;
    mssql_close($dbh);
    return $data;
}
function fncGetBeneficiaryUploadAuthentication($month){
    $dbh=fncOpenDBConn();
    $sql="select (
        select count(distinct beneficiary_id) from [dbo].[pre_paid_reload_wfp_$month]) as programme_reload,
        (select count(distinct customer_id) from [dbo].[pre_paid_reload_meps_$month]) as meps_reload,
        (SELECT count(*)
        FROM     pre_paid_reload_wfp_$month LEFT OUTER JOIN
                            pre_paid_reload_meps_$month ON pre_paid_reload_wfp_$month.Beneficiary_ID = pre_paid_reload_meps_$month.Customer_ID
        WHERE  (pre_paid_reload_meps_$month.Customer_ID IS NULL)) as programme_not_meps,
        (SELECT COUNT(*)
        FROM     pre_paid_reload_wfp_$month RIGHT OUTER JOIN
                            pre_paid_reload_meps_$month ON pre_paid_reload_wfp_$month.Beneficiary_ID = pre_paid_reload_meps_$month.Customer_ID
        WHERE  (pre_paid_reload_wfp_$month.Beneficiary_ID IS NULL)) as meps_not_programme,
        (SELECT count(*)
        FROM     pre_paid_reload_wfp_$month INNER JOIN
                            pre_paid_reload_meps_$month ON pre_paid_reload_wfp_$month.Beneficiary_ID = pre_paid_reload_meps_$month.Customer_ID
        where  pre_paid_reload_wfp_$month.Trans_Amount-pre_paid_reload_meps_$month.Trans_Amount<>0) as different_reloads";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data=$row;
    mssql_close($dbh);
    return $data;
}
function fncGetBeneficiaryVerification($ben_id,$month){
    $dbh=fncOpenDBConn();
    $sql="select (select max(household_size) from pre_paid_reload_wfp_$month where Beneficiary_ID='$ben_id') family_isze,
        (select max(card_number) from pre_paid_reload_meps_$month where Customer_ID='$ben_id') card_numbers,
        (select Trans_Amount
            from (select Customer_ID,Trans_date,Trans_Time,Trans_Amount,
                   row_number() over(partition by Customer_ID order by Trans_date desc,Trans_Time desc) as RowNum
                from sales_draft_$month where Customer_ID='$ben_id') cteRowNumber
            where RowNum = 1) last_amount,
        (select closingbalance from GeneralAccountBalances where Customer_ID='$ben_id' and cycle='$month')
        remaining_balance,
        (select dbo.group_concat(merchant) from sales_draft_$month where Customer_ID='$ben_id') shops_visited";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    $data=$row;
    mssql_close($dbh);
    return $data;
}
function fncGetVRLBankStatus(){
    $dbh=fncOpenDBConn();
    $sql="select 'All cards' as bank_status,count(*) as number from Finance_VRL
        union all select bank_status, count(*) as number from Finance_VRL
        group by bank_status";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetVRLFinanceStatus(){
    $dbh=fncOpenDBConn();
    $sql="select finance_status, count(*) as number from Finance_VRL
        group by finance_status";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetVRLDistributionStatus(){
    $dbh=fncOpenDBConn();
    $sql="select distribution_status, count(*) as number from Finance_VRL
        group by distribution_status";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetVRLProgrammeStatus(){
    $dbh=fncOpenDBConn();
    $sql="select programme_status, count(*) as number from Finance_VRL
        group by programme_status";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetShopSalesHistory($merchant){
    $dbh=fncOpenDBConn();
    $sql="select left(Trans_date,6) as Month,sum(Trans_Amount) as Amount
            from sales_draft t1 inner join merchants_meps t2 on t1.terminal_id=t2.terminal_id
            inner join merchants_wfp t3 on t2.triangulation_id=t3.id
            where t3.id=$merchant
            group by  left(Trans_date,6)
            order by Month";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $data[0][1] = 0;
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
        $data[0][1]+=$row["Amount"];
	}
    mssql_close($dbh);
    return $data;
}
function fncGetShopOSM($merchant){
    $dbh=fncOpenDBConn();
    $sql="select t1.*,concat(wfp_name, ' - ', branch, ' - ', address) as shop_name, t3.governorate as governorate_name,t4.Office
    from shop_monitoring_new t1
    left outer join merchants_wfp t2 on t1.retailer_id=t2.id
    left outer join Governorates t3 on t1.governorate=t3.id
    left outer join price_monitoring_offices t4 on t1.data_gatherer_office=t4.id
    where t1.retailer_id=".$merchant." Order by Data_Collection_Date";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_assoc($res);
        $data[$i]=$row;
    }
    mssql_close($dbh);
    return $data;
}
function fncGetOSMVariables(){
    $dbh=fncOpenDBConn();
    $sql="select var_name,var_label from shop_monitoring_variables";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$row["var_name"]]=$row["var_label"];
	}
    mssql_close($dbh);
    return $data;
}
function fncGetOSMValues(){
    $dbh=fncOpenDBConn();
    $sql="select * from shop_monitoring_values";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$row["var_name"].$row["value"]]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetCategorisationStatus(){
    $dbh=fncOpenDBConn();
    $sql="select t3.retailer_id,t1.merchant,concat(wfp_name,' - ',branch, ' - ',[address]) as shop_name,sales_value,all_barcodes,
        categorised_barcodes,all_barcodes-categorised_barcodes as remainder,categorised_barcodes*100/all_barcodes as categorisation_percent
        from (select merchant,count(distinct barcode) as all_barcodes, sum(Total_Amount) as sales_value from merchant_sales where barcode is not null group by merchant) as t1
        left join (select merchant,count(distinct t1.barcode) as categorised_barcodes from merchant_sales t1
		inner join t03barcodes t2 on t1.sku_code=t2.a03Code
		where t1.barcode is not null and sku_code is not null and a03family_id is not null and a03brand_id is not null group by merchant) as t2
        on t1.merchant=t2.Merchant
        inner join merchants_wfp as t3
        on t1.merchant=t3.id
        Order by t1.sales_value desc";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_assoc($res);
        $data[$i]=$row;
    }
    mssql_close($dbh);
    return $data;
}
function fncGetCategorisationTotals(){
    $dbh=fncOpenDBConn();
    $sql="select 'TOTAL' as total,count(distinct barcode) as total_barcodes,sum(Total_Amount) as total_sales,
        (select count(distinct t1.barcode) as categorised_barcodes from merchant_sales t1
		inner join t03barcodes t2 on t1.sku_code=t2.a03Code
		where sku_code is not null and a03family_id is not null and a03brand_id is not null) AS total_categorised
		FROM merchant_sales";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_assoc($res);
    $data=$row;
    mssql_close($dbh);
    return $data;
}
?>