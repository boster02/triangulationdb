<?php session_start();
      include("fncAnalytics.inc.php");
      //$startmonth=$_POST["year"]."01";
      //$endmonth=$_POST["year"].$_POST["month"];
      $data=fncGetFoodBasketData();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Food Basket Analysis</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" type="text/css"  href="../css/newtabs.css" />
</head>

<body>
    <ul id="globalnav">
        <li>
            <a href="food_basket_analysis.php" class="here">Safeway</a>
        </li>
        <ul></ul>
        <li>
            <a href="#">Tazweed</a>
        </li>
        <ul></ul>
    </ul>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table>
                        <tr>
                            <td style="text-align:left">Description</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td><?php echo fncGetMonthName(str_replace("-","",$data[$i]["Month"])) ?></td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>All</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["all_categories_percent"],1) ?>%
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>Ex Egg &amp; Produce</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["ex_egg_produce_percent"],1) ?>%
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>Grocery, Dairy &amp; Frozen</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["dairy_grocery_frozen_percent"],1) ?>%
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>Grocery</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["grocery_percent"],1) ?>%
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr style="background-color:#ccc">
                            <td></td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td></td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>All Sales Percent</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["all_sale_percent"],1) ?>%
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>Total SaleValues</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["Sales_Value"],2) ?>
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr style="background-color:#ccc">
                            <td></td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td>
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>All</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["all_categories"],2) ?>
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>Ex Egg &amp; Produce</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format( $data[$i]["ex_egg_produce"],2) ?>
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>Grocery, Dairy &amp; Frozen</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["dairy_grocery_frozen"],2) ?>
                            </td>
                            <?php }  ?>
                        </tr>
                        <tr>
                            <td>Grocery</td>
                            <?php  for($i=1;$i<=$data[0][0];$i++){ ?>
                            <td class="number">
                                <?php echo number_format($data[$i]["grocery"],2) ?>
                            </td>
                            <?php }  ?>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>Auth No.</th>
                                <th>Trans. Date</th>
                                <th>Customer ID</th>
                                <th>Merchant</th>
                                <th>Cashier ID</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month, duplicates) {
            if (month < 10) month = "0" + month;
            tablename = "(select Auth_no,Trans_Date,Customer_ID,Merchant,terminal_id,ROUND(Trans_Amount,2) AS Trans_Amount,ID from sales_draft_" + year + month
            + " where auth_no in (SELECT auth_no FROM sales_draft_" + year + month + " as tmp "
            + " GROUP BY auth_no HAVING COUNT(auth_no)=" + duplicates + "))";
            fields = "Auth_no,Trans_Date,Customer_ID,Merchant,terminal_id,Trans_Amount,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month, duplicates) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "select Auth_no,Trans_Date,Customer_ID,Merchant,terminal_id,ROUND(Trans_Amount,2) AS Trans_Amount from sales_draft_" + year + month
            + " where auth_no in (SELECT auth_no FROM sales_draft_" + year + month + " as tmp "
            + " GROUP BY auth_no HAVING COUNT(auth_no)=" + duplicates + ") Order by Auth_no";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=duplicate_authorization",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
