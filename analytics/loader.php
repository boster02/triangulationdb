<?php session_start();
      include("../config.inc.php");
      fncLogAccess2("Viewed report - ".$_GET["page"]);
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="../js/jquery-1.12.0.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/dataTables.tableTools.js"></script>
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="../css/dataTables.tableTools.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            getData(getURLParameter("page"), getURLParameter("month"), getURLParameter("year"))
        });
        function getData(p, pmonth, pyear) {
            var page = p;
            $('#loadingmessage').show();  // show the loading message.
            $.ajax({
                url: p,
                data: { month: pmonth, year: pyear },
                type: "POST",
                cache: false,
                success: function (html) {
                    $(".content").html(html);
                    $('#loadingmessage').hide(); // hide the loading message
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred while loading the report");
                }
            });
        }
        function getURLParameter(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null
        }
    </script>
</head>
<body>
    <div id='loadingmessage' style='display: none; width: 100%; text-align: center'>
        <img src='../images/download.gif' />
    </div>
    <div class="content"></div>
</body>
</html>
