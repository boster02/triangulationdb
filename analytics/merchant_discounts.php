<?php session_start();
      include("fncAnalytics.inc.php");
      $discounts=fncGetMerchantDiscounts();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Duplicate Authorization</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table style="width: 300px">
                        <tr>
                            <td class="number"><strong>Discount Rate</strong></td>
                            <td class="number"><strong>Number of Merchants</strong></td>
                        </tr>
                        <?php for($i=1;$i<=$discounts[0][0];$i++){ ?>
                        <tr>
                            <td class="number"><?php echo $discounts[$i]["discount_rate"] ?>%</td>
                            <td class="number"><a href="javascript:showDetails(<?php echo $discounts[$i]["discount_rate"] ?>)">
                                <?php echo $discounts[$i]["no_of_merchants"] ?>
                                </a>
                                <a href="javascript:exportDetails(<?php echo $discounts[$i]["discount_rate"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>Retailer ID</th>
                                <th>Account Name</th>
                                <th>WFP Name</th>
                                <th>Branch</th>
                                <th>Governorate</th>
                                <th>Contract Start Date</th>
                                <th>Contract End Date</th>
                                <th>Discount Rate</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(discount_rate) {
            tablename = "(Select retailer_id,acc_name,wfp_name,branch,governorate,contract_date_from,contract_date_to,discount_rate,id from merchants_wfp "
            + " where discount_rate = " + discount_rate+")";
            fields = "Retailer_ID,ACC_Name,WFP_Name,Branch,Governorate,Contract_Date_From,Contract_Date_To,Discount_Rate,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(discount_rate) {
            $('#loadingmessage').show();
            var sql = "Select retailer_id,acc_name,wfp_name,branch,governorate,contract_date_from,contract_date_to,discount_rate,id from merchants_wfp "
            + " where discount_rate = " + discount_rate;
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=discount_rate",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
