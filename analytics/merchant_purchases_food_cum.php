<?php session_start();
      include("fncAnalytics.inc.php");
      $startmonth=$_POST["year"]."01";
      $endmonth=$_POST["year"].$_POST["month"];
      $purchases=fncGetMerchantPurchasesFoodCum($startmonth,$endmonth);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Merchant Purchases Food Cum</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <div class="csstable" id="results">
        <?php if($purchases[0][0]==0){
                  echo "<div class='warning'>No data available for the selected month. Try a different month</div>";
              }
              else{ ?>
        <table id="tabela">
            <tr>
                <td>Month</td>
                <td>Food Group</td>
                <td>Number of Transactions</td>
                <td>Number of Customers</td>
                <td>Quantity</td>
                <td>Amount</td>
            </tr>
            <?php for($i=1;$i<=$purchases[0][0];$i++){ ?>
            <tr>
                <td rowspan="1" style="vertical-align:top">
                    <?php echo fncGetMonthName($purchases[$i]["Month"]) ?>
                </td>
                <td rowspan="1" class="number">
                    <?php echo $purchases[$i]["description"] ?>
                </td>
                <td rowspan="1" class="number">
                    <?php echo number_format($purchases[$i]["trans"]) ?>
                </td>
                <td rowspan="1" class="number">
                    <?php echo number_format($purchases[$i]["bens"]) ?>
                </td>
                <td rowspan="1" class="number">
                    <?php echo number_format($purchases[$i]["qty"]) ?>
                </td>
                <td rowspan="1" class="number">
                    <?php echo number_format($purchases[$i]["amount"],2) ?>
                </td>
            </tr>
            <?php } ?>
        </table>
        <?php } ?>
    </div>
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/tabletools.js"></script>
</body>
</html>