<?php session_start();
      include("fncAnalytics.inc.php");
     $purchases=fncGetMerchantPurchasesNFI($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Merchant Purchases - NFI</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <div class="csstable" id="results">
        <?php
 if($purchases[0][0]==0){
                  echo "<div class='warning'>No data available for the selected month. Try a different month</div>";
              }
              else{
        ?>
        <table>
            <tr>
                <td>Item</td>
                <td>Number of Transactions</td>
                <td>Number of Customers</td>
                <td>Quantity</td>
                <td>Amount</td>
            </tr>
            <?php for($i=1;$i<=$purchases[0][0];$i++){ ?>
            <tr>
                <td class="number">
                    <?php echo $purchases[$i]["description"] ?>
                </td>
                <td class="number">
                    <?php echo number_format($purchases[$i]["trans"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($purchases[$i]["bens"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($purchases[$i]["qty"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($purchases[$i]["amount"],2) ?>
                </td>
            </tr>
            <?php } ?>
        </table>
        <?php } ?>
    </div>
</body>
</html>