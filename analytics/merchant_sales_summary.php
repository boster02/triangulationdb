<?php session_start();
      header('Content-Type: text/html; charset=utf-8');
      include("fncAnalytics.inc.php");
      $sales=fncGetMerchantSalesSummary($_POST["year"].$_POST["month"]);
      $summary=fncGetMerchantSalesTotalSummary($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Merchant Sales Summary</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
</head>

<body>
    <h1>
        Summary for month <?php echo fncGetMonthName($_POST["year"].$_POST["month"]) ?>
    </h1>
    <div class="csstable">
        <table>
            <tr>
                <td>
                    Number of shops submitted data
                </td>
                <td>
                    Number of transactions
                </td>
                <td>
                    Number of beneficiaries
                </td>
                <td>
                    Amount in JOD
                </td>
            </tr>
            <?php for($i=1;$i<=$summary[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $summary[$i]["shops"] ?>
                </td>
                <td>
                    <?php echo number_format(floatval($summary[$i]["trans"])) ?>
                </td>
                <td>
                    <?php echo number_format(floatval($summary[$i]["bens"])) ?>
                </td>
                <td>
                    <?php echo number_format(floatval($summary[$i]["jod"]),2) ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <h1></h1>
    <h3>Details: 
        <a style="margin-top:10px" class="green-btn" href="javascript:exportSummary(<?php echo $_POST["year"].",".$_POST["month"] ?>)">Export this Report to Excel</a> <a style="margin-top:10px" class="green-btn" href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",0" ?>)">Export All Transactions to Excel</a></h3>
    <div class="csstable">
        <table>
            <tr>
                <td>
                    <strong>Merchant</strong>
                </td>
                <td class="number">
                    <strong>Number of Sales</strong>
                </td>
                <td class="number">
                    <strong>Number of Customers</strong>
                </td>
                <td class="number">
                    <strong>Amount</strong>
                </td>
            </tr>
            <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $sales[$i]["merchant_name"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales[$i]["merchant"] ?>)">
                        <?php echo number_format($sales[$i]["no_of_sales"]) ?>
                    </a>*
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales[$i]["merchant"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td class="number">
                    <?php echo number_format($sales[$i]["no_of_customers"]) ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales[$i]["total_amount"],2) ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div id="dialog" title="Sale details">
        <div id="detailscontainer" class="csstable-details">
            <table id="detailstable" class="display compact">
                <thead>
                    <tr>
                        <th>Trans Date</th>
                        <th>Trans Time</th>
                        <th>Receipt No</th>
                        <th>Customer ID</th>
                        <th>Barcode</th>
                        <th>SKU Code</th>
                        <th>SKU Description</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>VAT</th>
                        <th>Total Amount</th>
                        <th>Packaging</th>
                        <th>Payment Type</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.js"></script>

    <script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            autoOpen: false,
            maxWidth: 800,
            maxHeight: 500,
            width: 800,
            height: 500,
            modal: true
        });
    });
    function showDetails(year, month, merchant) {
        $("#dialog").dialog("open");
        if (month < 10) month = "0" + month;
        var tablename = "(Select Trans_Date,Trans_Time,Receipt_No,Customer_ID,Barcode,Material_Code,Item_Description,Quantity,Unit_Price,VAT,Total_Amount,Payment_Type,Packaging,ID from merchant_sales_" + year + month
        + " where merchant=" + merchant + ")";
        fields = "Trans_Date,Trans_Time,Receipt_No,Customer_ID,Barcode,Material_Code,Item_Description,Quantity,Unit_Price,VAT,Total_Amount,Packaging,Payment_Type,ID";
        $("#detailscontainer").show();
        $("#detailstable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
            "sPaginationType": "full_numbers"
        });
    }
    function exportSummary(year, month) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var sql = "SELECT  merchant,concat(merchant, '. ',wfp_name,' - ',branch, ' - ',[address]) as merchant_name, "
        + "count(*) as no_of_sales, "
        + "count(distinct customer_id) as no_of_customers, sum(Total_Amount) as total_amount "
        + "from merchant_sales_"+year+month
	    + " inner join merchants_wfp on Merchant=merchants_wfp.id "
	    + "group by merchant,wfp_name,branch,[address] "
	    + "order by merchant_name ";
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=merchant_sales",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }

    function exportDetails(year, month, merchant) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var sql = "select concat(merchant, '. ',wfp_name,' - ',branch, ' - ',[address]) as merchant_name,"
        +"Trans_Date,Trans_Time,Receipt_No,Customer_ID,Barcode,Material_Code,Item_Description,Quantity,"
        + "Unit_Price,VAT,Total_Amount,Payment_Type,Packaging from merchant_sales_" + year + month
        + " inner join merchants_wfp on Merchant=merchants_wfp.id"
        if (merchant > 0)
            sql += " where merchant=" + merchant;
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=merchant_sales",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
</body>
</html>
