<?php session_start();
      include("fncAnalytics.inc.php");
      $visits=fncGetMerchantVisits($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Merchant Visits</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table>
                        <tr>
                            <td><strong>No. of Customers</strong></td>
                            <td><strong>No. of Merchants</strong></td>
                        </tr>
                        <?php for($i=1;$i<=$visits[0][0];$i++){ ?>
                        <tr>
                            <td  class="number">
                                <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$visits[$i]["no_of_merchants"] ?>)">
                                    <?php echo $visits[$i]["no_of_customers"] ?></a>
                                <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$visits[$i]["no_of_merchants"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                            <td  class="number"><?php echo $visits[$i]["no_of_merchants"] ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th style="text-align:left">Customer ID</th>
                                <th style="text-align:left">No. of Merchants</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="shopscontainer" class="details csstable-details">
                    <table id="shopstable" class="display compact">
                        <thead>
                            <tr>
                                <th style="text-align:left">Shop Name</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month, shops) {
            if (month < 10) month = "0" + month;
            tablename = "(SELECT merchant_visits.Customer_ID,COUNT(merchant_visits.Merchant) as no_of_merchants,"
                        +"ROW_NUMBER() OVER (ORDER BY Customer_ID) as ID "
                        +"FROM (SELECT DISTINCT Customer_ID, Merchant "
                        +"FROM sales_draft_"+year+month+") AS merchant_visits "
                        +"GROUP BY merchant_visits.Customer_ID "
                        +"HAVING (((COUNT(merchant_visits.Merchant))="+shops+")))";
            fields = "Customer_ID,no_of_merchants,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $('td:eq(1)', nRow).html('<a href="javascript:showShops(\'' + year + '\',\'' + month + '\',\'' + aData[0] + '\')">'
                                        + aData[1] + '</a>');
                    return nRow;
                },
            });
        }
        function exportDetails(year, month, shops) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "SELECT merchant_visits.Customer_ID,COUNT(merchant_visits.Merchant) as no_of_merchants "
                        + "FROM (SELECT DISTINCT Customer_ID, Merchant "
                        + "FROM sales_draft_" + year + month + ") AS merchant_visits "
                        + "GROUP BY merchant_visits.Customer_ID "
                        + "HAVING (((COUNT(merchant_visits.Merchant))=" + shops + "))";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=merchant_visits",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
        function showShops(year,month,customer_id) {
            tablename = "(SELECT distinct Merchant, Merchant as ID "
                        + "FROM sales_draft_" + year + month + " where customer_id='" + customer_id + "')";
            fields = "Merchant,ID";
            $("#shopscontainer").show();
            $("#shopstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers",
            });
        }
    </script>
</body>
</html>
