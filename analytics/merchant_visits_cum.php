<?php session_start();
      include("fncAnalytics.inc.php");
      $startmonth=$_POST["year"]."01";
      $endmonth=$_POST["year"].$_POST["month"];
      $visits=fncGetMerchantVisitsCum($startmonth,$endmonth);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Merchant Visits Cum</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
        <table>
        <tr>
            <td style="vertical-align: top">
    <div class="csstable">
<table id="tabela">
  <tr>
    <th><strong>Month</strong></th>
    <th><strong>No. of Customers</strong></th>
    <th><strong>No. of Merchants</strong></th>
    </tr>
    <?php for($i=1;$i<=$visits[0][0];$i++){
              $pyear=substr($visits[$i]["Month"],0,4);
              $pmonth=substr($visits[$i]["Month"],4,2);
    ?>
  <tr>
    <td rowspan="1" class="number"><strong><?php echo fncGetMonthName($visits[$i]["Month"]) ?></strong></td>
    <td rowspan="1" class="number">
        <a href="javascript:showDetails(<?php echo $pyear.",".$pmonth.",".$visits[$i]["no_of_merchants"] ?>)">
            <?php echo $visits[$i]["no_of_customers"] ?></a>
        <a href="javascript:exportDetails(<?php echo $pyear.",".$pmonth.",".$visits[$i]["no_of_merchants"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
    </td>
    <td rowspan="1" class="number"><?php echo number_format($visits[$i]["no_of_merchants"]) ?></td>
  </tr>
    <?php } ?>  
</table>
        </div>
                            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>Customer ID</th>
                                <th>No. of Merchants</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month, shops) {
            if (month < 10) month = "0" + month;
            tablename = "(SELECT merchant_visits.Customer_ID,COUNT(merchant_visits.Merchant) as no_of_merchants,"
                        + "ROW_NUMBER() OVER (ORDER BY Customer_ID) as ID "
                        + "FROM (SELECT DISTINCT Customer_ID, Merchant "
                        + "FROM sales_draft_" + year + month + ") AS merchant_visits "
                        + "GROUP BY merchant_visits.Customer_ID "
                        + "HAVING (((COUNT(merchant_visits.Merchant))=" + shops + ")))";
            fields = "Customer_ID,no_of_merchants,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month, shops) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "SELECT merchant_visits.Customer_ID,COUNT(merchant_visits.Merchant) as no_of_merchants "
                        + "FROM (SELECT DISTINCT Customer_ID, Merchant "
                        + "FROM sales_draft_" + year + month + ") AS merchant_visits "
                        + "GROUP BY merchant_visits.Customer_ID "
                        + "HAVING (((COUNT(merchant_visits.Merchant))=" + shops + "))";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=unused",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>

    <script src="../js/tabletools.js"></script>
</body>
</html>