<?php session_start();
      include("fncAnalytics.inc.php");
      $sales=fncGetMerchantsWFPMEPS($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Merchants WFP vs. MEPS</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <div class="csstable">
        <a class="green-btn" href="javascript:exportDetails()">Export this Report to Excel</a>
        <br />
        <img style="height:1px" />
        <div id="report">
            <table border="1">
                <tr class="toprow">
                    <td>Retailer ID</td>
                    <td>Triangulation ID</td>
                    <td>Shop Name</td>
                    <td>MEPS Sales Value</td>
                    <td>Retailer Sales Value</td>
                    <td>Sales Value Variance</td>
                    <td>MEPS Transactions</td>
                    <td>Retailer Receipts</td>
                    <td>Transation Variance</td>
                </tr>
                <?php for($i=1;$i<=$sales[0][0];$i++){
                      $red="style='color:red'";
                      $sale_value_variance=floatval($sales[$i]["MEPS_Sales_Value"])-floatval($sales[$i]["Retailer_Sales_Value"]);
                      $trans_variance=floatval($sales[$i]["MEPS_Trans"])-floatval($sales[$i]["Retailer_Receipts"]);
                ?>
                <tr>
                    <td>
                        <?php echo $sales[$i]["Retailer_ID"] ?>
                    </td>
                    <td>
                        <?php echo $sales[$i]["Triangulation_ID"] ?>
                    </td>
                    <td>
                        <?php echo $sales[$i]["Shop_Name"] ?>
                    </td>
                    <td class="number">
                        <?php echo number_format($sales[$i]["MEPS_Sales_Value"],2) ?>
                    </td>
                    <td class="number">
                        <?php echo number_format($sales[$i]["Retailer_Sales_Value"],2) ?>
                    </td>
                    <td <?php if($sale_value_variance>0) echo $red  ?> class="number">
                        <?php echo number_format($sale_value_variance,2) ?>
                    </td>
                    <td class="number">
                        <?php echo number_format($sales[$i]["MEPS_Trans"]) ?>
                    </td>
                    <td class="number">
                        <?php echo number_format($sales[$i]["Retailer_Receipts"]) ?>
                    </td>
                    <td <?php if($trans_variance>0) echo $red  ?> class="number">
                        <?php echo number_format($trans_variance) ?>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function exportDetails() {
            debugger;
            $('#loadingmessage').show();
            var content = $("#report").html();
            debugger;
            $.ajax({
                url: "export_html.php",
                type: "POST",
                dataType: 'json',
                data: { tablename: 'wfp_vs_meps', html: content },
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>