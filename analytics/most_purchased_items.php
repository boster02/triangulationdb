<?php session_start();
      include("fncAnalytics.inc.php");
      $purchases=fncGetMostPurchasedItems($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Top 100 Most Purchased Items</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <style>
        body{
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }
        .warning {
            color:red;
            font-style:italic;
            text-align:center;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td valign="top">
                <form name="form1" id="form1">
                    <fieldset>
                        <legend>Level of Aggregation</legend>
                        <input type="radio" name="aggregate" value="item_description_english" onchange="changeAggregate()" checked="checked" />Item
                        <br />
                        <input type="radio" name="aggregate" value="family" onchange="changeAggregate()" />Family
                        <br />
                        <input type="radio" name="aggregate" value="sub_category" onchange="changeAggregate()" />Sub Category
                        <br />
                        <input type="radio" name="aggregate" value="category" onchange="changeAggregate()" />Category
                        <br />
                        <input type="radio" name="aggregate" value="food_group" onchange="changeAggregate()" />Food Group
                        <br />
                    </fieldset>
                    <br />
                    <fieldset>
                        <legend>Rank By</legend>
                        <input type="radio" name="rank" value="trans" onchange="changeAggregate()" checked="checked" />Number of Transactions
                        <br />
                        <input type="radio" name="rank" value="bens" onchange="changeAggregate()" />Number of Beneficiaries
                        <br />
                        <input type="radio" name="rank" value="qty" onchange="changeAggregate()" />Quantity of items
                        <br />
                        <input type="radio" name="rank" value="amount" onchange="changeAggregate()" />Monetory Value
                        <br />
                    </fieldset>
                </form>
            </td>
            <td valign="top">
                <div class="csstable" id="results">
                    <?php if($purchases[0][0]==0){
                  echo "<div class='warning'>No data available for the selected month. Try a different month</div>";
              }
              else{ ?>
                    <table>
                        <tr>
                            <td>#</td>
                            <td>Description</td>
                            <td>Number of Transactions</td>
                            <td>Number of Customers</td>
                            <td>Quantity</td>
                            <td>Amount</td>
                        </tr>
                        <?php for($i=1;$i<=$purchases[0][0];$i++){ ?>
                        <tr>
                            <td class="number">
                                <?php echo $i ?>
                            </td>
                            <td class="number">
                                <?php echo $purchases[$i]["description"] ?>
                            </td>
                            <td class="number">
                                <?php echo number_format($purchases[$i]["trans"]) ?>
                            </td>
                            <td class="number">
                                <?php echo number_format($purchases[$i]["bens"]) ?>
                            </td>
                            <td class="number">
                                <?php echo number_format($purchases[$i]["qty"]) ?>
                            </td>
                            <td class="number">
                                <?php echo number_format($purchases[$i]["amount"],2) ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                    <?php } ?>
                </div>
            </td>
        </tr>
    </table>
    <script>
        function changeAggregate() {
            $('#loadingmessage').show();
            var aggregate = form1.aggregate.value;
            var rank = form1.rank.value;
            var year='<?php echo $_POST["year"]?>';
            var month='<?php echo $_POST["month"]?>';
            $.get("most_purchased_items_partial.php?aggregate="+aggregate+"&rank="+rank+"&month="+month+"&year="+year, function (data, status) {
                $("#results").html(data);
                $('#loadingmessage').hide();
            });
        }
    </script>
</body>
</html>
