<?php session_start();
      include("fncAnalytics.inc.php");
      $purchases=fncGetMostPurchasedItems($_GET["year"].$_GET["month"],$_GET["aggregate"],$_GET["rank"]);
if($purchases[0][0]==0){
        echo "<div class='warning'>No data available for the selected month. Try a different month</div>";
    }
    else{ ?>
<table>
    <tr>
        <td>#</td>
        <td>Item Name</td>
        <td>Number of Transactions</td>
        <td>Number of Customers</td>
        <td>Quantity</td>
        <td>Amount</td>
    </tr>
    <?php for($i=1;$i<=$purchases[0][0];$i++){ ?>
    <tr>
        <td class="number">
            <?php echo $i ?>
        </td>
        <td class="number">
            <?php echo $purchases[$i]["description"] ?>
        </td>
        <td class="number">
            <?php echo number_format($purchases[$i]["trans"]) ?>
        </td>
        <td class="number">
            <?php echo number_format($purchases[$i]["bens"]) ?>
        </td>
        <td class="number">
            <?php echo number_format($purchases[$i]["qty"]) ?>
        </td>
        <td class="number">
            <?php echo number_format($purchases[$i]["amount"],2) ?>
        </td>
    </tr>
    <?php } ?>
</table>
<?php } ?>