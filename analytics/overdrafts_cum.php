<?php session_start();
      include("fncAnalytics.inc.php");
      $startmonth=$_POST["year"]."01";
      $endmonth=$_POST["year"].$_POST["month"];
      $overdrafts=fncGetOverdraftsCum($startmonth,$endmonth);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Overdrafts Cum</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table>
                        <tr>
                            <th>Period</th>
                            <th>No.    of Cards</th>
                            <th>Reload    Amount (JOD)s</th>
                            <th>Spent  Amount (JOD)s</th>
                            <th>Overdraft</th>
                        </tr>
                        <?php for($month=$startmonth;$month<=$endmonth;$month++){
                                  $pyear=substr($month,0,4);
                                  $pmonth=substr($month,4,2);                            
                        ?>
                        <tr>
                            <td><strong><?php echo fncGetMonthName($month) ?></strong></td>
                            <td class="number"><a href="javascript:showDetails(<?php echo $pyear.",".$pmonth ?>)">
                                <?php echo number_format($overdrafts[$month]["no_of_cards"]) ?></a>
                                <a href="javascript:exportDetails(<?php echo $pyear.",".$pmonth ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                            <td class="number"><?php echo number_format($overdrafts[$month]["reload_meps"],2) ?> </td>
                            <td class="number"><?php echo number_format($overdrafts[$month]["sales_amount"],2) ?> </td>
                            <td class="number"><?php echo number_format($overdrafts[$month]["overdraft"],2) ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Customer ID</th>
                                <th>Cum. Overdraft Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month) {
            if (month < 10) month = "0" + month;
            tablename = "(SELECT Customer_ID,Round(Sum(Overdraft)*-1,2) as Overdraft,ROW_NUMBER() OVER (ORDER BY Customer_ID) as ID "
            + "FROM UnusedResidualOverdraft "
            + "WHERE [Month]=" + year + month + " Group By Customer_ID "
            + "Having Round(Sum(Overdraft),2)<0)";
            fields = "Customer_ID,Overdraft,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "SELECT Customer_ID,Round(Sum(Overdraft)*-1,2) as Overdraft "
            + "FROM UnusedResidualOverdraft "
            + "WHERE [Month]=" + year + month + " Group By Customer_ID "
            + "Having Round(Sum(Overdraft),2)<0";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=overdrafts_cum",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>

</body>
</html>
