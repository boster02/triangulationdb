<?php session_start();
      header('Content-Type: text/html; charset=utf-8');
      include("fncAnalytics.inc.php");
      $sku=fncGetMerchantChangedSKUs($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>SKU List by Shop</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
</head>

<body>
    <div class="csstable">
        <a class="green-btn" href="javascript:exportSummary(<?php echo $_POST["year"].",".$_POST["month"] ?>)">Export All Items with Changed Prices</a>
        <br />
        <img style="height:1px" />
        <table>
            <tr>
                <td>
                    Retailer ID
                </td>
                <td>
                    Triangulation ID
                </td>
                <td>
                    Shop Name
                </td>
                <td>
                    SKU Items with Changed Prices
                </td>
            </tr>
            <?php for($i=1;$i<=$sku[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $sku[$i]["retailer_id"] ?>
                </td>
                <td class="number">
                    <?php echo $sku[$i]["merchant"] ?>
                </td>
                <td>
                    <?php echo $sku[$i]["fullname"] ?>
                </td>
                <td class="number">
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sku[$i]["merchant"] ?>)">
                        <?php echo number_format($sku[$i]["no_of_items"]) ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sku[$i]["merchant"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div id="dialog" title="Sale details">
        <div id="detailscontainer" class="csstable-details">
            <table id="detailstable" class="display compact">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>SKU Code</th>
                        <th>Item Description</th>
                        <th>Average Price</th>
                        <th>Quantity</th>
                        <th>Total Sale</th>
                        <?php
                        //echo $daysheader;
                        ?>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.js"></script>

    <script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            autoOpen: false,
            maxWidth: 800,
            maxHeight: 500,
            width: 800,
            height: 500,
            modal: true
        });
    });
    function showDetails(year, month, merchant) {
        debugger;
        $("#dialog").dialog("open");
        if (month < 10) month = "0" + month;
        $("#detailscontainer").show();
        var url = "sku_details_data_by_shop.php?year=" + year + "&month=" + month + "&merchant=" + merchant;
        console.log(url);
        $("#detailstable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": url,
            "sPaginationType": "full_numbers"
        });
    }

    function exportSummary(year, month) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var sql = "select t1.Retailer_ID,t1.id as Branch_ID,concat(wfp_name,' - ',branch, ' - ',[address]) as Shop_Name,Barcode,item_description,\
        round(min_price,2) as Minimun_Price,round(max_price,2) as Maximum_Price\
        from merchants_wfp t1 inner join\
        (select merchant,barcode,item_description,min(unit_price) as min_price, max(unit_price) as max_price\
        from merchant_sales_"+year+month+"\
        group by merchant,barcode,item_description\
        having max(unit_price)-min(unit_price)<>0) t2 on t1.id=t2.merchant\
        order by t1.retailer_id,t1.id";
        debugger;
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=changed_prices",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }

    function exportDetails(year, month, merchant) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        $.ajax({
            url: "export_sku_details_by_shop.php?year=" + year + "&month="+month+"&merchant="+merchant+"&changed=true",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
</body>
</html>
