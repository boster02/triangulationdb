<?php session_start();
      include("fncAnalytics.inc.php");
      $retailers=fncGetRetailerGroups($_POST["year"].$_POST["month"]);
      $prices=fncGetAveragePricesGrouped($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Merchant Purchases - NFI</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <div class="csstable">
        <br />
        <img style="height:1px" />
        <table>
            <tr>
                <td>Barcode</td>
                <td>Item Description</td>
                <?php foreach($retailers as $key => $value){ ?>
                <td>
                    <?php echo $retailers[$key] ?>
                </td>
                <?php } ?>
            </tr>
            <?php for($i=1;$i<=$prices[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $prices[$i][0] ?>
                </td>
                <td>
                    <?php echo $prices[$i][1] ?>
                </td>
                <?php for($j=1;$j<=count($retailers);$j++){ ?>
                <td class="number">
                    <?php if(floatval($prices[$i][$j+1])>0) echo number_format($prices[$i][$j+1],2) ?>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
        </table>
    </div>
    <script type="text/javascript">
    function exportDetails() {
        $('#loadingmessage').show();
        var sql = "<?php echo $prices[0][0] ?>";
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=sales_per_merchant",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
</body>
</html>