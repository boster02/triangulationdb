<?php
 session_start();
      include("fncAnalytics.inc.php");
      $retailers=fncGetWfpMerchantsVsNonWfp($_POST["year"].$_POST["month"]);
      $prices=fncGetAveragePricesWFPNonWFP($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Merchant Purchases - NFI</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <div class="csstable">
        <a class="green-btn" href="javascript:exportDetails()">Export this Report to Excel</a>
        <br />
        <img style="height:1px" />
        <div id="report">
            <table>
                <tr>
                    <td>#</td>
                    <td>RETAILER SKU CODE</td>
                    <td>BARCODE</td>
                    <td>SKU DESCRIPTION</td>
                    <td>WFP Desc</td>
                    <td>AMMAN CHAIN</td>
                    <td>AMMAN MEDIUM</td>
                    <td>AMMAN SMALL</td>
                    <td>AMMAN SOUK</td>
                    <td>IRBID CHAIN</td>
                    <td>IRBID MEDIUM</td>
                    <td>IRBID SMALL</td>
                    <td>IRBID SOUK</td>
                    <td>MAFRAQ CHAIN</td>
                    <td>MAFRAQ MEDIUM</td>
                    <td>MAFRAQ SMALL</td>
                    <td>MAFRAQ SOUK</td>
                    <td>ZARQA CHAIN</td>
                    <td>ZARQA MEDIUM</td>
                    <td>ZARQA SMALL</td>
                    <td>ZARQA SOUK</td>
                    <?php foreach($retailers as $key => $value){ ?>
                    <td>
                        <?php echo $retailers[$key] ?>
                    </td>
                    <?php } ?>
                </tr>
                <?php for($i=1;$i<=$prices[0][0];$i++){ ?>
                <tr>
                    <td>
                        <?php echo $i ?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["RETAILER_SKU_CODE"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["BARCODE"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["SKU_DESCRIPTION"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["WFP Desc"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["AMMAN CHAIN"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["AMMAN MEDIUM"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["AMMAN SMALL"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["AMMAN SOUK"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["IRBID CHAIN"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["IRBID MEDIUM"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["IRBID SMALL"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["IRBID SOUK"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["MAFRAQ CHAIN"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["MAFRAQ MEDIUM"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["MAFRAQ SMALL"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["MAFRAQ SOUK"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["ZARQA CHAIN"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["ZARQA MEDIUM"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["ZARQA SMALL"]?>
                    </td>
                    <td>
                        <?php echo $prices[$i]["ZARQA SOUK"]?>
                    </td>
                    <?php foreach($retailers as $key => $value){ ?>
                    <td class="number">
                        <?php if(floatval($prices[$i][$key])>0) echo number_format($prices[$i][$key],2) ?>
                    </td>
                    <?php } ?>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function exportDetails() {
            debugger;
        $('#loadingmessage').show();
        $.ajax({
            url: "export_html.php",
            type: "POST",
            dataType: 'json',
            data: { tablename: 'wfp_vs_non-wfp',html:$("#report").html() },
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
</body>
</html>