<?php session_start();
      include("fncAnalytics.inc.php");
      $data=fncGetPriceMonitoring();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Upload authentication</title>
    <link rel="stylesheet" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <a class="green-btn" href="javascript:exportDetails()">Export this Report to Excel</a>
    <div class="csstable">
        <img style="height:1px" />

        <table style="width: 500px">
            <tr>
                <td>Year</td>
                <td>Month</td>
                <td>Week</td>
                <td>Data Gatherer</td>
                <td>Data Gatherer Phone</td>
                <td>Retailer</td>
                <td>Retailer Type</td>
                <td>Governorate</td>
                <td>Item</td>
                <td>Availability</td>
                <td>Price</td>
                <td>price type</td>
            </tr>
            <?php for($i=1;$i<=$data[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $data[$i]["Year"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Month"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Week"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Data_Gatherer"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Data_Gatherer_Phone"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Retailer"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Retailer_Type"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Governorate"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Item"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Availability"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["Price"] ?>
                </td>
                <td>
                    <?php echo $data[$i]["price_type"] ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <script type="text/javascript">
        function exportDetails() {
            $('#loadingmessage').show();
            var sql = "SELECT * from price_monitoring_view";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=price_monitoring",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
