<?php session_start();
      include("fncAnalytics.inc.php");
      $data=fncGetPriceMonitoringHorizontal();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Upload authentication</title>
    <link rel="stylesheet" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <a class="green-btn" href="javascript:exportDetails()">Export this Report to Excel</a>
    <div class="csstable">
        <img style="height:1px" />

        <table style="width: 500px">
            <tr>
                <td>Year</td>
                <td>month</td>
                <td>week</td>
                <td>governorate</td>
                <td>retailer</td>
                <td>Availability of Veg. Oil, Corn</td>
                <td>Price of Veg. Oil, Corn</td>
                <td>Comments on Veg. Oil, Corn</td>
                <td>Availability of Olive Oil</td>
                <td>Price of Olive Oil</td>
                <td>Comments on Olive Oil</td>
                <td>Availability of Eggs, Hen, Fresh</td>
                <td>Price of Eggs, Hen, Fresh</td>
                <td>Comments on Eggs, Hen, Fresh</td>
                <td>Availability of Whole Chicken</td>
                <td>Price of Whole Chicken</td>
                <td>Comments on Whole Chicken</td>
                <td>Availability of Cheese, Canned</td>
                <td>Price of Cheese, Canned</td>
                <td>Comments on Cheese, Canned</td>
                <td>Availability of Lentils</td>
                <td>Price of Lentils</td>
                <td>Comments on Lentils</td>
                <td>Availability of Rice, Polished</td>
                <td>Price of Rice, Polished</td>
                <td>Comments on Rice, Polished</td>
                <td>Availability of Bulgur Wheat</td>
                <td>Price of Bulgur Wheat</td>
                <td>Comments on Bulgur Wheat</td>
                <td>Availability of Sugar</td>
                <td>Price of Sugar</td>
                <td>Comments on Sugar</td>
                <td>Availability of Cucumber</td>
                <td>Price of Cucumber</td>
                <td>Comments on Cucumber</td>
                <td>Availability of Pasta</td>
                <td>Price of Pasta</td>
                <td>Comments on Pasta</td>
                <td>Availability of Salt, Iodised</td>
                <td>Price of Salt, Iodised</td>
                <td>Comments on Salt, Iodised</td>
                <td>Availability of Powder Milk</td>
                <td>Price of Powder Milk</td>
                <td>Comments on Powder Milk</td>
                <td>Availability of Pasturized Milk</td>
                <td>Price of Pasturized Milk</td>
                <td>Comments on Pasturized Milk</td>
                <td>Availability of Dark Green Leaves</td>
                <td>Price of Dark Green Leaves</td>
                <td>Comments on Dark Green Leaves</td>
                <td>Availability of Tomatoes</td>
                <td>Price of Tomatoes</td>
                <td>Comments on Tomatoes</td>
                <td>Availability of Apples</td>
                <td>Price of Apples</td>
                <td>Comments on Apples</td>
                <td>Availability of Oranges</td>
                <td>Price of Oranges</td>
                <td>Comments on Oranges</td>
                <td>Availability of Chicken Parts</td>
                <td>Price of Chicken Parts</td>
                <td>Comments on Chicken Parts</td>
                <td>Availability of Canned Fish (Tuna)</td>
                <td>Price of Canned Fish (Tuna)</td>
                <td>Comments on Canned Fish (Tuna)</td>
                <td>Availability of Chickpeas</td>
                <td>Price of Chickpeas</td>
                <td>Comments on Chickpeas</td>
            </tr>
            <?php for($i=1;$i<=$data[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $data[$i][0] ?>
                </td>
                <td>
                    <?php echo $data[$i][1] ?>
                </td>
                <td>
                    <?php echo $data[$i][2] ?>
                </td>
                <td>
                    <?php echo $data[$i][3] ?>
                </td>
                <td>
                    <?php echo $data[$i][4] ?>
                </td>
                <td>
                    <?php echo $data[$i][5] ?>
                </td>
                <td>
                    <?php echo $data[$i][6] ?>
                </td>
                <td>
                    <?php echo $data[$i][7] ?>
                </td>
                <td>
                    <?php echo $data[$i][8] ?>
                </td>
                <td>
                    <?php echo $data[$i][9] ?>
                </td>
                <td>
                    <?php echo $data[$i][10] ?>
                </td>
                <td>
                    <?php echo $data[$i][11] ?>
                </td>
                <td>
                    <?php echo $data[$i][12] ?>
                </td>
                <td>
                    <?php echo $data[$i][13] ?>
                </td>
                <td>
                    <?php echo $data[$i][14] ?>
                </td>
                <td>
                    <?php echo $data[$i][15] ?>
                </td>
                <td>
                    <?php echo $data[$i][16] ?>
                </td>
                <td>
                    <?php echo $data[$i][17] ?>
                </td>
                <td>
                    <?php echo $data[$i][18] ?>
                </td>
                <td>
                    <?php echo $data[$i][19] ?>
                </td>
                <td>
                    <?php echo $data[$i][20] ?>
                </td>
                <td>
                    <?php echo $data[$i][21] ?>
                </td>
                <td>
                    <?php echo $data[$i][22] ?>
                </td>
                <td>
                    <?php echo $data[$i][23] ?>
                </td>
                <td>
                    <?php echo $data[$i][24] ?>
                </td>
                <td>
                    <?php echo $data[$i][25] ?>
                </td>
                <td>
                    <?php echo $data[$i][26] ?>
                </td>
                <td>
                    <?php echo $data[$i][27] ?>
                </td>
                <td>
                    <?php echo $data[$i][28] ?>
                </td>
                <td>
                    <?php echo $data[$i][29] ?>
                </td>
                <td>
                    <?php echo $data[$i][30] ?>
                </td>
                <td>
                    <?php echo $data[$i][31] ?>
                </td>
                <td>
                    <?php echo $data[$i][32] ?>
                </td>
                <td>
                    <?php echo $data[$i][33] ?>
                </td>
                <td>
                    <?php echo $data[$i][34] ?>
                </td>
                <td>
                    <?php echo $data[$i][35] ?>
                </td>
                <td>
                    <?php echo $data[$i][36] ?>
                </td>
                <td>
                    <?php echo $data[$i][37] ?>
                </td>
                <td>
                    <?php echo $data[$i][38] ?>
                </td>
                <td>
                    <?php echo $data[$i][39] ?>
                </td>
                <td>
                    <?php echo $data[$i][40] ?>
                </td>
                <td>
                    <?php echo $data[$i][41] ?>
                </td>
                <td>
                    <?php echo $data[$i][42] ?>
                </td>
                <td>
                    <?php echo $data[$i][43] ?>
                </td>
                <td>
                    <?php echo $data[$i][44] ?>
                </td>
                <td>
                    <?php echo $data[$i][45] ?>
                </td>
                <td>
                    <?php echo $data[$i][46] ?>
                </td>
                <td>
                    <?php echo $data[$i][47] ?>
                </td>
                <td>
                    <?php echo $data[$i][48] ?>
                </td>
                <td>
                    <?php echo $data[$i][49] ?>
                </td>
                <td>
                    <?php echo $data[$i][50] ?>
                </td>
                <td>
                    <?php echo $data[$i][51] ?>
                </td>
                <td>
                    <?php echo $data[$i][52] ?>
                </td>
                <td>
                    <?php echo $data[$i][53] ?>
                </td>
                <td>
                    <?php echo $data[$i][54] ?>
                </td>
                <td>
                    <?php echo $data[$i][55] ?>
                </td>
                <td>
                    <?php echo $data[$i][56] ?>
                </td>
                <td>
                    <?php echo $data[$i][57] ?>
                </td>
                <td>
                    <?php echo $data[$i][58] ?>
                </td>
                <td>
                    <?php echo $data[$i][59] ?>
                </td>
                <td>
                    <?php echo $data[$i][60] ?>
                </td>
                <td>
                    <?php echo $data[$i][61] ?>
                </td>
                <td>
                    <?php echo $data[$i][62] ?>
                </td>
                <td>
                    <?php echo $data[$i][63] ?>
                </td>
                <td>
                    <?php echo $data[$i][64] ?>
                </td>
                <td>
                    <?php echo $data[$i][65] ?>
                </td>
                <td>
                    <?php echo $data[$i][66] ?>
                </td>
                <td>
                    <?php echo $data[$i][67] ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <script type="text/javascript">
        function exportDetails() {
            $('#loadingmessage').show();
            var sql = "SELECT * from price_monitoring_horizontal_view";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=price_monitoring",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
