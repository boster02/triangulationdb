<?php session_start();
      include("fncAnalytics.inc.php");
      $sales=fncGetProcessingDateVariance($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Processing/Transaction Date Variance</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table>
                        <tr>
                            <td><strong>Number of days</strong></td>
                            <td><strong>No of Trans</strong></td>
                            <td><strong>Amount (JOD)</strong></td>
                            <td><strong>No. of Merchants</strong></td>
                        </tr>
                        <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
                        <tr>
                            <td class="number"><?php echo number_format($sales[$i]["Days"]) ?></td>
                            <td class="number">
                                <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales[$i]["Days"] ?>)">
                                    <?php echo number_format($sales[$i]["Trans"]) ?></a>
                                <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sales[$i]["Days"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                            <td class="number"><?php echo number_format($sales[$i]["Amount"],2) ?></td>
                            <td class="number"><?php echo $sales[$i]["Merchants"] ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact" style="width:100%">
                        <thead>
                            <tr>
                                <th>Trans. Date</th>
                                <th>Proc. Date</th>
                                <th>Customer ID</th>
                                <th>Merchant</th>
                                <th>Cashier ID</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month, days) {
            if (month < 10) month = "0" + month;
            tablename = "(select Trans_Date,Proc_Date,Customer_ID,Round(Trans_Amount,2) as Trans_Amount,Merchant,terminal_id,ID from sales_draft_" + year + month
            + " where datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))=" + days + ")";
            fields = "Trans_Date,Proc_Date,Customer_ID,Merchant,Trans_Amount,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month, days) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "select Trans_Date,Proc_Date,Customer_ID,Round(Trans_Amount,2) as Trans_Amount,Merchant,Terminal_ID from sales_draft_" + year + month
            + " where datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))=" + days;
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=processing_transation_date_variance",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
