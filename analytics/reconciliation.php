<?php session_start();
      include("fncAnalytics.inc.php");
      $reconciliation=fncGetReconciliation($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Overdrafts</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <style type="text/css">
        table.tableizer-table {
            font-size: 12px;
            border: 1px solid #CCC;
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
        }

        .tableizer-table td {
            padding: 4px;
            margin: 3px;
            border: 1px solid #808080;
        }

        .tableizer-table th {
            background-color: #104E8B;
            color: #FFF;
            font-weight: bold;
            border: 1px solid #FFF;
        }
    </style>
</head>

<body>
        <table class="tableizer-table">
            <tr>
                <th colspan="2">No. of Customers</th>
                <th>Opening Balance</th>
                <th colspan="4">Total Credit</th>
                <th colspan="4">Total Debit</th>
                <th>Balance</th>
                <th>Closing Balance</th>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;&nbsp;Transacted&nbsp;&nbsp;&nbsp;</th>
                <th>Non Transacted</th>
                <td rowspan="2" align="right"><?php echo number_format($reconciliation["OpeningBalance"],2) ?></td>
                <th>Reload</th>
                <th>Credit Voucher</th>
                <th>Credit Voucher (Sale)</th>
                <th>Pre-Paid Miscellaneous Credit</th>
                <th>Sales</th>
                <th>Pre-Paid Miscellaneous Debit</th>
                <th>Pre-Paid Redemption</th>
                <th style="border-right-style:solid;border-right-color:#ccc;border-right-width:2px">Cash ATM</th>
                <td rowspan="2" align="right"><?php echo number_format($reconciliation["Balance"],2) ?></td>
                <td rowspan="2" align="right">
                    <?php echo number_format($reconciliation["ClosingBalance"],2) ?>
                </td>

            </tr>
            <tr>
                <td>
                    <a href="javascript:showSummary(<?php echo $_POST["year"].",".$_POST["month"] ?>)">
                        <?php echo number_format($reconciliation["CustomersNumber"]) ?>
                    </a>
                    <a href="javascript:exportSummary(<?php echo $_POST["year"].",".$_POST["month"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
                <td align="right" style="border-right-style:solid;border-right-color:#ccc;border-right-width:2px">
                    <?php echo number_format($reconciliation["NonTransacted"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Pre-Paid Reload"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Credit Voucher"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Credit Voucher (Sale)"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Pre-Paid Miscellaneous Credit"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Sales Draft"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Pre-Paid Miscellaneous Debit"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Pre-Paid Redemption"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Cash ATM"],2) ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center"><?php
                    echo number_format($reconciliation["CustomersNumber"]+$reconciliation["NonTransacted"])?></td>
                <td align="right">
                    <?php echo number_format($reconciliation["OpeningBalance"],2) ?>
                </td>
                <td colspan="4" align="center"><?php echo number_format($reconciliation["Credit"],2) ?></td>
                <td colspan="4" align="center">
                    <?php echo number_format($reconciliation["Debit"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["Balance"],2) ?>
                </td>
                <td align="right">
                    <?php echo number_format($reconciliation["ClosingBalance"],2) ?>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td valign="top">
                    <div id="summarycontainer" class="details csstable-details">
                        <table id="summarytable" class="display compact" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Cycle</th>
                                    <th>Credit</th>
                                    <th>Debit</th>
                                    <th>Balance</th>
                                    <th>Opening Balance</th>
                                    <th>Closing Balance</th>
                                    <th>Customer ID</th>
                                    <th>Account Number</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </td>
                <td valign="top">
                    <div id="detailscontainer" class="details csstable-details">
                        <table id="detailstable" class="display compact" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Cycle</th>
                                    <th>Customer ID</th>
                                    <th>Credit</th>
                                    <th>Debit</th>
                                    <th>Transaction Type</th>
                                    <th>Trans Date</th>
                                    <th>Opening Balance</th>
                                    <th>Running Balance</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </td>
            </tr>
        </table>

    <script type="text/javascript">
        function showSummary(year, month) {
            if (month < 10) month = "0" + month;
            tablename = "(select cycle,credit,debit,Balance,OpenningBalance,ClosingBalance,Customer_ID,Account_Number, \
            Customer_ID as ID \
            from GeneralAccountBalances where cycle = '"+ year + month + "')";
            fields = "cycle,credit,debit,Balance,OpenningBalance,ClosingBalance,Customer_ID,Account_Number,ID";
            $("#summarycontainer").show();
            $("#summarytable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $('td:eq(6)', nRow).html('<a href="javascript:showDetails(\'' + aData[6] + '\')">' 
                                        + aData[6] + '</a> \
                                        <a href="javascript:exportDetails(\'' + aData[6] + '\')">' +
                                        '<img src="../images/exportxls.png" /></a>');
                    return nRow;
                },
            });
        }
        function exportSummary(year, month) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "select cycle,credit,debit,Balance,OpenningBalance,ClosingBalance,Customer_ID,Account_Number, \
            Customer_ID as ID \
            from GeneralAccountBalances where cycle = '" + year + month + "'";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=reconciliation",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
        function showDetails(id) {
            var tablename = "(Select *,ROW_NUMBER() OVER(ORDER BY cycle) as ID from OpeningBalances where Customer_ID = '" + id + "')";
            var fields = "cycle,Customer_ID,credit,debit,Transaction_Type,Trans_Date,OpenningBalance,CurrentBalance,ID";
            var url = "details_data.php?tablename=" + tablename + "&fields=" + fields;
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": url,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(id) {
            $('#loadingmessage').show();
            var sql = "select * from OpeningBalances where Customer_ID = '"+ id + "'";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=reconciliation",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>

</body>
</html>
