<?php session_start();
      include("fncAnalytics.inc.php");
      $startmonth=$_POST["year"]."01";
      $endmonth=$_POST["year"].$_POST["month"];
      $reconciliation=fncGetReconciliationCum($startmonth,$endmonth);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Overdrafts</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <div class="csstable">
        <table>
            <tr>
                <td>Month</td>
                <td>No. of Customers </td>
                <td>Reload Amount (JOD)s</td>
                <td>Spent Amount (JOD)s</td>
                <td>Variance</td>
                <td>&nbsp;</td>
                <td>Overdraft (JOD)s</td>
                <td>Residual (JOD)s</td>
                <td>Unused (JOD)s</td>
            </tr>
            <?php for($month=$startmonth;$month<=$endmonth;$month++){
                $pyear=substr($month,0,4);
                $pmonth=substr($month,4,2);
                ?>
            <tr>
                <td><strong><?php echo fncGetMonthName($month) ?></strong></td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $pyear.",".$pmonth ?>)">
                        <?php echo number_format($reconciliation[$month]["no_of_cards"]) ?></a>
                    <a href="javascript:exportDetails(<?php echo $pyear.",".$pmonth ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                </td>
                <td class="number"><?php echo number_format($reconciliation[$month]["reload_meps"],2) ?></td>
                <td class="number"><?php echo number_format($reconciliation[$month]["sales_amount"],2) ?> </td>
                <td class="number"><?php echo number_format($reconciliation[$month]["reload_meps"]-$reconciliation[$month]["sales_amount"],2) ?> </td>
                <td>&nbsp;</td>
                <td class="number"><?php echo number_format($reconciliation[$month]["overdraft"],2) ?></td>
                <td class="number"><?php echo number_format($reconciliation[$month]["residual"],2) ?></td>
                <td class="number"><?php echo number_format($reconciliation[$month]["unused"],2) ?></td>
            </tr>
            <?php } ?>
        </table>
    </div>
        <div id="detailscontainer" class="details csstable-details">
        <table id="detailstable" class="display compact" style="width: 100%">
            <thead>
                <tr>
                    <th>Customer ID</th>
                    <th>Reload</th>
                    <th>Sales</th>
                    <th>Variance</th>
                </tr>
            </thead>
        </table>
    </div>
    <script type="text/javascript">
        function showDetails(year, month) {
            if (month < 10) month = "0" + month;
            tablename = "(SELECT Customer_ID,Reload,round(Sales,2) as Sales,round(Variance,2) as Variance,Customer_ID as ID"
                + " FROM Reload_Sales_by_Month"
                + " WHERE cycle=" + year + month + ")";
            fields = "Customer_ID,Reload,Sales,Variance,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "SELECT Customer_ID,Reload,round(Sales,2) as Sales,round(Variance,2) as Variance "
                + " FROM Reload_Sales_by_Month"
                + " WHERE cycle=" + year + month;
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=reconciliation_cum",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>

</body>
</html>
