<?php session_start();
      include("fncAnalytics.inc.php");
      $residual=fncGetResidual($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Residual Balance</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table>
                        <tr>
                            <td><strong>Description</strong></td>
                            <td><strong>Amount    (JOD)</strong></td>
                        </tr>
                        <tr>
                            <td>No. of Cards </td>
                            <td class="number">
                                <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"] ?>)">
                                    <?php echo number_format($residual["no_of_cards"]) ?></a>
                                <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Reload Amount (JOD)s -    WFP </td>
                            <td class="number"><?php echo number_format($residual["reload_meps"],2) ?></td>
                        </tr>
                        <tr>
                            <td>Spent  Amount (JOD)s </td>
                            <td class="number"><?php echo number_format($residual["sales_amount"],2) ?> </td>
                        </tr>
                        <tr>
                            <td>Residual - WFP</td>
                            <td class="number"><?php echo number_format($residual["residual"],2) ?></td>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Customer ID</th>
                                <th>Account Number</th>
                                <th>Residual Balance</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month) {
            if (month < 10) month = "0" + month;
            tablename = "(select Customer_ID,Account_Number,ClosingBalance,Customer_ID as ID\
            from GeneralAccountBalances where cycle="+ year + month + " and ClosingBalance>0)";
            fields = "Customer_ID,Account_Number,ClosingBalance,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "select Customer_ID,Account_Number,ClosingBalance,Customer_ID as ID\
            from GeneralAccountBalances where cycle="+ year + month + " and ClosingBalance>0";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=residual",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
