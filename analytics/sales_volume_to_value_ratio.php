<?php session_start();
      include("fncAnalytics.inc.php");
      $sales=fncGetSalesValueVolumeRatio($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sales Volume Value Ratio</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table>
                        <tr>
                            <td>Retailer ID</td>
                            <td>Branch ID</td>
                            <td>Merchant</td>
                            <td>Sales Volume</td>
                            <td>Monetary Value</td>
                            <td>Ratio</td>
                            <td>Beneficiaries</td>
                            <td>Trans. per Beneficiary</td>
                            <td>Position</td>
                        </tr>
                        <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
                        <tr>
                            <td><?php echo $sales[$i]["retailer_id"] ?></td>
                            <td><?php echo $sales[$i]["branch_id"] ?></td>
                            <td><?php echo $sales[$i]["wfp_name"] ?></td>
                            <td class="number"><?php echo number_format($sales[$i]["Trans"]) ?></td>
                            <td class="number"><?php echo number_format($sales[$i]["Amount"],2) ?></td>
                            <td class="number"><?php echo number_format($sales[$i]["Ratio"]) ?></td>
                            <td class="number">
                                <?php echo number_format($sales[$i]["Bens"]) ?>
                            </td>
                            <td class="number">
                                <?php echo number_format($sales[$i]["Trans_Per_Ben"],2) ?>
                            </td>
                            <td class="number">
                                <?php echo number_format($sales[$i]["position"]) ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>Merchant</th>
                                <th>Sales Volume</th>
                                <th>Monetary Value</th>
                                <th>Ratio</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month, sales) {
            if (month < 10) month = "0" + month;
            tablename = "(SELECT Merchant, Trans, ROUND(Amount,2) AS Amount, ROUND(Trans/Amount,2) AS Ratio,ROW_NUMBER() OVER (ORDER BY Merchant) as ID "
            + " FROM (SELECT Merchant, COUNT(*) AS Trans, SUM(Trans_Amount) AS Amount "
            + " FROM sales_draft_" + year + month + " GROUP BY Merchant) "
            + " AS merchant_sales_" + year + month 
            + " WHERE round(Trans/Amount,2)=" + sales + ")";
            fields = "Merchant,Trans,Amount,Ratio,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month, sales) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "SELECT Merchant, Trans, ROUND(Amount,2) AS Amount, ROUND(Trans/Amount,2) AS Ratio "
            + " FROM (SELECT Merchant, COUNT(*) AS Trans, SUM(Trans_Amount) AS Amount "
            + " FROM sales_draft_" + year + month + " GROUP BY Merchant) "
            + " AS merchant_sales_" + year + month
            + " WHERE round(Trans/Amount,2)=" + sales;
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=sales_volume_to_value_ratio",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>

</body>
</html>
