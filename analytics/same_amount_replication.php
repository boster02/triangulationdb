<?php session_start();
      include("fncAnalytics.inc.php");
      //$sales=fncGetSameAmountReplication($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Same Amount Replication</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div id="mastercontainer" class="csstable-details" style="width:500px">
                    <table id="mastertable" class="display">
                        <thead>
                            <tr>
                                <th class="number">
                                    <strong>Customer ID</strong>
                                </th>
                                <th class="number">
                                    <strong>Trans. Amount</strong>
                                </th>
                                <th class="number">
                                    <strong>Date</strong>
                                </th>
                                <th class="number">
                                    <strong>Terminal ID</strong>
                                </th>
                                <th class="number">
                                    <strong>Number of Times</strong>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Customer ID</th>
                                <th>Amount</th>
                                <th>Merchant</th>
                                <th>Terminal ID</th>
                                <th>RRN</th>
                                <th>Auth No.</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        var year = <?php echo $_POST["year"] ?>;
        var month = <?php echo $_POST["month"] ?>;
        if (month < 10) month = "0" + month;
        $("#mastercontainer").show();
        tablename="(select ROW_NUMBER() OVER (Order by customer_Id) as ID,Customer_ID,round(Trans_Amount,2) as Trans_Amount,Trans_Date,\
        terminal_id,count(*) as no_of_times from sales_draft_"+year+month+" group by Customer_ID,\
        Trans_Amount,Trans_Date,terminal_id having count(*)>1)";
        fields = "Customer_ID,Trans_Amount,Trans_Date,terminal_id,no_of_times,ID";
        $("#mastertable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
            "sPaginationType": "full_numbers",
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $('td:eq(4)', nRow).html("<a href=javascript:showDetails(" + year+","+month+",'" + aData[0]+"','"
                             +aData[1]+"','"+aData[2]+"','"+aData[3]+"')>" + aData[4]+
                                    '</a>');
                return nRow;
            },
        });
        function showDetails(year, month, customer_id,trans_amount,trans_date,terminal_id) {
            if (month < 10) month = "0" + month;
            tablename = "(select Trans_Date,Trans_Time,Customer_ID,round(Trans_Amount,2) as Trans_Amount,Merchant,"
            +"terminal_id,rrn,auth_no,ID from sales_draft_"+year+month+" where Customer_ID='"+customer_id+"' and "
            +"round(Trans_Amount,2)="+trans_amount+" and Trans_Date='"+trans_date+"' and terminal_id='"+terminal_id+"')";
            fields = "Trans_Date,Trans_Time,Customer_ID,Trans_Amount,Merchant,terminal_id,rrn,auth_no,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
    </script>
</body>
</html>
