<?php
include("fncAnalytics.inc.php");
$results=fncGetShopOSM($_GET["id"]);
$var=fncGetOSMVariables();
$val=fncGetOSMValues();
?>
<div class="csstable">
    Number of OSM visits:<?php echo $results[0][0] ?>
    <table>
        <tr>
            <td style="vertical-align:bottom">Variable</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo "Visit ".$i ?>
            </td><?php } ?>
        </tr>

        <tr>
            <td style="vertical-align:bottom">Data collection date</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["data_collection_date"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Name of Data Gatherer</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["data_gatherer"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Phone number of Data Gatherer</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["data_gatherer_phone"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Office of the Data Gatherer</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["Office"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Email of theData Gathere</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["data_gatherer_email"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Governorate</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["governorate_name"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Name of the shop representative</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["shop_rep"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Status of the shop representative</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $val["shop_rep_type".$results[$i]["shop_rep_type"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">How much aware are you of WFP Cash Based Transfer and Retail Strategy programme</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["awareness".$results[$i]["awareness"]]["color"]?>">
                <?php echo $val["awareness".$results[$i]["awareness"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">In the last 2 months, how often have you been checking UNHCR beneficiary certificates</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["certificates".$results[$i]["certificates"]]["color"]?>">
                <?php echo $val["certificates".$results[$i]["certificates"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">In the last 2 months, how often have beneficiaries asked for NFIs</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["nfi".$results[$i]["nfi"]]["color"]?>">
                <?php echo $val["nfi".$results[$i]["nfi"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">In the last 2 months, how often have beneficiaries asked to cash their e-cards</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["cashing".$results[$i]["cashing"]]["color"]?>">
                <?php echo $val["cashing".$results[$i]["cashing"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">In the last 2 months, how often have beneficiaries presented fake vouchers/e-card</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["fake_vouchers".$results[$i]["fake_vouchers"]]["color"]?>">
                <?php echo $val["fake_vouchers".$results[$i]["fake_vouchers"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">In the last 2 months, how often have you experienced theft from beneficiaries</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["theft".$results[$i]["theft"]]["color"]?>">
                <?php echo $val["theft".$results[$i]["theft"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">In the last 2 months, how often have you experienced rudeness from beneficiaries</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["rudeness".$results[$i]["rudeness"]]["color"]?>">
                <?php echo $val["rudeness".$results[$i]["rudeness"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">In the last 2 months, how often have you had issues with the cooperating partner</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["cp".$results[$i]["cp"]]["color"]?>">
                <?php echo $val["cp".$results[$i]["cp"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Explain CP issues</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["cp_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">In the last 2 months, how often have you had issues with the bank</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["bank".$results[$i]["bank"]]["color"]?>">
                <?php echo $val["bank".$results[$i]["bank"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Explain bank issues</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["bank_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">How satisfied are you with the WFP CBT programme</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["satisfaction".$results[$i]["satisfaction"]]["color"]?>">
                <?php echo $val["satisfaction".$results[$i]["satisfaction"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Explain satisfaction</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["satisfaction_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Do you have any additional issues to let the programme know?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["retailer_additional"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">How many beneficiaries will you be able to interview</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["beneficiaries"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN1 - Are preferred items available?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["availability1".$results[$i]["availability1"]]["color"]?>">
                <?php echo $val["availability1".$results[$i]["availability1"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN1 - Which items are not available</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["availability1_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN1 - Whow do you rate the prices of essential commodities at this shop?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["prices1".$results[$i]["prices1"]]["color"]?>">
                <?php echo $val["prices1".$results[$i]["prices1"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN1 - Do you receive a receipt after your purchase?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["receipts1".$results[$i]["receipts1"]]["color"]?>">
                <?php echo $val["receipts1".$results[$i]["receipts1"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN1 - How accurate is the receipt?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["accuracy1".$results[$i]["accuracy1"]]["color"]?>">
                <?php echo $val["accuracy1".$results[$i]["accuracy1"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN1 - What are the issues with the receipt</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["accuracy1_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN1 - How far is the closest WFP Contracted Shop from your home?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["distance1".$results[$i]["distance1"]]["color"]?>">
                <?php echo $val["distance1".$results[$i]["distance1"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN1 - Do you have suggestions or recommendtaions for the programme?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["suggestions1"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN2 - Are preferred items available?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["availability2".$results[$i]["availability2"]]["color"]?>">
                <?php echo $val["availability2".$results[$i]["availability2"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN2 - Which items are not available</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["availability2_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN2 - Whow do you rate the prices of essential commodities at this shop?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["prices2".$results[$i]["prices2"]]["color"]?>">
                <?php echo $val["prices2".$results[$i]["prices2"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN2 - Do you receive a receipt after your purchase?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["receipts2".$results[$i]["receipts2"]]["color"]?>">
                <?php echo $val["receipts2".$results[$i]["receipts2"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN2 - How accurate is the receipt?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["accuracy2".$results[$i]["accuracy2"]]["color"]?>">
                <?php echo $val["accuracy2".$results[$i]["accuracy2"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN2 - What are the issues with the receipt</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["accuracy2_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN2 - How far is the closest WFP Contracted Shop from your home?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["distance2".$results[$i]["distance2"]]["color"]?>">
                <?php echo $val["distance2".$results[$i]["distance2"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN2 - Do you have suggestions or recommendtaions for the programme?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["suggestions2"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN3 - Are preferred items available?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["availability3".$results[$i]["availability3"]]["color"]?>">
                <?php echo $val["availability3".$results[$i]["availability3"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN3 - Which items are not available</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["availability3_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN3 - Whow do you rate the prices of essential commodities at this shop?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["prices3".$results[$i]["prices3"]]["color"]?>">
                <?php echo $val["prices3".$results[$i]["prices3"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN3 - Do you receive a receipt after your purchase?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["receipts3".$results[$i]["receipts3"]]["color"]?>">
                <?php echo $val["receipts3".$results[$i]["receipts3"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN3 - How accurate is the receipt?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["accuracy3".$results[$i]["accuracy3"]]["color"]?>">
                <?php echo $val["accuracy3".$results[$i]["accuracy3"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN3 - What are the issues with the receipt</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["accuracy3_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN3 - How far is the closest WFP Contracted Shop from your home?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["distance3".$results[$i]["distance3"]]["color"]?>">
                <?php echo $val["distance3".$results[$i]["distance3"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">BEN3 - Do you have suggestions or recommendtaions for the programme?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["suggestions3"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Commodities are stocked in a way than can affect the customers movement or safety?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["safety".$results[$i]["safety"]]["color"]?>">
                <?php echo $val["safety".$results[$i]["safety"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Explain safety</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["safety_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Overall shop cleanliness is:</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["cleanliness".$results[$i]["cleanliness"]]["color"]?>">
                <?php echo $val["cleanliness".$results[$i]["cleanliness"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">The backroom (storage) situation (Safety) is?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["backroom_safety".$results[$i]["backroom_safety"]]["color"]?>">
                <?php echo $val["backroom_safety".$results[$i]["backroom_safety"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Explain backroom safety</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["backroom_safety_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">The backroom (storage) situation (Tidiness, Cleanness) is:</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["backroom_cleanliness".$results[$i]["backroom_cleanliness"]]["color"]?>">
                <?php echo $val["backroom_cleanliness".$results[$i]["backroom_cleanliness"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">A shop employee is seen selling NFIs?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["selling_nfi".$results[$i]["selling_nfi"]]["color"]?>">
                <?php echo $val["selling_nfi".$results[$i]["selling_nfi"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">A shop employee is checking beneficiaries asylum seeker?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["asylum_checking".$results[$i]["asylum_checking"]]["color"]?>">
                <?php echo $val["asylum_checking".$results[$i]["asylum_checking"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Does the shop owner and the employees understand the project (all the elements)?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["understand_project".$results[$i]["understand_project"]]["color"]?>">
                <?php echo $val["understand_project".$results[$i]["understand_project"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">What elements do they not understand?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["understand_project_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Is WFP visibility available at the shop?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["visibility".$results[$i]["visibility"]]["color"]?>">
                <?php echo $val["visibility".$results[$i]["visibility"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">The cashier provides the beneficiaries with their receipts?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["gives_receipts".$results[$i]["gives_receipts"]]["color"]?>">
                <?php echo $val["gives_receipts".$results[$i]["gives_receipts"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Beneficiaries can check their cards balance any time for free?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["check_balance".$results[$i]["check_balance"]]["color"]?>">
                <?php echo $val["check_balance".$results[$i]["check_balance"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Noticed any bad behavior from the employees toward beneficiaries?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["bad_behaviour".$results[$i]["bad_behaviour"]]["color"]?>">
                <?php echo $val["bad_behaviour".$results[$i]["bad_behaviour"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Explain bad behaviour</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["bad_behaviour_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Number of customers in the checkout lane (The most crowded checkout if there is more than one):</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["checkout_customers".$results[$i]["checkout_customers"]]["color"]?>">
                <?php echo $val["checkout_customers".$results[$i]["checkout_customers"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Does the shop have any expired or defrosted items or any quality related issues? Please attach photos if possible.:</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["expired_goods".$results[$i]["expired_goods"]]["color"]?>">
                <?php echo $val["expired_goods".$results[$i]["expired_goods"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">The refrigeration conditions are:</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["refrigeration".$results[$i]["refrigeration"]]["color"]?>">
                <?php echo $val["refrigeration".$results[$i]["refrigeration"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Explain refrigeration</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["refrigeration_explain"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Which items are not available in the shop?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["unavailable_items"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Does the shop offer discounts for WFP Beneficiaries?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["discounts".$results[$i]["discounts"]]["color"]?>">
                <?php echo $val["discounts".$results[$i]["discounts"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Prices shown on the shelves?</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td class="<?php echo $val["prices_shown".$results[$i]["prices_shown"]]["color"]?>">
                <?php echo $val["prices_shown".$results[$i]["prices_shown"]]["value_label"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Number of male employees</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["male_employees"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Number of female employees</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["female_employees"] ?>
            </td><?php } ?>
        </tr>
        <tr>
            <td style="vertical-align:bottom">Additional information</td>
            <?php for ($i=1;$i<=$results[0][0];$i++) {?>
            <td>
                <?php echo $results[$i]["additional_info"] ?>
            </td><?php } ?>
        </tr>
    </table>
</div>
