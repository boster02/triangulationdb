<?php
include("fncAnalytics.inc.php");
$details=fncGetShopProfile($_GET["id"]);
?>

<table>
    <tr>
        <td valign="top">
            <div class="csstable">
                <table border="1" style="border-collapse:collapse">
                    <tr>
                        <td>Variable</td>
                        <td>Value</td>
                    </tr>
                    <tr>
                        <td>Shop WFP Name</td>
                        <td>
                            <?php echo $details["WFP_Name"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Shop MEPS Name</td>
                        <td>
                            <?php echo $details["Shop_Name"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Branch ID</td>
                        <td>


                            <?php echo $details["Branch_ID"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Type of shop</td>
                        <td>


                            <?php echo $details["Type_of_shop"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Ownership type</td>
                        <td>


                            <?php echo $details["Ownership_type"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Ownership gender</td>
                        <td>


                            <?php echo $details["Ownership_gender"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Shop Category </td>
                        <td>


                            <?php echo $details["Shop_Category"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Start date</td>
                        <td>


                            <?php echo $details["Start_date"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>End date</td>
                        <td>


                            <?php echo $details["End_date"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Governorate</td>
                        <td>


                            <?php echo $details["Governorate"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>District</td>
                        <td>


                            <?php echo $details["District"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td>


                            <?php echo $details["Area"] ?>
                        </td>
                    </tr>

                    <tr>
                        <td>GPS Coordinates - Latitude</td>
                        <td>


                            <?php echo $details["GPS_Coordinates_Latitude"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>GPS       Coordinates - Longitude</td>
                        <td>


                            <?php
                            echo $details["GPS_Coordinates_Longitude"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Phone Number</td>
                        <td>


                            <?php echo $details["Phone_Number"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td>


                            <?php echo $details["Email"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Cluster Reference</td>
                        <td>


                            <?php echo $details["Cluster_Reference"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Size</td>
                        <td>


                            <?php echo $details["Size"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Number of checkouts</td>
                        <td>


                            <?php echo $details["Number_of_checkouts"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Number of MEPS payment terminals</td>
                        <td>


                            <?php echo $details["Number_of_terminals"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Average number of customers per day</td>
                        <td>


                            <?php echo $details["Average_number_of_customers_per_day"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Average sales per day (JOD)</td>
                        <td>


                            <?php echo $details["Average_sales_per_day"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Number of SKUs</td>
                        <td>


                            <?php echo $details["Number_of_SKUs"] ?>
                        </td>
                    </tr>

                    <tr>
                        <td>Type of items sold</td>
                        <td>


                            <?php echo $details["Type_of_items_sold"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Number of employees (Males)</td>
                        <td>


                            <?php echo $details["Number_of_employees_Male"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Number of employees (Females)</td>
                        <td>


                            <?php echo $details["Number_of_employees_Female"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Percentage cost of goods sold to sales revenue (COG/Sales)</td>
                        <td>


                            <?php echo $details["COG_Percent"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Perceived customer rate</td>
                        <td>


                            <?php echo $details["Perceived_customer_rate"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>IT systems in place</td>
                        <td>


                            <?php echo $details["IT_systems_in_place"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Software name</td>
                        <td>


                            <?php echo $details["Software_name"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Perceived System Capability</td>
                        <td>


                            <?php echo $details["Perceived_System_Capability"] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Produce own barcode</td>
                        <td>


                            <?php echo $details["Produce_own_barcode"] ?>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
