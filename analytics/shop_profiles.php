<?php session_start();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Shop Profiles</title>
    <link rel="stylesheet" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <style>
        td {
            background-image: url('../images/spacer.gif');
            font-family: Calibri, 'Trebuchet MS', sans-serif;
        }

        th {
            background-image: url('../images/spacer.gif');
            font-family: Calibri, 'Trebuchet MS', sans-serif;
            font-size: 120%;
        }

        .btn-more {
            background: #3498db;
            background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
            background-image: -moz-linear-gradient(top, #3498db, #2980b9);
            background-image: -ms-linear-gradient(top, #3498db, #2980b9);
            background-image: -o-linear-gradient(top, #3498db, #2980b9);
            background-image: linear-gradient(to bottom, #3498db, #2980b9);
            -webkit-border-radius: 29;
            -moz-border-radius: 29;
            border-radius: 29px;
            font-family: Arial;
            color: #ffffff;
            font-size: 12px;
            padding: 3px 20px 3px 16px;
            text-decoration: none;
        }

            .btn-more:hover {
                background: #3cb0fd;
                background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
                background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
                background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
                background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
                background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
                text-decoration: none;
            }
    </style>
</head>

<body>
    <a style="float:right" class="green-btn" href="javascript:exportDetails()">Export to Excel</a>
    <br />
    <div id="detailscontainer">
        <table id="detailstable" class="display compact">
            <thead>
                <tr>
                    <th>SHOP PROFILES</th>
                </tr>
            </thead>
        </table>
    </div>
    <div id="dialog" title="Shop profile details">
        <div id="shop-details">
            
        </div>
    </div>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.js"></script>
    <script type="text/javascript">
        //debugger;
        fields = "ID,Shop_Name,Perceived_customer_rate,Type_of_shop,Ownership_type,Shop_Category,Area";
        url = "shop_details.php?fields=" + fields;
        nopicture = "nopicture";
        $("#detailscontainer").show();
        $("#detailstable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": url,
            "sPaginationType": "full_numbers",
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $('td:eq(0)', nRow).html("<table class='shop-table'> "
                 + " <tr>"
                    + " <td style='vertical-align:top'>"
                        + " <img src='" + pictureFile("../images/shops/" + aData[0] + ".jpg") + "' style='float:left;width:150px;margin-right:5px' />"
                    + " </td>"
                   + "  <td style='width:450px;' class='shop-table'>"
                        + " <strong style='font-size:120%'>" + aData[1] + "</strong><br />"
                        + " <img src='../images/rating/" + aData[2] + ".jpg' /><br />"
                        + " <strong>Type: </strong>" + aData[3] + "<br />"
                        + " <strong>Ownership Type: </strong>" + aData[4] + "<br />"
                        + " <strong>Category: </strong>" + aData[5] + "<br />"
                        + " <strong>Location: </strong>" + aData[6] + "<br /><br />"
                    + " </td>"
                    + " <td>"
                        + " <a class='btn-more' href='javascript:showDetails(" + aData[0] + ")'>Profile Details</a>"
                   + "  </td>"
                   + " <td>"
                        + " <a class='btn-more' href='javascript:showSalesHistory(" + aData[0] + ")'>Sales History</a>"
                   + "  </td>"
                   + " <td>"
                        + " <a class='btn-more' href='javascript:showOSM(" + aData[0] + ")'>On-site Monitoring</a>"
                   + "  </td>"
                + " </tr>"
            + " </table");
            }
        });
        function pictureFile(url) {
            debugger;
            var http = new XMLHttpRequest();
            http.open('HEAD', url, false);
            http.send();
            if (http.status != 404) {
                return url;
            }
            else {
                return "../images/shops/nopicture.jpg";
            }
        }
        $(function () {
            $("#dialog").dialog({
                autoOpen: false,
                maxWidth: 800,
                maxHeight: 500,
                width: 800,
                height: 500,
                modal: true
            });
        });
        function showDetails(id) {
            $("#dialog").dialog("open");
            $("#shop-details").html("<img src='../images/download.gif' />");
            $.get("shop_profile.php?id="+id, function (data, status) {
                $("#shop-details").html(data);
            });
        }
        function showSalesHistory(id) {
            $("#dialog").dialog("open");
            $("#shop-details").html("<img src='../images/download.gif' />");
            $.get("shop_sales_history.php?id=" + id, function (data, status) {
                $("#shop-details").html(data);
            });
        }
        function showOSM(id) {
            $("#dialog").dialog("open");
            $("#shop-details").html("<img src='../images/download.gif' />");
            $.get("shop_osm.php?id=" + id, function (data, status) {
                $("#shop-details").html(data);
            });
        }

        function exportDetails() {
            $('#loadingmessage').show();
            var sql = "select * from shop_profiles";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=shop_profiles",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
