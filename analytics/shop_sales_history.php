<?php
include("fncAnalytics.inc.php");
$sales=fncGetShopSalesHistory($_GET["id"]);
?>

<div class="csstable">
    <table border="1" style="border-collapse:collapse">
        <tr>
            <td>Month</td>
            <td>
                Sales
            </td>
        </tr>
        <?php for($i=1;$i<=$sales[0][0];$i++){?>
        <tr>
            <td>


                <?php echo fncGetMonthName($sales[$i]["Month"]) ?>
            </td>
            <td class="number">
                <?php echo number_format($sales[$i]["Amount"]) ?>
            </td>
        </tr>
        <?php } ?>
        <tr style="background-color:silver;font-weight:bold">
            <td>Total</td>
            <td class="number">
                <?php echo number_format($sales[0][1]) ?>
            </td>
        </tr>
    </table>
</div>


