<?php
header('Content-Type: text/html; charset=utf-8');
include("fncAnalytics.inc.php");
$year=isset($_GET["year"])?$_GET["year"]:0;
$month=isset($_GET["month"])?$_GET["month"]:0;
$month=str_pad($month,2,"0",STR_PAD_LEFT);
$merchant=isset($_GET["merchant"])?$_GET["merchant"]:0;
$result=fncGetSKUDailyPricesSQL($year.$month,$merchant);
$sql=$result[0];
$days=$result[1];
$daysheader=str_replace("],[","</th><th>",$days);
$daysheader=str_replace("[","<th>",$daysheader);
$daysheader=str_replace("]","</th>",$daysheader);
?>
<div id="detailscontainer" class="csstable-details">
    <table id="detailstable" class="display compact">
        <thead>
            <tr>
                <th>Barcode</th>
                <th>SKU Code</th>
                <th>Item Description</th>
                <th>Average Price</th>
                <th>Quantity</th>
                <th>Total Sale</th>
                <?php
                echo $daysheader;
                ?>
            </tr>
        </thead>
    </table>
</div>
<input type="hidden" id="year" value="<?php echo $year ?>" />
<input type="hidden" id="month" value="<?php echo $month ?>" />
<input type="hidden" id="sql" value="<?php echo $sql ?>" />
<input type="hidden" id="days" value="<?php echo $days ?>" />
<script>
    debugger;
    var year = $("#year").val();
    var month = $("#month").val();
    var sql = $("#sql").val();
    var days = $("#days").val();
    if (month < 10) month = "0" + month;
    var tablename = "("+sql+")";
    fields = "Barcode,SKU_Code,Item_Description,Average_Price,Quantity,Total_sale,"+days+",ID";
    $("#detailscontainer").show();
    url = "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields;
    $("#detailstable").DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "data.json",
        "sPaginationType": "full_numbers"
    });
</script>