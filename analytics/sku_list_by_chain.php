<?php
 session_start();
      header('Content-Type: text/html; charset=utf-8');
      include("fncAnalytics.inc.php");
      $sku=fncGetChainSKUs($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>SKU List by Shop</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
</head>

<body>
    <div class="csstable">
        <table>
            <tr>
                <td>
                    Chain ID
                </td>
                <td>
                    Shop Name
                </td>
                <td>
                    SKU Items
                </td>
            </tr>
            <?php for($i=1;$i<=$sku[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $sku[$i]["Retailer_ID"] ?>
                </td>
                <td>
                    <?php echo $sku[$i]["fullname"] ?>
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",'".$sku[$i]["Retailer_ID"]."'" ?>)">
                        <?php echo number_format($sku[$i]["no_of_items"]) ?>
                    </a>*
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",'".$sku[$i]["Retailer_ID"]."'" ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div id="dialog" title="Sale details">
        <div id="detailscontainer" class="csstable-details">
            <table id="detailstable" class="display compact">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>SKU_Code</th>
                        <th>item_description</th>
                        <th>Average_Price</th>
                        <th>Quantity</th>
                        <th>Total_sale</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.js"></script>

    <script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            autoOpen: false,
            maxWidth: 800,
            maxHeight: 500,
            width: 800,
            height: 500,
            modal: true
        });
    });
    function showDetails(year, month, merchant) {
        $("#dialog").dialog("open");
        if (month < 10) month = "0" + month;
        var tablename = "(Select min(barcode) as Barcode,min(material_code) as SKU_Code,item_description,avg(unit_price) "
            +"as Average_Price,sum(quantity) as Quantity,sum(total_amount) as Total_sale, item_description as ID "
            + "from merchant_sales_" + year + month + " inner join merchants_wfp on merchant=merchants_wfp.id where retailer_id='" + merchant + "' and item_description is not null "
            +"group by item_description)";
        fields = "Barcode,SKU_Code,Item_Description,Average_Price,Quantity,Total_sale,ID";
        $("#detailscontainer").show();
        $("#detailstable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
            "sPaginationType": "full_numbers"
        });
    }

    function exportDetails(year, month, merchant) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var sql = "Select min(barcode) as Barcode,min(material_code) as SKU_Code,item_description,avg(unit_price) "
            +"as Average_Price,sum(quantity) as Quantity,sum(total_amount) as Total_sale, item_description as ID "
            + "from merchant_sales_" + year + month + " inner join merchants_wfp on merchant=merchants_wfp.id where retailer_id='" + merchant + "' and item_description is not null "
            +"group by item_description";
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=merchant_sku_list",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
</body>
</html>
