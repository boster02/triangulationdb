<?php session_start();
      header('Content-Type: text/html; charset=utf-8');
      include("fncAnalytics.inc.php");
      $sku=fncGetMerchantSKUs($_POST["year"].$_POST["month"]);
      //$result=fncGetSKUDailyPricesSQL($_POST["year"].$_POST["month"],103);
      //$sql=$result[0];
      //$days=$result[1];
      //$daysheader=str_replace("],[","</th><th>",$days);
      //$daysheader=str_replace("[","<th>",$daysheader);
      //$daysheader=str_replace("]","</th>",$daysheader);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>SKU List by Shop</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <div class="csstable">
        <table>
            <tr>
                <td>
                    Triangulation ID
                </td>
                <td>
                    Shop Name
                </td>
                <td>
                    SKU Items
                </td>
            </tr>
            <?php for($i=1;$i<=$sku[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $sku[$i]["merchant"] ?>
                </td>
                <td>
                    <?php echo $sku[$i]["fullname"] ?>
                </td>
                <td class="number">
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sku[$i]["merchant"] ?>)">
                        <?php echo number_format($sku[$i]["no_of_items"]) ?>
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$sku[$i]["merchant"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div id="dialog" title="Sale details">
        <div id="detailscontainer" class="csstable-details">
            <table id="detailstable" class="display compact">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>SKU Code</th>
                        <th>Item Description</th>
                        <th>Average Price</th>
                        <th>Quantity</th>
                        <th>Total Sale</th>
                        <?php
                        //echo $daysheader;
                        ?>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.js"></script>

    <script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            autoOpen: false,
            maxWidth: 800,
            maxHeight: 500,
            width: 800,
            height: 500,
            modal: true
        });
    });
    function showDetails(year, month, merchant) {
        debugger;
        $("#dialog").dialog("open");
        if (month < 10) month = "0" + month;
        $("#detailscontainer").show();
        var url = "sku_details_data_by_shop.php?year=" + year + "&month=" + month + "&merchant=" + merchant;
        console.log(url);
        $("#detailstable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": url,
            "sPaginationType": "full_numbers"
        });
    }

    function exportDetails(year, month, merchant) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        $.ajax({
            url: "export_sku_details_by_shop.php?year=" + year + "&month="+month+"&merchant="+merchant,
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
</body>
</html>
