{
  "draw": 2,
  "recordsTotal": 57,
  "recordsFiltered": 57,
  "data": [
    {
      "DT_RowId": "row_4",
      "first_name": "Garrett",
      "last_name": "Winters",
      "position": "Accountant",
      "office": "Tokyo",
      "start_date": "25th Jul 11",
      "salary": "$170,750"
    },
    {
      "DT_RowId": "row_7",
      "first_name": "Airi",
      "last_name": "Satou",
      "position": "Accountant",
      "office": "Tokyo",
      "start_date": "28th Nov 08",
      "salary": "$162,700"
    },
    {
      "DT_RowId": "row_25",
      "first_name": "Angelica",
      "last_name": "Ramos",
      "position": "Chief Executive Officer (CEO)",
      "office": "London",
      "start_date": "9th Oct 09",
      "salary": "$1,200,000"
    },
    {
      "DT_RowId": "row_17",
      "first_name": "Paul",
      "last_name": "Byrd",
      "position": "Chief Financial Officer (CFO)",
      "office": "New York",
      "start_date": "9th Jun 10",
      "salary": "$725,000"
    },
    {
      "DT_RowId": "row_22",
      "first_name": "Yuri",
      "last_name": "Berry",
      "position": "Chief Marketing Officer (CMO)",
      "office": "New York",
      "start_date": "25th Jun 09",
      "salary": "$675,000"
    },
    {
      "DT_RowId": "row_29",
      "first_name": "Fiona",
      "last_name": "Green",
      "position": "Chief Operating Officer (COO)",
      "office": "San Francisco",
      "start_date": "11th Mar 10",
      "salary": "$850,000"
    },
    {
      "DT_RowId": "row_57",
      "first_name": "Donna",
      "last_name": "Snider",
      "position": "Customer Support",
      "office": "New York",
      "start_date": "25th Jan 11",
      "salary": "$112,000"
    },
    {
      "DT_RowId": "row_47",
      "first_name": "Serge",
      "last_name": "Baldwin",
      "position": "Data Coordinator",
      "office": "Singapore",
      "start_date": "9th Apr 12",
      "salary": "$138,575"
    },
    {
      "DT_RowId": "row_26",
      "first_name": "Gavin",
      "last_name": "Joyce",
      "position": "Developer",
      "office": "Edinburgh",
      "start_date": "22nd Dec 10",
      "salary": "$92,575"
    },
    {
      "DT_RowId": "row_32",
      "first_name": "Suki",
      "last_name": "Burks",
      "position": "Developer",
      "office": "London",
      "start_date": "22nd Oct 09",
      "salary": "$114,500"
    }
  ]
}