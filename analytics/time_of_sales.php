<?php session_start();
      include("fncAnalytics.inc.php");
      $sales=fncGetTimeOfSales($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Time of Sales</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table>
                        <tr>
                            <td>#</td>
                            <td>Period</td>
                            <td>No. of trans</td>
                            <td>Value (JOD) s</td>
                        </tr>
                        <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $sales[$i]["period"] ?> </td>
                            <td class="number">
                                <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$i ?>)">
                                    <?php echo number_format($sales[$i]["Trans"]) ?></a> 
                                <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$i ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                            <td class="number"><?php echo number_format($sales[$i]["Amount"],2) ?> </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact">
                        <thead>
                            <tr>
                                <th>Trans. Date</th>
                                <th>Trans. Time</th>
                                <th>Customer ID</th>
                                <th>Merchant</th>
                                <th>Cashier ID</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month, criteria) {
            switch (criteria) {
                case 1:
                    filter = " where left(Trans_Time,2)>='00' and left(Trans_Time,2)<='05'";
                    break;
                case 2:
                    filter = " where left(Trans_Time,2)>='06' and left(Trans_Time,2)<='11'";
                    break;
                case 3:
                    filter = " where left(Trans_Time,2)>='12' and left(Trans_Time,2)<='17'";
                    break;
                case 4:
                    filter = " where left(Trans_Time,2)>='18' and left(Trans_Time,2)<='23'";
                    break;
            }

            if (month < 10) month = "0" + month;
            tablename = "(select Trans_Date,Trans_Time,Customer_ID,Merchant,terminal_id,ROUND(Trans_Amount,2) AS Trans_Amount,ID from sales_draft_" + year + month
            + filter+")";
            fields = "Trans_Date,Trans_Time,Customer_ID,Merchant,terminal_id,Trans_Amount,ID";
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "details_data.php?tablename=" + tablename + "&year=" + year + "&month=" + month + "&fields=" + fields,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month, criteria) {
            switch (criteria) {
                case 1:
                    filter = " where left(Trans_Time,2)>='00' and left(Trans_Time,2)<='05'";
                    break;
                case 2:
                    filter = " where left(Trans_Time,2)>='06' and left(Trans_Time,2)<='11'";
                    break;
                case 3:
                    filter = " where left(Trans_Time,2)>='12' and left(Trans_Time,2)<='17'";
                    break;
                case 4:
                    filter = " where left(Trans_Time,2)>='18' and left(Trans_Time,2)<='23'";
                    break;
            }
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "select Trans_Date,Trans_Time,Customer_ID,ROUND(Trans_Amount,2) AS Trans_Amount,Merchant,Terminal_ID from sales_draft_" + year + month
            + filter;
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=time_of_sales",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>

</body>
</html>
