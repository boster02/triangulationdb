<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
include("fncAnalytics.inc.php");
$sales=fncGetTop20PerMerchantCummulative();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Duplicate Authorization</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <a class="green-btn" href="javascript:exportDetails()">Export this Report to Excel</a>
    <div class="csstable">
        <img style="height:1px" />
        <table>
            <tr>
                <td>
                    <strong>Merchant</strong>
                </td>
                <td>
                    <strong>Item Description</strong>
                </td>
                <td>
                    <strong>Translation</strong>
                </td>
                <td class="number">
                    <strong>Transactions</strong>
                </td>
                <td class="number">
                    <strong>Average Price</strong>
                </td>
            </tr>
            <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php $merchant_name=$sales[$i]["ID"].". ". $sales[$i]["wfp_name"]. " - ".$sales[$i]["branch"]." - ".$sales[$i]["address"];
                          $merchant_name=trim($merchant_name,' - ');
                          echo $merchant_name;  ?>
                </td>
                <td>
                    <?php echo $sales[$i]["Item_Description"] ?>
                </td>
                <td>
                    <?php echo $sales[$i]["Translation"] ?>
                </td>
                <td class="number">
                    <?php echo $sales[$i]["Trans"] ?>
                </td>
                <td class="number">
                    <?php echo number_format($sales[$i]["AvgUnitPrice"],2) ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <script type="text/javascript">

        function exportDetails(year, month, merchant) {
            if (month < 10) month = "0" + month;
            $('#loadingmessage').show();
            var sql = "SELECT COALESCE (merchants_wfp.wfp_name, N' - ', merchants_wfp.branch, N' - ', merchants_wfp.address)"
            + " AS merchant_name, Item_Description,"
            + " Translation, Trans, AvgUnitPrice"
            + " FROM            Top20PerMerchantTranslated INNER JOIN"
            + " merchants_wfp ON Merchant = merchants_wfp.id";
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename=top20merchant_sales",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
