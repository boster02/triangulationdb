<?php session_start();
      include("fncAnalytics.inc.php");
      $sales=fncGetTotalSalesPerMerchant_Cum($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Merchant Purchases - NFI</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <div class="csstable">
        <a class="green-btn" href="javascript:exportDetails()">Export to Excel</a><br />
        <img style="height:2px"/>        
        <table>
            <tr>
                <td>#</td>
                <td>Merchant</td>
                <td>Sales Volume</td>
                <td>Sales Value</td>
                <td>Sales Volume Percent</td>
                <td>Sales value Percent</td>
            </tr>
            <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $sales[$i]["Merchant"] ?></td>
                <td class="number"><?php echo number_format($sales[$i]["SalesVolume"]) ?></td>
                <td class="number"><?php echo number_format($sales[$i]["SalesValue"],2) ?></td>
                <td class="number"><?php echo number_format($sales[$i]["SalesVolumePercent"],2) ?></td>
                <td class="number"><?php echo number_format($sales[$i]["SalesValuePercent"],2) ?></td>
            </tr>
            <?php } ?>
        </table>
        </div>
    <script type="text/javascript">
        function exportDetails() {
            var year = <?php echo $_POST["year"] ?>;
        var month = <?php echo $_POST["month"] ?>;
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var sql = "SELECT Merchant,Sum(Trans_Amount) as SalesValue,COUNT(*) as SalesVolume, "
        +"convert(float,Count(*))/(select Count(*) from sales_draft)*100 as SalesVolumePercent, "
        +"Sum(Trans_Amount)/(select SUM(Trans_Amount) from sales_draft)*100 as "
        +"SalesValuePercent from sales_draft where LEFT(Trans_Date,6)<=" + year + month + " group by merchant "
        +"order by SalesVolumePercent DESC";
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=sales_per_merchant_cum",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
</script>
</body>
</html>