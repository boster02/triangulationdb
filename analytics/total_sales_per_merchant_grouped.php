<?php session_start();
      include("fncAnalytics.inc.php");
      $sales=fncGetTotalSalesPerMerchantGrouped($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Merchant Purchases - NFI</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>

    <div class="csstable">
           <a class="green-btn" href="javascript:exportDetails()">Export this Report to Excel</a>
        <a class="green-btn" href="javascript:exportAllDetails()">Export Transactions to Excel</a><br />
    <img style="height:1px" /> 

        <table>
            <tr>
                <td>Governorate</td>
                <td>Shop (WFP Name)</td>
                <td>MEPS Name</td>
                <td>WFP ID</td>
                <td>MEPS ID</td>
                <td>Sales Volume</td>
                <td>Beneficaries</td>
                <td>Sales Value</td>
                <td>Sales Volume Percent</td>
                <td>Sales value Percent</td>
            </tr>
            <?php for($i=1;$i<=$sales[0][0];$i++){ ?>
            <tr>
                <td><?php echo $sales[$i]["Governorate"] ?></td>
                <td><?php echo $sales[$i]["shop_name"] ?></td>
                <td>
                    <?php echo $sales[$i]["meps_name"] ?>
                </td>
                <td>
                    <?php echo $sales[$i]["retailer_id"] ?>
                </td>
                <td>
                    <?php echo $sales[$i]["meps_id"] ?>
                </td>
                <td class="number"><?php echo number_format($sales[$i]["SalesVolume"]) ?></td>
                <td class="number"><?php echo number_format($sales[$i]["Bens"]) ?></td>
                <td class="number"><?php echo number_format($sales[$i]["SalesValue"],2) ?></td>
                <td class="number"><?php echo number_format($sales[$i]["SalesVolumePercent"],2) ?></td>
                <td class="number"><?php echo number_format($sales[$i]["SalesValuePercent"],2) ?></td>
            </tr>
            <?php } ?>
        </table>
        </div>
<script type="text/javascript">
    function exportDetails() {
        var year = <?php echo $_POST["year"] ?>;
        var month = <?php echo $_POST["month"] ?>;
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var sql = "SELECT Governorate,min(wfp_name) as shop_name,min(merchant) "
        +"as meps_name,min(sales_draft_" +year+month+".retailer_id) "
        +"as meps_id,merchants_wfp.retailer_id,Count(Distinct Customer_ID) "
        +"as Bens,Sum(Trans_Amount) as SalesValue,COUNT(*) as SalesVolume, "
        +"convert(float,Count(*))/(select Count(*) from sales_draft_" +year+month+")*100 as SalesVolumePercent, "
        +"Sum(Trans_Amount)/(select SUM(Trans_Amount) from sales_draft_" +year+month+")*100 as "
        +"SalesValuePercent from sales_draft_" +year+month+" "
		+"sales_draft_" +year+month+" left outer JOIN "
        +"merchants_meps ON sales_draft_" +year+month+".retailer_id = merchants_meps.MEPS_Merchant_ID left outer JOIN "
        +"merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id "
		+"group by Governorate,merchants_wfp.retailer_id order by SalesVolumePercent DESC";
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=sales_per_merchant",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    function exportAllDetails(){
        var year = <?php echo $_POST["year"] ?>;
        var month = <?php echo $_POST["month"] ?>;
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        var sql = "SELECT Merchant,Trans_Date,Trans_Amount,Customer_ID,Card_Number "
        + "from sales_draft_" + year + month;
        $.ajax({
            url: "export_data.php?sql=" + sql + "&tablename=sales_per_merchant_trans",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
</script>
</body>
</html>