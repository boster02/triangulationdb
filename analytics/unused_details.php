<?php
include("fncAnalytics.inc.php");

$sIndexColumn = "ID";

$month=$_GET["year"].$_GET["month"];
$onemonthbefore=fncGetPreviousMonth($month);
$twomonthsbefore=fncGet2MonthsBefore($month);
$sTable = "(select
            r.Customer_ID as ID,
            max(CONVERT(DATETIME,r.Trans_Date,112)) ReloadDate,
            min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)) LastTransaction,
            DATEADD(day,60,max(CONVERT(DATETIME,cast(r.Trans_Date as char(8)),112))) CreditExpiryDate,
            DATEDIFF(day, max(CONVERT(DATETIME,r.Trans_Date,112)),isnull(min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)),GETDATE())) UnusedPeriodInDays,
            Location,
            CurrentBalance
            from (select * from [dbo].[sales_draft]  where substring(cast(trans_date as char(8)),0,7) in ($twomonthsbefore, $onemonthbefore, $month) ) S
            right join pre_paid_reload_meps_$twomonthsbefore R on r.Customer_ID = s.Customer_ID
            left join pre_paid_reload_wfp_$twomonthsbefore on Beneficiary_ID = r.Customer_ID
            left join (
            select t.Customer_ID,CurrentBalance from (
            select *,
            ROW_NUMBER() over (partition by customer_id order by trans_date desc, serial desc) rn
             from OpeningBalanceArchive
            ) t
            where rn = 1
            ) Balances on Balances.Customer_ID = R.Customer_ID
            where
             r.Customer_ID not in (select Customer_ID from [MEPS_Adjustment_Transactions] c where c.cycle
            in ($twomonthsbefore, $onemonthbefore, $month) and transaction_type in ('Pre-Paid Redemption','Cash ATM'))
            group by r.Customer_ID,Location,CurrentBalance,r.Prepaid_Program
            having
            min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)) is null
            and DATEDIFF(day, max(CONVERT(DATETIME,r.Trans_Date,112)),isnull(min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)),GETDATE()))  > 60
            and CurrentBalance > 0
            and r.Prepaid_Program in('AL ZAATARI CARD','WFP NORMAL CARD SALES'))";

/* Database connection information */
$gaSql['user']       = "sa";
$gaSql['password']   = "P@ssword1";
$gaSql['db']         = "jordandb";
$gaSql['server']     = "10.67.67.130";

/*
 * Columns
 * If you don't want all of the columns displayed you need to hardcode $aColumns array with your elements.
 * If not this will grab all the columns associated with $sTable
 */
$fields=explode(",","ID,ReloadDate,LastTransaction,CreditExpiryDate,UnusedPeriodInDays,Location,CurrentBalance");
$aColumns = $fields;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
 * no need to edit below this line
 */

/*
 * ODBC connection
 */
$connectionInfo = array("UID" => $gaSql['user'], "PWD" => $gaSql['password'], "Database"=>$gaSql['db'],"ReturnDatesAsStrings"=>true,"CharacterSet" => "UTF-8");
$gaSql['link'] = sqlsrv_connect( $gaSql['server'], $connectionInfo);
$params = array();
$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );


/* Ordering */
$sOrder = "";
if ( isset( $_GET['iSortCol_0'] ) ) {
    $sOrder = "ORDER BY  ";
    for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ) {
        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ) {
            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                    ".addslashes( $_GET['sSortDir_'.$i] ) .", ";
        }
    }
    $sOrder = substr_replace( $sOrder, "", -2 );
    if ( $sOrder == "ORDER BY" ) {
        $sOrder = "";
    }
}

/* Filtering */
$sWhere = "";
if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
    $sWhere = "WHERE (";
    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
        $sWhere .= $aColumns[$i]." LIKE '%".addslashes( $_GET['sSearch'] )."%' OR ";
    }
    $sWhere = substr_replace( $sWhere, "", -3 );
    $sWhere .= ')';
}
/* Individual column filtering */
for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
    if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )  {
        if ( $sWhere == "" ) {
            $sWhere = "WHERE ";
        } else {
            $sWhere .= " AND ";
        }
        $sWhere .= $aColumns[$i]." LIKE '%".addslashes($_GET['sSearch_'.$i])."%' ";
    }
}

/* Paging */
$top = (isset($_GET['iDisplayStart']))?((int)$_GET['iDisplayStart']):0 ;
$limit = (isset($_GET['iDisplayLength']))?((int)$_GET['iDisplayLength'] ):10;
$sQuery = "SELECT TOP $limit ".implode(",",$aColumns)."
        FROM $sTable as t1
        $sWhere ".(($sWhere=="")?" WHERE ":" AND ")." $sIndexColumn NOT IN
        (
            SELECT $sIndexColumn FROM
            (
                SELECT TOP $top ".implode(",",$aColumns)."
                FROM $sTable as t2
                $sWhere
                $sOrder
            )
            as [virtTable]
        )
        $sOrder";

$rResult = sqlsrv_query($gaSql['link'],$sQuery) or die("$sQuery: " . sqlsrv_errors());

$sQueryCnt = "SELECT * FROM $sTable as t3 $sWhere";
$rResultCnt = sqlsrv_query( $gaSql['link'], $sQueryCnt ,$params, $options) or die (" $sQueryCnt: " . sqlsrv_errors());
$iFilteredTotal = sqlsrv_num_rows( $rResultCnt );

$sQuery = " SELECT * FROM $sTable as t4 ";
$rResultTotal = sqlsrv_query( $gaSql['link'], $sQuery ,$params, $options) or die(sqlsrv_errors());
$iTotal = sqlsrv_num_rows( $rResultTotal );

$output = array(
    "sEcho" => intval(isset($_GET['sEcho'])?$_GET['sEcho']:0),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ( $aRow = sqlsrv_fetch_array( $rResult ) ) {
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
        if ( $aColumns[$i] != ' ' ) {
            $column_name=str_replace("[","",$aColumns[$i]);
            $column_name=str_replace("]","",$column_name);
            $v = $aRow[ $column_name ];
            $v = mb_check_encoding($v, 'UTF-8') ? $v : utf8_encode($v);
            $row[]=$v;
        }
    }
    If (!empty($row)) { $output['aaData'][] = $row; }
}
echo json_encode( $output );
?>