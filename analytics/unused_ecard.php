<?php session_start();
      include("fncAnalytics.inc.php");
      $unused=fncGetUnused($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Unused E-cards</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table>
                        <tr>
                            <td><strong>Number of Cards</strong></td>
                            <td><strong>Unused Balance</strong></td>
                        </tr>
                        <tr>
                            <td class="number">
                                <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"] ?>)">
                                    <?php echo number_format($unused["CardsNumber"]) ?></a>
                                <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a></td>
                            <td class="number"><?php echo number_format($unused["UnusedBalance"],2) ?></td>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="detailscontainer" class="details csstable-details">
                    <table id="detailstable" class="display compact" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Customer ID</th>
                                <th>Reload Date</th>
                                <th>Last Transaction</th>
                                <th>Credit Expiry Date</th>
                                <th>Unused Period in Days</th>
                                <th>Location</th>
                                <th>Current Balance</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function showDetails(year, month) {
            debugger;
            if (month < 10) month = "0" + month;
            var url = "unused_details.php?year=" + year + "&month=" + month;
            $("#detailscontainer").show();
            $("#detailstable").DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": url,
                "sPaginationType": "full_numbers"
            });
        }
        function exportDetails(year, month) {
            debugger;
            $('#loadingmessage').show();
            if (month < 10) month = "0" + month;
            var thismonth=year+month;
            var onemonthbefore = getPreviousMonth(year + "-" + month);
            var twomonthsbefore = getTwoMonthsAgo(year + "-" + month);
            var sql = "Select ID,format(ReloadDate,'yyyy-MM-dd') as ReloadDate, LastTransaction,format(CreditExpiryDate,'yyyy-MM-dd')\
                as CreditExpiryDate,UnusedPeriodInDays,Location,CurrentBalance from\
                (select\
                r.Customer_ID as ID,\
                max(CONVERT(DATETIME,r.Trans_Date,112)) ReloadDate,\
                min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112))\
                LastTransaction,\
                DATEADD(day,60,max(CONVERT(DATETIME,cast(r.Trans_Date as char(8)),112)))\
                CreditExpiryDate,\
                DATEDIFF(day, max(CONVERT(DATETIME,r.Trans_Date,112)),\
                isnull(min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)),GETDATE()))\
                UnusedPeriodInDays,            Location,            CurrentBalance\
                from (select * from [dbo].[sales_draft]\
                where substring(cast(trans_date as char(8)),0,7)\
                in ("+twomonthsbefore+", "+onemonthbefore+", "+thismonth+") ) S\
                right join pre_paid_reload_meps_"+twomonthsbefore+" R on r.Customer_ID = s.Customer_ID\
                left join pre_paid_reload_wfp_"+twomonthsbefore+" on Beneficiary_ID = r.Customer_ID\
                left join (            select t.Customer_ID,CurrentBalance from (\
                select *,            ROW_NUMBER() over (partition by customer_id order by trans_date desc, serial desc) rn\
                from OpeningBalanceArchive            ) t\
                where rn = 1            ) Balances on Balances.Customer_ID = R.Customer_ID\
                where             r.Customer_ID not in (select Customer_ID from [MEPS_Adjustment_Transactions] c\
                where c.cycle            in ("+twomonthsbefore+", "+onemonthbefore+", "+thismonth+") and transaction_type in ('Pre-Paid Redemption','Cash ATM'))\
                group by r.Customer_ID,Location,CurrentBalance,r.Prepaid_Program\
                having            min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)) is null\
                and DATEDIFF(day, max(CONVERT(DATETIME,r.Trans_Date,112)),isnull(min(CONVERT(DATETIME,cast(s.Trans_Date as char(8)),112)),\
                GETDATE()))  > 60\
                and CurrentBalance > 0\
                and r.Prepaid_Program in('AL ZAATARI CARD','WFP NORMAL CARD SALES')) z";
            $.ajax({
                url: "export_data.php?sql=" + sql+"&tablename=unused",
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    //var err = eval("(" + xhr.responseText + ")");
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                    //$('#loadingmessage').hide();
                }
            });
        }
        
        function getPreviousMonth(date) {
            var d = new Date(date);
            d.setDate(1);
            d.setMonth(d.getMonth());
            month = d.getMonth();
            if (month == 0) month = 12;
            if (month < 10) month = "0" + month;
            return d.getFullYear() + "" + month;
        }
        function getTwoMonthsAgo(date) {
            debugger;
            var d = new Date(date);
            d.setMonth(d.getMonth() - 2);
            return d.toISOString().substring(0, 7).replace("-", "")
        }
    </script>
</body>
</html>
