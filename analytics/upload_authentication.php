<?php session_start();
      include("fncAnalytics.inc.php");
      $uploaddata=fncGetUploadAuthentication($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Upload authentication</title>
    <link rel="stylesheet" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="../css/jquery.qtip.min.css" />
    <style>
        .tipStyle {
            font-size: 16px;
            font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td style="vertical-align: top">
                <div class="csstable">
                    <table style="width: 500px">
                        <tr>
                            <td><strong>Source</strong></td>
                            <td><strong>No. of Transactions</strong></td>
                            <td><strong>Amount (JOD)</strong></td>
                            <td><strong>Variance</strong></td>
                        </tr>
                        <?php for($i=1;$i<=$uploaddata[0][0];$i++){
                        $variance=number_format($uploaddata[1]["Amount"]-$uploaddata[$i]["Amount"],2);
                        ?>
                        
                        <tr>
                            <td><strong><?php echo $uploaddata[$i]["Description"] ?></strong></td>
                            <td class="number">
                                <a href="javascript:showDetails(<?php echo $i.",".$_POST["year"].",".$_POST["month"] ?>)">
                                    <?php echo number_format($uploaddata[$i]["Trans"])?></a>
                                <a style="text-decoration:none" href="javascript:exportDetails(<?php echo $i.",".$_POST["year"].",".$_POST["month"] ?>)">
                                    <img src="../images/exportxls.png" alt="Export" />
                                </a>
                            </td>
                            <td class="number"><?php echo number_format($uploaddata[$i]["Amount"],2) ?></td>
                            <td class="number"><a <?php if ($variance!=0) { echo "href='javascript:void(0)' data-explanation='Food for training transactions'"; } ?> ><?php echo $variance ?></a></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div id="dmeps_reload" class="details csstable-details">
                    <table id="meps_reload" class="display compact">
                        <thead>
                            <tr>
                                <th>Trans. Date</th>
                                <th>Customer ID</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div id="djab_reload" class="details csstable-details">
                    <table id="jab_reload" class="display compact">
                        <thead>
                            <tr>
                                <th>Trans. Date</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div id="dwfp_reload" class="details csstable-details">
                    <table id="wfp_reload" class="display compact">
                        <thead>
                            <tr>
                                <th>Trans. Date</th>
                                <th>Beneficiary ID</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div id="dloa_reload" class="details csstable-details">
                    <table id="loa_reload" class="display compact">
                        <thead>
                            <tr>
                                <th>Trans. Date</th>
                                <th>Location</th>
                                <th>Amount</th>
                                <th>View Letter</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <script src="../js/jquery.qtip.min.js"></script>
    <script>
        $('[data-explanation!=""]').qtip({ 
            content: {
                title: 'Explanation',
                attr: 'data-explanation' 
            },
            style: {
                classes: 'tipStyle'
            }
        })
    </script>
    <script type="text/javascript">
        function showDetails(tableid, year, month) {
            $("#djab_reload").hide();
            $("#dmeps_reload").hide();
            $("#dwfp_reload").hide();
            if (month < 10) month = "0" + month;
            if (tableid == 4) {
                tablename = "(Select Trans_Date,Location,Trans_Amount,file_path,id from pre_paid_reload_loa where Cycle=" + year + month +")";
                displayid = "loa_reload";
                fields = "trans_date,location,trans_amount,file_path,id";
            }
            if (tableid == 3) {
                tablename = "jab_statement_" + year + month;
                displayid = "jab_reload";
                fields = "trans_date,amount,id";
            }
            if (tableid == 2) {
                tablename = "pre_paid_reload_meps_" + year + month;
                displayid = "meps_reload";
                fields = "Trans_Date,Customer_ID,Trans_Amount,ID";
            }
            if (tableid == 1) {
                tablename = "pre_paid_reload_wfp_" + year + month;
                displayid = "wfp_reload";
                fields = "Trans_Date,Beneficiary_ID,Trans_Amount,ID";
            }
            $("#d" + displayid).show();
            if (tableid == 4) {
                $("#" + displayid).DataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "details_data.php?tablename=" + tablename + "&fields=" + fields,
                    "sPaginationType": "full_numbers",
                    "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                        $('td:eq(3)', nRow).html('<a href="../' + aData[3] + '">' +
                            '<img src="../images/pdf.png" />' + '</a>');
                        return nRow;
                    },
                });
            }
            else {
                $("#" + displayid).DataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "details_data.php?tablename=" + tablename + "&fields=" + fields,
                    "sPaginationType": "full_numbers"
                });
            }
        }
        function exportDetails(tableid, year, month) {
            if (month < 10) month = "0" + month;
            var tablename = "";
            var displayid = "";
            if (tableid == 4) {
                tablename = "Select Trans_Date,Location,Trans_Amount from pre_paid_reload_loa where Cycle=" + year + month;
                displayid = "loa_reload";
            }
            if (tableid == 3) {
                tablename = "Select Trans_Date,Amount from jab_statement_" + year + month;
                displayid = "jab_reload";
            }
            if (tableid == 2) {
                tablename = "Select Trans_Date,Customer_ID,Trans_Amount from pre_paid_reload_meps_" + year + month;
                displayid = "meps_reload";
            }
            if (tableid == 1) {
                tablename = "select Trans_Date,Beneficiary_ID,Trans_Amount from pre_paid_reload_wfp_" + year + month;
                displayid = "wfp_reload";
            }
            
            $('#loadingmessage').show();
            var sql = tablename;
            $.ajax({
                url: "export_data.php?sql=" + sql + "&tablename="+displayid,
                dataType: 'JSON',
                success: function (response) {
                    if (response.xls) {
                        location.href = response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
