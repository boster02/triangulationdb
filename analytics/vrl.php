<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title> Basic example </title>
    <link rel="stylesheet" href="../css/Treant.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/basic-example.css">
    <script src="../js/raphael.js"></script>
    <script src="../js/Treant.js"></script>
    <script src="../js/bootstrap.js"></script>

</head>
<body>
<div class="row">
<div class="col-lg-9">
    <input type="hidden" name="limit" id="limit" value="25">
    <input type="hidden" name="condition" id="condition" value="">
    <div class="input-group input-group " style="margin: 20px 0">
        <input class="form-control" type="text" id="search" placeholder="UNHCR Id OR Card Number ">
        <span class="input-group-btn">
  <button type="button" class="btn btn-info btn-flat" id="find">find</button>
                    </span>
    </div>
    <div class="chart" id="basic-example"></div>

</div>
</div>
<div class="row">
    <div class="col-lg-12">

<div  id="data"></div>
        </div>

        </div>
<?php
include "config.inc.php";
function fncGetVRLSummary()
{
    $dbh = fncOpenDBConn();
    $sql = "select * from VRL_Summary";
    $res = mssql_query($sql, $dbh);
    $row = mssql_fetch_array($res);


    mssql_close($dbh);
    return $row;
}

function fncGetVRLData($where)
{
    $dbh = fncOpenDBConn();
    $sql = "select * from Finance_VRL where $where";
    $res = mssql_query($sql, $dbh) or die($sql);
    $row = mssql_fetch_array($res);
    mssql_close($dbh);
    //var_dump($row);
    return $row;
}

//var_dump(fncGetVRLData("Bank_Status = 'Active'"));
?>

<script type="text/javascript">
    $(document).on("click", "#loadMore", function (event) {
        fetchVRL('',1);
        $('html', '#data').animate({
            scrollTop: $("#loadMore").offset().top
        }, 1000);
    });

    $(document).on("click", "#export", function (event) {
        $('#loadingmessage').show();
       var tablename =$('#condition').val();
        $.ajax({
            url: "export_vrl.php?tablename=" + tablename,
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                //var err = eval("(" + xhr.responseText + ")");
                alert("An error has occurred when creating the Excel file");
            }
        });

       });
    $(document).on("click", "#find", function (event) {
        fetchVRL('find',0);
        $('#loadMore').hide();
    });
    function fetchVRL(data,type) {
        $('#loadingmessage').show();
        var limit;
        var data ;

        if(type=='1')
        {
            data =$('#condition').val();
            limit =parseInt($('#limit').val())+25;
            $('#limit').val(parseInt(limit));

        }
        else
        {
            $('#condition').val(data);
            data =data;
            limit = 25;
            $('#limit').val(parseInt(25));



        }
        var find=$('#search').val();
        $.ajax({
            url: "vrl_new_details.php?where=" + data+'&limit='+limit+'&find='+find,
            dataType: 'JSON',
            success: function (response) {
                var HTMLData ;
                if(type !='1') {
                    HTMLData ='<div class="row"><div class="col-lg-4"><label>Display : '+data.replace(/([a-z](?=[A-Z]))/g, '$1 ')+'</label> &nbsp <img  style="cursor: pointer " id="export"  src="../images/exportxls.png" alt="Export"> </div></div>';
                    HTMLData += '<table id="dataTable" class="table table-bordered table-striped" > <tr >';
                    HTMLData += '   <th>Customer ID</th>';
                    HTMLData += '  <th>Card Number</th>';
                    HTMLData += '  <th>Creation Date</th>';
                    HTMLData += '  <th>Received Date From Bank</th>';
                    HTMLData += '   <th>Handover Date(CP)</th>';
                    HTMLData += '   <th>CP Name</th>';
                    HTMLData += '  <th>Distribution Period</th>';
                    HTMLData += '  <th>Distribution Status</th>';
                    HTMLData += '  <th>Bank Status</th>';
                    HTMLData += '  <th>Programme Status</th>';
                    HTMLData += '   <th>Finance Status</th>';
                    HTMLData += '   <th>Expiry Date</th>';
                    HTMLData += '   <th>Reason Replaced</th>';
                    HTMLData += '   <th>Date Replaced</th>';
                    HTMLData += '   <th>Replacement Card Number</th></tr>';
                }
                $.each(response.data, function (key, value) {
                    HTMLData +='<tr>';
                    HTMLData +='<td>'+value.Customer_id+'</td>';
                    HTMLData +='<td>'+value.Card_Number+'</td>';
                    HTMLData +='<td>'+value.Creation_date+'</td>';
                    HTMLData +='<td>'+value.Received_Date_from_Bank+'</td>';
                    HTMLData +='<td>'+value.Handover_date_to_CP+'</td>';
                    HTMLData +='<td>'+value.CP_Name+'</td>';
                    HTMLData +='<td>'+value.Distribution_Period+'</td>';
                    HTMLData +='<td>'+value.Distribution_Status+'</td>';
                    HTMLData +='<td>'+value.Bank_Status+'</td>';
                    HTMLData +='<td>'+value.Programme_status+'</td>';
                    HTMLData +='<td>'+value.finance_status+'</td>';
                    HTMLData +='<td>'+value.Expiry_Date+'</td>';
                    HTMLData +='<td>'+value.Reason_Replaced+'</td>';
                    HTMLData +='<td>'+value.Date_Replaced+'</td>';
                    HTMLData +='<td>'+value.Replacement_Card_Number+'</td>';
                    HTMLData +='</tr>';

                });
                var HTMLDataFooter ='</table><div class="col-lg-6 col-lg-push-5"> <button id="loadMore" class="btn btn-info btn-flat" >Load More Result </button></div>';
                if(type=='1')
                {
                    $('#limit').val(limit+25)
                    $("#dataTable > tbody").append(HTMLData);

                }     else{
                    $('#data').html(HTMLData);
                    $('#data').append(HTMLDataFooter);
                    $('#limit').val(25)

                }


                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                //var err = eval("(" + xhr.responseText + ")");
                alert("An error has occurred when creating the Excel file");
            }
        });

    }
    function moreDetailes(condetion) {
        switch (condetion) {
            case 'AllCardsPrinted':
                fetchVRL('AllCardsPrinted');
                break;
            case 'ActiveCards':
                fetchVRL('ActiveCards');

                break;
            case 'InactiveCards':
                fetchVRL('InactiveCards');

                break;
            case 'BlockedCards':
                fetchVRL('BlockedCards');

                break;
            case 'NondistributedCards':
                fetchVRL('NondistributedCards');

                break;
            case 'CardsWithCP':
                fetchVRL('CardsWithCP');

                break;
            case 'WithFinance':
                fetchVRL('WithFinance');

                break;
            case 'DestroyedCards':
                fetchVRL('DestroyedCards');
                break;
            case 'PendingHandoverCards':
                fetchVRL('PendingHandoverCards');
                break;
        }
    }
    <?php
    $data = fncGetVRLSummary();
    ?>
</script>
<script type="text/javascript">
    var config = {
            container: "#basic-example",

            connectors: {
                type: 'step'
            },
            node: {
                HTMLclass: 'nodeExample1'
            }
        },
        AllCardsPrinted = {
            text: {
                name: "All Cards Printed",
                title: "<?php echo "Total : " . $data['AllCardsPrinted'] ?>",
            },
            link: {
                href: "javascript:moreDetailes('AllCardsPrinted');"
            },
                    image: "../css/images/credit-card-icon.png",


        },

        ActiveCards = {
            parent: AllCardsPrinted,
            text: {
                name: "Active Cards",
                title: "<?php echo "Total : " . $data['ActiveCards'] ?>",
            },
            link: {
                href: "javascript:moreDetailes('ActiveCards');"
            },

            stackChildren: true,
            image: "../css/images/credit-card-icon.png",

        },

        InactiveCards = {
            parent: AllCardsPrinted,
            text: {
                name: "Inactive Cards",
                title: "<?php echo "Total : " . $data['InActiveCards'] ?>",
            },
            link: {
                href: "javascript:moreDetailes('InactiveCards');"
            },
              image: "../css/images/credit-card-icon.png",


        },
        BlockedCards = {
            parent: InactiveCards,
            text: {
                name: "Blocked Cards",
                title: "<?php echo "Total : " . $data['BlockedCards'] ?>",
            },
            link: {
                href: "javascript:moreDetailes('BlockedCards');"
            },
                                image: "../css/images/credit-card-icon.png",

        },
        NondistributedCards = {
            parent: InactiveCards,
            text: {
                name: "Non Distributed Cards",
                title: "<?php echo "Total : " . $data['NondistributedCards'] ?>",
            },
            link: {
                href: "javascript:moreDetailes('NondistributedCards');"
            },
                                image: "../css/images/credit-card-icon.png",


        },
        CardsWithCP = {
            parent: NondistributedCards,
            text: {
                name: "Cards With CP",
                title: "<?php echo "Total : " . $data['CardsWithCP'] ?>",
            },

            link: {
                href: "javascript:moreDetailes('CardsWithCP');"
            },
                                image: "../css/images/credit-card-icon.png",

        },
        WithFinance = {
            parent: NondistributedCards,
            text: {
                name: "With Finance",
                title: "<?php echo "Total : " . $data['WithFinance'] ?>",
            },
            link: {
                href: "javascript:moreDetailes('WithFinance');"
            },
                                image: "../css/images/credit-card-icon.png",

        },
        DestroyedCards = {
            parent: WithFinance,
            text: {
                name: "Destroyed Cards",
                title: "<?php echo "Total : " . $data['DestroyedCards'] ?>",
            },
            link: {
                href: "javascript:moreDetailes('DestroyedCards');"
            },
                                image: "../css/images/credit-card-icon.png",

        },
        PendingHandoverCards = {
            parent: WithFinance,
            text: {
                name: "Pending Handover Cards",
                title: "<?php echo "Total : " . $data['PendingHandoverCards'] ?>",
            },
            link: {
                href: "javascript:moreDetailes('PendingHandoverCards');"
            },
                                image: "../css/images/credit-card-icon.png",


        }

    chart_config = [
        config,
        AllCardsPrinted
        , ActiveCards
        , InactiveCards
        , BlockedCards
        , NondistributedCards
        , CardsWithCP
        , WithFinance
        , DestroyedCards
        , PendingHandoverCards
    ];
    new Treant(chart_config);
</script>

</body>
</html>