<?php
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "ID";

/* DB table to use */
$tablename="vrl";
switch($_GET["tablename"]){
    case "jab_pressed":
        $tablename = "(select * from vrl where customer_id in (select customer_id from jab_vrl))";
        break;
    case "program_listing":
        $tablename = "vrl";
        break;
    case "distributed":
        $tablename = "(select * from vrl where customer_id in (select customer_id from prepaid_reload_meps)"
        ." AND customer_id NOT IN (select customer_id from Replaced_cards))";
        break;
    case "replaced":
        $tablename = "(select * from vrl where customer_id in (select customer_id from Replaced_Cards where customer_id in (select customer_id from prepaid_reload_meps)))";
        break;
    case "undistributed":
        $tablename = "(select * from vrl where customer_id not in (select customer_id from prepaid_reload_meps))";
        break;
    case "destroyed":
        $tablename = "(select * from vrl where customer_id in (select customer_id from Destroyed_Cards))";
        break;
    case "lsd":
        $tablename = "(select * from vrl where customer_id in (select customer_id from Replaced_Cards where customer_id in (select customer_id from prepaid_reload_meps)
    and customer_id not in (select customer_id from destroyed_cards)))";
        break;
}

$sTable = $tablename;

/* Database connection information */
$gaSql['user']       = "sa";
$gaSql['password']   = "P@ssword1";
$gaSql['db']         = "jordandb";
$gaSql['server']     = "10.67.67.130";

/*
 * Columns
 * If you don't want all of the columns displayed you need to hardcode $aColumns array with your elements.
 * If not this will grab all the columns associated with $sTable
 */
$fields=explode(",",$_GET["fields"]);
$aColumns = $fields;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
 * no need to edit below this line
 */

/*
 * ODBC connection
 */
$connectionInfo = array("UID" => $gaSql['user'], "PWD" => $gaSql['password'], "Database"=>$gaSql['db'],"ReturnDatesAsStrings"=>true,"CharacterSet" => "UTF-8");
$gaSql['link'] = sqlsrv_connect( $gaSql['server'], $connectionInfo);
$params = array();
$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );


/* Ordering */
$sOrder = "";
if ( isset( $_GET['iSortCol_0'] ) ) {
    $sOrder = "ORDER BY  ";
    for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ) {
        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ) {
            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                    ".addslashes( $_GET['sSortDir_'.$i] ) .", ";
        }
    }
    $sOrder = substr_replace( $sOrder, "", -2 );
    if ( $sOrder == "ORDER BY" ) {
        $sOrder = "";
    }
}

/* Filtering */
$sWhere = "";
if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
    $sWhere = "WHERE (";
    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
        $sWhere .= $aColumns[$i]." LIKE '%".addslashes( $_GET['sSearch'] )."%' OR ";
    }
    $sWhere = substr_replace( $sWhere, "", -3 );
    $sWhere .= ')';
}
/* Individual column filtering */
for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
    if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )  {
        if ( $sWhere == "" ) {
            $sWhere = "WHERE ";
        } else {
            $sWhere .= " AND ";
        }
        $sWhere .= $aColumns[$i]." LIKE '%".addslashes($_GET['sSearch_'.$i])."%' ";
    }
}

/* Paging */
$top = (isset($_GET['iDisplayStart']))?((int)$_GET['iDisplayStart']):0 ;
$limit = (isset($_GET['iDisplayLength']))?((int)$_GET['iDisplayLength'] ):10;
$sQuery = "SELECT TOP $limit ".implode(",",$aColumns)."
        FROM $sTable as t1
        $sWhere ".(($sWhere=="")?" WHERE ":" AND ")." $sIndexColumn NOT IN
        (
            SELECT $sIndexColumn FROM
            (
                SELECT TOP $top ".implode(",",$aColumns)."
                FROM $sTable as t2
                $sWhere
                $sOrder
            )
            as [virtTable]
        )
        $sOrder";

$rResult = sqlsrv_query($gaSql['link'],$sQuery) or die("$sQuery: " . sqlsrv_errors());

$sQueryCnt = "SELECT * FROM $sTable as t3 $sWhere";
$rResultCnt = sqlsrv_query( $gaSql['link'], $sQueryCnt ,$params, $options) or die (" $sQueryCnt: " . sqlsrv_errors());
$iFilteredTotal = sqlsrv_num_rows( $rResultCnt );

$sQuery = " SELECT * FROM $sTable as t4 ";
$rResultTotal = sqlsrv_query( $gaSql['link'], $sQuery ,$params, $options) or die(sqlsrv_errors());
$iTotal = sqlsrv_num_rows( $rResultTotal );

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ( $aRow = sqlsrv_fetch_array( $rResult ) ) {
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
        if ( $aColumns[$i] != ' ' ) {
            $v = $aRow[ $aColumns[$i] ];
            $v = mb_check_encoding($v, 'UTF-8') ? $v : utf8_encode($v);
            $row[]=$v;
        }
    }
    If (!empty($row)) {
        $row[2]=""; //Original distribution date
        $row[5]=fncGetNoOfReloads($row[0]);
        $row[6]=fncGetNoOfSales($row[0]);  //$output['aaData'][$k]["In"]="";
        $row[7]="";  //Date reruned to WFP
        $row[8]=fncGetDateDestroyed($row[0]);  //date destroyed
        $row[9]=fncGetReasonReplaced($row[0]);  //Reason replaced
        $row[10]=fncGetDateReplaced($row[0]);  //$output['aaData'][$k]["Date_Returned"]="";
        $row[11]=fncGetReplacementCardNo($row[0]); //$output['aaData'][$k]["Date_Destroyed"]="";
        $output['aaData'][] = $row;
    }
}
echo json_encode( $output );
function fncGetNoofReloads($custid){
    $sql="select Reloads from NumberOfReloads where customer_id='$custid'";
    return fncGetDBValue($sql);
}
function fncGetNoofSales($custid){
    $sql="select Sales from NumberOfSales where customer_id='$custid'";
    return fncGetDBValue($sql);
}
function fncGetDateDestroyed($custid){
    $sql="select Date_Destroyed from Destroyed_Cards where customer_id='$custid'";
    return fncGetDBValue($sql);
}
function fncGetReasonReplaced($custid){
    $sql="select Reason_Replaced from Replaced_cards where customer_id='$custid'";
    return fncGetDBValue($sql);
}
function fncGetDateReplaced($custid){
    $sql="select Date_Replaced from Replaced_cards where customer_id='$custid'";
    return fncGetDBValue($sql);
}
function fncGetReplacementCardNo($custid){
    $sql="select Reloads from NumberOfReloads where customer_id='$custid'";
    return fncGetDBValue($sql);
}
function fncGetDBValue($sql){
    global $gaSql;
    $res = sqlsrv_query($gaSql['link'],$sql) or die("$sql: " . sqlsrv_errors());
    $row=sqlsrv_fetch_array($res);
    return $row[0];
}
?>