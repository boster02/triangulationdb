<?php
include "config.inc.php";
function fncGetVRLData($where_condetion,$limit,$find)
{
    switch ($where_condetion) {
        case 'AllCardsPrinted':
           $where= " 1 =1";
            break;
        case 'ActiveCards':
           $where="Bank_Status = 'Active'";

            break;
        case 'InactiveCards':
           $where="Bank_Status <> 'Active' ";

            break;
        case 'BlockedCards':
           $where="Distribution_Status = 'distributed' and Bank_Status <> 'Active'";

            break;
        case 'NondistributedCards':
           $where="Distribution_Status <> 'distributed' and Bank_Status <> 'Active'  ";

            break;
        case 'CardsWithCP':
           $where="finance_status = 'With Partner' and Handover_date_to_CP is not null and Bank_Status <> 'Active' ";

            break;
        case 'WithFinance':
           $where="Distribution_Status <> 'distributed' and Bank_Status <> 'Active' and finance_status <> 'With Partner' and finance_status <> 'NA' ";

            break;
        case 'DestroyedCards':
           $where="Distribution_Status <> 'distributed' and  finance_status = 'Destroyed' and Bank_Status <> 'Active' ";

            break;
        case 'PendingHandoverCards':
           $where="Distribution_Status = 'Pending Distribution' and  finance_status = 'Finance Safe' and Bank_Status <> 'Active' ";
            break;
        case 'find':
            $where="Customer_id like '%".$find."%' or right(Card_Number,4) like '%".$find."%'";
            break;


    }

    $sql="select * from (
            select *, ROW_NUMBER() over (order by customer_id) rn from Finance_VRL where $where)t
 where rn between $limit-25 and $limit-1";
    $dbh = fncOpenDBConn();
    $result=array();
    $res = mssql_query($sql, $dbh) or die($sql);

    while ($row = mssql_fetch_assoc($res)) {
        $result []= $row;

    }
    mssql_close($dbh);
    return array('data'=>$result);
    exit;
}

echo json_encode(fncGetVRLData($_GET['where'],$_GET['limit'],$_GET['find'])) ;

/* old file
$sIndexColumn = "ID";

/* DB table to use
$tablename=$_GET["tablename"];
$category=$_GET["category"];
if($category=="All cards"){
    $sql="(Select 1 as ID, * From Finance_VRL)";
}
else{
    $sql="(Select 1 as ID,* From Finance_VRL where $tablename='$category')";
}

$sTable = $sql;

/* Database connection information
$gaSql['user']       = "sa";
$gaSql['password']   = "P@ssword1";
$gaSql['db']         = "jordandb";
$gaSql['server']     = "10.67.67.130";

/*
 * Columns
 * If you don't want all of the columns displayed you need to hardcode $aColumns array with your elements.
 * If not this will grab all the columns associated with $sTable

$fieldstr= "Customer_id,Card_Number,Creation_date,Received_Date_from_Bank,Handover_date_to_CP,CP_Name,"
           ."Distribution_Period,Distribution_Status,Bank_Status,Programme_status,finance_status,Expiry_Date,"
           ."Reason_Replaced,Date_Replaced,Replacement_Card_Number,ID";
$fields=explode(",",$fieldstr);
$aColumns = $fields;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
 * no need to edit below this line
 */

/*
 * ODBC connection

$connectionInfo = array("UID" => $gaSql['user'], "PWD" => $gaSql['password'], "Database"=>$gaSql['db'],"ReturnDatesAsStrings"=>true,"CharacterSet" => "UTF-8";
$gaSql['link'] = sqlsrv_connect( $gaSql['server'], $connectionInfo);
$params = array();
$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );


/* Ordering
$sOrder = "";
if ( isset( $_GET['iSortCol_0'] ) ) {
    $sOrder = "ORDER BY  ";
    for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ) {
        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ) {
            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                    ".addslashes( $_GET['sSortDir_'.$i] ) .", ";
        }
    }
    $sOrder = substr_replace( $sOrder, "", -2 );
    if ( $sOrder == "ORDER BY" ) {
        $sOrder = "";
    }
}

/* Filtering
$sWhere = "";
if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
    $sWhere = "WHERE (";
    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
        $sWhere .= $aColumns[$i]." LIKE '%".addslashes( $_GET['sSearch'] )."%' OR ";
    }
    $sWhere = substr_replace( $sWhere, "", -3 );
    $sWhere .= ')';
}
/* Individual column filtering
for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
    if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )  {
        if ( $sWhere == "" ) {
            $sWhere = "WHERE ";
        } else {
            $sWhere .= " AND ";
        }
        $sWhere .= $aColumns[$i]." LIKE '%".addslashes($_GET['sSearch_'.$i])."%' ";
    }
}

/* Paging
$top = (isset($_GET['iDisplayStart']))?((int)$_GET['iDisplayStart']):0 ;
$limit = (isset($_GET['iDisplayLength']))?((int)$_GET['iDisplayLength'] ):10;
$sQuery = "SELECT TOP $limit ".implode(",",$aColumns)."
        FROM $sTable as t1
        $sWhere ".(($sWhere=="")?" WHERE ":" AND ")." $sIndexColumn NOT IN
        (
            SELECT $sIndexColumn FROM
            (
                SELECT TOP $top ".implode(",",$aColumns)."
                FROM $sTable as t2
                $sWhere
                $sOrder
            )
            as [virtTable]
        )
        $sOrder";

$rResult = sqlsrv_query($gaSql['link'],$sQuery) or die("$sQuery: " . sqlsrv_errors());

$sQueryCnt = "SELECT * FROM $sTable as t3 $sWhere";
$rResultCnt = sqlsrv_query( $gaSql['link'], $sQueryCnt ,$params, $options) or die (" $sQueryCnt: " . sqlsrv_errors());
$iFilteredTotal = sqlsrv_num_rows( $rResultCnt );

$sQuery = " SELECT * FROM $sTable as t4 ";
$rResultTotal = sqlsrv_query( $gaSql['link'], $sQuery ,$params, $options) or die(sqlsrv_errors());
$iTotal = sqlsrv_num_rows( $rResultTotal );

$output = array(
    "sEcho" => intval(isset($_GET['sEcho'])?$_GET['sEcho']:0),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ( $aRow = sqlsrv_fetch_array( $rResult ) ) {
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
        if ( $aColumns[$i] != ' ' ) {
            $column_name=str_replace("[","",$aColumns[$i]);
            $column_name=str_replace("]","",$column_name);
            $v = $aRow[ $column_name ];
            $v = mb_check_encoding($v, 'UTF-8') ? $v : utf8_encode($v);
            $row[]=$v;
        }
    }
    If (!empty($row)) { $output['aaData'][] = $row; }
}
echo json_encode( $output );
?>
*/
?>