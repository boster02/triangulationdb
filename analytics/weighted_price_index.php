<?php session_start();
      include("fncAnalytics.inc.php");
      $prices=fncGetWeightedPriceIndex($_POST["year"].$_POST["month"]);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>SKU List by Shop</title>
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
</head>

<body>
    <div class="csstable">
        <table>
            <tr>
                <td>
                    Retailer ID
                </td>
                <td>
                    Triangulation ID
                </td>
                <td>
                    Shop Name
                </td>
                <td>
                    Matching Barcodes
                </td>
                <td>
                    Price Index
                </td>
            </tr>
            <?php for($i=1;$i<=$prices[0][0];$i++){ ?>
            <tr>
                <td>
                    <?php echo $prices[$i]["retailer_id"] ?>
                </td>
                <td>
                    <?php echo $prices[$i]["Merchant"] ?>
                </td>
                <td>
                    <?php echo $prices[$i]["shop_name"] ?>
                </td>
                <td class="number">
                    <?php echo number_format($prices[$i]["Matching_Barcodes"]) ?>
                </td>
                <td class="number">
                    <a href="javascript:showDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$prices[$i]["Merchant"] ?>)">
                        <?php echo number_format($prices[$i]["Price_Index"],1) ?>%
                    </a>
                    <a href="javascript:exportDetails(<?php echo $_POST["year"].",".$_POST["month"].",".$prices[$i]["Merchant"] ?>)">
                        <img src="../images/exportxls.png" alt="Export" />
                    </a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <div id="dialog" title="Weighted Price Index Details">
        <img src='../images/download.gif' />
    </div>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.js"></script>

    <script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            autoOpen: false,
            maxWidth: 800,
            maxHeight: 500,
            width: 800,
            height: 500,
            modal: true
        });
    });
    function showDetails(year, month, merchant) {
        $("#dialog").dialog("open");
        $("#dialog").html("<img src='../images/download.gif' />");
        $.get("weighted_price_index_details.php?year=" + year + "&month="+month+"&merchant="+merchant, function (response) {
            $("#dialog").html(response);
        });
    }

    function exportDetails(year, month, merchant) {
        if (month < 10) month = "0" + month;
        $('#loadingmessage').show();
        $.ajax({
            url: "export_weighted_price_index.php?year=" + year + "&month=" + month + "&merchant=" + merchant,
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
</body>
</html>
