<?php
header('Content-Type: text/html; charset=utf-8');
include("fncAnalytics.inc.php");
$year=isset($_GET["year"])?$_GET["year"]:0;
$month=isset($_GET["month"])?$_GET["month"]:0;
$month=str_pad($month,2,"0",STR_PAD_LEFT);
$merchant=isset($_GET["merchant"])?$_GET["merchant"]:0;
$prices=fncGetWeightedPriceIndexDetails($year.$month,$merchant);
?>
    <div class="csstable">
        <table>
            <tr>
                <td>Barcode</td>
                <td>SKU DESCRIPTION</td>
                <td>Weighted Quantity</td>
                <td>Shop Price</td>
                <td>Weighted Shop Price</td>
                <td>Non WFP Price</td>
                <td>Weighted Market Price</td>
            </tr>
            <?php for($i=1;$i<=$prices[0][0];$i++){ ?>
            <tr>
                <td>
                    <a href="javascript:showItems('<?php echo $i ?>')">
                         <?php echo $prices[$i]["alt_Barcode"] ?>
                    </a>
                    <div id="item<?php echo $i ?>" style="display:none">
                        <?php echo $prices[$i]["originals"] ?>
                    </div></td>
                <td>
                    <?php echo $prices[$i]["SKU_DESCRIPTION"] ?>
                </td>
                <td class="number">
                    <?php echo $prices[$i]["Weighted_Quantity"] ?>
                </td>
                <td class="number">
                    <?php echo number_format($prices[$i]["Unit_Price"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($prices[$i]["Weighted_Shop_Price"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($prices[$i]["Non_WFP_Price"],2) ?>
                </td>
                <td class="number">
                    <?php echo number_format($prices[$i]["Weighted_Market_Price"],2) ?>
                </td>
            </tr>
            <?php } ?>
            <tr style="background-color:#ccc">
                <td>
                    <strong>TOTAL</strong>
                </td>
                <td>
                    &nbsp;
                </td>
                <td class="number">
                    &nbsp;
                </td>
                <td class="number">
                    &nbsp;
                </td>
                <td class="number">
                    <?php echo number_format($prices[0]["sum_wfp"],2) ?>
                </td>
                <td class="number">
                    &nbsp;
                </td>
                <td class="number">
                    <?php echo number_format($prices[0]["sum_non_wfp"],2) ?>
                </td>
            </tr>
        </table>
        <h2>Price Index = <?php echo number_format($prices[0]["sum_wfp"],2) ?>/<?php echo number_format($prices[0]["sum_non_wfp"],2) ?>
         x 100=<?php echo number_format($prices[0]["price_index"],2) ?>%</h2>
        <script>
            function showItems(id) {
                if($("#item" + id).is(":visible"))
                    $("#item" + id).hide();
                else
                    $("#item" + id).show();
            }
        </script>
    </div>
