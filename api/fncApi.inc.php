<?php
function fncGetFieldPosition($fieldname,$fields){
    global $msg;
    $fieldname=strtoupper($fieldname);
    $msg.="Looking for field with name $fieldname : \n";
    if(array_search($fieldname,$fields)!==false){
        $position=array_search($fieldname,$fields);
        $msg.="$fieldname is found\n";
    }
    else{
        $position=-1;
        $msg.="$fieldname is not found\n";
    }
    return $position;
}
function fncValidateDate($date, $checktime=false)
{
    $date_correct=false;
    $time_correct=false;
    if(strlen(trim($date))>0){
        $date_elements=explode(" ",$date);
        list($check_year,$check_month,$check_day)=explode("-",$date_elements[0]);
        $date_correct=checkdate($check_month,$check_day,$check_year);
        if(strlen(trim($date_elements[1]))>0){
            list($hour,$min,$sec)=explode(":",$date_elements[1]);
            $time_correct=checktime($hour,$min,$sec);
        }
    }
    if($checktime){
        return $date_correct && $time_correct;
    }
    else{
        return $date_correct;
    }
}
function checktime($hour, $min, $sec) {
    if ($hour < 0 || $hour > 23 || !is_numeric($hour)) {
        return false;
    }
    if ($min < 0 || $min > 59 || !is_numeric($min)) {
        return false;
    }
    if ($sec < 0 || $sec > 59 || !is_numeric($sec)) {
        return false;
    }
    return true;
}
?>