<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
include("../config.inc.php");
$dbh=fncOpenDBConn();
$id=isset($_GET["id"])?$_GET["id"]:"";

$trans_date=isset($_GET["trans_date"])?$_GET["trans_date"]:"";

$trans_time=isset($_GET["trans_time"])?$_GET["trans_time"]:"";

$receipt_number=isset($_GET["receipt_number"])?$_GET["receipt_number"]:"";

$item_code=isset($_GET["item_code"])?$_GET["item_code"]:"";

$barcode=isset($_GET["barcode"])?$_GET["barcode"]:"";

$item_description=isset($_GET["item_description"])?$_GET["item_description"]:"";

$quantity=isset($_GET["quantity"])?$_GET["quantity"]:"";

$unit_price=isset($_GET["unit_price"])?$_GET["unit_price"]:"";

$sale_value=isset($_GET["sale_value"])?$_GET["sale_value"]:"";

$tax=isset($_GET["tax"])?$_GET["tax"]:"";

$beneficiary_id=isset($_GET["beneficiary"])?$_GET["beneficiary"]:"";

$packaging=isset($_GET["packaging"])?$_GET["packaging"]:"";

$payment_method=isset($_GET["payment_method"])?$_GET["payment_method"]:"";

$merchant=isset($_GET["merchant"])?$_GET["merchant"]:"";
    $sql="insert into merchant_sales_201611_test"
    ."(Trans_Date,Trans_Time,Receipt_No,Customer_ID,Barcode,Material_Code,Item_Description,
    Quantity,Unit_Price,VAT,Total_Amount,Merchant,Payment_Type,Packaging) "
    ."VALUES (N'$trans_date',N'$trans_time',N'$receipt_number',N'$beneficiary_id',N'$barcode',N'$item_code',N"
    .sqlvalue2($item_description,true).",".floatval($quantity).",".floatval($unit_price).","
    .floatval($tax).",".floatval($sale_value).",N'$merchant',N'$payment_method',N'$packaging')";
    $res=mssql_query($sql,$dbh);
    if($res){
        fncDeliverResponse("200","The price has been succcessfully updated ",$id);
    }
    else{
        fncDeliverResponse("400","An error occured while uploading the file ".$sql,$id);
    }

