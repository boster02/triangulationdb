<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
include("../config.inc.php");
include("fncApi.inc.php");
$dbh=fncOpenDBConn();
$msg="\r\n----------STACK TRACE------------\r\n";
$msg.="Number of POST variables: ".count($_POST)."\r\n";
$msg.="POST variable names:".implode(",", array_keys($_POST))."\r\n";
$msg.="Number of GET variables".count($_GET)."\r\n";
$msg.="GET variable names:".implode(",", array_keys($_GET))."\r\n";
$msg.="\r\nChecking POST data... \r\n";
$dateupload=isset($_POST["cycle"])?$_POST["cycle"]:"";
if(intval($dateupload)>0)
    $msg.="cycle is found\r\n";
else
    $msg.="cycle is not found\r\n";

$data=isset($_POST["data"])?$_POST["data"]:"";
if(isset($_POST["data"]))
    $msg.="Data records have been found\r\n";
else
    $msg.="Data records are not found\r\n";
$lines=explode(PHP_EOL, $_POST['data']);
$msg.="Number of records submitted: ".count($lines)."\r\n";
$fields=explode(",",strtoupper($lines[0]));
$msg.="Number of fields found: ".count($fields)."\r\n";
$msg.="Searching for fields ...\r\n";
$a=0;
$b=0;
$r=0;

$proc_date_pos=fncGetFieldPosition('WTR_PROC_DATE',$fields);
$trans_date_pos=fncGetFieldPosition('WTR_PURC_DATE',$fields);
$trans_time_pos=fncGetFieldPosition('WTR_TRANS_TIME',$fields);
$transaction_type_pos=fncGetFieldPosition('WTR_TCO_LABE',$fields);
$card_number_pos=fncGetFieldPosition('WTR_CARD_NUMB',$fields);
$customer_id_pos=fncGetFieldPosition('WTR_CUS_IDEN',$fields);
$account_number_pos=fncGetFieldPosition('WTR_ACC_NUMB_MXP',$fields);
$prepaid_program_pos=fncGetFieldPosition('WTR_PPR_LABE',$fields);
$trans_amount_pos=fncGetFieldPosition('WTR_TRAN_AMOU',$fields);
$trans_currency_pos=fncGetFieldPosition('WTR_ALPH_CODE',$fields);
$merchant_pos=fncGetFieldPosition('WTR_MERC_NAME',$fields);
$auth_no_pos=fncGetFieldPosition('WTR_AUTH_CODE',$fields);
$aouthno2_pos=fncGetFieldPosition('WTR_TRANS_CON',$fields);
$trans_num_pos=fncGetFieldPosition('WTR_IDEN_TRANS_NUM',$fields);
$vtr_id_pos=fncGetFieldPosition('WTR_VTR_ID',$fields);
$retailer_id_pos=fncGetFieldPosition('WTR_MERC_IDEN',$fields);
$terminal_id_pos=fncGetFieldPosition('WTR_TERM_ID',$fields);
$rrn_pos=fncGetFieldPosition('WTR_RRN',$fields);

for($i=1;$i<count($lines)-1;$i++){
    $r++;
    $stream = fopen('data://text/plain,' . $lines[$i],'r');
    $data=fgetcsv($stream, 5000, ",");
    $proc_date=$proc_date_pos>=0?$data[$proc_date_pos]:'';
    $trans_date=$trans_date_pos>=0?$data[$trans_date_pos]:'';
    $trans_time=$trans_time_pos>=0?$data[$trans_time_pos]:'';
    $transaction_type=$transaction_type_pos>=0?$data[$transaction_type_pos]:'';
    $card_number=$card_number_pos>=0?$data[$card_number_pos]:'';
    $customer_id=$customer_id_pos>=0?$data[$customer_id_pos]:'';
    $account_number=$account_number_pos>=0?$data[$account_number_pos]:'';
    $prepaid_program=$prepaid_program_pos>=0?$data[$prepaid_program_pos]:'';
    $trans_amount=$trans_amount_pos>=0?$data[$trans_amount_pos]:'';
    $trans_currency=$trans_currency_pos>=0?$data[$trans_currency_pos]:'';
    $merchant=$merchant_pos>=0?$data[$merchant_pos]:'';
    $auth_no=$auth_no_pos>=0?$data[$auth_no_pos]:'';
    $aouthno2=$aouthno2_pos>=0?$data[$aouthno2_pos]:'';
    $trans_num=$trans_num_pos>=0?$data[$trans_num_pos]:'';
    $vtr_id=$vtr_id_pos>=0?$data[$vtr_id_pos]:'';
    $retailer_id=$retailer_id_pos>=0?$data[$retailer_id_pos]:'';
    $terminal_id=$terminal_id_pos>=0?$data[$terminal_id_pos]:'';
    $rrn=$rrn_pos>=0?$data[$rrn_pos]:'';

    $merchant = str_replace("'"," ", $merchant);
    $proc_datele = substr($trans_date,0,2);
    if ($proc_datele == '20'){
        $check_date = substr($trans_date,0,6);
        if ($check_date != $dateupload){
            sqlsrv_rollback($dbh);
            fncDeliverResponse("400","Invalid date found '".$trans_date."' which does not belong to the cycle ".$dateupload."\r\n".$msg,"0");
            exit;
        }
        $transmonth=substr($trans_date,0,6);
        $dbtable="";
        $auth="";
        $authval="";
        $cycle="";
        $cycleval="";
        $ext="";
        $valid=false;
        switch($transaction_type){
            case "Pre-Paid Reload":
                $ext="_$transmonth";
                $dbtable="pre_paid_reload_meps";
                $valid=true;
                break;
            case "Sales Draft":
                $ext="_$transmonth";
                $dbtable="sales_draft";
                $auth=",merchant,auth_no,trans_num,retailer_id,terminal_id,rrn";
                $authval=",'$merchant','$auth_no','$trans_num','$retailer_id','$terminal_id','$rrn'";
                $valid=true;
                break;
            default:
                $ext="";
                $cycle=",Cycle";
                $cycleval=",$transmonth";
                $dbtable="MEPS_Adjustment_Transactions";
                $auth=",merchant,auth_no,trans_num,retailer_id,terminal_id,rrn";
                $authval=",'$merchant','$auth_no','$trans_num','$retailer_id','$terminal_id','$rrn'";
                $valid=true;
                break;
        }
        if($valid){
            $a++;
            $sql="insert into $dbtable".$ext
            ."(proc_date,trans_date,trans_time,transaction_type,card_number,customer_id,"
            ."account_number,prepaid_program,trans_amount,"
            ."trans_currency$auth$cycle) values "
            ."('$proc_date','$trans_date','$trans_time','$transaction_type','$card_number',"
            ."'$customer_id','$account_number','$prepaid_program','$trans_amount',"
            ."'$trans_currency'$authval$cycleval)";
            $res=mssql_query($sql,$dbh);
            if ($res){
                $b++;
            }
            else{
                fncDeliverResponse("400","ERROR on line no. $r This query failed\r\n".$sql,"0");
                sqlsrv_rollback($dbh);
                exit;
            }
        }
        else{
            $msg.="Invalid record found on line no. $r. Possible cause - date format";
        }
    }
}

if ($a>0 && $a==$b){
    sqlsrv_commit($dbh);
    fncDeliverResponse("200","The data has been succcessfully updated ","0");
    $send=true;
}
else{
    $msg.= "No record uploaded. The file is not in the expected format";
    sqlsrv_rollback($dbh);
    $send=false;
    fncDeliverResponse("400","An error occured while uploading the file ".$msg,0);
}
mssql_close($dbh);

