<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
include("../config.inc.php");
include("fncApi.inc.php");
$dbh=fncOpenDBConn();
$msg="\r\n----------STACK TRACE------------";
$msg.="Checking POST data... \r\n";
$merchant=isset($_POST["merchant"])?$_POST["merchant"]:"";
if(intval($merchant)>0)
    $msg.="Merchant ID is found\r\n";
else
    $msg.="Merchant ID is not found\r\n";

$data=isset($_POST["data"])?$_POST["data"]:"";
if(isset($_POST["data"]))
    $msg.="Data records have been found\r\n";
else
    $msg.="Data records are not found\r\n";
$lines=explode(PHP_EOL, $_POST['data']);
$msg.="Number of records submitted: ".count($lines)."\r\n";
$fields=explode(",",strtolower($lines[0]));
$msg.="Number of fields found: ".count($fields)."\r\n";
$msg.="Searching for fields ...\r\n";
$a=0;
$b=0;
$r=0;
$id_pos=fncGetFieldPosition("id",$fields);
$receipt_number_pos=fncGetFieldPosition("receipt_number",$fields);
$trans_date_pos=fncGetFieldPosition("trans_date",$fields);
$trans_time_pos=fncGetFieldPosition("trans_time",$fields);
$item_code_pos=fncGetFieldPosition("item_code",$fields);
$item_description_pos=fncGetFieldPosition("item_description",$fields);
$quantity_pos=fncGetFieldPosition("quantity",$fields);
$unit_price_pos=fncGetFieldPosition("unit_price",$fields);
$sale_value_pos=fncGetFieldPosition("sale_value",$fields);
$tax_pos=fncGetFieldPosition("tax",$fields);
$beneficiary_id_pos=fncGetFieldPosition("beneficiary_id",$fields);
$barcode_pos=fncGetFieldPosition("barcode",$fields);
$payment_method_pos=fncGetFieldPosition("payment_method",$fields);
$packaging_pos=fncGetFieldPosition("packaging",$fields);

for($i=1;$i<count($lines)-1;$i++){
    $r++;
    $stream = fopen('data://text/plain,' . $lines[$i],'r');
    $data=fgetcsv($stream, 5000, ",");
    $id=$id_pos>=0?$data[$id_pos]:"";
    $receipt_number=$receipt_number_pos>=0?$data[$receipt_number_pos]:"";
    $trans_date=$trans_date_pos>=0?$data[$trans_date_pos]:"";
    $trans_time=$trans_time_pos>=0?$data[$trans_time_pos]:"";
    $item_code=$item_code_pos>=0?$data[$item_code_pos]:"";
    $item_description=$item_description_pos>=0?$data[$item_description_pos]:"";
    $quantity=$quantity_pos>=0?$data[$quantity_pos]:"";
    $unit_price=$unit_price_pos>=0?$data[$unit_price_pos]:"";
    $sale_value=$sale_value_pos>=0?$data[$sale_value_pos]:"";
    $tax=$tax_pos>=0?$data[$tax_pos]:"";
    $beneficiary_id=$beneficiary_id_pos>=0?$data[$beneficiary_id_pos]:"";
    $barcode=$barcode_pos>=0?$data[$barcode_pos]:"";
    $payment_method=$payment_method_pos>=0?$data[$payment_method_pos]:"";
    $packaging=$packaging_pos>=0?$data[$packaging_pos]:"";
    $trans_date=explode(" ",trim($trans_date))[0];
  //Checking date
    $date_elements=explode("-",$trans_date);
    $check_month="";$check_day="";$check_year="";
    if(count($date_elements)!=3){
        fncDeliverResponse("400","Error. The date format on record no. $r with ID:$id Reciept No. $receipt_number is invalid. Date format should be YYYY-MM-DD. \r\n $msg","0");
        sqlsrv_rollback($dbh);
        exit;
    }
    else{
        list($check_year,$check_month,$check_day)=$date_elements;
        if (checkdate($check_month,$check_day,$check_year) && strlen($item_description)>0){
            $a++;
            $trans_date=$check_year."-".$check_month."-".$check_day;
            $vat=floatval(str_replace("%","",$tax));
            $trans_time=substr(trim($trans_time),0,8);
            $sql="insert into merchant_sales_201611_test"
            ."(Trans_Date,Trans_Time,Receipt_No,Customer_ID,Barcode,Material_Code,Item_Description,
        Quantity,Unit_Price,VAT,Total_Amount,Merchant,Payment_Type,Packaging) "
            ."VALUES (N'$trans_date',N'$trans_time',N'$receipt_number',N'$beneficiary_id',N'$barcode',N'$item_code',N"
            .sqlvalue2($item_description,true).",".floatval($quantity).",".floatval($unit_price).","
            .floatval($vat).",".floatval($sale_value).",N'$merchant',N'$payment_method',N'$packaging')";
            $res=mssql_query($sql,$dbh);
            if ($res){
                $b++;
            }
            else{
                fncDeliverResponse("400","ERROR on line no. $r with ID:$id Reciept No. $receipt_number This query failed\r\n".$sql,"0");
                sqlsrv_rollback($dbh);
                exit;
            }
        }
    }
}

if ($a>0 && $a==$b){
    sqlsrv_commit($dbh);
    fncDeliverResponse("200","The data has been succcessfully updated ","0");
    $send=true;
}
else{
    $msg.= "No record uploaded. The file is not in the expected format";
    sqlsrv_rollback($dbh);
    $send=false;
    fncDeliverResponse("400","An error occured while uploading the file ".$msg,0);
}
mssql_close($dbh);

