<?php session_start();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Retailer sales</title>
    <link rel="stylesheet" href="../css/csstable.css" />
    <link rel="stylesheet" href="../css/jquery.dataTables.css" />
    <script type="text/javascript" src="../js/jquery-1.12.0.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/dataTables.tableTools.js"></script>
</head>

<body>
    <a style="float:right" class="green-btn" href="javascript:exportDetails()">Export to Excel</a>
    <br />
    <div id="detailscontainer" class="csstable-details">
        <table id="detailstable" class="display compact">
            <thead>
                <tr>
                    <td>Merchant</td>
                    <td>Trans Date</td>
                    <td>Trans Time</td>
                    <td>Receipt No</td>
                    <td>Customer ID</td>
                    <td>Barcode</td>
                    <td>Item Code</td>
                    <td>Item Description</td>
                    <td>Quantity</td>
                    <td>Unit Price</td>
                    <td>VAT</td>
                    <td>Total Amount</td>
                    <td>Payment Type</td>
                    <td>Packaging</td>
                </tr>
            </thead>
        </table>
    </div>

    <script type="text/javascript">
        fields = "Merchant,Trans_Date,Trans_Time,Receipt_No,Customer_ID,Barcode,"
                +"Material_Code,Item_Description,Quantity,Unit_Price,"
                +"VAT,Total_Amount,Payment_Type,Packaging,ID";
        $("#detailscontainer").show();
        $("#detailstable").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "pageLength": 25,
            "sAjaxSource": "report_details.php?fields=" + fields,
            "sPaginationType": "full_numbers"
        });
    </script>
</body>
</html>
