<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
include("../config.inc.php");
include("fncApi.inc.php");
$dbh=fncOpenDBConn();
$msg="\r\n----------STACK TRACE------------\r\n";
$msg.="Number of POST variables: ".count($_POST)."\r\n";
$msg.="POST variable names:".implode(",", array_keys($_POST))."\r\n";
$msg.="Number of GET variables".count($_GET)."\r\n";
$msg.="GET variable names:".implode(",", array_keys($_GET))."\r\n";
$msg.="\r\nChecking POST data... \r\n";
$dateupload=isset($_POST["cycle"])?$_POST["cycle"]:"";
$data=isset($_POST["data"])?$_POST["data"]:"";
if(isset($_POST["data"]))
    $msg.="Data records have been found\r\n";
else
    $msg.="Data records are not found\r\n";
$lines=explode(PHP_EOL, $_POST['data']);
$data_lines=count($lines)-2;
$msg.="Number of data records submitted: ".$data_lines."\r\n";
$fields=explode(",",strtoupper($lines[0]));
$msg.="Number of fields found: ".count($fields)."\r\n";
$msg.="Searching for fields ...\r\n";
$a=0;
$b=0;
$r=0;
$UNHCRCaseNumber_pos=fncGetFieldPosition('UNHCRCaseNumber',$fields);
$CardNumber_pos=fncGetFieldPosition('CardNumber',$fields);
$CardCaseNumber_pos=fncGetFieldPosition('CardCaseNumber',$fields);
$UserID_pos=fncGetFieldPosition('UserID',$fields);
$PicMatch_pos=fncGetFieldPosition('PicMatch',$fields);
$VerificationDate_pos=fncGetFieldPosition('VerificationDate',$fields);
$LocationID_pos=fncGetFieldPosition('LocationID',$fields);
$IsVerified_pos=fncGetFieldPosition('IsVerified',$fields);
$Notes_pos=fncGetFieldPosition('Notes',$fields);
$IsSubmitted_pos=fncGetFieldPosition('IsSubmitted',$fields);
$MachineName_pos=fncGetFieldPosition('MachineName',$fields);
$PhoneNumber_pos=fncGetFieldPosition('PhoneNumber',$fields);
$CertificateExpDate_pos=fncGetFieldPosition('CertificateExpDate',$fields);
$IsManual_pos=fncGetFieldPosition('IsManual',$fields);
$SubmitDate_pos=fncGetFieldPosition('SubmitDate',$fields);
for($i=1;$i<count($lines)-1;$i++){
    $r++;
    $stream = fopen('data://text/plain,' . $lines[$i],'r');
    $data=fgetcsv($stream, 5000, ",");
    $UNHCRCaseNumber=$UNHCRCaseNumber_pos>=0?$data[$UNHCRCaseNumber_pos]:'';
    $CardNumber=$CardNumber_pos>=0?$data[$CardNumber_pos]:'';
    $CardCaseNumber=$CardCaseNumber_pos>=0?$data[$CardCaseNumber_pos]:'';
    $UserID=$UserID_pos>=0?$data[$UserID_pos]:'';
    $PicMatch=$PicMatch_pos>=0?$data[$PicMatch_pos]:'';
    $VerificationDate=$VerificationDate_pos>=0?$data[$VerificationDate_pos]:'';
    $LocationID=$LocationID_pos>=0?$data[$LocationID_pos]:'';
    $IsVerified=$IsVerified_pos>=0?$data[$IsVerified_pos]:'';
    $Notes=$Notes_pos>=0?$data[$Notes_pos]:'';
    $IsSubmitted=$IsSubmitted_pos>=0?$data[$IsSubmitted_pos]:'';
    $MachineName=$MachineName_pos>=0?$data[$MachineName_pos]:'';
    $PhoneNumber=$PhoneNumber_pos>=0?$data[$PhoneNumber_pos]:'';
    $CertificateExpDate=$CertificateExpDate_pos>=0?$data[$CertificateExpDate_pos]:'';
    $IsManual=$IsManual_pos>=0?$data[$IsManual_pos]:'';
    $SubmitDate=$SubmitDate_pos>=0?$data[$SubmitDate_pos]:'';
    if (!fncValidateDate($VerificationDate,true)){
        sqlsrv_rollback($dbh);
        fncDeliverResponse("400","Invalid Validation Date found on line ".$r."\r\n".$msg,"0");
        exit;
    }
    if (!fncValidateDate($CertificateExpDate,false)){
        sqlsrv_rollback($dbh);
        fncDeliverResponse("400","Invalid Certificate Expiry Date found on line ".$r."\r\n".$msg,"0");
        exit;
    }
    if (!fncValidateDate($SubmitDate,true)){
        sqlsrv_rollback($dbh);
        fncDeliverResponse("400","Invalid Submission Date found on line ".$r."\r\n".$msg,"0");
        exit;
    }
    $transmonth=substr($trans_date,0,6);
    $a++;
    $sql="INSERT INTO [Validation].[VerificationDetails]
           ([UNHCRCaseNumber]
           ,[CardNumber]
           ,[CardCaseNumber]
           ,[UserID]
           ,[PicMatch]
           ,[VerificationDate]
           ,[LocationID]
           ,[IsVerified]
           ,[Notes]
           ,[IsSubmitted]
           ,[MachineName]
           ,[PhoneNumber]
           ,[CertificateExpDate]
           ,[IsManual]
           ,[SubmitDate])
     VALUES
           ('$UNHCRCaseNumber',
            '$CardNumber',
            '$CardCaseNumber',
            '$UserID',
            '$PicMatch',
            '$VerificationDate',
            '$LocationID',
            '$IsVerified',
            '$Notes',
            '$IsSubmitted',
            '$MachineName',
            '$PhoneNumber',
            '$CertificateExpDate',
            '$IsManual',
            '$SubmitDate'
            )";
    $res=mssql_query($sql,$dbh);
    if ($res){
        $b++;
    }
    else{
        fncDeliverResponse("400","ERROR on line no. $r This query failed\r\n".$sql,"0");
        sqlsrv_rollback($dbh);
        exit;
    }
}
if ($a>0 && $a==$b){
    sqlsrv_commit($dbh);
    fncDeliverResponse("200","The data has been succcessfully updated ","0");
    $send=true;
}
else{
    $msg.= "Number of records processed: $a \r\n";
    $msg.= "Number of valid records: $b \r\n";
    $msg.= "Not all records submitted are valid\r\n";
    sqlsrv_rollback($dbh);
    $send=false;
    fncDeliverResponse("400","An error occured while uploading the file ".$msg,0);
}
mssql_close($dbh);
