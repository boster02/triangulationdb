<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('MAX_EXECUTION_TIME', 900);
ini_set('memory_limit', '-1');
include("../config.inc.php");
include("../mails/fncMailSender.inc.php");
function fncSendDailyReport($day){
    $sql="Select 1 as #, 'All recorded tickets' as [Type], count(*) as Number,'100%' as [Percent] from callcenter_calls where date='$day'
        union Select 2 as #, 'Closed tickets' as [Type], count(*) as Number,str(count(*)*100/(select count(*) from callcenter_calls where date='$day'))+'%' as [Percent] from callcenter_calls where date='$day' and status=2
        union Select 3 as #, 'Open tickets' as [Type], count(*) as Number,str(count(*)*100/(select count(*) from callcenter_calls where date='$day'))+'%' as [Percent] from callcenter_calls where date='$day' and status=1
        union Select 4 as #, 'Referral tickets' as [Type], count(*) as Number,str(count(*)*100/(select count(*) from callcenter_calls where date='$day'))+'%' as [Percent] from callcenter_calls where date='$day' and referral=1
        union Select 5 as #, 'Tickets assigned to functional units' as [Type], count(*) as Number,str(count(*)*100/(select count(*) from callcenter_calls where date='$day'))+'%' as [Percent] from callcenter_calls where date='$day' and ResponsibleUnit is not null
        union Select 5 as #, 'Tickets assigned to staff' as [Type], count(*) as Number,str(count(*)*100/(select count(*) from callcenter_calls where date='$day'))+'%' as [Percent] from callcenter_calls where date='$day' and ResponsiblePerson is not null
        union Select 6 as #, 'Shop related calls' as [Type], count(*) as Number,str(count(*)*100/(select count(*) from callcenter_calls where date='$day'))+'%' as [Percent] from callcenter_calls where date='$day'  and ShopProblem is not null
        union Select 7 as #, 'Protection related calls' as [Type], count(*) as Number,str(count(*)*100/(select count(*) from callcenter_calls where date='$day'))+'%' as [Percent] from callcenter_calls where date='$day'  and ProtectionIssue is not null
        union Select 8 as #, 'Cash comparative study calls' as [Type], count(*) as Number,str(count(*)*100/(select count(*) from callcenter_calls where date='$day'))+'%' as [Percent] from callcenter_calls where date='$day'  and ComparativeStudy is not null
        ";
    $sql2="select Staff,Count(*) as Number from callcenter_calls where date='$day' group by Staff
        union all Select 'Total' as Staff,(Select count(*) callcenter_calls from callcenter_calls  where [date]='$day') as number";
    $sql3="select mainpurposedesc as Main_Purpose_of_the_Call,  count(*) as Number from callcenter_calls_view  where date='$day'
            group by mainpurposedesc";
    $txt="<div style='font-family:Calibri'><h3>Call Center End of Day Report for $day</h3>";
    $txt.=createTable($sql);
    $txt.="<br /><strong>Report by Main Purpose of the Call</strong>";
    $txt.=createTable($sql3);
    $txt.="<br /><strong>Report by Staff Member</strong>";
    $txt.=createTable($sql2);

    $success=fncSendEmailToAdmins("Call Center End of Day Report for $day",$txt);
    return $success;
}
function fncSendEmailToAdmins($subject,$message){
    $dbh=fncOpenDBConn();
    $sql ="select name,email from callcenter_admins where status='Y'";
    $res = mssql_query($sql,$dbh);
    $to[0][0]=0;
    $cc[0][0]=0;
    $bcc[0][0]=0;
    $to[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$to[0][0];$i++){
        $row = mssql_fetch_array($res);
        $to[$i][0]=$row["name"];
        $to[$i][1]=$row["email"];
	}
    mssql_close($dbh);
    return authMail($to, $cc, $bcc, $subject, $message);
}
function createTable($sql) {
    $dbh=fncOpenDBConn();
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $flag = false;
    $txt="";
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);
        if(!$flag) {
            $txt="<table border='1' cellpadding='3' style='border-collapse:collapse'>";
            $style='style="background-color: #003366;color: white;text-align: left"';
            $txt.="<tr><th $style>".implode("</th><th $style>", array_keys($row)) . "</th></tr>";
            $txt=str_replace("_"," ",$txt);
            $flag = true;
        }
        $txt.="\r\n<tr><td>".implode("</td><td>", array_values($row)) . "</td></tr>";
    }
    $txt.="    </table>";
    return $txt;
}
