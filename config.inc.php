<?php

function fncOpenDBConn(){
	$serverName = "10.67.67.130";
	$connectionInfo = array( "Database"=>"jordandb", "UID"=>"sa", "PWD"=>"P@ssword1","CharacterSet" => "UTF-8");
	$conn = sqlsrv_connect( $serverName, $connectionInfo );
	if( $conn === false ) {
    die( print_r( sqlsrv_errors(), true));
	}
	return $conn;
}
function mssql_query($sql,$conn){
	$params = array();
    $options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
    $stmt = sqlsrv_query($conn, $sql,$params,$options);
	/*if( $stmt === false) {
    die( print_r( sqlsrv_errors(), true) );
	}*/
	return $stmt;
}

function mssql_fetch_array($results){
	return sqlsrv_fetch_array($results, SQLSRV_FETCH_BOTH);
}

function mssql_fetch_row($results){
    return sqlsrv_fetch_array($results, SQLSRV_FETCH_NUMERIC);
}

function mssql_fetch_assoc($results){
    return sqlsrv_fetch_array($results, SQLSRV_FETCH_ASSOC);
}

function mssql_num_rows($results){
    $rows=sqlsrv_num_rows($results);
    if(!$rows)
        return 0;
    else
        return $rows;
}
function mssql_close($results){
	return sqlsrv_close($results);
}
function sqlvalue2($val, $quote)
{
    if ($quote)
        $tmp = sqlstr2($val);
    else
        $tmp = $val;
    if ($tmp == "")
        $tmp = "NULL";
    elseif ($quote)
        $tmp = "'".$tmp."'";
    return $tmp;
}

function sqlstr2($val)
{
    return str_replace("'", "''", $val);
}
function fncLogAccess2($activity,$user=""){
    $dbh=fncOpenDBConn();
    if(isset($_SESSION["name"]))
        $name=$_SESSION["name"];
    else
        $name=$user;
    $datetime=date("Y-m-d H:i:s");
    $sql="insert into activity_log (activity,doneby,datetime) values ('$activity','$name','$datetime')";
    mssql_query($sql,$dbh);
    mssql_close($dbh);
}
function fncDeliverResponse($status,$status_message,$id){
    $response["status"]=$status;
    $response["status_message"]=$status_message;
    $response["id"]=$id;

    $json_response=json_encode($response);
    echo $json_response;

}

?>