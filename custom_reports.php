<?php session_start();
      include("fncCashAnalyzer.inc.php");
      $reports=fncGetCustomReports();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Custom Reports</title>
    <link rel="stylesheet" type="text/css" href="css/c3.min.css">
    <link rel="stylesheet" type="text/css" href="css/chosen.min.css">
    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.4.js"></script>
    <script type="text/javascript" src="js/d3.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/jquery.csv-0.71.min.js"></script>
    <script type="text/javascript" src="js/chosen.jquery.js"></script>
    <script type="text/javascript" src="js/c3.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <!-- PivotTable.js libs from ../dist -->
    <link rel="stylesheet" type="text/css" href="css/pivot.css">
    <script type="text/javascript" src="js/pivot.js"></script>
    <script type="text/javascript" src="js/d3_renderers.js"></script>
    <script type="text/javascript" src="js/c3_renderers.js"></script>
    <script type="text/javascript" src="js/export_renderers.js"></script>
    <style>
        body {
            font-family: Calibri,Arial,Verdana;
        }

        .node {
            border: solid 1px white;
            font: 10px sans-serif;
            line-height: 12px;
            overflow: hidden;
            position: absolute;
            text-indent: 2px;
        }

        .c3-line, .c3-focused {
            stroke-width: 3px !important;
        }

        .c3-bar {
            stroke: white !important;
            stroke-width: 1;
        }

        .c3 text {
            font-size: 12px;
            color: grey;
        }

        .tick line {
            stroke: white;
        }

        .c3-axis path {
            stroke: grey;
        }

        .c3-circle {
            opacity: 1 !important;
        }

        #loadImg {
            position: absolute;
            z-index: 999;
        }

            #loadImg div {
                display: table-cell;
                width: 950px;
                height: 633px;
                background: #fff;
                text-align: center;
                vertical-align: middle;
            }
    </style>
    <script>
        //Special aggregators
        var aggr = [];
        aggr[4] = "DISTRIBUTIONS";
        aggr[5] = "BENEFICIARIES";
        aggr[7] = "HOUSEHOLDS";
        aggr[11] = "PERSONS";
        $(document).ready(function () {
            $("#loadImg").hide();
            $("#instruction").hide();
            $(function () {
                $("#dialog").dialog({
                    autoOpen: false,
                    maxWidth: 800,
                    maxHeight: 500,
                    width: 800,
                    height: 500,
                    modal: true
                });
            });

            $("#create-appt")
            .click(function () {
                $("#dialog").dialog("open");
            });
        });
        function changeReport(selObj) {
            $("#output").hide();
            $("#output4").hide();
            $("#output5").hide();
            $("#output7").hide();
            $("#output11").hide();
            $("#loadImg").show();
            var renderers = $.extend($.pivotUtilities.renderers,
            $.pivotUtilities.c3_renderers);
            var view_id = selObj.options[selObj.selectedIndex].value;
            $.get("saphana.php?id=" + view_id, function (mps) {
                var input = $.csv.toArrays(mps);
                var tpl = $.pivotUtilities.aggregatorTemplates;
                if (view_id == 4 || view_id == 5 || view_id == 7 || view_id == 11) {
                    $("#output" + view_id).show();
                    $("#output" + view_id).pivotUI(input,
                        {
                            renderers: renderers,
                            aggregators: {
                                "Count": function () { return tpl.sum()([aggr[view_id]]) },
                            }
                        });
                }
                else {
                    $("#output").show();
                    $("#output").pivotUI(input,
                        {
                            renderers: renderers
                        });
                }
                $("#loadImg").hide();
                $("#instruction").show();
            });
        }
    </script>
</head>


<body>
    <h1 style="text-align: center">Create your own report</h1>
    Select Table:
    <select name="custom_report" onchange="changeReport(this)">
        <option value=""></option>
        <?php for($i=1;$i<=$reports[0][0];$i++){ ?>
        <option value="<?php echo $reports[$i]["id"] ?>"><?php echo $reports[$i]["source"].": ".$reports[$i]["description"]?></option>
        <?php } ?>
    </select>
    <div id="loadImg">Please wait... Loading data
        <div><img src="images/download.gif"/></div>
    </div>
    <div id="instruction">Drag fields to the row or column panels. <a href="javascript:void(0)" id="create-appt">Show me!</a></div>
    <div id="dialog" title="How to create a report">
        <img src="images/howto.gif" />
    </div>
    <div id="output" style="margin: 30px;display:none"></div>
    <div id="output4" style="margin: 30px;display:none"></div>
    <div id="output5" style="margin: 30px;display:none"></div>
    <div id="output7" style="margin: 30px;display:none"></div>
    <div id="output11" style="margin: 30px;display:none"></div>
</body>
</html>
