<div id="my_connectivity_panel" class="pathwindow connectivity active">
    <div class="pathwindow_title">
        <div class="icon"></div>
        <a class="btn_back"><span></span>
            <p>Back</p>
        </a>
        <h1>Dashboard</h1>
    </div>
    <div class="pathwindow_content">
        <style>
            #loadImg {
                position: absolute;
                z-index: 999;
            }

                #loadImg div {
                    display: table-cell;
                    width: 100%;
                    height: 633px;
                    background: #fff;
                    text-align: center;
                    vertical-align: middle;
                }
        </style>
        <div id="loadImg">
            <div>
                <img src="images/download.gif" /></div>
        </div>
        <iframe name="iframe" src="DashboardContent.php" class="contentframe" style="width:100%" onload="document.getElementById('loadImg').style.display='none'"></iframe>
    </div>
</div>
