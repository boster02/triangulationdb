<?php session_start();
      include("fncCashAnalyzer.inc.php");
?>
<style>
    #menu ul {
        margin-left: 20px;
        list-style-image: url('images/data_icon.png');
    }

        #menu ul li {
            font-size: 16px;
            font-family: 'Trebuchet MS', Geneva, sans-serif;
        }
</style>
<div id="my_connectivity_panel" class="pathwindow connectivity active">
    <div class="pathwindow_title">
        <div class="icon"></div>
        <a class="btn_back"><span></span>
            <p>Back</p>
        </a>
        <h1>Data Management</h1>
    </div>
    <div class="pathwindow_content">
        <div class="arrow-list" id="menu">
            <ul>
                <li><a href="<?php echo fncPermLink("javascript:showMerchants()","MC","acdata") ?>">Update Merchant Contracts</a></li>
                <li>
                    <a href="<?php echo fncPermLink("javascript:showMerchantUploads()","MC","acdata") ?>">View Merchant Uploads (Jordan)</a>
                </li>
                <li>
                    <a href="<?php echo fncPermLink("javascript:showLebanonUploads()","LB","acdata") ?>">View Merchant Uploads (Lebanon)</a>
                </li>
                <li><a href="<?php echo fncPermLink("javascript:showFinancialContracts()","FI","acdata") ?>">Update Financial Service Provider Contracts</a></li>
                <li><a href="<?php echo fncPermLink("javascript:showQueries()","FI","acdata") ?>">Manage Queries</a></li>
                <li><a href="<?php echo fncPermLink("javascript:showSetup()","FI","acdata") ?>">Setup</a></li>
            </ul>
        </div>
        <a id="back_button" href="javascript:showMenu()" style="display: none">
            <img src="images/back_to_menu.png" alt="back" /></a>
        <div id="merchants" style="display: none">
            <iframe src="merchant_contracts/merchants.php" class="dataframe" style="width: 100%" id="frame1"></iframe>
        </div>
        <div id="financial_contracts" style="display: none">
            <iframe src="financial_contracts/financial_contracts.php" class="dataframe" style="width: 100%" id="frame2"></iframe>
        </div>
        <div id="merchant_uploads" style="display: none">
            <iframe src="uploads/Retailer_Files/index.php" class="dataframe" style="width: 100%" id="frame5"></iframe>
        </div>
        <div id="lebanon_uploads" style="display: none">
            <iframe src="uploads/Retailer_Files/Lebanon/.index.php" class="dataframe" style="width: 100%" id="frame5"></iframe>
        </div>
        <div id="queries" style="display: none">
            <iframe src="queries/queries.php" class="dataframe" style="width: 100%" id="frame4"></iframe>
        </div>
        <div id="setup" style="display: none">
            <iframe src="setup.php" class="dataframe" style="width: 100%" id="frame3"></iframe>
        </div>
        <div class="clear"></div>
    </div>
</div>
<script>
    function showMenu() {
        $("#menu").show();
        $("#merchants").hide();
        $("#financial_contracts").hide();
        $("#setup").hide();
        $("#queries").hide();
        $("#back_button").hide();
        $("#merchant_uploads").hide();
        $("#lebanon_uploads").hide();
    }
    function showMerchants() {
        $("#menu").hide();
        $("#merchants").show();
        $("#back_button").show();
    }
    function showFinancialContracts() {
        $("#menu").hide();
        $("#financial_contracts").show();
        $("#back_button").show();
    }
    function showSetup() {
        $("#menu").hide();
        $("#setup").show();
        $("#back_button").show();
    }
    function showQueries() {
        $("#menu").hide();
        $("#queries").show();
        $("#back_button").show();
    }
    function showMerchantUploads() {
        $("#menu").hide();
        $("#merchant_uploads").show();
        $("#back_button").show();
    }
    function showLebanonUploads() {
        $("#menu").hide();
        $("#lebanon_uploads").show();
        $("#back_button").show();
    }
</script>
