<?php function showroweditor($row, $iseditmode)
      {
          global $conn;
?>
<table class="table table-striped table-bordered" border="0" cellspacing="1" cellpadding="5" style="width: 400px">
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Contractor Name")."&nbsp;" ?></td>
        <td class="dr">
            <input class="form-control" type="text" name="Contractor_Name" maxlength="50" value="<?php echo str_replace("�","&#39", trim($row["Contractor_Name"])) ?>" /></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Start Date")."&nbsp;" ?></td>
        <td class="dr">
            <input class="form-control datepicker" type="text" name="Start_Date" maxlength="10" value="<?php echo str_replace("�","&#39", trim($row["Start_Date"])) ?>" /></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("End Date")."&nbsp;" ?></td>
        <td class="dr">
            <input class="form-control datepicker" type="text" name="End_Date" maxlength="10" value="<?php echo str_replace("�","&#39", trim($row["End_Date"])) ?>" /></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Services Contracted For")."&nbsp;" ?></td>
        <td class="dr">
            <input class="form-control"  name="Services_Contracted_For" maxlength="255" value="<?php echo str_replace("�","&#39", trim($row["Services_Contracted_For"])) ?>" /></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Contract File")."&nbsp;" ?></td>
        <td class="dr">
            <input class="form-control" type="file" name="Contract_File" /></td>
    </tr>
</table>
          <input type="hidden" name="Updated_By" value="<?php echo $_SESSION["name"] ?>"/>
          <input type="hidden" value="<?php echo date('Y-m-d') ?>" name="Date_Updated" />
<?php } ?>
