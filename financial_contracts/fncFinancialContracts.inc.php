<?php 
include("../fncDataFunctions.inc.php");
?>
<?php function showpagenav($page, $pagecount)
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="financial_contracts.php?a=add">Add Record</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="financial_contracts.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Prev</a>&nbsp;</td>
<?php } ?>
<?php
          global $pagerange;

          if ($pagecount > 1) {

              if ($pagecount % $pagerange != 0) {
                  $rangecount = intval($pagecount / $pagerange) + 1;
              }
              else {
                  $rangecount = intval($pagecount / $pagerange);
              }
              for ($i = 1; $i < $rangecount + 1; $i++) {
                  $startpage = (($i - 1) * $pagerange) + 1;
                  $count = min($i * $pagerange, $pagecount);

                  if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
                      for ($j = $startpage; $j < $count + 1; $j++) {
                          if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="financial_contracts.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php }
                      }
                  } else { ?>
<td><a href="financial_contracts.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php }
              }
          } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="financial_contracts.php?page=<?php echo $page + 1 ?>">Next&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="financial_contracts.php">List of contracts</a></td>
</tr>
</table>
<?php } ?>

<?php function addrec()
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="financial_contracts.php">List of contracts</a></td>
</tr>
</table>
<form enctype="multipart/form-data" action="financial_contracts.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
          $row = array(
            "ID" => "",
            "Contractor_Name" => "",
            "Start_Date" => "",
            "End_Date" => "",
            "Services_Contracted_For" => "",
            "Contract_File" => "",
            "Updated_By" => "",
            "Date_Updated" => "");
          showroweditor($row, false);
?>
<p><input class="btn btn-primary" type="submit" name="action" value="Save" /></p>
<br />    
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("view", $recid, $count);
?>
<?php showrow($row, $recid) ?>

<?php
  
} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("edit", $recid, $count);
?>
<form enctype="multipart/form-data" action="financial_contracts.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xID" value="<?php echo $row["ID"] ?>">
<?php showroweditor($row, true); ?>
<p><input class="btn btn-primary" type="submit" name="action" value="Save" /></p>
    <br />
</form>
<?php
  
} ?>

<?php function deleterec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("del", $recid, $count);
?>
<form action="financial_contracts.php" method="post">
<input type="hidden" name="sql" value="delete">
<input type="hidden" name="xID" value="<?php echo $row["ID"] ?>">
<?php showrow($row, $recid) ?>
<p><input class="btn btn-danger" type="submit" name="action" value="Confirm"></p>
    <br />
</form>
<?php
  
} 
      function sql_select()
      {
          global $conn;
          global $order;
          global $ordtype;
          global $filter;
          global $filterfield;
          global $wholeonly;

          $filterstr = sqlstr($filter);
          if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
          $sql = "SELECT ID, Contractor_Name, Start_Date, End_Date, Services_Contracted_For, Contract_File, Updated_By, Date_Updated FROM financial_contracts";
          if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
              $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
          } elseif (isset($filterstr) && $filterstr!='') {
              $sql .= " where (Contractor_Name like '" .$filterstr ."') or (Start_Date like '" .$filterstr ."') or (End_Date like '" .$filterstr ."') or (Services_Contracted_For like '" .$filterstr ."') or (Contract_File like '" .$filterstr ."') or (Updated_By like '" .$filterstr ."') or (Date_Updated like '" .$filterstr ."')";
          }
          if (isset($order) && $order!='') $sql .= " order by " .sqlstr($order) ."";
          if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
          $res = mssql_query($sql, $conn);
          fncLogAccess("Viewed the list of financial contracts");
          return $res;
      }

      function sql_getrecordcount()
      {
          global $conn;
          global $order;
          global $ordtype;
          global $filter;
          global $filterfield;
          global $wholeonly;

          $filterstr = sqlstr($filter);
          if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
          $sql = "SELECT COUNT(*) FROM financial_contracts";
          if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
              $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
          } elseif (isset($filterstr) && $filterstr!='') {
              $sql .= " where (Contractor_Name like '" .$filterstr ."') or (Start_Date like '" .$filterstr ."') or (End_Date like '" .$filterstr ."') or (Services_Contracted_For like '" .$filterstr ."') or (Contract_File like '" .$filterstr ."') or (Updated_By like '" .$filterstr ."') or (Date_Updated like '" .$filterstr ."')";
          }
          $res = mssql_query($sql, $conn);
          $row = mssql_fetch_array($res);
          reset($row);
          return current($row);
      }

      function sql_insert()
      {
          global $conn;
          global $_POST;

          $file=uploadfile("uploads/","Contract_File");
          $sql = "insert into financial_contracts (Contractor_Name, Start_Date, End_Date, 
                  Services_Contracted_For, Contract_File, Updated_By, Date_Updated) values (" 
                  .sqlvalue(@$_POST["Contractor_Name"], true).", " .sqlvalue(@$_POST["Start_Date"], true).", "
                  .sqlvalue(@$_POST["End_Date"], true).", " .sqlvalue(@$_POST["Services_Contracted_For"], true).", " 
                  .sqlvalue($file, true).", " .sqlvalue(@$_POST["Updated_By"], true).", " 
                  .sqlvalue(@$_POST["Date_Updated"], true).")";
          mssql_query($sql, $conn);
          fncLogAccess("Added a new financial contract for  - ".sqlvalue(@$_POST["Contractor_Name"], true));
      }

      function sql_update()
      {
          global $conn;
          global $_POST;

          $file=uploadfile("uploads/","Contract_File");
          $uploadstr=($file=="")?"":",Contract_File=" .sqlvalue($file, true);
          $sql = "update financial_contracts set Contractor_Name=" .sqlvalue(@$_POST["Contractor_Name"], true).", Start_Date=" .sqlvalue(@$_POST["Start_Date"], true).", End_Date=" .sqlvalue(@$_POST["End_Date"], true).", Services_Contracted_For=" .sqlvalue(@$_POST["Services_Contracted_For"], true).", Updated_By=" .sqlvalue(@$_POST["Updated_By"], true).", Date_Updated=" .sqlvalue(@$_POST["Date_Updated"], true) ."$uploadstr where " .primarykeycondition();
          mssql_query($sql, $conn);
          fncLogAccess("Updated financial contract for  - ".sqlvalue(@$_POST["Contractor_Name"], true));
      }

      function sql_delete()
      {
          global $conn;
          $sql="select Contractor_Name from financial_contracts where ".primarykeycondition();
          $res=mssql_query($sql, $conn);
          $row=mssql_fetch_array($res);
          $sql = "delete from financial_contracts where " .primarykeycondition();
          mssql_query($sql, $conn);
          fncLogAccess("Deleted financial contract for  - ".$row["Contractor_Name"]);
      }
      function primarykeycondition()
      {
          global $_POST;
          $pk = "";
          $pk .= "(ID";
          if (@$_POST["xID"] == "") {
              $pk .= " IS NULL";
          }else{
              $pk .= " = " .sqlvalue(@$_POST["xID"], false);
          };
          $pk .= ")";
          return $pk;
      }
?>
