<?php function select()
      {
          global $a;
          global $showrecs;
          global $page;
          global $filter;
          global $filterfield;
          global $wholeonly;
          global $order;
          global $ordtype;


          if ($a == "reset") {
              $filter = "";
              $filterfield = "";
              $wholeonly = "";
              $order = "";
              $ordtype = "";
          }

          $checkstr = "";
          if ($wholeonly) $checkstr = " checked";
          if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
          $res = sql_select();
          $count = sql_getrecordcount();
          if ($count % $showrecs != 0) {
              $pagecount = intval($count / $showrecs) + 1;
          }
          else {
              $pagecount = intval($count / $showrecs);
          }
          $startrec = $showrecs * ($page - 1);
          if ($startrec < $count) {sqlsrv_fetch($res, $startrec);}
          $reccount = min($showrecs * $page, $count);
?>
<style type="text/css">
    td a {
        color: white;
        text-align: left;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-md-10">
            <form action="financial_contracts.php" method="post">
                <table class="filter-table" border="0" cellspacing="1" cellpadding="4">
                    <tr>
                        <td><b>Custom Filter</b>&nbsp;</td>
                        <td>
                            <input type="text" name="filter" value="<?php echo $filter ?>"></td>
                        <td>
                            <select name="filter_field">
                                <option value="">All Fields</option>
                                <option value="<?php echo "Contractor_Name" ?>"<?php if ($filterfield == "Contractor_Name") { echo "selected"; } ?>><?php echo htmlspecialchars("Contractor Name") ?></option>
                                <option value="<?php echo "Start_Date" ?>"<?php if ($filterfield == "Start_Date") { echo "selected"; } ?>><?php echo htmlspecialchars("Start Date") ?></option>
                                <option value="<?php echo "End_Date" ?>"<?php if ($filterfield == "End_Date") { echo "selected"; } ?>><?php echo htmlspecialchars("End Date") ?></option>
                                <option value="<?php echo "Services_Contracted_For" ?>"<?php if ($filterfield == "Services_Contracted_For") { echo "selected"; } ?>><?php echo htmlspecialchars("Services Contracted For") ?></option>
                                <option value="<?php echo "Contract_File" ?>"<?php if ($filterfield == "Contract_File") { echo "selected"; } ?>><?php echo htmlspecialchars("Contract File") ?></option>
                            </select></td>
                        <td>
                            <input type="checkbox" name="wholeonly"<?php echo $checkstr ?>>Whole words only</td>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" name="action" value="Apply Filter"></td>
                        <td><a href="financial_contracts.php?a=reset">Reset Filter</a></td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-sm-2 col-md-2" style="text-align: right">
            <a href="financial_contracts.php?a=add" class="btn btn-primary">Add New Merchant</a>
        </div>
    </div>
    <hr size="1" noshade />
    <div class="csstable">
        <table class="tbl" border="0" cellspacing="1" cellpadding="5" width="100%">
            <tr>
                <td class="hr"><a class="hr" href="financial_contracts.php?order=<?php echo "Contractor_Name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Contractor Name") ?></a></td>
                <td class="hr"><a class="hr" href="financial_contracts.php?order=<?php echo "Start_Date" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Start Date") ?></a></td>
                <td class="hr"><a class="hr" href="financial_contracts.php?order=<?php echo "End_Date" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("End Date") ?></a></td>
                <td class="hr"><a class="hr" href="financial_contracts.php?order=<?php echo "Services_Contracted_For" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Services Contracted For") ?></a></td>
                <td class="hr"><a class="hr" href="financial_contracts.php?order=<?php echo "Contract_File" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Contract File") ?></a></td>
                <td class="hr">&nbsp;</td>
                <td class="hr">&nbsp;</td>
                <td class="hr">&nbsp;</td>
            </tr>
            <?php
          for ($i = $startrec; $i < $reccount; $i++)
          {
              $row = mssql_fetch_array($res);
            ?>
            <tr>
                <td><?php echo htmlspecialchars($row["Contractor_Name"]) ?></td>
                <td><?php echo htmlspecialchars($row["Start_Date"]) ?></td>
                <td><?php echo htmlspecialchars($row["End_Date"]) ?></td>
                <td><?php echo htmlspecialchars($row["Services_Contracted_For"]) ?></td>
                <td><a style="color:blue" href="uploads/<?php echo htmlspecialchars($row["Contract_File"]) ?>" target="_blank"><?php echo htmlspecialchars($row["Contract_File"]) ?></a></td>
                <td style="width: 18px"><a href="financial_contracts.php?a=view&recid=<?php echo $i ?>">
                    <img src="../images/view.png" alt="view" /></a></td>
                <td style="width: 18px"><a href="financial_contracts.php?a=edit&recid=<?php echo $i ?>">
                    <img src="../images/edit-bw.png" alt="edit" /></a></td>
                <td style="width: 18px"><a href="financial_contracts.php?a=del&recid=<?php echo $i ?>">
                    <img src="../images/delete.png" alt="delete" /></a></td>
            </tr>
            <?php
          }
          
            ?>
        </table>
        <br />
    </div>
</div>
<?php } ?>