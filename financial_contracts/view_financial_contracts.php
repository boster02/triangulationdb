<?php function showrow($row, $recid)
      {
?>
<table class="table table-striped table-bordered" style="width: 400px">
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Contractor Name")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["Contractor_Name"]) ?></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Start Date")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["Start_Date"]) ?></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("End Date")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["End_Date"]) ?></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Services Contracted For")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["Services_Contracted_For"]) ?></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Contract File")."&nbsp;" ?></td>
        <td class="dr"><a href="<?php echo htmlspecialchars($row["Contract_File"]) ?>"><?php echo htmlspecialchars($row["Contract_File"]) ?></a></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Updated By")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["Updated_By"]) ?></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Date Updated")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["Date_Updated"]) ?></td>
    </tr>
</table>
<?php } ?>
