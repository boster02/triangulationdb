<?php header('Content-Type: text/html; charset=utf-8');
ini_set('MAX_EXECUTION_TIME', 9000);
include "config.inc.php";
$redirectUri = '//' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
if(!isset($_SESSION["name"])){
    fncShowMessage("You are not logged in","/index.php?returnurl=".$redirectUri);
}


function fncImportCSV_Reload_WFP($name,$month,$year){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$a=0;
	$b=0;
    if ( sqlsrv_begin_transaction( $dbh ) === false ) {
        die( print_r( sqlsrv_errors(), true ));
    }
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            list ($Beneficiary_ID,$household_size,$location,$governorate,$reload_percent,$trans_amount)=$data;
			if (is_numeric(substr($Beneficiary_ID,0,1))){
                //$check_date = substr($trans_date,0,6);
                //if ($check_date != $dateupload){
                //    sqlsrv_rollback($dbh);
                //    echo "Kindly delete this date in the CSV and try again. '<b>".$check_date."' </b>";
                //    exit;
                //}
				$a++;
                $trans_date=$dateupload."01";
                $household_size=floatval($household_size);
                $location=sqlstr2($location);
                $governorate=sqlstr2($governorate);
                $reload_percent=floatval($reload_percent);
                $trans_amount=floatval($trans_amount);
				$sql="insert into pre_paid_reload_wfp_$dateupload 	"
				."(trans_date,Beneficiary_ID,household_size,location,governorate,reload_percent,trans_amount,source) values "
				."('$trans_date','$Beneficiary_ID','$household_size','$location','$governorate','$reload_percent','$trans_amount','Programme')";
				$res=mssql_query($sql,$dbh);
				if ($res){
					$b++;
				}
				else{
					echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
					exit;
				}
			}
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}

function fncImportCSV_Reload_LOA($arch_fijo,$month,$year,$location,$trans_amount,$trans_date){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$a=0;
	$b=0;
    if ( sqlsrv_begin_transaction( $dbh ) === false ) {
        die( print_r( sqlsrv_errors(), true ));
    }
    if (isset($arch_fijo) && isset($month) && isset($year) && isset($location) && isset($trans_amount) && isset($trans_date)) {
        $a++;
        $location=sqlstr2($location);
        $trans_amount=floatval($trans_amount);
        $sql="insert into pre_paid_reload_loa 	"
        ."(cycle,trans_date,location,trans_amount,file_path) values "
        ."('$dateupload','$trans_date','$location','$trans_amount','$arch_fijo')";
        $res=mssql_query($sql,$dbh);
        if ($res){
            $b++;
        }
        else{
            echo "ERROR: This query failed<br /><br />".$sql;
            sqlsrv_rollback($dbh);
            exit;
        }
    }
    else{
        echo "ERROR: A required field had no data";
        sqlsrv_rollback($dbh);
        exit;
    }

	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}


// upload for MEPS Data
function fncImportCSV_MEPS_Data($name,$month,$year){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$a=0;
	$b=0;
    sqlsrv_begin_transaction($dbh);
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            list ($proc_date,$trans_date,$trans_time,$transaction_type,$card_number,$customer_id,
            $account_number,$prepaid_program,$trans_amount,$trans_currency,$merchant,$auth_no,$aouthno2,$trans_num,$vtr_id,	$retailer_id,$terminal_id,$rrn)=$data;
            //In future add $tc and $tin in the array. For now we set them to null  to avoid notices in the output
			$merchant = str_replace("'"," ", $merchant);
			$proc_datele = substr($trans_date,0,2);
            if ($proc_datele == '20'){
                $check_date = substr($trans_date,0,6);
                if ($check_date != $dateupload){
                    sqlsrv_rollback($dbh);
                    echo "Invalid date found '<b>".$check_date."' </b> which does not belong to the selected month ".$dateupload;
                    exit;
                }
                $transmonth=substr($trans_date,0,6);
                $dbtable="";
                $auth="";
                $authval="";
                $cycle="";
                $cycleval="";
                $ext="";
                $valid=false;
                switch($transaction_type){
					case "Pre-Paid Reload":
                        $ext="_$transmonth";
						$dbtable="pre_paid_reload_meps";
						$valid=true;
						break;
					case "Sales Draft":
                        $ext="_$transmonth";
					    $dbtable="sales_draft";
                        $auth=",merchant,auth_no,trans_num,retailer_id,terminal_id,rrn";
                        $authval=",'$merchant','$auth_no','$trans_num','$retailer_id','$terminal_id','$rrn'";
						$valid=true;
						break;
					default:
                        $ext="";
                        $cycle=",Cycle";
                        $cycleval=",$transmonth";
					    $dbtable="MEPS_Adjustment_Transactions";
                        $auth=",merchant,auth_no,trans_num,retailer_id,terminal_id,rrn";
                        $authval=",'$merchant','$auth_no','$trans_num','$retailer_id','$terminal_id','$rrn'";
						$valid=true;
						break;
                }
                if($valid){
                    $a++;

                    $sql="insert into $dbtable".$ext
                    ."(proc_date,trans_date,trans_time,transaction_type,card_number,customer_id,"
                    ."account_number,prepaid_program,trans_amount,"
                    ."trans_currency$auth$cycle) values "
                    ."('$proc_date','$trans_date','$trans_time','$transaction_type','$card_number',"
                    ."'$customer_id','$account_number','$prepaid_program','$trans_amount',"
                    ."'$trans_currency'$authval$cycleval)";
                    $res=mssql_query($sql,$dbh);
                    if ($res){
                        $b++;
                    }
                    else{
                        echo "ERROR: This query failed<br /><br />".$sql;
                        sqlsrv_rollback($dbh);
                        exit;
                    }
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        $res=sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        $res=sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}

// upload for Merchant Sales
function fncImportCSV_Tazweed($name,$month,$year){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$today = date('Y-m-d');
	$a=0;
	$b=0;
    $r=0;
	sqlsrv_begin_transaction($dbh);
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            $r++;
            list ($invoice_no,$material_code,$barcode,$item_description,$qty,$cost_price,$vat,$unit_price,
                $total_sales,$customer_id,$card_number,$trans_date,$mastercard,$cash,$regular_voucher,
                $new_arival_voucher,$nrc_welcome_voucher,$nrc_welcome_voucher9,$stc_tod)=$data;
            if (substr($invoice_no,0,2) == '20'){
                list($check_day,$check_month,$check_year)=explode("/",$trans_date);
                if(strlen($check_month)<2) $check_month="0".$check_month;
                if(strlen($check_day)<2) $check_day="0".$check_day;
                if ($check_year.$check_month != $dateupload){
                    sqlsrv_rollback($dbh);
                    echo "Error on line no. $r! This date '<b>".$trans_date."' </b> is not for the selected month ".$dateupload;
                    exit;
                }
                $a++;
                $trans_date=$check_year."-".$check_month."-".$check_day;
                $vat=floatval(str_replace("%","",$vat));
                $item_description=sqlvalue2($item_description,true);
                $qty=floatval($qty);
                $unit_price=floatval($unit_price);
                $total_sales=floatval($total_sales);
                $item_cost=floatval($cost_price);
                $total_cost=$item_cost*$qty;
                $vat_value=$total_sales*$vat/100;
                $sales_without_vat=$total_sales-$vat_value;
                $sql="insert into merchant_sales_$dateupload"
                ."(Receipt_No,Trans_Date,Customer_ID,Card_Number,Barcode,Material_Code,Item_Description,
                    Quantity, Unit_price,VAT,Total_Amount,Item_Cost,Total_Cost,VAT_Value,Sales_Without_VAT,Merchant) "
                ."VALUES ('$invoice_no','$trans_date','$customer_id','$card_number','$barcode','$material_code',
                $item_description,$qty,$unit_price,'$vat',$total_sales,$item_cost,$total_cost,$vat_value,$sales_without_vat,'86')";
                $res=mssql_query($sql,$dbh);
                if ($res){
                    $b++;
                }
                else{
                    echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
                    exit;
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}

function fncImportCSV_Safeway($name,$month,$year){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$today = date('Y-m-d');
	$a=0;
	$b=0;
    $r=0;
	sqlsrv_begin_transaction($dbh);
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            $r++;
            list ($invoice_no,$transactiondate,$sku_code,$item_description,$quantity,$unit_price,$total_sales,
                $sales_without_vat,$total_cost,$vat_value,$month,$customer_id,$card_number,$payment_flag,$p_method,$barcode,$dept,
                $dept_name,$cat,$cat_name,$brand,$brand_name)=$data;
            if ($p_method == 'MASTER_CARD'){
                list($check_month,$check_day,$check_year)=explode("/",$transactiondate);
                $check_month=trim($check_month);
                $check_day=trim($check_day);
                $check_year=trim($check_year);
                if(strlen($check_year)==2) $check_year="20".$check_year;
                if(strlen($check_month)<2) $check_month="0".$check_month;
                if(strlen($check_day)<2) $check_day="0".$check_day;
                if ($check_year.$check_month != $dateupload){
                    sqlsrv_rollback($dbh);
                    echo "Error on line no. $r! This date '<b>".$transactiondate."</b> is not for the selected month ".$dateupload;
                    exit;
                }
                $a++;
                $trans_date=$check_year."-".$check_month."-".$check_day;
                $description=mb_convert_encoding($description,'UTF-8', 'UTF-8');
                $invoice_no=str_replace(",","",$invoice_no);
                $vat=floatval(str_replace("%","",$vat));
                $item_description=sqlvalue2($description,true);
                $qty=floatval($quantity);
                $total_sales=floatval($total_sales);
                $unit_price=$qty>0?$total_sales/$qty:"NULL";
                $total_cost=floatval($total_cost);
                $item_cost=floatval($item_cost);
                $vat_value=$total_sales*$vat/100;
                $sales_without_vat=$total_sales-$vat_value;
                $sales_without_vat=floatval($sales_without_vat);
                $sql="insert into merchant_sales_$dateupload"
                ." (Receipt_No,Trans_Date,Customer_ID,Card_Number,Barcode,Material_Code,Item_Description,
                    Quantity, Unit_price,VAT,Total_Amount,Item_Cost,Total_Cost,VAT_Value,Sales_Without_VAT,Merchant)"
                ."VALUES ('$invoice_no','$trans_date','$customer_id','$card_number','$barcode','$sku_code',
                $item_description,$qty,$unit_price,'$vat',$total_sales,$item_cost,$total_cost,$vat_value,$sales_without_vat,'63')";
                $res=mssql_query($sql,$dbh);
                if ($res){
                    $b++;
                }
                else{
                    echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
                    exit;
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
function fncImportCSV_OtherRetailerSales($name,$month,$year,$merchant,$date_format=1,$separator="/"){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$today = date('Y-m-d');
	$a=0;
	$b=0;
    $r=0;
	sqlsrv_begin_transaction($dbh);
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $r++;
            list ($trans_date,$trans_time,$receipt_number,$item_code,$barcode,$item_description,$quantity,
                $unit_price,$sale_value,$tax,$beneficiary_id,$packaging,$payment_method)=$data;
            $trans_date=explode(" ",trim($trans_date))[0];
            $date_elements=explode($separator,$trans_date);
            $check_month="";$check_day="";$check_year="";
            if(count($date_elements)==3){
                switch($date_format){
                    case 1:
                        list($check_month,$check_day,$check_year)=$date_elements;
                        break;
                    case 2:
                        list($check_day,$check_month,$check_year)=$date_elements;
                        break;
                    case 3:
                        list($check_year,$check_month,$check_day)=$date_elements;
                        break;
                    case  4:
                        list($check_year,$check_month,$check_day)=$date_elements;
                        break;
                }
                if (checkdate($check_month,$check_day,$check_year) && strlen($item_description)>0){
                    if(strlen($check_month)<2) $check_month="0".$check_month;
                    if(strlen($check_day)<2) $check_day="0".$check_day;
                    if ($check_year.$check_month != $dateupload){
                        sqlsrv_rollback($dbh);
                        echo "Error on line no. $r! This date '<b>".$trans_date."</b> is not for the selected month ".$dateupload;
                        exit;
                    }
                    $a++;
                    $trans_date=$check_year."-".$check_month."-".$check_day;
                    $vat=floatval(str_replace("%","",$tax));
                    $trans_time=substr(trim($trans_time),0,8);
                    $sql="insert into merchant_sales_$check_year$check_month"
                    ."(Trans_Date,Trans_Time,Receipt_No,Customer_ID,Barcode,Material_Code,Item_Description,
                    Quantity,Unit_Price,VAT,Total_Amount,Merchant,Payment_Type,Packaging) "
                    ."VALUES (N'$trans_date',N'$trans_time',N'$receipt_number',N'$beneficiary_id',N'$barcode',N'$item_code',N"
                    .sqlvalue2($item_description,true).",".floatval($quantity).",".floatval($unit_price).","
                    .floatval($vat).",".floatval($sale_value).",N'$merchant',N'$payment_method',N'$packaging')";
                    $res=mssql_query($sql,$dbh);
                    if ($res){
                        $b++;
                    }
                    else{
                        echo "ERROR on line no. $r: This query failed<br /><br />".$sql;
                        sqlsrv_rollback($dbh);
                        exit;
                    }
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
function fncImportCSV_OtherRetailerSales_Special($name,$month,$year,$merchant,$date_format=1,$separator="/",$field_order){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$today = date('Y-m-d');
	$a=0;
	$b=0;
    $r=0;
	sqlsrv_begin_transaction($dbh);
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            $r++;
            $required_fields =explode("|",$field_order);
            $trans_date=$data[$required_fields[0]];
            $trans_time=$data[$required_fields[1]];
            $receipt_number=$data[$required_fields[2]];
            $customer_id=$data[$required_fields[3]];
            $barcode=$data[$required_fields[4]];
            $material_code=$data[$required_fields[5]];
            $item_description=$data[$required_fields[6]];
            $quantity=$data[$required_fields[7]];
            $unit_price=$data[$required_fields[8]];
            $vat=$data[$required_fields[9]];
            $total_amount=$data[$required_fields[10]];
            $payment_type=$data[$required_fields[11]];
            $packaging=$data[$required_fields[12]];
            $trans_date=explode(" ",trim($trans_date))[0];
            $date_elements=explode($separator,$trans_date);
            $check_month="";$check_day="";$check_year="";
            if(count($date_elements)==3){
                switch($date_format){
                    case 1:
                        list($check_month,$check_day,$check_year)=$date_elements;
                        break;
                    case 2:
                        list($check_day,$check_month,$check_year)=$date_elements;
                        break;
                    case 3:
                        list($check_year,$check_month,$check_day)=$date_elements;
                        break;
                    case  4:
                        list($check_year,$check_month,$check_day)=$date_elements;
                        break;
                }
                if (checkdate($check_month,$check_day,$check_year) && strlen($item_description)>0){
                    if(strlen($check_month)<2) $check_month="0".$check_month;
                    if(strlen($check_day)<2) $check_day="0".$check_day;
                    if ($check_year.$check_month != $dateupload){
                        sqlsrv_rollback($dbh);
                        echo "Error on line no. $r! This date '<b>".$trans_date."</b> is not for the selected month ".$dateupload;
                        exit;
                    }
                    $a++;
                    $trans_date=$check_year."-".$check_month."-".$check_day;
                    $vat=floatval(str_replace("%","",$vat));
                    $trans_time=substr(trim($trans_time),0,8);
                    $sql="insert into merchant_sales_$check_year$check_month"
                    ."(Trans_Date,Trans_Time,Receipt_No,Customer_ID,Barcode,Material_Code,Item_Description,
                    Quantity,Unit_Price,VAT,Total_Amount,Merchant,Payment_Type,Packaging) "
                    ."VALUES (N'$trans_date',N'$trans_time',N'$receipt_number',N'$customer_id',N'$barcode',N'$material_code',N"
                    .sqlvalue2($item_description,true).",".floatval($quantity).",".floatval($unit_price).","
                    .floatval($vat).",".floatval($total_amount).",N'$merchant',N'$payment_type',N'$packaging')";
                    $res=mssql_query($sql,$dbh);
                    if ($res){
                        $b++;
                    }
                    else{
                        echo "ERROR on line no. $r: This query failed<br /><br />".$sql;
                        sqlsrv_rollback($dbh);
                        exit;
                    }
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
function fncImportCSV_MerchantPurchases($name,$month,$year,$merchantid){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$today = date('Y-m-d');
	$a=0;
	$b=0;
	sqlsrv_begin_transaction($dbh);
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            list ($trans_date,$sku_code,$barcode,$packaging,$sku_description,$cost_price,$supplier)=$data;
            list($check_day,$check_month,$check_year)=explode("/",$trans_date);
            if(substr($check_year,0,2)=="20"){
                //if ($check_year.$check_month != $dateupload){
                //    sqlsrv_rollback($dbh);
                //    echo "Error! This date '<b>".$trans_date."' </b> is not for the selected month ".$dateupload;
                //    exit;
                //}
                $a++;
                $trans_date=$check_year."-".$check_month."-".$check_day;
                $sql="insert into merchant_purchases_".$check_year.$check_month
                ."(trans_date,sku_code,barcode,packaging,sku_description,cost_price,merchant,supplier) "
                ."VALUES ('$trans_date','$sku_code','$barcode','$packaging',".sqlvalue2($sku_description,true).",'"
                .floatval($cost_price)."','$merchantid',".sqlvalue2($supplier,true).")";
                $res=mssql_query($sql,$dbh);
                if ($res){
                    $b++;
                }
                else{
                    echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
                    exit;
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
// upload for JAB Statement
function fncImportCSV_JAB($name,$month,$year){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$today = date('Y-m-d');
	$a=0;
	$b=0;
	sqlsrv_begin_transaction($dbh);
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            list ($currency,$trans_amount,$action,$trans_date,$source)=$data;
            if(trim($action) == "Pre-Paid Reload JAB"){
				//$check_date = substr($trans_date,0,6);
                //if ($check_date != $dateupload){
                //    sqlsrv_rollback($dbh);
                //    echo "Kindly delete this date in the CSV and try again. '<b>".$check_date."' </b>";
                //    exit;
                //}
				$a++;
                $trans_amount=str_replace("(","",$trans_amount); //remove open brackets
                $trans_amount=str_replace(")","",$trans_amount); //remove close brackets
                $trans_amount=str_replace(",","",$trans_amount); //remove commas
                $trans_date=date('Ymd',strtotime($trans_date));
                $trans_month=substr($trans_date,0,6);
                if(floatval($trans_amount)<0)
                    $trans_amount=$trans_amount*-1;
				$sql="insert into jab_statement_$trans_month (trans_date,amount,source,currency) values"
				."('$trans_date','$trans_amount','$source','$currency')";
				$res=mssql_query($sql,$dbh);
				if ($res){
					$b++;
				}
				else{
					echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
					exit;
				}
		    }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
function fncShowMessage($str,$url){
	echo"	<script>
					alert('$str');
					window.location='$url';
				</script>";
}
function fncGetTotalSalesByMonth($startmonth,$endmonth){
    $dbh=fncOpenDBConn();
    $c=0;
    for($month=$startmonth;$month<=$endmonth;$month++){
        $sql="SELECT left(trans_date,6) as Month,SUM(Trans_Amount) AS Amount "
        ."FROM sales_draft_$month "
        ."GROUP BY left(trans_date,6) ";
        $res = mssql_query($sql,$dbh);
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $c++;
            $row = mssql_fetch_array($res);
            $data[$c]=$row;
        }
    }
    $data[0][0]=$c;
    mssql_close($dbh);
    return $data;
}
function fncGetMonthName($month){
    return strtoupper(date('M - Y',strtotime($month."01")));
}
function fncPermLink($link,$permission,$type){
	$denylink="javascript:alert('Access is denied')";
    if ($type=="main"){
		$acarray=explode("%",$_SESSION["access"]);
        $denylink= "accessdenied.php";
    }
	else
		$acarray=explode("%",$_SESSION[$type]);
	if (in_array($permission,$acarray)){
		return $link;
	}
	else{
		return $denylink;
	}
}
function fncCheckPermission($permission,$type){
    if ($type=="main"){
		$acarray=explode("%",$_SESSION["access"]);
    }
	else
		$acarray=explode("%",$_SESSION[$type]);
	if (in_array($permission,$acarray)){
		return true;
	}
	else{
		return false;
	}
}
function fncImportCSV_VRL($name,$month,$year){
	$dbh=fncOpenDBConn();
	$a=0;
	$b=0;
    sqlsrv_begin_transaction($dbh);
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            list ($customer_id,$governorate,$status,$owner,$note,$partner,
            $phone_number,$card_number)=$data;
            $test_customer_id=explode("-",$customer_id);
            $governorate=str_replace("'","''",$governorate);
            $note=str_replace("'","''",$note);
            if (intval($test_customer_id[0])>0){
                $a++;
                $sql="insert into vrl"
                ."(customer_id,governorate,status,owner,note,partner,"
                ."phone_number,card_number"
                .") values "
                ."('$customer_id','$governorate','$status','$owner','$note',"
                ."'$partner','$phone_number','$card_number')";
                $res=mssql_query($sql,$dbh);
                if ($res){
                    $b++;
                }
                else{
                    echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
                    exit;
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
function fncImportCSV_Destroyed_Cards($name,$month,$year){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$a=0;
	$b=0;
    $r=0;
    if ( sqlsrv_begin_transaction( $dbh ) === false ) {
        die( print_r( sqlsrv_errors(), true ));
    }
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            list ($beneficiary_id,$cardnumber,$status,$partner,$rawdate)=$data;
            $r++;
            $date=date("Ymd", strtotime($rawdate));
			if (substr($date,0,3) == '201'){
                if (substr(str_replace("-","",$date),0,6) != $dateupload){
                    sqlsrv_rollback($dbh);
                    echo "Error! This date '<b>".$rawdate."'</b> on line $r is not for the selected month ".$dateupload;
                    exit;
                }
				$a++;
				$sql="insert into Destroyed_Cards	"
				."(Customer_ID,Card_Number,Status,Partner,Date_Destroyed,Reason_Destroyed) values "
				."('$beneficiary_id','$cardnumber','$status','$partner','$date','$status')";
				$res=mssql_query($sql,$dbh);
				if ($res){
					$b++;
				}
				else{
					echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
					exit;
				}
			}
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
function fncImportCSV_Replaced_Cards($name,$month,$year){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$a=0;
	$b=0;
    if ( sqlsrv_begin_transaction( $dbh ) === false ) {
        die( print_r( sqlsrv_errors(), true ));
    }
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            list ($Replacement_Card_Number,$Customer_ID,$Date_Replaced,$Hotline_Date,$Pickup_Date,$CP,$Reason_Replaced)=$data;
            list($check_day,$check_month,$check_year)=explode("/",$Date_Replaced);
            if (substr($check_year,0,3) == '201'){
                $a++;
                $Reason_Replaced=sqlstr2($Reason_Replaced);
                $sql="insert into Replaced_Cards "
                ."(Replacement_Card_Number,Customer_ID,Date_Replaced,Hotline_Date,Pickup_Date,CP,Reason_Replaced) values "
                ."('$Replacement_Card_Number','$Customer_ID','$Date_Replaced','$Hotline_Date','$Pickup_Date','$CP','$Reason_Replaced')";
                $res=mssql_query($sql,$dbh);
                if ($res){
                    $b++;
                }
                else{
                    echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
                    exit;
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
function fncImportCSV_Unused_Exceptions($name,$month,$year){
	$dbh=fncOpenDBConn();
	$dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
	$a=0;
	$b=0;
    $r=0;
    if ( sqlsrv_begin_transaction( $dbh ) === false ) {
        die( print_r( sqlsrv_errors(), true ));
    }
    if (($handle = fopen($name, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
            list ($Customer_ID,$Reason)=$data;
            $r++;
            if ($r>1){
                $a++;
                $Reason=sqlstr2($Reason);
                $sql="insert into Unused_Exceptions "
                ."(Customer_ID,Reason,Month) VALUES "
                ."('$Customer_ID','$Reason','$dateupload')";
                $res=mssql_query($sql,$dbh);
                if ($res){
                    $b++;
                }
                else{
                    echo "ERROR: This query failed<br /><br />".$sql;
                    sqlsrv_rollback($dbh);
                    exit;
                }
            }
		}
	}
	if ($a>0 && $a==$b){
        sqlsrv_commit($dbh);
        echo "$a records uploaded";
        $send=true;
	}
	else{
        echo "No record upladed. The file is not in the expected format";
        sqlsrv_rollback($dbh);
        $send=false;
	}
	mssql_close($dbh);
	return $send;
}
function fncGetProgrammeSetup(){
    $dbh=fncOpenDBConn();
    $sql="Select * from program_setup";
    $res = mssql_query($sql,$dbh);
    $row = mssql_fetch_array($res);
    mssql_close($dbh);
    return $row;
}
function fncUpdateProgrammeSetup($region,$country_office,$programme_name,$about_the_programme,$about_the_database,$start_date,$end_date,$updated_by,$date_updated){
    $dbh=fncOpenDBConn();
    $sql="UPDATE [program_setup] SET [Region]=".sqlvalue2($region,true).",[Country_Office]=".sqlvalue2($country_office,true)
        .",[Programme_Name]=".sqlvalue2($programme_name,true).",[About_the_programme]=".sqlvalue2($about_the_programme,true)
        .",[About_the_database]=".sqlvalue2($about_the_database,true).",[Start_Date]=".sqlvalue2($start_date,true)
        .",[End_Date]=".sqlvalue2($end_date,true).",[Updated_By]=".sqlvalue2($updated_by,true)
        .",[Date_Updated]=".sqlvalue2($date_updated,true);
    $res = mssql_query($sql,$dbh);
    return $res;
}
function fncGetAvailableMEPS(){
    $dbh=fncOpenDBConn();
    $sql="Select distinct left(datename(month,trans_date),3)+' '+DATENAME(yy,Trans_Date) as
    Upload_Month,DATENAME(yy,Trans_Date) as uyear,MONTH(trans_date) as umonth
    from dbo.prepaid_reload_meps order by DATENAME(yy,Trans_Date),MONTH(trans_date)";
    $res = mssql_query($sql,$dbh);
    return createList($res);
}
function fncGetAvailableWFP(){
    $dbh=fncOpenDBConn();
    $sql="Select distinct left(datename(month,trans_date),3)+' '+DATENAME(yy,Trans_Date) as
    Upload_Month,DATENAME(yy,Trans_Date) as uyear,MONTH(trans_date) as umonth
    from dbo.prepaid_reload_wfp order by DATENAME(yy,Trans_Date),MONTH(trans_date)";
    $res = mssql_query($sql,$dbh);
    return createList($res);
}
function fncGetAvailableJAB(){
    $dbh=fncOpenDBConn();
    $sql="Select distinct left(datename(month,trans_date),3)+' '+DATENAME(yy,Trans_Date) as
    Upload_Month,DATENAME(yy,Trans_Date) as uyear,MONTH(trans_date) as umonth
    from dbo.jab_statement order by DATENAME(yy,Trans_Date),MONTH(trans_date)";
    $res = mssql_query($sql,$dbh);
    return createList($res);
}
function fncGetAvailableMerchantSales(){
    $dbh=fncOpenDBConn();
    $sql="select distinct [MONTH] from dbo.merchant_sales";
    $res = mssql_query($sql,$dbh);
    return createList($res);
}
function fncGetAvailableMerchantPurchases(){
    $dbh=fncOpenDBConn();
    $sql="select distinct [MONTH] from dbo.merchant_purchases";
    $res = mssql_query($sql,$dbh);
    return createList($res);
}
function fncGetAvailableMerchantsWFP(){
    return "";
}
function fncGeAvailableVRL(){
    return "";
}
function createTable($result) {
    $table = '<table><tr>';
    for ($x=0;$x<sqlsrv_num_fields($result);$x++) $table .= '<th>'.sqlsrv_get_field($result,$x).'</th>';
    $table .= '</tr>';
    while ($rows = mssql_fetch_array($result)) {
        $table .= '<tr>';
        foreach ($rows as $row) $table .= '<td>'.$row.'</td>';
        $table .= '</tr>';
    }
    $table .= '<table>';
    //mssql_data_seek($result,0); //if we need to reset the mssql result pointer to 0
    return $table;
}
function createList($result){
    $list='<ul style="list-style-type: circle">';
    while ($row = mssql_fetch_array($result)) {
        $list .= '<li>';
        $list .= $row[0];
        $list .= '</li>';
    }
    $list.="</ul>";
    return $list;
}
function fncGetActivityLog(){
    $dbh=fncOpenDBConn();
    $sql="select * from activity_log order by datetime desc";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetAllMerchants(){
    $dbh=fncOpenDBConn();
    $sql="select id,concat(wfp_name,' - ',branch, ' - ',[address]) as fullname from merchants_wfp order by id";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetCustomReports(){
    $dbh=fncOpenDBConn();
    $sql="select id,description,view_name,source from analytic_views";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetRetailerName($id){
    $dbh=fncOpenDBConn();
    if(substr($id,0,4)=='LBCO')
        $sql="select a00Shop_Name from Lebanon_Retailers where retailer_id='$id'";
    else
        $sql="select wfp_name from merchants_wfp where retailer_id='$id'";
    $res = mssql_query($sql,$dbh);
    $row=mssql_fetch_array($res);
    return $row[0];
}
function fncGetRetailerBranches($joco_retailer_id){
    $dbh=fncOpenDBConn();
    if(substr($joco_retailer_id,0,4)=='LBCO'){
        $sql="select a02Code as id,concat(a00Shop_Name,' - ',lebanon_retailers.a02Name) as fullname
        from lebanon_retailers where retailer_id='$joco_retailer_id'
        UNION SELECT 0 as id,'All Branches' as fullname
        order by a02Code";
    }
    else{
        $sql="select id,concat(wfp_name,' - ',branch, ' - ',[address]) as fullname
        from merchants_wfp where retailer_id='$joco_retailer_id' order by id";
    }
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetFullName($username){
    $names=explode(".",trim($username));
    $firstname=$names[0];
    if(count($names)>1)
        $lastname=$names[1];
    else
        $lastname="";
    return ucfirst($firstname)." ".ucfirst($lastname);
}
function fncGetFieldNames($name){
    $data=array();
    if (($handle = fopen($name, "r")) !== FALSE) {
        $data = fgetcsv($handle, 5000, ",");
    }
    return $data;
}
function fncAlreadyUploaded($month,$year,$merchant){
    $dateupload = $year.str_pad($month,2,"0",STR_PAD_LEFT);
    $dbh=fncOpenDBConn();
    $sql="Select top (1) id from merchant_sales_$dateupload where merchant=$merchant";
    $res = mssql_query($sql,$dbh);
    if(mssql_num_rows($res)>0){
        return true;
    }
    else{
        return false;
    }
}
function fncUpdateRetailerUploads($filename,$fullpath,$triangulation_id,$type,$month){
    $dbh=fncOpenDBConn();
    $ext=pathinfo($fullpath, PATHINFO_EXTENSION);
    $filename=sqlvalue2($filename,true);
    $icon=sqlvalue2($ext,true);
    $file_type=sqlvalue2(fncGetFileExtension($ext),true);
    $size=sqlvalue2(pretty_filesize($fullpath),true);
    $date_uploaded=sqlvalue2(date('Y-m-d H:i:s'),true);
    $type=sqlvalue2($type,true);
    $month=sqlvalue2($month,true);

    $status=1;
    $sql="INSERT INTO [retailer_uploads]([Filename],[File_Type],[Size],[Date_Uploaded],[Triangulation_ID]
           ,[Month],[Type],[icon],[Status])
     VALUES
           ($filename,$file_type,$size,$date_uploaded,$triangulation_id,$month,$type,$icon,$status)";
    $res=mssql_query($sql,$dbh);
    if($res){
        return "TRUE";
    }
    else{
        return "FALSE: $sql";
    }
}
function fncGetFileExtension($icon){
    $extn="Unknown File Type";
     switch (strtolower($icon)){
        case "csv": $extn="CSV File"; break;
        case "png": $extn="PNG Image"; break;
        case "jpg": $extn="JPEG Image"; break;
        case "jpeg": $extn="JPEG Image"; break;
        case "svg": $extn="SVG Image"; break;
        case "gif": $extn="GIF Image"; break;
        case "ico": $extn="Windows Icon"; break;
        case "txt": $extn="Text File"; break;
        case "log": $extn="Log File"; break;
        case "htm": $extn="HTML File"; break;
        case "html": $extn="HTML File"; break;
        case "xhtml": $extn="HTML File"; break;
        case "shtml": $extn="HTML File"; break;
        case "php": $extn="PHP Script"; break;
        case "js": $extn="Javascript File"; break;
        case "css": $extn="Stylesheet"; break;
        case "pdf": $extn="PDF Document"; break;
        case "xls": $extn="Excel Spreadsheet"; break;
        case "xlsx": $extn="Excel Spreadsheet"; break;
        case "doc": $extn="Microsoft Word Document"; break;
        case "docx": $extn="Microsoft Word Document"; break;
        case "zip": $extn="ZIP Archive"; break;
        case "rar": $extn="RAR Archive"; break;
        case "htaccess": $extn="Apache Config File"; break;
        case "exe": $extn="Windows Executable"; break;
        default
    : if($extn!=""){$extn=strtoupper($extn)." File";} else{$extn="Unknown";} break;

	}
     return $extn;
}
function pretty_filesize($file) {
	$size=filesize($file);
	if($size<1024){$size=$size." Bytes";}
	elseif(($size<1048576)&&($size>1023)){$size=round($size/1024, 1)." KB";}
	elseif(($size<1073741824)&&($size>1048575)){$size=round($size/1048576, 1)." MB";}
	else{$size=round($size/1073741824, 1)." GB";}
	return $size;
}
function fncGetLastMonthCycle(){
    $date=date("Y-m-d");
    $datestring="$date first day of last month";
    $datestring="$datestring first day of last month";
    $dt=date_create($datestring);
    return $dt->format('Ym'); //201102
}

?>