<?php

function connect()
{
	$serverName = "10.67.67.130";
	$connectionInfo = array( "Database"=>"jordandb", "UID"=>"sa", "PWD"=>"P@ssword1","CharacterSet" => "UTF-8");
	$conn = sqlsrv_connect( $serverName, $connectionInfo );
	if( $conn === false ) {
        die( print_r( sqlsrv_errors(), true));
	}
	return $conn;
}
function sqlvalue($val, $quote)
{
  if ($quote)
    $tmp = sqlstr($val);
  else
    $tmp = $val;
  if ($tmp == "")
    $tmp = "NULL";
  elseif ($quote)
    $tmp = "'".$tmp."'";
  return $tmp;
}

function sqlstr($val)
{
  return str_replace("'", "''", $val);
}

function uploadfile($uploaddir,$control_name){
	if(isset($_FILES[$control_name]['name'])){
		$uploadfile = $uploaddir . basename($_FILES[$control_name]['name']);
		if (move_uploaded_file($_FILES[$control_name]['tmp_name'], $uploadfile)) {
			return $_FILES[$control_name]['name'];
		} else {
			return "";
		}
	}
	else{
		return "";
	}
}
function build_link($prefix, $href, $suffix, $target, $content)
{
  $res = "<a href=\"".$prefix.$href.$suffix."\" target=\"".$target."\">".$content."</a>";
  return $res;
}
function fncLogAccess($activity,$user=""){
    $dbh=fncOpenDBConn();
    if(isset($_SESSION["name"]))
        $name=$_SESSION["name"];
    else
        $name=$user;
    $datetime=date("Y-m-d H:i:s");
    $sql="insert into activity_log (activity,doneby,datetime) values ('$activity','$name','$datetime')";
    mssql_query($sql,$dbh);
    mssql_close($dbh);
}
?>