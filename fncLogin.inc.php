<?php
include("config.inc.php");
function fncVerifyUser($user, $pass,$usertype){
    $dbh=fncOpenDBConn();
	if($usertype==1) {
        $ldaphost = "global.wfp.org";
        $ldapport = 389;

        $ds = ldap_connect($ldaphost, $ldapport)
        or die("Could not connect to $ldaphost");

        if ($ds)
        {
            $username = $user."@wfp.org";
            $upasswd = $pass;

            $ldapbind = ldap_bind($ds, $username, $upasswd);

            if ($ldapbind) {
                $sql = "select login,acmain,status,acupload,acdata,acqueries,acsys from access where status='Y' and login='$user'";
                $res = mssql_query($sql,$dbh);
                $rows = mssql_num_rows($res);
                if ($rows==1){
                    $data = mssql_fetch_array($res);
                }
                else{
                    $data[0]='NOTFOUND';
                }
            }
            else{
                $data[0]='NOTFOUND3';
            }
            mssql_close($dbh);
        }
        else{
            $data[0]='NOTFOUND4';
        }
	}
	else{
		$sql = "SELECT TOP (1) login,acmain,status,acupload,acdata,acqueries,acsys from access where status='Y' and login='$user' and passwd=HASHBYTES('MD5','$pass')";
		$res = mssql_query($sql,$dbh);
		$rows = mssql_num_rows($res);
		if ($rows==1){
			$data = mssql_fetch_array($res);
		}
		else{
			$data[0]='NOTFOUND3';
		}
		mssql_close($dbh);
		return $data;
	}
    return $data;
}
function fncGetUserTypes(){
	$dbh=fncOpenDBConn();
	$sql="select id,UserType from UserTypes";
	$res=mssql_query($sql,$dbh);
	$data[0][0]=mssql_num_rows($res);
	for ($i=1;$i<=$data[0][0];$i++){
		$row=mssql_fetch_row($res);
		$data[$i][0]=$row[0];
		$data[$i][1]=$row[1];
	}
	mssql_close($dbh);
	return $data;
}
function fncGetUserType($username){
	$dbh=fncOpenDBConn();
	$sql="select usertype from access where login='$username' and status='Y'";
	$res=mssql_query($sql,$dbh);
	$data=mssql_fetch_array($res);
    mssql_close($dbh);
    return $data[0];
}
function fncGetFullName($username){
    $names=explode(".",trim($username));
    $firstname=$names[0];
    if(count($names)>1)
        $lastname=$names[1];
    else
        $lastname="";
    return ucfirst($firstname)." ".ucfirst($lastname);
}
?>