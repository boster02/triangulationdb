<div id="my_connectivity_panel" class="pathwindow connectivity active">
  <div class="pathwindow_title">
    <div class="icon"></div>
    <a class="btn_back"><span></span>
    <p>Back</p>
    </a>
    <h1>Cash Analyzer Help</h1>
  </div>
  <div class="pathwindow_content">
    <div class="listholder ">
      <div class="sectionholder"> 
	<script type="text/javascript" src="//code.jquery.com/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/smk-accordion.css">
    	<style type="text/css">
		/*Demo styles*/
		body{
			margin: 0;
			padding: 0;
			font-family: 'Helvetica', Arial, sans-serif;
			font-size: 12px;
		}
		.container_demo{
			max-width: 900px;
			margin: 30px auto 100px;
		}
		h2{
			margin: 25px 0 20px;
			text-align: center;
			color: #555;
		}
		table{
		border:1px solid black;border-collapse:collapse
		}
		table td {
			border:1px solid black;
		}
	</style>
		<div class="accordion_example2">
			<div class="accordion_in">
				<div class="acc_head">Introduction</div>
				<div class="acc_content">
					Cash Analyzer is the triangualtion database for analysing transaction in the WFP Jordan e-voucher program for refugees. The system allows uploading of data from WFP, MEPS, JAB and merchants. The data is triangulated to check for anomalies. The system has the dashboard for the quick overview of the data, the analytics where the exception reports are displayed and standard reports which are summarised reports demonstrating the general performance of the program
				</div>
			</div>
            <div class="accordion_in">
				<div class="acc_head">Upload Data</div>
				<div class="acc_content">
					<p>The following  is the list of files to be uploaded:</p>
<table>
  <tr>
    <td><p><strong>#</strong></p></td>
    <td><p><strong>Name</strong></p></td>
    <td><p><strong>Source</strong></p></td>
    <td><p><strong>Description</strong></p></td>
  </tr>
  <tr>
    <td><p>1</p></td>
    <td><p>Prepaid Reload - MEPS</p></td>
    <td><p>MEPS</p></td>
    <td><p>Report of beneficiary    entitlements loaded on the debit cards as instructed by WFP</p></td>
  </tr>
  <tr>
    <td><p>2</p></td>
    <td><p>Sales Draft</p></td>
    <td><p>MEPS</p></td>
    <td><p>Report of transactions that    took place during the month</p></td>
  </tr>
  <tr>
    <td><p>3</p></td>
    <td><p>Prepaid Redemption</p></td>
    <td><p>MEPS</p></td>
    <td><p>Report of redeemed entitlements    by the bank</p></td>
  </tr>
  <tr>
    <td><p>4</p></td>
    <td><p>Prepaid Reload - WFP</p></td>
    <td><p>WFP</p></td>
    <td><p>Report of beneficiary    entitlements sent to the bank by WFP</p></td>
  </tr>
  <tr>
    <td><p>5</p></td>
    <td><p>List of retailers - WFP</p></td>
    <td><p>WFP</p></td>
    <td><p>List of WFP contracted    retailers maintained by WFP</p></td>
  </tr>
  <tr>
    <td><p>6</p></td>
    <td><p>Bank Statement</p></td>
    <td><p>JAB</p></td>
    <td><p>List of transactions on the WFP    account for the e-voucher program - Wings automatic upload</p></td>
  </tr>
  <tr>
    <td><p>7</p></td>
    <td><p>Merchant Transactions</p></td>
    <td><p>Merchants</p></td>
    <td><p>Report of transactions from the    merchants</p></td>
  </tr>
</table>
				</div>
			</div>            

		<div class="accordion_in">
				<div class="acc_head">Dashboard</div>
				<div class="acc_content">
					Cash Analyzer is the triangualtion database for analysing transaction in the WFP Jordan e-voucher program for refugees. The system allows uploading of data from WFP, MEPS, JAB and merchants. The data is triangulated to check for anomalies. The system has the dashboard for the quick overview of the data, the analytics where the exception reports are displayed and standard reports which are summarised reports demonstrating the general performance of the program
				</div>
			</div>

<div class="accordion_in">
				<div class="acc_head">Analytics</div>
				<div class="acc_content">
					Cash Analyzer is the triangualtion database for analysing transaction in the WFP Jordan e-voucher program for refugees. The system allows uploading of data from WFP, MEPS, JAB and merchants. The data is triangulated to check for anomalies. The system has the dashboard for the quick overview of the data, the analytics where the exception reports are displayed and standard reports which are summarised reports demonstrating the general performance of the program
				</div>
			</div>
            
<div class="accordion_in">
				<div class="acc_head">Standard Reports</div>
				<div class="acc_content">
					Cash Analyzer is the triangualtion database for analysing transaction in the WFP Jordan e-voucher program for refugees. The system allows uploading of data from WFP, MEPS, JAB and merchants. The data is triangulated to check for anomalies. The system has the dashboard for the quick overview of the data, the analytics where the exception reports are displayed and standard reports which are summarised reports demonstrating the general performance of the program
				</div>
			</div>

<div class="accordion_in">
				<div class="acc_head">Settings</div>
				<div class="acc_content">
					Cash Analyzer is the triangualtion database for analysing transaction in the WFP Jordan e-voucher program for refugees. The system allows uploading of data from WFP, MEPS, JAB and merchants. The data is triangulated to check for anomalies. The system has the dashboard for the quick overview of the data, the analytics where the exception reports are displayed and standard reports which are summarised reports demonstrating the general performance of the program
				</div>
			</div>
<div class="accordion_in">
				<div class="acc_head">Help</div>
				<div class="acc_content">
					Cash Analyzer is the triangualtion database for analysing transaction in the WFP Jordan e-voucher program for refugees. The system allows uploading of data from WFP, MEPS, JAB and merchants. The data is triangulated to check for anomalies. The system has the dashboard for the quick overview of the data, the analytics where the exception reports are displayed and standard reports which are summarised reports demonstrating the general performance of the program
				</div>
			</div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
	<script type="text/javascript" src="js/smk-accordion.js"></script>
	<script type="text/javascript">
		$(".accordion_example2").smk_Accordion({
			closeAble: true, //boolean
		});
	</script>
