<?php function showroweditor($row, $iseditmode)
      {
          global $conn;
?>
<style>
    .hr {
        width:150px;
    }
</style>
<table>
    <tr>
        <td style="vertical-align:top">
            <table class="table table-striped table-bordered" border="0" cellspacing="1" cellpadding="5" style="width: 400px">
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Account Name")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="acc_name" maxlength="100" value="<?php echo str_replace("�","&#39", trim($row["acc_name"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("WFP Name")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="wfp_name" maxlength="100" value="<?php echo str_replace("�","&#39", trim($row["wfp_name"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Address")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="address" maxlength="255" value="<?php echo str_replace("�","&#39", trim($row["address"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Branch")."&nbsp;" ?></td>
                    <td class="dr">
                        <select class="form-control" name="branch">
                            <option value=""></option>
                            <?php
          $sql = "select lut_branches, lut_branches from lut_branches";
          $res = mssql_query($sql, $conn);

          while ($lp_row = mssql_fetch_array($res)){
              $val = $lp_row["lut_branches"];
              $caption = $lp_row["lut_branches"];
              if ($row["branch"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                            ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Governorate")."&nbsp;" ?></td>
                    <td class="dr">
                        <select class="form-control" name="governorate">
                            <option value=""></option>
                            <?php
          $sql = "select governorate, governorate from lut_governorates";
          $res = mssql_query($sql, $conn);

          while ($lp_row = mssql_fetch_array($res)){
              $val = $lp_row["governorate"];
              $caption = $lp_row["governorate"];
              if ($row["governorate"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                            ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("City")."&nbsp;" ?></td>
                    <td class="dr">
                        <select class="form-control" name="city">
                            <option value=""></option>
                            <?php
          $sql = "select city, city from lut_cities";
          $res = mssql_query($sql, $conn);

          while ($lp_row = mssql_fetch_array($res)){
              $val = $lp_row["city"];
              $caption = $lp_row["city"];
              if ($row["city"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                            ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Owner")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="owner" maxlength="50" value="<?php echo str_replace("�","&#39", trim($row["owner"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Focal Point")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="focal_point" maxlength="50" value="<?php echo str_replace("�","&#39", trim($row["focal_point"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Phone")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="tel" maxlength="50" value="<?php echo str_replace("�","&#39", trim($row["tel"])) ?>" /></td>
                </tr>
            </table>
        </td>
        <td style="vertical-align:top">
            <table class="table table-striped table-bordered" style="width:400px;margin-left:5px">
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Email")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="email" maxlength="50" value="<?php echo str_replace("�","&#39", trim($row["email"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Financial Statements Available")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="financial_statements" maxlength="10" value="<?php echo str_replace("�","&#39", trim($row["financial_statements"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Opening Hour")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="open_hours" maxlength="10" value="<?php echo str_replace("�","&#39", trim($row["open_hours"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Closing Hour")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="text" name="close_hours" maxlength="10" value="<?php echo str_replace("�","&#39", trim($row["close_hours"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Status")."&nbsp;" ?></td>
                    <td class="dr">
                        <select class="form-control" name="status">
                            <option value=""></option>
                            <?php
          $sql = "select id, status from lut_status";
          $res = mssql_query($sql, $conn);

          while ($lp_row = mssql_fetch_array($res)){
              $val = $lp_row["id"];
              $caption = $lp_row["status"];
              if ($row["status"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                            ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Contract Begin Date")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control datepicker" type="text" name="contract_date_from" maxlength="10" value="<?php echo str_replace("�","&#39", trim($row["contract_date_from"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Contract End Date")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control datepicker" type="text" name="contract_date_to" maxlength="10" value="<?php echo str_replace("�","&#39", trim($row["contract_date_to"])) ?>" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Picture of store front")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="file" name="picture_of_store" /></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Contract File")."&nbsp;" ?></td>
                    <td class="dr">
                        <input class="form-control" type="file" name="contract_file" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php } ?>
