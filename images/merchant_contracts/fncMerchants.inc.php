<?php 
include("../fncDataFunctions.inc.php");
?>
<?php function showpagenav($page, $pagecount)
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="merchants.php?a=add">Add Record</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="merchants.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Prev</a>&nbsp;</td>
<?php } ?>
<?php
          global $pagerange;

          if ($pagecount > 1) {

              if ($pagecount % $pagerange != 0) {
                  $rangecount = intval($pagecount / $pagerange) + 1;
              }
              else {
                  $rangecount = intval($pagecount / $pagerange);
              }
              for ($i = 1; $i < $rangecount + 1; $i++) {
                  $startpage = (($i - 1) * $pagerange) + 1;
                  $count = min($i * $pagerange, $pagecount);

                  if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
                      for ($j = $startpage; $j < $count + 1; $j++) {
                          if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="merchants.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php }
                      }
                  } else { ?>
<td><a href="merchants.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php }
              }
          } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="merchants.php?page=<?php echo $page + 1 ?>">Next&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="merchants.php">List of merchants</a></td>
</tr>
</table>
<?php } ?>

<?php function addrec()
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="merchants.php">Index Page</a></td>
</tr>
</table>
<form enctype="multipart/form-data" action="merchants.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
          $row = array(
            "id" => "",
            "acc_name" => "",
            "wfp_name" => "",
            "address" => "",
            "branch" => "",
            "governorate" => "",
            "city" => "",
            "owner" => "",
            "focal_point" => "",
            "tel" => "",
            "email" => "",
            "financial_statements" => "",
            "open_hours" => "",
            "close_hours" => "",
            "status" => "",
            "contract_date_from" => "",
            "contract_date_to" => "",
            "picture_of_store" => "",
            "contract_file" => "");
          showroweditor($row, false);
?>
<p><input class="btn btn-primary" type="submit" name="action" value="Save" /></p>
<br />    
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("view", $recid, $count);
?>
<?php showrow($row, $recid) ?>

<?php
  
} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("edit", $recid, $count);
?>
<form enctype="multipart/form-data" action="merchants.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xid" value="<?php echo $row["id"] ?>">
<?php showroweditor($row, true); ?>
<p><input class="btn btn-primary" type="submit" name="action" value="Save" /></p>
    <br />
</form>
<?php
  
} ?>

<?php function deleterec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("del", $recid, $count);
?>
<form action="merchants.php" method="post">
<input type="hidden" name="sql" value="delete">
<input type="hidden" name="xid" value="<?php echo $row["id"] ?>">
<?php showrow($row, $recid) ?>
<p><input class="btn btn-danger" type="submit" name="action" value="Confirm"></p>
    <br />
</form>
<?php
  
} 
 function sql_select()
      {
          global $conn;
          global $order;
          global $ordtype;
          global $filter;
          global $filterfield;
          global $wholeonly;

          $filterstr = sqlstr($filter);
          if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
          $sql = "SELECT * FROM (SELECT t1.id, t1.acc_name, t1.wfp_name, t1.address, t1.branch, lp4.lut_branches AS lp_branch, t1.governorate, lp5.governorate AS lp_governorate, t1.city, lp6.city AS lp_city, t1.owner, t1.focal_point, t1.tel, t1.email, t1.financial_statements, t1.open_hours, t1.close_hours, t1.status, lp14.status AS lp_status, t1.contract_date_from, t1.contract_date_to, t1.picture_of_store, t1.contract_file FROM merchants_wfp AS t1 LEFT OUTER JOIN lut_branches AS lp4 ON (t1.branch = lp4.lut_branches) LEFT OUTER JOIN lut_governorates AS lp5 ON (t1.governorate = lp5.governorate) LEFT OUTER JOIN lut_cities AS lp6 ON (t1.city = lp6.city) LEFT OUTER JOIN lut_status AS lp14 ON (t1.status = lp14.id)) subq";
          if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
              $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
          } elseif (isset($filterstr) && $filterstr!='') {
              $sql .= " where (acc_name like '" .$filterstr ."') or (wfp_name like '" .$filterstr ."') or (address like '" .$filterstr ."') or (lp_branch like '" .$filterstr ."') or (lp_governorate like '" .$filterstr ."') or (lp_city like '" .$filterstr ."') or (owner like '" .$filterstr ."') or (focal_point like '" .$filterstr ."') or (tel like '" .$filterstr ."') or (email like '" .$filterstr ."') or (financial_statements like '" .$filterstr ."') or (open_hours like '" .$filterstr ."') or (close_hours like '" .$filterstr ."') or (lp_status like '" .$filterstr ."') or (contract_date_from like '" .$filterstr ."') or (contract_date_to like '" .$filterstr ."') or (picture_of_store like '" .$filterstr ."') or (contract_file like '" .$filterstr ."')";
          }
          if (isset($order) && $order!='') $sql .= " order by " .sqlstr($order) ."";
          if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
          $res = sqlsrv_query($conn,$sql,array(), array( "Scrollable" => 'static' ));
          return $res;
      }

      function sql_getrecordcount()
      {
          global $conn;
          global $order;
          global $ordtype;
          global $filter;
          global $filterfield;
          global $wholeonly;

          $filterstr = sqlstr($filter);
          if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
          $sql = "SELECT COUNT(*) FROM (SELECT t1.id, t1.acc_name, t1.wfp_name, t1.address, t1.branch, lp4.lut_branches AS lp_branch, t1.governorate, lp5.governorate AS lp_governorate, t1.city, lp6.city AS lp_city, t1.owner, t1.focal_point, t1.tel, t1.email, t1.financial_statements, t1.open_hours, t1.close_hours, t1.status, lp14.status AS lp_status, t1.contract_date_from, t1.contract_date_to, t1.picture_of_store, t1.contract_file FROM merchants_wfp AS t1 LEFT OUTER JOIN lut_branches AS lp4 ON (t1.branch = lp4.lut_branches) LEFT OUTER JOIN lut_governorates AS lp5 ON (t1.governorate = lp5.governorate) LEFT OUTER JOIN lut_cities AS lp6 ON (t1.city = lp6.city) LEFT OUTER JOIN lut_status AS lp14 ON (t1.status = lp14.id)) subq";
          if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
              $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
          } elseif (isset($filterstr) && $filterstr!='') {
              $sql .= " where (acc_name like '" .$filterstr ."') or (wfp_name like '" .$filterstr ."') or (address like '" .$filterstr ."') or (lp_branch like '" .$filterstr ."') or (lp_governorate like '" .$filterstr ."') or (lp_city like '" .$filterstr ."') or (owner like '" .$filterstr ."') or (focal_point like '" .$filterstr ."') or (tel like '" .$filterstr ."') or (email like '" .$filterstr ."') or (financial_statements like '" .$filterstr ."') or (open_hours like '" .$filterstr ."') or (close_hours like '" .$filterstr ."') or (lp_status like '" .$filterstr ."') or (contract_date_from like '" .$filterstr ."') or (contract_date_to like '" .$filterstr ."') or (picture_of_store like '" .$filterstr ."') or (contract_file like '" .$filterstr ."')";
          }
          $res = mssql_query($sql, $conn);
          $row = mssql_fetch_array($res);
          reset($row);
          return current($row);
      }

      function sql_insert()
      {
          global $conn;
          global $_POST;

          $sql = "insert into merchants_wfp (id, acc_name, wfp_name, address, branch, governorate, city, owner, focal_point, tel, email, financial_statements, open_hours, close_hours, status, contract_date_from, contract_date_to, picture_of_store, contract_file) values (" .sqlvalue(@$_POST["id"], false).", " .sqlvalue(@$_POST["acc_name"], true).", " .sqlvalue(@$_POST["wfp_name"], true).", " .sqlvalue(@$_POST["address"], true).", " .sqlvalue(@$_POST["branch"], true).", " .sqlvalue(@$_POST["governorate"], true).", " .sqlvalue(@$_POST["city"], true).", " .sqlvalue(@$_POST["owner"], true).", " .sqlvalue(@$_POST["focal_point"], true).", " .sqlvalue(@$_POST["tel"], true).", " .sqlvalue(@$_POST["email"], true).", " .sqlvalue(@$_POST["financial_statements"], true).", " .sqlvalue(@$_POST["open_hours"], true).", " .sqlvalue(@$_POST["close_hours"], true).", " .sqlvalue(@$_POST["status"], true).", " .sqlvalue(@$_POST["contract_date_from"], true).", " .sqlvalue(@$_POST["contract_date_to"], true).", " .sqlvalue(@$_POST["picture_of_store"], true).", " .sqlvalue(@$_POST["contract_file"], true).")";
          mssql_query($sql, $conn);
      }

      function sql_update()
      {
          global $conn;
          global $_POST;

          $sql = "update merchants_wfp set id=" .sqlvalue(@$_POST["id"], false).", acc_name=" .sqlvalue(@$_POST["acc_name"], true).", wfp_name=" .sqlvalue(@$_POST["wfp_name"], true).", address=" .sqlvalue(@$_POST["address"], true).", branch=" .sqlvalue(@$_POST["branch"], true).", governorate=" .sqlvalue(@$_POST["governorate"], true).", city=" .sqlvalue(@$_POST["city"], true).", owner=" .sqlvalue(@$_POST["owner"], true).", focal_point=" .sqlvalue(@$_POST["focal_point"], true).", tel=" .sqlvalue(@$_POST["tel"], true).", email=" .sqlvalue(@$_POST["email"], true).", financial_statements=" .sqlvalue(@$_POST["financial_statements"], true).", open_hours=" .sqlvalue(@$_POST["open_hours"], true).", close_hours=" .sqlvalue(@$_POST["close_hours"], true).", status=" .sqlvalue(@$_POST["status"], true).", contract_date_from=" .sqlvalue(@$_POST["contract_date_from"], true).", contract_date_to=" .sqlvalue(@$_POST["contract_date_to"], true).", picture_of_store=" .sqlvalue(@$_POST["picture_of_store"], true).", contract_file=" .sqlvalue(@$_POST["contract_file"], true) ." where " .primarykeycondition();
          mssql_query($sql, $conn);
      }

      function sql_delete()
      {
          global $conn;

          $sql = "delete from merchants_wfp where " .primarykeycondition();
          mssql_query($sql, $conn);
      }
      function primarykeycondition()
      {
          global $_POST;
          $pk = "";
          $pk .= "(id";
          if (@$_POST["xid"] == "") {
              $pk .= " IS NULL";
          }else{
              $pk .= " = " .sqlvalue(@$_POST["xid"], false);
          };
          $pk .= ")";
          return $pk;
      }
?>
