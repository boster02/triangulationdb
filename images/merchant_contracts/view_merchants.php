<?php function showrow($row, $recid)
      {
?>
<table>
    <tr>
        <td style="vertical-align: top">
            <table class="table table-striped table-bordered" style="width: 400px">
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Account Name")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["acc_name"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Trading Name (WFP Name)")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["wfp_name"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Address")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["address"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Branch")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["lp_branch"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Governorate")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["lp_governorate"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("City")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["lp_city"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Owner")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["owner"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Focal Point")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["focal_point"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Phone")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["tel"]) ?></td>
                </tr>
            </table>
        </td>
        <td style="vertical-align: top">
            <table class="table table-striped table-bordered" style="width: 400px; margin-left: 5px">
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Email")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["email"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Financial Statements Available")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["financial_statements"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Opening Hour")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["open_hours"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Closing Hour")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["close_hours"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Status")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["lp_status"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Contract Begin Date")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["contract_date_from"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Contract End Date")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["contract_date_to"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Picture of store front")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["picture_of_store"]) ?></td>
                </tr>
                <tr>
                    <td class="hr"><?php echo htmlspecialchars("Contract File")."&nbsp;" ?></td>
                    <td class="dr"><?php echo htmlspecialchars($row["contract_file"]) ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php } ?>
