<?php session_start();
      include("fncCashAnalyzer.inc.php");
      //$wfp_data=fncGetAvailableWFP();
      //$meps_data=fncGetAvailableMEPS();
      //$jab_data=fncGetAvailableJAB();
      $merchants=fncGetAllMerchants();
     // $merchant_sales_data=fncGetAvailableMerchantSales();
     // $merchant_purchases_data=fncGetAvailableMerchantPurchases();
      //$merchants_wfp_data=fncGetAvailableMerchantsWFP();
      //$vrl_data=fncGeAvailableVRL();
?>
<div id="my_connectivity_panel" class="pathwindow connectivity active">
    <div class="pathwindow_title">
        <div class="icon"></div>
        <a class="btn_back"><span></span>
            <p>Back</p>
        </a>
        <h1>Import data</h1>
    </div>
    <div class="pathwindow_content">
        <link rel="stylesheet" href="css/nice-form.css" />
        <ul id="globalnav">
            <?php if(fncCheckPermission("WF","acupload")) { ?>
            <li><a id="Reload_WFP" href="javascript:setTab('Reload_WFP')" class="here">Reload Pgm</a></li>
            <ul></ul>
            <?php } ?>
            <?php if(fncCheckPermission("WF","acupload")) { ?>
            <li><a id="Reload_LOA" href="javascript:setTab('Reload_LOA')" class="here">Reload LOA</a></li>
            <ul></ul>
            <?php } ?>
            <?php if(fncCheckPermission("ME","acupload")) { ?><li><a id="MEPS_Data" href="javascript:setTab('MEPS_Data')">Reload MEPS</a></li>
            <ul></ul><?php } ?>
            <?php if(fncCheckPermission("JA","acupload")) { ?><li><a id="JAB" href="javascript:setTab('JAB')">Bank Stmt</a></li>
            <ul></ul><?php } ?>
            <?php if(fncCheckPermission("MS","acupload")) { ?><li><a id="Merchant_Sales" href="javascript:setTab('Merchant_Sales')">Merchant Sales</a></li>
            <ul></ul><?php } ?>
            <?php if(fncCheckPermission("MP","acupload")) { ?><li><a id="Merchant_Purchases" href="javascript:setTab('Merchant_Purchases')">Merchant Purchases</a></li>
            <ul></ul><?php } ?>
            <?php if(fncCheckPermission("MC","acupload")) { ?><li><a id="Merchants_WFP" href="javascript:setTab('Merchants_WFP')">Merchants WFP</a></li>
            <ul></ul><?php } ?>
            <?php if(fncCheckPermission("WF","acupload")) { ?><li><a id="VRL" href="javascript:setTab('VRL')">VRL</a></li>
            <ul></ul><?php } ?>
            <?php if(fncCheckPermission("WF","acupload")) { ?><li><a id="Unused_Exceptions" href="javascript:setTab('Unused_Exceptions')">Unused Exceptions</a></li>
            <ul></ul><?php } ?>
            <?php if(fncCheckPermission("WF","acupload")) { ?>
            <li><a id="paper_vouchers" href="javascript:setTab('paper_vouchers')">Paper Vouchers</a></li>
            <ul></ul><?php } ?>
        </ul>
    </div>

    <div id="import-panel">
        <img id="loadingb" src="images/download.gif" style="display: none;width:100px;height:100px" />
        <table>
            <tr>
                <td>
                    <div class="nice-form" style="float: left; width: 400px">
                        <form id="form1" name="form1" enctype="multipart/form-data"
                            action="main.php?action=upload" method="post">
                            <div class="nice-form-heading">Please provide this information</div>
                            <label for="month">
                                <span>Month <span class="required">*</span></span>
                                <select name="month" id="month" class="select-field">
                                    <option value="">Select Upload Month</option>
                                    <?php
                                    for ($i=1; $i<=12; $i++){
                                        $month=date("F", mktime(0, 0, 0, "$i", 1, 2000));	
                                        echo "<option value='$i'>$month</option>";
                                    }
                                    ?>
                                </select>
                            </label>
                            <label for="year">
                                <span>Year <span class="required">*</span></span>
                                <select name="year" id="year" class="select-field">
                                    <option value="">Select Year</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                </select>
                            </label>
                            <label for="file">
                                <span><span id="file_label">Upload file</span><span class="required">*</span></span>
                                <input type="file" name="file" id="file" class="input-field" />

                            </label>
                            <input type="hidden" name="filetype" id="filetype" value="Reload_WFP" class="form-control" />
                            <div id="merchant_fields">
                                <label for="merchant">
                                    <span>Merchant<span class="required">*</span></span>
                                    <select id="merchant" name="merchant" class="select-field">
                                        <option value="">Select Merchant</option>
                                        <?php for($i=1;$i<=$merchants[0][0];$i++){ ?>
                                        <option value="<?php echo $merchants[$i]["id"]?>"><?php echo $merchants[$i]["id"].": ".$merchants[$i]["fullname"]?></option>
                                        <?php } ?>
                                    </select>
                                </label>
                                <label for="date_format">
                                    <span>Date Format</span>
                                    <select id="date_format" name="date_format" class="select-field">
                                        <option value="1">MM/DD/YYYY</option>
                                        <option value="2">DD/MM/YYYY</option>
                                        <option value="3">YYYY/MM/DD</option>
                                        <option value="4">YYYY/MM/DD HH:MM:SS</option>
                                    </select>
                                </label>
                                <label for="date_separator">
                                    <span>Date Separator</span>
                                    <select id="date_separator" name="date_separator" class="select-field">
                                        <option value="/">/</option>
                                        <option value="-">-</option>
                                        <option value=".">.</option>
                                    </select>
                                </label>
                                <label>
                                    <span>&nbsp;</span>
                                    <a class="a-button" name="select-fields" id="select-fields" style="background-color:green">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Field Mappings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                </label>
                            </div>
                            <div id="loa_fields">
                                <label for="loa_date">
                                    <span>Letter Date<span class="required">*</span></span>
                                    <input name="loa_date" id="loa_date" type="text" class="input-field datepicker" placeholder="Date on the letter" />
                                </label>
                                <label for="loa_location">
                                    <span>Location<span class="required">*</span></span>
                                    <select name="loa_location" id="loa_location" class="select-field">
                                        <option value="">Select Location</option>
                                        <option value="Za'atari">Za'atari</option>
                                        <option value="Azraq">Azraq</option>
                                        <option value="KAP">KAP</option>
                                        <option value="Cyber City">Cyber City</option>
                                        <option value="Community">Community</option>
                                        <option value="Community Cash Study">Community Cash Study</option>
                                    </select>
                                </label>
                                <label for="loa_amount">
                                    <span>Amount<span class="required">*</span></span>
                                    <input name="loa_amount" id="loa_amount" type="text" class="input-field" placeholder="Amount" />
                                </label>
                            </div>
                            <div id="vrl_fields">
                                <label for="vrl_type">
                                    <span>VRL Type<span class="required">*</span></span>
                                    <select name="vrl_type" id="vrl_type" class="select-field">
                                        <option value="">Select VRL Type</option>
                                        <option value="JAB Pressed">JAB Pressed Cards</option>
                                        <option value="Programme Listing">Programmme Listing</option>
                                        <option value="Replaced Cards">Replaced Cards</option>
                                        <option value="Lost Cards">Lost Cards</option>
                                        <option value="Destroyed Cards">Destroyed Cards</option>
                                    </select>
                                </label>
                            </div>
                            <div id="paper_voucher_fields" style="display:none">
                                <label for="paper_voucher_type">
                                    <span>Voucher Type<span class="required">*</span></span>
                                    <select name="paper_voucher_type" id="paper_voucher_type" class="select-field">
                                        <option value="">Select Voucher Type</option>
                                        <option value="1">Planned Vouchers</option>
                                        <option value="2">Printed Vouchers</option>
                                        <option value="3">Delivered Vouchers to CP</option>
                                        <option value="4">Distributed Vouchers</option>
                                        <option value="5">Redeemed Vouchers</option>
                                        <option value="6">Returned Vouchers</option>
                                        <option value="7">Expired Vouchers</option>
                                        <option value="8">Destroyed Vouchers</option>
                                    </select>
                                </label>
                            </div>
                            <label>
                                <span>&nbsp;</span>
                                <input type="submit" name="submit" value="Upload Data" id="submit" />
                            </label>
                        </form>
                    </div>
                </td>
                <td>
                    <div class="available_reloads" style="float: right; width: 400px">
                        <div id="Available_Reload_WFP" style="margin-left: 20px">
                            Already uploaded data:
            <br /><button>Show..</button>
                        </div>
                        <div id="Available_Reload_LOA" style="margin-left: 20px">
                            Already uploaded data:<br /><button>Show..</button>
                        </div>
                        <div id="Available_MEPS_Data" style="margin-left: 20px">
                            Already uploaded data:
            <br /><button>Show..</button>
                        </div>
                        <div id="Available_JAB" style="margin-left: 20px">Already uploaded data:<br /><button>Show..</button></div>
                        <div id="Available_Merchant_Sales" style="margin-left: 20px">Already uploaded data:<br /><button>Show..</button></div>
                        <div id="Available_Merchant_Purchases" style="margin-left: 20px">Already uploaded data:<br /><button>Show..</button></div>
                        <div id="Available_Merchants_WFP" style="margin-left: 20px">Already uploaded data<br /><button>Show..</button></div>
                        <div id="Available_VRL" style="margin-left: 20px">Already uploaded data <br /><button>Show..</button></div>
                        <div id="Available_Unused_Exceptions" style="margin-left: 20px">Already uploaded data <br /><button>Show..</button></div>
                        <div id="Available_Paper_Vouchers" style="margin-left: 20px">Already uploaded data <br /><button>Show..</button></div>
                    </div>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            setTab("Reload_WFP"); //initial tab
            $(function () {
                $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
                $("#submit").click(function () {
                    if(formIsValid()) {
                        $('#loadingb').show();
                        return true;
                    }
                    else {
                        return false;
                    }
                });
            });
            $('#select-fields').on('click', function () {
                //debugger;
                if (formIsValid()) {
                    $("body").trigger("popUpLoading");
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    var year = $('#year').val();
                    var month = $('#month').val();
                    var date_format = $('#date_format').val();
                    var date_separator = $('#date_separator').val();
                    var filetype = $('#filetype').val();
                    var merchant = $('#merchant').val();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'upload_retailer_file.php', // point to server-side PHP script
                        dataType: 'text',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (php_script_response) {
                            if (php_script_response.split("|")[0] == 'success') {
                                var filename = php_script_response.split("|")[1];
                                nexturl = "upload_select_fields.php?month=" + month
                                    + "&year=" + year + "&merchant=" + merchant + "&date_format=" + date_format
                                    + "&date_separator=" + date_separator + "&filename=" + filename
                                    + "&filetype=" + filetype;
                                $.get(nexturl, function (data) {
                                    showPopup("Upload Retailer File", data, "")
                                });
                            }
                            else {
                                showPopup("Upload Retailer File", "<div style=width:600px>Error reading the file " + php_script_response + "</div>", "");
                            }
                        }
                    });
                }
            });
            var formIsValid = function () {
                //debugger;
                var msg = "";
                var val = 0;
                var year = $('#year').val();
                var month = $('#month').val();
                var file = $('#file').val();
                var filetype = $('#filetype').val();
                var merchant = $('#merchant').val();
                var loa_date = $('#loa_date').val();
                var loa_location = $('#loa_location').val();
                var loa_amount = $('#loa_amount').val();
                if (!year.trim()) {
                    msg = msg + 'Select a year!!\r\n';
                    val = 1;
                }
                if (!month.trim()) {
                    msg = msg + 'Select month!!\r\n';
                    val = 1;
                }
                if (!file.trim()) {
                    msg = msg + 'Select file to upload!!\r\n';
                    val = 1;
                }
                if ((filetype == "Merchant_Sales" || filetype == "Merchant_Purchases") && !merchant.trim()) {
                    msg = msg + 'Select the source of data!!\r\n';
                    val = 1;
                }
                if (filetype == "Reload_LOA" && !loa_date) {
                    msg = msg + 'Kindly select date!!\r\n';
                    val = 1;
                }
                if (filetype == "Reload_LOA" && !loa_location) {
                    msg = msg + 'Kindly select location!!\r\n';
                    val = 1;
                }
                if (filetype == "Reload_LOA" && !loa_amount) {
                    msg = msg + 'Kindly select amount!!\r\n';
                    val = 1;
                }
                if (val == 1) {
                    alert(msg);
                    return false;
                }
                else{
                    return true;
                }
            }
        </script>
    </div>
</div>

