<?php session_start();
      if (isset($_GET['msg'])){
          $msg=$_GET['msg'];
          if ($msg=="nf"){
              $msg="*This global account does not have rights to use the Cash Triangulation Analyzer. Please contact your System Administrator";
          }
          elseif ($msg=="nf2"){
              $msg="*There was a problem setting your profile active. Please contact your System Administrator";
          }
          elseif ($msg=="nf3"){
              $msg="Invalid user name or password";
          }
          elseif ($msg=="nf4"){
              $msg="The GLOBAL domain could not be reached to authenticate your password. Please try again or contact the system administrator";
          }    
      }
      else {
          $msg="";
      }
      if(isset($_GET["returnurl"])){
          $_SESSION["returnurl"]=$_GET["returnurl"];
      }
?>
<!doctype html>
<html class="" id="jewel" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WFP Jordan Cash Analyzer</title>

    <meta name="description" content="">
        <meta name="viewport" content="width=960, maximum-scale=0.92"/>
                    <link rel="stylesheet" href="css/main.css" />
        
                <!--[if IE 9]><link rel="stylesheet" href="css/main_ie.css" /><![endif]-->
                    <link rel="icon" href="images/favicon.ico">    <script type="text/javascript"> if (!window.console) console = {log: function() {}}; </script>
            <script type="text/javascript" src="js/html5.js"></script>
    
    <script type="text/javascript" src="js/head.min.js"></script>
    <script type="text/javascript">document.getElementsByTagName('html')[0].id='jewel'</script>

            <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>


             
     <!-- custom scripts block --> </head>
<body >

            <ui-view>
            <div id="nav" class="structural">
                            </div>

            <div id="home" class="structural">
                <div class="wrapper window">
                            <div id="login">
		<div id="login_logo"></div>
		<div id="loginwindow">
			<div class="loginwindow_title">
            <h1>Log into CBT Triangulation Analyzer</h1>
        </div>
        <div class="loginwindow_content">
            <p>Please enter your user name and password provided by the System Administrator.
                    If you are WFP staff, enter your global account credentials.</p>
            <form action="verify_user.php" method="post">
            <div class="notice_error" id="notice_error" >
            	            </div>
            <div class="notice_success" id="notice_success"></div>
            <div class="holder_label"><span>Username</span><input required="required" type="text" class="holder_field" id="user" name="user" value="" /></div>
            <div class="holder_label"><span>Password</span><input required="required" type="password" class="holder_field" id="pass" name="pass" /></div>
            
            <?php
            if (strlen($msg)>0){
                echo "	
                <p align=center><font color=#FF0000>$msg</font>";
            }
            ?>
            <div class="loginwindow_buttons">
                <p>By logging in you accept our latest <a href="#">Terms and Conditions</a>.</p>
                <input type="submit" class="btn_styled preferred" id="btn_login" value="Login" />
                <a class="btn_styled" id="forgotpassword">Forgot Password</a>
                <div class="clear"></div>
            </div>
                </form>
        </div>
		</div>
	</div>
                </div>

                <div id="footer">
                    <span>Copyright &copy; 2015 WFP Jordan Internet Services.</span>
                    <a class="act_popup_open" title="Report a bug!" rel="warning" minlength="2"
                       href="/en/dashboard/report-a-bug">Report a Bug</a>
                    <a class="act_popup_open" title="Request a Feature" rel="info"
                       href="/en/dashboard/feature-request">Request
                        a Feature</a>
                    <a class="act_popup_open" title="Contact Us" rel="info"
                       href="/en/dashboard/contact-us">Contact
                        us</a>
                    <span style="display: none;" id="showServerName">cz5</span>
                </div>

            </div>

            <div id="panels"></div>
            <div id="blackout" class="structural"></div>

            <div id="temp"></div>
            <div id="loader" class="animated pop" style="display: none">
                <div></div>
                processing your request
            </div>
        </ui-view>
    
</body>
</html>
