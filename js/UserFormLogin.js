var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-480847-8']);
_gaq.push(['_setDomainName', 'WFP Jordan.com']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();

window.addEventListener('error', function(e) {
    _gaq.push([
        '_trackEvent',
        'JavaScript Error',
        e.message,
        e.filename + ':  ' + e.lineno,
        true
    ]);
});

// Track AJAX errors (jQuery API)
$(document).ajaxError(function(e, request, settings) {
    _gaq.push([
        '_trackEvent',
        'Ajax error',
        settings.url,
        e.result,
        true
    ]);
});

$(document).ready(function () {

    //FADING BUTTON MOUSEOVERS
    //Adds hover spans to items with the class of .btnfade and does a fade-in animation

    $('.btnfade').append('<span class="hover"></span>').each(function () {
        var $span = $('> span.hover', this).css('opacity', 0);
        $(this).hover(function () {
            $span.stop().fadeTo(300, 1);
        }, function () {
            $span.stop().fadeTo(300, 0);
        });
    });

    $('#password').keyup(function (e) {
        if (e.keyCode == 13) {
            login();
        }
    });

    $('#btn_login').click(function () {
        login();
    });


    function login() {
        $('#login_form').addClass('loading');
        $("#btn_login").fadeOut();
        $("#forgotpassword").fadeOut();
        $('#login_form').submit();
        $('#login_form input').prop('readonly', true);
    }

    function forgotPasswordEvent() {
        $('#login_form').addClass('loading');
        $('#forgotpassword').off('click', forgotPasswordEvent);

        $.ajax({
            method: "POST",
            url: "/request_password",
            data: { email: $('#username').val() }
        })
            .done(function(data) {
                if (data.match(/reminder has been sent to/)) {
                    $('#login_form #notice_success').html(data).show();
                } else {
                    $('#login_form #notice_error').html(data).show();
                }
            })
            .fail(function() {
                $('#login_form #notice_error').html('There was a problem processing the request.').show();
            })
            .always(function() {
                $('#login_form').removeClass('loading');
                setTimeout(function () {
                    $('#login_form #notice_success, #login_form #notice_error').slideUp(500);
                    $('#forgotpassword').on('click', forgotPasswordEvent);
                }, 3000);
            });
    }

    $('#forgotpassword').on('click', forgotPasswordEvent);

    //FIELD
    //Common field triggers

    $('.field').focus(function () {
        $(this).addClass("active");
        if (this.value == this.defaultValue) {
            this.value = '';
        }
        if (this.value != this.defaultValue) {
            this.select();
        }
    });
    $('.field').blur(function () {
        $(this).removeClass("active");
        if ($.trim(this.value) == '') {
            this.value = (this.defaultValue ? this.defaultValue : '');
        }
    });

});
$(document).ready(function () {
    $(document).off('click', '#reset_password #change_password').on('click', '#reset_password #change_password', function (e) {
        e.preventDefault();
        var form = $(this).parents('form');

        var validate = form.validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 5
                },
                new_password_again: {
                    equalTo: "#new_password"
                }
            },
            showErrors: function (errorMap, errorList) {
            },
            invalidHandler: function (form, validator) {
                $.each(validator.errorList, function (index, item) {
                    form_field_animate_error($(item['element']), item['message']);
                });
            }
        });

        function show_error_notice(form, message) {
            var notice_error = $(form).find('div.notice_error');
            notice_error.html(message);
            notice_error.fadeIn(500, function () {
                setTimeout(function () {
                    notice_error.slideUp(500);
                }, 3000);
            });
        }

        if (form.valid()) {
            $.ajax({
                global: false,
                url: form.attr('action'),
                data: form.serialize(),
                type: "POST",
                beforeSend: function (xhr) {
                },
                success: function (data) {
                    if (data.code == 1) {
                        document.location = '/en/';
                    } else {
                        show_error_notice(form, data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    message = 'An Error Occured when trying to update your details';
                    show_error_notice(form, message);
                }
            });
        }
        ;

        $('form div.notice_error')

    });

});
