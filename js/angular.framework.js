(function () {
    "use strict";
    angular.element(document).ready(function () {
        angular.module("Cash Analyzer", ["ngSanitize", "ui.router", "app.config", "voice.config"]);
        angular.bootstrap(document, ["Cash Analyzer"])
    })
})();
(function () {
    "use strict";
    var appConfig = angular.module("app.config", ["app.controller", "app.factory", "app.filter"]);
    appConfig.run(["$window", "$timeout", "$rootScope", "$state", "$location", "$urlRouter", "PanelService", "PopupService", function ($window, $timeout, $rootScope, $state, $location, $urlRouter, PanelService, PopupService) {
        $rootScope.$watch(function () {
            return _.size(PanelService.panels) >= 1 || _.size(PopupService.popups) >= 1
        }, function (newValue, oldValue) {
            $timeout(function () {
                newValue ? $("#blackout").fadeIn(500) : $("#blackout").fadeOut(500)
            })
        }, true);
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            var absToUrl = $state.href(toState.name, $state.params, {
                absolute: true
            });
            if (absToUrl.split("#")[1] === $location.path() && fromState.url != "^") {
                return false
            }
            if (toState.hasOwnProperty("panel") && !$rootScope.panelClosing) {
                PanelService.open({
                    templateUrl: toState.panel.templateUrl,
                    controller: toState.panel.controller,
                    title: toState.panel.title,
                    type: toState.panel.type,
                    subtitle: toState.panel.subtitle,
                    scope: event.targetScope,
                    resolve: toState.panel.resolve,
                    state: toState.name,
                    stateParams: toParams
                })
            }
        });
        $rootScope.$on("$locationChangeSuccess", function (event, oldUrl, newUrl) {
            $rootScope.panelClosing = false
        })
    }]);
    appConfig.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.interceptors.push("HttpInterceptor");
        $stateProvider.state("base", {
            url: "/"
        }).state("app", {
            url: "/ah",
            controller: "appController"
        }).state("app.one", {
            url: "/one/:test/:another",
            panel: {
                templateUrl: "../bundles/ahuiCash Analyzer/js/app/modules/app/views/one.html",
                controller: "testController",
                resolve: {
                    one: ["voiceFactory", function (voiceFactory) {
                        return voiceFactory.getUserProducts()
                    }]
                }
            }
        }).state("app.two", {
            url: "/two",
            panel: {
                templateUrl: "../bundles/ahuiCash Analyzer/js/app/modules/app/views/two.html",
                controller: "againController"
            }
        }).state("app.three", {
            url: "/three",
            panel: {
                templateUrl: "../bundles/ahuiCash Analyzer/js/app/modules/app/views/three.html",
                controller: "againController",
                resolve: {
                    one: ["voiceFactory", function (voiceFactory) {
                        return voiceFactory.getUserProducts()
                    }]
                }
            }
        })
    }]);
    appConfig.constant("CONFIG", {
        dev_mode: 0,
        base_url: "https://Cash Analyzer.WFP Jordan.com/en"
    })
})();
(function () {
    "use strict";
    var appController = angular.module("app.controller", []);
    appController.controller("appController", ["$state", "$scope", "$rootScope", "$timeout", "PanelService", "PopupService", function ($state, $scope, $rootScope, $timeout, PanelService, PopupService) {
        $rootScope.goHome = function (e) {
            $(".popwindow").slideDown("200", function () {
                $(".popwindow").remove()
            });
            var panelsToClose = $("#panels .pathwindow");
            panelsToClose.removeClass("active");
            panelsToClose.each(function (i, mypanel) {
                var jwindow = $(mypanel);
                jwindow.animate({
                    left: "150%"
                }, 500 / (i + 1), function () {
                    $(this).remove()
                });
                if (i == panelsToClose.length - 1) {
                    $("#blackout").fadeOut(500);
                    $("#nav a").removeClass("active");
                    $("#nav a").first().removeClass("path");
                    setTimeout(function () {
                        $("#nav a.path").fadeOut().remove()
                    }, 100);
                    history.pushState({}, "", "#");
                    window.location.hash = "#"
                }
            });
            $timeout(function () {
                $state.go("base")
            }, 1e3)
        }
    }])
})();
(function () {
    var factories = angular.module("app.factory", []);
    factories.factory("PanelService", ["$injector", "$timeout", "$location", "$compile", "$controller", "$http", "$rootScope", "$q", "$window", "$state", "Tools", "BreadcrumbService", function ($injector, $timeout, $location, $compile, $controller, $http, $rootScope, $q, $window, $state, Tools, BreadcrumbService) {
        var self = {};
        self.panelCount = 0;
        self.panels = {};
        self.history = {};
        var openPanel = function () {
            var pathwindows = angular.element(document.querySelectorAll("#panels .pathwindow"));
            var numberofwindows = pathwindows.length;
            $timeout(function () {
                pathwindows.each(function (i, mypanel) {
                    var jwindow = angular.element(mypanel);
                    jwindow.removeClass("active");
                    if (numberofwindows > 2) {
                        jwindow.animate({
                            left: "-50%"
                        }, 500, function () {
                            $(this).addClass("active")
                        });
                        numberofwindows--;
                        return true
                    }
                    if (numberofwindows == 2) {
                        jwindow.animate({
                            left: "-25%"
                        }, 500, function () {
                            $(this).addClass("active")
                        });
                        numberofwindows--;
                        return true
                    }
                    if (numberofwindows == 1) {
                        jwindow.animate({
                            left: "50%"
                        }, 500, function () {
                            $(this).addClass("active")
                        });
                        numberofwindows--;
                        return true
                    }
                })
            }, 250)
        };
        var closePanel = function () {
            var pathwindows = angular.element(document.querySelectorAll("#panels .pathwindow"));
            var numberofwindows = pathwindows.length;
            pathwindows.each(function (i, mypanel) {
                var jwindow = angular.element(mypanel);
                jwindow.removeClass("active");
                if (numberofwindows > 3) {
                    jwindow.animate({
                        left: "-50%"
                    }, 500, function () {
                        $(this).addClass("active")
                    });
                    numberofwindows--;
                    return true
                }
                if (numberofwindows == 3) {
                    jwindow.animate({
                        left: "-25%"
                    }, 500, function () {
                        $(this).addClass("active")
                    });
                    numberofwindows--;
                    return true
                }
                if (numberofwindows == 2) {
                    jwindow.animate({
                        left: "50%"
                    }, 500, function () {
                        $(this).addClass("active");
                        $(this).find(".pathwindow_blackout").fadeOut(250)
                    });
                    numberofwindows--;
                    return true
                }
                if (numberofwindows == 1) {
                    jwindow.animate({
                        left: "150%"
                    }, 500, function () {
                        $(this).remove()
                    });
                    numberofwindows--;
                    return true
                }
            });
            if ($("#nav a").length >= 3) {
                var navitem = $("#nav a:last").prev();
                navitem.removeClass("active");
                $timeout(function () {
                    navitem.remove()
                }, 500)
            }
            $timeout(function () {
                if ($("#nav a").length < 3) {
                    $("#nav a").first().removeClass("path");
                    $("#blackout").fadeOut(500)
                }
            }, 500)
        };
        self.open = function (options) {
            var deferred = $q.defer();
            var panelId = ++self.panelCount;
            var panel = {
                id: panelId,
                active: true,
                title: typeof options.title !== "undefined" ? options.title : "Panel Title",
                subtitle: typeof options.subtitle !== "undefined" ? options.subtitle : null,
                state: typeof options.state !== "undefined" ? options.state : "base",
                type: typeof options.type !== "undefined" ? options.type : ""
            };
            var opts = {
                scope: angular.isObject(options.scope) ? options.scope.$new() : $rootScope.$new(),
                template: typeof options.template !== "undefined" ? options.template : null,
                templateUrl: typeof options.templateUrl !== "undefined" ? options.templateUrl : null,
                controller: typeof options.controller !== "undefined" ? options.controller : null,
                controllerAs: typeof options.controllerAs !== "undefined" ? options.controllerAs : null,
                stateParams: angular.extend({}, options.stateParams),
                resolve: angular.extend({}, options.resolve),
                appendTo: typeof options.appendTo !== "undefined" ? options.appendTo : "#panels"
            };
            if (_.isNull(opts.controller)) {
                deferred.reject("No controller has been specified.");
                return deferred.promise
            }
            if (angular.isString(opts.controllerAs)) {
                opts.controller = opts.controller + " as " + opts.controllerAs
            }
            _.each(self.panels, function (panel, index) {
                panel.active = false
            });
            angular.forEach(opts.resolve, function (value, key) {
                opts.resolve[key] = angular.isString(value) ? $injector.get(value) : $injector.invoke(value, null, {
                    $stateParams: opts.stateParams
                }, key)
            });
            $q.all({
                template: Tools.getTemplate(opts.template, opts.templateUrl),
                locals: $q.all(opts.resolve)
            }).then(function (data) {
                var template = data.template;
                var locals = data.locals;
                var panelTemplate = '<div class="pathwindow {{ panel.type }} panel" id="panel_{{ panel.id }}">                                        <div class="pathwindow_title">                                            <div class="icon"></div>                                            <a class="ah_btn_back" data-ng-click="panel.close($event)"><span></span><p>Back</p></a>                                            <h1 data-ng-if="!panel.subtitle" data-ng-click="panel.onTitleClick($event)"> {{ panel.title }} </h1>                                            <span data-ng-if="panel.subtitle">                                                <h2 data-ng-click="panel.onTitleClick($event)">{{ panel.title }}</h2>                                                <h3 data-ng-click="panel.onSubTitleClick($event)">{{ panel.subtitle }}</h3>                                            </span>                                        </div>' + template + '                                        <div class="pathwindow_blackout" data-ng-class="{active: !panel.active}"></div>                                    </div>';
                var panelTemplateElement = angular.element(panelTemplate).css({
                    left: "150%"
                });
                panel.close = function (event) {
                    $timeout(function () {
                        $rootScope.panelClosing = true;
                        opts.scope.$destroy();
                        closePanel();
                        delete self.panels[panelId];
                        var lastPanelKey = _.findLastKey(self.panels);
                        var lastPanel = self.panels[lastPanelKey];
                        if (angular.isObject(lastPanel)) {
                            lastPanel.active = true
                        }
                        $state.go(typeof lastPanel == "undefined" || _.size(self.panels) == 0 ? "base" : lastPanel.state)
                    })
                };
                panel.onTitleClick = function (event) {
                    opts.scope.$broadcast("titleClick", event)
                };
                panel.onSubTitleClick = function (event) {
                    opts.scope.$broadcast("subTitleClick", event)
                };
                panel.setTitle = function (title) {
                    $timeout(function () {
                        panel.title = title;
                        $('#nav .path[ rel ="' + panelId + '"] > dl > dt').html(title)
                    })
                };
                panel.setSubTitle = function (subtitle) {
                    $timeout(function () {
                        panel.subtitle = subtitle
                    })
                };
                panel.getPanelScope = function () {
                    return opts.scope
                };
                self.panels[panelId] = self.history[panelId] = opts.scope.panel = panel;
                $timeout(function () {
                    var panelElement = $compile(panelTemplateElement)(opts.scope);
                    $controller(opts.controller, angular.extend(locals, {
                        $scope: opts.scope,
                        $element: panelElement,
                        panel: panel
                    }));
                    angular.element(document.querySelector(opts.appendTo)).append(panelElement);
                    openPanel();
                    BreadcrumbService.updatePath(self.panels[panelId]);
                    deferred.resolve(panel)
                })
            });
            return deferred.promise
        };
        self.getActivePanel = function () {
            return _.find(self.panels, {
                active: true
            })
        };
        self.getAllPanels = function () {
            return self.panels
        };
        return self
    }]);
    factories.factory("PopupService", ["$injector", "$timeout", "$compile", "$controller", "$http", "$rootScope", "$q", "Tools", "PanelService", "LoadingService", function ($injector, $timeout, $compile, $controller, $http, $rootScope, $q, Tools, PanelService, LoadingService) {
        var self = {};
        self.count = 0;
        self.popups = {};
        self.open = function (options) {
            var deferred = $q.defer();
            var id = ++self.count;
            var popup = {
                id: id,
                active: false,
                type: typeof options.type !== "undefined" ? options.type : "info",
                title: typeof options.title !== "undefined" ? options.title : "Popup Title",
                style: angular.extend({}, options.style)
            };
            var opts = {
                template: typeof options.template !== "undefined" ? options.template : null,
                templateUrl: typeof options.templateUrl !== "undefined" ? options.templateUrl : null,
                controller: typeof options.controller !== "undefined" ? options.controller : null,
                controllerAs: typeof options.controllerAs !== "undefined" ? options.controllerAs : null,
                scope: angular.isObject(options.scope) ? options.scope.$new() : $rootScope.$new(),
                resolve: angular.extend({}, options.resolve),
                appendTo: typeof options.appendTo !== "undefined" ? options.appendTo : "body"
            };
            LoadingService.show();
            if (opts.controllerAs) {
                if (_.isNull(opts.controller)) {
                    deferred.reject("No controller has been specified.");
                    return deferred.promise
                }
                opts.controller = opts.controller + " as " + options.controllerAs
            }
            angular.forEach(popup.resolve, function (value, key) {
                opts.resolve[key] = angular.isString(value) ? $injector.get(value) : $injector.invoke(value, null, null, key)
            });
            $q.all({
                content: Tools.getTemplate(opts.template, opts.templateUrl),
                locals: $q.all(opts.resolve)
            }).then(function (data) {
                var content = data.content;
                var locals = data.locals;
                var template = angular.element('<div class="popwindow {{ popup.type }}" data-ng-class="{ active: popup.active }" data-ng-style="popup.style">                                                    <div class="popwindow_title">                                                        <div class="icon"></div>                                                        <a class="btn_close" data-ng-click="popup.close($event)">X</a>                                                        <h1>{{ popup.title }}</h1>                                                    </div>                                                    <div class="popwindow_content small" style="min-width: 350px;">' + content + "</div>                                                </div>");
                popup.close = function () {
                    $timeout(function () {
                        var styles = {
                            top: "53%"
                        };
                        popup.style = angular.extend(popup.style, styles);
                        popup.setActive(false);
                        opts.element.animate({
                            marginTop: "1200px"
                        }, 750, function () {
                            opts.scope.$destroy();
                            opts.element.remove();
                            delete self.popups[id]
                        });
                        $("body").trigger("popUpClosed");
                        deferred.resolve(popup)
                    })
                };
                popup.setActive = function (value) {
                    $timeout(function () {
                        popup.active = value
                    })
                };
                var element = opts.element = $compile(template)(opts.scope);
                if (!_.isNull(opts.controller) || !_.isNull(opts.controllerAs)) {
                    $controller(opts.controller, angular.extend(locals, {
                        $scope: opts.scope,
                        $element: element,
                        popup: popup
                    }))
                }
                self.popups[id] = opts.scope.popup = popup;
                angular.element(document.querySelector(opts.appendTo)).append(element);
                $timeout(function () {
                    var newTop = parseInt(element.outerHeight()) / 2;
                    var newLeft = parseInt(element.width()) / 2;
                    var styles = {
                        marginLeft: -newLeft
                    };
                    if (angular.element(document.querySelector(".sf-toolbarreset")).length > 0 && newTop > 1e3) {
                        newTop = window.outerHeight / 2.5;
                        newLeft = window.outerWidth / 2.5;
                        styles.minWidth = window.outerWidth / 1.3;
                        styles.minHeight = window.outerHeight / 1.4;
                        styles.marginLeft = -newLeft;
                        styles.height = "10%";
                        styles.overflowY = "scroll"
                    }
                    popup.style = angular.extend(popup.style, styles);
                    element.animate({
                        marginTop: -newTop
                    }, 750, function () {
                        popup.setActive(true)
                    })
                }, 250);
                LoadingService.hide();
                $("body").trigger("popUpLoaded")
            });
            return deferred.promise
        };
        return self
    }]);
    factories.factory("BreadcrumbService", ["$injector", "$timeout", "$compile", "$rootScope", "$state", function ($injector, $timeout, $compile, $rootScope, $state) {
        var self = {};
        self.updatePath = function (panel) {
            $("#nav a.path").removeClass("disabled");
            var panels = $injector.get("PanelService").getAllPanels();
            $timeout(function () {
                var scope = $rootScope.$new();
                scope.close = function ($event) {
                    var navElement = angular.element($event.currentTarget);
                    var goToId = navElement.attr("rel");
                    var goToPanel = panels[goToId];
                    var goToPanelElement = angular.element(document.querySelector("#panel_" + goToId));
                    var panelsToClose = goToPanelElement.nextAll();
                    var panelLeft = goToPanelElement.prev();
                    if (navElement.hasClass("disabled")) {
                        return
                    }
                    $rootScope.panelClosing = true;
                    _.each(panels, function (panel, index) {
                        panel.active = false
                    });
                    goToPanelElement.removeClass("active");
                    goToPanelElement.animate({
                        left: "50%"
                    }, 500, function () {
                        $(this).addClass("active");
                        $(this).find(".pathwindow_blackout").fadeOut(250)
                    });
                    panelLeft.each(function (i, mypanel) {
                        var jwindow = $(mypanel);
                        jwindow.removeClass("active");
                        jwindow.animate({
                            left: "-25%"
                        }, 500, function () {
                            $(this).addClass("active")
                        })
                    });
                    panelsToClose.each(function (i, mypanel) {
                        var jwindow = $(mypanel);
                        jwindow.removeClass("active");
                        jwindow.animate({
                            left: "150%"
                        }, 500, function () {
                            $(this).remove()
                        });
                        var scope = jwindow.scope();
                        delete panels[scope.panel.id];
                        scope.$destroy()
                    });
                    var lastPanelKey = _.findLastKey(panels);
                    var lastPanel = panels[lastPanelKey];
                    if (angular.isObject(lastPanel)) {
                        lastPanel.active = true
                    }
                    $state.go(goToPanel.state)
                };
                var element = $compile('<a class="path disabled" data-ng-click="close($event)" rel="' + panel.id + '"><dl><dt>' + panel.title + "</dt><dd></dd></dl></a>")(scope);
                $("#nav a:last").before(element)
            }, 100);
            $timeout(function () {
                $("#nav a:last").prev().addClass("active");
                if ($("#nav a").length > 2) {
                    $("#nav a :first").addClass("path");
                    $("#blackout").fadeIn(500)
                }
            }, 150)
        };
        return self
    }]);
    factories.factory("LoadingService", ["$rootScope", "$q", "$timeout", "$injector", function ($rootScope, $q, $timeout, $injector) {
        var self = {};
        self.show = function () {
            $timeout(function () {
                $("#loader").show();
                $("#blackout").fadeIn(500)
            })
        };
        self.hide = function () {
            $timeout(function () {
                $("#loader").hide();
                if (_.size($injector.get("PanelService").panels) < 1) {
                    $("#blackout").fadeOut(500)
                }
            })
        };
        return self
    }]);
    factories.factory("HttpInterceptor", ["$injector", "$q", "LoadingService", function ($injector, $q, LoadingService) {
        return {
            request: function (config) {
                if (config.url.indexOf(".html") <= 0) {
                    LoadingService.show()
                }
                return config
            },
            requestError: function (rejection) {
                LoadingService.hide();
                return $q.reject(rejection)
            },
            response: function (response) {
                if (response.config.url.indexOf(".html") <= 0) {
                    LoadingService.hide()
                }
                return response
            },
            responseError: function (rejection) {
                LoadingService.hide();
                if (rejection.status === 401) {
                    window.location = "/en/login"
                }
                if (rejection.config.url.indexOf("voice/balances") >= 1) {
                    return $q.reject(rejection)
                }
                if (rejection.status === 400) {
                    $injector.get("PopupService").open({
                        title: "Oops an Error Occurred!",
                        template: "<p>" + (rejection.data.hasOwnProperty("message") ? rejection.data.message : "Oh no, it seems something went wrong! Please ensure any required data input has been given.") + "</p>",
                        type: "warning"
                    })
                }
                if (rejection.status >= 500 || rejection.status === 0) {
                    $injector.get("PopupService").open({
                        title: "Oops an Error Occurred!",
                        template: "<p>" + (rejection.data.hasOwnProperty("message") ? rejection.data.message : "Oh no, it seems something went wrong!") + "</p>",
                        type: "warning"
                    })
                }
                return $q.reject(rejection)
            }
        }
    }]);
    factories.factory("Tools", ["$templateCache", "$http", "$q", function ($templateCache, $http, $q) {
        var self = {};
        self.getTemplate = function (template, templateUrl) {
            var deferred = $q.defer();
            if (angular.isString(template)) {
                deferred.resolve(template);
                return deferred.promise
            }
            $http({
                method: "GET",
                url: templateUrl,
                cache: $templateCache
            }).then(function (result) {
                deferred.resolve(result.data)
            }, function (error) {
                deferred.reject(error)
            });
            return deferred.promise
        };
        return self
    }])
})();
(function () {
    "use strict";
    angular.module("app.filter", []).filter("bytesFormat", function () {
        return function (bytes, precision, unitClass) {
            unitClass = unitClass || "";
            if (bytes === 0) {
                return bytes + '<span class="' + unitClass + '">bytes</span>'
            }
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return "-";
            if (typeof precision === "undefined") precision = 1;
            var units = ["bytes", "KB", "MB", "GB", "TB", "PB"],
                number = Math.floor(Math.log(bytes) / Math.log(1024));
            var result = (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision);
            return result + '<span class="' + unitClass + '">' + units[number] + "</span>"
        }
    }).filter("formatCellNumber", function () {
        return function (input) {
            var number = input.toString(),
                formatted = number.replace("27", "0");
            return formatted.substring(3, 0) + " " + formatted.substring(6, 3) + " " + formatted.substring(6)
        }
    }).filter("bytesToSize", function () {
        return function (bytes, precision, AddLineBreaks, asArray) {
            var kilobyte = 1024;
            var megabyte = kilobyte * 1024;
            var gigabyte = megabyte * 1024;
            var terabyte = gigabyte * 1024;
            var lineBreaks = AddLineBreaks == true ? "<br />" : "";
            if (bytes >= 0 && bytes < kilobyte) {
                return asArray ? [bytes, "B"] : bytes + lineBreaks + "B"
            } else if (bytes >= kilobyte && bytes < megabyte) {
                return asArray ? [(bytes / kilobyte).toFixed(precision), "KB"] : (bytes / kilobyte).toFixed(precision) + lineBreaks + "KB"
            } else if (bytes >= megabyte && bytes < gigabyte) {
                return asArray ? [(bytes / megabyte).toFixed(precision), "MB"] : (bytes / megabyte).toFixed(precision) + lineBreaks + "MB"
            } else if (bytes >= gigabyte && bytes < terabyte) {
                return asArray ? [(bytes / gigabyte).toFixed(precision), "GB"] : (bytes / gigabyte).toFixed(precision) + lineBreaks + "GB"
            } else if (bytes >= terabyte) {
                return asArray ? [(bytes / terabyte).toFixed(precision), "TB"] : (bytes / terabyte).toFixed(precision) + lineBreaks + "TB"
            } else {
                return asArray ? [bytes, "B"] : bytes + lineBreaks + "B"
            }
        }
    })
})();
(function () {
    "use strict";
    var uiDirective = angular.module("ui.directive", []);
    uiDirective.service("haloService", function () {
        return function (options) {
            options.value = typeof options.value !== "undefined" ? options.value : 0;
            var halo_id = options.selector;
            var halo_val = parseInt(options.value);
            var halo_size = 100;
            var halo_stroke = 6;
            var halo_rad = halo_size / 2 - halo_stroke;
            var halo_col = "#0083a9";
            var halo_shadow = "#0083a9";
            var halos = Raphael(halo_id, 150, 150),
                radius = halo_rad,
                param = {
                    stroke: "#fff",
                    "stroke-width": halo_stroke
                };
            halos.customAttributes.arc = function (value, total, radius) {
                var alpha = 360 / total * value,
                    a = (90 - alpha) * Math.PI / 180,
                    xloc = 49,
                    yloc = 48,
                    x = xloc + radius * Math.cos(a),
                    y = yloc - radius * Math.sin(a),
                    path, isChrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
                if ($.support.boxSizingReliable) {
                    xloc = 50;
                    yloc = isChrome ? 48 : 50;
                    y = yloc - radius * Math.sin(a);
                    x = xloc + radius * Math.cos(a)
                }
                if (total == value) {
                    path = [
                        ["M", xloc, yloc - radius],
                        ["A", radius, radius, 0, 1, 1, xloc - .01, yloc - radius]
                    ]
                } else {
                    path = [
                        ["M", xloc, yloc - radius],
                        ["A", radius, radius, 0, +(alpha > 180), 1, x, y]
                    ]
                }
                return {
                    path: path,
                    stroke: halo_col
                }
            };
            var filledhalo = halos.path().attr(param).attr({
                arc: [options.value, 100, radius]
            });

            function updateVal(value, total, radius, obj, id) {
                obj.animate({
                    arc: [halo_val, 100, halo_rad]
                }, 750, ">", function () {
                    if (value > 0) {
                        obj.glow({
                            color: halo_shadow,
                            width: 5,
                            opacity: .5
                        })
                    }
                })
            } (function () {
                updateVal(halo_val, 100, halo_rad, filledhalo)
            })()
        }
    });
    uiDirective.directive("halo", ["haloService", function (haloService) {
        return {
            scope: {
                percentage: "@"
            },
            link: function (scope, el, attrs) {
                attrs.$observe("percentage", function (val) {
                    var czHalo = new haloService({
                        selector: el[0],
                        value: scope.percentage
                    })
                })
            }
        }
    }]);
    uiDirective.directive("usageGraph", function () {
        return {
            replace: true,
            link: function (scope, element, attrs) { }
        }
    });
    uiDirective.directive("overviewGraph", ["bytesToSizeFilter", function (bytesToSizeFilter) {
        return {
            restrict: "A",
            scope: {
                graphdata: "="
            },
            link: function (scope, el, attrs) {
                scope.$watch("graphdata", function (graphdata) {
                    if (graphdata) {
                        var upVals = _.values(scope.graphdata.upload).map(Number);
                        var downVals = _.values(scope.graphdata.download).map(Number);
                        var maxYVal = _.max(upVals.concat(downVals));
                        new Highcharts.Chart({
                            chart: {
                                renderTo: el[0]
                            },
                            xAxis: {
                                categories: scope.graphdata.days,
                                min: 0,
                                max: scope.graphdata.days.length - 1
                            },
                            yAxis: {
                                min: 0,
                                max: maxYVal <= 0 ? 1 : maxYVal,
                                labels: {
                                    formatter: function () {
                                        if (this.isFirst) {
                                            return this.value
                                        }
                                        return bytesToSizeFilter(this.value, 0, true)
                                    }
                                }
                            },
                            series: [{
                                name: "Download",
                                fillColor: "rgba(0, 140, 194, 0.3)",
                                lineColor: "#008cc2",
                                data: downVals
                            }, {
                                name: "Upload",
                                fillColor: "rgba(117, 0, 198, 0.4)",
                                lineColor: "#7500c6",
                                marker: {
                                    enabled: false,
                                    states: {
                                        hover: {
                                            enabled: false
                                        }
                                    }
                                },
                                data: upVals
                            }]
                        })
                    }
                })
            }
        }
    }])
})();
(function () {
    "use strict";
    var voiceConfig = angular.module("voice.config", ["voice.controller", "voice.factory", "voice.productBundle", "voice.productBundle", "voice.productBundleAction", "voice.serviceManage", "voice.friendlyName", "voice.autoLimit", "voice.cancelService", "voice.changePackage", "connectivity.directives", "ui.directive", "ui.slider"]);
    voiceConfig.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "voiceCzConfig", function ($stateProvider, $urlRouterProvider, $httpProvider, voiceCzConfig) {
        $stateProvider.state("app.voice", {
            url: "/voice",
            panel: {
                title: "WFP Jordan Mobile",
                type: "voice_beta",
                templateUrl: voiceCzConfig.pathTemplates + "/landing/landing.html",
                controller: "voiceController",
                resolve: {
                    userProducts: ["voiceFactory", function (voiceFactory) {
                        return voiceFactory.getUserProducts()
                    }]
                }
            }
        }).state("app.voice.activate", {
            url: "/activate/:csid",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/landing/activate_service.html",
                controller: "voiceActivateController",
                title: "Activate Service",
                type: "voice_beta",
                resolve: {
                    userProduct: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getUserProduct($stateParams.csid)
                    }]
                }
            }
        }).state("app.voice.activate.number", {
            url: "/number",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/landing/activate_service_form.html",
                controller: "voiceActivateController",
                title: "Activate Service",
                type: "voice_beta",
                resolve: {
                    userProduct: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getUserProduct($stateParams.csid)
                    }]
                }
            }
        }).state("app.voice.package", {
            url: "/package/:csid",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/product.html",
                controller: "voicePackageController",
                title: "WFP Jordan Mobile",
                type: "voice_beta",
                resolve: {
                    userProduct: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getUserProduct($stateParams.csid)
                    }],
                    czButtons: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getCZButtons($stateParams.csid)
                    }]
                }
            }
        }).state("app.voice.package.submit_issue", {
            url: "/submit-issue",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/submit_issue.html",
                controller: "voiceSubmitIssueController",
                title: "Submit an Issue",
                type: "voice_beta",
                resolve: {
                    userProduct: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getUserProduct($stateParams.csid)
                    }],
                    issueData: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getVoiceIssueData($stateParams.csid)
                    }]
                }
            }
        }).state("app.voice.package.bundles", {
            url: "/bundles",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/bundles_n_airtime.html",
                controller: "voicePackageBundlesController",
                title: "Bundles & Airtime",
                type: "voice_beta",
                resolve: {
                    topupOptionsAirtime: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getTopupAirtime($stateParams.csid)
                    }],
                    topupOptionsSms: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getTopupSms($stateParams.csid)
                    }],
                    topupOptionData: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getTopupData($stateParams.csid)
                    }]
                }
            }
        }).state("app.voice.package.auto_limit", {
            url: "/autolimit",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/auto_limit.html",
                type: "voice_beta",
                controller: "voicePackageAutoController",
                title: "Auto-Limits",
                resolve: {
                    userProduct: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getUserProduct($stateParams.csid)
                    }],
                    topUpDetails: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getTopupData($stateParams.csid)
                    }],
                    voiceSettings: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.autoVoice.getVoiceSettings($stateParams.csid)
                    }]
                }
            }
        }).state("app.voice.package.usage", {
            url: "/usage",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/usage_history.html",
                type: "voice_beta",
                controller: "voicePackageController",
                title: "Usage History"
            }
        }).state("app.voice.package.notifications", {
            url: "/notifications",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/notifications.html",
                type: "voice_beta",
                controller: "voicePackageNotifications",
                title: "Notifications",
                resolve: {
                    notifications: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getNotifications($stateParams.csid)
                    }],
                    userProduct: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getUserProduct($stateParams.csid)
                    }]
                }
            }
        }).state("app.voice.package.edit", {
            url: "/edit",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/edit.html",
                type: "voice_beta",
                controller: "voicePackageEditController",
                title: "Edit Package",
                resolve: {
                    voicePackages: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getVoicePackages($stateParams.csid)
                    }],
                    userProduct: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getUserProduct($stateParams.csid)
                    }]
                }
            }
        }).state("app.voice.package.sim_swap", {
            url: "/simswap",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/sim_swap.html",
                type: "voice_beta",
                controller: "voicePackageController",
                title: "SIM Swap"
            }
        }).state("app.voice.package.data_usage", {
            url: "/datalinks",
            panel: {
                templateUrl: voiceCzConfig.pathTemplates + "/product/data_usage.html",
                type: "voice_beta",
                controller: "voiceDataUsageController",
                title: "Mobile Data Usage",
                resolve: {
                    userProduct: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getUserProduct($stateParams.csid)
                    }],
                    czButtons: ["voiceFactory", "$stateParams", function (voiceFactory, $stateParams) {
                        return voiceFactory.getCZButtons($stateParams.csid)
                    }]
                }
            }
        })
    }]).constant("voiceCzConfig", {
        env: "dev",
        czUrl: "http://Cash Analyzer.trunk",
        pathTemplates: "../bundles/ahuiCash Analyzer/js/app/modules/voice/views",
        pathRootTemplates: "../bundles/ahuiCash Analyzer/js/app/modules/ui/views",
        pathImages: "/bundles/ahuiCash Analyzer/img",
        signUpLink: "https://Cash Analyzer.WFP Jordan.com/order/#/mobile"
    })
})();
(function () {
    "use strict";
    var voiceController = angular.module("voice.controller", []);
    voiceController.controller("voiceController", ["$scope", "$filter", "voiceCzConfig", "userProducts", function ($scope, $filter, voiceCzConfig, userProducts) {
        $scope.path = {
            mobile_logo: voiceCzConfig.pathImages + "/elements/hero-voice.png",
            order_url: voiceCzConfig.signUpLink
        };
        $scope.products = userProducts;
        $scope.products.active = true;
        $scope.search = {
            value: ""
        };
        $scope.numberRequired = function (lookup) {
            var req = false;
            req = _.find(lookup.attributes, function (myArr) {
                if (myArr.detail_type.id == 9042) return true
            });
            return req
        };
        $scope.getProdStatusClass = function (status) {
            if (status == "Active") {
                return "active"
            } else if (status == "Awaiting DMA") {
                return "trial"
            }
            return "inactive"
        };
        $scope.getProdStatusName = function (status) {
            if (status == "Awaiting DMA") {
                return "Waiting for activation"
            }
            return status
        };
        $scope.getDisplayTitle = function (data) {
            if (data.uid == "") {
                data.uid = "Waiting for number"
            }
            if (data.uid != data.friendlyname) return data.friendlyname;
            return $filter("formatCellNumber")(data.uid)
        };
        $scope.getSubTitle = function (data) {
            if (data.uid != data.friendlyname) return $filter("formatCellNumber")(data.uid);
            return data.solution.display_name
        }
    }]);
    voiceController.controller("voiceSubmitIssueController", ["$scope", "$stateParams", "$filter", "voiceCzConfig", "PanelService", "panel", "PopupService", "userProduct", "issueData", "voiceFactory", function ($scope, $stateParams, $filter, voiceCzConfig, PanelService, panel, PopupService, userProduct, issueData, voiceFactory) {
        $scope.voiceCzConfig = voiceCzConfig;
        $scope.product = userProduct.data;
        $scope.issueData = issueData;
        $scope.submitted = false;
        $scope.issue = {
            service_id: "",
            issue_date: issueData.issue_date,
            issue_description: ""
        };
        panel.setSubTitle($filter("formatCellNumber")($scope.product.solution.uid));
        $scope.submitIssue = function (form, issue) {
            $scope.submitted = true;
            if (form.$invalid) {
                PopupService.open({
                    title: "Whoops!",
                    templateUrl: voiceCzConfig.pathTemplates + "/popups/issueSubmitError.html",
                    type: "warning",
                    scope: $scope
                });
                return false
            }
            voiceFactory.submitVoiceIssue($stateParams.csid, issue.service_id, issue.issue_date, issue.issue_description).then(function (data) {
                PopupService.open({
                    title: "Success!",
                    templateUrl: voiceCzConfig.pathTemplates + "/popups/issueSubmitSuccess.html",
                    type: "success",
                    scope: $scope
                }).then(function () {
                    panel.close()
                })
            })
        }
    }]);
    voiceController.controller("voiceActivateController", ["$scope", "$stateParams", "voiceCzConfig", "PanelService", "panel", "PopupService", "voiceFactory", "userProduct", "voiceServiceManage", function ($scope, $stateParams, voiceCzConfig, PanelService, panel, PopupService, voiceFactory, userProduct, voiceServiceManage) {
        $scope.product = userProduct.data;
        panel.setSubTitle($scope.product.solution.solution.display_name);
        $scope.voiceCzConfig = voiceCzConfig;
        $scope.serviceManage = new voiceServiceManage
    }]);
    voiceController.controller("voicePackageController", ["$scope", "$filter", "$stateParams", "voiceCzConfig", "PanelService", "PopupService", "panel", "voiceFactory", "userProduct", "czButtons", "voiceFriendlyName", function ($scope, $filter, $stateParams, voiceCzConfig, PanelService, PopupService, panel, voiceFactory, userProduct, czButtons, voiceFriendlyName) {
        $scope.graphData = userProduct.data.data.graph_data;
        $scope.pii_status = false;
        $scope.pii_consent_status_panel = false;
        $scope.userPii = function () {
            $scope.pii_consent_status_panel = false;
            $scope.pii_status = true
        };
        $scope.buttons = czButtons.data;
        $scope.product = userProduct.data;
        $scope.voiceCzConfig = voiceCzConfig;
        var title = $scope.product.solution.solution.display_name;
        if ($scope.product.solution.friendlyname != $scope.product.solution.uid) {
            panel.setTitle($scope.product.solution.friendlyname);
            panel.setSubTitle($filter("formatCellNumber")($scope.product.solution.uid))
        } else {
            panel.setTitle(title);
            panel.setSubTitle($filter("formatCellNumber")($scope.product.solution.uid))
        }
        $scope.wallet_data = false;
        $scope.show_error = false;
        voiceFactory.getWalletBalances($scope.product.airtime.id).then(function (data) {
            if (!data.data) {
                $scope.show_error = true;
                $scope.wallet_loadding_error = true;
                return
            }
            $scope.wallet_data = true;
            $scope.balances = data.data;
            if (data.pii_consent_status && data.pii_consent_status == "Complete") {
                $scope.pii_consent_status_panel = false;
                $scope.show_error = false;
                $scope.pii_status = false
            } else {
                $scope.pii_status = true;
                $scope.pii_consent_status_panel = true;
                $scope.show_error = true
            }
        }, function () {
            $scope.show_error = true;
            $scope.wallet_data = false;
            $scope.wallet_loadding_error = true
        });
        $scope.getProdStatusClass = function (status) {
            if (status == "Active") {
                return "active"
            } else if (status == "Awaiting DMA") {
                return "trial"
            }
            return "inactive"
        };
        $scope.getProdStatusName = function (status) {
            if (status == "Awaiting DMA") {
                return "Waiting for activation"
            }
            return status
        };
        panel.onTitleClick = function (event) {
            var friendlyName = userProduct.data.solution.friendlyname;
            if (userProduct.data.solution.uid == userProduct.data.solution.friendlyname) friendlyName = "";
            voiceFriendlyName.doPopUp(userProduct.data.solution.id, friendlyName)
        }
    }]);
    voiceController.controller("voicePackageBundlesController", ["$scope", "$stateParams", "$filter", "voiceCzConfig", "PanelService", "panel", "PopupService", "topupOptionsAirtime", "topupOptionsSms", "topupOptionData", "voiceProductBundle", "voiceProductBundleAction", function ($scope, $stateParams, $filter, voiceCzConfig, PanelService, panel, PopupService, topupOptionsAirtime, topupOptionsSms, topupOptionData, voiceProductBundle, voiceProductBundleAction) {
        if (topupOptionsAirtime.client_airtime_solution.uid) {
            panel.setSubTitle($filter("formatCellNumber")(topupOptionsAirtime.client_airtime_solution.uid))
        }
        $scope.bundle = {
            airtime: new voiceProductBundle,
            sms: new voiceProductBundle,
            data: new voiceProductBundle
        };
        $scope.bundle.airtime.setData(topupOptionsAirtime.data, topupOptionsAirtime.client_airtime_solution.payment_detail);
        $scope.bundle.data.setData(topupOptionData.data.topup_items, topupOptionData.data.payment_detail);
        $scope.bundle.sms.setData(topupOptionsSms.sms_details, topupOptionData.data.payment_detail);
        $scope.doPayment = function (type, data) {
            if (data.hasErrors()) {
                return false
            }
            if (!data.checked) {
                return false
            }
            $scope.tempTopupData = {
                data: data,
                type: type
            };
            var bundleAction = new voiceProductBundleAction;
            switch (type) {
                case "airtime":
                    var options = {
                        uid: topupOptionsAirtime.client_airtime_solution.uid,
                        productBundle: $scope.bundle.airtime,
                        selectedBundle: _.find($scope.bundle.airtime.productData, {
                            id: parseInt($scope.bundle.airtime.productId)
                        }),
                        topUpData: {
                            paymentId: $scope.bundle.airtime.paymentId,
                            topUpId: _.result(_.find($scope.bundle.airtime.productData, {
                                id: parseInt($scope.bundle.airtime.productId)
                            }), "id"),
                            actionId: topupOptionsAirtime.client_airtime_solution.id,
                            mixer: topupOptionsAirtime.mixer
                        },
                        purchase: {
                            title: "Purchase Confirmation",
                            templateUrl: voiceCzConfig.pathTemplates + "/popups/topupAirtime.html"
                        },
                        success: {
                            title: "Airtime Bundle Purchased",
                            templateUrl: voiceCzConfig.pathTemplates + "/popups/topupAirtimePurchase.html"
                        }
                    };
                    bundleAction.handle($scope, $stateParams.csid, options, panel, type);
                    break;
                case "data":
                    var options = {
                        uid: topupOptionsAirtime.client_airtime_solution.uid,
                        productBundle: $scope.bundle.data,
                        selectedBundle: _.find($scope.bundle.data.productData, {
                            id: parseInt($scope.bundle.data.productId)
                        }),
                        topUpData: {
                            paymentId: $scope.bundle.data.paymentId,
                            topUpId: $scope.bundle.data.productId,
                            actionId: topupOptionData.data.client_conn_product.username,
                            mixer: topupOptionData.data.mixer,
                            identifier: topupOptionData.data.client_conn_product.id
                        },
                        purchase: {
                            title: "Purchase Confirmation",
                            templateUrl: voiceCzConfig.pathTemplates + "/popups/topupData.html"
                        },
                        success: {
                            title: "Data Purchased",
                            templateUrl: voiceCzConfig.pathTemplates + "/popups/topupDataPurchase.html"
                        }
                    };
                    bundleAction.handle($scope, $stateParams.csid, options, panel, type);
                    break;
                case "sms":
                    var options = {
                        uid: topupOptionsAirtime.client_airtime_solution.uid,
                        productBundle: $scope.bundle.sms,
                        selectedBundle: _.find($scope.bundle.sms.productData, {
                            name: $scope.bundle.sms.productId
                        }),
                        topUpData: {
                            paymentId: $scope.bundle.sms.paymentId,
                            topUpId: _.result(_.find($scope.bundle.sms.productData, {
                                name: $scope.bundle.sms.productId
                            }), "name"),
                            actionId: $stateParams.csid
                        },
                        purchase: {
                            title: "Purchase Confirmation",
                            templateUrl: voiceCzConfig.pathTemplates + "/popups/topupSms.html"
                        },
                        success: {
                            title: "SMS Bundle Purchased",
                            templateUrl: voiceCzConfig.pathTemplates + "/popups/topupSmsPurchase.html"
                        }
                    };
                    bundleAction.handle($scope, $stateParams.csid, options, panel, type);
                    break
            }
        }
    }]);
    voiceController.controller("voicePackageAutoController", ["$scope", "$filter", "voiceCzConfig", "PanelService", "panel", "userProduct", "topUpDetails", "voiceSettings", "voiceDataAutoLimit", "voiceAirtimeAutoLimit", function ($scope, $filter, voiceCzConfig, PanelService, panel, userProduct, topUpDetails, voiceSettings, voiceDataAutoLimit, voiceAirtimeAutoLimit) {
        panel.setSubTitle($filter("formatCellNumber")(userProduct.data.solution.uid));
        voiceDataAutoLimit.init(userProduct.data, topUpDetails.data);
        $scope.autoLimits = voiceDataAutoLimit;
        $scope.slider = {
            predefined: [50, 100, 250, 500],
            options: {
                orientation: "horizontal",
                min: 10,
                max: 2e3,
                range: "max",
                change: voiceDataAutoLimit.calculateData,
                slide: voiceDataAutoLimit.calculateData
            },
            setAmount: function (amount) {
                voiceDataAutoLimit.bundle.outBundle.amount = amount
            }
        };
        voiceAirtimeAutoLimit.init(userProduct.data, voiceSettings);
        $scope.airtimeLimits = voiceAirtimeAutoLimit
    }]);
    voiceController.controller("voicePackageNotifications", ["$scope", "$filter", "$stateParams", "notifications", "voiceFactory", "PopupService", "userProduct", "panel", function ($scope, $filter, $stateParams, notifications, voiceFactory, PopupService, userProduct, panel) {
        panel.setSubTitle($filter("formatCellNumber")(userProduct.data.solution.uid));
        $scope.notifications = notifications.data;
        $scope.update = function (obj) {
            voiceFactory.updateNotifications($stateParams.csid, $scope.notifications).then(function (result) {
                if (result.data.data.hasOwnProperty("message")) {
                    PopupService.open({
                        title: "SMS Notifications",
                        template: "<h1>" + result.data.data.message + "</h1>",
                        type: "warning",
                        scope: $scope
                    })
                }
                return false
            })
        }
    }]);
    voiceController.controller("voiceDataUsageController", ["$scope", "$filter", "voiceCzConfig", "PanelService", "panel", "userProduct", "czButtons", function ($scope, $filter, voiceCzConfig, PanelService, panel, userProduct, czButtons) {
        panel.setSubTitle($filter("formatCellNumber")(userProduct.data.solution.uid));
        $scope.buttons = czButtons.data;
        $scope.product = userProduct.data;
        if ($scope.product.solution.uid != $scope.product.solution.friendlyname) {
            panel.setSubTitle($filter("formatCellNumber")($scope.product.solution.uid) + " | " + $scope.product.solution.friendlyname)
        } else {
            panel.setSubTitle($filter("formatCellNumber")($scope.product.solution.uid))
        }
    }]);
    voiceController.controller("voicePackageEditController", ["$scope", "$filter", "voiceCzConfig", "PanelService", "panel", "voiceChangePackage", "voiceCancelService", "voicePackages", "userProduct", function ($scope, $filter, voiceCzConfig, PanelService, panel, voiceChangePackage, voiceCancelService, voicePackages, userProduct) {
        panel.setSubTitle($filter("formatCellNumber")(userProduct.data.solution.uid));
        $scope.path = {
            mobile_logo: voiceCzConfig.pathImages + "/elements/hero-voice.png"
        };
        voiceChangePackage.userProduct = userProduct.data;
        voiceChangePackage.availablePackages = voicePackages.change_options;
        voiceChangePackage.init();
        $scope.change = voiceChangePackage
    }])
})();
(function () {
    var voiceFactory = angular.module("voice.factory", []);
    voiceFactory.factory("voiceFactory", ["$http", function ($http) {
        var self = {};
        self.getUserProducts = function () {
            return $http.get("/en/api/voice").then(function (res) {
                if (res.data.data.length) return res.data.data;
                return null
            })
        };
        self.getUserProduct = function (pid) {
            return $http.get("/en/api/voice/" + pid + "/composite").then(function (res) {
                return res.data
            })
        };
        self.getCZButtons = function (pid) {
            return $http.get("/en/api/voice/cz_buttons/" + pid + "").then(function (res) {
                return res.data
            })
        };
        self.getUserConnUsage = function (pid) {
            return $http.get("/en/api/voice/" + pid + "/composite").then(function (res) {
                return res.data
            })
        };
        self.updateNotifications = function (pid, notificationSettings) {
            return $http.post("/en/api/voice/notifications/do/" + pid, {
                settings: notificationSettings
            }).then(function (res) {
                return res
            })
        };
        self.getNotifications = function (pid) {
            return $http.get("/en/api/voice/notifications/get/" + pid).then(function (res) {
                return res.data
            })
        };
        self.getAllBalances = function (pid) {
            return $http.get("/en/api/voice/all/balances/" + pid).then(function (res) {
                return res.data
            })
        };
        self.getWalletBalances = function (csvid) {
            return $http.get("/en/api/voice/balances/wallet/" + csvid).then(function (res) {
                return res.data
            })
        };
        self.getTopupAirtime = function (pid) {
            return $http.get("/en/api/voice/topup/options/airtime/composite/" + pid).then(function (res) {
                return res.data
            })
        };
        self.getTopupSms = function (pid) {
            return $http.get("/en/api/voice/topup/options/sms/" + pid).then(function (res) {
                return res.data
            })
        };
        self.getTopupData = function (pid) {
            return $http.get("/en/api/voice/topup/options/data/" + pid).then(function (res) {
                return res.data
            })
        };
        self.setMsisdnNumber = function (pid, number) {
            return $http.post("/en/api/voice/provision/" + pid, number).then(function (res) {
                return res
            })
        };
        self.setMemorableName = function (cscid, action, name) {
            var data = {
                custom_name_select: action,
                memorable_name: name
            };
            return $http.post("/en/api/voice/update/memorable-name/" + cscid, data).then(function (res) {
                return res
            })
        };
        self.doTopup = {};
        self.doTopup.action = function (type, id, settings) {
            switch (type) {
                case "airtime":
                    var params = {
                        airtime_product_id: settings.actionId,
                        topup_id: settings.id,
                        paymentDetailId: settings.paymentId,
                        mixer: settings.mixer
                    };
                    return self.doTopup.airtime(id, params);
                    break;
                case "sms":
                    var params = {
                        sms_id: settings.id,
                        payment_id: settings.paymentId
                    };
                    return self.doTopup.sms(id, params);
                    break;
                case "data":
                    var params = {
                        topup_item: settings.id,
                        pdid: settings.paymentId,
                        client_conn_product: settings.identifier,
                        mixer: settings.mixer
                    };
                    var username = settings.actionId;
                    return self.doTopup.data(username, params);
                    break
            }
        };
        self.doTopup.airtime = function (aid, settings) {
            return $http.post("/en/api/voice/topup/do/airtime/" + aid, settings).then(function (res) {
                return res.data
            })
        };
        self.doTopup.sms = function (aid, settings) {
            return $http.post("/en/api/voice/topup/do/sms/" + aid, settings).then(function (res) {
                return res.data
            })
        };
        self.doTopup.data = function (username, settings) {
            return $http.post("/en/api/connectivity/topup/" + username + "~2", settings).then(function (res) {
                return res.data
            })
        };
        self.autoLimits = {};
        self.autoLimits.outOfBundle = function (ccpid, amount, action) {
            var params = {
                amount: parseInt(amount),
                action: action
            };
            return $http.get("/en/api/my-connectivity/change_account-detail_oob/mobile/" + ccpid, {
                params: params
            }).then(function (res) {
                return res
            })
        };
        self.autoLimits.autoBundle = function (account, params) {
            return $http.post("/en/api/my-connectivity/update-auto-topup/" + account + "~2", params).then(function (res) {
                return res
            })
        };
        self.autoVoice = {};
        self.autoVoice.getVoiceSettings = function (cscid) {
            return $http.get("/en/api/voice/airtime/auto-limit/settings/" + cscid).then(function (res) {
                return res.data.data
            })
        };
        self.autoVoice.updateSettings = function () { };
        self.getVoicePackages = function (cscid) {
            return $http.get("/en/api/voice/get/packages/" + cscid).then(function (res) {
                return res.data.data
            })
        };
        self.changeSolution = function (cscid, newSolutionId) {
            var params = {
                solution_id: newSolutionId
            };
            return $http.post("/en/api/voice/post/packages/" + cscid, params).then(function (res) {
                return res.data.data
            })
        };
        self.getVoiceIssueData = function (pid) {
            return $http.get("/en/api/voice/issue/" + pid).then(function (res) {
                return res.data
            })
        };
        self.submitVoiceIssue = function (pid, serviceId, issueDate, details) {
            var params = {
                service_id: serviceId,
                issue_date: issueDate,
                details: details
            };
            return $http.post("/en/api/voice/issue/submit/" + pid, params).then(function (res) {
                return res.data
            })
        };
        return self
    }])
})();
(function () {
    "use strict";
    var voiceProductBundle = angular.module("voice.productBundle", []);
    voiceProductBundle.service("voiceProductBundle", function () {
        return function () {
            this.resetErrors = function () {
                this.errors = {
                    product: false,
                    payment: false,
                    checked: false
                }
            };
            this.hasErrors = function () {
                this.resetErrors();
                if (this.productId == "") {
                    this.errors.product = true
                }
                if (this.paymentId == "") {
                    this.errors.payment = true
                }
                if (this.checked == false) {
                    this.errors.checked = true
                }
                return !!(this.errors.product || this.errors.payment)
            };
            this.setData = function (productData, paymentData) {
                this.productData = productData;
                this.paymentData = paymentData
            };
            this.updateDisclaimer = function (type) {
                this.errors.product = false;
                if (type == "sms") {
                    var disclaimer = _.result(_.find(this.productData, {
                        name: this.productId
                    }), "disclaimer")
                }
                if (disclaimer == null) {
                    disclaimer = this.disclaimerDefault
                }
                this.disclaimer = disclaimer
            };
            this.productId = "";
            this.paymentId = "";
            this.checked = false;
            this.disclaimer = "";
            this.paymentData = null;
            this.productData = null;
            this.disclaimerDefault = "I understand that I will be charged immediately for this bundle and that this bundle will expire on the last day of next month.";
            this.disclaimer = this.disclaimerDefault;
            this.resetErrors()
        }
    })
})();
(function () {
    "use strict";
    var voiceServiceManage = angular.module("voice.serviceManage", []);
    voiceServiceManage.service("voiceServiceManage", ["$stateParams", "$state", "voiceFactory", "PopupService", "voiceCzConfig", function ($stateParams, $state, voiceFactory, PopupService, voiceCzConfig) {
        return function () {
            this.number = "";
            this.numberValid = false;
            this.cancelModal = function () {
                PopupService.open({
                    title: "Cancel Service",
                    templateUrl: voiceCzConfig.pathTemplates + "/popups/activateCancel.html",
                    controller: ["$scope", "voiceFactory", "popup", "PopupService", function ($scope, voiceFactory, popup, PopupService) { }]
                })
            };
            this.isValid = function () {
                this.numberValid = this.number ? true : false
            };
            this.activate = function () {
                if (this.number) {
                    voiceFactory.setMsisdnNumber($stateParams.csid, {
                        number: this.number
                    }).then(function (data) {
                        $state.transitionTo("app.voice.package", {
                            csid: $stateParams.csid
                        }, {
                            reload: true,
                            inherit: true,
                            notify: true
                        })
                    }, function (err) {
                        PopupService.open({
                            title: "An Error Occurred",
                            templateUrl: voiceCzConfig.pathRootTemplates + "/popups/error.html",
                            type: "warning",
                            controller: ["$scope", function ($scope) {
                                $scope.status_text = err.statusText;
                                $scope.status_code = err.statusCode
                            }]
                        })
                    })
                }
            }
        }
    }])
})();
(function () {
    "use strict";
    var voiceProductBundleAction = angular.module("voice.productBundleAction", []);
    voiceProductBundleAction.service("voiceProductBundleAction", ["voiceFactory", "voiceProductBundle", "voiceCzConfig", "PopupService", function (voiceFactory, voiceProductBundle, voiceCzConfig, PopupService) {
        return function () {
            this.options = {
                uid: null,
                productBundle: new voiceProductBundle,
                selectedBundle: null,
                topUpData: {
                    paymentId: null,
                    topUpId: null,
                    actionId: null
                },
                purchase: {
                    title: "Purchase Confirmation",
                    templateUrl: ""
                },
                success: {
                    title: "Bundle Purchased",
                    templateUrl: ""
                }
            };
            this.handle = function ($scope, csid, options, panel, type) {
                PopupService.open({
                    title: options.purchase.title,
                    templateUrl: options.purchase.templateUrl,
                    scope: $scope,
                    controller: ["$scope", "voiceFactory", "popup", "PopupService", function ($scope, voiceFactory, popup, PopupService) {
                        $scope.name = options.selectedBundle.description;
                        $scope.price = options.selectedBundle.price;
                        $scope.disclaimer = options.productBundle.disclaimer;
                        $scope.number = options.uid;
                        $scope.doPayRequest = function () {
                            popup.close();
                            var topUpSettings = {
                                id: options.topUpData.topUpId,
                                actionId: options.topUpData.actionId,
                                paymentId: options.topUpData.paymentId,
                                mixer: options.topUpData.mixer || null,
                                identifier: options.topUpData.identifier
                            };
                            voiceFactory.doTopup.action(type, topUpSettings.actionId, topUpSettings).then(function () {
                                panel.close();
                                PopupService.open({
                                    title: options.success.title,
                                    templateUrl: options.success.templateUrl,
                                    type: "success",
                                    controller: ["$scope", function ($scope) {
                                        $scope.name = options.selectedBundle.description
                                    }]
                                })
                            }, function (err) {
                                PopupService.open({
                                    title: "An Error Occurred",
                                    templateUrl: voiceCzConfig.pathRootTemplates + "/popups/error.html",
                                    type: "warning",
                                    controller: ["$scope", function ($scope) {
                                        $scope.status_text = err.data.code;
                                        $scope.status_code = err.data.message
                                    }]
                                })
                            })
                        }
                    }]
                })
            }
        }
    }])
})();
(function () {
    "use strict";
    var voiceFriendlyName = angular.module("voice.friendlyName", []);
    voiceFriendlyName.factory("voiceFriendlyName", ["$state", "$stateParams", "voiceFactory", "voiceCzConfig", "PopupService", function ($state, $stateParams, voiceFactory, voiceCzConfig, PopupService) {
        var self = {};
        self.doPopUp = function (cscid, currentFriendlyName) {
            currentFriendlyName = currentFriendlyName || "";
            PopupService.open({
                title: "Customise your Product Title",
                templateUrl: voiceCzConfig.pathTemplates + "/popups/friendlyName.html",
                controller: ["$scope", "popup", function ($scope, popup) {
                    $scope.error = {
                        select: false,
                        name: false
                    };
                    $scope.value = {
                        friendly: "",
                        action: ""
                    };
                    if (currentFriendlyName != "") {
                        $scope.value.action = "custom";
                        $scope.value.friendly = currentFriendlyName
                    }
                    $scope.updateName = function () {
                        $scope.error.name = false;
                        $scope.error.select = false;
                        if ($scope.value.action == "") {
                            $scope.error.select = true;
                            return
                        }
                        var updateAction = 0;
                        if ($scope.value.action == "custom") {
                            updateAction = 1;
                            console.log($scope.value.friendly);
                            if ($scope.value.friendly == "") {
                                $scope.error.name = true;
                                return
                            }
                        }
                        voiceFactory.setMemorableName(cscid, updateAction, $scope.value.friendly).then(function () {
                            popup.close();
                            PopupService.open({
                                title: "Customise Title Success",
                                templateUrl: voiceCzConfig.pathTemplates + "/popups/friendlyNameSuccess.html",
                                type: "success"
                            })
                        })
                    }
                }]
            })
        };
        return self
    }])
})();
(function () {
    "use strict";
    angular.module("voice.autoLimit", []).factory("voiceDataAutoLimit", ["$stateParams", "voiceFactory", "voiceCzConfig", "PopupService", function ($stateParams, voiceFactory, voiceCzConfig, PopupService) {
        var self = {};
        self.setDefaults = function () {
            self.topupData = null;
            self.userProduct = null;
            self.view = {
                section: "in_bundle",
                autoChecked: false,
                error: {
                    auto_increment: false,
                    auto_monthly: false,
                    auto_disclaimer: false
                }
            };
            self.bundle = {
                inBundle: false,
                outBundle: {
                    enabled: false,
                    amount: 500,
                    action: null,
                    data: 0
                },
                autoLimit: {
                    increment: "",
                    monthly: "",
                    max: 1
                }
            };
            self.autoLimitMonthly = []
        };
        self.init = function (userProduct, topupData) {
            self.setDefaults();
            self.userProduct = userProduct;
            self.topupData = topupData;
            self.bundle.autoLimit.max = self.topupData.auto_topup_max;
            if (!self.topupData.oob.enabled && !self.topupData.auto_topup_on) {
                self.bundle.inBundle = true
            } else if (self.topupData.oob.enabled) {
                self.view.section = "out_bundle";
                self.bundle.outBundle = {
                    enabled: true,
                    amount: self.topupData.oob.rands,
                    action: "update",
                    data: self.topupData.oob.bytes
                }
            } else if (self.topupData.auto_topup_on) {
                self.view.section = "auto";
                self.bundle.autoLimit = {
                    increment: _.find(self.topupData.auto_topup_items, {
                        id: self.topupData.auto_package_id
                    }),
                    monthly: parseFloat(self.userProduct.data.usage.auto_topup_max),
                    max: self.topupData.auto_topup_max
                };
                self.topupData.auto_topup_items = _.select(self.topupData.auto_topup_items, function (chr) {
                    return chr.bandwidth_limit <= userProduct.data.solution.solution.bandwidth_limit_bytes
                });
                self.autoLimitMonthly = self.topupData.auto_selection_map[self.bundle.autoLimit.increment.id]
            }
        };
        self.reload = function () {
            voiceFactory.getTopupData($stateParams.csid).then(function (top) {
                voiceFactory.getUserProduct($stateParams.csid).then(function (prod) {
                    self.init(prod.data, top.data)
                })
            })
        };
        self.enableInBundle = function () {
            if (self.topupData.oob.enabled) {
                self.disableOutOfBundle().then(self.enableInBundleSuccess)
            } else if (self.topupData.auto_topup_on) {
                self.autoLimitDisable().then(self.enableInBundleSuccess)
            } else {
                self.enableInBundleSuccess()
            }
        };
        self.enableInBundleSuccess = function (res) {
            PopupService.open({
                title: "In Bundle Enabled",
                type: "success",
                templateUrl: voiceCzConfig.pathTemplates + "/popups/autoLimitInBundleEnabled.ng.html",
                scope: "=",
                controller: ["$scope", function ($scope) {
                    $scope.number = self.userProduct.solution.uid
                }]
            }).then(self.reload)
        };
        self.updateOutOfBundle = function () {
            self.bundle.outBundle.action = "update";
            var ccpid = self.userProduct.data.ccpid;
            var amount = self.bundle.outBundle.amount;
            var action = self.bundle.outBundle.action;
            var number = self.userProduct.solution.uid;
            PopupService.open({
                title: "Mobile Data Out-of-Bundle",
                templateUrl: voiceCzConfig.pathTemplates + "/popups/autoLimitOutOfBundleConfirm.html",
                scope: "=",
                controller: ["$scope", "popup", function ($scope, popup) {
                    $scope.option = {
                        number: number,
                        amount: amount
                    };
                    $scope.onClick = function () {
                        popup.close();
                        voiceFactory.autoLimits.outOfBundle(ccpid, amount, action).then(function (res) {
                            PopupService.open({
                                title: "Mobile Data Out-of-Bundle",
                                type: "success",
                                templateUrl: voiceCzConfig.pathTemplates + "/popups/autoLimitOutOfBundleSuccess.ng.html",
                                scope: "=",
                                controller: ["$scope", "popup", function ($scope, popup) {
                                    $scope.number = number;
                                    $scope.amount = amount
                                }]
                            });
                            self.reload()
                        })
                    }
                }]
            })
        };
        self.disableOutOfBundle = function () {
            var ccpid = self.userProduct.data.ccpid;
            var amount = self.bundle.outBundle.amount;
            return voiceFactory.autoLimits.outOfBundle(ccpid, amount, "deactivate")
        };
        self.calculateData = function () {
            self.bundle.outBundle.data = self.bundle.outBundle.amount / .1 * (1024 * 1024)
        };
        self.updateAutoLimit = function () {
            var error = false;
            if (!self.view.autoChecked) {
                error = true;
                self.view.error.auto_disclaimer = true
            }
            if (self.bundle.autoLimit.monthly == "") {
                error = true;
                self.view.error.auto_monthly = true
            }
            if (self.bundle.autoLimit.increment == "" || !self.bundle.autoLimit.increment.id) {
                error = true;
                self.view.error.auto_increment = true
            }
            if (error) {
                return
            }
            var params = {
                client_conn_product: self.userProduct.data.ccpid,
                mixer: self.topupData.mixer,
                max_autotop: self.bundle.autoLimit.monthly,
                enable_auto_topups: true,
                auto_items_packages: self.bundle.autoLimit.increment.id
            };
            var username = self.topupData.client_conn_product.username;
            if (self.topupData.oob.enabled) {
                self.disableOutOfBundle().then(function () {
                    voiceFactory.autoLimits.autoBundle(username, params).then(self.autoLimitSuccessPopup);
                    self.reload()
                })
            } else {
                voiceFactory.autoLimits.autoBundle(username, params).then(self.autoLimitSuccessPopup);
                self.reload()
            }
        };
        self.autoLimitSuccessPopup = function () {
            var amount = self.bundle.outBundle.amount;
            var number = self.userProduct.solution.uid;
            PopupService.open({
                title: "Mobile Data Auto-Limit",
                type: "success",
                templateUrl: voiceCzConfig.pathTemplates + "/popups/autoLimitOutOfBundleSuccess.ng.html",
                scope: "=",
                controller: ["$scope", "popup", function ($scope, popup) {
                    $scope.number = number;
                    $scope.amount = amount
                }]
            })
        };
        self.autoLimitMonthlyData = function () {
            self.autoLimitMonthly = self.topupData.auto_selection_map[self.bundle.autoLimit.increment.id]
        };
        self.autoLimitDisable = function () {
            var username = self.topupData.client_conn_product.username;
            var params = {
                client_conn_product: self.userProduct.data.ccpid,
                mixer: self.topupData.mixer,
                max_autotop: self.bundle.autoLimit.monthly,
                enable_auto_topups: false,
                auto_items_packages: self.bundle.autoLimit.increment
            };
            return voiceFactory.autoLimits.autoBundle(username, params)
        };
        return self
    }]).factory("voiceAirtimeAutoLimit", ["voiceFactory", "voiceCzConfig", "PopupService", function (voiceFactory, voiceCzConfig, PopupService) {
        var self = {};
        self.setDefaults = function () {
            self.topupSettings = null;
            self.userProduct = null;
            self.view = {
                selected: "in_bundle",
                disclaimer_checked: false,
                error: {
                    increment: false,
                    monthly: false,
                    disclaimer: false
                }
            };
            self.airtime = {
                monthly: 0,
                increment: 0
            }
        };
        self.init = function (userProduct, topupSettings) {
            self.setDefaults();
            self.topupSettings = topupSettings;
            self.userProduct = userProduct
        };
        self.updateAutoLimit = function () {
            var error = false;
            if (!self.view.disclaimer_checked) {
                error = true;
                self.view.error.disclaimer = true
            }
            if (self.airtime.monthly == 0) {
                error = true;
                self.view.error.monthly = true
            }
            if (self.airtime.increment == 0) {
                error = true;
                self.view.error.increment = true
            }
            if (error) {
                return
            }
        };
        return self
    }])
})();
(function () {
    "use strict";
    angular.module("voice.cancelService", []).factory("voiceCancelService", ["voiceFactory", "voiceCzConfig", "PopupService", function (voiceFactory, voiceCzConfig, PopupService) {
        var self = {};
        return self
    }])
})();
(function () {
    "use strict";
    angular.module("voice.changePackage", []).factory("voiceChangePackage", ["$filter", "voiceFactory", "voiceCzConfig", "PopupService", function ($filter, voiceFactory, voiceCzConfig, PopupService) {
        var solutionTypeIds = {
            airtime: 900,
            data: 220,
            sms: 910
        };
        var self = {};
        self.view = {
            checked: false,
            error: false,
            noProduct: false
        };
        self.solution = 0;
        self.availablePackages = null;
        self.userProduct = null;
        self.disclaimer = null;
        self.currentPackage = {};
        self.newPackage = {};
        var dateNextMonth = function () {
            var CurrentDate = new Date;
            CurrentDate.setMonth(CurrentDate.getMonth() + 1);
            return CurrentDate
        };
        var buildDisclaimer = function () {
            var nextMonth = dateNextMonth();
            var upgradeStatus = self.getUpgradeStatus(self.currentPackage, self.newPackage);
            return "I understand that this " + upgradeStatus + " will only be effective from the 1st of " + $filter("date")(nextMonth, "MMMM") + " " + $filter("date")(nextMonth, "yyyy")
        };
        var confirmPopup = function () {
            var nextMonth = dateNextMonth();
            var upgradeStatus = self.getUpgradeStatus(self.currentPackage, self.newPackage);
            PopupService.open({
                title: upgradeStatus + " Package",
                templateUrl: voiceCzConfig.pathTemplates + "/popups/solutionChange.html",
                scope: "=",
                controller: ["$scope", "$filter", "voiceFactory", "popup", "PopupService", function ($scope, $filter, popup, PopupService) {
                    $scope.upgradeStatus = upgradeStatus;
                    $scope.date = $filter("date")(nextMonth, "1 MMMM yyyy");
                    $scope.package = self.newPackage;
                    $scope.number = $filter("formatCellNumber")(self.userProduct.airtime.uid);
                    $scope.doSolutionChange = self.packageUpgrade
                }]
            })
        };
        var successPopup = function () {
            var nextMonth = dateNextMonth();
            var upgradeStatus = self.getUpgradeStatus(self.currentPackage, self.newPackage);
            PopupService.open({
                title: upgradeStatus + " Package",
                templateUrl: voiceCzConfig.pathTemplates + "/popups/solutionChangeSuccess.html",
                scope: "=",
                controller: ["$scope", "$filter", "voiceFactory", "popup", "PopupService", function ($scope, $filter, popup, PopupService) {
                    $scope.upgradeStatus = upgradeStatus;
                    $scope.date = $filter("date")(nextMonth, "1 MMMM yyyy");
                    $scope.package = self.newPackage;
                    $scope.number = $filter("formatCellNumber")(self.userProduct.airtime.uid)
                }]
            })
        };
        self.init = function () {
            self.disclaimer = buildDisclaimer();
            self.currentPackage = self.getCurrentPackageData();
            self.newPackage = self.getCurrentPackageData()
        };
        self.showSelectedSolution = function () {
            if (self.solution != 0) {
                var foundYou = _.find(self.availablePackages, {
                    solution: {
                        id: parseInt(self.solution)
                    }
                });
                self.newPackage = self.buildNewPackageData(foundYou)
            } else {
                self.newPackage = self.getCurrentPackageData()
            }
            self.disclaimer = buildDisclaimer()
        };
        self.changePackage = function () {
            var error = false;
            if (self.solution == 0) {
                error = true;
                self.view.noProduct = true
            }
            if (!self.view.checked) {
                error = true;
                self.view.error = true
            }
            if (error) {
                return
            }
            confirmPopup()
        };
        self.buildNewPackageData = function (vSolution) {
            var xPackage = {};
            var airtime = _.result(_.find(vSolution.client_solution_composite_child_change_data, {
                solution: {
                    solution_type_id: parseInt(solutionTypeIds.airtime)
                }
            }), "solution").display_name;
            var data = _.result(_.find(vSolution.client_solution_composite_child_change_data, {
                solution: {
                    solution_type_id: parseInt(solutionTypeIds.data)
                }
            }), "solution").name;
            xPackage.solutionId = parseInt(self.solution);
            xPackage.name = vSolution.solution.display_name;
            xPackage.price = vSolution.price;
            xPackage.airtime = airtime || "N/A";
            xPackage.data = data || "N/A";
            return xPackage
        };
        self.getCurrentPackageData = function () {
            var xPackage = {};
            xPackage.name = self.userProduct.solution.solution.display_name;
            xPackage.price = self.userProduct.solution.solution.price;
            xPackage.airtime = self.userProduct.airtime.solution.display_name;
            xPackage.data = self.userProduct.data.solution.solution.name;
            return xPackage
        };
        self.getUpgradeStatus = function (currentPackage, newPackage) {
            if (parseFloat(currentPackage.price) < parseFloat(newPackage.price)) return "Upgrade";
            return "Downgrade"
        };
        self.packageUpgrade = function () {
            console.log(self.newPackage);
            console.log(self.userProduct);
            voiceFactory.changeSolution(self.userProduct.solution.id, self.newPackage.solutionId).then(function (res) {
                console.log(res);
                successPopup();
                panel.close()
            })
        };
        return self
    }])
})();
(function () {
    "use strict";
    var connectivityDirective = angular.module("connectivity.directives", []);
    connectivityDirective.directive("czConnDataLinks", function () {
        return {
            templateUrl: "../bundles/ahuiCash Analyzer/js/app/modules/connectivity/directives/templates/cz_conn_data_link.html",
            replace: true,
            link: function (scope, element, attrs) {
                scope.links = eval(attrs.links)
            }
        }
    })
})();