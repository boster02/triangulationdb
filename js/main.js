var curstep = 0;

function message_pulse() {
    if ($("#message_notifier.active span").css("opacity") == 0) {
        $("#message_notifier.active span").fadeTo(1e3, 1)
    }
    for (var i = 0; i <= 200; i++) {
        $("#message_notifier.active span").delay(1e3).fadeToggle(1e3)
    }
}

function adjust_home() {
    message_pulse()
}

function is_touch() {
    if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i)) return true
}
$(function () {
    $(window).resize(function () {
        adjust_home()
    })
});

function adjust_listholder() {
    $(".listholder.secondary").each(function () {
        cur_h = parseInt($(this).css("max-height"));
        prev_element_h = parseInt($(this).prev().outerHeight()) + parseInt($(this).prev().css("margin-bottom"));
        if (prev_element_h == 0) {
            prev_element_h = 80
        }
        new_h = cur_h - prev_element_h;
        $(this).css("height", new_h)
    });
    $(".listholder.primary").each(function () {
        cur_h = parseInt($(this).css("max-height"));
        next_element_h = parseInt($(this).next().outerHeight()) + parseInt($(this).next().css("margin-top"));
        new_h = cur_h - next_element_h;
        if (new_h > cur_h) new_h = cur_h;
        $(this).css("height", new_h)
    })
}
$(document).ready(function () {
    $(window).resize(function () {
        adjust_listholder()
    });
    adjust_home();
    $("body,form, this").keyup(function (e) {
        if (e.keyCode == 13) {
            return false
        }
        var popwindow = $("body").find(".popwindow:first");
        if (e.keyCode == 27 && !popwindow.length >= 1) {
            var currentPanel = $("#panels .pathwindow :last");
            closePanel()
        }
        if (e.keyCode == 27) {
            var popwindow = $("body").find(".popwindow:first");
            if (popwindow.length >= 1) {
                closePopup(popwindow.find(".btn_close").prop("data-popup-id"))
            }
        }
    });
    $(".fadein").append('<span class="hover"></span>').each(function () {
        $("> span.hover", this).css("opacity", 0);
        if (!is_touch()) {
            $("a").hover(function () {
                $(this).find(".fadein .hover").stop().fadeTo(300, 1)
            }, function () {
                $(this).find(".fadein .hover").stop().fadeTo(300, 0)
            })
        }
    });
    $(document).ajaxComplete(function (event, XMLHttpRequest, ajaxOption) {
        $("#loader").hide();
        var currentPanel = $("#panels .pathwindow :last");
        if (currentPanel.length > 0) {
            currentPanel.find(".pathwindow_blackout").remove()
        }
        hide_whiteout();
        $("#clearout").remove();
        if (XMLHttpRequest.status != 0) {
            url = window.location.hash.replace("#", "");
            var urlParts = url.split("/");
            urlParts.shift();
            url = urlParts.join("/");
            if (urlParts.length == 4) {
                urlParts.pop();
                url = urlParts.join("/")
            }
            //_gaq.push(["_trackPageview", url])
        }
        replace_default();
        textarea_auto_size()
    });
    $(document).off("click", "#clearout").on("click", "#clearout", function () { });
    $(this).ajaxSend(function (event, jqXHR, ajaxOptions) {
        var url = window.location.hash.replace("#", "");
        var urlParts = url.split("/");
        if (ajaxOptions.url == "" || ajaxOptions.url == "/" || urlParts[1] == "ah") {
            jqXHR.abort();
            return
        }
        if (ajaxOptions.url.indexOf("_wdt") > 0) {
            return
        }
        $("#blackout").fadeIn(500);
        $("#loader").show();
        if (!ajaxOptions.panel) {
            var currentPanel = $("#panels .pathwindow:last");
            show_whiteout(currentPanel)
        }
        $("body").append('<div id="clearout" style="height: 100%;	width: 100%; position: absolute; top: 0;left: 0; z-index: 20;"></div>')
    })
});
$(document).on("panelOpening", function (e) {
    var currentPanel = $("#panels .pathwindow:last .pathwindow_content:last");
    blackOutPannel(currentPanel)
});

function blackOutPannel(currentPanel) {
    if (currentPanel.length > 0 && currentPanel.find(".pathwindow_blackout").length === 0) {
        currentPanel.append('<div class="pathwindow_blackout active" style="display:none;"></div>');
        currentPanel.find(".pathwindow_blackout").fadeIn(250)
    }
}
$(document).on("popUpLoading", function (e) { });
$(document).on("popUpLoaded", function (e) {
    setTimeout(function () {
        var currentPanel = $("#panels .pathwindow:last");
        show_whiteout(currentPanel)
    }, 650)
});
$(document).on("popUpClosed", function (e) {
    if ($("#connectivty_edit_package").length > 0 && $("#connectivty_edit_package").offset().left > 0) {
        var username = $("#connectivty_edit_package").prop("data-username");
        var type = $("#connectivty_edit_package").prop("data-type");
        reload_edit_package(username, type)
    }
    if ($("#bundle_panel").length > 0 && $("#bundle_panel").offset().left > 0) {
        var number = $("#bundle_panel").attr("data-number");
        reload_bundles(number);
        reload_line_account(number)
    }
    if ($("#connectivity_services").length > 0 && $("#connectivity_services").offset().left > 0) {
        var username = $("#connectivity_services").prop("data-username");
        var type = $("#connectivity_services").prop("data-type");
        reload_service(username, type)
    }
    if ($("#connectivity_account_details").length > 0 && $("#connectivity_account_details").offset().left > 0) {
        var username = $("#connectivity_account_details").attr("data-username");
        var type = $("#connectivity_account_details").attr("data-type");
        reload_account_detail(username, type)
    }
    if ($("#my_cloud_panel").length > 0 && $("#my_cloud_panel").offset().left > 0) {
        var domain = $("#my_cloud_panel").attr("data-domain");
        reload_my_cloud(domain)
    }
    if ($("#visp_hosting_panel").length > 0 && $("#visp_hosting_panel").offset().left > 0) {
        var domain = $("#visp_hosting_panel").prop("data-domain");
        reload_my_visp(domain)
    }
});

function show_whiteout(currentPanel) {
    if (currentPanel.length > 0) {
        if (currentPanel.length > 0 && currentPanel.find(".pathwindow_whiteout:first").length === 0) {
            currentPanel.append('<div class="pathwindow_whiteout" style="display:none;"></div>');
            currentPanel.find(".pathwindow_whiteout").fadeIn(250)
        }
    }
}

function hide_whiteout() {
    if ($("body").find(".popwindow").length >= 1) {
        return false
    }
    $("body").find(".pathwindow:last .pathwindow_whiteout").delay(750).fadeOut(500, function () {
        $(this).remove()
    })
}

function form_fields_highlight_error(fields) {
    $.each(fields, function (index, field) {
        var holderLabel = field.parent();
        if (field.attr("type") == "checkbox") {
            holderLabel = holderLabel.parent()
        }
        holderLabel.removeClass("verified");
        holderLabel.addClass("animated");
        holderLabel.addClass("error")
    })
}

function form_fields_highlight_normal(fields) {
    $.each(fields, function (index, field) {
        form_field_clear_error($(field))
    })
}

function show_success_notice(form, message) {
    var notice_success = form.children("div.notice_success");
    notice_success.html(message);
    notice_success.fadeIn(500, function () {
        setTimeout(function () {
            notice_success.slideUp(500)
        }, 3e3)
    })
}

function show_error_notice(form, message) {
    var notice_error = form.children("div.notice_error");
    notice_error.html(message);
    notice_error.fadeIn(500, function () {
        setTimeout(function () {
            notice_error.slideUp(500)
        }, 3e3)
    })
}

function form_field_animate_error(field, message) {
    var holderLabel = field.parent();
    if (field.attr("type") == "checkbox") {
        holderLabel = holderLabel.parent()
    }
    form_fields_highlight_error([field]);
    holderLabel.addClass("animated");
    holderLabel.removeClass("shake").addClass("shake");
    window.setTimeout(function () {
        holderLabel.removeClass("shake")
    }, 1100);
    show_error_notice(holderLabel.parents("form"), message)
}

function clear_form_values(form) {
    form.find("input").val("")
}

function form_field_animate_success(field, message) {
    var holderLabel = field.parent();
    if (field.attr("type") == "checkbox") {
        holderLabel = holderLabel.parent()
    }
    holderLabel.removeClass("error");
    holderLabel.removeClass("animated");
    holderLabel.addClass("animated");
    holderLabel.addClass("verified");
    holderLabel.removeClass("pulse").addClass("pulse");
    window.setTimeout(function () {
        holderLabel.removeClass("pulse")
    }, 1100);
    window.setTimeout(function () {
        holderLabel.removeClass("verified")
    }, 4100);
    form_field_clear_error(field);
    show_success_notice(holderLabel.parents("form"), message)
}

function form_field_clear_error(field) {
    var holderLabel = field.parent();
    if (field.attr("type") == "checkbox") {
        holderLabel = holderLabel.parent()
    }
    holderLabel.removeClass("error")
}

function support_animate_tickets(fields, direction, callbackFunction) {
    $.each(fields, function (index, field) {
        $(field).css({
            bottom: direction == "up" ? 0 : $(field).height() * (index + 1),
            opacity: direction == "up" ? 1 : 0,
            position: "relative"
        });
        $(field).animate({
            bottom: direction == "up" ? $(field).height() * (index + 1) : 0,
            opacity: direction == "up" ? 0 : 1
        }, 750, callbackFunction)
    })
}

function textarea_auto_size() {
    $("textarea.expanding").each(function () {
        var txt = $(this),
            hiddenDiv = $(this).next("div.expanding"),
            content = null;
        txt.on("keyup", function () {
            content = $(this).val();
            content = content.replace(/\n/g, "<br>");
            hiddenDiv.html(content + '<br class="lbr">');
            $(this).css("height", parseInt(hiddenDiv.height()) + 15)
        })
    })
}

function replace_default() {
    $(".replacedefault").focus(function () {
        $(this).addClass("active");
        if (this.value == this.defaultValue) {
            this.value = ""
        }
        if (this.value != this.defaultValue) {
            this.select()
        }
    });
    $(".replacedefault").blur(function () {
        $(this).removeClass("active");
        if ($.trim(this.value) == "") {
            this.value = this.defaultValue ? this.defaultValue : ""
        }
    });
    $(".searchbox .replacedefault").focus(function () {
        $(this).parent().find(".btn_action").addClass("cross");
        if (this.value == this.defaultValue) {
            this.value = ""
        }
        if (this.value != this.defaultValue) {
            this.select()
        }
    });
    $(".searchbox .replacedefault").blur(function () {
        $(this).parent().find(".btn_action").removeClass("cross");
        if ($.trim(this.value) == "") {
            this.value = this.defaultValue ? this.defaultValue : ""
        }
    })
}

function openPanel(url, trigger, params, http_method) {
    var params = typeof params !== "undefined" ? params : {};
    var http_method = typeof http_method !== "undefined" ? http_method : "get";
    if (null != url && url !== "") {
        $.ajax({
            url: url,
            data: params,
            method: http_method,
            beforeSend: function (xhr, settings) {
                $("body").trigger("panelOpening");
                settings.panel = true
            },
            success: function (html, textStatus, jqXHR) {
                window.location.hash = url;
                $("#panels").append($(html).removeClass("active").css({
                    left: "150%"
                }));
                var pathwindows = $("#panels .pathwindow");
                var numberofwindows = pathwindows.length;
                setTimeout(function () {
                    pathwindows.each(function (i, mypanel) {
                        var jwindow = $(mypanel);
                        jwindow.removeClass("active");
                        if (numberofwindows > 2) {
                            jwindow.animate({
                                left: "-50%"
                            }, 500, function () {
                                $(this).addClass("active")
                            });
                            numberofwindows--;
                            return true
                        }
                        if (numberofwindows == 2) {
                            jwindow.animate({
                                left: "-25%"
                            }, 500, function () {
                                $(this).addClass("active")
                            });
                            numberofwindows--;
                            return true
                        }
                        if (numberofwindows == 1) {
                            jwindow.animate({
                                left: "50%"
                            }, 500, function () {
                                $(this).trigger("panelShown");
                                if (trigger) {
                                    $(this).trigger(trigger)
                                }
                                $(this).trigger("updatePath");
                                $(this).addClass("active")
                            });
                            numberofwindows--;
                            return true
                        }
                    })
                }, 250)
            }
        })
    }
}

function closePathWindows(pathwindows) {
    var numberofwindows = pathwindows.length;
    pathwindows.each(function (i, mypanel) {
        var jwindow = $(mypanel);
        jwindow.removeClass("active");
        if (numberofwindows > 3) {
            jwindow.animate({
                left: "-50%"
            }, 500, function () {
                $(this).addClass("active")
            });
            numberofwindows--;
            return true
        }
        if (numberofwindows == 3) {
            jwindow.animate({
                left: "-25%"
            }, 500, function () {
                $(this).addClass("active")
            });
            numberofwindows--;
            return true
        }
        if (numberofwindows == 2) {
            jwindow.animate({
                left: "50%"
            }, 500, function () {
                $(this).addClass("active");
                $(this).find(".pathwindow_blackout").fadeOut(250, function () {
                    $(this).remove()
                })
            });
            numberofwindows--;
            return true
        }
        if (numberofwindows == 1) {
            jwindow.animate({
                left: "150%"
            }, 500, function () {
                $(this).remove()
            });
            numberofwindows--;
            return true
        }
    })
}

function closePanelsAfter(panel) {
    var panelsToClose = panel.nextAll();
    var panelLeft = panel.prev();
    panel.removeClass("active");
    panel.animate({
        left: "50%"
    }, 500, function () {
        $(this).addClass("active");
        $(this).find(".pathwindow_blackout").fadeOut(250, function () {
            $(this).remove()
        })
    });
    panelLeft.each(function (i, mypanel) {
        var jwindow = $(mypanel);
        jwindow.removeClass("active");
        jwindow.animate({
            left: "-25%"
        }, 500, function () {
            $(this).addClass("active")
        })
    });
    panelsToClose.each(function (i, mypanel) {
        var jwindow = $(mypanel);
        jwindow.removeClass("active");
        jwindow.animate({
            left: "150%"
        }, 500, function () {
            $(this).remove();
            history.back()
        })
    });
    $(".popwindow").slideDown("200", function () {
        $(".popwindow").remove()
    })
}

function closePanel(trigger) {
    var pathwindows = $("#panels .pathwindow");
    closePathWindows(pathwindows);
    if (trigger) {
        $(this).trigger(trigger)
    }
    if ($("#nav a").length >= 3) {
        var navitem = $("#nav a:last").prev();
        navitem.removeClass("active");
        setTimeout(function () {
            navitem.remove()
        }, 500);
        if ($("#nav a").length > 3) {
            history.back()
        }
    }
    setTimeout(function () {
        if ($("#nav a").length < 3) {
            $("#nav a").first().removeClass("path");
            $("#blackout").fadeOut(500);
            window.location.hash = "#"
        }
    }, 500)
}
$(document).on("updatePath", function (e) {
    thisPanel = $(e.target);
    var navTitle = thisPanel.find(".pathwindow_title h1,h2").html();
    $("#nav a.path").removeClass("disabled");
    setTimeout(function () {
        $("#nav a:last").before('<a class="path disabled" rel="' + thisPanel.attr("id") + '"><dl><dt>' + navTitle + "</dt><dd></dd></dl></a>")
    }, 100);
    setTimeout(function () {
        $("#nav a:last").prev().addClass("active");
        if ($("#nav a").length > 2) {
            $("#nav a :first").addClass("path");
            $("#blackout").fadeIn(500)
        }
    }, 150)
});
$(document).ready(function () {
    $(document).on("click", ".close_active_panel", function () {
        $("body").find(".pathwindow:last .pathwindow_whiteout").fadeOut(500, function () {
            $(this).remove()
        });
        closePanel()
    });
    $(document).off("click", "#nav a.path").on("click", "#nav a.path", function (e) {
        e.preventDefault();
        if ($(this).hasClass("disabled")) {
            return false
        }
        closePanelsAfter($("#panels #" + $(this).prop("rel")));
        $(this).addClass("disabled");
        $(this).nextAll(".path").fadeOut().remove()
    });
    $(document).off("click", "a.act_panel_open").on("click", "a.act_panel_open", function (event) {
        event.preventDefault();
        openPanel($(this).attr("href"), $(this).prop("rel"));
        return false
    });
    $(document).off("click", ".pathwindow a.btn_back").on("click", ".pathwindow a.btn_back", function (event) {
        $("body").find(".pathwindow:last .pathwindow_whiteout").fadeOut(500, function () {
            $(this).remove()
        });
        closePanel()
    });
    $(document).off("click", ".home").on("click", ".home", function (event) {
        event.preventDefault();
        if ($(".panel").length <= 0) {
            $(".popwindow").slideDown("200", function () {
                $(".popwindow").remove()
            });
            var panelsToClose = $("#panels .pathwindow");
            panelsToClose.removeClass("active");
            panelsToClose.each(function (i, mypanel) {
                var jwindow = $(mypanel);
                jwindow.animate({
                    left: "150%"
                }, 500 / (i + 1), function () {
                    $(this).remove()
                });
                if (i == panelsToClose.length - 1) {
                    $("#blackout").fadeOut(500);
                    $("#nav a").removeClass("active");
                    $("#nav a").first().removeClass("path");
                    setTimeout(function () {
                        $("#nav a.path").fadeOut().remove()
                    }, 100);
                    history.pushState({}, "", "#");
                    window.location.hash = "#"
                }
            })
        }
    });
    openPanel(window.location.hash.replace("#", ""))
});
$(document).ready(function () {
    $(document).on("click", ".popwindow .btn_close, .popwindow .pop_close", function (event) {
        "use strict";
        event.preventDefault();
        closePopup($(this).prop("data-popup-id"))
    });
    $(document).off("click", "a.act_popup_open").on("click", "a.act_popup_open", function (event) {
        "use strict";
        event.preventDefault();
        var title;
        var popup_class;
        var content_class;
        title = $(this).prop("title");
        popup_class = "info";
        content_class = "";
        if ($(this).prop("rel")) {
            popup_class = $(this).prop("rel")
        }
        if ($(this).attr("popsize")) {
            content_class = $(this).attr("popsize")
        }
        $("body").trigger("popUpLoading");
        $.get($(this).attr("href"), function (data) {
            showPopup(title, data, popup_class, content_class)
        });
        return false
    });
    $(document).ajaxError(function (event, jqXHR, ajaxSettings, thrownError) {
        "use strict";
        var content;
        var responseObj;
        var pathHashArray = window.location.hash.split("/");
        if (ajaxSettings.url == "" || pathHashArray[1] == "ah") {
            return false
        }
        content = $("<p>").css({
            "min-width": 350
        });
        responseObj = {};
        content.html(jqXHR.responseText);
        try {
            responseObj = JSON.parse(jqXHR.responseText)
        } catch (e) { }
        if (typeof responseObj === "object" && responseObj.hasOwnProperty("message")) {
            content.html(responseObj.message)
        }
        content.after('<div class="popwindow_buttons"><a class="pop_close btn_styled">OK</a><div class="clear"></div></div>');
        showPopup("An Error Occurred", content, "warning")
    });
    $(document).on("click", ".popwindow .preferred", function (event) {
        "use strict";
        event.preventDefault();
        var thisPopup;
        var form;
        var fields;
        var check;
        var valid;
        var currentPanel;
        var popup_id;
        var formData;
        var processD;
        var contentT;
        thisPopup = $(this).parents(".popwindow");
        if (thisPopup.length > 0) {
            form = thisPopup.find("form");
            if (form.length > 0) {
                fields = form.children().find("input.required,select.required,textarea.required");
                check = form.validate({
                    showErrors: function (errorMap, errorList) {
                        if (errorList[0]) {
                            $(errorList[0]["element"]).data("error", errorList[0]["message"])
                        }
                    }
                });
                valid = form.valid();
                $.each(fields, function (i, ifield) {
                    var field;
                    var valid;
                    field = $(ifield);
                    valid = check.element(field);
                    if (!valid) {
                        form_field_animate_error(field, field.data("error"));
                        show_error_notice(form, field.data("error"));
                        return false
                    }
                    if (field.val() == "Write your query here") {
                        show_error_notice(form, "Please enter your query");
                        return false
                    }
                    if (valid) {
                        form_field_clear_error(field)
                    }
                });
                currentPanel = $("#panels .pathwindow :last");
                popup_id = thisPopup.find(".btn_close").prop("data-popup-id");
                if (valid) {
                    formData = form.serialize();
                    processD = true;
                    contentT = "application/x-www-form-urlencoded";
                    if (typeof FormData !== "undefined") {
                        formData = new FormData(form[0]);
                        processD = false;
                        contentT = false
                    }
                    $("body").trigger("popUpLoading");
                    $.ajax({
                        url: form.attr("action"),
                        data: formData,
                        processData: processD,
                        contentType: contentT,
                        xhr: function () {
                            return $.ajaxSettings.xhr()
                        },
                        type: "POST",
                        beforeSend: function (jqXHR, settings) {
                            closePopup(popup_id);
                            currentPanel.append('<div class="pathwindow_whiteout" style="display:none;"></div>');
                            currentPanel.find(".pathwindow_whiteout").fadeIn(250)
                        },
                        success: function (data, textStatus, jqXHR) {
                            var pop_title;
                            var pop_class;
                            form.trigger("popupFormSubmited", [currentPanel]);
                            if (typeof data == "object") {
                                showPopup(data.title, data.message, data.code ? "success" : "warning")
                            } else {
                                pop_title = "Success";
                                pop_class = "success";
                                showPopup(pop_title, data, pop_class)
                            }
                        }
                    })
                }
            }
        }
    })
});

function showPopup(title, content, popup_class, content_class) {
    "use strict";
    var data;
    var popup;
    var jpopup;
    var id;
    var newtop;
    var newleft;
    content_class = typeof content_class !== "undefined" ? content_class : "";
    data = $(content);
    if (data.filter("#pop_title").length == 1) {
        title = data.filter("#pop_title").val()
    }
    if (data.filter("#pop_class").length == 1) {
        popup_class = data.filter("#pop_class").val()
    }
    if (data.filter("#popsize").length == 1) {
        content_class = data.filter("#popsize").val()
    }
    if ($(".popwindow").size() > 0) {
        closePopup($(".popwindow").children().find(".btn_close").prop("data-popup-id"))
    }
    $("#blackout").fadeIn(500);
    popup = '<div class="popwindow"><div class="popwindow_title"><div class="icon"></div><a class="btn_close">X</a><h1></h1></div><div class="popwindow_content ' + content_class + '"></div></div>';
    jpopup = $(popup);
    id = Math.floor(Math.random() * 10 + Math.random() * 10 + 1);
    jpopup.attr("id", "popup" + id);
    jpopup.addClass(popup_class);
    jpopup.find(".popwindow_title h1").prepend(title);
    jpopup.find(".popwindow_content").append(content);
    jpopup.find(".popwindow_title .btn_close").prop("data-popup-id", id);
    jpopup.find(".popwindow_buttons .pop_close").prop("data-popup-id", id);
    $("body").append(jpopup);
    newtop = parseInt(jpopup.outerHeight()) / 2;
    newleft = parseInt(jpopup.width()) / 2;
    if ($(".sf-toolbarreset").length > 0 && newtop > 1e3) {
        newtop = window.outerHeight / 2.5;
        newleft = window.outerWidth / 2.5;
        jpopup.css("min-width", window.outerWidth / 1.3);
        jpopup.css("min-height", window.outerHeight / 1.4);
        jpopup.css("height", "10%");
        jpopup.css("overflow-y", "scroll")
    }
    jpopup.css("marginLeft", -newleft);
    jpopup.delay(250).animate({
        marginTop: -newtop
    }, 750, function () {
        jpopup.addClass("active")
    });
    $("body").trigger("popUpLoaded")
}

function closePopup(id) {
    "use strict";
    var thisPopup;
    thisPopup = $("#popup" + id);
    thisPopup.closest(".popwindow").css("top", "53%").removeClass("active");
    thisPopup.closest(".popwindow").animate({
        marginTop: "1200px"
    }, 750, function () {
        thisPopup.closest(".popwindow").remove()
    });
    if ($("body").find(".pathwindow:last").length == 0) {
        $("#blackout").fadeOut(500)
    }
    $("body").find(".pathwindow:last .pathwindow_whiteout").fadeOut(500, function () {
        $(this).remove()
    });
    $("body").trigger("popUpClosed")
}
$(document).ready(function () {
    $(document).off("click", "#support_ticket_list .duolink").on("click", "#support_ticket_list .duolink", function (e) {
        e.preventDefault();
        $("#support_ticket_list a").removeClass("selected");
        $(this).addClass("selected");
        $.get($(this).attr("href"), function (data) {
            $("#support_ticket_messages").html(data);
            support_animate_tickets($("#support_ticket_messages").find(".messageholder"))
        });
        return false
    });
    $(document).off("click", "#create_new_ticket").on("click", "#create_new_ticket", function (e) {
        e.preventDefault();
        $("#support_ticket_list a").removeClass("selected");
        if ($("#support_ticket_messages .create_ticket_holder").size() <= 0) {
            $.get($(this).attr("href"), function (data) {
                $("#support_ticket_messages > :first-child").hide();
                $("#support_ticket_messages").prepend(data);
                support_animate_tickets($("#support_ticket_messages .create_ticket_holder"))
            })
        }
        return false
    });
    $(document).off("click", "#submit_new_ticket").on("click", "#submit_new_ticket", function (e) {
        e.preventDefault();
        var form = $(this).parents("form");
        var valid = validate_support_form(form);
        if (valid) {
            $.post(form.attr("action"), form.serialize(), function (data) {
                $("#support_ticket_list").html(data);
                $("#support_ticket_list").find("a:first").click()
            })
        }
        return false
    });
    $(document).off("click", ".message_action_reply").on("click", ".message_action_reply", function (e) {
        e.preventDefault();
        $(this).parent().fadeOut();
        support_animate_tickets($("#support_ticket_messages .listholder > :first-child").show());
        return false
    });
    $(document).off("click", "#close_support_ticket").on("click", "#close_support_ticket", function (e) {
        e.preventDefault();
        $(this).fadeOut();
        $.ajaxSetup({
            global: true
        });
        $("#support_ticket_list").load($(this).attr("href"), function () {
            $.ajaxSetup({
                global: true
            })
        });
        return false
    });
    $(document).off("click", "#cancel_create_new_ticket").on("click", "#cancel_create_new_ticket", function (e) {
        e.preventDefault();
        support_animate_tickets($(this).parents(".messageholder"), "up", function () {
            $(this).remove()
        });
        if ($("#support_ticket_list .duolink:first").length > 0) {
            $("#support_ticket_list .duolink:first").trigger("click")
        } else {
            $("#support_ticket_messages > :first-child").fadeIn()
        }
        return false
    });
    $(document).off("click", "#cancel_support_post").on("click", "#cancel_support_post", function (e) {
        e.preventDefault();
        support_animate_tickets($("#cancel_support_post").parents(".messageholder"), "up", function () {
            $(this).hide()
        });
        $(".msg_actions").fadeIn();
        return false
    });
    $(document).off("click", "#submit_ticket_post").on("click", "#submit_ticket_post", function (e) {
        e.preventDefault();
        var form = $(this).parents("form");
        var valid = validate_support_form(form);
        if (valid) {
            $.post(form.attr("action"), form.serialize(), function (data) {
                $("#support_ticket_messages").html(data)
            })
        }
        return false
    })
});

function validate_support_form(form) {
    var fields = form.children().find("input,select,textarea");
    var check = form.validate({
        showErrors: function (errorMap, errorList) {
            if (errorList[0]) {
                $(errorList[0]["element"]).data("error", errorList[0]["message"])
            }
        }
    }),
        valid = false;
    $.each(fields, function (i, field) {
        field = $(field);
        valid = check.element(field);
        if (!valid) {
            form_field_animate_error(field, field.data("error"));
            show_error_notice(form, field.data("error"));
            return false
        }
        if (field.val() == "Write your query here") {
            show_error_notice(form, "Please enter your query");
            valid = false;
            return false
        }
        if (valid) {
            form_field_clear_error(field)
        }
    });
    return valid
}
$(document).ready(function () {
    $(document).off("change", "#tempDir").on("change", "#tempDir", function (e) {
        e.preventDefault();
        var directory = $(this).val();
        $.ajax({
            type: "POST",
            url: "/en/newsletters/templates",
            data: {
                dir: directory
            },
            dataType: "json"
        }).done(function (obj) {
            var options = '<option value="">-- Please Choose --</option>';
            $.each(obj, function (key, value) {
                options += '<option value="' + value + '">' + value + "</option>"
            });
            $("#tempName").html(options)
        }).fail(function (e) {
            alert("Something went wrong")
        })
    });
    $(document).off("change", "#tempName").on("change", "#tempName", function (e) {
        e.preventDefault();
        var directory = $("#tempDir").val();
        var template = $(this).val();
        $("#previewDiv").load("/en/newsletters/preview?templateName=" + template + "&folderName=" + directory).html()
    });
    $(document).off("click", "#submitStats").on("click", "#submitStats", function (e) {
        e.preventDefault();
        var selectedVal = "";
        var selected = $("input[type='radio'][name='numResults']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val()
        }
        var sql = $("#rawSQL").val();
        if (!sql) {
            $("#infoSQlalert").removeClass("alert-info");
            $("#infoSQlalert").addClass("alert-danger");
            $("#infoSQlalert").show();
            $("#infoSQl").html("Please enter SQL statment!");
            $("#infoSQlalert").delay(3e3).fadeOut(400);
            return false
        }
        $("#infoSQlalert").removeClass("alert-danger");
        $("#infoSQlalert").addClass("alert-info");
        $("#infoSQlalert").show();
        $("#infoSQl").html("LOADING..");
        $.ajax({
            type: "POST",
            url: "/en/newsletters/preview/stats",
            data: {
                rawSQL: sql,
                numResults: selectedVal
            },
            dataType: "html"
        }).done(function (obj) {
            $("#infoSQlalert").show();
            $("#infoSQl").html(obj)
        }).fail(function (e) {
            $("#infoSQlalert").hide();
            $("#infoSQlalert").removeClass("alert-info");
            $("#infoSQlalert").addClass("alert-danger");
            $("#infoSQlalert").show();
            $("#infoSQl").html("There was an error!");
            $("#infoSQlalert").delay(3e3).fadeOut(400)
        })
    });
    $("#textWording").keyup(function () {
        var textWording = $("#textWording").val();
        $("#previewDiv").html('<pre style="background-color: white; border:0;">' + textWording + "</pre>")
    });
    $(document).off("click", "#submitPreview").on("click", "#submitPreview", function (e) {
        e.preventDefault();
        var directory = $("#tempDir").val();
        var template = $("#tempName").val();
        var type = $("#template").val();
        var temlWording = $("#textWording").val();
        var selectedVal = "";
        var selected = $("input[type='radio'][name='numResults']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val()
        }
        var sql = $("#rawSQL").val();
        var mailSubject = $("#mailSubject").val();
        var prevEmail = $("#prevEmail").val();
        var emailFormatVal = $("input[name=emailFormat]:checked").val();
        var fromEmailVal = "";
        var fromEmailSelected = $("input[type='radio'][name='fromEmail']:checked");
        if (fromEmailSelected.length > 0) {
            fromEmailVal = fromEmailSelected.val()
        }
        $("#infoSQlalert").removeClass("alert-danger");
        $("#infoSQlalert").addClass("alert-info");
        $("#infoSQlalert").show();
        $("#infoSQl").html("SENDING..");
        $.ajax({
            type: "POST",
            url: "/en/newsletters/preview/send",
            data: {
                dir: directory,
                templ: template,
                rawSQL: sql,
                numResults: selectedVal,
                prevEmail: prevEmail,
                mailSubject: mailSubject,
                tempType: type,
                templWording: temlWording,
                fromEmail: fromEmailVal,
                emailFormat: emailFormatVal
            },
            dataType: "html"
        }).done(function (obj) {
            $("#infoSQlalert").show();
            $("#infoSQl").html(obj)
        }).fail(function (e) {
            $("#infoSQlalert").hide();
            $("#infoSQlalert").removeClass("alert-info");
            $("#infoSQlalert").addClass("alert-danger");
            $("#infoSQlalert").show();
            $("#infoSQl").html(e);
            $("#infoSQlalert").delay(3e3).fadeOut(400)
        })
    });
    $(document).off("click", "#submitAll").on("click", "#submitAll", function (e) {
        e.preventDefault();
        var directory = $("#tempDir").val();
        var template = $("#tempName").val();
        var type = $("#template").val();
        var temlWording = $("#textWording").val();
        var sql = $("#rawSQL").val();
        var mailSubject = $("#mailSubject").val();
        var fromEmailVal = "";
        var fromEmailSelected = $("input[type='radio'][name='fromEmail']:checked");
        if (fromEmailSelected.length > 0) {
            fromEmailVal = fromEmailSelected.val()
        }
        var emailFormatVal = $("input[name=emailFormat]:checked").val();
        $("#infoSQlalert").removeClass("alert-danger");
        $("#infoSQlalert").addClass("alert-info");
        $("#infoSQlalert").show();
        $("#infoSQl").html("SENDING..");
        $.ajax({
            type: "POST",
            url: "/en/newsletters/send",
            data: {
                dir: directory,
                templ: template,
                rawSQL: sql,
                mailSubject: mailSubject,
                tempType: type,
                templWording: temlWording,
                fromEmail: fromEmailVal,
                emailFormat: emailFormatVal
            },
            dataType: "html"
        }).done(function (obj) {
            $("#infoSQlalert").show();
            $("#infoSQl").html(obj)
        }).fail(function (e) {
            $("#infoSQlalert").hide();
            $("#infoSQlalert").removeClass("alert-info");
            $("#infoSQlalert").addClass("alert-danger");
            $("#infoSQlalert").show();
            $("#infoSQl").html("There was an error!");
            $("#infoSQlalert").delay(3e3).fadeOut(400)
        })
    });
    $(document).off("click", ".radioNumEmails").on("click", ".radioNumEmails", function (e) {
        var selected = $("input[type='radio'][name='numResults']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val()
        }
        $("#numCopies").html(selectedVal)
    })
});
$(document).ready(function () {
    $(document).off("click", "#notices_list .duolink").on("click", "#notices_list .duolink", function (e) {
        e.preventDefault();
        $("#notices_list .duolink").removeClass("selected");
        $(this).addClass("selected");
        $.ajaxSetup({
            global: false
        });
        $("div#client_notices").load($(this).attr("href"), function () {
            $.ajaxSetup({
                global: true
            })
        });
        return false
    });
    $(document).off("click", "#client_notices a.markasread").on("click", "#client_notices a.markasread", function (e) {
        e.preventDefault();
        $.ajaxSetup({
            global: false
        });
        $.post($(this).attr("href"), {
            notificationId: $(this).prop("id")
        }, function (data) {
            $("h1#read-messages").slideDown("slow");
            $("#notices_list .selected span").removeClass("active");
            $("#notices_list .selected").insertAfter("#read-messages");
            $("#notices_list .selected").trigger("click");
            if ($("h1#unread-messages").next().is($("h1#read-messages"))) {
                $("h1#unread-messages").slideUp("slow");
                $("#message_notifier.active").removeClass("active")
            }
        });
        $.ajaxSetup({
            global: true
        });
        return false
    });
    $(document).off("click", "#client_notices a.markasunread").on("click", "#client_notices a.markasunread", function (e) {
        e.preventDefault();
        $.ajaxSetup({
            global: false
        });
        $.post($(this).attr("href"), {
            notificationId: $(this).prop("id")
        }, function (data) {
            $("h1#unread-messages").slideDown("slow");
            $("#notices_list .selected span").addClass("active");
            $("#notices_list .selected").insertAfter("#unread-messages");
            $("#notices_list .selected").trigger("click");
            if ($("#notices_list").children().last().is($("h1#read-messages"))) {
                $("h1#read-messages").slideUp("slow");
                $("#message_notifier").addClass("active");
                message_pulse()
            }
        });
        $.ajaxSetup({
            global: true
        });
        return false
    });
    $(document).on("click", "#btn_generate_half_off_mifi_coupon", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/en/api/promotion-invitations/coupons/claim",
            type: "GET",
            async: false,
            success: function (data) {
                $.ajax({
                    url: "/coupon/loadCoupon",
                    data: {
                        coupon_code: data.code
                    },
                    async: false,
                    success: function (data1) {
                        var json = $.parseJSON(data1);
                        location.href = "https://Cash Analyzer.WFP Jordan.com" + json.url
                    }
                })
            }
        });
        return false
    })
});
$(document).ready(function () {
    $(document).off("focusout", "#personal_details_container .holder_label").on("focusout", "#personal_details_container .holder_label", function () {
        var field = $(this).children("input,select");
        var form = field.parents("form");
        var currentVal = field.attr("rel");
        var valid = form.validate({
            showErrors: function (errorMap, errorList) {
                if (errorList[0]) {
                    $(errorList[0]["element"]).data("error", errorList[0]["message"])
                }
            }
        }).element(field);
        if (!valid) {
            form_field_animate_error(field, field.data("error"));
            return false
        }
        if (valid) {
            form_field_clear_error(field)
        }
        if (valid && currentVal == field.val()) {
            form_field_clear_error(field);
            return false
        }
        if (valid && currentVal != field.val()) {
            $.ajax({
                global: false,
                url: form.attr("action"),
                data: field.serialize(),
                type: "POST",
                beforeSend: function (xhr) { },
                success: function (data) {
                    field.attr("rel", field.val());
                    form_field_animate_success(field, data.message)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    form_field_animate_error(field, "An Error Occured when trying to update your details")
                }
            })
        }
        return false
    });
    $(document).off("keyup", "#account_access_details #Username").on("keyup", "#account_access_details #Username", function () {
        var form = $(this).parents("form");
        if ($(this).val() == $(this).prop("rel")) {
            form_field_clear_error($(this))
        }
        var valid = form.validate({
            showErrors: function (errorMap, errorList) {
                if (errorList[0]) {
                    $(errorList[0]["element"]).data("error", errorList[0]["message"])
                }
            }
        }).element($(this));
        if (valid) {
            form_field_clear_error($(this))
        }
        if (!valid) {
            form_fields_highlight_error([$(this)])
        }
    });
    $(document).off("click", "#account_access_details #change_email_address").on("click", "#account_access_details #change_email_address", function (e) {
        e.preventDefault();
        var form = $(this).parents("form");
        var field = $("#account_access_details #Username");
        var checkb = $("#account_access_details #updateEppDetails");
        var currentVal = field.attr("rel");
        var valid = form.validate({
            showErrors: function (errorMap, errorList) {
                if (errorList[0]) {
                    $(errorList[0]["element"]).data("error", errorList[0]["message"])
                }
            }
        }).element(field);
        var valid1 = form.validate({
            showErrors: function (errorMap, errorList) {
                if (errorList[0]) {
                    $(errorList[0]["element"]).data("error", errorList[0]["message"])
                }
            }
        }).element(checkb);
        if (!valid) {
            form_field_animate_error(field, field.data("error"));
            return false
        }
        if (!valid1) {
            form_field_animate_error(checkb, checkb.data("error"));
            return false
        }
        if (valid) {
            form_field_clear_error(field)
        }
        if (valid && currentVal == field.val()) {
            form_field_clear_error(field);
            return false
        }
        if (valid && valid1 && currentVal != field.val()) {
            $.ajax({
                global: false,
                url: form.attr("action"),
                data: form.serialize(),
                type: "POST",
                beforeSend: function (xhr) { },
                success: function (data) {
                    if (data.code == 1) {
                        field.attr("rel", field.val());
                        form_field_animate_success(field, data.message);
                        form_field_animate_success(checkb, data.message)
                    } else {
                        form_field_animate_error(field, data.message);
                        form_field_animate_error(checkb, data.message)
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    message = "An Error Occured when trying to update your details";
                    form_field_animate_error(field, message);
                    form_field_animate_error(checkb, message)
                }
            })
        }
    });
    $(document).off("click", "#account_access_details #change_password").on("click", "#account_access_details #change_password", function (e) {
        e.preventDefault();
        var form = $(this).parents("form");
        validate = form.validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 5
                },
                new_password_again: {
                    equalTo: "#new_password"
                }
            },
            showErrors: function (errorMap, errorList) { },
            invalidHandler: function (form, validator) {
                $.each(validator.errorList, function (index, item) {
                    form_field_animate_error($(item["element"]), item["message"])
                })
            }
        });
        if (form.valid()) {
            $.ajax({
                global: false,
                url: form.attr("action"),
                data: form.serialize(),
                type: "POST",
                beforeSend: function (xhr) { },
                success: function (data) {
                    if (data.code == 1) {
                        form_fields_highlight_normal(form.find("input"));
                        show_success_notice(form, data.message);
                        clear_form_values(form)
                    } else {
                        show_error_notice(form, data.message)
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    message = "An Error Occured when trying to update your details";
                    show_error_notice(form, message)
                }
            })
        }
    });
    $(document).off("click", "#contact_preferences :checkbox").on("click", "#contact_preferences :checkbox", function (e) {
        var form = $(this).parents("form");
        var holder = form.parents(".holder_check");
        var parent = holder.parents(".sectionholder");
        var notice = parent.children(".notice_success");
        var label = holder.children("label");
        var message = "";
        $.ajax({
            global: false,
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            beforeSend: function (xhr) { },
            success: function (data) {
                if (data.code == 1) {
                    form_field_animate_success(label, data.message);
                    notice.fadeIn(500, function () {
                        setTimeout(function () {
                            notice.slideUp(500)
                        }, 3e3)
                    })
                } else {
                    message = "An Error Occured when trying to update your details";
                    show_error_notice(label, message)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                message = "An Error Occured when trying to update your details";
                show_error_notice(form, message)
            }
        })
    });
    $(document).off("click", "#personal_details_links a").on("click", "#personal_details_links a", function (e) {
        e.preventDefault();
        if ($(this).hasClass("act_panel_open")) {
            return
        }
        var viewToShow = $($(this).attr("href"));
        $("#personal_details_links a").removeClass("selected");
        $(this).addClass("selected");
        $("#personal_details_links a").each(function (index, element) {
            if ($(element).hasClass("act_panel_open")) {
                return
            }
            $($(element).attr("href")).slideUp(500)
        });
        viewToShow.slideDown("500")
    });
    $(document).off("click", "a#expire_client_tiny_urls").on("click", "a#expire_client_tiny_urls", function (e) {
        e.preventDefault();
        $.post("/en/api/tiny_url/expire_links.json", {}, function (data, status) {
            if (data.code == 200) {
                setTimeout(function () {
                    showPopup("Success!", "<h1>Links Expired</h1><p>" + data.count + " links have been disabled! This will not affect future links</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "success")
                }, 500)
            } else {
                setTimeout(function () {
                    showPopup("An Error Occured!", "<p>Please try again later or call us on 011 612 7200</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "warning")
                }, 500)
            }
        })
    })
});
$(document).ready(function () {
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email)
    }
    $("#new_client_signup_panel #add_new_client").click(function (e) {
        $("#error_msg").hide();
        var error_msg = 0;
        if (!$("#new_mobile").val() || !$.isNumeric($("#new_mobile").val()) || $("#new_mobile").val().length < 10 || $("#new_mobile").val().length > 12) {
            error_msg = "Please provide a valid mobile number"
        }
        if (!$("#new_id").val() || $("#new_id").val().length < 6) {
            error_msg = "Please provide a valid ID Number"
        }
        if (!$("#new_surname").val() || $("#new_surname").val().length < 3) {
            error_msg = "Please provide your Last Name"
        }
        if (!$("#new_firstname").val()) {
            error_msg = "Please provide your Name"
        }
        if (!validateEmail($("#new_email").val())) {
            error_msg = "Please provide a valid email address"
        }
        if (error_msg != 0) {
            $("#error_msg").html("<label><span>" + error_msg + "</span></label>");
            $("#error_msg").show();
            return false
        }
        $("#new_client_signup_panel").append('<div class="pathwindow_whiteout" style="display:none;"></div>');
        var form = $("#new_client_signup_panel #new_client_signup_form");
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                showPopup("Complete!", data, "success");
                $("#add_new_client").hide();
                $("#login_link").show()
            }
        });
        return false
    })
});
$(document).off("click", "#accountHistoryListHolder a").on("click", "#accountHistoryListHolder a", function (e) {
    e.preventDefault();
    $("#accountHistoryListHolder a.account_select").removeClass("selected");
    $(this).addClass("selected");
    account_to_show = $(this).prop("rel");
    holder = "holder" + account_to_show;
    if (holder == $("#accountHistorySections .sectionholder:visible").prop("id")) {
        return false
    }
    $("#accountHistorySections .sectionholder").slideUp("500", function () {
        $("#" + holder).slideDown("500")
    });
    return false
});
$(document).off("click", ".act_download_statement").on("click", ".act_download_statement", function (e) {
    e.preventDefault();
    form = $(this).parent().prev();
    form.submit()
});
$(document).off("click", "#accountPaymentListHolder a").on("click", "#accountPaymentListHolder a", function (e) {
    e.preventDefault();
    $("#accountPaymentListHolder a").removeClass("selected");
    $(this).addClass("selected");
    account_to_show = $(this).prop("rel");
    holder = "holder" + account_to_show;
    if (holder == $("#account_payment_details .sectionholder:visible").prop("id")) {
        return false
    }
    $("#account_payment_details .sectionholder").slideUp("500", function () {
        $("#" + holder).slideDown("500")
    });
    return false
});
$(document).off("click", "#accountPaymentListHolder2 a").on("click", "#accountPaymentListHolder2 a", function (e) {
    e.preventDefault();
    $("#accountPaymentListHolder2 a").removeClass("selected");
    $(this).addClass("selected");
    account_to_show = $(this).prop("rel");
    holder = "holder2" + account_to_show;
    if (holder == $("#account_payment_details.acpd .sectionholder:visible").prop("id")) {
        return false
    }
    $("#account_payment_details.acpd .sectionholder").slideUp("500", function () {
        $("#" + holder).slideDown("500")
    });
    return false
});
$(document).off("click", "#account_payment_details .act_settle_account").on("click", "#account_payment_details .act_settle_account", function (e) {
    e.preventDefault();
    form = $(this).prev();
    accid = $(this).prop("rel");
    form_fields_highlight_normal(form.find("input,select"));
    validate = form.validate({
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    if (form.valid()) {
        openPanel(form.attr("action"), null, form.serialize(), "post")
    }
    return false
});
$(document).off("click", "#settleMyAccountListHolder a").on("click", "#settleMyAccountListHolder a", function (e) {
    e.preventDefault();
    $("#settleMyAccountListHolder a").removeClass("selected");
    $(this).addClass("selected");
    account_to_show = $(this).prop("rel");
    holder = "holder" + account_to_show;
    if (holder == $("#settle_my_account_list_holder .sectionholder:visible").prop("id")) {
        return false
    }
    $("#settle_my_account_list_holder .sectionholder").slideUp("500", function () {
        $("#" + holder).slideDown("500")
    });
    return false
});

function reload_settle_account() {
    var settle_my_account_panel = $("#settle_my_account.pathwindow");
    $.ajaxSetup({
        global: false
    });
    if (settle_my_account_panel.length > 0) {
        $("#settle_my_account .pathwindow_content").load("/en/my-billing/settle-my-account #settle_my_account .pathwindow_content > *", function () {
            settle_my_account_panel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}
$(document).ready(function () {
    "use strict";
    var validate;

    function makeAccountPaymentAdditionalExisting(e) {
        e.preventDefault();
        var form = $("#account_payment_additional_form");
        form_fields_highlight_normal(form.find("input,select"));
        validate = form.validate({
            showErrors: function (errorMap, errorList) { },
            invalidHandler: function (form, validator) {
                $.each(validator.errorList, function (index, item) {
                    form_field_animate_error($(item["element"]), item["message"])
                })
            }
        });
        if (form.valid()) {
            if ($("input[name='payment_type']").val() == "cc" || $("#payment_type_settle_account").val() == "cc") {
                openPanel("/en/my-billing/account/payment/credit-card", null, form.serialize(), "post")
            } else {
                $.post(form.attr("action"), form.serialize()).done(function (data) {
                    closePanel();
                    reload_settle_account();
                    showPopup("Payment Submitted!", data, "success", "", "small")
                }).fail(function () { })
            }
        }
    }
    $(document).on("click", "#account_payment_additional_form .actionbuttons", makeAccountPaymentAdditionalExisting)
});
$(document).off("change", "#invoiceYear, #invoiceMonth").on("change", "#invoiceYear, #invoiceMonth", function (e) {
    e.preventDefault();
    form = $(this).parents("form");
    $.post(form.attr("action"), form.serialize(), function (data) {
        $("#invoiceHolder").slideUp("500", function () {
            $("#invoiceHolder").html(data);
            $("#invoiceHolder").slideDown("500")
        })
    });
    return false
});
$(document).off("click", "#invoiceAccountListHolder a").on("click", "#invoiceAccountListHolder a", function (e) {
    e.preventDefault();
    $("#invoiceAccountListHolder a").removeClass("selected");
    $(this).addClass("selected");
    if ($("#invoiceContainer .sectionholder:visible input").val() == $(this).prop("rel")) {
        return false
    }
    $("#invoiceContainer").load($(this).attr("href") + $(this).prop("rel"), function (data) {
        $("#invoiceContainer").slideUp("500", function () {
            $("#invoiceContainer").html(data);
            $("#invoiceContainer").slideDown("500")
        });
        $.ajaxSetup({
            global: true
        })
    })
});
$(document).on("popupFormSubmited", function (e, panel) {
    form = $(e.target);
    if (form.attr("id") == "delete_payment_details") {
        reload_manage_payment_details();
        reload_my_billing();
        closePanel();
        currentPanel = $(panel).prev();
        show_whiteout(currentPanel)
    }
});

function reload_my_billing() {
    var myBillingPanel = $("#my_billing.pathwindow");
    $.ajaxSetup({
        global: false
    });
    if (myBillingPanel.length > 0) {
        $("#my_billing .pathwindow_content").load("/en/my-billing #my_billing .pathwindow_content > *", function () {
            myBillingPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_manage_payment_details() {
    $.ajaxSetup({
        global: false
    });
    var managePaymentDetailPanel = $("#manage_payment_details.pathwindow");
    if (managePaymentDetailPanel.length > 0) {
        $("#manage_payment_details .pathwindow_content").load("/en/my-billing/manage-payment-details #manage_payment_details .pathwindow_content > *", function () {
            managePaymentDetailPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}
$(document).off("keyup", "#credit_card_update_form #card_number").on("keyup", "#credit_card_update_form #card_number", function () {
    $("#ccard_front_number,#ccard_back_number").html($(this).val());
    hasX = false;
    if ($(this).val().indexOf("X") >= 0) {
        hasX = true
    }
    if (!hasX) {
        $("#card_image").removeClass("mc", 1e3);
        $("#card_image").removeClass("visa", 1e3);
        $("#ccard_back_lastdigits").html("")
    }
    if ($(this).val().length == 16) {
        $("#ccard_back_lastdigits").html($(this).val().substr(12, 4))
    }
    if ($(this).val().match(/^4\d{12}(\d\d\d){0,1}$/) && !hasX) {
        $("#card_image").addClass("visa", 1e3)
    }
    if ($(this).val().match(/^5[12345]\d{14}$/) && !hasX) {
        $("#card_image").addClass("mc", 1e3)
    }
    return false
});
$(document).off("change", "#credit_card_update_form #cc_month").on("change", "#credit_card_update_form #cc_month", function () {
    $("#ccard_fexp,ccard_bexp").html($(this).val());
    return false
});
$(document).off("change", "#credit_card_update_form #cc_year").on("change", "#credit_card_update_form #cc_year", function () {
    $("#ccard_fyear,#ccard_byear").html($(this).val());
    return false
});
$(document).off("keyup", "#credit_card_update_form #card_cvv").on("keyup", "#credit_card_update_form #card_cvv", function () {
    $("#ccard_cvv").html($(this).val());
    return false
});
$(document).off("keyup", "#credit_card_update_form #card_holder").on("keyup", "#credit_card_update_form #card_holder", function () {
    $("#ccard_back_name,#ccard_front_name").html($(this).val());
    return false
});
$(document).off("click", "#payment_product_holder a.move_to_linked").on("click", "#payment_product_holder a.move_to_linked", function (e) {
    e.preventDefault();
    group_linked = $("#group_linked");
    group_not_linked = $("#group_not_linked");
    check_span = $(this).find(".check");
    check_input = $(this).find(".check input");
    if (check_span.hasClass("checked")) {
        group_not_linked.after($(this));
        check_input.removeAttr("checked");
        check_span.removeClass("checked status_indicator active").addClass("unchecked")
    } else {
        group_not_linked.before($(this));
        check_input.prop("checked", true);
        check_span.removeClass("unchecked").addClass("status_indicator active checked")
    }
    return false
});
$(document).off("click", "#update_debit_order").on("click", "#update_debit_order", function (e) {
    e.preventDefault();
    form = $("#debit_order_update_form");
    dovalidate = false;
    form_valid = true;
    form.find("input,select").each(function (index, el) {
        if ($(el).val() != $(el).prop("rel")) {
            dovalidate = true
        }
    });
    form_fields_highlight_normal(form.find("input,select"));
    if (dovalidate) {
        validate = form.validate({
            showErrors: function (errorMap, errorList) { },
            invalidHandler: function (form, validator) {
                $.each(validator.errorList, function (index, item) {
                    form_field_animate_error($(item["element"]), item["message"])
                })
            }
        });
        form_valid = form.valid()
    }
    if (form_valid) {
        data = "update_card=" + dovalidate + "&" + form.serialize();
        if ($("#link_product_form").serialize() != "") {
            data = data + "&update_payment_links=true&" + $("#link_product_form").serialize()
        }
        if ($("#link_product_form").serialize() == "" && !dovalidate) {
            content = '<h1>Nothing Has Changed</h1><p><strong>There has been no modifications to your payment options</strong></p><div class="popwindow_buttons"><a class="pop_close btn_styled">OK</a><div class="clear"></div></div>';
            showPopup("No Modifications Found", content, "warning");
            return false
        }
        $.ajax({
            url: form.attr("action"),
            data: data,
            type: "POST",
            success: function (data) {
                closePanel();
                reload_manage_payment_details();
                reload_my_billing();
                var list = window.location.hash.split("/");
                if (list.length > 4) {
                    var username = list.pop();
                    var parts = username.split("~");
                    if (parts.length == 2) {
                        reload_account_detail(parts[0], parts[1])
                    }
                }
                setTimeout(function () {
                    showPopup("Payment Details Updated!", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#update_credit_card").on("click", "#update_credit_card", function (e) {
    e.preventDefault();
    form = $("#credit_card_update_form");
    dovalidate = false;
    form_valid = true;
    form.find("input,select").each(function (index, el) {
        if ($(el).val() != $(el).prop("rel")) {
            dovalidate = true
        }
    });
    form_fields_highlight_normal(form.find("input,select"));
    if (dovalidate) {
        validate = form.validate({
            showErrors: function (errorMap, errorList) { },
            invalidHandler: function (form, validator) {
                $.each(validator.errorList, function (index, item) {
                    form_field_animate_error($(item["element"]), item["message"])
                })
            }
        });
        form_valid = form.valid()
    }
    if (form_valid) {
        data = "update_card=" + dovalidate + "&" + form.serialize();
        if ($("#link_product_form").serialize() != "") {
            data = data + "&update_payment_links=true&" + $("#link_product_form").serialize()
        }
        if ($("#link_product_form").serialize() == "" && !dovalidate) {
            content = '<h1>Nothing Has Changed</h1><p><strong>There has been no modifications to your payment options</strong></p><div class="popwindow_buttons"><a class="pop_close btn_styled">OK</a><div class="clear"></div></div>';
            showPopup("No Modifications Found", content, "warning");
            return false
        }
        openPanel("/en/my-billing/payment/credit-card", null, data, "post")
    }
    return false
});
$(document).off("change", "#methodSelect").on("change", "#methodSelect", function (e) {
    e.preventDefault();
    if ($("#methodSelect").val() == "cc") {
        $("#doCreate").hide();
        $("#ccCreate").show()
    } else {
        $("#doCreate").show();
        $("#ccCreate").hide()
    }
});
$(document).off("change", "#bankSelect").on("change", "#bankSelect", function (e) {
    e.preventDefault();
    var bankName = $("#bankSelect").val();
    $.ajax({
        url: "api/my-billing/bank-branches.json",
        data: {
            bankName: bankName
        },
        type: "POST",
        success: function (data) {
            var mySelect = $("#selectBranch");
            mySelect.empty();
            mySelect.append($("<option></option>").val("").html("- Please Select -"));
            $.each(data.branches, function (index, item) {
                mySelect.append($("<option></option>").val(item.branch_name).html(item.branch_name))
            })
        }
    });
    return false
});
$(document).off("click", "#new_payment_details #addNewPaymentDetail_btn").on("click", "#new_payment_details #addNewPaymentDetail_btn", function (e) {
    e.preventDefault();
    var form = $("#created_payment_details_frm");
    var method = $("#methodSelect").val();
    var cc_number = $("#created_payment_details_frm #cc_number");
    var cc_expireyear = $("#created_payment_details_frm #cc_expireyear");
    var cc_expiremonth = $("#created_payment_details_frm #cc_expiremonth");
    var cc_holder = $("#created_payment_details_frm #cc_holder");
    var cc_cvv = $("#created_payment_details_frm #cc_cvv");
    var do_holder = $("#created_payment_details_frm #do_accholder");
    var do_number = $("#created_payment_details_frm #do_accnumber");
    var do_type = $("#created_payment_details_frm #do_acctype");
    var do_bank = $("#created_payment_details_frm #bankSelect");
    var do_branch = $("#created_payment_details_frm #selectBranch");
    if (method == "cc") {
        if (cc_number.val() == "" || /[^0-9]/.test(cc_number.val())) {
            form_field_animate_error(cc_number, "Please insert a vallid credit card number.");
            return false
        } else {
            form_field_clear_error(cc_number)
        }
        if (cc_expireyear.val() == "") {
            form_field_animate_error(cc_expireyear, "Please select an expire year.");
            return false
        } else {
            form_field_clear_error(cc_expireyear)
        }
        if (cc_expiremonth.val() == "") {
            form_field_animate_error(cc_expiremonth, "Please select an expire month.");
            return false
        } else {
            form_field_clear_error(cc_expiremonth)
        }
        if (cc_holder.val() == "") {
            form_field_animate_error(cc_holder, "Please enter a valid account holder.");
            return false
        } else {
            form_field_clear_error(cc_holder)
        }
        if (cc_cvv.val() == "") {
            form_field_animate_error(cc_cvv, "Please enter your cvv number.");
            return false
        } else {
            form_field_clear_error(cc_cvv)
        }
    }
    if (method == "do") {
        if (do_holder.val() == "") {
            form_field_animate_error(do_holder, "Please input a valid account holder.");
            return false
        } else {
            form_field_clear_error(do_holder)
        }
        if (do_number.val() == "" || /[^0-9]/.test(do_number.val())) {
            form_field_animate_error(do_number, "Please select an account number.");
            return false
        } else {
            form_field_clear_error(do_number)
        }
        if (do_type.val() == "") {
            form_field_animate_error(do_type, "Please select an account type.");
            return false
        } else {
            form_field_clear_error(do_type)
        }
        if (do_bank.val() == "") {
            form_field_animate_error(do_bank, "Please select an account type.");
            return false
        } else {
            form_field_clear_error(do_bank)
        }
        if (do_branch.val() == "") {
            form_field_animate_error(do_branch, "Please select an account type.");
            return false
        } else {
            form_field_clear_error(do_branch)
        }
    }
    if (form.valid) {
        if (method == "cc") {
            openPanel("/en/my-billing/payment/credit-card", null, form.serialize(), "post")
        }
        if (method == "do") {
            $.ajax({
                url: form.attr("action"),
                data: form.serialize(),
                type: "POST",
                success: function (data) {
                    setTimeout(function () {
                        showPopup("Thank You!", data, "success")
                    }, 500)
                }
            })
        }
    }
    return false
});
$(document).off("click", "#new_link_product_frm #select_products").on("click", "#new_link_product_frm #select_products", function (e) {
    e.preventDefault();
    $(".productselect_link_pay").each(function () {
        this.checked = true
    })
});
$(document).off("click", "#new_link_product_frm #deselect_products").on("click", "#new_link_product_frm #deselect_products", function (e) {
    e.preventDefault();
    $(".productselect_link_pay").each(function () {
        this.checked = false
    })
});
$(document).off("click", "#new_link_product_frm #link_selected_product").on("click", "#new_link_product_frm #link_selected_product", function (e) {
    e.preventDefault();
    var form = $("#new_link_product_frm");
    $(".productselect_link_pay").each(function () {
        if ($("#new_link_product_frm input:checkbox:checked").length == 0) {
            $(".notice_error").html("Please select at least 1 product!");
            $(".notice_error").show().delay(5e3).fadeOut()
        }
    });
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                showPopup("Are You Sure?", data, "info")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#complete_new_paymentdetails").on("click", "#complete_new_paymentdetails", function (e) {
    e.preventDefault();
    reload_manage_payment();
    closePanel()
});
$(document).on("panelShown", function (e) {
    currentPanel = $(e.target);
    animateDataLines();
    if ($("#connectivity_account_details").length > 0 || $("#connectivity_line_account").length > 0 || $("#shared_hosting_panel").length > 0) {
        if ($("#connectivity_account_details #device_delivery_panel").length > 0) {
            return false
        }
        if ($("#connectivity_account_details #provisioning_status_panel").length > 0) {
            return false
        }
        var yVals = JSON.parse($(".datagraph").attr("data-yvals"));
        var xVals = JSON.parse($(".datagraph").attr("data-xvals").replace(/\b0(\d)/g, "$1"));
        var maxYVal = getMaxOfArray(yVals);
        $(".graphholder").highcharts({
            chart: {
                type: "areaspline",
                marginLeft: 30,
                backgroundColor: null,
                plotBorderWidth: 1,
                plotBackgroundColor: {
                    linearGradient: [0, 0, 0, 150],
                    stops: [
                        [.5, "rgb(255, 255, 255)"],
                        [1, "rgb(221, 221, 221)"]
                    ]
                },
                events: {
                    load: function () {
                        this.plotBackground.attr({
                            rx: 15,
                            ry: 15
                        });
                        this.plotBorder.attr({
                            rx: 15,
                            ry: 15
                        });
                        $(".highcharts-series-group").prop({
                            transform: "translate(0,-8)"
                        })
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            title: {
                text: ""
            },
            subtitle: {
                text: ""
            },
            xAxis: {
                categories: xVals,
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: "transparent",
                minorTickLength: 0,
                min: 0,
                max: 13,
                tickLength: 0,
                labels: {
                    rotation: -90,
                    align: "right",
                    style: {
                        fontSize: "12px",
                        fontWeight: "bold",
                        fontFamily: "Sterling, sans-serif",
                        color: "#999"
                    },
                    x: 5,
                    y: 28
                }
            },
            yAxis: {
                title: {
                    text: ""
                },
                minPadding: .25,
                min: 0,
                max: maxYVal <= 0 ? 1 : maxYVal,
                labels: {
                    formatter: function () {
                        if (this.isFirst) {
                            return this.value
                        }
                        return bytesToSize(this.value, 0, true)
                    },
                    style: {
                        fontSize: "12px",
                        fontWeight: "bold",
                        fontFamily: "Sterling, sans-serif",
                        color: "#999"
                    },
                    align: "center",
                    x: -16,
                    y: 12
                },
                gridLineColor: "transparent"
            },
            tooltip: {
                enabled: false,
                shared: true,
                formatter: function () {
                    var tooltip = $("<div>");
                    var total = 0;
                    $.each(this.points, function (i, point) {
                        total += this.y;
                        tooltip.append($('<div style="color: ' + point.series.area.fill + ';">').html(point.series.name + ": " + bytesToSize(this.y, 2)));
                        if (!(i % 2)) {
                            tooltip.append("<br />")
                        }
                    });
                    tooltip.prepend($('<div style="color: #999;">').html("Total: " + bytesToSize(total, 2) + "<br />"));
                    return tooltip.get(0).outerHTML
                }
            },
            plotOptions: {
                areaspline: {
                    lineWidth: 1.3,
                    marker: {
                        states: {
                            hover: {
                                radius: 5
                            }
                        }
                    },
                    states: {
                        hover: {
                            lineWidth: 1.3
                        }
                    }
                }
            },
            series: [{
                name: "Download",
                fillColor: "rgba(0, 140, 194, 0.3)",
                lineColor: "#008cc2",
                data: yVals[0]
            }, {
                name: "Upload",
                fillColor: "rgba(117, 0, 198, 0.4)",
                lineColor: "#7500c6",
                marker: {
                    enabled: false,
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                },
                data: yVals[1]
            }]
        })
    }
});
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    var form_id = form.attr("id");
    var username = $("#" + form_id + " #conn_username").val();
    var type = $("#" + form_id + " #conn_type").val();
    if (username != undefined && type != undefined) {
        reload_account_detail(username, type);
        reload_my_connectivity();
        setTimeout(function () {
            show_whiteout($("#connectivity_account_details"))
        }, 500)
    }
    if (form_id == "cancel_client_order") {
        reload_my_connectivity()
    }
});

function animateDataLines() {
    $("#dataLinks").find(".usagebar .usageline").animate({
        width: "100%"
    }, 750);
    $("#dataLinks").find(".usagebar").find(".star").each(function () {
        $(this).animate({
            left: "100%"
        }, 750, function () { })
    });
    adjust_listholder()
}

function reload_account_detail(username, type) {
    var myAccountPanel = $("#connectivity_account_details.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-connectivity/account-detail/" + username + "~" + type;
    if (myAccountPanel.length > 0) {
        $("#connectivity_account_details").load(url + " #connectivity_account_details > *", function () {
            myAccountPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            });
            if ($("#panels .pathwindow").length - 1 > $("#panels .pathwindow").index(myAccountPanel)) {
                blackOutPannel(myAccountPanel)
            }
        })
    }
}

function reload_mobile_account_detail(ccp_id) {
    var myAccountPanel = $("#connectivity_account_details.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-connectivity/account-detail/mobile/" + ccp_id;
    if (myAccountPanel.length > 0) {
        $("#connectivity_account_details").load(url + " #connectivity_account_details > *", function () {
            myAccountPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_service(username, type) {
    var myServicePanel = $("#connectivity_services.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-connectivity/services/" + username + "~" + type;
    if (myServicePanel.length > 0) {
        $("#connectivity_services .pathwindow_content").load(url + " #connectivity_services .pathwindow_content > *", function () {
            myServicePanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_my_connectivity() {
    var myConnPanel = $("#my_connectivity_panel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-connectivity";
    if (myConnPanel.length > 0) {
        $("#my_connectivity_panel .pathwindow_content").load(url + " #my_connectivity_panel .pathwindow_content > *", function () {
            myConnPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_edit_package(username, type) {
    var myConnEditPanel = $("#connectivty_edit_package.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-connectivity/edit-package/" + username + "~" + type;
    if (myConnEditPanel.length > 0) {
        $("#connectivty_edit_package .pathwindow_content").load(url + " #connectivty_edit_package .pathwindow_content > *", function () {
            myConnEditPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_bundles(number) {
    var myBundlesPanel = $("#bundle_panel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-connectivity/bundles/" + number;
    if (myBundlesPanel.length > 0) {
        $("#bundle_panel .pathwindow_content").load(url + " #bundle_panel .pathwindow_content > *", function () {
            myBundlesPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_line_account(number) {
    var myConnLine = $("#connectivity_line_account.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-connectivity/line/" + number;
    if (myConnLine.length > 0) {
        $("#connectivity_line_account .pathwindow_content").load(url + " #connectivity_line_account .pathwindow_content > *", function () {
            myConnLine.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_zero_steel(zeroid) {
    var MyStealPanel = $("#apn_zero_steel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/zero-rand-steal/" + zeroid;
    if (MyStealPanel.length > 0) {
        $("#apn_zero_steel").load(url + " #apn_zero_steel .pathwindow_content > *", function () {
            MyStealPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_fax_account() {
    var myFaxPanel = $("#fax_content_panel .pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/other/fax2email";
    if (myFaxPanel.length > 0) {
        $("#fax_content_panel .pathwindow_content").load(url + " #fax_content_panel .pathwindow_content > *", function () {
            myFaxPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_manage_payment() {
    var myPaymentPanel = $("#manage_payment_details .pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-billing/manage-payment-details";
    if (myPaymentPanel.length > 0) {
        $("#fax_content_panel .pathwindow_content").load(url + " #manage_payment_details .pathwindow_content > *", function () {
            myPaymentPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function bytesToSize(bytes, precision, AddLineBreaks) {
    var kilobyte = 1024;
    var megabyte = kilobyte * 1024;
    var gigabyte = megabyte * 1024;
    var terabyte = gigabyte * 1024;
    var lineBreaks = AddLineBreaks == true ? "<br />" : "";
    if (bytes >= 0 && bytes < kilobyte) {
        return bytes + lineBreaks + " B"
    } else if (bytes >= kilobyte && bytes < megabyte) {
        return (bytes / kilobyte).toFixed(precision) + lineBreaks + " KB"
    } else if (bytes >= megabyte && bytes < gigabyte) {
        return (bytes / megabyte).toFixed(precision) + lineBreaks + " MB"
    } else if (bytes >= gigabyte && bytes < terabyte) {
        return (bytes / gigabyte).toFixed(precision) + lineBreaks + " GB"
    } else if (bytes >= terabyte) {
        return (bytes / terabyte).toFixed(precision) + lineBreaks + " TB"
    } else {
        return bytes + lineBreaks + " B"
    }
}

function getMaxOfArray(numArray) {
    if (!Array.isArray(numArray)) {
        return false
    }
    if (Array.isArray(numArray[0])) {
        numArray = numArray.reduce(function (a, b) {
            return a.concat(b)
        })
    }
    return Math.max.apply(null, numArray)
}
$(document).off("keyup", "#connectivity_search_input").on("keyup", "#connectivity_search_input", function (e) {
    var pattern = new RegExp($(this).val(), "i");
    $("#my_connectivity_panel .searchable a").each(function () {
        packageText = $(this).find("dt").first().html();
        usernameText = $(this).find("dd").first().html();
        nospaceText = packageText.split(" ").join("");
        if (pattern.test(usernameText) || pattern.test(packageText) || pattern.test(nospaceText)) {
            $(this).fadeIn()
        } else {
            $(this).fadeOut()
        }
    })
});
$(document).off("click", "#rand_steal_other_form #activate_other_number_btn").on("click", "#rand_steal_other_form #activate_other_number_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    var number = $("#rand_steal_other_form #new_number").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_zero_steel();
                setTimeout(function () {
                    showPopup("Thank you!", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#activate_shaping_form #activate_shaping_btn").on("click", "#activate_shaping_form #activate_shaping_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    var username = $("#activate_shaping_form #client_conn_username").val();
    var type = $("#activate_shaping_form #client_conn_type").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_account_detail(username, type);
                setTimeout(function () {
                    showPopup("Thank you!", data, "warning")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#deactivate_shaping_form #deactivate_shaping_btn").on("click", "#deactivate_shaping_form #deactivate_shaping_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    var username = $("#deactivate_shaping_form #client_conn_username").val();
    var type = $("#deactivate_shaping_form #client_conn_type").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_account_detail(username, type);
                setTimeout(function () {
                    showPopup("Thank you!", data, "warning")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("change", "#custom_name_select").on("change", "#custom_name_select", function () {
    if ($("#custom_name_select").val() == 1) {
        $("#custom_name_input").show();
        $("#custom_name_value").addClass("required")
    } else {
        $("#custom_name_input").hide();
        $("#custom_name_value").removeClass("required")
    }
});
$(document).off("mouseover", ".pathwindow_title.tooltip").on("mouseover", ".pathwindow_title.tooltip", function () {
    $(".pathwindow_title_tooltip").stop().fadeIn(500)
});
$(document).off("mouseout", ".pathwindow_title.tooltip").on("mouseout", ".pathwindow_title.tooltip", function () {
    $(".pathwindow_title_tooltip").stop().fadeOut(500)
});
$(document).off("change", "#connectivity_dsl_signup #packageDrp").on("change", "#connectivity_dsl_signup #packageDrp", function (e) {
    e.preventDefault();
    var prCost = $(this).find(":selected").attr("data-pr-cost");
    var prData = $(this).find(":selected").attr("data-pr-data");
    var prDoubleData = $(this).find(":selected").prop("data-pr-dd");
    var optionText = $(this).find(":selected").text();
    var now = new Date;
    if (optionText.toLowerCase().indexOf("prepaid") >= 0) {
        $("#renewaldDateText").text("Expiry Date");
        var firstDayYear_non = new Date(now.getFullYear(), now.getMonth() + 12, now.getDate());
        var firstDayYear = $.datepicker.formatDate("yy-mm-dd", firstDayYear_non);
        $("#renewalDate").text(firstDayYear)
    } else {
        $("#renewaldDateText").text("Renewal Date");
        var firstDayPrevMonth_non = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        var firstDayPrevMonth = $.datepicker.formatDate("yy-mm-dd", firstDayPrevMonth_non);
        $("#renewalDate").text(firstDayPrevMonth)
    }
    if (prCost == "FREE") {
        $("#proRataCost").text(prCost);
        $("#freegbnumber").show()
    } else if (prCost == "") {
        $("#proRataCost").text("");
        $("#renewalDate").text("");
        $("#freegbnumber").hide()
    } else {
        $("#proRataCost").text("R" + prCost);
        $("#freegbnumber").hide()
    }
    $("#proRataData").text(prData);
    $("#dsl_signup_disclaimer").html("");
    if (parseInt(prDoubleData) == 1) {
        $("#dsl_signup_disclaimer").html("*Double Data promotion in effect")
    }
});
$(document).off("click", "#connectivity_dsl_signup #dslSignup").on("click", "#connectivity_dsl_signup #dslSignup", function (e) {
    e.preventDefault();
    var form = $("#dsl_signup_frm");
    var prCost = $("#connectivity_dsl_signup #packageDrp").find(":selected").attr("data-pr-cost");
    if ($("#connectivity_dsl_signup #packageDrp").val() == "") {
        form_field_animate_error($("#connectivity_dsl_signup #packageDrp"), "Please select a package");
        return false
    } else {
        form_field_clear_error($("#connectivity_dsl_signup #packageDrp"))
    }
    if ($("#connectivity_dsl_signup #dslusername").val() == "" || /[^a-zA-Z0-9]/.test($("#connectivity_dsl_signup #dslusername").val())) {
        form_field_animate_error($("#connectivity_dsl_signup #dslusername"), "Please select a valid username");
        return false
    } else {
        form_field_clear_error($("#connectivity_dsl_signup #dslusername"))
    }
    if ($("#connectivity_dsl_signup #realm").val() == "") {
        form_field_animate_error($("#connectivity_dsl_signup #realm"), "Please choose a realm");
        return false
    } else {
        form_field_clear_error($("#connectivity_dsl_signup #realm"))
    }
    if (prCost == "FREE") {
        var freecalling = $("#connectivity_dsl_signup #freegbcalling").val();
        if (freecalling == "" || freecalling.length != 10 || /[^0-9]/.test(freecalling)) {
            form_field_animate_error($("#connectivity_dsl_signup #freegbcalling"), "Please enter a landlline number for the free GB");
            return false
        } else {
            form_field_clear_error($("#connectivity_dsl_signup #freegbcalling"))
        }
    }
    if (!$("#connectivity_dsl_signup #terms").prop("checked")) {
        form_field_animate_error($("#connectivity_dsl_signup #terms"), "In order to purchase this package you must accept the terms");
        return false
    } else {
        form_field_clear_error($("#connectivity_dsl_signup #terms"))
    }
    if (!$("#connectivity_dsl_signup #policy").prop("checked")) {
        form_field_animate_error($("#connectivity_dsl_signup #policy"), "In order to purchase this you must accept the usage policy");
        return false
    } else {
        form_field_clear_error($("#connectivity_dsl_signup #policy"))
    }
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                closePanel();
                reload_my_connectivity();
                setTimeout(function () {
                    showPopup("Thank You!", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#connectivity_dsl_signup #terms").on("click", "#connectivity_dsl_signup #terms", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Terms accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to change your package you must accept the terms")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#connectivity_dsl_signup #policy").on("click", "#connectivity_dsl_signup #policy", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Policy accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to purchase this you must accept the usage policy")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#connectivity_mobile_signup #mobileSignup").on("click", "#connectivity_mobile_signup #mobileSignup", function (e) {
    e.preventDefault();
    var form = $("#dsl_signup_frm");
    if ($("#connectivity_mobile_signup #mobilePackage").val() == "") {
        form_field_animate_error($("#connectivity_mobile_signup #mobilePackage"), "Please select a package");
        return false
    } else {
        form_field_clear_error($("#connectivity_mobile_signup #mobilePackage"))
    }
    if ($("#connectivity_mobile_signup #simSelect").val() == "") {
        form_field_animate_error($("#connectivity_mobile_signup #simSelect"), "Please select a sim");
        return false
    } else {
        form_field_clear_error($("#connectivity_mobile_signup #simSelect"))
    }
    if ($("#connectivity_mobile_signup #simSelect").val() == "newSim") {
        if ($("#connectivity_mobile_signup #simSize").val() == "") {
            form_field_animate_error($("#connectivity_mobile_signup #simSize"), "Please select a sim type");
            return false
        } else {
            form_field_clear_error($("#connectivity_mobile_signup #simSize"))
        }
    }
    if ($("#connectivity_mobile_signup #simSelect").val() == "existingSim") {
        if ($("#connectivity_mobile_signup #phoneNumber").val() == "" || /[^0-9]/.test($("#connectivity_dsl_signup #phoneNumber").val())) {
            form_field_animate_error($("#connectivity_mobile_signup #phoneNumber"), "Please enter a phone number");
            return false
        } else {
            form_field_clear_error($("#connectivity_mobile_signup #phoneNumber"))
        }
    }
    if (!$("#connectivity_mobile_signup #terms").prop("checked")) {
        form_field_animate_error($("#connectivity_mobile_signup #terms"), "In order to purchase this package you must accept the terms");
        return false
    } else {
        form_field_clear_error($("#connectivity_mobile_signup #terms"))
    }
    if (!$("#connectivity_mobile_signup #policy").prop("checked")) {
        form_field_animate_error($("#connectivity_mobile_signup #policy"), "In order to purchase this you must accept the usage policy");
        return false
    } else {
        form_field_clear_error($("#connectivity_mobile_signup #policy"))
    }
});
$(document).off("click", "#connectivity_mobile_signup #terms").on("click", "#connectivity_mobile_signup #terms", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Terms accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to change your package you must accept the terms")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#connectivity_mobile_signup #policy").on("click", "#connectivity_mobile_signup #policy", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Policy accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to purchase this you must accept the usage policy")
    }
    e.stopImmediatePropagation()
});
$(document).off("change", "#connectivity_mobile_signup #simSelect").on("change", "#connectivity_mobile_signup #simSelect", function (e) {
    e.preventDefault();
    var selectedSim = $("#connectivity_mobile_signup #simSelect").val();
    if (selectedSim == "") {
        $("#connectivity_mobile_signup #existingSimDetails").hide();
        $("#connectivity_mobile_signup #newSimDetails").hide()
    }
    if (selectedSim == "newSim") {
        $("#connectivity_mobile_signup #newSimDetails").show();
        $("#connectivity_mobile_signup #existingSimDetails").hide()
    }
    if (selectedSim == "existingSim") {
        $("#connectivity_mobile_signup #existingSimDetails").show();
        $("#connectivity_mobile_signup #newSimDetails").hide()
    }
});
$(document).off("click", "#connectivity_notifications input").on("click", "#connectivity_notifications input", function (e) {
    var form = $(this).parents("form");
    var field = $(this);
    $.ajax({
        global: false,
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            if (data.code == 1) {
                form_field_animate_success(field, data.message)
            } else {
                form_field_animate_error(field, "An error occured when updating your settings")
            }
        }
    })
});
$(document).off("click", "#device_protect_choice_panel #submit_device_protect").on("click", "#device_protect_choice_panel #submit_device_protect", function (e) {
    if ($("#extra_sub_prod").length > 0) {
        $("#device_protect_choice_panel #error_msg").hide();
        if ($("#extra_sub_prod").val() == "none") {
            $("#device_protect_choice_panel #error_msg").show();
            form_field_animate_error($("#device_protect_choice_panel #error_msg"), "");
            return false
        }
    }
    var form = $("#device_protect_choice_panel #device_protect_on_form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            setTimeout(function () {
                showPopup("Complete!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#device_protect_choice_off_panel #submit_device_protect_off").on("click", "#device_protect_choice_off_panel #submit_device_protect_off", function (e) {
    var form = $("#device_protect_choice_off_panel #device_protect_off_form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            setTimeout(function () {
                showPopup("Complete!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#device_protect_direct_choice_panel #submit_device_direct_protect").on("click", "#device_protect_direct_choice_panel #submit_device_direct_protect", function (e) {
    if ($("#extra_sub_prod").length > 0) {
        $("#device_protect_direct_choice_panel #error_msg").hide();
        if ($("#extra_sub_prod").val() == "none") {
            $("#device_protect_direct_choice_panel #error_msg").show();
            form_field_animate_error($("#device_protect_direct_choice_panel #error_msg"), "");
            return false
        }
    }
    var form = $("#device_protect_direct_choice_panel #device_protect_direct_on_form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            setTimeout(function () {
                showPopup("Complete!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#connectivty_edit_package #terms").on("click", "#connectivty_edit_package #terms", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Terms accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to change your package you must accept the terms")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#connectivty_edit_package #connectivity_change_package_btn").on("click", "#connectivty_edit_package #connectivity_change_package_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    if (!$("#connectivty_edit_package #terms").prop("checked")) {
        form_field_animate_error($("#connectivty_edit_package #terms"), "In order to change your package you must accept the terms");
        return false
    }
    var username = $("#connectivty_edit_package #conn_username").val();
    var type = $("#connectivty_edit_package #conn_type").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            reload_account_detail(username, type);
            reload_my_connectivity();
            setTimeout(function () {
                showPopup("Thank You!", data, "success")
            }, 500)
        }
    })
});
$(document).off("change keyup", "#connectivity_cancel_package_form #reason").on("change keyup", "#connectivity_cancel_package_form #reason", function (e) {
    if ($(this).val() == "Other") {
        $("#connectivity_cancel_package_form #other_holder").slideDown(500)
    } else {
        $("#connectivity_cancel_package_form #other_holder").slideUp(500)
    }
    return false
});
$(document).off("click", "#connectivity_cancel_package_form #cancel_connectivity_btn").on("click", "#connectivity_cancel_package_form #cancel_connectivity_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    validate = form.validate({
        messages: {
            reason: "Please Select a reason for cancelling",
            service_rating: "Please rate our service"
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    var username = $("#connectivity_cancel_package_form #conn_cancel_username").val();
    var type = $("#connectivity_cancel_package_form #conn_cancel_type").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                closePanel();
                reload_account_detail(username, type);
                reload_my_connectivity();
                setTimeout(function () {
                    showPopup("Sorry to see you leave!", data, "warning")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#connectivity_pause_package_form #pause_connectivity_btn").on("click", "#connectivity_pause_package_form #pause_connectivity_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    validate = form.validate({
        messages: {
            reason: "Please Select a reason for cancelling"
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    var username = $("#connectivty_edit_package #conn_pause_username").val();
    var type = $("#connectivty_edit_package #conn_pause_type").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                closePanel();
                reload_edit_package(username, type);
                setTimeout(function () {
                    showPopup("Sorry to see you leave!", data, "warning")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("change", "#connectivity_pause_package_form #pause_reason").on("change", "#connectivity_pause_package_form #pause_reason", function (e) {
    if ($(this).val() == "Other") {
        $("#connectivity_pause_package_form #other_holder").slideDown(500)
    } else {
        $("#connectivity_pause_package_form #other_holder").slideUp(500)
    }
    return false
});
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    if (form.attr("id") == "connectivity_cancel_package_form") {
        var username = $("#connectivity_cancel_package_form #conn_cancel_username").val();
        var type = $("#connectivity_cancel_package_form #conn_cancel_type").val();
        closePanel();
        reload_account_detail(username, type);
        reload_my_connectivity();
        setTimeout(function () {
            show_whiteout($("#connectivity_line_account"))
        }, 300)
    }
    if (form.attr("id") == "connectivity_pause_package_form") {
        var username = $("#connectivity_pause_package_form #conn_pause_username").val();
        var type = $("#connectivity_pause_package_form #conn_pause_type").val();
        closePanel();
        reload_account_detail(username, type);
        reload_edit_package(username, type);
        setTimeout(function () {
            show_whiteout($("#connectivty_edit_package"))
        }, 300)
    }
    if (form.attr("id") == "unpause_connectivity_package_form") {
        var username = $("#unpause_connectivity_package_form #conn_username").val();
        var type = $("#unpause_connectivity_package_form #conn_type").val();
        closePanel();
        reload_account_detail(username, type);
        reload_edit_package(username, type);
        setTimeout(function () {
            show_whiteout($("#connectivty_edit_package"))
        }, 300)
    }
});
$(document).on("popUpLoaded", function (e) {
    if ($("#id_document_file").is(":visible")) {
        $("input[type=file]").filestyle({
            image: "/bundles/ahuiCash Analyzer/img/elements/file_add.png",
            imageheight: 34,
            imagewidth: 34,
            width: "100%"
        })
    }
});
$(document).on("click", "#btn_SubmitVerificationDocument", function (e) {
    e.preventDefault();
    if ($("#id_document_file").val() == "") {
        form_field_animate_error($("#id_document_file"), "");
        return false
    } else {
        form_field_clear_error($("#id_document_file"))
    }
    if ($("#certify_checkbox").prop("checked")) {
        var form = $("#id_verification_upload");
        var formData = new FormData(form[0]);
        var url = form.attr("action");
        $.ajax({
            url: url,
            type: "POST",
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            closePopup();
            showPopup("Success!", "<p>" + data.message + "</p>", "success")
        }).fail(function (data) {
            showPopup("Oops!", "<p>" + data.message + "</p>", "warning")
        })
    } else {
        form_field_animate_error($("#certify_checkbox"), "")
    }
    return false
});
$(document).on("click", "#certify_checkbox", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "")
    } else {
        form_field_animate_error($(this), "")
    }
});
$(document).off("focusout", "#manage_email #email_forwarder").on("focusout", "#manage_email #email_forwarder", function () {
    var field = $(this);
    var form = field.parents("form");
    var currentVal = field.attr("rel");
    var current_forwarder = form.find(".current_forwarder");
    var valid = form.validate({
        showErrors: function (errorMap, errorList) {
            if (errorList[0]) {
                $(errorList[0]["element"]).data("error", errorList[0]["message"])
            }
        }
    }).element(field);
    if (!valid) {
        form_field_animate_error(field, field.data("error"));
        return false
    }
    if (valid) {
        form_field_clear_error(field)
    }
    if (valid && currentVal == field.val()) {
        form_field_clear_error(field);
        return false
    }
    if (valid && currentVal != field.val()) {
        $.ajax({
            global: false,
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            beforeSend: function (xhr) { },
            success: function (data) {
                $.ajaxSetup({
                    global: true
                });
                if (data.code == 1) {
                    field.attr("rel", data.forwarder);
                    current_forwarder.val(data.forwarder);
                    form_field_animate_success(field, data.message)
                } else {
                    form_field_animate_error(field, data.message)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                form_field_animate_error(field, "An Error Occured when trying to update your details")
            }
        })
    }
    return false
});
$(document).off("focusout", "#manage_email #mailbox_aliases :input").on("focusout", "#manage_email #mailbox_aliases :input", function () {
    var form = $(this).parents("form");
    var current_alias_input = form.find(".current_alias");
    var alias_input = form.find(".alias_input");
    if ($(this).val() == $(this).prop("rel")) {
        return false
    }
    $.ajax({
        global: false,
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        beforeSend: function (xhr) { },
        success: function (data) {
            $.ajaxSetup({
                global: true
            });
            if (data.code == 1) {
                alias_input.val(data.alias);
                alias_input.attr("rel", data.alias);
                current_alias_input.val(data.alias);
                form_field_animate_success(alias_input, data.message);
                var currentAliases = $("#manage_email #alias_halo").prop("rel");
                $("#manage_email #alias_halo").prop("rel", data.alias_count / 5 * 100);
                loadHalo($("#manage_email #alias_halo"), currentAliases);
                $(".num_alias").html(data.alias_count)
            } else {
                if (data.code == 407) {
                    form_field_animate_error(alias_input, data.message)
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            form_field_animate_error(alias_input, "An Error Occured when trying to update your details")
        }
    });
    return false
});
(function ($) {
    $.fn.filestyle = function (options) {
        var settings = {
            width: 250
        };
        if (options) {
            $.extend(settings, options)
        }
        return this.each(function () {
            var self = this;
            var wrapper = $("<div>").css({
                width: settings.imagewidth + "px",
                height: settings.imageheight + "px",
                background: "url(" + settings.image + ") 0 0 no-repeat",
                "background-position": "right",
                display: "block",
                position: "absolute",
                overflow: "hidden",
                "z-index": "10",
                right: "-1px",
                top: "0"
            });
            var filename = $('<input class="file" disabled="disabled">').addClass($(self).prop("class")).css({
                display: "inline",
                width: settings.width + "px"
            });
            $(self).before(filename);
            $(self).wrap(wrapper);
            $(self).css({
                position: "absolute",
                height: settings.imageheight + "px",
                width: settings.width + "px",
                display: "inline",
                cursor: "pointer",
                opacity: "0.0"
            });
            if ($.browser.mozilla) {
                if (/Win/.test(navigator.platform)) {
                    $(self).css("margin-left", "-142px")
                } else {
                    $(self).css("margin-left", "-168px")
                }
            } else {
                $(self).css("margin-left", settings.imagewidth - settings.width + "px")
            }
            $(self).bind("change", function () {
                filename.val($(self).val())
            })
        })
    }
})(jQuery);
$(document).off("click", "#activate_b_static_ip_form .activate_b_static_ip_btn").on("click", "#activate_b_static_ip_form .activate_b_static_ip_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    var username = $("#activate_b_static_ip_form #static_ip_b_username").val();
    var type = $("#activate_b_static_ip_form #static_ip_b_type").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_account_detail(username, type);
                reload_service(username, type);
                setTimeout(function () {
                    showPopup("Thank you!", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#deactivate_b_static_ip_form .deactivate_b_static_ip_btn").on("click", "#deactivate_b_static_ip_form .deactivate_b_static_ip_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    var username = $("#deactivate_b_static_ip_form #static_ip_b_username").val();
    var type = $("#deactivate_b_static_ip_form #static_ip_b_type").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_account_detail(username, type);
                reload_service(username, type);
                setTimeout(function () {
                    showPopup("Thank you!", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    if (form.attr("id") == "remove_static_ip_cancellation_form") {
        var username = $("#remove_static_ip_cancellation_form #conn_username").val();
        var type = $("#remove_static_ip_cancellation_form #conn_type").val();
        reload_account_detail(username, type);
        setTimeout(function () {
            show_whiteout($("#connectivity_account_details"))
        }, 300)
    }
    if (form.attr("id") == "deactivate_b_static_ip_form") {
        var username = $("#deactivate_b_static_ip_form #conn_username").val();
        var type = $("#deactivate_b_static_ip_form #conn_type").val();
        reload_account_detail(username, type);
        reload_service(username, type);
        setTimeout(function () {
            show_whiteout($("#connectivity_account_details"))
        }, 300)
    }
    if (form.attr("id") == "activate_b_static_ip") {
        var username = $("#activate_b_static_ip_form #conn_username").val();
        var type = $("#activate_b_static_ip_form #conn_type").val();
        reload_account_detail(username, type);
        reload_service(username, type);
        setTimeout(function () {
            show_whiteout($("#connectivity_account_details"))
        }, 300)
    }
});
$(document).off("click", "#topupData #topupCheck").on("click", "#topupData #topupCheck", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Terms accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to Topup you must accept the terms")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#topupData #enable_auto_topups").on("click", "#topupData #enable_auto_topups", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#topupData #auto_topup_account_btn").on("click", "#topupData #auto_topup_account_btn", function () {
    var form = $(this).prev("form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            showPopup("Auto Topup", data, "success")
        }
    });
    return false
});
$(document).off("click", "#topupData #auto_topup_disable_btn").on("click", "#topupData #auto_topup_disable_btn", function () {
    var form = $(this).prev("form");
    if ($("#update_auto_topup #enable_auto_topups").prop("checked")) {
        form_field_animate_error($("#update_auto_topup #enable_auto_topups"), "Please uncheck 'Enable Auto Topups' checkbox!");
        return false
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            showPopup("Auto Topup", data, "success")
        }
    });
    return false
});
$(document).off("click", "#topupData #topup_account_btn").on("click", "#topupData #topup_account_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    if (!$("#topupData #topupCheck").prop("checked")) {
        form_field_animate_error($("#topupData #topupCheck"), "In order to Topup you must accept the terms");
        return false
    }
    var username = $("#topupData #conn_username").val();
    var type = $("#topupData #conn_type").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            reload_account_detail(username, type);
            reload_my_connectivity();
            setTimeout(function () {
                showPopup("Data Topup", data, "success")
            }, 500)
        }
    });
    return false
});
var turboHalo;
var turboTotalMinutes;
var turboSeconds = -1;
var timerHandler;
var adslUsername;
var adslType;
$(document).on("panelShown", function (e) {
    if ($("#turboHalo").is(":visible")) {
        activateTurboTimer();
        return
    }
});
$(document).off("click", "#turbocharge #topupCheck").on("click", "#turbocharge #topupCheck", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Terms accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to Topup you must accept the terms")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#turbocharge #btn_turbocharge_topup").on("click", "#turbocharge #btn_turbocharge_topup", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    var username = $("#turbocharge #conn_username").val();
    var type = $("#turbocharge #conn_type").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_turbocharge(username, type);
            showPopup("Turbo Topup", data, "success")
        }
    });
    return false
});
$(document).off("click", "#turbocharge #btn_turbocharge_activate").on("click", "#turbocharge #btn_turbocharge_activate", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    var username = $("#turbocharge #conn_username").val();
    var type = $("#turbocharge #conn_type").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            showPopup("Turbocharge", data, "success");
            reload_account_detail(username, type);
            closePanel()
        }
    });
    return false
});
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    if (form.attr("id") == "topup_turbocharge") {
        var username = $("#topup_turbocharge #conn_username").val();
        var type = $("#topup_turbocharge #conn_type").val();
        reload_turbocharge(username, type)
    }
});

function reload_turbocharge(username, type) {
    var turbochargePanel = $("#turbocharge.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-connectivity/turbocharge/" + username + "~" + type;
    if (turbochargePanel.length > 0) {
        $("#turbocharge").load(url + " #turbocharge > *", function () {
            turbochargePanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function activateTurboTimer() {
    turboHalo = drawTurboHalo($("#turboHalo"));
    var minutes = $("#minutesLeft").val();
    var totalMinutes = $("#totalMinutes").val();
    adslUsername = $("#adslUsername").val();
    adslType = $("#adslType").val();
    turboSeconds = eval(parseInt(minutes) + "*60");
    turboTotalMinutes = totalMinutes;
    timerHandler = setInterval(function () {
        updateTurboTimer()
    }, 1e3)
}

function updateTurboTimer() {
    if (!$("#turboHalo").is(":visible")) {
        clearInterval(timerHandler);
        return
    }
    turboSeconds = turboSeconds - 1;
    if (turboSeconds >= 0) {
        var mins = Math.ceil(turboSeconds / 60);
        var hours = Math.floor(turboSeconds / 3600);
        var percentage = Math.round(mins / turboTotalMinutes * 100);
        var paddedMinutes = ("0" + mins % 60).slice(-2);
        var newHTML = hours + ":" + paddedMinutes + "<span class='descriptor'>TURBO LEFT</span>";
        if ($("#turboHalo p").html() != newHTML) {
            $("#turboHalo p").html(newHTML)
        }
        if (percentage < parseInt($("#turboHalo").prop("rel"))) {
            updateTurboHalo(percentage)
        }
    }
    if (turboSeconds < 0) {
        clearInterval(timerHandler);
        reload_account_detail(adslUsername, adslType)
    }
}

function drawTurboHalo(thisHalo, initVal) {
    initVal = typeof initVal !== "undefined" ? initVal : 0;
    var halo_id = thisHalo.attr("id");
    var halo_val = parseInt(thisHalo.attr("rel"));
    var halo_size = parseInt(thisHalo.outerWidth());
    var halo_stroke = 6;
    var halo_rad = halo_size / 2 - halo_stroke;
    var halo_col = thisHalo.css("color");
    var halo_shadow = "#00C5FF";
    thisHalo.children("svg").remove();
    var halos = Raphael(halo_id, 150, 150),
        radius = halo_rad,
        param = {
            stroke: "#fff",
            "stroke-width": halo_stroke
        };
    halos.customAttributes.arc = function (value, total, radius) {
        var alpha = 360 / total * value,
            a = (90 - alpha) * Math.PI / 180,
            xloc = 50,
            yloc = 50,
            x = xloc + radius * Math.cos(a),
            y = yloc - radius * Math.sin(a),
            path, isChrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
        if ($.support.boxSizingReliable) {
            xloc = 50, yloc = 50;
            y = yloc - radius * Math.sin(a);
            x = xloc + radius * Math.cos(a)
        }
        if (total == value) {
            path = [
                ["M", xloc, yloc - radius],
                ["A", radius, radius, 0, 1, 1, xloc - .01, yloc - radius]
            ]
        } else {
            path = [
                ["M", xloc, yloc - radius],
                ["A", radius, radius, 0, +(alpha > 180), 1, x, y]
            ]
        }
        return {
            path: path,
            stroke: halo_col
        }
    };
    var filledhalo = halos.path().attr(param).attr({
        arc: [initVal, 100, radius]
    });

    function updateVal(value, total, radius, obj, id) {
        obj.animate({
            arc: [halo_val, 100, halo_rad]
        }, 750, ">")
    } (function () {
        updateVal(halo_val, 100, halo_rad, filledhalo)
    })();
    return filledhalo
}

function updateTurboHalo(value) {
    var halo_val = parseInt($("#turboHalo").attr("rel"));
    var halo_size = parseInt($("#turboHalo").outerWidth());
    var halo_stroke = 6;
    var halo_rad = halo_size / 2 - halo_stroke;
    var halo_shadow = "#00C5FF";
    var easing = ">";
    if (value < halo_val) {
        easing = "<"
    }
    turboHalo.animate({
        arc: [value, 100, halo_rad]
    }, 750, easing)
}
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    if (form.attr("id") == "disable_roaming") {
        username = form.find("#conn_username").val();
        type = form.find("#conn_type").val();
        reload_account_detail(username, type);
        reload_service(username, type)
    }
    if (form.attr("id") == "enable_wifiroaming_form") {
        var username = $("#enable_wifiroaming_form #conn_username").val();
        var type = $("#enable_wifiroaming_form #conn_type").val();
        reload_account_detail(username, type);
        reload_service(username, type)
    }
});
$(document).off("click", "#enable_wifiroaming_form #enable_roaming_btn").on("click", "#enable_wifiroaming_form #enable_roaming_btn", function () {
    var form = $(this).prev("form");
    validate = form.validate({
        rules: {
            new_wifi_password: {
                required: true,
                minlength: 8
            },
            wifi_password_repeat: {
                equalTo: "#new_wifi_password"
            }
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    if (form.valid()) {
        var username = $("#enable_wifi_form #conn_username").val();
        var type = $("#enable_wifi_form #conn_type").val();
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                closePanel();
                reload_service(username, type);
                setTimeout(function () {
                    showPopup("Enable Roaming", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#enable_uncapped_wifi_form #enable_uncapped_roaming_btn").on("click", "#enable_uncapped_wifi_form #enable_uncapped_roaming_btn", function () {
    var form = $(this).prev("form");
    validate = form.validate({
        rules: {
            new_wifi_password: {
                required: true,
                minlength: 8
            },
            wifi_password_repeat: {
                equalTo: "#new_wifi_password"
            }
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    if (form.valid()) {
        var username = $("#enable_upcapped_roaming #conn_username").val();
        var type = $("#enable_upcapped_roaming #conn_type").val();
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                closePanel();
                reload_account_detail(username, type);
                reload_my_connectivity();
                setTimeout(function () {
                    showPopup("Enable Roaming", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#detail_wifi_form #update_details_roaming").on("click", "#detail_wifi_form #update_details_roaming", function () {
    var form = $(this).prev("form");
    validate = form.validate({
        rules: {
            new_wifi_password: {
                required: true,
                minlength: 8
            }
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    if (form.valid()) {
        var username = $("#detail_wifi_form #conn_username").val();
        var type = $("#detail_wifi_form #conn_type").val();
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                closePanel();
                reload_account_detail(username, type);
                reload_my_connectivity();
                setTimeout(function () {
                    showPopup("Enable Roaming", data, "success")
                }, 500)
            }
        })
    }
    return false
});
var connectivityOutOfBundle = {};
(function (connectivityOutOfBundle) {
    "use strict";
    var oob = connectivityOutOfBundle;
    var data = {
        mobileId: 0,
        oobOriginalAmount: 0,
        oobAmount: 0,
        enabled: null,
        rate: 0,
        maxRands: 0,
        minRands: 10,
        cellNumber: "",
        spentRands: 0,
        spentMB: 0
    };
    var slider;
    oob.initialize = function (info) {
        data.mobileId = info.mobileId;
        data.oobAmount = info.oobAmount;
        data.oobOriginalAmount = info.oobAmount;
        data.enabled = info.enabled;
        data.maxRands = info.maxRands;
        data.rate = info.rate;
        data.accUsername = info.accUsername;
        data.accType = info.accType;
        data.cellNumber = info.cellNumber;
        data.spentRands = info.spentRands;
        data.spentMB = info.spentMB;
        if (data.oobAmount < 10) {
            data.oobAmount = 10
        }
        oob.updateDisplay();
        $(document).off("click", ".btnSetOobAmount").on("click", ".btnSetOobAmount", function (e) {
            $(".btnSetOobAmount").removeClass("active");
            $(this).addClass("active");
            slider.slider("value", $(this).attr("data-set-oob-amount"));
            data.oobAmount = $(this).attr("data-set-oob-amount");
            oob.updateDisplay()
        });
        $(document).off("click", "#enableOob").on("click", "#enableOob", function (e) {
            e.preventDefault();
            oob.activeOOB()
        });
        $(document).off("click", "#btnOobEnableComplete").on("click", "#btnOobEnableComplete", function (e) {
            e.preventDefault();
            oob.closePopup(this)
        });
        $("#showEdit").hide();
        $(document).off("click", "#btnCancelUpdateOob").on("click", "#btnCancelUpdateOob", function (e) {
            e.preventDefault();
            oob.reset();
            $("#showEdit").hide();
            $("#showEnabled").show()
        });
        $(document).off("click", "#btnChangeOobAmount").on("click", "#btnChangeOobAmount", function (e) {
            e.preventDefault();
            $("#showEdit").show();
            $("#showEnabled").hide()
        });
        $(document).off("click", "#btnUpdateOob").on("click", "#btnUpdateOob", function (e) {
            e.preventDefault();
            oob.changeAmountOOB()
        });
        $(document).off("click", "#btnCheckUpdateOobAmount").on("click", "#btnCheckUpdateOobAmount", function (e) {
            e.preventDefault();
            oob.changeAmountOOB(true)
        });
        $(document).off("click", "#btnOobUpdateAmountComplete").on("click", "#btnOobUpdateAmountComplete", function (e) {
            e.preventDefault();
            oob.closePopup(this)
        });
        $(document).off("click", "#btnDisableOob").on("click", "#btnDisableOob", function (e) {
            e.preventDefault();
            var now = moment();
            var date = moment().date(25);
            if (date.isBefore(now)) {
                date = now.add("month", 1).date(25)
            }
            oob.addWhiteOut();
            showPopup("Disable Out-of-Bundle", "<h1>Are You Sure?</h1>" + "<p>" + "Are you sure you want to disable Out-of-Bundle for<br>" + " the SIM Card with number <strong>" + data.cellNumber + "</strong>?" + "</p>" + "<p>" + "<strong>Please Note:</strong> Any outstanding Out-of-Bundle data" + "<br>charges will be charged on " + date.format("D MMMM YYYY") + "." + "</p>" + '<div class="popwindow_buttons">' + '<a class="btn_styled preferred" id="btnRealyDisableOob">Yes, Disable Out-of-Bundle</a>' + '<a class="btn_styled btn_close neverMind" >Nevermind</a>' + '<div class="clear"></div>', "warning")
        });
        oob.closePopup = function (object) {
            closePopup($(object).closest(".popwindow").attr("id").replace("popup", ""));
            window.setTimeout(function () {
                oob.removeWhiteOut()
            }, 1e3)
        };
        oob.addWhiteOut = function () {
            var currentPanel = $("#panels .pathwindow:last");
            if (currentPanel.find(".pathwindow_whiteout:first").length === 0) {
                show_whiteout(currentPanel)
            }
        };
        oob.removeWhiteOut = function () {
            hide_whiteout()
        };
        $(document).off("click", ".neverMind").on("click", ".neverMind", function (e) {
            e.preventDefault();
            oob.closePopup(this)
        });
        $(document).off("click", "#btnRealyDisableOob").on("click", "#btnRealyDisableOob", function (e) {
            e.preventDefault();
            oob.deactivateOOB();
            oob.closePopup(this)
        });
        slider = $("#dataslider .slider").slider({
            range: "max",
            animate: "fast",
            step: 1,
            min: data.minRands,
            max: data.maxRands,
            value: data.oobAmount,
            slide: function (event, ui) {
                data.oobAmount = ui.value;
                $(".btnSetOobAmount").removeClass("active");
                oob.updateDisplay()
            }
        })
    };
    oob.reload_account_detail = function () {
        var myAccountPanel = $("#outOfBundlebPannel");
        $.ajaxSetup({
            global: false
        });
        var url = "/en/my-connectivity/account-detail_oob/mobile/" + data.mobileId;
        if (myAccountPanel.length > 0) {
            myAccountPanel.load(url, {
                update: false
            }, function () {
                myAccountPanel.trigger("panelShown");
                $.ajaxSetup({
                    global: true
                });
                setTimeout(oob.addWhiteOut, 0)
            })
        }
    };
    var send = function (postData, callback) {
        postData.update = true;
        $.ajax({
            type: "GET",
            url: "/en/api/my-connectivity/change_account-detail_oob/mobile/" + data.mobileId,
            data: postData
        }).done(function (data) {
            callback(data);
            if ($("#connectivity_account_details").length > 0) {
                var username = $("#connectivity_account_details").attr("data-username");
                var type = $("#connectivity_account_details").attr("data-type");
                reload_account_detail(username, type)
            }
        })
    };
    oob.deactivateOOB = function () {
        send({
            action: "deactivate"
        }, oob.reload_account_detail)
    };
    oob.activeOOB = function () {
        send({
            action: "activate",
            amount: data.oobAmount
        }, function (dataReturned) {
            oob.addWhiteOut();
            oob.reload_account_detail();
            showPopup("Out-of-Bundle Enabled", "<h1>Out-of-Bundle Has Been Enabled!</h1>" + "<p>" + "Out-of-Bundle has been enabled on " + data.cellNumber + " with" + "<br> a maximum of <strong>R" + data.oobAmount + ".00 (" + calculateBandwidthGB(data.oobAmount) + "GB)</strong>. If you wish to edit the" + "<br> maximum usage, you can do so here in Cash Analyzer." + "</p>" + '<div class="popwindow_buttons">' + '<a class="btn_styled" id="btnOobEnableComplete">Done</a>' + '<div class="clear"></div>' + "</div>", "success")
        })
    };
    oob.changeAmountOOB = function (check) {
        if (data.spentRands > 0 && data.spentRands > data.oobAmount && null == check) {
            showPopup("Change Out-of-Bundle to less than current spend", "<h1>Are You Sure?</h1>" + "<p>" + "You have already spent " + "R" + data.spentRands + "<br> this month which is more than new amount (R" + data.oobAmount + ")" + "<br>  that you have selected." + "</p>" + "<p>" + "This change will take effect from next month and you will " + "<br> be billed R" + data.spentRands + " for the current usage at the end of this month." + "</p>" + '<div class="popwindow_buttons">' + '<a class="btn_styled preferred" id="btnCheckUpdateOobAmount">Proceed</a>' + '<a class="btn_styled btn_close neverMind" >Cancel</a>' + '<div class="clear"></div>', "warning")
        } else {
            send({
                action: "update",
                amount: data.oobAmount
            }, function (dataReturned) {
                oob.reload_account_detail();
                showPopup("Maximum Usage Updated", "<h1>Max Usage Updated Successfully!</h1>" + "<p>" + "Your Out-of-Bundle maximum has been updated to " + "<br><strong>R" + data.oobAmount + ".00 (" + calculateBandwidthGB(data.oobAmount) + "GB)</strong>. If you have any queries about adjusting your" + '<br> maximum usage, please email <a href="mailto:support@WFP Jordan.com">support@WFP Jordan.com</a>' + "</p>" + "<p>" + "<strong>Please Note:</strong> The maximum usage on Out-of-Bundle " + "<br>will only take effect in the new month when decreased.</p>" + '<div class="popwindow_buttons">' + '<a class="btn_styled" id="btnOobUpdateAmountComplete">Okay</a>' + '<div class="clear"></div>' + "</div>", "success")
            })
        }
    };
    oob.updateDisplay = function () {
        $("#oobAmount").html("Maximum Out-of-Bundle usage: <span >R" + data.oobAmount + "</span> (" + filterBandwidth(data.oobAmount) + ")");
        if ($("#megAmount").length > 0) {
            $("#megAmount").html(calculateBandwidth(data.oobAmount) + "MB")
        }
    };
    var calculateBandwidth = function (rand) {
        if ($.isNumeric(rand) && rand >= 0 && rand <= data.maxRands) {
            return Math.floor(rand * 100 / data.rate) / 100
        } else {
            return 0
        }
    };
    var calculateBandwidthGB = function (rand) {
        return Math.round(calculateBandwidth(rand) * 10 / 1024) / 10
    };
    var filterBandwidth = function (rand) {
        var bandwidth = calculateBandwidth(rand);
        if (bandwidth > 900) {
            return (Math.round(calculateBandwidth(rand) * 100 / 1024) / 100).toString() + "GB"
        }
        return bandwidth + "MB"
    };
    var calculateRands = function (bandwidth) {
        if ($.isNumeric(bandwidth) && bandwidth >= 0 && bandwidth <= data.maxRands / data.rate) {
            return Math.floor(bandwidth * data.rate)
        } else {
            return 0
        }
    };
    oob.reset = function () {
        data.oobAmount = data.oobOriginalAmount;
        oob.updateDisplay()
    }
})(connectivityOutOfBundle);
$(document).off("click", "#network_issue_form #network_issue_submit_button").on("click", "#network_issue_form #network_issue_submit_button", function (e) {
    e.preventDefault();
    var form = $("#network_issue_form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            setTimeout(function () {
                showPopup("Issue Submitted!", data, "success")
            }, 500)
        }
    })
});
$(document).off("click", "#bundle_option_change .act_bundle_decision").on("click", "#bundle_option_change .act_bundle_decision", function () {
    var form = $(this).parent("form");
    var number = form.find(".number").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_my_connectivity();
            reload_line_account(number);
            closePanel();
            setTimeout(function () {
                showPopup(data.heading, data.message, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#bundle_option_change .act_no_thanks").on("click", "#bundle_option_change .act_no_thanks", function () {
    closePanel();
    return false
});
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    if (form.attr("id") == "bundle_reminder_form") {
        reload_my_connectivity()
    }
});
$(document).off("change keyup", "#line_cancel_package_form #reasonForLeaving").on("change keyup", "#line_cancel_package_form #reasonForLeaving", function (e) {
    if ($(this).val() == "Other") {
        $("#line_cancel_package_form #other_holder").slideDown(500)
    } else {
        $("#line_cancel_package_form #other_holder").slideUp(500)
    }
    return false
});
$(document).off("change keyup", "#line_cancel_package_form #lineCancelType").on("change keyup", "#line_cancel_package_form #lineCancelType", function (e) {
    var jpopup = $(".popwindow");
    if ($(this).val() == 9) {
        $("#non_relocate").slideUp("slow", function () {
            $("#relocate").slideDown("fast", function () {
                var newtop = parseInt(jpopup.outerHeight()) / 2;
                var newleft = parseInt(jpopup.width()) / 2;
                jpopup.css("marginLeft", -newleft);
                jpopup.css("marginTop", -newtop);
                $("#line_cancel_package_form #line_migratedate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "d MM yy",
                    minDate: 1,
                    maxDate: "+1M +10D"
                })
            })
        })
    } else {
        $("#relocate").slideUp("slow", function () {
            $("#non_relocate").slideDown("fast", function () {
                var newtop = parseInt(jpopup.outerHeight()) / 2;
                var newleft = parseInt(jpopup.width()) / 2;
                jpopup.css("marginLeft", -newleft);
                jpopup.css("marginTop", -newtop)
            })
        })
    }
});
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    if (form.attr("id") == "line_cancel_package_form" || form.attr("id") == "reactivate_line") {
        var number = form.find("#conn_line_number").val();
        reload_line_account(number);
        reload_my_connectivity();
        setTimeout(function () {
            show_whiteout($("#connectivity_line_account"))
        }, 300)
    }
});
$(document).off("click", "#linePackageChange #terms").on("click", "#linePackageChange #terms", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Terms accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to change your package you must accept the terms")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#linePackageChange #line_change_my_package_btn").on("click", "#linePackageChange #line_change_my_package_btn", function () {
    if (!$("#linePackageChange #terms").prop("checked")) {
        form_field_animate_error($("#linePackageChange #terms"), "In order to change your package you must accept the terms");
        return false
    }
    var form = $("#line_change_package_frm");
    var number = $("#linePackageChange").find("#conn_line_number").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            reload_line_account(number);
            setTimeout(function () {
                showPopup("Thank You!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#test_line_submit #test_line_btn").on("click", "#test_line_submit #test_line_btn", function (e) {
    e.preventDefault();
    $("#test_line_submit").fadeOut("500", function () {
        $("#test_line_loading").fadeIn("500")
    });
    form = $("#test_conn_line_form");
    $.ajax({
        url: form.attr("action"),
        global: false,
        data: form.serialize(),
        type: "POST",
        error: function (info) {
            showPopup("Test Line", '<h1>The Line test failed</h1><p>Please try again. If you still have problems, please contact us.</p><div class="popwindow_buttons"><a class="btn_styled pop_close">OK</a><div class="clear"></div></div>', "warning");
            $("#test_line_loading").fadeOut("500", function () {
                $("#test_line_submit").fadeIn("500")
            })
        },
        success: function (data) {
            $.each(data.result.ATUC, function (index, element) {
                classc = "active";
                if (index == "SNR") {
                    if (parseInt(element) < 6) {
                        classc = "inactive"
                    }
                }
                $("#ATUC_" + index).children("span").addClass(classc);
                if ($("#ATUC_" + index).children("dd").length == 2) {
                    $("#ATUC_" + index).children("dd:first").remove()
                }
                $("#ATUC_" + index).children("dt").after("<dd><strong>" + element + "</strong></dd>")
            });
            $.each(data.result.ATUR, function (index, element) {
                classc = "active";
                if (index == "SNR") {
                    if (parseInt(element) < 6) {
                        classc = "inactive"
                    }
                }
                $("#ATUR_" + index).children("span").addClass(classc);
                if ($("#ATUR_" + index).children("dd").length == 2) {
                    $("#ATUR_" + index).children("dd:first").remove()
                }
                $("#ATUR_" + index).children("dt").after("<dd><strong>" + element + "</strong></dd>")
            });
            $("#test_line_loading").fadeOut("500", function () {
                $("#test_line_submit").fadeIn("500")
            })
        }
    });
    return false
});
$(document).off("click", "#trouble_shooter_log_fault").on("click", "#trouble_shooter_log_fault", function (e) {
    closePanel();
    e.preventDefault();
    var url = $(this).attr("href");
    _gaq.push(["_trackEvent", "Legacy Troubleshooter", "Legacy Troubleshooter Click", "trouble_shooter_log_fault"]);
    openPanel(url);
    return false
});
$(document).off("click", "#trouble_shooter_links .iconlink").on("click", "#trouble_shooter_links .iconlink", function (e) {
    var section = $(this).prop("id");
    if (!$(this).hasClass("selected")) {
        $(".iconlink").removeClass("selected");
        $(this).addClass("selected");
        $(".subnav.active").slideUp("fast", function () {
            $(this).removeClass("active");
            $(".subnav#" + section).slideDown().addClass("active")
        })
    }
});
$(document).off("click", "#trouble_shooter_links a").on("click", "#trouble_shooter_links a", function (e) {
    e.preventDefault();
    var viewToShow = $($(this).attr("href"));
    $("#trouble_shooter_links a").parent("li").removeClass("selected");
    $(this).parent("li").addClass("selected");
    if ($(this).hasClass("service")) {
        $("#service").addClass("selected")
    }
    if ($(this).hasClass("dropouts")) {
        $("#dropouts").addClass("selected")
    }
    if ($(this).hasClass("speed")) {
        $("#speed").addClass("selected")
    }
    $("#trouble_shooter_links a").each(function (index, element) {
        $($(element).attr("href")).slideUp(500)
    });
    hideDivs();
    if ($(this).hasClass("iconlink")) {
        var section = $(this).prop("id");
        $("#" + section + "_holder").slideDown("500");
        $("#" + section + "_holder").show()
    }
    var gaLabel = "";
    if ($(this).is("[id]") && $(this).attr("href") == "#") {
        gaLabel = $(this).attr("id");
        _gaq.push(["_trackEvent", "Legacy Troubleshooter", "Legacy Troubleshooter Click", gaLabel])
    } else if ($(this).is("[href]")) {
        gaLabel = $(this).attr("href").replace("#", "").replace("_holder", "");
        _gaq.push(["_trackEvent", "Legacy Troubleshooter", "Legacy Troubleshooter Click", gaLabel])
    }
    viewToShow.slideDown("500")
});
$(document).off("click", "#trouble_shooter_sub_links a").on("click", "#trouble_shooter_sub_links a", function (e) {
    e.preventDefault();
    var viewToShow = $($(this).attr("href"));
    if ($(this).hasClass("service")) {
        $("#service").addClass("selected")
    }
    if ($(this).hasClass("dropouts")) {
        $("#dropouts").addClass("selected")
    }
    if ($(this).hasClass("speed")) {
        $("#speed").addClass("selected")
    }
    if ($(this).hasClass("preferred")) {
        var current = $(".iconlink.selected").next();
        current.children("li").removeClass("selected");
        current.children("li:first").addClass("selected")
    } else {
        var current = $("#trouble_shooter_links a").parent("li.selected");
        current.removeClass("selected");
        current.next().addClass("selected")
    }
    $("#trouble_shooter_links a").each(function (index, element) {
        $($(element).attr("href")).slideUp(500)
    });
    hideDivs();
    if ($(this).hasClass("iconlink")) {
        var section = $(this).prop("id");
        $("#" + section + "_holder").slideDown("500");
        $("#" + section + "_holder").show()
    }
    viewToShow.slideDown("500");
    gaLabel = $(this).attr("href").replace("#", "").replace("_holder", "");
    _gaq.push(["_trackEvent", "Legacy Troubleshooter", "Legacy Troubleshooter Click", gaLabel])
});

function hideDivs() {
    $("#service_holder").hide();
    $("#dropouts_holder").hide();
    $("#speed_holder").hide()
}
$(document).off("click", "#tweak_my_line_panel .tweak").on("click", "#tweak_my_line_panel .tweak", function (e) {
    e.preventDefault();
    var urlto = $(this).attr("href");
    var message = $(this).prop("name");
    if ($(this).hasClass("act_warning")) {
        showPopup("Fix Your DSL Line", '<h1>Please be patient</h1><p>Performing line tweaks too often is not advisable, please wait at least 5 minutes till you try again.</p><div class="popwindow_buttons"><a class="btn_styled pop_close">Ok</a><div class="clear"></div></div>', "warning", "small");
        return false
    }
    showPopup("Fix Your DSL Line", '<div class="loading"><h1>Processing</h1><p>We are in the process of ' + message + ". <br />This usually takes approximately 3 minutes - please be patient</p></div>", "info", "small");
    $.ajax({
        url: urlto,
        global: false,
        error: function (info) {
            showPopup("Fix Your DSL Line", '<h1>Fix My Line Failed</h1><p>Please try again. If you continue to have problems, please contact <a href="mailto:support@WFP Jordan.com">support@WFP Jordan.com</a>.</p><div class="popwindow_buttons"><a class="btn_styled pop_close">Okay</a><div class="clear"></div></div>', "warning", "small")
        },
        success: function (data) {
            showPopup("Fix Your DSL Line", "<h1>" + data.result.heading + "</h1><p>" + data.result.message + '. <br /><br />If you continue to have problems, please contact <a href="mailto:support@WFP Jordan.com">support@WFP Jordan.com</a>.</p><div class="popwindow_buttons"><a class="btn_styled pop_close tweak_close_success">Okay</a><div class="clear"></div></div></div>', data.result.pclass, "small")
        }
    });
    return false
});
$(document).off("click", "#tweak_my_line_panel .tweak").on("click", "#tweak_my_line_panel .tweak", function (e) {
    e.preventDefault();
    var urlto = $(this).attr("href");
    if ($(this).hasClass("act_warning")) {
        showPopup("Tweak Your ADSL Line", '<h1>Please be patient</h1><p>Performing line tweaks too often is not advisable, please wait at least 5 minutes till you try again.</p><div class="popwindow_buttons"><a class="btn_styled pop_close">Ok</a><div class="clear"></div></div>', "warning");
        return false
    }
    $.ajax({
        url: urlto,
        global: false,
        error: function (info) {
            showPopup("Stabilise Your ADSL Line", "An error occurred please try again", "warning")
        },
        success: function (data) {
            showPopup("Stabilise Your ADSL Line", "<h1>" + data.result.heading + "</h1><p><strong>" + data.result.message + '</strong>. <br />Please wait until you have received an SMS alerting you know the process has been completed</p><div class="popwindow_buttons"><a class="btn_styled pop_close tweak_close_success">Okay</a><div class="clear"></div></div></div>', data.result.pclass)
        }
    });
    return false
});
$(document).off("click", "#tweak_my_line_panel .pre-tweak").on("click", "#tweak_my_line_panel .pre-tweak", function (e) {
    e.preventDefault();
    var urlto = $(this).attr("href");
    showPopup("Stabilise Your Line", '<div class="loading"><h1>Checking</h1><p>We are checking a few things with Telkom. <br />This can take a minute or 2 - please be patient</p></div>', "info");
    $.ajax({
        url: urlto,
        global: false,
        error: function (info) {
            showPopup("Tweak Your ADSL Line", '<h1>LIne Check Failed</h1><p>Please try again. If you still have issues, please contact us.</p><div class="popwindow_buttons"><a class="btn_styled pop_close">Okay</a><div class="clear"></div></div>', "warning")
        },
        success: function (data) {
            showPopup("Stabilise Your Telkom Line", data, "info")
        }
    });
    return false
});
$(document).off("click", ".tweak_close_success").on("click", ".tweak_close_success", function (e) {
    setTimeout(function () {
        closePanel()
    }, 500)
});
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    if (form.attr("id") == "reactivate_shared_hosting") {
        reload_hosting_detail($("#reactivate_shared_hosting #domain_name").val());
        reload_my_hosting();
        setTimeout(function () {
            show_whiteout($("#shared_hosting_panel"))
        }, 650)
    }
    if (form.attr("id") == "cancel_cloud") {
        var form = $("#cancel_cloud");
        form_field_clear_error($("#other_reason"));
        form_field_clear_error($("#transfer_recipient"));
        if ($("#reason").val() == "other" && $("#other_reason").val() == "") {
            form_field_animate_error($("#cancel_shared #check"), "Please let us know your reason");
            return false
        }
        if ($("#transfer_accept").val() == 1 && $("#transfer_recipient").val() == "") {
            form_field_animate_error($("#cancel_shared #transfer_recipient"), "Please provide transfer details");
            return false
        }
        if ($("#transfer_accept").val() != 0 && $("#transfer_accept").val() != 1 && $("#transfer_accept").val() != 2) {
            form_field_animate_error($("#cancel_shared #transfer_accept"));
            return false
        }
        var domain = $("#domainCloud").val();
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                closePanel();
                reload_hosting_detail(domain);
                reload_my_cloud(domain);
                setTimeout(function () {
                    showPopup("Thank You!", data, "success")
                }, 500)
            }
        });
        return false
    }
});
$(document).off("click", "#cancel_shared #do_cancel_shared").on("click", "#cancel_shared #do_cancel_shared", function (e) {
    e.preventDefault();
    var form = $("#cancelShared #cancel_shared");
    form_field_clear_error($("#other_reason"));
    form_field_clear_error($("#transfer_recipient"));
    if ($("#reason").val() == "other" && $("#other_reason").val() == "") {
        form_field_animate_error($("#cancel_shared #check"), "Please let us know your reason");
        return false
    }
    if ($("#transfer_accept").val() == 1 && $("#transfer_recipient").val() == "") {
        form_field_animate_error($("#cancel_shared #transfer_recipient"), "Please provide transfer details");
        return false
    }
    if ($("#transfer_accept").val() != 0 && $("#transfer_accept").val() != 1 && $("#transfer_accept").val() != 2) {
        form_field_animate_error($("#cancel_shared #transfer_accept"));
        return false
    }
    var domain = $("#editSharedForm #editSharedDomain").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            reload_hosting_detail(domain);
            reload_my_hosting();
            setTimeout(function () {
                showPopup("Thank You!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#cpanel_email_delete").on("click", "#cpanel_email_delete", function (e) {
    e.preventDefault();
    var username = $(this).prop("rel");
    $("#my_hosting_email_edit #" + username).remove();
    $("#my_hosting_email_edit #email_section_holder").html('<h1>Success</h1><p>The Email account has been deleted</p><div class="clear"></div>')
});
$(document).off("click", "#my_hosting_email_edit a.act_email_form_load").on("click", "#my_hosting_email_edit a.act_email_form_load", function (e) {
    e.preventDefault();
    if ($(this).hasClass("selected")) {
        return false
    }
    $("#my_hosting_email_edit a.act_email_form_load").removeClass("selected");
    $("#my_hosting_email_edit a.act_email_form_load dl span").removeClass("active");
    $("#my_hosting_email_edit #email_section_holder").load($(this).attr("href"));
    $(this).addClass("selected");
    $(this).find("dl span").addClass("active");
    return false
});
$(document).off("click", "#my_hosting_email_edit a.act_email_create_form_load").on("click", "#my_hosting_email_edit a.act_email_create_form_load", function (e) {
    e.preventDefault();
    $("#my_hosting_email_edit a.act_email_form_load").removeClass("selected");
    $("#my_hosting_email_edit a.act_email_form_load dl span").removeClass("active");
    $("#my_hosting_email_edit #email_section_holder").load($(this).attr("href"));
    return false
});
$(document).off("click", "#my_hosting_email_edit #create_email").on("click", "#my_hosting_email_edit #create_email", function (e) {
    e.preventDefault();
    var form = $("#my_hosting_email_edit #cpanel_mail_create_form");
    validate = form.validate({
        rules: {
            password: {
                required: true,
                minlength: 5
            },
            password_again: {
                required: true,
                equalTo: "input#password"
            }
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            beforeSend: function (xhr) { },
            success: function (data) {
                if (data.code == 1) {
                    form_fields_highlight_normal(form.find("input"));
                    show_success_notice(form, data.message);
                    $("#my_hosting_email_edit .listholder.secondary h1").after('<a id="' + data.username + '" class="act_email_form_load duolink" href="' + data.url + '"><dl><span class="status_indicator"></span><dt>' + data.username + "</dt><dd>" + data.quotaMessage + "</dd></dl></a>");
                    $("#cpanel_mail_create_form #username").val("");
                    $("#cpanel_mail_create_form #qouta").val("");
                    $("#cpanel_mail_create_form #password").val("");
                    $("#cpanel_mail_create_form #password_again").val("")
                } else {
                    show_error_notice(form, data.message)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                message = "An error occured when trying to create your account";
                show_error_notice(form, message)
            }
        })
    }
    return false
});
$(document).off("click", "#my_hosting_email_edit #update_email").on("click", "#my_hosting_email_edit #update_email", function (e) {
    e.preventDefault();
    var form = $("#my_hosting_email_edit #cpanel_mail_update_form");
    var dovalidate = false;
    form.find("input,select").each(function (index, el) {
        if ($(el).val() != $(el).prop("rel")) {
            dovalidate = true
        }
    });
    if (!dovalidate) {
        content = '<h1>Nothing Has Changed</h1><p><strong>There has been no modifications to your email options</strong></p><div class="popwindow_buttons"><a class="pop_close btn_styled">OK</a><div class="clear"></div></div>';
        showPopup("No Modifications Found", content, "warning");
        return false
    }
    form_fields_highlight_normal(form.find("input"));
    validate = form.validate({
        rules: {
            password: {
                minlength: 5
            },
            password_again: {
                equalTo: "input#password"
            }
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    data = form.serialize();
    if ($("#cpanel_mail_update_form #qouta").prop("rel") != $("#cpanel_mail_update_form #qouta").val()) {
        data = data + "&update_quota=true"
    }
    if ($("#cpanel_mail_update_form #password").prop("rel") != $("#cpanel_mail_update_form #password").val()) {
        data = data + "&update_password=true"
    }
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: data,
            type: "POST",
            beforeSend: function (xhr) { },
            success: function (data) {
                if (data.code == 1) {
                    form_fields_highlight_normal(form.find("input,select"));
                    show_success_notice(form, data.message);
                    $("#cpanel_mail_update_form #qouta").prop("rel", $("#cpanel_mail_update_form #qouta").val());
                    $("#cpanel_mail_update_form #password").val("");
                    $("#cpanel_mail_update_form #password_again").val("");
                    if (data.quotaMessage !== "") {
                        $("#my_hosting_email_edit #" + data.user + " dd").html(data.quotaMessage)
                    }
                } else {
                    show_error_notice(form, data.message)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                message = "An error occured when trying to update your details";
                show_error_notice(form, message)
            }
        })
    }
    return false
});
$(document).off("click", "#my_hosting_cpanel_pass #update_cpanel_pass").on("click", "#my_hosting_cpanel_pass #update_cpanel_pass", function (e) {
    e.preventDefault();
    var form = $("#my_hosting_cpanel_pass #cpanel_mail_update_form");
    validate = form.validate({
        rules: {
            password: {
                minlength: 8
            },
            password_again: {
                equalTo: "input#password"
            }
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: data,
            type: "POST",
            beforeSend: function (xhr) { },
            success: function (data) {
                if (data.code == 1) {
                    form_fields_highlight_normal(form.find("input,select"));
                    show_success_notice(form, data.message);
                    $("#cpanel_reset_pass_form #password").val("");
                    $("#cpanel_reset_pass_form #password_again").val("");
                    if (data.quotaMessage !== "") {
                        $("#my_hosting_email_edit #" + data.user + " dd").html(data.quotaMessage)
                    }
                } else {
                    show_error_notice(form, data.message)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                message = "An error occured when trying to update your details";
                show_error_notice(form, message)
            }
        })
    }
    return false
});
$(document).off("click", "#cpanel_reset_pass_form #update_cpanel_pass_btn").on("click", "#cpanel_reset_pass_form #update_cpanel_pass_btn", function (e) {
    e.preventDefault();
    var form = $("#cpanel_reset_pass_form");
    validate = form.validate({
        rules: {
            password: {
                minlength: 8,
                required: true
            },
            password_again: {
                equalTo: "input#password"
            },
            messages: {
                required: "Passwords are required",
                minlength: "Please, at least 8 characters are necessary",
                equalTo: "PAsswords need to match"
            }
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    var password = $("#cpanel_reset_pass_form #password").val();
    var password_again = $("#cpanel_reset_pass_form #password_again").val();
    var domain = $("#cpanel_reset_pass_form #domain").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_cpanel_password(domain);
                setTimeout(function () {
                    showPopup("Success!", data, "success")
                }, 500)
            }
        })
    }
    return false
});

function reload_cpanel_password(domain) {
    var myCPanel = $("#my_hosting_cpanel_pass.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-hosting/change-cpanel-password/" + domain;
    if (myCPanel.length > 0) {
        $("#my_hosting_cpanel_pass").load(url + " #my_hosting_cpanel_pass > *", function () {
            myCPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_hosting_detail(domain) {
    var sharedHostingPanel = $("#shared_hosting_panel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/shared-hosting/" + domain;
    if (sharedHostingPanel.length > 0) {
        $("#shared_hosting_panel").load(url + " #shared_hosting_panel > *", function () {
            sharedHostingPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_my_hosting() {
    var myHostingPanel = $("#my_hosting_panel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-hosting";
    if (myHostingPanel.length > 0) {
        $("#my_hosting_panel .pathwindow_content").load(url + " #my_hosting_panel .pathwindow_content > *", function () {
            myHostingPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_my_cloud(domain) {
    var myCloudPanel = $("#my_cloud_panel");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-cloud/" + domain;
    if (myCloudPanel.length > 0) {
        $("#my_cloud_panel .pathwindow_content").load(url + " #my_cloud_panel .pathwindow_content > *", function () {
            myCloudPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_sitebuilder(sbid) {
    var mySitebuilderPanel = $("#hosting_sitebuilder");
    $.ajaxSetup({
        global: false
    });
    url = "/en/site-builder/dashboard/" + sbid;
    if (mySitebuilderPanel.length > 0) {
        $("#my_cloud_panel .pathwindow_content").load(url + " #hosting_sitebuilder .pathwindow_content > *", function () {
            mySitebuilderPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_my_visp(domain) {
    var myVispPanel = $("#visp_hosting_panel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-hosting/visp/" + domain;
    if (myVispPanel.length > 0) {
        $("#visp_hosting_panel .pathwindow_content").load(url + " #visp_hosting_panel .pathwindow_content > *", function () {
            myVispPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}
$(document).off("keyup", "#domain_search_input").on("keyup", "#domain_search_input", function (e) {
    var pattern = new RegExp($(this).val(), "i");
    $("#my_hosting_panel .searchable a").each(function () {
        domainText = $(this).find("dt").first().html();
        packageText = $(this).find("dd").first().html();
        if (pattern.test(domainText) || pattern.test(packageText)) {
            $(this).fadeIn()
        } else {
            $(this).fadeOut()
        }
    })
});
$(document).off("click", "#shared_settings_panel #submit_domain_settings").on("click", "#shared_settings_panel #submit_domain_settings", function (e) {
    if (!$("#shared_settings_panel #check").prop("checked")) {
        form_field_animate_error($("#shared_settings_panel #check"), "Please let us know your reason");
        return false
    }
    var form = $("#shared_settings_panel #domain_settings_form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            setTimeout(function () {
                showPopup("Complete!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#renew_settings_confirm_panel #settings_choice_renew_confirm").on("click", "#renew_settings_confirm_panel #settings_choice_renew_confirm", function (e) {
    var domainName = $("#renew_settings_choice_panel #domain").val();
    $.ajax({
        url: "/en/my-hosting/queue-renewal",
        type: "POST",
        data: {
            domain: domainName
        },
        success: function (data) {
            closePopup();
            setTimeout(function () {
                showPopup("Renewal Queued! ", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#hosting_contacts #update_billing_contact_btn").on("click", "#hosting_contacts #update_billing_contact_btn", function (e) {
    e.preventDefault();
    var form = $("#billing_contact_update_frm");
    if ($("#billing_contact_update_frm #firstname").val() == "") {
        form_field_animate_error($("#billing_contact_update_frm #firstname"), "Please enter firstname!");
        return false
    } else {
        form_field_clear_error($("#billing_contact_update_frm #firstname"))
    }
    if ($("#billing_contact_update_frm #lastname").val() == "") {
        form_field_animate_error($("#billing_contact_update_frm #lastname"), "Please enter lastname!");
        return false
    } else {
        form_field_clear_error($("#billing_contact_update_frm #lastname"))
    }
    if ($("#billing_contact_update_frm #email").val() == "") {
        form_field_animate_error($("#billing_contact_update_frm #email"), "Please enter email!");
        return false
    } else {
        form_field_clear_error($("#billing_contact_update_frm #email"))
    }
    if ($("#billing_contact_update_frm #cell").val() == "") {
        form_field_animate_error($("#billing_contact_update_frm #cell"), "Please enter cell!");
        return false
    } else {
        form_field_clear_error($("#billing_contact_update_frm #cell"))
    }
    if ($("#billing_contact_update_frm #company").val() == "") {
        form_field_animate_error($("#billing_contact_update_frm #company"), "Please enter company name!");
        return false
    } else {
        form_field_clear_error($("#billing_contact_update_frm #company"))
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                showPopup("Success!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#hosting_contacts #update_tech_contact_btn").on("click", "#hosting_contacts #update_tech_contact_btn", function (e) {
    e.preventDefault();
    var form = $("#tech_contact_update_frm");
    if ($("#tech_contact_update_frm #firstname").val() == "") {
        form_field_animate_error($("#tech_contact_update_frm #firstname"), "Please enter firstname!");
        return false
    } else {
        form_field_clear_error($("#tech_contact_update_frm #firstname"))
    }
    if ($("#tech_contact_update_frm #lastname").val() == "") {
        form_field_animate_error($("#tech_contact_update_frm #lastname"), "Please enter lastname!");
        return false
    } else {
        form_field_clear_error($("#tech_contact_update_frm #lastname"))
    }
    if ($("#tech_contact_update_frm #email").val() == "") {
        form_field_animate_error($("#tech_contact_update_frm #email"), "Please enter email!");
        return false
    } else {
        form_field_clear_error($("#tech_contact_update_frm #email"))
    }
    if ($("#tech_contact_update_frm #cell").val() == "") {
        form_field_animate_error($("#tech_contact_update_frm #cell"), "Please enter cell!");
        return false
    } else {
        form_field_clear_error($("#tech_contact_update_frm #cell"))
    }
    if ($("#tech_contact_update_frm #company").val() == "") {
        form_field_animate_error($("#tech_contact_update_frm #company"), "Please enter company name!");
        return false
    } else {
        form_field_clear_error($("#tech_contact_update_frm #company"))
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                showPopup("Success!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", ".popwindow_buttons #reseller_signup_btn").on("click", ".popwindow_buttons #reseller_signup_btn", function (e) {
    e.preventDefault();
    var form = $("#reseller_signup_frm");
    var re = /^[a-zA-Z0-9-]{3,30}$/;
    if (!re.test($("#reseller_signup_frm #domain_name").val())) {
        form_field_animate_error($("#reseller_signup_frm #domain_name"), "Please enter a valid domain name!");
        return false
    } else {
        form_field_clear_error($("#reseller_signup_frm #domain_name"))
    }
    if ($("#reseller_signup_frm #package").val() == "") {
        form_field_animate_error($("#reseller_signup_frm #package"), "Please select a package!");
        return false
    } else {
        form_field_clear_error($("#reseller_signup_frm #package"))
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_my_hosting();
            setTimeout(function () {
                showPopup("Success!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#give_domain_frm #selectAll").on("click", "#give_domain_frm #selectAll", function (e) {
    e.preventDefault();
    $(".domain_products").each(function () {
        this.checked = true
    })
});
$(document).off("click", "#give_domain_frm #selectNone").on("click", "#give_domain_frm #selectNone", function (e) {
    e.preventDefault();
    $(".domain_products").each(function () {
        this.checked = false
    })
});
$(document).off("click", "#ownership_give #change_ownership_btn").on("click", "#ownership_give #change_ownership_btn", function (e) {
    e.preventDefault();
    var form = $("#give_domain_frm");
    var atLeastOneIsChecked = $("input:checkbox").is(":checked");
    if (!atLeastOneIsChecked) {
        form_field_animate_error($("#ownership_give .domain_products"), "Please select at least one domain!");
        return false
    } else {
        form_field_clear_error($("#ownership_give .domain_products"))
    }
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test($("#give_email").val())) {
        form_field_animate_error($("#ownership_give #give_email"), "Please enter valid email address.");
        return false
    } else {
        form_field_clear_error($("#ownership_give #give_email"))
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                showPopup("Transfer Confirmation!", data, "info")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#popwindow_buttons #give_domain_confirm_btn").on("click", "#popwindow_buttons #give_domain_confirm_btn", function (e) {
    e.preventDefault();
    var form = $("give_domain_confirm_frm");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                showPopup("Success!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#popwindow_buttons #success_give_domain").on("click", "#popwindow_buttons #success_give_domain", function (e) {
    e.preventDefault();
    closePanel()
});
$(document).off("click", "#ownership_gain #complete_ownership_btn").on("click", "#ownership_gain #complete_ownership_btn", function (e) {
    e.preventDefault();
    var form = $("#complete_ownership_frm");
    if ($("#complete_ownership_frm #auth_key").val() == "") {
        form_field_animate_error($("#complete_ownership_frm #auth_key"), "Please enter valid auth key.");
        return false
    } else {
        form_field_clear_error($("#ownership_give #auth_key"))
    }
    if (jQuery("input[type=radio][name=payment_method]", form).length) {
        var atLeastOneIsChecked = $("input:radio").is(":checked");
        if (!atLeastOneIsChecked) {
            $("#complete_ownership_frm .notice_error").html("Please select a payment method!").slideDown();
            setTimeout(function () {
                $("#complete_ownership_frm .notice_error").slideUp()
            }, 3e3);
            return false
        } else {
            form_field_clear_error($("#complete_ownership_frm .paymentMethod"))
        }
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                showPopup("Change of Ownership!", data, "info")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#ownership_gain #complete_domain_gain_btn").on("click", "#ownership_gain #complete_domain_gain_btn", function (e) {
    e.preventDefault();
    var form = $("#complete_gain_domain_frm");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_my_hosting();
            closePanel();
            setTimeout(function () {
                showPopup("Success!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("change", "#editShared #editSharedNewProductID").on("change", "#editShared #editSharedNewProductID", function () {
    if ($("#editSharedNewProductID").val() == "") {
        $("#calculationResultHolder").hide();
        $("#editShared #terms").removeAttr("checked");
        form_field_animate_error($("#editShared #terms"), "Please select your new package");
        return false
    }
    if ($("#downgrade_cost").length) {
        $("#downgrade_cost").hide();
        if ($("#editSharedNewProductID").val() == 8) {
            $("#downgrade_cost").show()
        }
    }
    $("#editShared #editSharedNewProductID").parent().removeClass("animated");
    $("#editShared #editSharedNewProductID").parent().removeClass("error");
    $.ajax({
        global: false,
        url: "/en/my-hosting/calculate",
        data: $("#editSharedForm").serialize(),
        type: "POST",
        success: function (data) {
            $.ajaxSetup({
                global: true
            });
            $("#editShared #terms").removeAttr("checked");
            $("#calculationResult").html(data);
            $("#calculationResultHolder").show()
        }
    });
    return false
});
$(document).off("click", "#editShared #terms").on("click", "#editShared #terms", function (e) {
    if ($(this).prop("checked")) {
        form_field_animate_success($(this), "Terms accepted");
        $(this).prop("checked", true)
    } else {
        form_field_animate_error($(this), "In order to change your package you must accept the terms")
    }
    e.stopImmediatePropagation()
});
$(document).off("click", "#editShared #hosting_change_package_btn").on("click", "#editShared #hosting_change_package_btn", function (e) {
    e.preventDefault();
    var form = $("#editShared #editSharedForm");
    if ($("#editSharedNewProductID").val() == "") {
        $("#editShared #terms").removeAttr("checked");
        form_field_animate_error($("#editShared #editSharedNewProductID"), "Please select your new package");
        return false
    }
    if ($("#editShared #terms").is(":visible") && !$("#editShared #terms").prop("checked")) {
        form_field_animate_error($("#editShared #terms"), "In order to change your package you must accept the terms");
        return false
    }
    $("#editShared #editSharedNewProductID").parent().removeClass("animated");
    $("#editShared #editSharedNewProductID").parent().removeClass("error");
    var domain = $("#editSharedForm #editSharedDomain").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            if (data.redirect == "") {
                reload_hosting_detail(domain);
                reload_my_hosting();
                setTimeout(function () {
                    showPopup("Thank You!", data.html, "success")
                }, 500)
            } else {
                location.href = data.redirect
            }
        }
    });
    return false
});
$(document).ready(function () {
    $(document).on("click", "#btn_host_sitebuilder", function (e) {
        e.preventDefault();
        var dest = $(this).attr("href");
        var login = dest.search("login");
        if (login > -1) {
            $.ajax({
                global: false,
                type: "GET",
                url: $("#btn_host_sitebuilder").attr("href"),
                success: function (data) {
                    $.ajaxSetup({
                        global: true
                    });
                    if (parseInt(data.intResponseCode) > 1) {
                        showPopup("SiteBuilder Login Failed", "<p>" + data.error + "</p>", "warning")
                    } else {
                        var sbWindow = window.open(data.url);
                        var blocked = false;
                        if (sbWindow == undefined) {
                            blocked = true
                        }
                        if (sbWindow != undefined && sbWindow.closed) {
                            blocked = true
                        }
                        if (blocked) {
                            showPopup("SiteBuilder Login", "<p>The new window/tab did not open successfully.<br/>Please ensure your popup blocker is disabled.</p>", "info")
                        }
                    }
                }
            })
        }
        return false
    });
    $("body").on("click", "#btn_sitebuilder_activate", function (e) {
        e.preventDefault();
        $.ajax({
            global: false,
            type: "GET",
            url: $("#btn_sitebuilder_activate").attr("href"),
            success: function (data) {
                $.ajaxSetup({
                    global: true
                });
                if (parseInt(data.intResponseCode) > 1) {
                    showPopup("SiteBuilder Activation Failed", '<div style="width:400px;"><p>' + data.error + "</p></div>", "warning")
                } else {
                    reload_hosting_detail(data.domain);
                    reload_my_hosting();
                    setTimeout(function () {
                        showPopup("Thank You!", "<p>Your SiteBuilder has been activated. You can now login via the SiteBuilder button</p>", "success")
                    }, 500);
                    var popupId = $(".popwindow .btn_close, .popwindow .pop_close").prop("id");
                    closePopup(popupId)
                }
            }
        })
    });
    $("body").on("click", "#btn_sitebuilder_demo", function (e) {
        e.preventDefault();
        $.ajax({
            global: false,
            type: "GET",
            url: $("#btn_sitebuilder_demo").attr("href"),
            success: function (data) {
                $.ajaxSetup({
                    global: true
                });
                if (parseInt(data.intResponseCode) > 1) {
                    $("#sitebuilder_dashboard").html("<p>" + data.error + "</p>")
                } else {
                    var sbWindow = window.open(data.url);
                    var blocked = false;
                    if (sbWindow == undefined) {
                        blocked = true
                    }
                    if (sbWindow != undefined && sbWindow.closed) {
                        blocked = true
                    }
                    if (blocked) {
                        showPopup("SiteBuilder Login", "<p>The new window/tab did not open successfully.<br/>Please ensure your popup blocker is disabled.</p>", "info")
                    }
                }
            }
        })
    })
});
$(document).ready(function () {
    $(document).off("click", "#cancel_sitebuilder_btn").on("click", "#cancel_sitebuilder_btn", function (e) {
        e.preventDefault();
        var form = $("#hosting_sitebuilder #sitebuilder_cancel_form");
        var sitebuilder_id = $("#client_product_id");
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_sitebuilder(sitebuilder_id);
                setTimeout(function () {
                    showPopup("Complete!", data, "success")
                }, 500)
            }
        });
        return false
    });
    $(document).off("click", "#reactivate_sitebuilder_btn").on("click", "#reactivate_sitebuilder_btn", function (e) {
        e.preventDefault();
        var form = $("#hosting_sitebuilder #sitebuilder_reactivate_form");
        var sitebuilder_id = $("#client_product_id");
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_sitebuilder(sitebuilder_id);
                setTimeout(function () {
                    showPopup("Complete!", data, "success")
                }, 500)
            }
        });
        return false
    })
});
$(document).ready(function () {
    $("body").ajaxComplete(function (event, XMLHttpRequest, ajaxOption) {
        if (XMLHttpRequest.getResponseHeader("x-debug-token")) {
            $(".sf-toolbarreset").remove();
            $.get(window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/_wdt/" + XMLHttpRequest.getResponseHeader("x-debug-token"), function (data) {
                $("body").append(data)
            })
        }
    })
});
$(document).off("focus", "#my_firewall_panel #fw_source").on("focus", "#my_firewall_panel #fw_source", function () {
    if ($(this).val() == "ANY") { }
});
$(document).off("blur", "#my_firewall_panel #fw_source").on("blur", "#my_firewall_panel #fw_source", function () {
    if ($(this).val() == "") {
        $(this).val("ANY")
    }
});
$(document).off("click", "#my_firewall_panel #fw_add_rule").on("click", "#my_firewall_panel #fw_add_rule", function (e) {
    e.preventDefault();
    var form = $("#my_firewall_panel #firewallRuleForm");
    if ($("#fw_destination").val() == "") {
        form_field_animate_error($("#editShared #fw_destination"), "Please select the destination IP");
        return false
    }
    if ($("#fw_protocol").val() == "") {
        form_field_animate_error($("#editShared #fw_protocol"), "Please select your the protocol");
        return false
    }
    if ($("#fw_ports").val() == "") {
        form_field_animate_error($("#editShared #fw_ports"), "Please specify the relevant ports");
        return false
    }
    $("#editShared #fw_destination").parent().removeClass("animated");
    $("#editShared #fw_destination").parent().removeClass("error");
    $("#editShared #fw_protocol").parent().removeClass("animated");
    $("#editShared #fw_protocol").parent().removeClass("error");
    $("#editShared #fw_ports").parent().removeClass("animated");
    $("#editShared #fw_ports").parent().removeClass("error");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_rules_list();
            setTimeout(function () {
                showPopup("Thank You!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#btn_firewall_delete").on("click", "#btn_firewall_delete", function (e) {
    e.preventDefault();
    var popup_id = $(this).parent("div").parent("div").parent(".popwindow").prop("id");
    $.ajax({
        url: $(this).attr("href"),
        type: "GET",
        success: function (data) {
            closePopup(popup_id);
            reload_rules_list();
            setTimeout(function () {
                showPopup("Thank You!", data, "success")
            }, 500)
        }
    });
    return false
});

function reload_rules_list() {
    var container = $("#my_firewall_panel #fw_rules_list");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-firewall/get-rules";
    if (container.length > 0) {
        $("#my_firewall_panel #fw_rules_list").load(url, function () {
            $.ajaxSetup({
                global: true
            })
        })
    }
}
$(document).off("click", ".datatable tr.act_popup_open").on("click", ".datatable tr.act_popup_open", function (event) {
    event.preventDefault();
    title = $(this).prop("title");
    popup_class = "info";
    if ($(this).prop("rel")) {
        popup_class = $(this).prop("rel")
    }
    $("body").trigger("popUpLoading");
    $.get($(this).attr("href"), function (data) {
        showPopup(title, data, popup_class)
    });
    return false
});
var specHandler = function () {
    var mapping = {},
        selectedPackage = 0,
        domain;
    var getMapping = function () {
        return mapping
    };
    var setMapping = function (json) {
        mapping = $.parseJSON(json)
    };
    var getMap = function (k) {
        return mapping[k]
    };
    var getSelectedPackage = function () {
        return selectedPackage
    };
    var setSelectedPackage = function (v) {
        selectedPackage = v
    };
    var getDomain = function () {
        return domain
    };
    var setDomain = function (v) {
        domain = v
    };
    return {
        getMapping: getMapping,
        setMapping: setMapping,
        getMap: getMap,
        getSelectedPackage: getSelectedPackage,
        setSelectedPackage: setSelectedPackage,
        getDomain: getDomain,
        setDomain: setDomain
    }
}();
$(document).on("change keyup", "#cloud_new_package", function () {
    var k = $(this).val();
    if (k == "") {
        k = 0
    }
    specHandler.setSelectedPackage(k);
    if (k <= 0) {
        $(".serverspecs").hide();
        $("#cost_explanation").hide();
        return false
    }
    var map = specHandler.getMap(k);
    $("#specCPU").html(map.items.CPU.description);
    $("#specRam").html(map.items.Ram.name);
    $("#specHardDrive").html(map.items.HardDrive.name + " NAS");
    $("#specTraffic").html(map.items.Traffic.description);
    $("#specIP").html(map.items.IP.description);
    if (map.charge < 0) {
        $("#cost_explanation").html("<strong>Please note:</strong> This package change will be effective immediately and you will be credited <strong>R" + Number(Math.abs(map.charge)).toFixed(2) + "</strong>")
    } else {
        $("#cost_explanation").html("<strong>Please note:</strong> This package change will be effective immediately and you will be charged pro-rata <strong>R" + Number(Math.abs(map.charge)).toFixed(2) + "</strong>")
    }
    $(".serverspecs").show();
    $("#cost_explanation").show()
});
$(document).off("click", "#cloud_edit_package_panel #cloud_edit_package_button").on("click", "#cloud_edit_package_panel #cloud_edit_package_button", function (e) {
    e.preventDefault();
    var form = $("#cloud_edit_package_form");
    if (specHandler.getSelectedPackage() <= 0) {
        return false
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        async: false,
        success: function (data) {
            closePanel();
            reload_cloud_control_panel(specHandler.getDomain());
            setTimeout(function () {
                showPopup("Thank You!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).on("panelShown", function (e) {
    currentPanel = $(e.target);
    if ($("#deploying").is("input")) {
        return
    }
    if ($("#cloudControlPanel").length > 0 || $("#dedicatedControlPanel").length > 0 || $("#rackspaceControlPanel").length > 0) {
        var yVals = JSON.parse($(".datagraph").attr("data-yvals"));
        var xVals = JSON.parse($(".datagraph").attr("data-xvals").replace(/\b0(\d)/g, "$1"));
        var maxYVal = getMaxOfArray(yVals);
        $(".graphholder").highcharts({
            chart: {
                type: "areaspline",
                marginLeft: 25,
                backgroundColor: null,
                plotBorderWidth: 1,
                plotBackgroundColor: {
                    linearGradient: [0, 0, 0, 150],
                    stops: [
                        [.5, "rgb(255, 255, 255)"],
                        [1, "rgb(221, 221, 221)"]
                    ]
                },
                events: {
                    load: function () {
                        this.plotBackground.attr({
                            rx: 15,
                            ry: 15
                        });
                        this.plotBorder.attr({
                            rx: 15,
                            ry: 15
                        });
                        $(".highcharts-series-group").prop({
                            transform: "translate(0,-8)"
                        })
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            title: {
                text: ""
            },
            subtitle: {
                text: ""
            },
            xAxis: {
                categories: xVals,
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: "transparent",
                minorTickLength: 0,
                min: 0,
                max: 13,
                tickLength: 0,
                labels: {
                    rotation: -90,
                    align: "right",
                    style: {
                        fontSize: "12px",
                        fontWeight: "bold",
                        fontFamily: "Sterling, sans-serif",
                        color: "#999"
                    },
                    x: 5,
                    y: 28
                }
            },
            yAxis: {
                title: {
                    text: ""
                },
                minPadding: .25,
                min: 0,
                max: maxYVal <= 0 ? 1 : maxYVal,
                labels: {
                    formatter: function () {
                        if (this.isFirst) {
                            return this.value
                        }
                        return bytesToSize(this.value, 0, true)
                    },
                    style: {
                        fontSize: "12px",
                        fontWeight: "bold",
                        fontFamily: "Sterling, sans-serif",
                        color: "#999"
                    },
                    align: "center",
                    x: -16,
                    y: 12
                },
                gridLineColor: "transparent"
            },
            tooltip: {
                enabled: false,
                shared: true,
                formatter: function () {
                    var tooltip = $("<div>");
                    var total = 0;
                    $.each(this.points, function (i, point) {
                        total += this.y;
                        tooltip.append($('<div style="color: ' + point.series.area.fill + ';">').html(point.series.name + ": " + bytesToSize(this.y, 2)));
                        if (!(i % 2)) {
                            tooltip.append("<br />")
                        }
                    });
                    tooltip.prepend($('<div style="color: #999;">').html("Total: " + bytesToSize(total, 2) + "<br />"));
                    return tooltip.get(0).outerHTML
                }
            },
            plotOptions: {
                areaspline: {
                    lineWidth: 1.3,
                    marker: {
                        states: {
                            hover: {
                                radius: 5
                            }
                        }
                    },
                    states: {
                        hover: {
                            lineWidth: 1.3
                        }
                    }
                }
            },
            series: [{
                name: "Download",
                fillColor: "rgba(0, 140, 194, 0.3)",
                lineColor: "#008cc2",
                data: yVals[0]
            }, {
                name: "Upload",
                fillColor: "rgba(117, 0, 198, 0.4)",
                lineColor: "#7500c6",
                marker: {
                    enabled: false,
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                },
                data: yVals[1]
            }]
        })
    }
    return true
});

function getMaxOfArray(numArray) {
    if (!Array.isArray(numArray)) {
        return false
    }
    if (Array.isArray(numArray[0])) {
        numArray = numArray.reduce(function (a, b) {
            return a.concat(b)
        })
    }
    return Math.max.apply(null, numArray)
}

function bytesToSize(bytes, precision, AddLineBreaks) {
    var kilobyte = 1024;
    var megabyte = kilobyte * 1024;
    var gigabyte = megabyte * 1024;
    var terabyte = gigabyte * 1024;
    var lineBreaks = AddLineBreaks == true ? "<br />" : "";
    if (bytes >= 0 && bytes < kilobyte) {
        return bytes + lineBreaks + " B"
    } else if (bytes >= kilobyte && bytes < megabyte) {
        return (bytes / kilobyte).toFixed(precision) + lineBreaks + " KB"
    } else if (bytes >= megabyte && bytes < gigabyte) {
        return (bytes / megabyte).toFixed(precision) + lineBreaks + " MB"
    } else if (bytes >= gigabyte && bytes < terabyte) {
        return (bytes / gigabyte).toFixed(precision) + lineBreaks + " GB"
    } else if (bytes >= terabyte) {
        return (bytes / terabyte).toFixed(precision) + lineBreaks + " TB"
    } else {
        return bytes + lineBreaks + " B"
    }
}
$(document).on("change", "#power_timeframe", function () {
    if ($(this).val() == "later") {
        $("#power_timeframe_specified").mask("9999-99-99 99:99");
        $("#power_timeframe_holder").show();
        return false
    }
    $("#power_timeframe_holder").hide()
});
$(document).on("click", "#btn_host_console", function (e) {
    e.preventDefault();
    $("#loader").show();
    $.ajax({
        url: $(this).attr("href"),
        type: "GET",
        success: function (data) {
            $("#loader").hide();
            if (parseInt(data.result) == 13) {
                setTimeout(function () {
                    showPopup("Something went wrong!", "<p>" + data.message + "</p>", "warning")
                }, 500)
            } else {
                var sbWindow = window.open(data.url);
                var blocked = false;
                if (sbWindow == undefined) {
                    blocked = true
                }
                if (sbWindow != undefined && sbWindow.closed) {
                    blocked = true
                }
                if (blocked) {
                    showPopup("Cloud Console", "<p>The new window/tab did not open successfully.<br/>Please ensure your popup blocker is disabled.</p>", "info")
                }
            }
        }
    })
});

function reload_cloud_control_panel(domain) {
    var cloud_control_panel = $("#cloudControlPanel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    if (cloud_control_panel.length > 0) {
        $("#cloudControlPanel .pathwindow_content").load("/en/my-cloud/" + domain + " #cloudControlPanel .pathwindow_content > *", function () {
            cloud_control_panel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}
$(document).off("click", "#cloudDiskManagement #btnAddCloudDisk").on("click", "#cloudDiskManagement #btnAddCloudDisk", function (e) {
    e.preventDefault();
    var form = $("#cloud_add_disk_form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        async: false,
        success: function (data) {
            closePanel();
            reload_cloud_control_panel($("#domain").val());
            setTimeout(function () {
                showPopup("New Disk Allocation", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#cloudDiskManagement #btnRedirectToEditPackage").on("click", "#cloudDiskManagement #btnRedirectToEditPackage", function (e) {
    e.preventDefault();
    var url = "/en/my-cloud/edit-package/" + $("#domain").val();
    closePanel();
    openPanel(url);
    return false
});
$(document).off("click", "#dedicated_control_panel_execute_btn").on("click", "#dedicated_control_panel_execute_btn", function (e) {
    e.preventDefault();
    var thisPopup = $(this).parents(".popwindow");
    var domain = $("#dedicated_control_panel_execute_form #domain").val();
    var currentPanel = $("#panels .pathwindow :last");
    var popup_id = thisPopup.find(".btn_close").prop("data-popup-id");
    $.ajax({
        url: $("#dedicated_control_panel_execute_action").val(),
        data: $("#dedicated_control_panel_execute_form").children().find("input, select, textarea").serialize(),
        type: "POST",
        beforeSend: function (jqXHR, settings) {
            closePopup(popup_id);
            currentPanel.append('<div class="pathwindow_whiteout" style="display:none;"></div>');
            currentPanel.find(".pathwindow_whiteout").fadeIn(250)
        },
        success: function (data) {
            reload_dedicated_control_panel(domain);
            showPopup("Success", data, "success")
        }
    });
    return false
});

function reload_dedicated_control_panel(domain) {
    var dedicatedControlPanel = $("#dedicatedControlPanel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-dedicated/" + domain;
    if (dedicatedControlPanel.length > 0) {
        $("#dedicatedControlPanel").load(url + " #dedicatedControlPanel > *", function () {
            dedicatedControlPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

function reload_rackspace_control_panel(domain) {
    console.log("reload_rackspace_control_panel accessed");
    var rackspaceControlPanel = $("#rackspaceControlPanel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/my-rackspace/" + domain;
    if (rackspaceControlPanel.length > 0) {
        $("#rackspaceControlPanel").load(url + " #rackspaceControlPanel > *", function () {
            rackspaceControlPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}
$(document).off("change keyup", "#dedicated_cancel_form #reason").on("change keyup", "#dedicated_cancel_form #reason", function () {
    if ($(this).val() == "Other") {
        $("#dedicated_cancel_form #other_holder").slideDown(500)
    } else {
        $("#dedicated_cancel_form #other_holder").slideUp(500)
    }
    return false
});
$(document).on("click", "#cancel_dedicated_btn", function (e) {
    e.preventDefault();
    console.log("cancel");
    var form = $("#dedicated_cancel_form");
    validate = form.validate({
        messages: {
            reason: "Please Select a reason for cancelling",
            service_rating: "Please rate our service"
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    var domainname = $("#dedicated_domain").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                if ($("#dedicatedControlPanel").is(":visible")) {
                    reload_dedicated_control_panel(domainname)
                }
                if ($("#rackspaceControlPanel").is(":visible")) {
                    reload_rackspace_control_panel(domainname)
                }
                setTimeout(function () {
                    showPopup("Sorry to see you leave!", data, "warning")
                }, 500)
            }
        })
    }
    return false
});
$(document).on("click", "#reactivate_dedicated_btn", function (e) {
    e.preventDefault();
    var form = $("#dedicated_reactivate_form");
    var domainname = $("#dedicated_domain").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                if ($("#dedicatedControlPanel").is(":visible")) {
                    reload_dedicated_control_panel(domainname)
                }
                if ($("#rackspaceControlPanel").is(":visible")) {
                    reload_rackspace_control_panel(domainname)
                }
                setTimeout(function () {
                    showPopup("Welcome back!", data, "success")
                }, 500)
            }
        })
    }
    return false
});
"use strict";
$(document).on("click", "#afridesign_promotion_panel .afridesign-accept", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/en/api/promotion/afridesign-accept/" + $(this).attr("data-domain") + "/" + $(this).attr("data-id"),
        type: "GET",
        dataType: "json"
    }).done(function (json) {
        closePanel();
        location.href = "/en/hosting/afridesign-form/" + json.data.domain
    }).fail(function (data) {
        showPopup("Oops!", "<p>" + data.message + "</p>", "warning")
    })
});
$(document).on("click", "#afridesign_promotion_soldout_panel .afridesign-waiting-list", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/en/api/promotion/afridesign-waiting-list/" + $(this).attr("data-domain"),
        type: "GET",
        dataType: "json"
    }).done(function (data) {
        closePanel();
        setTimeout(function () {
            showPopup("Thank You!", "<p>" + data.message + "</p>", "success")
        }, 500)
    }).fail(function (data) {
        showPopup("Oops!", "<p>" + data.message + "</p>", "warning")
    })
});
$(document).on("click", "#mobile_invite_panel #btn_invite_mail", function (e) {
    e.preventDefault();
    if ($("#btn_invite_mail").hasClass("selected")) {
        return false
    }
    $("#btn_invite_code").removeClass("selected");
    $("#btn_invite_mail").removeClass("selected").addClass("selected");
    $("#invite_content_div").load("/en/promotion-invitations/emails");
    return false
});
$(document).on("click", "#btn_invite_code", function (e) {
    e.preventDefault();
    if ($("#btn_invite_code").hasClass("selected")) {
        return false
    }
    $("#btn_invite_mail").removeClass("selected");
    $("#btn_invite_code").removeClass("selected").addClass("selected");
    $("#invite_content_div").load("/en/promotion-invitations/coupons");
    return false
});
$(document).on("click", "#btn_generate_coupon", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/en/promotion-invitations/coupons/generate",
        type: "POST",
        success: function (data) {
            $("#invite_content_div").load("/en/promotion-invitations/coupons");
            showPopup("Code Successfully Generated", data, "info")
        }
    });
    return false
});
$(document).on("click", "#mobile_invite_panel #btn_send_email", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/en/promotion-invitations/email/send",
        type: "POST",
        data: $("#mobile_preorder_invites").serialize(),
        success: function (data) {
            if (data.indexOf("Oops!") === -1) {
                $("#invite_content_div").load("/en/promotion-invitations/emails");
                showPopup("Invitation Emails Sent", data, "info")
            } else {
                showPopup("Invitation Emails NOT Sent", data, "warning")
            }
        }
    });
    return false
});
$(document).on("click", "#mobile_invite_panel #btn_send_promo_email", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/en/promotion-invitations/email/send",
        type: "POST",
        data: $("#mobile_preorder_invites").serialize(),
        success: function (data) {
            if (data.indexOf("Oops!") === -1) {
                $("#invite_content_div").load("/en/promotion-invitations/emails");
                showPopup("Invitation Emails Sent", data, "info")
            } else {
                showPopup("Invitation Emails NOT Sent", data, "warning")
            }
        }
    });
    return false
});
$(document).on("click", "#mobile_data_invite_panel #btn_invite_mail", function (e) {
    e.preventDefault();
    if ($("#btn_invite_mobile_data_email").hasClass("selected")) {
        return false
    }
    $("#btn_invite_mobile_data_email").removeClass("selected").addClass("selected");
    $("#invite_content_div").load("/en/mobile-data-invitations/emails");
    return false
});
$(document).on("click", "#btn_send_mobile_data_email", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/en/promotion-invitations/email/send",
        type: "POST",
        data: $("#mobile_preorder_invites").serialize(),
        success: function (data) {
            if (data.indexOf("Oops!") === -1) {
                $("#invite_content_div").load("/en/promotion-invitations/emails");
                showPopup("Invitation Emails Sent", data, "info")
            } else {
                showPopup("Invitation Emails NOT Sent", data, "warning")
            }
        }
    });
    return false
});
$(document).on("change", "#provinceSelect", function () {
    $.ajax({
        url: "/en/WFP Jordan-mobile/get-towns/" + $("#provinceSelect").val(),
        type: "GET",
        global: false,
        success: function (html) {
            $("#cityDiv").html(html)
        }
    });
    $("#suburbHolder").val("Please select your city")
});
$(document).on("change", "#townSelect", function () {
    $.ajax({
        url: "/en/WFP Jordan-mobile/get-suburbs/" + $("#townSelect :selected").val(),
        type: "GET",
        global: false,
        success: function (html) {
            $("#suburbDiv").html(html)
        }
    });
    $("#city").val($("#townSelect :selected").text())
});
$(document).on("change", "#suburbSelect", function () {
    $("#suburb").val($("#suburbSelect :selected").text())
});
$(document).on("click", "#btnUpdateDeliveryAddress", function (e) {
    e.preventDefault();
    var valid = true;
    var error = "<strong>Please fix the following errors:</strong><ul>";
    var recipient = $("#recipient").val();
    var contactNumber = $("#contact_number").val();
    var street = $("#street").val();
    var city = $("#townSelect").val();
    var suburb = $("#suburbSelect").val();
    if (recipient.indexOf(" ") === -1) {
        valid = false;
        error = error + "<li>Name and surname required</li>"
    }
    if (!/\d/.test(street)) {
        valid = false;
        error = error + "<li>Street address must contain a number</li>"
    }
    if (isNaN(parseInt(city)) || parseInt(city) <= 0) {
        valid = false;
        error = error + "<li>Please select your city</li>"
    }
    if (isNaN(parseInt(suburb)) || parseInt(suburb) <= 0) {
        valid = false;
        error = error + "<li>Please select your suburb</li>"
    }
    error = error + "</ul>";
    if (!valid) {
        $("#validationError").html(error).show();
        return false
    }
    $("#validationError").html("").hide();
    var form = $("#deviceDeliveryForm");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (response) {
            if (parseInt(response.code) == 1) {
                showPopup("Update Successful!", response.html, "success");
                return false
            }
            if (parseInt(response.code) == 13) {
                $("#validationError").html(response.html).show();
                return false
            }
            showPopup("An Error Occurred!", response.html, "error")
        }
    });
    return false
});
$(document).on("click", "#atHome", function () {
    $("#companyNameHolder").hide();
    $("#company").val("");
    $("#buildingLabel").html("Complex/Estate");
    $("#formFields").show();
    $("#atWork").removeClass("selected");
    $("#atHome").addClass("selected")
});
$(document).on("click", "#atWork", function () {
    $("#companyNameHolder").show();
    $("#buildingLabel").html("Building/Officepark");
    $("#formFields").show();
    $("#atWork").addClass("selected");
    $("#atHome").removeClass("selected")
});
$;
$(document).ready(function () {
    $(document).off("click", "#telkom-assistance #no_service_btn").on("click", "#telkom-assistance #no_service_btn", function (e) {
        e.preventDefault();
        var form = $("#telkom-assistance #no_service_form");
        form_fields_highlight_normal($("#telkom-assistance #no_service_form :input"));
        validate = form.validate({
            messages: {
                power_check: "Please ensure that the router has power",
                adsl_light_check: "Please check your ADSL Light",
                status_light_check: "Please check the status light",
                fault_date: "Please specify when the fault occurred"
            },
            showErrors: function (errorMap, errorList) { },
            invalidHandler: function (form, validator) {
                $.each(validator.errorList, function (index, item) {
                    form_field_animate_error($(item["element"]), item["message"])
                })
            }
        });
        if (form.valid()) {
            $.ajax({
                url: form.attr("action"),
                data: form.serialize(),
                type: "POST",
                success: function (data) {
                    closePanel();
                    var html = '<h1>Thank you!</h1><p>We will update you as soon as we do some routine tests!</p><div class="popwindow_buttons"><a class="btn_styled pop_close">OK</a><div class="clear"></div></div>';
                    setTimeout(function () {
                        showPopup("Fault Request", html, "success")
                    }, 500)
                }
            })
        }
        return false
    });
    $(document).off("click", "#telkom-assistance .assistance_options").on("click", "#telkom-assistance .assistance_options", function (e) {
        e.preventDefault();
        $(".assistance_options").removeClass("selected");
        var panel_to_show = $(this).attr("href");
        var selected_item = $(this);
        $(".assistance_holder").slideUp("slow", function (e) {
            $(panel_to_show).slideDown("slow");
            selected_item.addClass("selected")
        })
    })
});

function reload_dns_editor(domain) {
    var dnsPanel = $("#dns_editor_panel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/dns-editor/records/" + domain;
    if (dnsPanel.length > 0) {
        $("#dns_editor_panel").load(url + " #dns_editor_panel > *", function () {
            dnsPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}
$(document).off("change keyup", "#visp_cancel_package_form #reason").on("change keyup", "#visp_cancel_package_form #reason", function () {
    if ($(this).val() == "Other") {
        $("#visp_cancel_package_form #other_holder").slideDown(500)
    } else {
        $("#visp_cancel_package_form #other_holder").slideUp(500)
    }
    return false
});
$(document).off("click", "#visp_cancel_package_form #cancel_visp_btn").on("click", "#visp_cancel_package_form #cancel_visp_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    validate = form.validate({
        messages: {
            reason: "Please Select a reason for cancelling",
            service_rating: "Please rate our service"
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    var domainname = $("#visp_cancel_package_form #visp_domain").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_my_visp(domainname);
                setTimeout(function () {
                    showPopup("Sorry to see you leave!", data, "warning")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#visp_reactivate_package_form #reactivate_visp_btn").on("click", "#visp_reactivate_package_form #reactivate_visp_btn", function (e) {
    e.preventDefault();
    var form = $(this).prev("form");
    var domainname = $("#visp_reactivate_package_form #visp_domain").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_my_visp(domainname);
                setTimeout(function () {
                    showPopup("Sorry to see you leave!", data, "warning")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#visp_manage_diskspace_form #add_disk_btn").on("click", "#visp_manage_diskspace_form #add_disk_btn", function (e) {
    e.preventDefault();
    var countGB = $("#visp_diskspace_form #countGBs").val();
    var newValue = parseInt(countGB) + 1;
    $("#visp_diskspace_form #countGBs").val(newValue);
    var costPerGB = parseInt($("#costPerGbAmount").val());
    $.ajax({
        url: "/my-hosting/visp/diskspace/calculation",
        data: {
            cost: costPerGB,
            amount: newValue
        },
        type: "POST",
        success: function (data) {
            $("#costDiv").html(data)
        }
    });
    if (newValue != 0) {
        $("#costDiv").show()
    }
});
$(document).off("click", "#visp_manage_diskspace_form #less_disk_btn").on("click", "#visp_manage_diskspace_form #less_disk_btn", function (e) {
    e.preventDefault();
    var countGB = $("#visp_diskspace_form #countGBs").val();
    var newValue = parseInt(countGB) - 1;
    if (newValue >= 0) {
        $("#visp_diskspace_form #countGBs").val(newValue);
        var costPerGB = parseInt($("#costPerGbAmount").val());
        calculateAmount(costPerGB, newValue)
    }
    if (newValue == 0) {
        $("#costDiv").hide()
    }
});
$(document).off("click", "#registerVispDomainBtn").on("click", "#registerVispDomainBtn", function (e) {
    e.preventDefault();
    var domainname = $("#domain").val();
    var form = $("#visp_register_form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            reload_my_visp(domainname);
            setTimeout(function () {
                showPopup("Success!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).on("popupFormSubmited", function (e, panel) {
    var form = $(e.target);
    if (form.attr("id") == "visp_reactivate_package_form") {
        var domainnameReactivate = $("#visp_reactivate_package_form #visp_domain").val();
        reload_my_visp(domainnameReactivate);
        setTimeout(function () {
            show_whiteout($("#visp_hosting_panel"))
        }, 300)
    }
    if (form.attr("id") == "visp_cancel_package_form") {
        var domainnameCancel = $("#visp_register_form #visp_domain").val();
        reload_my_visp(domainnameCancel);
        setTimeout(function () {
            show_whiteout($("#visp_hosting_panel"))
        }, 300)
    }
});
$(document).off("click", "#visp_diskspace_form .plusminus span").on("click", "#visp_diskspace_form .plusminus span", function (e) {
    e.preventDefault();
    var direction = $(this).prop("class");
    var costPerGB = $("#costPerGbAmount").val();
    var curVal = $("#diskspace input").val();
    curVal.replace("GB", "");
    if (direction == "plus") {
        curVal = parseInt(curVal) + 1;
        $("#diskspace input").val(curVal + "GB");
        if (curVal != 0) {
            $("#costDetails").show();
            $("#debitAmount").html("R" + parseInt(costPerGB) * parseInt(curVal));
            $("#dAmount").val(parseInt(costPerGB) * parseInt(curVal))
        }
    } else {
        curVal = parseInt(curVal) - 1;
        if (curVal >= 0) {
            $("#diskspace input").val(curVal + "GB");
            $("#costDetails").show();
            if (curVal == 0) {
                $("#costDetails").hide()
            }
            $("#debitAmount").html("R" + parseInt(costPerGB) * parseInt(curVal));
            $("#dAmount").val(parseInt(costPerGB) * parseInt(curVal))
        }
    }
});
$(document).off("click", ".checkDomainAvailability").on("click", ".checkDomainAvailability", function (e) {
    e.preventDefault();
    $.ajaxSetup({
        global: false
    });
    var pattern = /^((?!xn--))[A-Za-z0-9\-]+\.co\.za$/i;
    var domain = $("#domainName").val();
    var mainDomain = $("#mainDomain").val();
    if (!pattern.test(domain) || domain.length < 7) {
        $("#holderDomainName").addClass("error");
        $("#holderDomainName").addClass("animated");
        $("#holderDomainName").removeClass("shake").addClass("shake");
        window.setTimeout(function () {
            $("#holderDomainName").removeClass("shake").removeClass("error")
        }, 1100);
        var notice_error = $(".notice_error");
        notice_error.html("Please enter a valid .co.za domain name.");
        notice_error.fadeIn(500, function () {
            setTimeout(function () {
                notice_error.slideUp(500)
            }, 3e3)
        });
        return false
    }
    $(".domainNameDisplay").html(domain);
    $("#checkDomainDiv").hide();
    $("#notAvailableDomainDiv").hide();
    $("#thinkingDomainDiv").show();
    $.ajax({
        url: "/en/my-hosting/visp/checkdomain/" + domain,
        data: {
            domain: domain
        },
        type: "GET",
        success: function (data) {
            success = data;
            $("#thinkingDomainDiv").hide();
            if (success == "true") {
                $("#successDomainDiv").show();
                $("#domainReg").val(domain);
                $("#registerUrl").prop("href", "/en/my-hosting/visp/register/" + mainDomain + "/" + domain)
            } else {
                $("#enterDomainDiv").hide();
                $("#checkDomainDiv").show();
                $("#notAvailableDomainDiv").show()
            }
        }
    });
    $.ajaxSetup({
        global: true
    })
});
$(document).off("click", "#searchAgainBtn").on("click", "#searchAgainBtn", function (e) {
    e.preventDefault();
    $("#notAvailableDomainDiv").hide();
    $("#enterDomainDiv").show();
    $("#successDomainDiv").hide();
    $("#checkDomainDiv").show()
});
$(document).ready(function () {
    $(document).on("click", "div#troubleshooterPanel a#troubleshootingRefreshButton", function (e) {
        troubleshootingRefresher.cancelTimer()
    });
    $(document).on("click", "#troubleshooterPanel .btn_big, #troubleshooterPanel a.troubleshootingInternalForm", function (e) {
        e.preventDefault();
        var buttonID = $(this).prop("id");
        var method = "GET";
        var url = $(this).attr("href");
        var form = null;
        if (buttonID == "troubleshootingNextButton" || buttonID == "troubleshootingCompleteButton") {
            form = $("#troubleshooterPanel").find("form");
            if (form.length !== 0) {
                method = form.attr("method");
                url = form.attr("action")
            } else {
                form = null
            }
        } else if ($(this).hasClass("troubleshootingInternalForm")) {
            form = $(this).closest("form");
            if (form.length !== 0) {
                method = form.attr("method");
                url = form.attr("action")
            } else {
                form = null
            }
        }
        var serializedFormData = "";
        if (form !== null) {
            serializedFormData = form.serializeArray()
        }
        $.ajax({
            url: url,
            type: method,
            data: serializedFormData,
            beforeSend: function (xhr, settings) {
                $(document).trigger("panelOpening");
                settings.panel = true
            },
            success: function (html, textStatus, jqXHR) {
                $("#panels div#troubleshooterPanel").replaceWith($(html).css({
                    left: "50%"
                }));
                return true
            }
        })
    });
    $(document).on("keypress keydown keyup", "#troubleshooterPanel form ", function (e) {
        if (e.keyCode == 13 && e.target.tagName != "TEXTAREA") {
            e.preventDefault()
        }
    })
});
var troubleshootingRefresherCreate = function () {
    "use strict";
    var timeoutID = null;
    var cancelTimer = function () {
        if (timeoutID != null) {
            clearTimeout(timeoutID);
            timeoutID = null
        }
    };
    var triggerButtonClick = function () {
        cancelTimer();
        $("div#troubleshooterPanel a#troubleshootingRefreshButton").trigger("click");
        true
    };
    var startTimer = function (seconds) {
        var delay = seconds * 1e3;
        timeoutID = setTimeout(function () {
            triggerButtonClick()
        }, delay)
    };
    return {
        cancelTimer: cancelTimer,
        startTimer: startTimer
    }
};
var troubleshootingRefresher = troubleshootingRefresherCreate();
$(document).off("click", "#bundle_panel #bundle_products").on("click", "#bundle_panel #bundle_products", function (e) {
    e.preventDefault();
    var form = $("#create_bundle_frm");
    validate = form.validate({
        messages: {
            reason: "Please Select a connectivity product."
        },
        showErrors: function (errorMap, errorList) { },
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, item) {
                form_field_animate_error($(item["element"]), item["message"])
            })
        }
    });
    var number = $("#create_bundle_frm #line_number").val();
    var username = $("#create_bundle_frm #username").val();
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "GET",
            success: function (data) {
                reload_bundles(number);
                reload_line_account(number);
                setTimeout(function () {
                    showPopup("Thank you!", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", "#unbundle_div_container #unbundle_line").on("click", "#unbundle_div_container #unbundle_line", function (e) {
    e.preventDefault();
    var number = $("#unbundle_line_frm #conn_line_number").val();
    reload_bundles(number);
    setTimeout(function () {
        show_whiteout($("#bundle_panel"))
    }, 300)
});
var OrderBundle = {};
(function (OrderBundle) {
    "use strict";
    var order = OrderBundle;
    order.removeItems = function () {
        var editItemsForm = $("#editOrderItemsForm");
        var data = editItemsForm.serialize();
        var allItems = false;
        if ($("#edit_order input:checked").length == $(".order_items").length) {
            allItems = true
        }
        var confirmValue = true;
        if (allItems) {
            confirmValue = confirm("This will cancel the entire order")
        }
        if (confirmValue) {
            $.post(editItemsForm.attr("action"), data, function (data) {
                $.each(data.sub_products_removed, function (i, item) {
                    var toRemove = "#item" + item.id;
                    $(toRemove).remove()
                });
                if ($("#active_orders").length >= 1) {
                    $("#active_orders").load("/en/my-orders");
                    $.ajaxSetup({
                        global: true
                    })
                }
                reload_my_connectivity();
                if (data.close_panel) {
                    closePanel()
                }
            })
        }
    };
    order.showWarning = function (message) {
        var warning = $("#order_warning");
        warning.html(message);
        warning.fadeIn(500, function () {
            setTimeout(function () {
                warning.slideUp(500)
            }, 6e3)
        })
    };
    order.selectAll = function () {
        $(".order_items").prop("checked", 1)
    };
    order.clearAll = function () {
        $(".order_items").prop("checked", 0)
    };
    $(document).off("click", "#btnItemsRemove").on("click", "#btnItemsRemove", function (e) {
        e.preventDefault();
        order.removeItems()
    });
    $(document).off("click", ".order_items").on("click", ".order_items", function (e) {
        if ($("#edit_order input:checked").length == $(".order_items").length) {
            order.showWarning("The entire order will be cancelled")
        } else {
            $("#order_warning").slideUp(500)
        }
    });
    $(document).off("click", "#select_all").on("click", "#select_all", function (e) {
        e.preventDefault();
        order.selectAll();
        order.showWarning("The entire order will be cancelled")
    });
    $(document).off("click", "#clear_all").on("click", "#clear_all", function (e) {
        e.preventDefault();
        order.clearAll()
    })
})(OrderBundle);
$(document).off("click", "#fax2mail_edit #update_fax2mail").on("click", "#fax2mail_edit #update_fax2mail", function (e) {
    e.preventDefault();
    var firstname = $("#fax2mail_edit #firstname");
    var lastname = $("#fax2mail_edit #lastname");
    var email = $("#fax2mail_edit #email");
    if (firstname.val() == "") {
        form_field_animate_error(firstname, "Please enter a firstname.");
        return false
    } else {
        form_field_clear_error(firstname)
    }
    if (lastname.val() == "") {
        form_field_animate_error(lastname, "Please enter a lastname.");
        return false
    } else {
        form_field_clear_error(lastname)
    }
    if (email.val() == "" || !isValidEmailAddress(email.val())) {
        form_field_animate_error(email, "Please enter a valid email.");
        return false
    } else {
        form_field_clear_error(email)
    }
});
$(document).off("click", "#other_faxemail #create_fax2mail").on("click", "#other_faxemail #create_fax2mail", function (e) {
    e.preventDefault();
    var form = $("#add_fax2email_frm");
    var firstname = $("#other_faxemail #firstname");
    var lastname = $("#other_faxemail #lastname");
    var email = $("#other_faxemail #email");
    if (firstname.val() == "") {
        form_field_animate_error(firstname, "Please enter firstname.");
        return false
    } else {
        form_field_clear_error(firstname)
    }
    if (lastname.val() == "") {
        form_field_animate_error(lastname, "Please enter lastname.");
        return false
    } else {
        form_field_clear_error(lastname)
    }
    if (email.val() == "" || !isValidEmailAddress(email.val())) {
        form_field_animate_error(email, "Please enter a valid email.");
        return false
    } else {
        form_field_clear_error(email)
    }
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_fax_account();
                setTimeout(function () {
                    showPopup("Thank you!", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).off("click", ".popwindow_buttons #cancel_fax2mail_btn").on("click", ".popwindow_buttons #cancel_fax2mail_btn", function (e) {
    e.preventDefault();
    var form = $("#delete_fax2mail_frm");
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                reload_fax_account();
                setTimeout(function () {
                    showPopup("Thank you!", data, "success")
                }, 500)
            }
        })
    }
    return false
});

function isValidEmailAddress(emailAddress) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(emailAddress)
}
$(document).off("click", "#refer_create_new_link_panel #refer_create_new_link").on("click", "#refer_create_new_link_panel #refer_create_new_link", function (e) {
    e.preventDefault();
    var form = $("#refer_create_new_link_panel #refer_create_new_link_form");
    if (form.valid()) {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                setTimeout(function () {
                    showPopup("Link Successfully Generated", data, "success")
                }, 500)
            }
        })
    }
    return false
});
$(document).on("change", "#refer_create_new_link_panel .type_select", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/en/products/refer-change-link",
        data: {
            type: $(this).val()
        },
        type: "POST",
        success: function (data) {
            $("#refer_create_new_link_panel #link_text").val(data)
        }
    });
    return false
});
$(document).off("click", "#products_refer #refer_links a").on("click", "#products_refer #refer_links a", function (e) {
    e.preventDefault();
    doContent(this)
});
$(document).off("click", "#landing_holder #inner_faq_button").on("click", "#landing_holder #inner_faq_button", function (e) {
    e.preventDefault();
    doContent("#btn_refer_faq")
});
$(document).off("click", "#landing_holder #get_started").on("click", "#landing_holder #get_started", function (e) {
    e.preventDefault();
    doContent("#btn_refer_link")
});

function doContent(this_element) {
    $(".iconlink").removeClass("selected");
    $(this_element).addClass("selected");
    var viewToShow = $($(this_element).attr("href"));
    $("#refer_main_content .content").slideUp();
    viewToShow.slideDown("500")
}

function reload_refer_panel() {
    var refer_panel = $("#products_refer");
    $.ajaxSetup({
        global: false
    });
    url = "/en/products/refer-a-friend";
    if (refer_panel.length > 0) {
        $("#products_refer .pathwindow_content").load(url + " #products_refer .pathwindow_content > *", {
            reload: "1"
        }, function () {
            refer_panel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            });
            doContent("#btn_refer_link")
        })
    }
}
$(document).off("click", ".popwindow_buttons #send_refer_btn").on("click", ".popwindow_buttons #send_refer_btn", function (e) {
    e.preventDefault();
    var form = $("#send_refer_frm");
    if ($("#send_refer_frm #refer_name").val() == "") {
        form_field_animate_error($("#send_refer_frm #refer_name"), "Please enter a name.");
        return false
    } else {
        form_field_clear_error($("#send_refer_frm #refer_name"))
    }
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test($("#send_refer_frm #refer_email").val())) {
        form_field_animate_error($("#send_refer_frm #refer_email"), "Please enter valid email address.");
        return false
    } else {
        form_field_clear_error($("#send_refer_frm #refer_email"))
    }
    if ($("#send_refer_frm #refer_message").val() == "") {
        form_field_animate_error($("#send_refer_frm #refer_message"), "Please enter a message.");
        return false
    } else {
        form_field_clear_error($("#send_refer_frm #refer_message"))
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) { }
    });
    return false
});
$(document).off("click", ".popwindow_buttons #preview_refer_email_btn").on("click", ".popwindow_buttons #preview_refer_email_btn", function (e) {
    e.preventDefault();
    var linkid = $("#send_refer_frm #linkid").val();
    var tamplateid = $("#send_refer_frm #templateId").val();
    var name = $("#send_refer_frm #refer_name").val();
    var email = $("#send_refer_frm #refer_email").val();
    var message = $("#send_refer_frm #refer_message").val();
    if ($("#send_refer_frm #refer_name").val() == "") {
        form_field_animate_error($("#send_refer_frm #refer_name"), "Please enter a name.");
        return false
    } else {
        form_field_clear_error($("#send_refer_frm #refer_name"))
    }
    if ($("#send_refer_frm #refer_message").val() == "") {
        form_field_animate_error($("#send_refer_frm #refer_message"), "Please enter a message.");
        return false
    } else {
        form_field_clear_error($("#send_refer_frm #refer_message"))
    }
    window.open("products/refer-email-link/preview/" + linkid + "/" + tamplateid + "?refer_name=" + name + "&refer_email=" + email + "&refer_message=" + message, "_blank")
});

function reload_eset_panel(cp_id) {
    var eset_panel = $("#eset_product_panel");
    $.ajaxSetup({
        global: false
    });
    url = "/en/products/eset/" + cp_id;
    if (eset_panel.length > 0) {
        $("#eset_product_panel .pathwindow_content").load(url + " #eset_product_panel .pathwindow_content > *", function () {
            eset_panel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}

$(document).off("change", ".account_type").on("change", ".account_type", function () {
    var selectedVal = "";
    var selected = $("input[type='radio'][name='account_type']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
        $("#benefit_mobile #existing_package_panel").hide();
        $("#benefit_mobile #existing_sim_panel").hide();
        $("#benefit_mobile #new_sim_panel").hide();
        $("#label_existing").removeClass("active");
        $("#label_existingsim").removeClass("active");
        $("#label_newsim").removeClass("active");
        if (selectedVal == "existing_package") {
            $("#label_existing").addClass("active");
            $("#benefit_mobile #existing_package_panel").show()
        }
        if (selectedVal == "existing_sim") {
            $("#label_existingsim").addClass("active");
            $("#benefit_mobile #existing_sim_panel").show()
        }
        if (selectedVal == "new_sim") {
            $("#label_newsim").addClass("active");
            $("#benefit_mobile #new_sim_panel").show()
        }
    }
});
$(document).off("click", "#delivery_home").on("click", "#delivery_home", function (e) {
    e.preventDefault();
    $("#delivery_work").removeClass("selected");
    $("#delivery_home").addClass("selected");
    $("#process_delivery_frm #office_building").hide();
    $("#process_delivery_frm #company_name").hide();
    $("#process_delivery_frm #home_building").show();
    $("#process_delivery_frm #contact_type").val("home")
});
$(document).off("click", "#delivery_work").on("click", "#delivery_work", function (e) {
    e.preventDefault();
    $("#delivery_home").removeClass("selected");
    $("#delivery_work").addClass("selected");
    $("#process_delivery_frm #office_building").show();
    $("#process_delivery_frm #company_name").show();
    $("#process_delivery_frm #home_building").hide();
    $("#process_delivery_frm #contact_type").val("work")
});
$(document).off("click", "#ap_delivery_details #process_delivery_btn").on("click", "#ap_delivery_details #process_delivery_btn", function (e) {
    e.preventDefault();
    var form = $("#process_delivery_frm");
    if ($("#process_delivery_frm #delivery_name").val() == "") {
        form_field_animate_error($("#process_delivery_frm #delivery_name"), "Please enter a Recipient name!");
        return false
    } else {
        form_field_clear_error($("#process_delivery_frm #delivery_name"))
    }
    if ($("#process_delivery_frm #delivery_number").val() == "") {
        form_field_animate_error($("#process_delivery_frm #delivery_number"), "Please enter a valid contact number!");
        return false
    } else {
        form_field_clear_error($("#process_delivery_frm #delivery_number"))
    }
    if ($("#process_delivery_frm #provinceSelect").val() == "") {
        form_field_animate_error($("#process_delivery_frm #provinceSelect"), "Please select a province!");
        return false
    } else {
        form_field_clear_error($("#process_delivery_frm #provinceSelect"))
    }
    if ($("#process_delivery_frm #townSelect").val() == "") {
        form_field_animate_error($("#process_delivery_frm #townSelect"), "Please select a city!");
        return false
    } else {
        form_field_clear_error($("#process_delivery_frm #townSelect"))
    }
    if ($("#process_delivery_frm #suburbSelect").val() == "") {
        form_field_animate_error($("#process_delivery_frm #suburbSelect"), "Please select a suburb!");
        return false
    } else {
        form_field_clear_error($("#process_delivery_frm #suburbSelect"))
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_benefits_panel($("#process_delivery_frm #benefit_id").val());
            setTimeout(function () {
                showPopup("Delivery Details Captured!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#activate_WFP Jordanplus #activate_benefits_btn").on("click", "#activate_WFP Jordanplus #activate_benefits_btn", function (e) {
    e.preventDefault();
    var form = $("#activate_benefits_frm");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                showPopup("Please Confirm Your Activation!", data, "info")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#manage_benefits #manage_benefits_btn").on("click", "#manage_benefits #manage_benefits_btn", function (e) {
    e.preventDefault();
    var form = $("#manage_benefits_frm");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                showPopup("Benefits Update!", data, "info")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#update_benefits_btn").on("click", "#update_benefits_btn", function (e) {
    e.preventDefault();
    closePanel();
    reload_benefits_panel($("#update_benefits_frm #benefit_id").val())
});
$(document).off("click", "#deactivateBenefitBtn").on("click", "#deactivateBenefitBtn", function (e) {
    e.preventDefault();
    var form = $("#benefits_opt_out_form");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            setTimeout(function () {
                showPopup("WFP Jordan Plus+", data, "success")
            }, 500);
            reload_benefits_panel($("#benefits_opt_out_form #benefitIdDeActivated").val())
        }
    });
    return false
});
$(document).off("click", "#reactivate_WFP Jordan_plus_btn").on("click", "#reactivate_WFP Jordan_plus_btn", function (e) {
    e.preventDefault();
    reload_benefits_panel($("#reactivate_WFP Jordan_plus_frm #product_id").val())
});
$(document).off("click", "#benefit_mobile #existing_account_btn").on("click", "#benefit_mobile #existing_account_btn", function (e) {
    e.preventDefault();
    var form = $("#benefit_mobile #existing_account_frm");
    if ($("#existing_account_frm #existing_account").val() == "") {
        form_field_animate_error($("#existing_account_frm #existing_account"), "Please select an account!");
        return false
    } else {
        form_field_clear_error($("#existing_account_frm #existing_account"))
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            reload_benefits_panel($("#existing_account_frm #benefit_id").val());
            setTimeout(function () {
                showPopup("Success!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#benefit_mobile #existing_sim_btn").on("click", "#benefit_mobile #existing_sim_btn", function (e) {
    e.preventDefault();
    var form = $("#benefit_mobile #existing_sim_frm");
    var re = /[0-9]{9}$/;
    if (!re.test($("#existing_sim_frm #existing_sim_number").val())) {
        form_field_animate_error($("#existing_sim_frm #existing_sim_number"), "Please enter valid sim number without spaces and special characters!");
        return false
    } else {
        form_field_clear_error($("#existing_sim_frm #existing_sim_number"))
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            closePanel();
            reload_benefits_panel($("#existing_sim_frm #benefit_id").val());
            setTimeout(function () {
                showPopup("Success!", data, "success")
            }, 500)
        }
    });
    return false
});
$(document).off("click", "#benefit_mobile #mobile_delivery_details_btn").on("click", "#benefit_mobile #mobile_delivery_details_btn", function (e) {
    e.preventDefault();
    var form = $("#benefit_mobile #mobile_delivery_frm");
    var benefitId = $("#mobile_delivery_frm #benefit_id").val();
    if ($("#mobile_delivery_frm #sim_type").val() == "") {
        form_field_animate_error($("#mobile_delivery_frm #sim_type"), "Please select sim type!");
        return false
    } else {
        form_field_clear_error($("#mobile_delivery_frm #sim_type"))
    }
    openPanel("/en/benefits/data/mobile/delivery/" + benefitId, false, {
        existing_mobile: $("#mobile_delivery_frm #existing_mobile").val(),
        sim_type: $("#mobile_delivery_frm #sim_type").val()
    });
    return false
});

function reload_benefits_panel(id) {
    var myBenefits = $("#benefit_view_panel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/benefits/" + id;
    if (myBenefits.length > 0) {
        $("#benefit_view_panel .pathwindow_content").load(url + " #benefit_view_panel .pathwindow_content > *", function () {
            myBenefits.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}
$(document).off("click", "#keepSimfyCheckbox").on("click", "#keepSimfyCheckbox", function () {
    "use strict";
    if ($("#keepSimfyCheckbox").prop("checked")) {
        $(".actionbuttons dt").html("De-Activate WFP Jordan Plus+")
    } else {
        $(".actionbuttons dt").html("De-Activate WFP Jordan Plus+ and Simfy")
    }
});
var simfyDisplaySignupForm = function (displayId, sideBySideHolder) {
    sideBySideHolder.find("label.active").removeClass("active");
    sideBySideHolder.find('label input[value="' + displayId + '"]').closest("label").addClass("active");
    $("div#simfy_signup_new").hide();
    $("div#simfy_signup_partner_switch").hide();
    $("div#simfy_link_existing").hide();
    $("div#" + displayId).show()
};
$(document).off("click", "#simfy_signup_form_select.holder_radio_sidebyside label input").on("click", "#simfy_signup_form_select.holder_radio_sidebyside label input", function (e) {
    e.preventDefault();
    var displayId = $(this).val();
    var sideBySideHolder = $(this).closest(".holder_radio_sidebyside");
    simfyDisplaySignupForm(displayId, sideBySideHolder)
});
$(document).off("click", "form a.simfy_signup").on("click", "form a.simfy_signup", function (e) {
    e.preventDefault();
    var form = $(this).closest("form");
    var username = form.find('input[name="username"]');
    if (username.length) {
        if (username.val() == "") {
            form_field_animate_error(username, "Please enter a username!");
            return false
        } else {
            form_field_clear_error(username)
        }
    }
    var email = form.find('input[name="email"]');
    if (email.length) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email.val())) {
            form_field_animate_error(email, "Please enter a valid email!");
            return false
        } else {
            form_field_clear_error(email)
        }
    }
    var password = form.find('input[name="password"]');
    if (password.length) {
        if (password.val() == "") {
            form_field_animate_error(password, "Please enter a valid password!");
            return false
        } else {
            form_field_clear_error(password)
        }
    }
    var passwordConfirm = form.find('input[name="confirm_password"]');
    if (passwordConfirm.length) {
        if (passwordConfirm.val() != password.val()) {
            form_field_animate_error(passwordConfirm, "Password don't not match!");
            return false
        } else {
            form_field_clear_error(passwordConfirm)
        }
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            var popupContent = " ";
            if (data.status_code == 200) {
                closePanel();
                popupContent = '<p style="width: 500px;">';
                if (form.attr("name") == "simfy_link_existing_frm") {
                    popupContent += " <h1>Your Simfy Account Has Been Bundled</h1>"
                } else {
                    popupContent += " <h1>Your Account Has Been Created!</h1>"
                }
                popupContent += "<p>" + data.message + '</p><p><div class="popwindow_buttons"> ' + '<a href="/en/simfy/sm/' + data.simfy_id + '" class="btn_styled pop_close act_panel_open">Okay</a> ' + '<div class="clear"></div></div></p></p>';
                setTimeout(function () {
                    showPopup("Success!", popupContent, "success")
                }, 500)
            } else if (data.status_code == 401) {
                popupContent = "<h1>" + data.email + " is already in use!</h1>\n";
                popupContent += "<p>We can not create a Simfy Africa account with the email <strong>" + data.email + "</strong> as the email address is already in use.</p>\n";
                popupContent += "<p>If this is your existing account, you can migrate it to WFP Jordan. " + "Alternatively, select another email address and try again. If you have any queries, please contact " + '<a href="mailto:support@WFP Jordan.com">support@WFP Jordan.com</a>.</p>\n';
                popupContent += '<div class="popwindow_buttons"><a class="btn_styled pop_close">Use Another Email</a>' + "<a class=\"btn_styled pop_close\" onclick=\"simfyDisplaySignupForm('simfy_signup_partner_switch', $('#simfy_signup_form_select.holder_radio_sidebyside')); return false;\">" + 'Migrate to WFP Jordan</a> <div class="clear"></div></div>';
                setTimeout(function () {
                    showPopup("Email Address Already In Use", '<p style="width: 500px;">' + popupContent + "</p>", "warning")
                }, 500)
            } else if (data.status_code == 402) {
                popupContent = "<h1>Looks like " + data.email + " does not exist</h1>" + "<p>We can not find the Simfy Africa account with the email <strong>" + data.email + "</strong>. " + "Please make sure you have entered the correct email address.</p>" + "<p>Alternatively, you can create a new account with this email address.</p>";
                popupContent += '<div class="popwindow_buttons"><a class="btn_styled pop_close">Check Email Address</a>' + '<a class="btn_styled pop_close" onclick="simfyDisplaySignupForm(\'simfy_signup_new\', $(\'#simfy_signup_form_select.holder_radio_sidebyside\')); return false;">Create New Account</a> <div class="clear"></div></div>';
                setTimeout(function () {
                    showPopup("Account Not Found", '<p style="width: 500px;">' + popupContent + "</p>", "warning")
                }, 500)
            } else {
                setTimeout(function () {
                    showPopup("An Error Occured!", '<p style="width: 500px;">' + data.message + "</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "warning")
                }, 500)
            }
        }
    });
    return false
});
$(document).off("click", "#cancellation_simfy_confirm_btn").on("click", "#cancellation_simfy_confirm_btn", function (e) {
    e.preventDefault();
    var form = $("#cancellation_simfy_confirm_frm");
    var simfyId = $("#cancellation_simfy_confirm_frm #simfy_id").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_simfy_panel(simfyId);
            if (data.status_code == 200) {
                setTimeout(function () {
                    showPopup("Success!", "<h1>De-Activation Submitted</h1><p>" + data.message + "</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "success")
                }, 500)
            } else {
                setTimeout(function () {
                    showPopup("An Error Occured!", "<p>" + data.message + "</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "warning")
                }, 500)
            }
        }
    });
    return false
});
$(document).off("click", "#remove_cancel_simfy_confirm_btn").on("click", "#remove_cancel_simfy_confirm_btn", function (e) {
    e.preventDefault();
    var form = $("#remove_cancel_simfy_confirm_frm");
    var simfyId = $("#remove_cancel_simfy_confirm_frm #simfy_id").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_simfy_panel(simfyId);
            if (data.status_code == 200) {
                setTimeout(function () {
                    showPopup("Success!", "<p>" + data.message + ". Click okay to continue</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "success")
                }, 500)
            } else {
                setTimeout(function () {
                    showPopup("An Error Occured!", "<p>" + data.message + "</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "warning")
                }, 500)
            }
        }
    });
    return false
});
$(document).off("click", "#reactivate_cancelled_simfy_confirm_btn").on("click", "#reactivate_cancelled_simfy_confirm_btn", function (e) {
    e.preventDefault();
    var form = $("#reactivate_cancelled_simfy_confirm_frm");
    var simfyId = $("#reactivate_cancelled_simfy_confirm_frm #simfy_id").val();
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_simfy_panel(simfyId);
            if (data.status_code == 200) {
                setTimeout(function () {
                    showPopup("Success!", "<p>" + data.message + ". Click okay to continue</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "success")
                }, 500)
            } else {
                setTimeout(function () {
                    showPopup("An Error Occured!", "<p>" + data.message + "</p><p><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div></p>", "warning")
                }, 500)
            }
        }
    });
    return false
});
$(document).off("click", "#request_simfy_btn").on("click", "#request_simfy_btn", function (e) {
    e.preventDefault();
    var form = $("#request_simfy_frm");
    if ($("#request_simfy_frm #request_title").val() == "" && $("#request_simfy_frm #request_artist").val() == "" && $("#request_simfy_frm #request_album").val() == "") {
        form_field_animate_error($("#request_simfy_frm #simfy_request"), "You need to provide at least a Title or an Artist or an Album");
        return false
    } else {
        form_field_clear_error($("#request_simfy_frm #simfy_request"))
    }
    return false
});
$(document).off("click", "#change_password_simfy_confirm_btn").on("click", "#change_password_simfy_confirm_btn", function (e) {
    e.preventDefault();
    var form = $("#password_change_simfy_confirm_frm");
    var simfyId = $("#password_change_simfy_confirm_frm #simfy_id").val();
    var password = form.find('input[name="password"]');
    if (password.length) {
        if (password.val().length < 6) {
            form_field_animate_error(password, "Your password must be at least 6 characters long");
            return false
        } else {
            form_field_clear_error(password)
        }
    }
    var passwordConfirm = form.find('input[name="confirm_password"]');
    if (passwordConfirm.length) {
        if (passwordConfirm.val() != password.val()) {
            form_field_animate_error(passwordConfirm, "Password don't not match!");
            return false
        } else {
            form_field_clear_error(passwordConfirm)
        }
    }
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            reload_simfy_panel(simfyId);
            if (data.status_code == 200) {
                setTimeout(function () {
                    showPopup("Simfy Africa Password", '<h1>Your Password has been Updated</h1><p style=\'width: 400px;\'>Your Simfy Africa password has been updated successfully! If you need any further assistance, please contact <a href="mailto:support@WFP Jordan.com">support@WFP Jordan.com</a>.</p><br /><div class="popwindow_buttons"><a class="btn_styled pop_close">Okay</a><div class="clear"></div></div>', "success")
                }, 500)
            } else {
                setTimeout(function () {
                    showPopup("Simfy Africa Password", "<h1>Something Went Wrong</h1><p style='width: 400px;' >We could not update your password at this time. Please try again in a few minutes.</p><p style='width: 400px;'>" + data.message + "</p><p>If this problem persists, please contact <a href=\"mailto:support@WFP Jordan.com\">support@WFP Jordan.com</a> with the error above.</p><br /><div class='popwindow_buttons'> <a class='btn_styled pop_close'>Okay</a> <div class='clear'></div></div>", "warning")
                }, 500)
            }
        }
    });
    return false
});

function reload_simfy_panel(id) {
    var mySimfyPanel = $("#simfy_panel.pathwindow");
    $.ajaxSetup({
        global: false
    });
    url = "/en/simfy/sm/" + id;
    if (mySimfyPanel.length > 0) {
        $("#simfy_panel .pathwindow_content").load(url + " #simfy_panel .pathwindow_content > *", function () {
            mySimfyPanel.trigger("panelShown");
            $.ajaxSetup({
                global: true
            })
        })
    }
}