﻿    $('#tabela').each(function () {

        var dimension_cells = new Array();
        var dimension_col = null;
        var columnTitle = "Month";

        var i = 0;

        $(this).find('th').each(function () {

            if ($(this).html().trim() == columnTitle) {
                dimension_col = i;
            }
            i++;
        });



        var first_instance = null;

        $(this).find('tr').each(function () {

            var dimension_td = $(this).find('td').eq(dimension_col);

            if (first_instance == null) {
                first_instance = dimension_td;
            } else if (dimension_td.text() == first_instance.text()) {

                dimension_td.remove();

                first_instance.attr('rowspan', parseInt(first_instance.attr('rowspan'), 10) + 1);
            } else {
                first_instance = dimension_td;
            }

        });
    });
