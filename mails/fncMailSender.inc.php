<?php
require_once('../phpmailer/phpmailer.php');
function authMail($to, $cc, $bcc, $subject, $message)
{
    $mail = new PHPMailer();
    $mail->IsHTML(true);
    $mail->IsSMTP();
    $mail->SMTPAuth = false;
    //$mail->SMTPSecure = "ssl";
    // sets the prefix to the servier
    $mail->Host = "10.48.67.17";
    $mail->Port = 25;
    //$mail->Username = "";
    //$mail->Password = "";
    $mail->From = "jordan.triangulationbi@wfp.org";
    $mail->Subject = $subject;
    $mail->Body = $message;
    //$mail->AddCC("member@spelloveryou.com", "Mr.Member "); //CC
    //$mail->AddBCC("member1@spelloveryou.com", "Mr.Member1 "); //CC

    for ($i=1;$i<=$to[0][0];$i++){
        $mail->AddAddress($to[$i][1], $to[$i][0]); // to Address
    }
    for ($i=1;$i<=$cc[0][0];$i++){
        $mail->AddCC($cc[$i][1], $cc[$i][0]); //CC
    }
    for ($i=1;$i<=$bcc[0][0];$i++){
        $mail->AddBCC($bcc[$i][1], $bcc[$i][0]); //CC
    }
    $mail->set('X-Priority', '1'); //Priority 1 = High, 3 = Normal, 5 = low
    $res=$mail->Send();
    print_r($res);
    return $res;
}
function fncSendEmailToUserRight($permission,$type, $subject, $message){
    $dbh=fncOpenDBConn();
    $sql = "select login,$type from access where status='Y' and usertype=1";
    $res = mssql_query($sql,$dbh);
    $rows = mssql_num_rows($res);
    $to[0][0]=0;
    $cc[0][0]=0;
    $bcc[0][0]=0;
    for ($i=1;$i<=$rows;$i++){
        $row = mssql_fetch_array($res);
        $acarray=explode("%",$row[$type]);
        if (in_array($permission,$acarray)){
            $to[0][0]++;
            $to[$to[0][0]][0]=fncGetFullName($row["login"]);
            $to[$to[0][0]][1]=$row["login"]."@wfp.org";
        }
	}
    authMail($to, $cc, $bcc, $subject, $message);
    mssql_close($dbh);
}
?>