<?php session_start();
      include("fncCashAnalyzer.inc.php");
      if(isset($_GET["action"]) && $_GET["action"]=="upload"){
          include("upload_data.php");
      }
?>
<!doctype html>
<html class="" id="jewel" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WFP Jordan Cash Transfer Triangulation Analyzer</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=960, maximum-scale=0.92" />
    <link rel="stylesheet" href="css/main.css?token=<?php echo date("Ymd") ?>" />
    <link rel="stylesheet" type="text/css" href="css/newtabs.css">
    <link rel="stylesheet" type="text/css" href="css/bullet.css">
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/style3.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.11.4.css" />
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/main_ie.css"/>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico">
    <script type="text/javascript"> if (!window.console) console = { log: function () { } }; </script>
    <script type="text/javascript" src="js/html5.js"></script>
    <script type="text/javascript" src="js/head.min.js"></script>
    <script type="text/javascript">document.getElementsByTagName('html')[0].id = 'jewel'</script>
    <script type="text/javascript" src="js/jquery-1.12.0.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="js/angular.min.js"></script>
    <script type="text/javascript" src="js/angular.framework.js"></script>
    <script type="text/javascript" src="js/MainPlugins.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script src="js/querytitles.js?token=<?php echo date("Ymdhis") ?>" type="text/javascript"></script>
    <script type="text/javascript">

        function setTab(tabId) {
            ResetTabs();
            $("#" + tabId).addClass("here");
            $("#Available_" + tabId).show();
            $("#filetype").val(tabId);
            if (tabId == "Merchant_Sales" || tabId == "Merchant_Purchases")
                $("#merchant_fields").show();
            if (tabId == "Reload_LOA") {
                $("#loa_fields").show();
                $("#file_label").html("Upload letter");
            }
            if (tabId == "VRL") {
                $("#vrl_fields").show();
            }
            if (tabId == "Paper_Vouchers") {
                $("#paper_voucher_fields").show();
            }
        }
        function ResetTabs() {
            $("#Reload_WFP").removeClass("here");
            $("#Reload_LOA").removeClass("here");
            $("#MEPS_Data").removeClass("here");
            $("#JAB").removeClass("here");
            $("#Merchant_Sales").removeClass("here");
            $("#Merchant_Purchases").removeClass("here");
            $("#Merchants_WFP").removeClass("here");
            $("#VRL").removeClass("here");
            $("#Unused_Exceptions").removeClass("here");
            $("#Paper_Vouchers").removeClass("here");
            $("#merchant_fields").hide();
            $("#loa_fields").hide();
            $("#vrl_fields").hide();
            $("#Available_Reload_WFP").hide();
            $("#Available_Reload_LOA").hide();
            $("#Available_MEPS_Data").hide();
            $("#Available_JAB").hide();
            $("#Available_Merchant_Sales").hide();
            $("#Available_Merchant_Purchases").hide();
            $("#Available_Merchants_WFP").hide();
            $("#Available_VRL").hide();
            $("#Available_Unused_Exceptions").hide();
            $("#Paper_Vouchers").hide();
            $("#file_label").html("Upload file");
        }
    </script>

    <!-- custom scripts block -->
</head>
<body>
    <ui-view>
  <div id="nav" class="structural"> <a class="home" href="en/" data-ng-click="goHome($event)">
    <dl>
      <dt><span></span>Home</dt>
      <dd></dd>
    </dl>
      <div class="emboss" style="position: absolute; left: 50%;margin-top:-5px">
        <div class="emboss4" style="position: relative; left: -50%;">
            <p style="font-family: 'Century Gothic', sans-serif;font-size:22px">Cash Transfer Triangulation Analyzer</p>
        </div>
    </div>
    </a> 
      
      <a class="logout" href="index.php">
    <dl>
      <dt><span></span>Logout</dt>
      <dd></dd>
    </dl>
    </a>       <div style="position: absolute; left: 50%;margin-top:48px">
        <div style="position: relative; left: -50%;">
            <img src="images/about.png" width="566" height="42" alt="about" usemap="#aboutmap" />
            <map name="aboutmap">
              <area shape="rect" coords="0,0,288,42" href="main.php?panel=about_programme.php" alt="about the programme">
              <area shape="rect" coords="288,0,566,42" href="main.php?panel=about_database.php" alt="about the database">
            </map>
        </div>
    </div></div>
  <div id="home" class="structural">
    <div class="wrapper window">
      <div id="logo" class="animated fadeInDown">WFP Jordan Cash Analyzer</div>
      <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
         
      <ul id="zoneselectors" class="large">
        <dl class="connectivity">
          <a class="act_panel_open" href="dashboard.php">
          <dt>Dashboard</dt>
          <dd>Data visualization displaying the current status of metrics</dd>
          </a>
        </dl>
        <dl class="hosting">
          <a class="act_panel_open" href="<?php echo fncPermLink("import.php","UP","main") ?>">
          <dt>Import Data</dt>
          <dd>Import data from CSV files into the online database</dd>
          </a>
        </dl>
        <dl class="billing">
          <a class="act_panel_open" href="<?php echo fncPermLink("analytics.php","QR","main") ?>">
          <dt>Reports</dt>
          <dd>View key perfomance indicators.</dd>
          </a>
        </dl>
        <dl class="support">
          <a class="act_panel_open" href="<?php echo fncPermLink("data_management.php","DA","main") ?>">
          <dt>Data Management</dt>
          <dd>View and manage data</dd>
          </a>
        </dl>
        <dl class="other">
          <a class="act_panel_open" href="create_report.php">
          <dt>Create Own Report</dt>
          <dd>Create your own report from SAP HANA or the Triangulation Database</dd>
          </a>
        </dl>
        <dl class="account">
          <a class="act_panel_open" href="portals.php">
          <dt>Special Portals</dt>
          <dd>Specialised portals with data management and reports</dd>
          </a>
        </dl>
        <div class="clear"></div>
      </ul>
      <div class="clear"></div>
      <a id="message_notifier" class="act_panel_open"
       href="notices.php"><span></span>
      <div class="unread">0</div>
      </a> </div>
    <div id="footer"><img src="images/wfp_white_transparent.png" alt="logo" /></div>
  </div>
  <div id="panels"></div>
  <div id="blackout" class="structural"></div>
  <div id="temp"></div>
  <div id="loader" class="animated pop" style="display: none">
    <div></div>
    processing your request </div>
</ui-view>
    <?php
	if(isset($_GET["panel"])){
		$url=$_GET["panel"];
	    echo "<script>
			openPanel('$url');
		</script>";
	}
    ?>
    <ul id="bkslideshow" class="cb-slideshow">
        <li><span>Image 01</span></li>
        <li><span>Image 02</span></li>
        <li><span>Image 03</span></li>
        <li><span>Image 04</span></li>
        <li><span>Image 05</span></li>
        <li><span>Image 06</span></li>
    </ul>
</body>
</html>
