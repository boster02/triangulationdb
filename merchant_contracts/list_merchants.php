<?php function select()
      {
          global $a;
          global $showrecs;
          global $page;
          global $filter;
          global $filterfield;
          global $wholeonly;
          global $order;
          global $ordtype;


          if ($a == "reset") {
              $filter = "";
              $filterfield = "";
              $wholeonly = "";
              $order = "";
              $ordtype = "";
          }

          $checkstr = "";
          if ($wholeonly) $checkstr = " checked";
          if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
          $res = sql_select();
          $count = sql_getrecordcount();
          if ($count % $showrecs != 0) {
              $pagecount = intval($count / $showrecs) + 1;
          }
          else {
              $pagecount = intval($count / $showrecs);
          }
          $startrec = $showrecs * ($page - 1);
          if ($startrec < $count) {sqlsrv_fetch($res, $startrec);}
          $reccount = min($showrecs * $page, $count);
?>
<style type="text/css">
    td a {
        color: white;
        text-align: left;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-md-10">
            <form action="merchants.php" method="post">
                <table class="filter-table" border="0" cellspacing="1" cellpadding="4">
                    <tr>
                        <td><b>Custom Filter</b>&nbsp;</td>
                        <td>
                            <input type="text" name="filter" value="<?php echo $filter ?>"></td>
                        <td>
                            <select name="filter_field">
                                <option value="">All Fields</option>
                                <option value="<?php echo "acc_name" ?>"<?php if ($filterfield == "acc_name") { echo "selected"; } ?>><?php echo htmlspecialchars("Account Name") ?></option>
                                <option value="<?php echo "wfp_name" ?>"<?php if ($filterfield == "wfp_name") { echo "selected"; } ?>><?php echo htmlspecialchars("Trading Name (WFP Name)") ?></option>
                                <option value="<?php echo "address" ?>"<?php if ($filterfield == "address") { echo "selected"; } ?>><?php echo htmlspecialchars("Address") ?></option>
                                <option value="<?php echo "lp_branch" ?>"<?php if ($filterfield == "lp_branch") { echo "selected"; } ?>><?php echo htmlspecialchars("Branch") ?></option>
                                <option value="<?php echo "lp_governorate" ?>"<?php if ($filterfield == "lp_governorate") { echo "selected"; } ?>><?php echo htmlspecialchars("Governorate") ?></option>
                                <option value="<?php echo "lp_city" ?>"<?php if ($filterfield == "lp_city") { echo "selected"; } ?>><?php echo htmlspecialchars("City") ?></option>
                                <option value="<?php echo "owner" ?>"<?php if ($filterfield == "owner") { echo "selected"; } ?>><?php echo htmlspecialchars("Owner") ?></option>
                                <option value="<?php echo "focal_point" ?>"<?php if ($filterfield == "focal_point") { echo "selected"; } ?>><?php echo htmlspecialchars("Focal Point") ?></option>
                                <option value="<?php echo "tel" ?>"<?php if ($filterfield == "tel") { echo "selected"; } ?>><?php echo htmlspecialchars("Phone") ?></option>
                                <option value="<?php echo "email" ?>"<?php if ($filterfield == "email") { echo "selected"; } ?>><?php echo htmlspecialchars("Email") ?></option>
                                <option value="<?php echo "financial_statements" ?>"<?php if ($filterfield == "financial_statements") { echo "selected"; } ?>><?php echo htmlspecialchars("Financial Statements Available") ?></option>
                                <option value="<?php echo "open_hours" ?>"<?php if ($filterfield == "open_hours") { echo "selected"; } ?>><?php echo htmlspecialchars("Opening Hour") ?></option>
                                <option value="<?php echo "close_hours" ?>"<?php if ($filterfield == "close_hours") { echo "selected"; } ?>><?php echo htmlspecialchars("Closing Hour") ?></option>
                                <option value="<?php echo "lp_status" ?>"<?php if ($filterfield == "lp_status") { echo "selected"; } ?>><?php echo htmlspecialchars("Status") ?></option>
                                <option value="<?php echo "contract_date_from" ?>"<?php if ($filterfield == "contract_date_from") { echo "selected"; } ?>><?php echo htmlspecialchars("Contract Begin Date") ?></option>
                                <option value="<?php echo "contract_date_to" ?>"<?php if ($filterfield == "contract_date_to") { echo "selected"; } ?>><?php echo htmlspecialchars("Contract End Date") ?></option>
                                <option value="<?php echo "picture_of_store" ?>"<?php if ($filterfield == "picture_of_store") { echo "selected"; } ?>><?php echo htmlspecialchars("Picture of store front") ?></option>
                                <option value="<?php echo "contract_file" ?>"<?php if ($filterfield == "contract_file") { echo "selected"; } ?>><?php echo htmlspecialchars("Contract File") ?></option>
                            </select></td>
                        <td>
                            <input type="checkbox" name="wholeonly"<?php echo $checkstr ?>>Whole words only</td>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" name="action" value="Apply Filter"></td>
                        <td><a href="merchants.php?a=reset">Reset Filter</a></td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-sm-2 col-md-2" style="text-align: right">
            <a href="merchants.php?a=add" class="btn btn-primary">Add New Merchant</a>
        </div>
    </div>
    <hr size="1" noshade />
    <div class="csstable">
        <h3>
            <a style="margin-top:10px" class="green-btn" href="javascript:exportDetails()">Export retailer list to Excel</a>
        </h3>
        <table class="tbl" border="0" cellspacing="1" cellpadding="5" width="100%">
            <tr>
                <td class="hr"><a class="hr" href="merchants.php?order=<?php echo "id" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Retailer ID") ?></a></td>
                <td class="hr"><a class="hr" href="merchants.php?order=<?php echo "acc_name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Account Name") ?></a></td>
                <td class="hr"><a class="hr" href="merchants.php?order=<?php echo "wfp_name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Trading Name (WFP Name)") ?></a></td>
                <td class="hr"><a class="hr" href="merchants.php?order=<?php echo "lp_branch" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Branch") ?></a></td>
                <td class="hr"><a class="hr" href="merchants.php?order=<?php echo "lp_governorate" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Governorate") ?></a></td>
                <td class="hr"><a class="hr" href="merchants.php?order=<?php echo "address" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Address") ?></a></td>
                <td class="hr">&nbsp;</td>
                <td class="hr">&nbsp;</td>
                <td class="hr">&nbsp;</td>
            </tr>
            <?php
          for ($i = $startrec; $i < $reccount; $i++)
          {
              $row = mssql_fetch_array($res);
            ?>
            <tr>
                <td><?php echo htmlspecialchars($row["id"]) ?></td>
                <td><?php echo htmlspecialchars($row["acc_name"]) ?></td>
                <td><?php echo htmlspecialchars($row["wfp_name"]) ?></td>
                <td><?php echo htmlspecialchars($row["lp_branch"]) ?></td>
                <td><?php echo htmlspecialchars($row["lp_governorate"]) ?></td>
                <td><?php echo htmlspecialchars($row["address"]) ?></td>
                <td style="width: 18px"><a href="merchants.php?a=view&recid=<?php echo $i ?>">
                    <img src="../images/view.png" alt="view" /></a></td>
                <td style="width: 18px"><a href="merchants.php?a=edit&recid=<?php echo $i ?>">
                    <img src="../images/edit-bw.png" alt="edit" /></a></td>
                <td style="width: 18px"><a href="merchants.php?a=del&recid=<?php echo $i ?>">
                    <img src="../images/delete.png" alt="delete" /></a></td>
            </tr>
            <?php
          }
          
            ?>
        </table>
        <br />
    </div>
</div>
<script>
    function exportDetails() {
        $('#loadingmessage').show();
        var sql = "SELECT * FROM (SELECT t1.id, t1.acc_name, t1.wfp_name, t1.retailer_id, t1.discount_rate, t1.address, t1.branch, lp4.lut_branches AS lp_branch, t1.governorate, lp5.governorate AS lp_governorate, t1.city, lp6.city AS lp_city, t1.owner, t1.focal_point, t1.tel, t1.email, t1.financial_statements, t1.open_hours, t1.close_hours, t1.status, lp14.status AS lp_status, t1.contract_date_from, t1.contract_date_to, t1.picture_of_store, t1.contract_file FROM merchants_wfp AS t1 LEFT OUTER JOIN lut_branches AS lp4 ON (t1.branch = lp4.lut_branches) LEFT OUTER JOIN lut_governorates AS lp5 ON (t1.governorate = lp5.governorate) LEFT OUTER JOIN lut_cities AS lp6 ON (t1.city = lp6.city) LEFT OUTER JOIN lut_status AS lp14 ON (t1.status = lp14.id)) subq";
        $.ajax({
            url: "../analytics/export_data.php?sql=" + sql + "&tablename=merchant_contracts",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
<?php } ?>