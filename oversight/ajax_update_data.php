<?php

include "fncOversight.inc.php";
if(isset($_POST["ID"])){
    $id=$_POST["ID"];
    $res=fncSaveAuditTracking($id,sqlvalue2($_POST["Due_date"],true),sqlvalue2($_POST["Actions_Taken"],true),
        sqlvalue2($_POST["Responsible"],true),sqlvalue2($_POST["Status"],true),sqlvalue2($_POST["Proof_Document"],true));
    if($res)
        fncDeliverResponse("200","The record was successfully saved",$id);
    else
        fncDeliverResponse("400","An error occured while updating the record",$id);
}
else{
    fncDeliverResponse("400","An error occured while updating the record","");
}