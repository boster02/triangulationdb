<?php
include "../fncCheckLogin.inc.php";
include "fncOversight.inc.php";
$data=fncGetAuditTracking();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="Boster Sibande" />
    <title>CBT Triangulation - Audit Tracking</title>
    <link rel="icon" href="../images/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../css/jquery-ui-1.11.4.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.11.1.js"></script>
    <script src="../js/jquery-ui-1.11.4.js"></script>
    <script src="../js/bootstrap.js"></script>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="container-fluid" style="background-color: #1f90ff">
                <img src="../../images/WFP_logo_white.gif" alt="logo" />
            </div>
            <div class="container-fluid" style="background-color: #72b5f8; margin-top: 1px; padding-top: 2px; padding-bottom: 2px">
                <span style="color:white" class="fla-breadcrumb">
                    <a style="color:white">&nbsp;&nbsp;Home</a>
                </span>
                <span style="color:white" class="pull-right">
                    <a href="/index.php" style="color:white">Logout&nbsp;&nbsp;</a>
                </span>
            </div>
        </nav>
        <div id="page-wrapper">
            <h3 style="text-align:center;font-weight:bold">OVERSIGHT COMMITTEE AUDIT TRACKING</h3>
            <div class="csstable">
                <table>
                    <tr>
                        <td></td>
                        <td>Audit</td>
                        <td>No</td>
                        <td>Priority</td>
                        <td>Recommendation</td>
                        <td>Agreed Task</td>
                        <td>Due Date</td>
                        <td>Actions Taken</td>
                        <td>Responsible</td>
                        <td>Status</td>
                        <td>Proof Document</td>
                    </tr>
                    <?php for($i=1;$i<=$data[0][0];$i++){?>
                    <tr>
                        <td style="width:18px">
                            <a href="javascript:edit(<?php echo $data[$i]["ID"] ?>)">
                                <img src="../images/edit-bw.png" alt="edit" />
                            </a>
                        </td>
                        <td>
                            <?php echo $data[$i]["Audit"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["No"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["Priority"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["Recommendation"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["Agreed_Task"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["Due_date"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["Actions_Taken"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["Responsible"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["Status"] ?>
                        </td>
                        <td>
                            <?php echo $data[$i]["Proof_Document"] ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update_data" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:880px">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">
                        Update Recommendation
                    </h3>
                </div>
                <div class="modal-body">
                    <div id='loader2' style='display: none; width: 100%; text-align: center'>
                        <img src='../images/download.gif' />
                    </div>
                    <form id="form1" name="form1" method="post">
                        <input type="hidden" id="ID" name="ID" />
                        <table class="table">
                            <tr>
                                <td>Audit</td>
                                <td>
                                    <span id="Audit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>No</td>
                                <td>
                                    <span id="No"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Priority</td>
                                <td>
                                    <span id="Priority"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Recommendation</td>
                                <td>
                                    <span id="Recommendation"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Agreed Task</td>
                                <td>
                                    <span id="Agreed_Task"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Due Date</td>
                                <td>
                                    <input type="text" class="form-control" name="Due_date" id="Due_date" />
                                </td>
                            </tr>
                            <tr>
                                <td>Actions Taken</td>
                                <td>
                                    <textarea class="form-control" name="Actions_Taken" id="Actions_Taken"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>Responsible</td>
                                <td>
                                    <input type="text" class="form-control" name="Responsible" id="Responsible" />
                                </td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    <input type="radio" name="radStatus" value="Completed" onchange="changeStatus(this.value,true)" />Completed
                                    <br />
                                    <input type="radio" name="radStatus" value="Update Needed" onchange="changeStatus(this.value,true)" />Update Needed
                                    <br />
                                    <input type="radio" name="radStatus" value="Other" onchange="changeStatus(this.value,true)" />Other
                                    <br />
                                    <input type="text" class="form-control" name="txtStatus" id="txtStatus" onchange="editStatus()" />
                                    <input type="hidden" id="Status" name="Status" />
                                </td>
                            </tr>
                            <tr>
                                <td>Proof Document</td>
                                <td>
                                    <textarea type="text" class="form-control" name="Proof_Document" id="Proof_Document"></textarea>
                                </td>
                            </tr>
                        </table>
                        <input class="btn btn-success" type="button" name="saveclose" id="saveclose" value="Save Updates" style="width:200px" onclick="saveRecord()" />
                        <input class="btn btn-danger" type="button" name="cancel" id="cancel" value="Cancel" style="width:200px" onclick="hideForm()" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function edit(id) {
            reset();
            $('#update_data').modal({
                backdrop: 'static',
                keyboard: false
            });
            if (id > 0) {
                $.post("ajax_get_data.php?id=" + id, function (data, status) {
                    debugger;
                    var obj = JSON.parse(data);
                    $("#ID").val(obj.ID);
                    $("#Audit").html(obj.Audit);
                    $("#No").html(obj.No);
                    $("#Priority").html(obj.Priority);
                    $("#Recommendation").html(obj.Recommendation);
                    $("#Agreed_Task").html(obj.Agreed_Task);
                    $("#Due_date").val(obj.Due_date);
                    $("#Actions_Taken").val(obj.Actions_Taken);
                    $("#Responsible").val(obj.Responsible);
                    changeStatus(obj.Status, false);
                    $("#Proof_Document").val(obj.Proof_Document);
                    $('#Due_date').datepicker({
                        dateFormat: 'yy-mm-dd'
                    });
                });
            }
        }
        function changeStatus(value, click) {
            $("#txtStatus").hide();
            $("#Status").val(value);
            if (value == "Completed" || value == "Update Needed") {
                form1.radStatus.value = value;
            }
            else {
                form1.radStatus.value = "Other";
                $("#txtStatus").show();
                if (click)
                    $("#txtStatus").val("");
                else
                    $("#txtStatus").val(value);
            }
        }
        function editStatus() {
            $("#Status").val($("#txtStatus").val());
        }
        function saveRecord() {
            $("#loader2").show();
            debugger;
            var id = $("#ID").val();
            var url = "ajax_update_data.php?id=" + id;
            $.ajax({
                type: "POST",
                url: url,
                data: $('form').serialize(),
                success: function (data) {
                    debugger;
                    var msg = JSON.parse(data);
                    if (msg.status == "200") {
                        alert("The recommendation has been successfully updated");
                        hideForm();
                    }
                    else {
                        alert(msg.StatusMessage);
                    }
                    $("#loader2").hide();
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#loader2").hide();
                    alert(err.Message);
                }
            });
        }
        function hideForm() {
            reset();
            $('#update_data').modal('hide');
            location.reload();
        }
        function reset() {
            $("#Audit").html("");
            $("#No").html("");
            $("#Priority").html("");
            $("#Recommendation").html("");
            $("#Agreed_Task").html("");
            $("#Due_date").val("");
            $("#Actions_Taken").val("");
            $("#Responsible").val("");
            $("#Proof_Document").val("");
            $('#Due_Date').val("");
        }
    </script>
</body>
</html>
