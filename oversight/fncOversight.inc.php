<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('MAX_EXECUTION_TIME', 900);
ini_set('memory_limit', '-1');
include("../config.inc.php");
include("../mails/fncMailSender.inc.php");
function fncSendDailyReport($today,$monthend){
    $sql="select Recommendation,Agreed_Task,Responsible,status,format(convert(date,due_date),'dd-MMM-yy') as Due_Date from Oversight_Tracking where Due_date<'$today' and status='Update Needed';";
    $sql2="select Recommendation,Agreed_Task,Responsible,status,format(convert(date,due_date),'dd-MMM-yy') as Due_Date from Oversight_Tracking where Due_date<='$monthend' and status='Update Needed'
            and Recommendation not in (select Recommendation from Oversight_Tracking where Due_date<'$today' and status='Update Needed');";
    $txt="<div style='font-family:Calibri'><h2>Audit action update as of $today</h2>";
    $txt.="<div style='font-family:Calibri;background-color:red;color:white'><h3>Overdue issues</h3>";
    $txt.=createTable($sql);
    $txt.="<div style='font-family:Calibri;background-color:yellow;color:black'><h3>Issues due this month</h3>";
    $txt.=createTable($sql2);
    $txt.="<p><em>Please do not reply to this email. It comes from an automated system. For details contact Claire Conan or Mehpara Hunzai.</em></p>";

    $success=fncSendEmailToAdmins("Audit Action Update for $today",$txt);
    return $success;
}
function fncSendEmailToAdmins($subject,$message){
    $dbh=fncOpenDBConn();
    $sql ="select name,email from oversight_admins where status='Y'";
    $res = mssql_query($sql,$dbh);
    $to[0][0]=0;
    $cc[0][0]=0;
    $bcc[0][0]=0;
    $to[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$to[0][0];$i++){
        $row = mssql_fetch_array($res);
        $to[$i][0]=$row["name"];
        $to[$i][1]=$row["email"];
	}
    mssql_close($dbh);
    return authMail($to, $cc, $bcc, $subject, $message);
}
function createTable($sql) {
    $dbh=fncOpenDBConn();
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $flag = false;
    $txt="";
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);
        if(!$flag) {
            $txt="<table border='1' cellpadding='3' style='border-collapse:collapse'>";
            $style='style="background-color: #003366;color: white;text-align: left"';
            $txt.="<tr><th $style>".implode("</th><th $style>", array_keys($row)) . "</th></tr>";
            $txt=str_replace("_"," ",$txt);
            $flag = true;
        }
        $txt.="\r\n<tr><td>".implode("</td><td>", array_values($row)) . "</td></tr>";
    }
    $txt.="    </table>";
    return $txt;
}
function fncGetAuditTracking(){
    $dbh=fncOpenDBConn();
    $sql="select * from oversight_tracking";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetAuditTrackingById($id){
    $dbh=fncOpenDBConn();
    $sql="select * from oversight_tracking where id=".$id;
    $res = mssql_query($sql,$dbh);
    $data=array();
    if(mssql_num_rows($res)>0){
        $row=mssql_fetch_assoc($res);
        $data=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncSaveAuditTracking($id,$due_date,$actions_taken,$responsible,$status,$proof_document){
    $dbh=fncOpenDBConn();
    $sql="update oversight_tracking set due_date=$due_date,actions_taken=$actions_taken,
            responsible=$responsible,status=$status,proof_document=$proof_document where id=".$id;
    $res = mssql_query($sql,$dbh);
    return $res;
}
