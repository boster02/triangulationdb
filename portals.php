<?php session_start();
      include("fncCashAnalyzer.inc.php");
?>
<style>
    #menu ul {
        margin-left: 20px;
        list-style-image: url('images/data_icon.png');
    }

        #menu ul li {
            font-size: 16px;
            font-family: 'Trebuchet MS', Geneva, sans-serif;
        }
</style>
<div id="my_connectivity_panel" class="pathwindow connectivity active">
    <div class="pathwindow_title">
        <div class="icon"></div>
        <a class="btn_back"><span></span>
            <p>Back</p>
        </a>
        <h1>Data Management</h1>
    </div>
    <div class="pathwindow_content">
        <div class="arrow-list" id="menu">
            <ul>
                <li><a href="price_monitoring">Price Monitoring</a></li>
                <li>
                    <a href="shop_monitoring/shop_monitoring.php">On-site shop monitoring</a>
                </li>
                <li>
                    <a href="shop_monitoring/performance">Shop perfomance monitoring</a>
                </li>
                <li>
                    <a href="/CallCenter">Call Center CRM</a>
                </li>
                <li>
                    <a href="<?php echo fncPermLink("javascript:userManagement()","US","acsys") ?>">User Management</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<script>
    function userManagement() {
        window.location = "https://cbttriangulation.jor.wfp.org/user_loader.php";
    }
</script>

