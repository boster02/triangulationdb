<?php function showroweditor($row, $iseditmode)
      {
          global $conn;
?>
<table class="table table-striped table-bordered" border="0" style="width:700px">
    <tr>
        <td class="hr"><?php echo htmlspecialchars("File Name")."&nbsp;" ?></td>
        <td class="dr">
            <input class="form-control" type="text" name="File_Name" maxlength="50" value="<?php echo str_replace("�","&#39", trim($row["File_Name"])) ?>" /></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Title")."&nbsp;" ?></td>
        <td class="dr">
            <input class="form-control" type="text" name="Title" maxlength="255" value="<?php echo str_replace("�","&#39", trim($row["Title"])) ?>" /></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Description")."&nbsp;" ?></td>
        <td class="dr">
            <textarea class="form-control"  name="Description" rows="4"><?php echo str_replace("�","&#39", trim($row["Description"])) ?></textarea></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Responsible Unit")."&nbsp;" ?></td>
        <td class="dr">
            <select class="form-control" name="Responsible_Unit">
                <option value=""></option>
                <?php
          $sql = "select ID, Unit from units";
          $res = mssql_query($sql, $conn);

          while ($lp_row = mssql_fetch_assoc($res)){
              $val = $lp_row["ID"];
              $caption = $lp_row["Unit"];
              if ($row["Responsible_Unit"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
</table>
<?php } ?>
