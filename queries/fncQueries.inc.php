<?php 
include("../fncDataFunctions.inc.php");
?>

<?php function showpagenav($page, $pagecount)
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
    <tr>
        <td><a href="queries.php?a=add">Add Record</a>&nbsp;</td>
        <?php if ($page > 1) { ?>
        <td><a href="queries.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Prev</a>&nbsp;</td>
        <?php } ?>
        <?php
          global $pagerange;

          if ($pagecount > 1) {

              if ($pagecount % $pagerange != 0) {
                  $rangecount = intval($pagecount / $pagerange) + 1;
              }
              else {
                  $rangecount = intval($pagecount / $pagerange);
              }
              for ($i = 1; $i < $rangecount + 1; $i++) {
                  $startpage = (($i - 1) * $pagerange) + 1;
                  $count = min($i * $pagerange, $pagecount);

                  if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
                      for ($j = $startpage; $j < $count + 1; $j++) {
                          if ($j == $page) {
        ?>
        <td><b><?php echo $j ?></b></td>
        <?php } else { ?>
        <td><a href="queries.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
        <?php }
                      }
                  } else { ?>
        <td><a href="queries.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
        <?php }
              }
          } ?>
        <?php if ($page < $pagecount) { ?>
        <td>&nbsp;<a href="queries.php?page=<?php echo $page + 1 ?>">Next&nbsp;&gt;&gt;</a>&nbsp;</td>
        <?php } ?>
    </tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
    <tr>
        <td><a href="queries.php">List of Queries</a></td>
    </tr>
</table>
<hr size="1" noshade>
<?php } ?>

<?php function addrec()
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
    <tr>
        <td><a href="queries.php">Index Page</a></td>
    </tr>
</table>
<hr size="1" noshade>
<form enctype="multipart/form-data" action="queries.php" method="post">
    <p>
        <input type="hidden" name="sql" value="insert"></p>
    <?php
          $row = array(
            "File_Name" => "",
            "Title" => "",
            "Description" => "",
            "Responsible_Unit" => "");
          showroweditor($row, false);
    ?>
    <p>
        <input type="submit" name="action" value="Post"></p>
</form>
<?php } ?>

<?php function viewrec($recid)
      {
          $res = sql_select();
          $count = sql_getrecordcount();
          $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
          $row = mssql_fetch_assoc($res);
          showrecnav("view", $recid, $count);
?>
<br>
<?php showrow($row, $recid) ?>
<br>
<hr size="1" noshade>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
    <tr>
        <td><a href="queries.php?a=add">Add Record</a></td>
        <td><a href="queries.php?a=edit&recid=<?php echo $recid ?>">Edit Record</a></td>
        <td><a href="queries.php?a=del&recid=<?php echo $recid ?>">Delete Record</a></td>
    </tr>
</table>
<?php
          
      } ?>

<?php function editrec($recid)
      {
          $res = sql_select();
          $count = sql_getrecordcount();
          $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
          $row = mssql_fetch_assoc($res);
          showrecnav("edit", $recid, $count);
?>
<br>
<form enctype="multipart/form-data" action="queries.php" method="post">
    <input type="hidden" name="sql" value="update">
    <input type="hidden" name="xid" value="<?php echo $row["id"] ?>">
    <?php showroweditor($row, true); ?>
    <p>
        <input type="submit" name="action" value="Post"></p>
</form>
<?php
          
      } ?>

<?php function deleterec($recid)
      {
          $res = sql_select();
          $count = sql_getrecordcount();
          $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
          $row = mssql_fetch_assoc($res);
          showrecnav("del", $recid, $count);
?>
<br>
<form action="queries.php" method="post">
    <input type="hidden" name="sql" value="delete">
    <input type="hidden" name="xid" value="<?php echo $row["id"] ?>">
    <?php showrow($row, $recid) ?>
    <p>
        <input type="submit" name="action" value="Confirm"></p>
</form>
<?php
          
      } ?>

<?php function sql_select()
      {
          global $conn;
          global $order;
          global $ordtype;
          global $filter;
          global $filterfield;
          global $wholeonly;

          $filterstr = sqlstr($filter);
          if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
          $sql = "SELECT * FROM (SELECT t1.id, t1.File_Name, t1.Title, t1.Description, t1.Responsible_Unit, lp3.Unit AS lp_Responsible_Unit FROM queries AS t1 LEFT OUTER JOIN units AS lp3 ON (t1.Responsible_Unit = lp3.ID)) subq";
          if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
              $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
          } elseif (isset($filterstr) && $filterstr!='') {
              $sql .= " where (File_Name like '" .$filterstr ."') or (Title like '" .$filterstr ."') or (Description like '" .$filterstr ."') or (lp_Responsible_Unit like '" .$filterstr ."')";
          }
          if (isset($order) && $order!='') $sql .= " order by " .sqlstr($order) ."";
          if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
          $res = mssql_query($sql, $conn);
          return $res;
      }

      function sql_getrecordcount()
      {
          global $conn;
          global $order;
          global $ordtype;
          global $filter;
          global $filterfield;
          global $wholeonly;

          $filterstr = sqlstr($filter);
          if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
          $sql = "SELECT COUNT(*) FROM (SELECT t1.File_Name, t1.Title, t1.Description, t1.Responsible_Unit, lp3.Unit AS lp_Responsible_Unit FROM queries AS t1 LEFT OUTER JOIN units AS lp3 ON (t1.Responsible_Unit = lp3.ID)) subq";
          if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
              $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
          } elseif (isset($filterstr) && $filterstr!='') {
              $sql .= " where (File_Name like '" .$filterstr ."') or (Title like '" .$filterstr ."') or (Description like '" .$filterstr ."') or (lp_Responsible_Unit like '" .$filterstr ."')";
          }
          $res = mssql_query($sql, $conn);
          $row = mssql_fetch_assoc($res);
          reset($row);
          return current($row);
      }

      function sql_insert()
      {
          global $conn;
          global $_POST;

          $sql = "insert into queries (File_Name, Title, Description, Responsible_Unit) values (" .sqlvalue(@$_POST["File_Name"], true).", " .sqlvalue(@$_POST["Title"], true).", " .sqlvalue(@$_POST["Description"], true).", " .sqlvalue(@$_POST["Responsible_Unit"], false).")";
          mssql_query($sql, $conn);
      }

      function sql_update()
      {
          global $conn;
          global $_POST;

          $sql = "update queries set File_Name=" .sqlvalue(@$_POST["File_Name"], true).", Title=" 
              .sqlvalue(@$_POST["Title"], true).", Description=" .sqlvalue(@$_POST["Description"], true)
              .", Responsible_Unit=" .sqlvalue(@$_POST["Responsible_Unit"], false) 
              ." where " .primarykeycondition();
          mssql_query($sql, $conn);
      }

      function sql_delete()
      {
          global $conn;

          $sql = "delete from queries where " .primarykeycondition();
          mssql_query($sql, $conn);
      }
      function primarykeycondition()
      {
          global $_POST;
          $pk = "";
          $pk .= "(id";
          if (@$_POST["xid"] == "") {
              $pk .= " IS NULL";
          }else{
              $pk .= " = " .sqlvalue(@$_POST["xid"], false);
          };
          $pk .= ")";
          return $pk;
      }
?>