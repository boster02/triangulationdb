<?php function select()
      {
          global $a;
          global $showrecs;
          global $page;
          global $filter;
          global $filterfield;
          global $wholeonly;
          global $order;
          global $ordtype;


          if ($a == "reset") {
              $filter = "";
              $filterfield = "";
              $wholeonly = "";
              $order = "";
              $ordtype = "";
          }

          $checkstr = "";
          if ($wholeonly) $checkstr = " checked";
          if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
          $res = sql_select();
          $count = sql_getrecordcount();
          if ($count % $showrecs != 0) {
              $pagecount = intval($count / $showrecs) + 1;
          }
          else {
              $pagecount = intval($count / $showrecs);
          }
          $startrec = $showrecs * ($page - 1);
          $reccount = min($showrecs * $page, $count);
?>
<style type="text/css">
    td a {
        color: white;
        text-align: left;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-md-10">
            <form action="queries.php" method="post">
                <table class="filter-table" border="0" cellspacing="1" cellpadding="4">
                    <tr>
                        <td><b>Custom Filter</b>&nbsp;</td>
                        <td>
                            <input type="text" name="filter" value="<?php echo $filter ?>"></td>
                        <td>
                            <select name="filter_field">
                                <option value="">All Fields</option>
                                <option value="<?php echo "File_Name" ?>"<?php if ($filterfield == "File_Name") { echo "selected"; } ?>><?php echo htmlspecialchars("File Name") ?></option>
                                <option value="<?php echo "Title" ?>"<?php if ($filterfield == "Title") { echo "selected"; } ?>><?php echo htmlspecialchars("Title") ?></option>
                                <option value="<?php echo "Description" ?>"<?php if ($filterfield == "Description") { echo "selected"; } ?>><?php echo htmlspecialchars("Description") ?></option>
                                <option value="<?php echo "lp_Responsible_Unit" ?>"<?php if ($filterfield == "lp_Responsible_Unit") { echo "selected"; } ?>><?php echo htmlspecialchars("Responsible Unit") ?></option>
                            </select></td>
                        <td>
                            <input type="checkbox" name="wholeonly"<?php echo $checkstr ?>>Whole words only</td>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" name="action" value="Apply Filter"></td>
                        <td><a href="queries.php?a=reset">Reset Filter</a></td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-sm-2 col-md-2" style="text-align: right">
            <a href="merchants.php?a=add" class="btn btn-primary">Add New Query</a>
        </div>
    </div>
    <hr size="1" noshade />
    <div class="csstable">
        <table class="tbl" border="0" cellspacing="1" cellpadding="5" width="100%">
            <tr>
                <td class="hr"></td>
                <td class="hr"><a class="hr" href="queries.php?order=<?php echo "Title" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Title") ?></a></td>
                <td class="hr"><a class="hr" href="queries.php?order=<?php echo "Description" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Description") ?></a></td>
                <td class="hr"><a class="hr" href="queries.php?order=<?php echo "File_Name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("File Name") ?></a></td>
                <td class="hr"><a class="hr" href="queries.php?order=<?php echo "lp_Responsible_Unit" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Responsible Unit") ?></a></td>
                <td class="hr">&nbsp;</td>
                <td class="hr">&nbsp;</td>
                <td class="hr">&nbsp;</td>
            </tr>
            <?php
          for ($i = $startrec; $i < $reccount; $i++)
          {
              $row = mssql_fetch_assoc($res);
              $style = "dr";
              if ($i % 2 != 0) {
                  $style = "sr";
              }
            ?>
            <tr>
                <td class="<?php echo $style ?>"><?php echo $i+1 ?></td>
                <td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Title"]) ?></td>
                <td class="<?php echo $style ?>"><?php echo str_replace("�","&#39",$row["Description"]) ?></td>
                <td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["File_Name"]) ?></td>
                <td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_Responsible_Unit"]) ?></td>
                <td style="width: 18px"><a href="queries.php?a=view&recid=<?php echo $i-1 ?>">
                    <img src="../images/view.png" alt="view" /></a></td>
                <td style="width: 18px"><a href="queries.php?a=edit&recid=<?php echo $i-1 ?>">
                    <img src="../images/edit-bw.png" alt="edit" /></a></td>
                <td style="width: 18px"><a href="queries.php?a=del&recid=<?php echo $i-1 ?>">
                    <img src="../images/delete.png" alt="delete" /></a></td>
            </tr>
            <?php
          }
            ?>
        </table>
        <br />
    </div>
</div>
<?php showpagenav($page, $pagecount); ?>
<?php } ?>
