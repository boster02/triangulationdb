<?php session_start();
      include("fncQueries.inc.php");
      include("list_queries.php");
      include("view_queries.php");
      include("edit_queries.php");
      include("../fncCashAnalyzer.inc.php");
      if (isset($_GET["order"])) $order = @$_GET["order"];
      if (isset($_GET["type"])) $ordtype = @$_GET["type"];

      if (isset($_POST["filter"])) $filter = @$_POST["filter"];
      if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
      $wholeonly = false;
      if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

      if (!isset($order) && isset($_SESSION["order"])) $order = $_SESSION["order"];
      if (!isset($ordtype) && isset($_SESSION["type"])) $ordtype = $_SESSION["type"];
      if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
      if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"];

?>

<html>
<head>
    <title>Update queries</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/csstable.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../css/jquery-ui-1.11.4.css" rel="stylesheet" type="text/css">
    <script src="../js/jquery-1.11.1.js"></script>
    <script src="../js/jquery-ui-1.11.4.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script type="text/javascript">
        $().ready(function (e) {
            $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
            if (getParameterByName("a") == "edit" || getParameterByName("a") == "add") {
                $('#update_data').modal('show');
            }
            if (getParameterByName("a") == "view") {
                $('#view_data').modal('show');
            }
        });
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    </script>
    <style>
        .filter-table {
            font-family: Calibri,Arial;
        }

        .csstable td {
            font-family: Calibri,Arial;
        }
    </style>
</head>
<body>
    <?php
    $conn=fncOpenDBConn();
    $showrecs = 2000;
    $pagerange = 10;

    $a = @$_GET["a"];
    $recid = @$_GET["recid"];
    $page = @$_GET["page"];
    if (!isset($page)) $page = 1;

    $sql = @$_POST["sql"];

    switch ($sql) {
        case "insert":
            sql_insert();
            break;
        case "update":
            sql_update();
            break;
        case "delete":
            sql_delete();
            break;
    }

    switch ($a) {
        case "add":
            addrec();
            break;
        case "view":
            viewrec($recid);
            break;
        case "edit":
            editrec($recid);
            break;
        case "del":
            deleterec($recid);
            break;
        default:
            select();
            break;
    }

    if (isset($order)) $_SESSION["order"] = $order;
    if (isset($ordtype)) $_SESSION["type"] = $ordtype;
    if (isset($filter)) $_SESSION["filter"] = $filter;
    if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;
    if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

    mssql_close($conn);
    ?>
</body>
</html>




