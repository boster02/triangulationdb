
<?php function showrow($row, $recid)
      {
?>
<table class="tbl" border="0" cellspacing="1" cellpadding="5" width="50%">
    <tr>
        <td class="hr"><?php echo htmlspecialchars("File Name")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["File_Name"]) ?></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Title")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["Title"]) ?></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Description")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["Description"]) ?></td>
    </tr>
    <tr>
        <td class="hr"><?php echo htmlspecialchars("Responsible Unit")."&nbsp;" ?></td>
        <td class="dr"><?php echo htmlspecialchars($row["lp_Responsible_Unit"]) ?></td>
    </tr>
</table>
<?php } ?>