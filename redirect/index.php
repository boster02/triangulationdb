<!DOCTYPE html>
<html>
<head>
    <title>WFP Jordan CBT Triangulation Analyzer</title>
    <meta http-equiv="refresh" content="5;url=https://cbttriangulation.jor.wfp.org" />
    <link rel="stylesheet" href="../css/bootstrap-3.0.3.min.css" />
</head>
<body>
    <div class="container">
        <h1>Kindly take note that the Triangulation Database has moved to <a href="https://cbttriangulation.jor.wfp.org">https://cbttriangulation.jor.wfp.org</a>. Kindly update your bookmarks</h1>
    </div>
</body>
</html>
