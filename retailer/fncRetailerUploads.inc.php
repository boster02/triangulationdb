<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('MAX_EXECUTION_TIME', 900);
ini_set('memory_limit', '-1');
include("../config.inc.php");
include("../mails/fncMailSender.inc.php");
function fncSendMonthlyReport($year,$month){
    $month=str_pad($month,2,"0",STR_PAD_LEFT);
    $sql="select 'All retailer branches with active contracts' as Parameter, count(*) as N,'100%' as [Percent] from merchants_wfp where status=1
union all select 'Retailer branches with sales' as Parameter, count(distinct Triangulation_ID), 
str(count(distinct Triangulation_ID)*100/(select count(*) from merchants_wfp where status=1))+'%' as [Percent] from sales_draft_$year$month INNER JOIN
    merchants_meps ON sales_draft_$year$month.terminal_id = merchants_meps.Terminal_ID INNER JOIN
    merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id
union all select 'Retailer branches uploaded sales data files' as Parameter,
	count(distinct Triangulation_ID),str(count(distinct Triangulation_ID)*100/(select count(distinct Triangulation_ID) from sales_draft_$year$month INNER JOIN
                         merchants_meps ON sales_draft_$year$month.terminal_id = merchants_meps.Terminal_ID INNER JOIN
                         merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id))+'%' as [Percent] from retailer_uploads where month=$year$month
union all select 'Retailer branches with sales data uploaded in the database' as Parameter,
	count(distinct Merchant),str(count(distinct Merchant)*100/(select count(distinct Triangulation_ID) from sales_draft_$year$month INNER JOIN
                         merchants_meps ON sales_draft_$year$month.terminal_id = merchants_meps.Terminal_ID INNER JOIN
                         merchants_wfp ON merchants_meps.Triangulation_ID = merchants_wfp.id))+'%' as [Percent]
	from merchant_sales_$year$month";

    $month_name=fncGetMonthName($year.str_pad($month,2,"0",STR_PAD_LEFT));
    $txt="<div style='font-family:Calibri'><h1>Retailer data collection update for $month_name</h1>";
    $txt.=createTable($sql);
    $success=fncSendEmailToAdmins("Retailer data collection update for $month_name",$txt);
    return $success;
}
function fncSendEmailToAdmins($subject,$message){
    $dbh=fncOpenDBConn();
    $sql ="select name,email from retailer_upload_admins where status='Y'";
    $res = mssql_query($sql,$dbh);
    $to[0][0]=0;
    $cc[0][0]=0;
    $bcc[0][0]=0;
    $to[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$to[0][0];$i++){
        $row = mssql_fetch_array($res);
        $to[$i][0]=$row["name"];
        $to[$i][1]=$row["email"];
	}
    mssql_close($dbh);
    return authMail($to, $cc, $bcc, $subject, $message);
}
function createTable($sql) {
    $dbh=fncOpenDBConn();
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $flag = false;
    $txt="";
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);
        if(!$flag) {
            $txt="<table border='1' cellpadding='3' style='border-collapse:collapse'>";
            $style='style="background-color: #003366;color: white;text-align: left"';
            $txt.="<tr><th $style>".implode("</th><th $style>", array_keys($row)) . "</th></tr>";
            $txt=str_replace("_"," ",$txt);
            $flag = true;
        }
        $txt.="\r\n<tr><td>".implode("</td><td>", array_values($row)) . "</td></tr>";
    }
    $txt.="    </table>";
    return $txt;
}
function fncGetMonthName($month){
    return strtoupper(date('M - Y',strtotime($month."01")));
}