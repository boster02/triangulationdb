<?php session_start();
      include("../fncCashAnalyzer.inc.php");
      $joco_retailer_id=$_SESSION["retailer_id"]; 
      $retailer_name=fncGetRetailerName($joco_retailer_id);
      $branches=fncGetRetailerBranches($joco_retailer_id);
      header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>Retailer upload data</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body{
            font-family:Calibri;
        }
        td {
            vertical-align: top;
        }

        .myButton {
            background-color: #599bb3;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border-radius: 8px;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: Arial;
            font-size: 16px;
            font-weight: bold;
            padding: 7px 26px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #3d768a;
        }

            .myButton:hover {
                background-color: #408c99;
            }

            .myButton:active {
                position: relative;
                top: 1px;
            }

        .nice-form {
            padding: 20px 12px 10px 20px;
            font: 13px Arial, Helvetica, sans-serif;
        }

        .nice-form-heading {
            font-weight: bold;
            font-style: italic;
            border-bottom: 2px solid #ddd;
            margin-bottom: 20px;
            font-size: 15px;
            padding-bottom: 3px;
        }

        .nice-form label {
            display: block;
            margin: 0px 0px 15px 0px;
        }

            .nice-form label > span {
                width: 100px;
                font-weight: bold;
                float: left;
                padding-top: 8px;
                padding-right: 5px;
            }

        .nice-form span.required {
            color: red;
        }

        .nice-form .tel-number-field {
            width: 40px;
            text-align: center;
        }

        .nice-form input.input-field {
        }

        .nice-form input.input-field,
        .nice-form .tel-number-field,
        .nice-form .textarea-field,
        .nice-form .select-field {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            border: 1px solid #C2C2C2;
            box-shadow: 1px 1px 4px #EBEBEB;
            -moz-box-shadow: 1px 1px 4px #EBEBEB;
            -webkit-box-shadow: 1px 1px 4px #EBEBEB;
            border-radius: 3px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            padding: 7px;
            outline: none;
            width: 250px;
        }

            .nice-form .input-field:focus,
            .nice-form .tel-number-field:focus,
            .nice-form .textarea-field:focus,
            .nice-form .select-field:focus {
                border: 1px solid #0C0;
            }

        .nice-form .textarea-field {
            height: 100px;
            width: 55%;
        }

        .nice-form input[type=submit],
        .nice-form input[type=button] {
            border: none;
            padding: 8px 15px 8px 15px;
            background: #FF8500;
            color: #fff;
            box-shadow: 1px 1px 4px #DADADA;
            -moz-box-shadow: 1px 1px 4px #DADADA;
            -webkit-box-shadow: 1px 1px 4px #DADADA;
            border-radius: 3px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            width: 250px;
        }

            .nice-form input[type=submit]:hover,
            .nice-form input[type=button]:hover {
                background: #EA7B00;
                color: #fff;
            }

        .available_reload {
            font-family: Consolas,'Courier New',monospace,'Monotype Corsiva';
        }
    </style>
</head>

<body>
    <div id="wrapper"  style="background-image:url(../images/background.jpg)">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="container-fluid" style="background-color: #1f90ff">
                <img src="../images/WFP_logo_white.gif" alt="logo" />
            </div>
            <div class="container-fluid" style="background-color: #72b5f8; margin-top: 1px; padding-top: 2px; padding-bottom: 2px">
                <span style="color:white" class="fla-breadcrumb"><a style="color:white">&nbsp;&nbsp;Home</a></span>
                <span style="color:white" class="pull-right"><a href="/index.php" style="color:white">Logout&nbsp;&nbsp;</a></span>
            </div>
        </nav>

        <div id="page-wrapper">
            <img id="loadingb" src="../images/download.gif" style="display: none;width:100px;height:100px" />
            <div class="row">
                <div class="col-sm-12">
                    <div class="nice-form">
                        <form id="form1" name="form1" enctype="multipart/form-data"
                            action="/retailer/upload_data.php" method="post" accept-charset="utf-8">
                            <div class="nice-form-heading">Retailer Upload Data for <?php echo $retailer_name ?></div>
                            <label for="month">
                                <span>Month <span class="required">*</span></span>
                                <select name="month" id="month" class="select-field">
                                    <option value="">Select Upload Month</option>
                                    <?php
                                    for ($i=1; $i<=12; $i++){
                                        $month=date("F", mktime(0, 0, 0, "$i", 1, 2000));	
                                        echo "<option value='$i'>$month</option>";
                                    }
                                    ?>
                                </select>
                            </label>
                            <label for="year">
                                <span>Year <span class="required">*</span></span>
                                <select name="year" id="year" class="select-field">
                                    <option value="">Select Year</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                </select>
                            </label>
                            <label for="type">
                                <span>Type <span class="required">*</span></span>
                                <select name="type" id="type" class="select-field">
                                    <option value="">Select Type</option>
                                    <option value="Full Month Sales">Full Month Sales</option>
                                    <option value="Half Month Sales">Half Month Sales</option>
                                    <option value="Purchases">Purchases/Itemised cost report</option>
                                    <option value="Promised prices">Promised prices for the coming 3 months</option> 
                                    <option value="First week sales report">1st week sales report_current month**</option> 
                                    <option value="Previous month cost list">Previous month Final cost list</option>
                                    <option value="P & L Report">Previous month P & L report</option>
                                    <option value="Opening Closing Stock">Previous month_opening and closing Stock</option>
                                    <option value="Picture">Picture of the shop front</option>
                                </select>
                            </label>
                            <label for="branch">
                                <span>Branch <span class="required">*</span></span>
                                <select name="branch" id="branch" class="select-field">
                                    <option value="">Select Shop/Branch</option>
                                        <?php for($i=1;$i<=$branches[0][0];$i++){ ?>
                                        <option value="<?php echo $branches[$i]["id"]?>"><?php echo $branches[$i]["id"].": ".$branches[$i]["fullname"]?></option>
                                        <?php } ?>
                                </select>
                            </label>
                            <label for="file">
                                <span><span id="file_label">Upload file</span><span class="required">*</span></span>
                                <input type="file" name="file" id="file" class="input-field" />
                            </label>
                            <input type="hidden" name="filetype" id="filetype" value="Retailer_Files" class="form-control" />
                            <label>
                                <span>&nbsp;</span>
                                <input type="submit" name="submit" value="Upload Data" id="submit" />
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../js/jquery-1.12.0.js"></script>
    <script>
        $("#submit").click(function () {
            var msg = "";
            var val = 0;
            var year = $('#year').val();
            var month = $('#month').val();
            var file = $('#file').val();
            var filetype = $('#filetype').val();
            var type = $('#type').val();
            var branch = $('#branch').val();
            if (!year.trim()) {
                msg = msg + 'Select a year!!\r\n';
                val = 1;
            }
            if (!month.trim()) {
                msg = msg + 'Select month!!\r\n';
                val = 1;
            }
            if (!type.trim()) {
                msg = msg + 'Select file type!!\r\n';
                val = 1;
            }
            if (!file.trim()) {
                msg = msg + 'Select file to upload!!\r\n';
                val = 1;
            }
            if (!branch.trim()) {
                msg = msg + 'Select the branch for the retailer';
                val = 1;
            }
            if (val == 1) {
                alert(msg);
                return false;
            }
            else {
                $('#loadingb').show();
            }
        });
    </script>
</body>
</html>