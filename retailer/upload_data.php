<?php session_start();
      include("../fncCashAnalyzer.inc.php");
      include("../mails/fncMailSender.inc.php");
      header('Content-Type: text/html; charset=utf-8');
      $retailer_name=fncGetRetailerName($_SESSION["retailer_id"]);
      $month=$_POST['month'];
      $year=$_POST['year'];
      $type=$_POST["type"];
      $retailer_id=$_POST['branch'];
      $filetype=$_POST['filetype'];
      $name   = clean($_FILES['file']['name']);
      $size   = $_FILES['file']['size'];
      $tipo   = $_FILES['file']['type'];
      $tmp_name = $_FILES['file']['tmp_name'];
      $fp = fopen($tmp_name, "rb" );
      $content = fread($fp,$size);
      fclose($fp);
      if($month<10) $month="0".$month;
      if(substr($_SESSION["retailer_id"],0,4)=='LBCO'){
          $suffix="Lebanon/".$retailer_id."_".$year.$month."_".$type."_".$name;
      }
      else{
          $suffix=$retailer_id."_".$year.$month."_".$type."_".$name;
      }
      $arch_fijo = "../uploads/Retailer_Files/".$suffix;
      $filename="https://cbttriangulation.jor.wfp.org/uploads/Retailer_Files/".$suffix;
      $fp = fopen($arch_fijo,"wb");
      fwrite($fp, $content, $size);
      fclose($fp );

      fncLogAccess2("Uploaded - ".$filetype);
      if(strlen($content)==0){
          $str="Error. The file could not be saved on the server";
          fncShowMessage($str,"/retailer");
      }
      else{
          if(substr($_SESSION["retailer_id"],0,4)=='LBCO'){
              $msg="Kindly be notified that $retailer_name has uploaded retailer data. The file name is
                <a href=\"$filename\">$filename</a>";
              fncSendEmailToUserRight("LB","acdata","Retailer Upload Notification",$msg);
              $str="The file has been successfully uploaded";
              fncShowMessage($str,"/retailer");
          }
          else{
              $result=fncUpdateRetailerUploads($suffix,$arch_fijo,$retailer_id,$type,$year.$month);
              $msg="Kindly be notified that $retailer_name has uploaded retailer data. The file name is
                <a href=\"$filename\">$filename</a> <br/><br />DB Result:$result";
              fncSendEmailToUserRight("MS","acupload","Retailer Upload Notification",$msg);
              $str="The file has been successfully uploaded";
              fncShowMessage($str,"/retailer");
          }
      }
      function clean($string) {
          $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
          return preg_replace('/[^A-Za-z0-9\-.]/', '', $string); // Removes special chars.
      }

?>