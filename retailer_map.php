<?php
include "config.inc.php";
require_once 'class.MapBuilder.php';

$map = new MapBuilder();
$map->setCenter(32, 36);
$map->setMapTypeId(MapBuilder::MAP_TYPE_ID_ROADMAP);
$map->setSize(450, 250);
$map->setZoom(9);

$dbh=fncOpenDBConn();
$result = mssql_query("SELECT name, latitude, longitude FROM retailer_gps",$dbh);
while ($row = mssql_fetch_array($result)) {
    if(floatval($row['latitude'])>0 && floatval($row['latitude'])>0){
        $map->addMarker($row['latitude'], $row['longitude'], array(
            'title' => $row['name'] 
        ));
    }
}

$map->show();
mssql_close($dbh);
?>