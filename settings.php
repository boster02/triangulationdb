<?php session_start(); 
    include("fncCashAnalyzer.inc.php");?>
<div id="my_connectivity_panel" class="pathwindow connectivity active">
    <div class="pathwindow_title">
        <div class="icon"></div>
            <a class="btn_back"><span></span><p>Back</p></a>
            <h1>Settings</h1>
        </div>
        <div class="pathwindow_content">
        <div class="arrow-list">
          <ul>
           	<li style="padding-bottom:10px"><a href="<?php echo fncPermLink("users/users.php","US","acsys") ?>">User Management</a></li>
            <li style="padding-bottom:10px"><a href="merchant_locations.php">Merchant locations</a></li>
            <li style="padding-bottom:10px"><a href="programmes.php">Programmes</a></li>
            <li style="padding-bottom:10px"><a href="commodities.php">Commodities</a></li>
            <li style="padding-bottom:10px"><a href="transaction_types.php">Transaction types</a></li>
            <li style="padding-bottom:10px"><a href="exchangerate.php">Exchange rates</a></li>
          </ul>
          </div>
        <div class="clear"></div>
    </div>
</div>
