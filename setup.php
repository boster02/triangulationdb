<?php session_start();
      include("fncCashAnalyzer.inc.php");
      $row=fncGetProgrammeSetup();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Boster Sibande">

    <title>Programme Settings</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../css/jquery-ui-1.11.4.css" rel="stylesheet" type="text/css">
    <script src="../js/jquery-1.11.1.js"></script>
    <script src="../js/jquery-ui-1.11.4.js"></script>
    <script src="../js/bootstrap.js"></script>
    <style>
        .hr {
            width: 150px;
        }
    </style>
</head>

<body>
    <h3>Update Programme Settings</h3>
    <form name="form1" id="form1" method="post" action="save_programme_setup.php">
    <table class="table table-striped table-bordered table-condensed" border="0" cellspacing="1" cellpadding="5" style="width: 800px">
        <tr>
            <td class="hr"><?php echo htmlspecialchars("Region")."&nbsp;" ?></td>
            <td class="dr">
                <select class="form-control" id="Region" name="Region">
                    <option value="-1">[Select]</option>
                    <option value="OMB">OMB</option>
                    <option value="OMC" selected="selected">OMC</option>
                    <option value="OMD">OMD</option>
                    <option value="OMJ">OMJ</option>
                    <option value="OMN">OMN</option>
                    <option value="OMP">OMP</option>
                </select>
        </tr>

        <tr>
            <td class="hr"><?php echo htmlspecialchars("Country Office")."&nbsp;" ?></td>
            <td class="dr">
                <select class="form-control" id="Country" name="Country_Office">
                    <option value="-1">[Select]</option>
                    <option value="1">Afghanistan</option>
                    <option value="2">Bangladesh</option>
                    <option value="3">Bhutan</option>
                    <option value="4">Cambodia</option>
                    <option value="5">India</option>
                    <option value="6">Indonesia</option>
                    <option value="7">Laos</option>
                    <option value="8">Myanmar</option>
                    <option value="9">Nepal</option>
                    <option value="10">Pakistan</option>
                    <option value="11">Philippines</option>
                    <option value="12">Sri Lanka</option>
                    <option value="13">Timor Leste</option>
                    <option value="14">Algeria</option>
                    <option value="15">Armenia</option>
                    <option value="16">Egypt</option>
                    <option value="17">Georgia</option>
                    <option value="18">Iran</option>
                    <option value="19">Iraq</option>
                    <option value="20" selected="selected">Jordan</option>
                    <option value="21">Kyrgyzstan</option>
                    <option value="22">oPt</option>
                    <option value="23">Syria</option>
                    <option value="24">Sudan</option>
                    <option value="25">Tajikistan</option>
                    <option value="26">United Arab Emirates</option>
                    <option value="27">Benin</option>
                    <option value="28">Burkina Faso</option>
                    <option value="29">Cameroon</option>
                    <option value="30">Chad</option>
                    <option value="31">Central African republic</option>
                    <option value="32">C�te d'Ivoire</option>
                    <option value="33">Gambia</option>
                    <option value="34">Ghana</option>
                    <option value="35">Guinea Bissau</option>
                    <option value="36">Guinea</option>
                    <option value="37">Liberia</option>
                    <option value="38">Mali</option>
                    <option value="39">Mauritania</option>
                    <option value="40">Niger</option>
                    <option value="41">S�o Tom�</option>
                    <option value="42">Senegal</option>
                    <option value="43">Sierra Leone</option>
                    <option value="44">Togo</option>
                    <option value="45">Angola</option>
                    <option value="46">Congo Brazaville</option>
                    <option value="47">Lesotho</option>
                    <option value="48">Mozambique</option>
                    <option value="49">Madagascar</option>
                    <option value="50">Malawi</option>
                    <option value="51">Namibia</option>
                    <option value="52">Swaziland</option>
                    <option value="53">Tanzania</option>
                    <option value="54">Zambia</option>
                    <option value="55">Zimbabwe</option>
                    <option value="56">Burundi</option>
                    <option value="57">Djibouti</option>
                    <option value="58">Ethiopia</option>
                    <option value="59">Eritrea</option>
                    <option value="60">Kenya</option>
                    <option value="61">Somalia</option>
                    <option value="62">Rwanda</option>
                    <option value="63">South Sudan</option>
                    <option value="64">Uganda</option>
                    <option value="65">Bolivia</option>
                    <option value="66">Colombia</option>
                    <option value="67">Cuba</option>
                    <option value="68">Dominican Republic</option>
                    <option value="69">Ecuador</option>
                    <option value="70">El Savador</option>
                    <option value="71">Guatemala</option>
                    <option value="72">Haiti</option>
                    <option value="73">Honduras</option>
                    <option value="74">Nicaragua</option>
                    <option value="75">Peru</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="hr"><?php echo htmlspecialchars("Programme Name")."&nbsp;" ?></td>
            <td class="dr">
                <input class="form-control" type="text" name="Programme_Name" value="<?php echo $row["Programme_Name"] ?>" /></td>
        </tr>
        <tr>
            <td class="hr"><?php echo htmlspecialchars("About the Programme")."&nbsp;" ?></td>
            <td class="dr">
                <textarea class="form-control" rows="4" name="About_the_programme"><?php echo $row["About_the_programme"] ?>"></textarea></td>
        </tr>
        <tr>
            <td class="hr"><?php echo htmlspecialchars("About the Triangulation Database")."&nbsp;" ?></td>
            <td class="dr">
                <textarea class="form-control" rows="4" name="About_the_database"><?php echo $row["About_the_database"] ?>"></textarea></td>
        </tr>
        <tr>
            <td class="hr"><?php echo htmlspecialchars("Programme Start Date")."&nbsp;" ?></td>
            <td class="dr">
                <input class="form-control datepicker" type="text" name="Start_Date" value="<?php echo $row["Start_Date"] ?>" /></td>
        </tr>
        <tr>
            <td class="hr"><?php echo htmlspecialchars("Programme End Date")."&nbsp;" ?></td>
            <td class="dr">
                <input class="form-control datepicker" type="text"  name="End_Date" value="<?php echo $row["End_Date"] ?>" /></td>
        </tr>
    </table>
    <input type="hidden" name="Updated_By" value="<?php echo $_SESSION["name"] ?>"/>
    <input type="hidden" value="<?php echo date('Y-m-d') ?>" name="Date_Updated" />
    <input class="btn btn-primary" type="submit" value="Save Data" />
    <br />
        <br />
        <br />
        <br />
        </form>
</body>

</html>

