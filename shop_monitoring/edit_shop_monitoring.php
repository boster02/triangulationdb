<?php function showroweditor($row, $iseditmode)
      {
          global $conn;
?>
<div class="modal fade" id="update_data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:880px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">
        Enter Shop Monitoring Information</h3>
      </div>
      <div class="modal-body">
      <div class="table-responsive">
        <table class="table"><tr>
            <td style="vertical-align:top">
                <table class="table table-striped table-bordered" border="0" cellspacing="1" cellpadding="5" style="width:400px">
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Source")."&nbsp;" ?></td>
                        <td class="dr">
                            <select class="form-control" name="Source">
                                <option value=""></option>
                                <?php
                                    $sql = "select ID, Source from information_sources";
                                    $res = mssql_query($sql, $conn);
                                    while ($lp_row = mssql_fetch_assoc($res)){
                                        $val = $lp_row["ID"];
                                        $caption = $lp_row["Source"];
                                        if ($row["Source"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                                ?>
                                <option value="<?php echo $val ?>" <?php echo $selstr ?>><?php echo $caption ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Retailer ID")."&nbsp;" ?></td>
                        <td class="dr">
                            <select id="Retailer_ID" class="form-control" name="Retailer_ID" onchange="getBranches(this.value)">
                                <option value=""></option>
                                <?php
                                    $sql = "select distinct retailer_id,group_name from merchants_wfp where retailer_id is not null order by retailer_id";
                                    $res = mssql_query($sql, $conn);
                                    while ($lp_row = mssql_fetch_assoc($res)){
                                        $val = $lp_row["retailer_id"];
                                        $caption =$lp_row["retailer_id"]." ". $lp_row["group_name"];
                                        if ($row["Retailer_ID"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                                ?>
                                <option value="<?php echo $val ?>" <?php echo $selstr ?>><?php echo $caption ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Branch ID")."&nbsp;" ?></td>
                        <td class="dr">
                            <select id="Branch_ID" class="form-control" name="Branch_ID">
                                <option value=""></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Main Performance Area")."&nbsp;" ?></td>
                        <td class="dr">
                            <select class="form-control" name="Main_Performance_Area">
                                <option value=""></option>
                                <?php
                                $sql = "select ID, performance_area from performance_areas";
                                $res = mssql_query($sql, $conn);
                                while ($lp_row = mssql_fetch_assoc($res)){
                                    $val = $lp_row["ID"];
                                    $caption = $lp_row["performance_area"];
                                    if ($row["Main_Performance_Area"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                                ?>
                                <option value="<?php echo $val ?>" <?php echo $selstr ?>><?php echo $caption ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Issue Identified")."&nbsp;" ?></td>
                        <td class="dr"><textarea class="form-control" cols="35" rows="4" name="Issue_Identified" maxlength="500"><?php echo str_replace('"', '&quot;', trim($row["Issue_Identified"])) ?></textarea></td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Date")."&nbsp;" ?></td>
                        <td class="dr"><input class="form-control datepicker" type="text" name="Date" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["Date"])) ?>"></td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Function Assigned")."&nbsp;" ?></td>
                        <td class="dr">
                            <select class="form-control" name="Function_Assigned">
                                <option value=""></option>
                                <?php
                                $sql = "select ID, Unit from units";
                                $res = mssql_query($sql, $conn);
                                while ($lp_row = mssql_fetch_assoc($res)){
                                    $val = $lp_row["ID"];
                                    $caption = $lp_row["Unit"];
                                    if ($row["Function_Assigned"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                                ?>
                                <option value="<?php echo $val ?>" <?php echo $selstr ?>><?php echo $caption ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align:top">
                <table class="table table-striped table-bordered" border="0" cellspacing="1" cellpadding="5" style="width:400px">
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Assigned Staff")."&nbsp;" ?></td>
                        <td class="dr">
                            <select class="form-control" name="Assigned_Staff">
                                <option value=""></option>
                                <?php
                                    $sql = "select ID, Name from staff";
                                    $res = mssql_query($sql, $conn);
                                    while ($lp_row = mssql_fetch_assoc($res)){
                                        $val = $lp_row["ID"];
                                        $caption = $lp_row["Name"];
                                        if ($row["Assigned_Staff"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                                ?>
                                <option value="<?php echo $val ?>" <?php echo $selstr ?>><?php echo $caption ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Action Taken")."&nbsp;" ?></td>
                        <td class="dr"><textarea class="form-control" cols="35" rows="4" name="Action_Taken" maxlength="2000"><?php echo str_replace('"', '&quot;', trim($row["Action_Taken"])) ?></textarea></td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Comments")."&nbsp;" ?></td>
                        <td class="dr"><textarea class="form-control" cols="35" rows="4" name="Comments" maxlength="2000"><?php echo str_replace('"', '&quot;', trim($row["Comments"])) ?></textarea></td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Outstanding Issue")."&nbsp;" ?></td>
                        <td class="dr"><textarea class="form-control" cols="35" rows="4" name="Outstanding_Issue" maxlength="2000"><?php echo str_replace('"', '&quot;', trim($row["Outstanding_Issue"])) ?></textarea></td>
                    </tr>
                    <tr>
                        <td class="hr"><?php echo htmlspecialchars("Status")."&nbsp;" ?></td>
                        <td class="dr">
                            <select class="form-control" name="Status">
                                <option value=""></option>
                                <?php
                                $sql = "select id, status from shop_monitoring_status";
                                $res = mssql_query($sql, $conn);
                                while ($lp_row = mssql_fetch_assoc($res)){
                                    $val = $lp_row["id"];
                                    $caption = $lp_row["status"];
                                    if ($row["Status"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
                                ?>
                                <option value="<?php echo $val ?>" <?php echo $selstr ?>><?php echo $caption ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
          </tr>
          </table>
          <input type="hidden" name="Updated_By" value="<?php echo $_SESSION["name"] ?>"/>
          <input type="hidden" value="<?php echo date('Y-m-d') ?>" name="Date_Updated" />
          <input type="hidden" id="hBranch_ID" name="hBranch_ID" value="<?php echo $row["Branch_ID"]?>" />
          <input class="btn btn-primary" type="submit" name="action" value="Save Record">
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    if ($("#Retailer_ID").val() != null && $("#Retailer_ID").val() != "") {
        getBranches($("#Retailer_ID").val());
    }
    function getBranches(id) {
        $.ajax({
            url: "ajax_data.php?type=branches&retailer_id=" + id,
            dataType: 'JSON',
            success: function (response) {
                clearDropdown("Branch_ID");
                AddItem("Branch_ID", "", "")
                var options = response;
                for (var i = 0; i < options.length; i++) {
                    AddItem("Branch_ID", options[i].id, options[i].branch);
                }
                document.getElementById("Branch_ID").value = $("#hBranch_ID").val();
            },
            error: function (xhr, status, error) {
                alert("Couldn't retrieve branches for the selected retailer");
            }
        });
    }
    function AddItem(ddlName, optValue, optText) {
        var opt = document.createElement("option");
        opt.text = optText;
        opt.value = optValue;
        document.getElementById(ddlName).options.add(opt);
    }

    function clearDropdown(ddlName) {
        var selectbox = document.getElementById(ddlName);
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }
</script>
<?php } ?>