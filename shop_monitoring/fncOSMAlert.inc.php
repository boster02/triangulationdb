<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('MAX_EXECUTION_TIME', 900);
ini_set('memory_limit', '-1');
include("../config.inc.php");
include("../mails/fncMailSender.inc.php");
function fncSendMonthlyReport($year,$month,$office){
    if($office==3){
        $sql1="select t2.Office as Data_Gatherer_Office,count(distinct retailer_id) as Shops,sum(beneficiaries) as Beneficiaries
                from shop_monitoring_new t1
                inner join Price_Monitoring_Offices t2 on t1.data_gatherer_office=t2.id
                where Year(data_collection_date)=$year and month(data_collection_date)=$month and t1.data_gatherer_office=3
                Group by t2.Office, t2.id
                order by t2.id";
        $sql2="select t2.Office as Data_Gatherer_Office,Data_Gatherer, count(distinct retailer_id) as Shops,sum(beneficiaries) as Beneficiaries
                from shop_monitoring_new t1
                inner join Price_Monitoring_Offices t2 on t1.data_gatherer_office=t2.id
                where Year(data_collection_date)=$year and month(data_collection_date)=$month and t1.data_gatherer_office=3
                Group by t2.Office, t2.id,data_gatherer
                order by t2.id";
    }
    elseif($office==4){
        $sql1="select t2.Office as Data_Gatherer_Office,count(distinct retailer_id) as Shops,sum(beneficiaries) as Beneficiaries
                from shop_monitoring_new t1
                inner join Price_Monitoring_Offices t2 on t1.data_gatherer_office=t2.id
                where Year(data_collection_date)=$year and month(data_collection_date)=$month and t1.data_gatherer_office=4
                Group by t2.Office, t2.id
                order by t2.id";
        $sql2="select t2.Office as Data_Gatherer_Office,Data_Gatherer, count(distinct retailer_id) as Shops,sum(beneficiaries) as Beneficiaries
                from shop_monitoring_new t1
                inner join Price_Monitoring_Offices t2 on t1.data_gatherer_office=t2.id
                where Year(data_collection_date)=$year and month(data_collection_date)=$month and t1.data_gatherer_office=4
                Group by t2.Office, t2.id,data_gatherer
                order by t2.id";
    }
    else{
        $sql1="select t2.Office as Data_Gatherer_Office, count(distinct retailer_id) as Shops,sum(beneficiaries) as Beneficiaries
                from shop_monitoring_new t1
                inner join Price_Monitoring_Offices t2 on t1.data_gatherer_office=t2.id
                where Year(data_collection_date)=$year and month(data_collection_date)=$month
                Group by t2.Office, t2.id
                order by t2.id";
        $sql2="select t2.Office as Data_Gatherer_Office,Data_Gatherer, count(distinct retailer_id) as Shops,sum(beneficiaries) as Beneficiaries
                from shop_monitoring_new t1
                inner join Price_Monitoring_Offices t2 on t1.data_gatherer_office=t2.id
                where Year(data_collection_date)=$year and month(data_collection_date)=$month
                Group by t2.Office, t2.id,data_gatherer
                order by t2.id";
    }
    $month_name=fncGetMonthName($year.str_pad($month,2,"0",STR_PAD_LEFT));
    $txt="<div style='font-family:Calibri'><h1>On-site monitoring data collection update for $month_name</h1>";
    $txt.="<strong>Summary by Office</strong> <br />";
    $txt.=createTable($sql1);
    $txt.="<br /><br /><strong>Summary by Office and Data Gatherer</strong> <br />";
    $txt.=createTable($sql2);
    $txt.="</div>";
    $success=fncSendEmailToAdmins("On-site monitoring data collection update for $month_name",$txt,$office);
    return $success;
}
function fncSendEmailToAdmins($subject,$message,$office){
    $dbh=fncOpenDBConn();
    $sql ="select name,email from shop_monitoring_admins where status='Y' and office=".$office;
    $res = mssql_query($sql,$dbh);
    $to[0][0]=0;
    $cc[0][0]=0;
    $bcc[0][0]=0;
    $to[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$to[0][0];$i++){
        $row = mssql_fetch_array($res);
        $to[$i][0]=$row["name"];
        $to[$i][1]=$row["email"];
	}
    mssql_close($dbh);
    return authMail($to, $cc, $bcc, $subject, $message);
}
function createTable($sql) {
    $dbh=fncOpenDBConn();
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    $flag = false;
    $txt="";
    for($i=1; $i<=$data[0][0]; $i++)
    {
        $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);
        if(!$flag) {
            $txt="<table border='1' cellpadding='3' style='border-collapse:collapse'>";
            $style='style="background-color: #003366;color: white;text-align: left"';
            $txt.="<tr><th $style>".implode("</th><th $style>", array_keys($row)) . "</th></tr>";
            $txt=str_replace("_"," ",$txt);
            $flag = true;
        }
        $txt.="\r\n<tr><td>".implode("</td><td>", array_values($row)) . "</td></tr>";
    }
    $txt.="    </table>";
    return $txt;
}
function fncGetMonthName($month){
    return strtoupper(date('M - Y',strtotime($month."01")));
}