<?php
include("../fncDataFunctions.inc.php");
?>
<?php function showpagenav($page, $pagecount)
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
    <tr>
        <td>
            <a href="shop_monitoring.php?a=add">Add Record</a>&nbsp;
        </td>
        <?php if ($page > 1) { ?>
        <td>
            <a href="shop_monitoring.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Prev</a>&nbsp;
        </td>
        <?php } ?>
        <?php
          global $pagerange;

          if ($pagecount > 1) {

              if ($pagecount % $pagerange != 0) {
                  $rangecount = intval($pagecount / $pagerange) + 1;
              }
              else {
                  $rangecount = intval($pagecount / $pagerange);
              }
              for ($i = 1; $i < $rangecount + 1; $i++) {
                  $startpage = (($i - 1) * $pagerange) + 1;
                  $count = min($i * $pagerange, $pagecount);

                  if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
                      for ($j = $startpage; $j < $count + 1; $j++) {
                          if ($j == $page) {
        ?>
        <td>
            <b>
                <?php echo $j ?>
            </b>
        </td>
        <?php } else { ?>
        <td>
            <a href="shop_monitoring.php?page=<?php echo $j ?>">
                <?php echo $j ?>
            </a>
        </td>
        <?php }
                      }
                  } else { ?>
        <td>
            <a href="shop_monitoring.php?page=<?php echo $startpage ?>">
                <?php echo $startpage ."..." .$count ?>
            </a>
        </td>
        <?php }
              }
          } ?>
        <?php if ($page < $pagecount) { ?>
        <td>
            &nbsp;
            <a href="shop_monitoring.php?page=<?php echo $page + 1 ?>">Next&nbsp;&gt;&gt;</a>&nbsp;
        </td>
        <?php } ?>
    </tr>
</table>
<?php } ?>


<?php function showrecnav($a, $recid, $count)
      {
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
    <tr>
        <td>
            <a href="shop_monitoring.php">List of issues</a>
        </td>
    </tr>
</table>
<?php } ?>

<?php function addrec()
      {
?>
<form enctype="multipart/form-data" action="shop_monitoring.php" method="post">
    <p>
        <input type="hidden" name="sql" value="insert" />
    </p>
    <?php
$row = array(
  "ID" => "",
  "Source" => "",
  "Retailer_ID" => "",
  "Branch_ID" => "",
  "Main_Performance_Area" => "",
  "Issue_Identified" => "",
  "Date" => "",
  "Function_Assigned" => "",
  "Assigned_Staff" => "",
  "Action_Taken" => "",
  "Comments" => "",
  "Outstanding_Issue" => "",
  "Status" => "");
showroweditor($row, false);
    ?>
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("view", $recid, $count);
?>
<?php showrow($row, $recid) ?>

<?php

} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("edit", $recid, $count);
?>
<form enctype="multipart/form-data" action="shop_monitoring.php" method="post">
    <input type="hidden" name="sql" value="update" />
    <input type="hidden" name="xid" value="<?php echo $row["ID"] ?>" />
    <?php showroweditor($row, true); ?>
    <br />
</form>
<?php

} ?>

<?php function deleterec($recid)
      {
          $res = sql_select();
          $count = sql_getrecordcount();
          $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
          showrecnav("del", $recid, $count);
?>
<form action="shop_monitoring.php" method="post">
    <input type="hidden" name="sql" value="delete" />
    <input type="hidden" name="xid" value="<?php echo $row["ID"] ?>" />
    <?php showrow($row, $recid) ?>
    <p>
        <input class="btn btn-danger" type="submit" name="action" value="Confirm" />
    </p>
    <br />
</form>
<?php
      }

function sql_select()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT * FROM (SELECT t1.ID, t1.Source, lp1.Source AS lp_Source, t1.Retailer_ID,  concat(t1.Branch_ID,' - ',lp3.group_name) AS lp_Retailer_ID,
        t1.Branch_ID, lp3.branch AS lp_Branch_ID, t1.Main_Performance_Area,
        t1.Issue_Identified, t1.Date, t1.Function_Assigned, lp7.Unit AS lp_Function_Assigned, t1.Assigned_Staff,
        lp8.Name AS lp_Assigned_Staff, t1.Action_Taken, t1.Comments, t1.Outstanding_Issue, t1.Status, lp12.status AS lp_Status
        FROM shop_monitoring AS t1
        LEFT OUTER JOIN information_sources AS lp1 ON (t1.Source = lp1.ID)
        LEFT OUTER JOIN merchants_wfp AS lp3 ON (t1.Branch_ID = lp3.id)
        LEFT OUTER JOIN units AS lp7 ON (t1.Function_Assigned = lp7.ID)
        LEFT OUTER JOIN staff AS lp8 ON (t1.Assigned_Staff = lp8.ID)
        LEFT OUTER JOIN shop_monitoring_status AS lp12 ON (t1.Status = lp12.id)) subq";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (lp_Source like '" .$filterstr ."') or (lp_Retailer_ID like '" .$filterstr ."') or (lp_Branch_ID like '" .$filterstr ."') or (lp_Main_Performance_Area like '" .$filterstr ."') or (Issue_Identified like '" .$filterstr ."') or (Date like '" .$filterstr ."') or (lp_Function_Assigned like '" .$filterstr ."') or (lp_Assigned_Staff like '" .$filterstr ."') or (Action_Taken like '" .$filterstr ."') or (Comments like '" .$filterstr ."') or (Outstanding_Issue like '" .$filterstr ."') or (lp_Status like '" .$filterstr ."')";
  }
  if (isset($order) && $order!='') $sql .= " order by " .sqlstr($order) ."";
  if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
  $res = mssql_query($sql, $conn);
  return $res;
}

function sql_getrecordcount()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT count(*) FROM (SELECT t1.ID, t1.Source, lp1.Source AS lp_Source, t1.Retailer_ID,  concat(t1.Retailer_ID,' - ',lp3.group_name) AS lp_Retailer_ID,
        t1.Branch_ID, lp3.branch AS lp_Branch_ID, t1.Main_Performance_Area,
        t1.Issue_Identified, t1.Date, t1.Function_Assigned, lp7.Unit AS lp_Function_Assigned, t1.Assigned_Staff,
        lp8.Name AS lp_Assigned_Staff, t1.Action_Taken, t1.Comments, t1.Outstanding_Issue, t1.Status, lp12.status AS lp_Status
        FROM shop_monitoring AS t1
        LEFT OUTER JOIN information_sources AS lp1 ON (t1.Source = lp1.ID)
        LEFT OUTER JOIN merchants_wfp AS lp3 ON (t1.Branch_ID = lp3.id)
        LEFT OUTER JOIN units AS lp7 ON (t1.Function_Assigned = lp7.ID)
        LEFT OUTER JOIN staff AS lp8 ON (t1.Assigned_Staff = lp8.ID)
        LEFT OUTER JOIN shop_monitoring_status AS lp12 ON (t1.Status = lp12.id)) subq";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (lp_Source like '" .$filterstr ."') or (lp_Retailer_ID like '" .$filterstr ."') or (lp_Branch_ID like '" .$filterstr ."') or (lp_Main_Performance_Area like '" .$filterstr ."') or (Issue_Identified like '" .$filterstr ."') or (Date like '" .$filterstr ."') or (lp_Function_Assigned like '" .$filterstr ."') or (lp_Assigned_Staff like '" .$filterstr ."') or (Action_Taken like '" .$filterstr ."') or (Comments like '" .$filterstr ."') or (Outstanding_Issue like '" .$filterstr ."') or (lp_Status like '" .$filterstr ."')";
  }
  $res = mssql_query($sql, $conn);
  $row = mssql_fetch_assoc($res);
  reset($row);
  return current($row);
}

function sql_insert()
{
  global $conn;
  global $_POST;

  $sql = "insert into shop_monitoring (Source, Retailer_ID, Branch_ID, Main_Performance_Area, Issue_Identified, Date,
        Function_Assigned, Assigned_Staff, Action_Taken, Comments, Outstanding_Issue, Status)
        values (" .sqlvalue(@$_POST["Source"], false).", "
        .sqlvalue(@$_POST["Retailer_ID"], true).", " .sqlvalue(@$_POST["Branch_ID"], false).", "
        .sqlvalue(@$_POST["Main_Performance_Area"], false).", " .sqlvalue(@$_POST["Issue_Identified"], true).", "
        .sqlvalue(@$_POST["Date"], true).", " .sqlvalue(@$_POST["Function_Assigned"], false).", "
        .sqlvalue(@$_POST["Assigned_Staff"], false).", " .sqlvalue(@$_POST["Action_Taken"], true).", "
        .sqlvalue(@$_POST["Comments"], true).", " .sqlvalue(@$_POST["Outstanding_Issue"], true).", "
        .sqlvalue(@$_POST["Status"], false).")";
  $res=mssql_query($sql, $conn);
  if($res){
      fncShowMessage("The record has been successfully saved","shop_monitoring.php");
  }
  else{
      fncShowMessage("Error: The record could not be saved","shop_monitoring.php");
  }
}

function sql_update()
{
  global $conn;
  global $_POST;

  $sql = "update shop_monitoring set Source=" .sqlvalue(@$_POST["Source"], false)
        .", Retailer_ID=" .sqlvalue(@$_POST["Retailer_ID"], true).", Branch_ID=" .sqlvalue(@$_POST["Branch_ID"], false)
        .", Main_Performance_Area=" .sqlvalue(@$_POST["Main_Performance_Area"], false)
        .", Issue_Identified=" .sqlvalue(@$_POST["Issue_Identified"], true).", Date=" .sqlvalue(@$_POST["Date"], true)
        .", Function_Assigned=" .sqlvalue(@$_POST["Function_Assigned"], false).
        ", Assigned_Staff=" .sqlvalue(@$_POST["Assigned_Staff"], false).", Action_Taken=" .sqlvalue(@$_POST["Action_Taken"], true)
        .", Comments=" .sqlvalue(@$_POST["Comments"], true).", Outstanding_Issue=" .sqlvalue(@$_POST["Outstanding_Issue"], true)
        .", Status=" .sqlvalue(@$_POST["Status"], false) ." where " .primarykeycondition();
  $res=mssql_query($sql, $conn);
  if($res){
      fncShowMessage("The record has been successfully updated","shop_monitoring.php");
  }
  else{
      fncShowMessage("Error: The record could not be updated","shop_monitoring.php");
  }
}

function sql_delete()
{
  global $conn;

  $sql = "delete from shop_monitoring where " .primarykeycondition();
  $res=mssql_query($sql, $conn);
  if($res){
      fncShowMessage("The record has been successfully deleted","shop_monitoring.php");
  }
  else{
      fncShowMessage("Error: The record could not be deleted","shop_monitoring.php");
  }
}
function fncGetPerformanceAreaLabels(){
    global $conn;
    $sql="Select id,performance_area from performance_areas";
    $res=mssql_query($sql, $conn);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$row["id"]]=$row["performance_area"];
	}
    return $data;
}
function primarykeycondition()
{
  global $_POST;
  $pk = "";
  $pk .= "(ID";
  if (@$_POST["xID"] == "") {
    $pk .= " IS NULL";
  }else{
  $pk .= " = " .sqlvalue(@$_POST["xID"], false);
  };
  $pk .= ")";
  return $pk;
}
?>
