<?php function select()
      {
          global $a;
          global $showrecs;
          global $page;
          global $filter;
          global $filterfield;
          global $wholeonly;
          global $order;
          global $ordtype;


          if ($a == "reset") {
              $filter = "";
              $filterfield = "";
              $wholeonly = "";
              $order = "";
              $ordtype = "";
          }

          $checkstr = "";
          if ($wholeonly) $checkstr = " checked";
          if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
          $res = sql_select();
          $count = sql_getrecordcount();
          if ($count % $showrecs != 0) {
              $pagecount = intval($count / $showrecs) + 1;
          }
          else {
              $pagecount = intval($count / $showrecs);
          }
          $startrec = $showrecs * ($page - 1);
          if ($startrec < $count) {sqlsrv_fetch($res, $startrec);}
          $reccount = min($showrecs * $page, $count);
          $performance_area_labels=fncGetPerformanceAreaLabels();
?>
<style type="text/css">
    td a {
        color: white;
        text-align: left;
    }
</style>
<div style="width:90%;margin:auto auto auto auto">
    <h1 style="text-align:center">WFP Jordan Shop Monitoring Database</h1>
    <hr />
    <div class="row">
        <div class="col-sm-10 col-md-10">
            <form action="shop_monitoring.php" method="post">
                <table class="bd" border="0" cellspacing="1" cellpadding="4">
                    <tr>
                        <td><b>Search</b>&nbsp;</td>
                        <td><input class="form-control" type="text" name="filter" value="<?php echo $filter ?>"></td>
                        <td>
                            <select class="form-control" name="filter_field">
                                <option value="">All Fields</option>
                                <option value="<?php echo "lp_Source" ?>" <?php if ($filterfield == "lp_Source") { echo "selected"; } ?>><?php echo htmlspecialchars("Source") ?></option>
                                <option value="<?php echo "lp_Retailer_ID" ?>" <?php if ($filterfield == "lp_Retailer_ID") { echo "selected"; } ?>><?php echo htmlspecialchars("Retailer ID") ?></option>
                                <option value="<?php echo "lp_Branch_ID" ?>" <?php if ($filterfield == "lp_Branch_ID") { echo "selected"; } ?>><?php echo htmlspecialchars("Branch ID") ?></option>
                                <option value="<?php echo "Issue_Identified" ?>" <?php if ($filterfield == "Issue_Identified") { echo "selected"; } ?>><?php echo htmlspecialchars("Issue Identified") ?></option>
                                <option value="<?php echo "Date" ?>" <?php if ($filterfield == "Date") { echo "selected"; } ?>><?php echo htmlspecialchars("Date") ?></option>
                                <option value="<?php echo "lp_Function_Assigned" ?>" <?php if ($filterfield == "lp_Function_Assigned") { echo "selected"; } ?>><?php echo htmlspecialchars("Function Assigned") ?></option>
                                <option value="<?php echo "lp_Assigned_Staff" ?>" <?php if ($filterfield == "lp_Assigned_Staff") { echo "selected"; } ?>><?php echo htmlspecialchars("Assigned Staff") ?></option>
                                <option value="<?php echo "Action_Taken" ?>" <?php if ($filterfield == "Action_Taken") { echo "selected"; } ?>><?php echo htmlspecialchars("Action Taken") ?></option>
                                <option value="<?php echo "Comments" ?>" <?php if ($filterfield == "Comments") { echo "selected"; } ?>><?php echo htmlspecialchars("Comments") ?></option>
                                <option value="<?php echo "Outstanding_Issue" ?>" <?php if ($filterfield == "Outstanding_Issue") { echo "selected"; } ?>><?php echo htmlspecialchars("Outstanding Issue") ?></option>
                                <option value="<?php echo "lp_Status" ?>" <?php if ($filterfield == "lp_Status") { echo "selected"; } ?>><?php echo htmlspecialchars("Status") ?></option>
                            </select>
                        </td>
                        <td> &nbsp;&nbsp;<input type="checkbox" name="wholeonly" <?php echo $checkstr ?>>Whole words only</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" name="action" value="Search"></td>
                        <td><a href="shop_monitoring.php?a=reset">Reset Filter</a></td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-sm-2 col-md-2" style="text-align: right">
            <a href="shop_monitoring.php?a=add" class="btn btn-primary">Add New Issue</a>
        </div>
    </div>
    <hr size="1" noshade />
    <div class="csstable">
        <h3>
            <a style="margin-top:10px" class="green-btn" href="javascript:exportDetails()">Export the table to Excel</a>
        </h3>
        <table class="tbl" border="0" cellspacing="1" cellpadding="5" width="100%">
<tr>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "lp_Source" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Source") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "lp_Retailer_ID" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Retailer ID") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "lp_Branch_ID" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Branch ID") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "lp_Main_Performance_Area" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Main Performance Area") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "Issue_Identified" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Issue Identified") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "Date" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Date") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "lp_Function_Assigned" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Function Assigned") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "lp_Assigned_Staff" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Assigned Staff") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "Action_Taken" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Action Taken") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "Comments" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Comments") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "Outstanding_Issue" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Outstanding Issue") ?></a></td>
<td class="hr"><a class="hr" href="shop_monitoring.php?order=<?php echo "lp_Status" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Status") ?></a></td>
    <td class="hr">&nbsp;</td>
    <td class="hr">&nbsp;</td>
    <td class="hr">&nbsp;</td>
</tr>
<?php
          for ($i = $startrec; $i < $reccount; $i++)
          {
              $row = mssql_fetch_assoc($res);
              $performance_area_str="<ul>";
              if(isset($row["Main_Performance_Area"])){
                  $performance_areas=split(",",$row["Main_Performance_Area"]);
                  for($j=0;$j<count($performance_areas);$j++){
                      $performance_area_str.="<li>".$performance_area_labels[intval($performance_areas[$j])]."</li>";
                  }
                  $performance_area_str.="</ul>";
              }
              $style = "dr";
?>
<tr>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_Source"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_Retailer_ID"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_Branch_ID"]) ?></td>
<td class="<?php echo $style ?>"><?php echo $performance_area_str ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Issue_Identified"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Date"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_Function_Assigned"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_Assigned_Staff"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Action_Taken"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Comments"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Outstanding_Issue"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_Status"]) ?></td>
    <td style="width: 18px">
        <a href="shop_monitoring.php?a=view&recid=<?php echo $i ?>">
            <img src="../images/view.png" alt="view" />
        </a>
    </td>
    <td style="width: 18px">
        <a href="shop_monitoring.php?a=edit&recid=<?php echo $i ?>">
            <img src="../images/edit-bw.png" alt="edit" />
        </a>
    </td>
    <td style="width: 18px">
        <a href="shop_monitoring.php?a=del&recid=<?php echo $i ?>">
            <img src="../images/delete.png" alt="delete" />
        </a>
    </td>
</tr>
<?php
          }
          
?>
</table>
<br>
        <br />
    </div>
</div>
<script>
    function exportDetails() {
        $('#loadingmessage').show();
        var sql = "SELECT * FROM (SELECT t1.ID, t1.Source, lp1.Source AS lp_Source, t1.Retailer_ID,  concat(t1.Branch_ID,' - ',lp3.group_name) AS lp_Retailer_ID,\
        t1.Branch_ID, lp3.branch AS lp_Branch_ID, t1.Main_Performance_Area,\
        t1.Issue_Identified, t1.Date, t1.Function_Assigned, lp7.Unit AS lp_Function_Assigned, t1.Assigned_Staff,\
        lp8.Name AS lp_Assigned_Staff, t1.Action_Taken, t1.Comments, t1.Outstanding_Issue, t1.Status, lp12.status AS lp_Status\
        FROM shop_monitoring AS t1\
        LEFT OUTER JOIN information_sources AS lp1 ON (t1.Source = lp1.ID)\
        LEFT OUTER JOIN merchants_wfp AS lp3 ON (t1.Branch_ID = lp3.id)\
        LEFT OUTER JOIN units AS lp7 ON (t1.Function_Assigned = lp7.ID)\
        LEFT OUTER JOIN staff AS lp8 ON (t1.Assigned_Staff = lp8.ID)\
        LEFT OUTER JOIN shop_monitoring_status AS lp12 ON (t1.Status = lp12.id)) subq";
        $.ajax({
            url: "../analytics/export_data.php?sql=" + sql + "&tablename=on_site_monitoring",
            dataType: 'JSON',
            success: function (response) {
                if (response.xls) {
                    location.href = response.xls;
                }
                $('#loadingmessage').hide();
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred when creating the Excel file");
            }
        });
    }
    </script>
<?php } ?>