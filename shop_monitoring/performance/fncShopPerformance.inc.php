<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('MAX_EXECUTION_TIME', 900);
ini_set('memory_limit', '-1');

function fncGetOSMResults(){
    $dbh=fncOpenDBConn();
    $sql="select t1.*,concat(wfp_name, ' - ', branch, ' - ', address) as shop_name, t3.governorate as governorate_name,t4.Office
        from shop_monitoring_new t1
        left outer join merchants_wfp t2 on t1.retailer_id=t2.id
        left outer join Governorates t3 on t1.governorate=t3.id
        left outer join price_monitoring_offices t4 on t1.data_gatherer_office=t4.id";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetVariables(){
    $dbh=fncOpenDBConn();
    $sql="select var_name,var_label from shop_monitoring_variables";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$row["var_name"]]=$row["var_label"];
	}
    mssql_close($dbh);
    return $data;
}
function fncGetValues(){
    $dbh=fncOpenDBConn();
    $sql="select * from shop_monitoring_values";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$row["var_name"].$row["value"]]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetPieData($variable){
    $dbh=fncOpenDBConn();
    $data=array();
    $sql="select value_label,count($variable) as count,color from shop_monitoring_new t1
            inner join (select * from shop_monitoring_values where var_name='$variable') t2
            on t1.$variable=t2.value
            group by value_label,color";
    $res = mssql_query($sql,$dbh);
    if($res){
        $rows = mssql_num_rows($res);
        for ($i=1;$i<=$rows;$i++){
            $row = mssql_fetch_assoc($res);
            if($row["color"]=='green') $row["color"]='limegreen';
            if($row["color"]=='lime') $row["color"]='greenyellow';
            $data[]=$row;
        }
        mssql_close($dbh);
    }
    return json_encode($data);
}
function fncGetValueFromLabel($value_label){
    $values=explode("(",$value_label);
    $value=$values[count($values)-1];
    $value=trim($value,")");
    $value=str_replace("+","",$value);
    $value=intval($value);
    return $value;
}
function fncGetShopsForPie($variable,$value){
    $dbh=fncOpenDBConn();
    $sql="select t1.retailer_id,data_gatherer,data_collection_date,concat(wfp_name, ' - ', branch, ' - ', address) as shop_name, t3.governorate as governorate_name,t4.Office
        from shop_monitoring_new t1
        left outer join merchants_wfp t2 on t1.retailer_id=t2.id
        left outer join Governorates t3 on t1.governorate=t3.id
        left outer join price_monitoring_offices t4 on t1.data_gatherer_office=t4.id
        where $variable=$value";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_array($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetRetailerVisits(){
	$dbh=fncOpenDBConn();
    $month=fncGetLastMonthCycle();
    $data=array();
    $sql = "SELECT Merchants_MEPS.Triangulation_ID, concat(wfp_name,' - ',branch, ' - ',[address]) as shop_name,
	SUM(t2.Trans_Amount) AS sales_amount,max(visits) as visits
            FROM     merchants_wfp t1 INNER JOIN
                              Merchants_MEPS ON t1.id = Merchants_MEPS.Triangulation_ID INNER JOIN
                              sales_draft_$month AS t2 ON Merchants_MEPS.Terminal_ID = t2.terminal_id
							  left outer join (select retailer_id,count(*) as visits from shop_monitoring_new group by retailer_id) t4
							  on t1.id=t4.retailer_id
                              where t1.status=1
            GROUP BY Merchants_MEPS.Triangulation_ID,concat(wfp_name,' - ',branch, ' - ',[address])
			Order by Triangulation_ID";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_assoc($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}

function fncGetVisitSummary($month){
	$dbh=fncOpenDBConn();
    $data=array();
    $sql = "SELECT Merchants_MEPS.Triangulation_ID, concat(wfp_name,' - ',branch, ' - ',[address]) as shop_name,
	SUM(t2.Trans_Amount) AS sales_amount,max(visits) as visits, avg(wfp_score) as wfp_score
            FROM     merchants_wfp t1 INNER JOIN
                              Merchants_MEPS ON t1.id = Merchants_MEPS.Triangulation_ID INNER JOIN
                              sales_draft_$month AS t2 ON Merchants_MEPS.Terminal_ID = t2.terminal_id
							  left outer join
                                (select retailer_id,count(*) as visits,avg(awareness+certificates+safety+cleanliness
                                +backroom_safety+backroom_cleanliness+selling_nfi+asylum_checking+
                                understand_project+gives_receipts+check_balance+bad_behaviour+checkout_customers
                                +expired_goods+refrigeration+discounts+prices_shown) as wfp_score
                                from shop_monitoring_new group by retailer_id) t4
							  on t1.id=t4.retailer_id
                              where t1.status=1
            GROUP BY Merchants_MEPS.Triangulation_ID,concat(wfp_name,' - ',branch, ' - ',[address])
			Order by sales_amount desc";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_assoc($res);
        $data[$i]=$row;
	}
    mssql_close($dbh);
    return $data;
}
function fncGetVisitDetails($startmonth,$endmonth){
	$dbh=fncOpenDBConn();
    $data=array();
    $sql = "select retailer_id,replace(left(data_collection_date,7),'-','') as Month,count(*) as visits,avg(awareness+certificates+safety+cleanliness+backroom_safety+backroom_cleanliness+selling_nfi+asylum_checking+
        understand_project+gives_receipts+check_balance+bad_behaviour+checkout_customers+expired_goods+refrigeration+discounts+prices_shown) as wfp_score
        from shop_monitoring_new
        where replace(left(data_collection_date,7),'-','')>=$startmonth and replace(left(data_collection_date,7),'-','')<=$endmonth
        group by retailer_id,left(data_collection_date,7)";
    $res = mssql_query($sql,$dbh);
    $data[0][0] = mssql_num_rows($res);
    for ($i=1;$i<=$data[0][0];$i++){
        $row = mssql_fetch_assoc($res);
        $data[$row["retailer_id"]."_".$row["Month"]]=$row;
	}
    mssql_close($dbh);
    return $data;
}
?>