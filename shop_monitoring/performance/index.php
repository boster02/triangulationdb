<?php
include "../../fncCheckLogin.inc.php";
include "../../analytics/fncAnalytics.inc.php";
include "fncShopPerformance.inc.php";
$var=fncGetVariables();
$val=fncGetValues();
$results=fncGetOSMResults();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>CBT Triangulation - Shop Monitoring</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/css/csstable.css" />
    <script src="https://use.fontawesome.com/18e0f9a250.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--
    <link href="../css/jquery-ui-1.11.4.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.11.1.js"></script>
    <script src="../js/jquery-ui-1.11.4.js"></script>
    <script src="../js/bootstrap.js"></script>
    -->
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="container-fluid" style="background-color: #1f90ff">
                <img src="/images/WFP_logo_white.gif" alt="logo" />
            </div>
            <div class="container-fluid" style="background-color: #72b5f8; margin-top: 1px; padding-top: 2px; padding-bottom: 2px">
                <span style="color:white" class="fla-breadcrumb">
                    <a style="color:white">&nbsp;&nbsp;Home</a>
                </span>
                <span style="color:white" class="pull-right">
                    <a href="/index.php" style="color:white">Logout&nbsp;&nbsp;</a>
                </span>
            </div>
        </nav>
        <div style="margin-left:30px;margin-top:30px">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="index.php">
                        <i class="fa fa-table"></i>
                        <span>&nbsp;&nbsp;&nbsp;Explore&nbsp;&nbsp;&nbsp;</span>
                    </a>
                </li>
                <li>
                    <a href="treeview.php">
                        <i class="fa fa-pie-chart"></i>
                        <span>
                            &nbsp;&nbsp;&nbsp;Analysis&nbsp;&nbsp;&nbsp;
                        </span>
                    </a>
                </li>
                <li>
                    <a href="retailer_visits.php">
                        <i class="fa fa-check-square-o"></i>
                        <span>
                            &nbsp;&nbsp;&nbsp;Retailer Visits&nbsp;&nbsp;&nbsp;
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div id="page-wrapper">
            <br />
            <a class="green-btn" href="javascript:exportDetails()">Export this Report to Excel</a>
            <div id="loadingmessage" style="display:none">
                <img src="/images/download.gif" />
            </div>
            <div id="content">
                <h3 style="text-align:center;font-weight:bold">ON-SITE SHOP MONITORING DATA</h3>
                <div class="row csstable" style="margin-left:10px">
                    <style>
                        .red {
                            background-color: #ff0000;
                        }

                        .orange {
                            background-color: orange;
                        }

                        .yellow {
                            background-color: yellow;
                        }

                        .lime {
                            background-color: greenyellow;
                        }

                        .green {
                            background-color: limegreen;
                        }

                        .grey {
                            background-color: #ebebeb;
                        }
                    </style>
                    <table>
                        <tr class="tableizer-firstrow">
                            <td style="vertical-align:bottom">Retailer ID</td>
                            <td style="vertical-align:bottom">Retailer Name</td>
                            <td style="vertical-align:bottom">Data collection date</td>
                            <td style="vertical-align:bottom">Name of Data Gatherer</td>
                            <td style="vertical-align:bottom">Phone number of Data Gatherer</td>
                            <td style="vertical-align:bottom">Office of the Data Gatherer</td>
                            <td style="vertical-align:bottom">Email of theData Gathere</td>
                            <td style="vertical-align:bottom">Governorate</td>
                            <td style="vertical-align:bottom">Name of the shop representative</td>
                            <td style="vertical-align:bottom">Status of the shop representative</td>
                            <td style="vertical-align:bottom">How much aware are you of WFP Cash Based Transfer and Retail Strategy programme</td>
                            <td style="vertical-align:bottom">In the last 2 months, how often have you been checking UNHCR beneficiary certificates</td>
                            <td style="vertical-align:bottom">In the last 2 months, how often have beneficiaries asked for NFIs</td>
                            <td style="vertical-align:bottom">In the last 2 months, how often have beneficiaries asked to cash their e-cards</td>
                            <td style="vertical-align:bottom">In the last 2 months, how often have beneficiaries presented fake vouchers/e-card</td>
                            <td style="vertical-align:bottom">In the last 2 months, how often have you experienced theft from beneficiaries</td>
                            <td style="vertical-align:bottom">In the last 2 months, how often have you experienced rudeness from beneficiaries</td>
                            <td style="vertical-align:bottom">In the last 2 months, how often have you had issues with the cooperating partner</td>
                            <td style="vertical-align:bottom">Explain CP issues</td>
                            <td style="vertical-align:bottom">In the last 2 months, how often have you had issues with the bank</td>
                            <td style="vertical-align:bottom">Explain bank issues</td>
                            <td style="vertical-align:bottom">How satisfied are you with the WFP CBT programme</td>
                            <td style="vertical-align:bottom">Explain satisfaction</td>
                            <td style="vertical-align:bottom">Do you have any additional issues to let the programme know?</td>
                            <td style="vertical-align:bottom">How many beneficiaries will you be able to interview</td>
                            <td style="vertical-align:bottom">BEN1 - Are preferred items available?</td>
                            <td style="vertical-align:bottom">BEN1 - Which items are not available</td>
                            <td style="vertical-align:bottom">BEN1 - Whow do you rate the prices of essential commodities at this shop?</td>
                            <td style="vertical-align:bottom">BEN1 - Do you receive a receipt after your purchase?</td>
                            <td style="vertical-align:bottom">BEN1 - How accurate is the receipt?</td>
                            <td style="vertical-align:bottom">BEN1 - What are the issues with the receipt</td>
                            <td style="vertical-align:bottom">BEN1 - How far is the closest WFP Contracted Shop from your home?</td>
                            <td style="vertical-align:bottom">BEN1 - Do you have suggestions or recommendtaions for the programme?</td>
                            <td style="vertical-align:bottom">BEN2 - Are preferred items available?</td>
                            <td style="vertical-align:bottom">BEN2 - Which items are not available</td>
                            <td style="vertical-align:bottom">BEN2 - Whow do you rate the prices of essential commodities at this shop?</td>
                            <td style="vertical-align:bottom">BEN2 - Do you receive a receipt after your purchase?</td>
                            <td style="vertical-align:bottom">BEN2 - How accurate is the receipt?</td>
                            <td style="vertical-align:bottom">BEN2 - What are the issues with the receipt</td>
                            <td style="vertical-align:bottom">BEN2 - How far is the closest WFP Contracted Shop from your home?</td>
                            <td style="vertical-align:bottom">BEN2 - Do you have suggestions or recommendtaions for the programme?</td>
                            <td style="vertical-align:bottom">BEN3 - Are preferred items available?</td>
                            <td style="vertical-align:bottom">BEN3 - Which items are not available</td>
                            <td style="vertical-align:bottom">BEN3 - Whow do you rate the prices of essential commodities at this shop?</td>
                            <td style="vertical-align:bottom">BEN3 - Do you receive a receipt after your purchase?</td>
                            <td style="vertical-align:bottom">BEN3 - How accurate is the receipt?</td>
                            <td style="vertical-align:bottom">BEN3 - What are the issues with the receipt</td>
                            <td style="vertical-align:bottom">BEN3 - How far is the closest WFP Contracted Shop from your home?</td>
                            <td style="vertical-align:bottom">BEN3 - Do you have suggestions or recommendtaions for the programme?</td>
                            <td style="vertical-align:bottom">Commodities are stocked in a way than can affect the customers movement or safety?</td>
                            <td style="vertical-align:bottom">Explain safety</td>
                            <td style="vertical-align:bottom">Overall shop cleanliness is:</td>
                            <td style="vertical-align:bottom">The backroom (storage) situation (Safety) is?</td>
                            <td style="vertical-align:bottom">Explain backroom safety</td>
                            <td style="vertical-align:bottom">The backroom (storage) situation (Tidiness, Cleanness) is:</td>
                            <td style="vertical-align:bottom">A shop employee is seen selling NFIs?</td>
                            <td style="vertical-align:bottom">A shop employee is checking beneficiaries asylum seeker?</td>
                            <td style="vertical-align:bottom">Does the shop owner and the employees understand the project (all the elements)?</td>
                            <td style="vertical-align:bottom">What elements do they not understand?</td>
                            <td style="vertical-align:bottom">Is WFP visibility available at the shop?</td>
                            <td style="vertical-align:bottom">The cashier provides the beneficiaries with their receipts?</td>
                            <td style="vertical-align:bottom">Beneficiaries can check their cards balance any time for free?</td>
                            <td style="vertical-align:bottom">Noticed any bad behavior from the employees toward beneficiaries?</td>
                            <td style="vertical-align:bottom">Explain bad behaviour</td>
                            <td style="vertical-align:bottom">Number of customers in the checkout lane (The most crowded checkout if there is more than one):</td>
                            <td style="vertical-align:bottom">Does the shop have any expired or defrosted items or any quality related issues? Please attach photos if possible.:</td>
                            <td style="vertical-align:bottom">The refrigeration conditions are:</td>
                            <td style="vertical-align:bottom">Explain refrigeration</td>
                            <td style="vertical-align:bottom">Which items are not available in the shop?</td>
                            <td style="vertical-align:bottom">Does the shop offer discounts for WFP Beneficiaries?</td>
                            <td style="vertical-align:bottom">Prices shown on the shelves?</td>
                            <td style="vertical-align:bottom">Number of male employees</td>
                            <td style="vertical-align:bottom">Number of female employees</td>
                            <td style="vertical-align:bottom">Additional information</td>
                        </tr>
                        <?php for ($i=1;$i<=$results[0][0];$i++) {?>
                        <tr>
                            <td>
                                <?php echo $results[$i]["retailer_id"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["shop_name"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["data_collection_date"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["data_gatherer"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["data_gatherer_phone"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["Office"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["data_gatherer_email"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["governorate_name"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["shop_rep"] ?>
                            </td>
                            <td>
                                <?php echo $val["shop_rep_type".$results[$i]["shop_rep_type"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["awareness".$results[$i]["awareness"]]["color"]?>">
                                <?php echo $val["awareness".$results[$i]["awareness"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["certificates".$results[$i]["certificates"]]["color"]?>">
                                <?php echo $val["certificates".$results[$i]["certificates"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["nfi".$results[$i]["nfi"]]["color"]?>">
                                <?php echo $val["nfi".$results[$i]["nfi"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["cashing".$results[$i]["cashing"]]["color"]?>">
                                <?php echo $val["cashing".$results[$i]["cashing"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["fake_vouchers".$results[$i]["fake_vouchers"]]["color"]?>">
                                <?php echo $val["fake_vouchers".$results[$i]["fake_vouchers"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["theft".$results[$i]["theft"]]["color"]?>">
                                <?php echo $val["theft".$results[$i]["theft"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["rudeness".$results[$i]["rudeness"]]["color"]?>">
                                <?php echo $val["rudeness".$results[$i]["rudeness"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["cp".$results[$i]["cp"]]["color"]?>">
                                <?php echo $val["cp".$results[$i]["cp"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["cp_explain"] ?>
                            </td>
                            <td class="<?php echo $val["bank".$results[$i]["bank"]]["color"]?>">
                                <?php echo $val["bank".$results[$i]["bank"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["bank_explain"] ?>
                            </td>
                            <td class="<?php echo $val["satisfaction".$results[$i]["satisfaction"]]["color"]?>">
                                <?php echo $val["satisfaction".$results[$i]["satisfaction"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["satisfaction_explain"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["retailer_additional"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["beneficiaries"] ?>
                            </td>
                            <td class="<?php echo $val["availability1".$results[$i]["availability1"]]["color"]?>">
                                <?php echo $val["availability1".$results[$i]["availability1"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["availability1_explain"] ?>
                            </td>
                            <td class="<?php echo $val["prices1".$results[$i]["prices1"]]["color"]?>">
                                <?php echo $val["prices1".$results[$i]["prices1"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["receipts1".$results[$i]["receipts1"]]["color"]?>">
                                <?php echo $val["receipts1".$results[$i]["receipts1"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["accuracy1".$results[$i]["accuracy1"]]["color"]?>">
                                <?php echo $val["accuracy1".$results[$i]["accuracy1"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["accuracy1_explain"] ?>
                            </td>
                            <td class="<?php echo $val["distance1".$results[$i]["distance1"]]["color"]?>">
                                <?php echo $val["distance1".$results[$i]["distance1"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["suggestions1"] ?>
                            </td>
                            <td class="<?php echo $val["availability2".$results[$i]["availability2"]]["color"]?>">
                                <?php echo $val["availability2".$results[$i]["availability2"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["availability2_explain"] ?>
                            </td>
                            <td class="<?php echo $val["prices2".$results[$i]["prices2"]]["color"]?>">
                                <?php echo $val["prices2".$results[$i]["prices2"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["receipts2".$results[$i]["receipts2"]]["color"]?>">
                                <?php echo $val["receipts2".$results[$i]["receipts2"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["accuracy2".$results[$i]["accuracy2"]]["color"]?>">
                                <?php echo $val["accuracy2".$results[$i]["accuracy2"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["accuracy2_explain"] ?>
                            </td>
                            <td class="<?php echo $val["distance2".$results[$i]["distance2"]]["color"]?>">
                                <?php echo $val["distance2".$results[$i]["distance2"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["suggestions2"] ?>
                            </td>
                            <td class="<?php echo $val["availability3".$results[$i]["availability3"]]["color"]?>">
                                <?php echo $val["availability3".$results[$i]["availability3"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["availability3_explain"] ?>
                            </td>
                            <td class="<?php echo $val["prices3".$results[$i]["prices3"]]["color"]?>">
                                <?php echo $val["prices3".$results[$i]["prices3"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["receipts3".$results[$i]["receipts3"]]["color"]?>">
                                <?php echo $val["receipts3".$results[$i]["receipts3"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["accuracy3".$results[$i]["accuracy3"]]["color"]?>">
                                <?php echo $val["accuracy3".$results[$i]["accuracy3"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["accuracy3_explain"] ?>
                            </td>
                            <td class="<?php echo $val["distance3".$results[$i]["distance3"]]["color"]?>">
                                <?php echo $val["distance3".$results[$i]["distance3"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["suggestions3"] ?>
                            </td>
                            <td class="<?php echo $val["safety".$results[$i]["safety"]]["color"]?>">
                                <?php echo $val["safety".$results[$i]["safety"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["safety_explain"] ?>
                            </td>
                            <td class="<?php echo $val["cleanliness".$results[$i]["cleanliness"]]["color"]?>">
                                <?php echo $val["cleanliness".$results[$i]["cleanliness"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["backroom_safety".$results[$i]["backroom_safety"]]["color"]?>">
                                <?php echo $val["backroom_safety".$results[$i]["backroom_safety"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["backroom_safety_explain"] ?>
                            </td>
                            <td class="<?php echo $val["backroom_cleanliness".$results[$i]["backroom_cleanliness"]]["color"]?>">
                                <?php echo $val["backroom_cleanliness".$results[$i]["backroom_cleanliness"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["selling_nfi".$results[$i]["selling_nfi"]]["color"]?>">
                                <?php echo $val["selling_nfi".$results[$i]["selling_nfi"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["asylum_checking".$results[$i]["asylum_checking"]]["color"]?>">
                                <?php echo $val["asylum_checking".$results[$i]["asylum_checking"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["understand_project".$results[$i]["understand_project"]]["color"]?>">
                                <?php echo $val["understand_project".$results[$i]["understand_project"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["understand_project_explain"] ?>
                            </td>
                            <td class="<?php echo $val["visibility".$results[$i]["visibility"]]["color"]?>">
                                <?php echo $val["visibility".$results[$i]["visibility"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["gives_receipts".$results[$i]["gives_receipts"]]["color"]?>">
                                <?php echo $val["gives_receipts".$results[$i]["gives_receipts"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["check_balance".$results[$i]["check_balance"]]["color"]?>">
                                <?php echo $val["check_balance".$results[$i]["check_balance"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["bad_behaviour".$results[$i]["bad_behaviour"]]["color"]?>">
                                <?php echo $val["bad_behaviour".$results[$i]["bad_behaviour"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["bad_behaviour_explain"] ?>
                            </td>
                            <td class="<?php echo $val["checkout_customers".$results[$i]["checkout_customers"]]["color"]?>">
                                <?php echo $val["checkout_customers".$results[$i]["checkout_customers"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["expired_goods".$results[$i]["expired_goods"]]["color"]?>">
                                <?php echo $val["expired_goods".$results[$i]["expired_goods"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["refrigeration".$results[$i]["refrigeration"]]["color"]?>">
                                <?php echo $val["refrigeration".$results[$i]["refrigeration"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["refrigeration_explain"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["unavailable_items"] ?>
                            </td>
                            <td class="<?php echo $val["discounts".$results[$i]["discounts"]]["color"]?>">
                                <?php echo $val["discounts".$results[$i]["discounts"]]["value_label"] ?>
                            </td>
                            <td class="<?php echo $val["prices_shown".$results[$i]["prices_shown"]]["color"]?>">
                                <?php echo $val["prices_shown".$results[$i]["prices_shown"]]["value_label"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["male_employees"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["female_employees"] ?>
                            </td>
                            <td>
                                <?php echo $results[$i]["additional_info"] ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/jquery-1.12.0.js"></script>
    <script src="/js/bootstrap-3.0.3.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.11.4.js"></script>
    <script src="/js/treeview.js"></script>
    <script>
        function exportDetails() {
            $("#loadingmessage").show();
            var data = $("#content").html();
            $.ajax({
                url: "/analytics/export_html.php",
                type: "POST",
                dataType: 'json',
                data: { tablename: 'OSM_heat_map', html: data },
                success: function (response) {
                    if (response.xls) {
                        location.href = "/" + response.xls;
                    }
                    $('#loadingmessage').hide();
                },
                error: function (xhr, status, error) {
                    $('#loadingmessage').html(xhr.responseText);
                    alert("An error has occurred when creating the Excel file");
                }
            });
        }
    </script>
</body>
</html>
