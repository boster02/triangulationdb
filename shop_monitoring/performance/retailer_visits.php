<?php
include "../../fncCheckLogin.inc.php";
include "../../analytics/fncAnalytics.inc.php";
include "fncShopPerformance.inc.php";
$startmonth=date("Y")."01";
$endmonth=date("Ym");
$lastmonth=fncGetLastMonthCycle();
$summary=fncGetVisitSummary($lastmonth);
$details=fncGetVisitDetails($startmonth,$endmonth);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>CBT Triangulation - Shop Monitoring</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/css/csstable.css" />
    <link rel="stylesheet" type="text/css" href="/css/treeview.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <script src="https://use.fontawesome.com/18e0f9a250.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }
    </style>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0">
            <div class="container-fluid" style="background-color: #1f90ff">
                <img src="/images/WFP_logo_white.gif" alt="logo" />
            </div>
            <div class="container-fluid" style="background-color: #72b5f8; margin-top: 1px; padding-top: 2px; padding-bottom: 2px">
                <span style="color:white" class="fla-breadcrumb">
                    <a style="color:white">&nbsp;&nbsp;Home</a>
                </span>
                <span style="color:white" class="pull-right">
                    <a href="/index.php" style="color:white">Logout&nbsp;&nbsp;</a>
                </span>
            </div>
        </nav>
        <div style="margin-left:30px;margin-top:30px">
            <ul class="nav nav-tabs">
                <li>
                    <a href="index.php">
                        <i class="fa fa-table"></i>
                        <span>&nbsp;&nbsp;&nbsp;Explore&nbsp;&nbsp;&nbsp;</span>
                    </a>
                </li>
                <li>
                    <a href="treeview.php">
                        <i class="fa fa-pie-chart"></i>
                        <span>
                            &nbsp;&nbsp;&nbsp;Analysis&nbsp;&nbsp;&nbsp;
                        </span>
                    </a>
                </li>
                <li class="active">
                    <a href="retailer_visits.php">
                        <i class="fa fa-check-square-o"></i>
                        <span>
                            &nbsp;&nbsp;&nbsp;Retailer Visits&nbsp;&nbsp;&nbsp;
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="row" style="margin-left:30px">
            <!-----------------Start Content-------------->
            <div class="csstable">
                <br />
                <table>
                    <tr>
                        <td>
                            <strong>Branch ID</strong>
                        </td>
                        <td>
                            <strong>Shop Name</strong>
                        </td>
                        <td>
                            <strong>
                                <?php echo fncGetMonthName($lastmonth)." Sales (JOD)"?>
                            </strong>
                        </td>
                        <?php for($month=$startmonth;$month<=$endmonth;$month++){ ?>
                        <td colspan="2">
                            <?php echo fncGetMonthName($month) ?>
                        </td>
                        <?php } ?>
                        <td>
                            <strong>Total Visits</strong>
                        </td>
                        <td>
                            <strong>Overall Score (out of 37)</strong>
                        </td>
                    </tr>
                    <tr style="background-color:silver">
                        <td>
                            <strong></strong>
                        </td>
                        <td>
                            <strong></strong>
                        </td>
                        <td>
                            <strong>
                                &nbsp;
                            </strong>
                        </td>
                        <?php for($month=$startmonth;$month<=$endmonth;$month++){ ?>
                        <td>
                            Visits
                        </td>
                        <td>
                            Score
                        </td>
                        <?php } ?>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <?php for($i=1;$i<=$summary[0][0];$i++){?>
                    <tr>
                        <td>
                            <?php echo $summary[$i]["Triangulation_ID"] ?>
                        </td>
                        <td>
                            <?php echo $summary[$i]["shop_name"] ?>
                        </td>
                        <td class="number">
                            <?php echo number_format($summary[$i]["sales_amount"],2) ?>
                        </td>
                        <?php for($month=$startmonth;$month<=$endmonth;$month++){
                         $month_visits=isset($details[$summary[$i]["Triangulation_ID"]."_".$month]["visits"])?$details[$summary[$i]["Triangulation_ID"]."_".$month]["visits"]:"0";
                         $month_score=isset($details[$summary[$i]["Triangulation_ID"]."_".$month]["wfp_score"])?$details[$summary[$i]["Triangulation_ID"]."_".$month]["wfp_score"]:"";
                        ?>
                            <td class="number">
                                <?php echo $month_visits ?>
                            </td>
                            <td class="number">
                                <?php echo $month_score   ?><?php echo $month_score!=""?" (".number_format($month_score*100/37,1)."%)":""?>
                            </td>
                        <?php } ?>
                        <td class="number">
                            <a href="javascript:showOSM(<?php echo $summary[$i]["Triangulation_ID"] ?>)">
                                <?php echo number_format($summary[$i]["visits"]) ?>
                            </a>
                            <a style="text-decoration:none" href="javascript:exportOSM(<?php echo $summary[$i]["Triangulation_ID"] ?>)">
                                <img src="/images/exportxls.png" alt="Export" />
                            </a>
                        </td>
                        <td class="number">
                            <?php echo $summary[$i]["wfp_score"] ?>
                            <?php echo $summary[$i]["wfp_score"]>0?" (".number_format($summary[$i]["wfp_score"]*100/37,1)."%)":""?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            <!-----------------End Content-------------->
        </div>
    </div>
    <div id="dialog" title="Shop profile details">
        <div id="shop-details"></div>
    </div>
    <script src="/js/jquery-1.12.0.js"></script>
    <script src="/js/bootstrap-3.0.3.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.11.4.js"></script>
    <script src="/js/treeview.js"></script>
    <!-- AM Charts -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/dark.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script>
        var question;
        $(function () {
            $("#dialog").dialog({
                autoOpen: false,
                maxWidth: 800,
                maxHeight: 500,
                width: 800,
                height: 500,
                modal: true
            });
        });
        function showOSM(id) {
            $("#dialog").dialog("open");
            $("#shop-details").html("<img src='../images/download.gif' />");
            $.get("/analytics/shop_osm.php?id=" + id, function (data, status) {
                $("#shop-details").html(data);
            });
        }
        function exportOSM(id) {
            $.get("/analytics/shop_osm.php?id=" + id, function (data, status) {
                $.ajax({
                    url: "/analytics/export_html.php",
                    type: "POST",
                    dataType: 'json',
                    data: { tablename: 'OSM_raw_data', html: data },
                    success: function (response) {
                        if (response.xls) {
                            location.href = "/" + response.xls;
                        }
                        $('#loadingmessage').hide();
                    },
                    error: function (xhr, status, error) {
                        $('#loadingmessage').html(xhr.responseText);
                        alert("An error has occurred when creating the Excel file");
                    }
                });
            });
        }
    </script>
</body>
</html>