<?php
include "../../fncCheckLogin.inc.php";
include "../../analytics/fncAnalytics.inc.php";
include "fncShopPerformance.inc.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>CBT Triangulation - Shop Monitoring</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/css/csstable.css" />
    <link rel="stylesheet" type="text/css" href="/css/treeview.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <script src="https://use.fontawesome.com/18e0f9a250.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }
    </style>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0">
            <div class="container-fluid" style="background-color: #1f90ff">
                <img src="/images/WFP_logo_white.gif" alt="logo" />
            </div>
            <div class="container-fluid" style="background-color: #72b5f8; margin-top: 1px; padding-top: 2px; padding-bottom: 2px">
                <span style="color:white" class="fla-breadcrumb">
                    <a style="color:white">&nbsp;&nbsp;Home</a>
                </span>
                <span style="color:white" class="pull-right">
                    <a href="/index.php" style="color:white">Logout&nbsp;&nbsp;</a>
                </span>
            </div>
        </nav>
        <div style="margin-left:30px;margin-top:30px">
            <ul class="nav nav-tabs">
                <li>
                    <a href="index.php">
                        <i class="fa fa-table"></i>
                        <span>&nbsp;&nbsp;&nbsp;Explore&nbsp;&nbsp;&nbsp;</span>
                    </a>
                </li>
                <li class="active">
                    <a href="treeview.php">
                    <i class="fa fa-pie-chart"></i>
                        <span>
                            &nbsp;&nbsp;&nbsp;Analysis&nbsp;&nbsp;&nbsp;
                        </span>
                    </a>
                </li>
                <li>
                    <a href="retailer_visits.php">
                        <i class="fa fa-check-square-o"></i>
                        <span>
                            &nbsp;&nbsp;&nbsp;Retailer Visits&nbsp;&nbsp;&nbsp;
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="row" style="margin-left:30px">
            <div class="col-md-4">
                <h3 style="font-weight:bold">Indicators</h3>
                <ul id="tree1">
                    <li>
                        <a href="#">NO RATING</a>
                        <ul>
                            <li>
                                <a href="#">
                                    Operational Compliance - WFP
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('satisfaction')">How satisfied are you with the WFP CBT programme?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('visibility')">Is WFP visibility available at the shop?</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Operational Compliance - Beneficiaries
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('nfi')">In the last 2 months, how often have beneficiaries asked for NFIs?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('cashing')">In the last 2 months, how often have beneficiaries asked to cash their ecards?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('fake_vouchers')">In the last 2 months, how often have beneficiaries presented fake vouchers/ecard?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('rudeness')">In the last 2 months, how often have you experienced rudeness from beneficiaries?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('theft')">In the last 2 months, how often have you experienced theft from beneficiaries?</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Operational Compliance - CP
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('cp')">In the last 2 months, how often have you had issues with the cooperating partner?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('cp_explain')">What were the CP issues?</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Operational Compliance - Bank
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('bank')">In the last 2 months, how often have you had issues with the bank?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('bank_explain')">What were the Bank issues?</a>
                                        </li>
                                    </ul>
</li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">SHOP RATING</a>
                        <ul>
                            <li>
                                <a href="#">
                                    Commodity Availability
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('unavailable_items')">Which commodities are unavailable in the shop?</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Commodity Quality
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('expired')">Does the shop have any expired or defrosted items or any quality related issues?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('inspection_visit')">How often is the shop visited by JFDA or Municipality for inspection?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('defrosting')">The food items in the freezer are frozen and there is not signs of defrosting water?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('refrigeration')">How are the refrigeration conditions?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('fridge_temparature')">What is the temperature of the Freezer?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('jfda_visit')">When was the shop last visited by JFDA or Municipality?</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Customer Service
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('check_balance')">Beneficiaries can check their cards balance any time for free?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('cleanliness')">How would you rate the overall shopping environment (cleanliness, lighting, commodity display, shoping space, fresh smell, tidiness) </a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('shop_experience')">How would you rate the overall shopping experience (Variety, Price, waiting time, staff friendliness, display of items and signs)</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('bad_behaviour')">Noticed any bad behavior from the employees toward beneficiaries?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('checkout_customers')">Number of customers in the checkout lane (The most crowded checkout if there is more than one):</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('receipts')">The cashier provides the beneficiaries with their receipts?</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Gender Inclusive
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('employment_stats')">Employment stats:</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('disabled')">Shop has access way for adult or disabled</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('trolley')">Shop Provides trolley for customer use</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Operational Compliance
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('asylum_checking')">A shop employee is checking beneficiaries’ asylum seeker?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('selling_nfi')">A shop employee is seen selling NFIs on a WFP e-card?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('understand_project')">Does the shop owner and the employees understand the project (all the elements)?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('awareness')">How much aware are you of WFP Cash Based Transfer and Retail Engagement </a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('gave_cash')">In the last 2 months, how often have given beneficiaries cash in exchange of ecard value</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('certificate')">In the last 2 months, how often have you been checking UNHCR beneficiary certificates</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('sold_nfi')">In the last 2 months, how often have you sold NFIs on the WFP E-card?</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Price
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('discount')">Does the shop offer discounts for WFP Beneficiaries?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('prices_shown')">Prices shown on the shelves?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('price_changes')">The prices displayed are the same on the checkout</a>
                                        </li>
                                    </ul>
</li>
                            <li>
                                <a href="#">
                                    Safety and Security
                                </a>
                                    <ul>
                                        <li>
                                            <a href="javascript:showResults('aisless_clear')">All aisles and passageways are kept clear and in good repair, with no obstruction that could create a hazard.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('safety')">Commodities are stocked in a way than can affect the customers movement or safety</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('garbage_covered')">Garbage is kept covered and clear of insects and rodents.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('garbage_removed')">Garbage is quickly removed and dumped in appropriate bins</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('cleanliness')">Overall shop cleanliness is:</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('backroom_safety')">The backroom (storage) situation (Safety) is?</a>
                                        </li>
                                        <li>
                                            <a href="javascript:showResults('backroom_cleanliness')">The backroom (storage) situation (Tidiness, Cleanness) is:</a>
                                        </li>
                                    </ul>
</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <h3 style="font-weight:bold">Results</h3>
                <div id="chartdiv"><em>Select indicator to view results</em></div>
            </div>
        </div>
    </div>
    <div id="dialog" title="Shop profile details">
        <div id="shop-details"></div>
    </div>
    <script src="/js/jquery-1.12.0.js"></script>
    <script src="/js/bootstrap-3.0.3.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.11.4.js"></script>
    <script src="/js/treeview.js"></script>
    <!-- AM Charts -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/dark.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script>
        var question;
        $(function () {
            $("#dialog").dialog({
                autoOpen: false,
                maxWidth: 800,
                maxHeight: 500,
                width: 800,
                height: 500,
                modal: true
            });
        });
        function showResults(variable) {
            question = variable;
            viewChart(variable);
        }
        function viewChart(variable) {
            var chart = AmCharts.makeChart("chartdiv", {
                "type": "pie",
                "dataLoader": {
                    "url": "ajax_data.php?variable="+variable
                },
                "valueField": "count",
                "titleField": "value_label",
                "colorField": "color",
                "balloon": {
                    "fixedPosition": true
                },
                "export": {
                    "enabled": true
                }
            });
            chart.addListener("clickSlice", handleClick);
        }
        function handleClick(event) {
            var dp = event.dataItem.dataContext;
            //alert(dp["value_label"]);
            $("#dialog").dialog("open");
            $("#shop-details").html("<img src='/images/download.gif' />");
            $.get("treeview_shops.php?variable=" + question + "&value_label=" + dp["value_label"], function (data, status) {
                $("#shop-details").html(data);
            });
        }
    </script>
</body>
</html>