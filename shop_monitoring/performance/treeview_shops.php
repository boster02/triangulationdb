<?php
include "../../analytics/fncAnalytics.inc.php";
include("fncShopPerformance.inc.php");
$value=fncGetValueFromLabel($_GET["value_label"]);
$shops=fncGetShopsForPie($_GET["variable"],$value);
?>
<div class="csstable">
    <table border="1" style="border-collapse:collapse">
        <tr>
            <td>Retailer ID</td>
            <td>Retailer Name</td>
            <td>Data collection date</td>
            <td>Name of Data Gatherer</td>
            <td>Governorate</td>
        </tr>
        <?php for($i=1;$i<=$shops[0][0];$i++){?>
        <tr>
            <td>
                <?php echo $shops[$i]["retailer_id"] ?>
            </td>
            <td>
                <?php echo $shops[$i]["shop_name"] ?>
            </td>
            <td>
                <?php echo $shops[$i]["data_collection_date"] ?>
            </td>
            <td>
                <?php echo $shops[$i]["data_gatherer"] ?>
            </td>
            <td>
                <?php echo $shops[$i]["governorate_name"] ?>
            </td>
        </tr>
        <?php } ?>
    </table>
</div>
