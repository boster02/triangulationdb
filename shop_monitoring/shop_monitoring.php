<?php session_start();
      include("fncShopMonitoring.inc.php");
      include("list_shop_monitoring.php");
      include("view_shop_monitoring.php");
      include("edit_shop_monitoring.php");
      include("../fncCashAnalyzer.inc.php");

      if (isset($_GET["order"])) $order = @$_GET["order"];
      if (isset($_GET["type"])) $ordtype = @$_GET["type"];

      if (isset($_POST["filter"])) $filter = @$_POST["filter"];
      if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
      $wholeonly = false;
      if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

      if (!isset($order) && isset($_SESSION["shop_monitoring_order"])) $order = $_SESSION["shop_monitoring_order"];
      if (!isset($ordtype) && isset($_SESSION["shop_monitoring_type"])) $ordtype = $_SESSION["shop_monitoring_type"];
      if (!isset($filter) && isset($_SESSION["shop_monitoring_filter"])) $filter = $_SESSION["shop_monitoring_filter"];
      if (!isset($filterfield) && isset($_SESSION["shop_monitoring_filter_field"])) $filterfield = $_SESSION["shop_monitoring_filter_field"];

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>CBT Triangulation - Shop Monitoring</title>

    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../css/csstable.css" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../css/jquery-ui-1.11.4.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.11.1.js"></script>
    <script src="../js/jquery-ui-1.11.4.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script>
        var msg;
        var error;
        $().ready(function (e) {
            $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
            if (getParameterByName("a") == "edit" || getParameterByName("a") == "add") {
                $('#update_data').modal('show');
            }
            if (getParameterByName("a") == "view") {
                $('#view_data').modal('show');
            }
        });
        function validateForm() {
            msg = "";
            error = false;

            if (error) {
                alert(msg);
                return false;
            }
            else {
                return true;
            }
        }
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        function checkRequired(param, message) {
            if (document.getElementById("form1").elements[param].value == null
                || document.getElementById("form1").elements[param].value == '') {
                error = true;
                msg = msg + "\n\n" + message;
            }
        }

        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                alert("Only Numbers please!");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <style>
        body {
            overflow-x:hidden;
            background-image:url(images/background.jpg);
        }
    </style>
</head>

<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="container-fluid" style="background-color: #1f90ff">
                <img src="../images/WFP_logo_white.gif" alt="logo" />
            </div>
            <div class="container-fluid" style="background-color: #72b5f8; margin-top: 1px; padding-top: 2px; padding-bottom: 2px">
                <span style="color:white" class="fla-breadcrumb">
                    <a style="color:white">&nbsp;&nbsp;Home</a>
                </span>
                <span style="color:white" class="pull-right">
                    <a href="/index.php" style="color:white">Logout&nbsp;&nbsp;</a>
                </span>
            </div>
        </nav>

        <div id="page-wrapper">
           
                <div class="row">
                    
                    <div class="col-lg-12">
                        <?php
                        $conn=fncOpenDBConn();
                        $showrecs = 2500;
                        $pagerange = 10;

                        $a = @$_GET["a"];
                        $recid = @$_GET["recid"];
                        $page = @$_GET["page"];
                        if (!isset($page)) $page = 1;

                        $sql = @$_POST["sql"];

                        switch ($sql) {
                            case "insert":
                                sql_insert();
                                break;
                            case "update":
                                sql_update();
                                break;
                            case "delete":
                                sql_delete();
                                break;
                        }

                        switch ($a) {
                            case "add":
                                select();
                                addrec();
                                break;
                            case "view":
                                select();
                                viewrec($recid);
                                break;
                            case "edit":
                                select();
                                editrec($recid);
                                break;
                            case "del":
                                select();
                                deleterec($recid);
                                break;
                            default:
                                select();
                                break;
                        }

                        if (isset($order)) $_SESSION["shop_monitoring_order"] = $order;
                        if (isset($ordtype)) $_SESSION["shop_monitoring_type"] = $ordtype;
                        if (isset($filter)) $_SESSION["shop_monitoring_filter"] = $filter;
                        if (isset($filterfield)) $_SESSION["shop_monitoring_filter_field"] = $filterfield;
                        if (isset($wholeonly)) $_SESSION["shop_monitoring_wholeonly"] = $wholeonly;

                        ?>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </div>
        </div>
</body>

</html>
