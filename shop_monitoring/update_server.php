<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
include("../config.inc.php");
$dbh=fncOpenDBConn();
if(isset($_POST["id"])){
    $data_collection_date=$_POST['data_collection_date'];
    $data_gatherer=$_POST['data_gatherer'];
    $data_gatherer_phone=$_POST['data_gatherer_phone'];
    $data_gatherer_office=$_POST['data_gatherer_office'];
    $data_gatherer_email=$_POST['data_gatherer_email'];
    $gps_lat=floatval($_POST['gps_lat']);
    $gps_long=floatval($_POST['gps_long']);
    $governorate=$_POST['governorate'];
    $retailer_id=$_POST['retailer_id'];
    $shop_rep=sqlvalue2($_POST['shop_rep'],true);
    $shop_rep_type=$_POST['shop_rep_type'];
    $awareness=$_POST['awareness'];
    $certificates=$_POST['certificates'];
    $nfi=$_POST['nfi'];
    $cashing=$_POST['cashing'];
    $fake_vouchers=$_POST['fake_vouchers'];
    $theft=$_POST['theft'];
    $rudeness=$_POST['rudeness'];
    $cp=$_POST['cp'];
    $cp_other=sqlvalue2($_POST['cp_other'],true);
    $bank=$_POST['bank'];
    $bank_other=sqlvalue2($_POST['bank_other'],true);
    $satisfaction=$_POST['satisfaction'];
    $retailer_additional=sqlvalue2($_POST['retailer_additional'],true);
    $availability1=$_POST['availability1'];
    $prices1=$_POST['prices1'];
    $receipts1=$_POST['receipts1'];
    $distance1=$_POST['distance1'];
    $availability2=$_POST['availability2'];
    $prices2=$_POST['prices2'];
    $receipts2=$_POST['receipts2'];
    $distance2=$_POST['distance2'];
    $availability3=$_POST['availability3'];
    $prices3=$_POST['prices3'];
    $receipts3=$_POST['receipts3'];
    $distance3=$_POST['distance3'];
    $any_other=sqlvalue2($_POST['any_other'],true);
    $sql="INSERT INTO shop_monitoring "
                        . "(data_collection_date,data_gatherer,data_gatherer_phone,data_gatherer_office,"
                        . "data_gatherer_email,gps_lat,gps_long,governorate,retailer_id,shop_rep,shop_rep_type,awareness,certificates,"
                        . "nfi,cashing,fake_vouchers,theft,rudeness,cp,cp_other,bank,bank_other,satisfaction,"
                        . "retailer_additional,availability1,prices1,receipts1,distance1,"
                        . "availability2,prices2,receipts2,distance2,availability3,prices3,receipts3,distance3,any_other)"
                        . " VALUES ($data_collection_date,
                                    $data_gatherer,
                                    $data_gatherer_phone,
                                    $data_gatherer_office,
                                    $data_gatherer_email,
                                    $gps_lat,
                                    $gps_long,
                                    $governorate,
                                    $retailer_id,
                                    $shop_rep,
                                    $shop_rep_type,
                                    $awareness,
                                    $certificates,
                                    $nfi,
                                    $cashing,
                                    $fake_vouchers,
                                    $theft,
                                    $rudeness,
                                    $cp,
                                    $cp_other,
                                    $bank,
                                    $bank_other,
                                    $satisfaction,
                                    $retailer_additional,
                                    $availability1,
                                    $prices1,
                                    $receipts1,
                                    $distance1,
                                    $availability2,
                                    $prices2,
                                    $receipts2,
                                    $distance2,
                                    $availability3,
                                    $prices3,
                                    $receipts3,
                                    $distance3,
                                    $any_other)";
    $res=mssql_query($sql,$dbh);
    if($res){
        fncDeliverResponse("200","The price has been succcessfully updated",$_POST["id"]);
    }
    else{
        fncDeliverResponse("400","An error occured while uploading the file",$_POST["id"]);
    }
}
else{
    fncDeliverResponse("400","Error: incomplete data","");
}
