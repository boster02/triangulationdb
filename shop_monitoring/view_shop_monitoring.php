<?php function showrow($row, $recid)
      {
?>
<div class="modal fade" id="view_data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">View Shop Monitoring Issues</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td style="vertical-align:top">
                                <table class="table table-striped table-bordered" border="0" cellspacing="1" cellpadding="5" style="width:320px">
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Source")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["lp_Source"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Retailer ID")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["lp_Retailer_ID"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Branch ID")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["lp_Branch_ID"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Main Performance Area")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["lp_Main_Performance_Area"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Issue Identified")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["Issue_Identified"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Date")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["Date"]) ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="vertical-align:top">
                                <table class="table table-striped table-bordered" border="0" cellspacing="1" cellpadding="5" style="width:320px">
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Function Assigned")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["lp_Function_Assigned"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Assigned Staff")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["lp_Assigned_Staff"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Action Taken")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["Action_Taken"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Comments")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["Comments"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Outstanding Issue")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["Outstanding_Issue"]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hr">
                                            <?php echo htmlspecialchars("Status")."&nbsp;" ?>
                                        </td>
                                        <td class="dr">
                                            <?php echo htmlspecialchars($row["lp_Status"]) ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
