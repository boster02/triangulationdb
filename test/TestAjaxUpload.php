<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <form id="form1" method="post" enctype="multipart/form-data">
        <input id="sortpicture" type="file" name="sortpic" />
        <button id="upload">Upload</button>
    </form>
    <script src="js/jquery-1.12.0.js"></script>
    <script>
    $('#upload').on('click', function () {
        var file_data = $('#sortpicture').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: 'TestAjaxUploadSave.php', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (php_script_response) {
                
            }
        });
    });
    </script>
</body>
</html>


