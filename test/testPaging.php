<?php

function getPage($stmt, $pageNum, $rowsPerPage)
{
	$offset = ($pageNum - 1) * $rowsPerPage;
	$rows = array();
	$i = 0;
	while(($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC, SQLSRV_SCROLL_ABSOLUTE, $offset + $i)) && $i < $rowsPerPage)
	{
		array_push($rows, $row);
		$i++;
	}
	return $rows;
}

// Set the number of rows to be returned on a page.
$rowsPerPage = 10;

// Connect to the server.
$serverName = '10.67.67.130';
$connOptions = array("UID" => "sa", "PWD" => "P@ssword1", "Database"=>"Northwind");
$conn = sqlsrv_connect($serverName, $connOptions);
if (!$conn)
	die( print_r( sqlsrv_errors(), true));

// Define and execute the query.  
// Note that the query is executed with a "scrollable" cursor.
$sql = "SELECT CustomerID, ShipName FROM Orders";

$stmt = sqlsrv_query($conn, $sql, array(), array( "Scrollable" => 'static' ));
if ( !$stmt )
	die( print_r( sqlsrv_errors(), true));

// Get the total number of rows returned by the query.
// Display links to "pages" of rows.
$rowsReturned = sqlsrv_num_rows($stmt);
if($rowsReturned === false)
    die( print_r( sqlsrv_errors(), true));
elseif($rowsReturned == 0)
{
    echo "No rows returned.";
	exit();
}
else
{     
    // Display page links.
    $numOfPages = ceil($rowsReturned/$rowsPerPage);
    for($i = 1; $i<=$numOfPages; $i++)
    {
        $pageLink = "?pageNum=$i";
        print("<a href=$pageLink>$i</a>&nbsp;&nbsp;");
    }
    echo "<br/><br/>";
}

// Display the selected page of data.
echo "<table border='1px'>";
$pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
$page = getPage($stmt, $pageNum, $rowsPerPage);

foreach($page as $row)
	echo "<tr><td>$row[0]</td><td>$row[1]</td></tr>";

echo "</table>";

sqlsrv_close( $conn );
?>
