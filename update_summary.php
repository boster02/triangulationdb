<?php session_start();
ini_set('MAX_EXECUTION_TIME', 30000);
ini_set('memory_limit', '-1');
include "config.inc.php";
$dbh=fncOpenDBConn();
if ( sqlsrv_begin_transaction( $dbh ) === false ) {
    die( print_r( sqlsrv_errors(), true ));
}
for($year=2014;$year<=2017;$year++){
    for($month=1;$month<=12;$month++){
        if($month<10) $month="0".$month;
        $cycle=$year.$month;
        $previousmonth=fncGetLastMonth($cycle);

        //-------------Cumulative figures------------------------
        $cumulative_households="Update summary set cumulative_households=(select COUNT(distinct customer_id) from prepaid_reload_meps where left(Trans_Date,6)<=$cycle) where cycle=$cycle";
        $res1=sqlsrv_query($dbh,$cumulative_households);

        $cumulative_beneficiaries="Update summary set cumulative_beneficiaries=(select sum(hhsize)from (select beneficiary_id,max(household_size) as hhsize from prepaid_reload_wfp
                                    where left(Trans_date,6)<=$cycle
                                    group by beneficiary_id) as t1) where cycle=$cycle";
        $res2=sqlsrv_query($dbh,$cumulative_beneficiaries);

        $cumulative_reload_cycles="Update summary set cumulative_reload_cycles=(select Count(Distinct left(Trans_Date,6)) from prepaid_reload_meps where left(Trans_Date,6)<=$cycle) where cycle=$cycle";
        $res3=sqlsrv_query($dbh,$cumulative_reload_cycles);

        $cumulative_reload_value="Update summary set cumulative_reload_value=(select SUM(Trans_Amount) from prepaid_reload_meps where left(Trans_Date,6)<=$cycle) where cycle=$cycle";
        $res4=sqlsrv_query($dbh,$cumulative_reload_value);

        $cumulative_no_of_reloads="Update summary set cumulative_no_of_reloads=(select Count(*) from prepaid_reload_meps where left(Trans_Date,6)<=$cycle) where cycle=$cycle";
        $res5=sqlsrv_query($dbh,$cumulative_no_of_reloads);

        $cumulative_sales_transactions="Update summary set cumulative_sales_transactions=(select Count(*) from sales_draft where left(Trans_Date,6)<=$cycle) where cycle=$cycle";
        $res6=sqlsrv_query($dbh,$cumulative_sales_transactions);

        $cumulative_sales_value="Update summary set cumulative_sales_value=(select Sum(Trans_Amount) from sales_draft where left(Trans_Date,6)<=$cycle) where cycle=$cycle";
        $res7=sqlsrv_query($dbh,$cumulative_sales_value);

        //-------------This month figures------------------------
        $thismonth_households="Update summary set thismonth_households=(select Count(distinct Customer_ID) from pre_paid_reload_meps_$cycle) where cycle=$cycle";
        $res8=sqlsrv_query($dbh,$thismonth_households);

        $thismonth_beneficiaries="Update summary set thismonth_beneficiaries=(select Sum(household_size) from pre_paid_reload_wfp_$cycle) where cycle=$cycle";
        $res9=sqlsrv_query($dbh,$thismonth_beneficiaries);

        $thismonth_reload_value="Update summary set thismonth_reload_value=(select Sum(Trans_Amount) from pre_paid_reload_meps_$cycle) where cycle=$cycle";
        $res10=sqlsrv_query($dbh,$thismonth_reload_value);

        $thismonth_sales_transactions="Update summary set thismonth_sales_transactions=(select count(*) from sales_draft_$cycle) where cycle=$cycle";
        $res11=sqlsrv_query($dbh,$thismonth_sales_transactions);

        $thismonth_sales_value="Update summary set thismonth_sales_value=(select sum(Trans_amount) from sales_draft_$cycle) where cycle=$cycle";
        $res12=sqlsrv_query($dbh,$thismonth_sales_value);

        $thismonth_reload_cycles="Update summary set thismonth_reload_cycles=1 where cycle=$cycle";
        $res13=sqlsrv_query($dbh,$thismonth_reload_cycles);

        //----------------Previous month figures---------------------------------------
        $previousmonth_households="Update summary set previousmonth_households=(select Count(distinct Customer_ID) from pre_paid_reload_meps_$previousmonth) where cycle=$cycle";
        $res14=sqlsrv_query($dbh,$previousmonth_households);

        $previousmonth_beneficiaries="Update summary set previousmonth_beneficiaries=(select sum(household_size) from pre_paid_reload_wfp_$previousmonth) where cycle=$cycle";
        $res15=sqlsrv_query($dbh,$previousmonth_beneficiaries);

        $previousmonth_reload_value="Update summary set previousmonth_reload_value=(select Sum(Trans_Amount) from pre_paid_reload_meps_$previousmonth) where cycle=$cycle";
        $res16=sqlsrv_query($dbh,$previousmonth_reload_value);

        $previousmonth_sales_transactions="Update summary set previousmonth_sales_transactions=(select count(*) from sales_draft_$previousmonth) where cycle=$cycle";
        $res17=sqlsrv_query($dbh,$previousmonth_sales_transactions);

        $previousmonth_sales_value="Update summary set previousmonth_sales_value=(select sum(Trans_amount) from sales_draft_$previousmonth) where cycle=$cycle";
        $res18=sqlsrv_query($dbh,$previousmonth_sales_value);

        $previousmonth_reload_cycles="Update summary set previousmonth_reload_cycles=1 where cycle=$cycle";
        $res19=sqlsrv_query($dbh,$previousmonth_reload_cycles);

        //-----------------------Other summaries----------------------------------------------
        $loa_reload="Update summary set loa_reload=(select Sum(Trans_Amount) from pre_paid_reload_loa where cycle=$cycle) where cycle=$cycle";
        $res20=sqlsrv_query($dbh,$loa_reload);

        $wfp_reload="Update summary set wfp_reload=(select Sum(Trans_Amount) from pre_paid_reload_wfp_$cycle) where cycle=$cycle";
        $res21=sqlsrv_query($dbh,$wfp_reload);

        $jab_reload="Update summary set jab_reload=(select Sum(Amount) from dbo.jab_statement_$cycle) where cycle=$cycle";
        $res22=sqlsrv_query($dbh,$jab_reload);

        $meps_reload="Update summary set meps_reload=(select Sum(Trans_Amount) from pre_paid_reload_meps_$cycle) where cycle=$cycle";
        $res23=sqlsrv_query($dbh,$meps_reload);

        $jab_pressed="Update summary set jab_pressed=(select count(*) from jab_vrl where substring(Creation_Date,7,4)+substring(Creation_Date,4,2)='$cycle') where cycle=$cycle";
        $res24=sqlsrv_query($dbh,$jab_pressed);

        $programme_listing="Update summary set programme_listing=(select count(*) from vrl) where cycle=$cycle";
        $res25=sqlsrv_query($dbh,$programme_listing);

        $distributed="Update summary set distributed_cards=(select count(*) from vrl where customer_id in (select customer_id from prepaid_reload_meps) AND customer_id NOT IN (select customer_id from Replaced_cards)) where cycle=$cycle";
        $res26=sqlsrv_query($dbh,$distributed);

        $destroyed="Update summary set destroyed_cards=(select count(*) from Destroyed_Cards) where cycle=$cycle";
        $res27=sqlsrv_query($dbh,$destroyed);

        $undistributed="Update summary set undistributed_cards=(select count(*) from vrl where customer_id not in (select customer_id from prepaid_reload_meps) and customer_id not in (select customer_id from destroyed_cards)) where cycle=$cycle";
        $res28=sqlsrv_query($dbh,$undistributed);

        $total_unused="Update summary set total_unused=(select sum(unused) as unused from UnusedResidualOverdraft where Month=$cycle and unused>0) where cycle=$cycle";
        $res29=sqlsrv_query($dbh,$total_unused);

        $total_residual="Update summary set total_Residual=(select sum(Residual) as Residual from UnusedResidualOverdraft where Month=$cycle and Residual>0) where cycle=$cycle";
        $res30=sqlsrv_query($dbh,$total_residual);

        $total_overdraft="Update summary set total_Overdraft=(select sum(Overdraft) as Overdraft from UnusedResidualOverdraft where Month=$cycle) where cycle=$cycle";
        $res31=sqlsrv_query($dbh,$total_overdraft);

        $odd_sale_times="Update summary set odd_sale_times=(select COUNT(*)  from sales_draft_$cycle where left(Trans_Time,2)>='00' and left(Trans_Time,2)<='05') where cycle=$cycle";
        $res32=sqlsrv_query($dbh,$odd_sale_times);

        $duplicate_auth="Update summary set duplicate_auth=(select COUNT(*)  from (select auth_no,count(*) as trans from dbo.sales_draft_$cycle group by auth_no having COUNT(*)>1) as table1) where cycle=$cycle";
        $res33=sqlsrv_query($dbh,$duplicate_auth);

        $merchants_shopped="Update summary set merchants_shopped=(select COUNT(distinct merchant) from sales_draft_$cycle) where cycle=$cycle";
        $res34=sqlsrv_query($dbh,$merchants_shopped);

        $processing_transaction_date_variation="Update summary set processing_transaction_date_variation=(SELECT max(datediff(day,(convert(date,str(Trans_Date))),(convert(date,str(Proc_Date))))) as Days FROM sales_draft_$cycle) where cycle=$cycle";
        $res35=sqlsrv_query($dbh,$processing_transaction_date_variation);

        $cumulative_sales_by_merchant="Update summary set cumulative_sales_by_merchant=(select Count(*) from sales_draft where left(Trans_Date,6)<=$cycle) where cycle=$cycle";
        $res36=sqlsrv_query($dbh,$cumulative_sales_by_merchant);

        $most_purchased_items="Update summary set most_purchased_items=(SELECT TOP (1) Item_Description FROM dbo.merchant_sales_$cycle GROUP BY Item_Description ORDER BY COUNT(*) DESC) where cycle=$cycle";
        $res37=sqlsrv_query($dbh,$most_purchased_items);

        $nfi="Update summary set nfi=(SELECT top(1) Item_Description FROM dbo.merchant_sales_201511 INNER JOIN dbo.merchant_listing ON dbo.merchant_sales_201511.Item_Description = dbo.merchant_listing.Brand_Name + dbo.merchant_listing.Package_Measure WHERE (dbo.merchant_listing.Generic_Name = 'NFI')) where cycle=$cycle";
        $res38=sqlsrv_query($dbh,$nfi);

        //$food_basket_pricing_contracted_merchants
        //$food_quality
        //$shop_inspection
        //$dos_pricing
        //$food_basket_pricing_average
        //$food_basket_pricing_transfer_value
    }
}
if($res1 && $res2 && $res3 && $res4 && $res5 && $res6 && $res7 && $res8 && $res9 && $res10 && $res11 && $res12 && $res13 && $res14 && $res15 && $res16 && $res17 && $res18 && $res19 && $res20 && $res21 && $res22 && $res23 && $res24 && $res25 && $res26 && $res27 && $res28 && $res29 && $res30 && $res31 && $res32 && $res33 && $res34 && $res35 && $res36 && $res37 && $res38){
    sqlsrv_commit($dbh);
    echo "Done!";
}
else{
    sqlsrv_rollback($dbh);
    echo "An error occurred!";
}

function fncGetLastMonth($thismonth){
    $date=substr($thismonth,0,4)."-".substr($thismonth,4,2)."-01";
    $datestring="$date first day of last month";
    $dt=date_create($datestring);
    return $dt->format('Ym'); //2011-02
}

?>