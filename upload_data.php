<?php
$month=$_POST['month'];
$year=$_POST['year'];
$filetype=$_POST['filetype'];
$name   = $_FILES['file']['name'];
$size   = $_FILES['file']['size'];
$tipo   = $_FILES['file']['type'];
$tmp_name = $_FILES['file']['tmp_name'];
$fp = fopen($tmp_name, "rb" );
$content = fread($fp,$size);
fclose($fp);
$arch_fijo = "uploads/$filetype/".$name;
$fp = fopen($arch_fijo,"wb");
fwrite($fp, $content, $size);
fclose($fp );

fncLogAccess2("Uploaded - ".$filetype);
if(strlen($content)==0){
    $str="Error. The file could not be saved on the server";
    fncShowMessage($str,"main.php#/import.php");
}
// upload of MEPS Data
if ($filetype=="MEPS_Data"){
    $imported=fncImportCSV_MEPS_Data($arch_fijo,$month,$year);
    $str="";
    if (!$imported)
    {
        $str="ERROR: The Pre-paid Reload MEPS file was not imported!!!";
        fncShowMessage($str,"main.php#/import.php");
    }
    else {
        $str="The Pre-paid Reload MEPS file has been Successfully Imported!!!";
        fncShowMessage($str,"main.php#/import.php");
    }
}

// upload for Pre-Paid Reload - WFP
if ($filetype=="Reload_WFP"){
    $imported=fncImportCSV_Reload_WFP($arch_fijo,$month,$year);
    $str="";
    if (!$imported)
    {
		$str="ERROR The Prepaid WFP-reload file was not Imported!!!";
        fncShowMessage($str,"main.php#/import.php");
    }
    else {
        $str="The Prepaid WFP-reload file has been Successfully Imported!!!";
        fncShowMessage($str,"main.php#/import.php");
    }
}

// upload for Pre-Paid Reload - LOA
if ($filetype=="Reload_LOA"){
    $imported=fncImportCSV_Reload_LOA($arch_fijo,$month,$year,$_POST["loa_location"],$_POST["loa_amount"],$_POST["loa_date"]);
    $str="";
    if (!$imported)
    {
        $str="ERROR The Prepaid LOA-reload file was not Imported!!!";
        fncShowMessage($str,"main.php#/import.php");
    }
    else {
        $str="The Prepaid LOA-reload file has been Successfully Imported!!!";
        fncShowMessage($str,"main.php#/import.php");
    }
}

// upload for JAB Statement
if ($filetype=="JAB"){
    $imported=fncImportCSV_JAB($arch_fijo,$month,$year);
    $str="";
    if (!$imported)
    {
        $str='ERROR Pre_paid_reload_wfp was not imported!!!';
        fncShowMessage($str,"main.php#/import.php");
    }
    else {
        $str='Pre_paid_reload_wfp file has been Successfully Imported!!!';
        fncShowMessage($str,"main.php#/import.php");
    }
}
// upload for Merchant Sales
if ($filetype=="Merchant_Sales"){
    if(fncAlreadyUploaded($month,$year,$_POST["merchant"])){
        $str='ERROR: The data for the selected retailer was already uploaded';
        fncShowMessage($str,"main.php#/import.php");
    }
    else{
        if($_POST["merchant"]=="63")
            $imported=fncImportCSV_Safeway($arch_fijo,$month,$year);
        elseif($_POST["merchant"]=="86")
            $imported=fncImportCSV_Tazweed($arch_fijo,$month,$year);
        else
            $imported=fncImportCSV_OtherRetailerSales($arch_fijo,$month,$year,$_POST["merchant"],$_POST["date_format"],$_POST["date_separator"]);
        $str="";
        if (!$imported)
        {
            $str='ERROR: Raw Merchant Sales file was not imported!!!';
            fncShowMessage($str,"main.php#/import.php");
        }
        else {
            $str='Raw Merchant Sales file has been Successfully Imported!!!';
            fncShowMessage($str,"main.php#/import.php");
        }
    }
}
//Merchant Purchases
if ($filetype=="Merchant_Purchases"){
    $imported=fncImportCSV_MerchantPurchases($arch_fijo,$month,$year,86);
    $str="";
    if (!$imported)
    {
        $str='ERROR: Merchant Purchases file was not imported!!!';
        fncShowMessage($str,"main.php#/import.php");
    }
    else {
        $str='Merchant Purchases file has been Successfully Imported!!!';
        fncShowMessage($str,"main.php#/import.php");
    }
}

// upload for Merchant List WFP
if ($filetype=="Merchants_WFP"){
    $imported=false; //fncImportCSV_Merchants_WFP($arch_fijo,$month,$year);
    $str="";
    if (!$imported)
    {
        $str='ERROR: WFP file was NOT imported!!!';
        fncShowMessage($str,"main.php#/import.php");
    }
    else {
        $str='Merchants WFP file has been Successfully Imported!!!';
        fncShowMessage($str,"main.php#/import.php");
    }
}

//Import VRL
if ($filetype=="VRL"){
    $vrl_type=$_POST["vrl_type"];
    $str="";
    if ($vrl_type=="Destroyed Cards"){
        $imported=fncImportCSV_Destroyed_Cards($arch_fijo,$month,$year);
        if (!$imported)
        {
            $str='ERROR: Destroyed Cards File was NOT Imported!!!';
            fncShowMessage($str,"main.php#/import.php");
        }
        else {
            $str='Destroyed Cards file has been Successfully Imported!!!';
            fncShowMessage($str,"main.php#/import.php");
        }
    }
    elseif ($vrl_type=="Replaced Cards"){
        $imported=fncImportCSV_Replaced_Cards($arch_fijo,$month,$year);
        if (!$imported)
        {
            $str='ERROR: Replaced Cards File was NOT Imported!!!';
            fncShowMessage($str,"main.php#/import.php");
        }
        else {
            $str='Replaced Cards file has been Successfully Imported!!!';
            fncShowMessage($str,"main.php#/import.php");
        }
    }
    elseif ($vrl_type=="Programme Listing"){
        $imported=fncImportCSV_VRL($arch_fijo,$month,$year);
        if (!$imported)
        {
            $str='ERROR: VRL file was NOT Imported!!!';
            fncShowMessage($str,"main.php#/import.php");
        }
        else {
            $str='VRL file has been Successfully Imported!!!';
            fncShowMessage($str,"main.php#/import.php");
        }
    }
}
//Import Destroyed Cards
if ($filetype=="Unused_Exceptions"){
    $imported=fncImportCSV_Unused_Exceptions($arch_fijo,$month,$year);
    $str="";
    if (!$imported)
    {
        $str='ERROR: Unused Exceptions File was NOT Imported!!!';
        fncShowMessage($str,"main.php#/import.php");
    }
    else {
        $str='Unused Exceptions File has been Successfully Imported!!!';
        fncShowMessage($str,"main.php#/import.php");
    }
}

?>