<?php session_start();
    include "fncCashAnalyzer.inc.php";
    $fields=fncGetFieldNames($_GET["filename"]);
?>
<link rel="stylesheet" href="css/nice-form.css" />
            <div class="nice-form" style="float: left; width: 800px">
                <form id="form1" name="form1" enctype="multipart/form-data"
                      action="upload_special.php" method="post" onsubmit="return checkForm()">
                    <input type="hidden" name="filename" value="<?php echo $_GET["filename"] ?>" />
                    <input type="hidden" name="month" value="<?php echo $_GET["month"] ?>" />
                    <input type="hidden" name="year" value="<?php echo $_GET["year"] ?>" />
                    <input type="hidden" name="merchant" value="<?php echo $_GET["merchant"] ?>" />
                    <input type="hidden" name="date_format" value="<?php echo $_GET["date_format"] ?>" />
                    <input type="hidden" name="date_separator" value="<?php echo $_GET["date_separator"] ?>" />
                    <table>
                        <tr>
                            <td>
                                <label for="trans_date">
                                    <span>Date<span class="required">*</span></span>
                                    <select name="trans_date" id="trans_date" class="select-field">
                                        <option value="">Select Transaction Date</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="trans_time">
                                    <span>Time<span class="required">*</span></span>
                                    <select name="trans_time" id="trans_time" class="select-field">
                                        <option value="">Select Transaction Time</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="receipt_no">
                                    <span>Receipt No.<span class="required">*</span></span>
                                    <select name="receipt_no" id="receipt_no" class="select-field">
                                        <option value="">Select Receipt No.</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="customer_id">
                                    <span>Beneficiary ID<span class="required">*</span></span>
                                    <select name="customer_id" id="customer_id" class="select-field">
                                        <option value="">Select Beneficiary ID</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="barcode">
                                    <span>Barcode<span class="required">*</span></span>
                                    <select name="barcode" id="barcode" class="select-field">
                                        <option value="">Select Barcode</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="material_code">
                                    <span>SKU Code<span class="required">*</span></span>
                                    <select name="material_code" id="material_code" class="select-field">
                                        <option value="">Select SKU Code</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="item_description">
                                    <span>SKU Desc.<span class="required">*</span></span>
                                    <select name="item_description" id="item_description" class="select-field">
                                        <option value="">Select SKU Description</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                            </td>
                            <td>
                                <label for="quantity">
                                    <span>Quantity<span class="required">*</span></span>
                                    <select name="quantity" id="quantity" class="select-field">
                                        <option value="">Select Quantity</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="unit_price">
                                    <span>Unit Price<span class="required">*</span></span>
                                    <select name="unit_price" id="unit_price" class="select-field">
                                        <option value="">Select Unit Price</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="vat">
                                    <span>VAT<span class="required">*</span></span>
                                    <select name="vat" id="vat" class="select-field">
                                        <option value="">Select VAT</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="total_amount">
                                    <span>Total Amount<span class="required">*</span></span>
                                    <select name="total_amount" id="total_amount" class="select-field">
                                        <option value="">Select Total Amount</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="payment_type">
                                    <span>Payment Type<span class="required">*</span></span>
                                    <select name="payment_type" id="payment_type" class="select-field">
                                        <option value="">Select Payment Type</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label for="packaging">
                                    <span>Packaging<span class="required">*</span></span>
                                    <select name="packaging" id="packaging" class="select-field">
                                        <option value="">Select Packaging</option>
                                        <?php fncShowOptions() ?>
                                    </select>
                                </label>
                                <label>
                                    <span>&nbsp;</span>
                                    <input type="submit" name="submit" value="Upload Data" id="submit" />
                                </label>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

<?php
function fncShowOptions(){
    global $fields;
    for($i=0;$i<count($fields);$i++) {
?>
        <option value="<?php echo $i?>"><?php echo $fields[$i]?></option>
        <?php
    }
}
        ?>
<script>
    msg = "";
    error = false;
    function checkForm() {
       
        if (isValid()) {
            $("body").trigger("popUpLoading");
            return true;
        }
        else {
            return false;
        }
    }
    function isValid() {
        msg = "";
        error = false;
        checkRequired('trans_date', 'Date is required');
        checkRequired('trans_time', 'Time is required');
        checkRequired('receipt_no', 'Receipt no is required');
        checkRequired('customer_id', 'Customer ID is required');
        checkRequired('barcode', 'Barcode is required');
        checkRequired('material_code', 'SKU Code is required');
        checkRequired('item_description', 'SKU Description is required');
        checkRequired('quantity', 'Quantity is required');
        checkRequired('unit_price', 'Unit price is required');
        checkRequired('vat', 'VAT is required');
        checkRequired('total_amount', 'Amount is required');
        checkRequired('payment_type', 'Payment type is required');
        checkRequired('packaging', 'Packaging is required');
        if (error) {
            alert(msg);
            return false;
        }
        else {
            return true;
        }
    }
    function checkRequired(param, message) {
        if (document.getElementById(param).value == null
            || document.getElementById(param).value == '') {
            error = true;
            if (msg == "")
                msg = message;
            else
                msg = msg + "\n" + message;
        }
    }
</script>