<?php 
include("../fncDataFunctions.inc.php");
?>
<?php function showpagenav($page, $pagecount)
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a class="btn btn-primary" href="users.php?a=add">Add Record</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="users.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Prev</a>&nbsp;</td>
<?php } ?>
<?php
  global $pagerange;

  if ($pagecount > 1) {

  if ($pagecount % $pagerange != 0) {
    $rangecount = intval($pagecount / $pagerange) + 1;
  }
  else {
    $rangecount = intval($pagecount / $pagerange);
  }
  for ($i = 1; $i < $rangecount + 1; $i++) {
    $startpage = (($i - 1) * $pagerange) + 1;
    $count = min($i * $pagerange, $pagecount);

    if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
      for ($j = $startpage; $j < $count + 1; $j++) {
        if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="users.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php } } } else { ?>
<td><a href="users.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php } } } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="users.php?page=<?php echo $page + 1 ?>">Next&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="users.php">Index Page</a></td>
</tr>
</table>
<hr size="1" noshade />
<?php } ?>

<?php function addrec()
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="users.php">Index Page</a></td>
</tr>
</table>
<hr size="1" noshade>
<form enctype="multipart/form-data" action="users.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
    <?php
$row = array(
  "id" => "",
  "usertype" => "",
  "login" => "",
  "passwd" => "",
  "acmain" => "",
  "acupload" => "",
  "acdata" => "",
  "acqueries" => "",
  "acsys" => "",
  "accallcenter" => "",
  "status" => "",
  "insertedby" => "",
  "inserteddate" => "",
  "verifiedby" => "",
  "verifieddate" => "",
  "verifieddelby" => "",
  "verifieddeldate" => "",
  "deletedby" => "",
  "deleteddate" => "");
showroweditor($row, false);
    ?>
<p><input class="btn btn-primary" type="submit" name="action" value="Save"></p>
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("view", $recid, $count);
?>
<br />
<?php showrow($row, $recid) ?>

<?php
  
} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("edit", $recid, $count);
?>
<form enctype="multipart/form-data" action="users.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xid" value="<?php echo $row["id"] ?>">
<?php showroweditor($row, true); ?>
<p><input class="btn btn-primary" type="submit" name="action" value="Save"></p>
</form>
<?php
  
} ?>

<?php function deleterec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  $row = sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH, SQLSRV_SCROLL_ABSOLUTE, $recid);
  showrecnav("del", $recid, $count);
?>
<br />
<form action="users.php" method="post">
<input type="hidden" name="sql" value="delete">
<input type="hidden" name="xid" value="<?php echo $row["id"] ?>">
<?php showrow($row, $recid) ?>
<p><input class="btn btn-danger" type="submit" name="action" value="Confirm"></p>
</form>
<?php

}

function sql_select()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT * FROM (SELECT t1.id, t1.usertype, lp1.UserType AS lp_usertype, t1.login, t1.passwd, t1.acmain, t1.acupload, t1.acdata, t1.acqueries, t1.acsys,t1.accallcenter, t1.status, t1.insertedby, t1.inserteddate, t1.verifiedby, t1.verifieddate, t1.verifieddelby, t1.verifieddeldate, t1.deletedby, t1.deleteddate FROM access AS t1 LEFT OUTER JOIN usertypes AS lp1 ON (t1.usertype = lp1.id)) subq where status='Y'";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " and " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " and (lp_usertype like '" .$filterstr ."') or (login like '" .$filterstr ."') or (passwd like '" .$filterstr ."') or (acmain like '" .$filterstr ."') or (acpb like '" .$filterstr ."') or (acad like '" .$filterstr ."') or (acmt like '" .$filterstr ."') or (acrt like '" .$filterstr ."') or (acgn like '" .$filterstr ."') or (acps like '" .$filterstr ."') or (acdm like '" .$filterstr ."') or (status like '" .$filterstr ."') or (insertedby like '" .$filterstr ."') or (inserteddate like '" .$filterstr ."')";
  }
  if (isset($order) && $order!='') $sql .= " order by " .sqlstr($order) ."";
  if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
  $res = sqlsrv_query($conn,$sql,array(), array( "Scrollable" => 'static' ));
  fncLogAccess("Viewed a list of users");
  return $res;
}

function sql_getrecordcount()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT COUNT(*) FROM (SELECT t1.id, t1.usertype, lp1.UserType AS lp_usertype, t1.login, t1.passwd, t1.acmain,t1.acupload, t1.acdata, t1.acqueries, t1.acsys, t1.status, t1.insertedby, t1.inserteddate, t1.verifiedby, t1.verifieddate, t1.verifieddelby, t1.verifieddeldate, t1.deletedby, t1.deleteddate FROM access AS t1 LEFT OUTER JOIN usertypes AS lp1 ON (t1.usertype = lp1.id)) subq where status='Y'";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " and " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " and (lp_usertype like '" .$filterstr ."') or (login like '" .$filterstr ."') or (passwd like '" .$filterstr ."') or (acmain like '" .$filterstr ."') or (acpb like '" .$filterstr ."') or (acad like '" .$filterstr ."') or (acmt like '" .$filterstr ."') or (acrt like '" .$filterstr ."') or (acgn like '" .$filterstr ."') or (acps like '" .$filterstr ."') or (acdm like '" .$filterstr ."') or (status like '" .$filterstr ."') or (insertedby like '" .$filterstr ."') or (inserteddate like '" .$filterstr ."')";
  }
  $res = mssql_query($sql, $conn);
  $row = mssql_fetch_array($res);
  reset($row);
  return current($row);
}

function sql_insert()
{
  global $conn;
  global $_POST;
  global $_SESSION;

  $acmain=isset($_POST["UP"])?"%UP":"";
  $acmain.=isset($_POST["DA"])?"%DA":"";
  $acmain.=isset($_POST["QR"])?"%QR":"";
  $acmain.=isset($_POST["SY"])?"%SY":"";
  $acmain.=isset($_POST["CC"])?"%CC":"";
  $acmain=trim($acmain,"%");

  $acupload=isset($_POST["Upload-WF"])?"%WF":"";
  $acupload.=isset($_POST["Upload-JA"])?"%JA":"";
  $acupload.=isset($_POST["Upload-ME"])?"%ME":"";
  $acupload.=isset($_POST["Upload-MC"])?"%MC":"";
  $acupload.=isset($_POST["Upload-MS"])?"%MS":"";
  $acupload.=isset($_POST["Upload-MP"])?"%MP":"";
  $acupload=trim($acupload,"%");

  $acdata=isset($_POST["Data-MC"])?"%MC":"";
  $acdata.=isset($_POST["Data-FI"])?"%FI":"";
  $acdata=trim($acdata,"%");

  $acqueries=isset($_POST["Queries-RC"])?"%RC":"";
  $acqueries.=isset($_POST["Queries-MO"])?"%MO":"";
  $acqueries.=isset($_POST["Queries-CO"])?"%CO":"";
  $acqueries=trim($acqueries,"%");

  $acsys=isset($_POST["Sys-US"])?"US":"";

  $accallcenter=isset($_POST["CallCenter-CT"])?"%CT":"";
  $accallcenter.=isset($_POST["CallCenter-VA"])?"%VA":"";
  $accallcenter.=isset($_POST["CallCenter-ED"])?"%ED":"";
  $accallcenter.=isset($_POST["CallCenter-DE"])?"%DE":"";
  $accallcenter.=isset($_POST["CallCenter-VR"])?"%VR":"";
  $accallcenter.=isset($_POST["CallCenter-CR"])?"%CR":"";
  $accallcenter.=isset($_POST["CallCenter-AD"])?"%AD":"";
  $accallcenter=trim($accallcenter,"%");

  $sql = "insert into access (usertype, login, passwd, acmain, acupload, acdata, acqueries, acsys, accallcenter, status, insertedby, inserteddate) values (" .sqlvalue(@$_POST["usertype"], false).", " .sqlvalue(@$_POST["login"], true).", HASHBYTES('MD5','admin'), '$acmain', '$acupload', '$acdata', '$acqueries', '$acsys', '$accallcenter','Y', " .sqlvalue(@$_SESSION["name"], true).", " .sqlvalue(date("Y-m-d"), true).")";
  mssql_query($sql, $conn);
  fncLogAccess("Added a new user - ".$_POST["login"]);
}

function sql_update()
{
  global $conn;
  global $_POST;
  global $_SESSION;

  $acmain=isset($_POST["UP"])?"%UP":"";
  $acmain.=isset($_POST["DA"])?"%DA":"";
  $acmain.=isset($_POST["QR"])?"%QR":"";
  $acmain.=isset($_POST["SY"])?"%SY":"";
  $acmain.=isset($_POST["CC"])?"%CC":"";
  $acmain=trim($acmain,"%");

  $acupload=isset($_POST["Upload-WF"])?"%WF":"";
  $acupload.=isset($_POST["Upload-JA"])?"%JA":"";
  $acupload.=isset($_POST["Upload-ME"])?"%ME":"";
  $acupload.=isset($_POST["Upload-MC"])?"%MC":"";
  $acupload.=isset($_POST["Upload-MS"])?"%MS":"";
  $acupload.=isset($_POST["Upload-MP"])?"%MP":"";
  $acupload=trim($acupload,"%");

  $acdata=isset($_POST["Data-MC"])?"%MC":"";
  $acdata.=isset($_POST["Data-FI"])?"%FI":"";
  $acdata=trim($acdata,"%");

  $acqueries=isset($_POST["Queries-RC"])?"%RC":"";
  $acqueries.=isset($_POST["Queries-MO"])?"%MO":"";
  $acqueries.=isset($_POST["Queries-CO"])?"%CO":"";
  $acqueries=trim($acqueries,"%");

  $acsys=isset($_POST["Sys-US"])?"US":"";

  $accallcenter=isset($_POST["CallCenter-CT"])?"%CT":"";
  $accallcenter.=isset($_POST["CallCenter-VA"])?"%VA":"";
  $accallcenter.=isset($_POST["CallCenter-ED"])?"%ED":"";
  $accallcenter.=isset($_POST["CallCenter-DE"])?"%DE":"";
  $accallcenter.=isset($_POST["CallCenter-VR"])?"%VR":"";
  $accallcenter.=isset($_POST["CallCenter-CR"])?"%CR":"";
  $accallcenter.=isset($_POST["CallCenter-AD"])?"%AD":"";
  $accallcenter=trim($accallcenter,"%");

  $sql = "update access set usertype=" .sqlvalue(@$_POST["usertype"], false).", login=" .sqlvalue(@$_POST["login"], true).", acmain='$acmain', acupload='$acupload', acdata='$acdata',acqueries='$acqueries', acsys='$acsys',accallcenter='$accallcenter', status='Y', insertedby='" .$_SESSION["name"]."', inserteddate='" .date("Y/m/d")."' where " .primarykeycondition();
  mssql_query($sql, $conn);
  fncLogAccess("Update user rights for - ".$_POST["login"]);
}

function sql_delete()
{
  global $conn;
  $sql="select login from access where ".primarykeycondition();
  $res=mssql_query($sql, $conn);
  $row=mssql_fetch_array($res);
  $sql = "update access set status='N' where " .primarykeycondition();
  mssql_query($sql, $conn);
  fncLogAccess("Deleted a user - ".$row["login"]);
}
function primarykeycondition()
{
  global $_POST;
  $pk = "";
  $pk .= "(id";
  if (@$_POST["xid"] == "") {
    $pk .= " IS NULL";
  }else{
  $pk .= " = " .sqlvalue(@$_POST["xid"], false);
  };
  $pk .= ")";
  return $pk;
}
?>
