<?php function select()
  {
  global $a;
  global $showrecs;
  global $page;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $order;
  global $ordtype;


  if ($a == "reset") {
    $filter = "";
    $filterfield = "";
    $wholeonly = "";
    $order = "";
    $ordtype = "";
  }

  $checkstr = "";
  if ($wholeonly) $checkstr = " checked";
  if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
  $res = sql_select();
  $count = sql_getrecordcount();
  if ($count % $showrecs != 0) {
    $pagecount = intval($count / $showrecs) + 1;
  }
  else {
    $pagecount = intval($count / $showrecs);
  }
  $startrec = $showrecs * ($page - 1);
  if ($startrec < $count) {sqlsrv_fetch($res, $startrec);}
  $reccount = min($showrecs * $page, $count);
?>
<style type="text/css">
    td a {
        color:white;
        text-align:left;
    }
</style>
<div class="container">
<div class="row">
<div class="col-sm-8 col-md-8">
<form action="users.php" method="post">
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><b>Search</b>&nbsp;</td>
<td><input type="text" name="filter" value="<?php echo $filter ?>"></td>
<td><select name="filter_field">
<option value="">All Fields</option>
<option value="<?php echo "lp_usertype" ?>"<?php if ($filterfield == "lp_usertype") { echo "selected"; } ?>><?php echo htmlspecialchars("User Type") ?></option>
<option value="<?php echo "login" ?>"<?php if ($filterfield == "login") { echo "selected"; } ?>><?php echo htmlspecialchars("Login") ?></option>
</select></td>
<td><input type="checkbox" name="wholeonly"<?php echo $checkstr ?> />Whole words only</td>
</tr>
<tr>
<td>&nbsp;</td>
<td><input type="submit" name="action" value="Search" /></td>
<td><a style="color:blue" href="users.php?a=reset">Reset Filter</a></td><td> <a style="color:blue" href="log.php">View User Log</a></td>
</tr>
</table>
</form>
</div>
<div class="col-sm-4 col-md-4" style="text-align:right">
<a href="users.php?a=add" class="btn btn-primary">Add New User</a>
</div>
</div>
<hr size="1" noshade>
<div class="csstable">
<table class="table-hover" border="0" cellspacing="1" cellpadding="5"width="100%">
<tr>
<td><a href="#"><?php echo htmlspecialchars("#") ?></a></td>
<td><a href="users.php?order=<?php echo "login" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Name") ?></a></td>
<td><a href="users.php?order=<?php echo "lp_usertype" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("User Type") ?></a></td>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
</tr>
<?php
  for ($i = $startrec; $i < $reccount; $i++)
  {
    $row = mssql_fetch_array($res);
	$names=explode(".",trim($row["login"]));
	if(isset($names[1])){
		$fullname=ucfirst($names[0])." ".ucfirst($names[1]);
	}
	else{
		$fullname=ucfirst($names[0]);
	}
?>
<tr>
<td style="width:18px"><?php echo $i+1 ?></td>
<td><?php echo htmlspecialchars($fullname) ?></td>
<td><?php echo htmlspecialchars($row["lp_usertype"]) ?></td>
<td style="width:18px"><a href="users.php?a=view&recid=<?php echo $i ?>"><img src="../images/view.png" alt="view"></a></td>
<td style="width:18px"><a href="users.php?a=edit&recid=<?php echo $i ?>"><img src="../images/edit-bw.png" alt="edit"></a></td>
<td style="width:18px"><a href="users.php?a=del&recid=<?php echo $i ?>"><img src="../images/delete.png" alt="delete"></a></td>
</tr>
<?php
  }
  
?>
</table>
</div>
<br />
<?php showpagenav($page, $pagecount); ?>
</div>
<?php } ?>