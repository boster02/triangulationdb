<?php session_start();
      include("../fncCashAnalyzer.inc.php");
      $activity=fncGetActivityLog();
      fncLogAccess2("Viewed the user activity log");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Boster Sibande">

    <title>Programme Settings</title>

    <link rel="stylesheet" type="text/css" href="../css/csstable.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .hr {
            width: 150px;
        }
    </style>
</head>

<body>
    <h3>User activity log</h3>
    <div class="csstable">
        <table>
            <tr>
                <td>#</td>
                <td>User Name</td>
                <td>Activity</td>
                <td>Date</td>
            </tr>
            <?php for($i=1;$i<=$activity[0][0];$i++){ ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $activity[$i]["doneby"] ?></td>
                <td><?php echo $activity[$i]["activity"] ?></td>
                <td><?php echo $activity[$i]["datetime"] ?></td>
            </tr>
            <?php } ?>
            <tr>
            </tr>
        </table>
    </div>
</body>

</html>

