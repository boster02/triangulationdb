<?php session_start();
  include("fncUsers.inc.php");
  include("list_users.php");
  include("view_users.php");
  include("edit_users.php");
  include("../fncCashAnalyzer.inc.php");

  if (isset($_GET["order"])) $order = @$_GET["order"];
  if (isset($_GET["type"])) $ordtype = @$_GET["type"];

  if (isset($_POST["filter"])) $filter = @$_POST["filter"];
  if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
  $wholeonly = false;
  if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];
  
  if (!isset($order) && isset($_SESSION["users_order"])) $order = $_SESSION["users_order"];
  if (!isset($ordtype) && isset($_SESSION["users_type"])) $ordtype = $_SESSION["users_type"];
  if (!isset($filter) && isset($_SESSION["users_filter"])) $filter = $_SESSION["users_filter"];
  if (!isset($filterfield) && isset($_SESSION["users_filter_field"])) $filterfield = $_SESSION["users_filter_field"];

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Administrator</title>

    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/csstable.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="overflow-x:hidden">

    <div id="wrapper">

        <!-- Navigation -->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                          $conn=fncOpenDBConn();
						  $showrecs = 5000;
						  $pagerange = 10;
						
						  $a = @$_GET["a"];
						  $recid = @$_GET["recid"];
						  $page = @$_GET["page"];
						  if (!isset($page)) $page = 1;
						
						  $sql = @$_POST["sql"];
						
						  switch ($sql) {
							case "insert":
							  sql_insert();
							  break;
							case "update":
							  sql_update();
							  break;
							case "delete":
							  sql_delete();
							  break;
						  }
						
						  switch ($a) {
							case "add":
							  addrec();
							  break;
							case "view":
							  viewrec($recid);
							  break;
							case "edit":
							  editrec($recid);
							  break;
							case "del":
							  deleterec($recid);
							  break;
							default:
							  select();
							  break;
						  }
						
						  if (isset($order)) $_SESSION["users_order"] = $order;
						  if (isset($ordtype)) $_SESSION["users_type"] = $ordtype;
						  if (isset($filter)) $_SESSION["users_filter"] = $filter;
						  if (isset($filterfield)) $_SESSION["users_filter_field"] = $filterfield;
						  if (isset($wholeonly)) $_SESSION["users_wholeonly"] = $wholeonly;
						
						?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->

            <!-- /.row -->
        </div>

    </div>
</body>

</html>
