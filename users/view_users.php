<?php function showrow($row,$recid)
      {
          global $conn;
?>
<table class="table" border="0" cellspacing="1" cellpadding="5" style="width:400px">
<input type="hidden" name="id" value="<?php echo str_replace("�","&#39", trim($row["id"])) ?>">
<tr>
<td class="hr"><?php echo htmlspecialchars("User Type")."&nbsp;" ?></td>
<td class="dr"><select class="form-control" name="usertype">
<?php
          $sql = "select id, UserType from usertypes";
          $res = mssql_query($sql, $conn);

          while ($lp_row = mssql_fetch_array($res)){
              $val = $lp_row["id"];
              $caption = $lp_row["UserType"];
              if ($lp_row["UserType"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("User Name:")."&nbsp;" ?></td>
<td class="dr"><input class="form-control" name="login" maxlength="60" value="<?php echo str_replace("�","&#39", trim($row["login"])) ?>" /></td>
</tr>
<table>
<h3>User Rights</h3>
<?php
          $acmain=explode("%",$row["acmain"]);
          $acupload=explode("%",$row["acupload"]."%t");
          $acdata=explode("%",$row["acdata"]."%t");
          $acqueries=explode("%",$row["acqueries"]."%t");
          $acsys=explode("%",$row["acsys"]."%t");
?>

<table width=100% border=1 style="border-collapse: collapse;">
        <tr bgcolor="#19498B">
            <td>
                <font size=3 color="white">
                <b>1. Upload Data </b></font>&nbsp;<input type="checkbox" name="UP" <?php if(in_array("UP",$acmain)) echo "checked=checked" ?> />
            </td>
        </tr>
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Upload-WF" <?php if(in_array("WF",$acupload)) echo "checked=checked" ?> />Upload Pre-paid Reload WFP</td>
        </tr>
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Upload-JA" <?php if(in_array("JA",$acupload)) echo "checked=checked" ?> />
                Upload Wings/JAB Statement</td>
        </tr>
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Upload-ME" <?php if(in_array("ME",$acupload)) echo "checked=checked" ?> /> Upload MEPS data
            </td>
        </tr>        
        <tr>
            <td style="height: 21px">
				&nbsp;<input type="checkbox" name="Upload-MC" <?php if(in_array("MC",$acupload)) echo "checked=checked" ?> /> Upload list of merchants
            </td>
        </tr>
        <tr>
            <td style="height: 21px">
				&nbsp;<input type="checkbox" name="Upload-MS" <?php if(in_array("MS",$acupload)) echo "checked=checked" ?> /> Upload merchant sales
            </td>
        </tr> 
        <tr>
            <td style="height: 21px">
				&nbsp;<input type="checkbox" name="Upload-MP" <?php if(in_array("MP",$acupload)) echo "checked=checked" ?> /> Upload merchant purchases
            </td>
        </tr>                 
    </table>
    <br />
<table width=100% border=1 style="border-collapse: collapse;">
        <tr bgcolor="#19498B">
            <td>
	            <font size=3 color="white">
                <b>2. Data Management </b></font>
                &nbsp;<input type="checkbox" name="DA" <?php if(in_array("DA",$acmain)) echo "checked=checked" ?> />
            </td>
        </tr>
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Data-MC" <?php if(in_array("MC",$acdata)) echo "checked=checked" ?> />
                Update merchants contracts</td>
        </tr> 
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Data-FI" <?php if(in_array("FI",$acdata)) echo "checked=checked" ?> /> 
                Update financial service provider contracts
            </td>
        </tr> 
    </table>
    <br />
<table width=100% border=1 style="border-collapse: collapse;">
        <tr bgcolor="#19498B">
            <td>
                <font size=3 color="white">
                <b>4. Queries </b></font>&nbsp;<input type="checkbox" name="QR" <?php if(in_array("QR",$acmain)) echo "checked=checked" ?> />
            </td>
        </tr>
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Queries-RC" <?php if(in_array("RC",$acqueries)) echo "checked=checked" ?> />
                Reconciliation; Reload, Reload/Sales variance, Overdraft, Unused, Residuals</td>
        </tr> 
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Queries-MO" <?php if(in_array("MO",$acqueries)) echo "checked=checked" ?> />
                Monitoring; Time of sale, Overdrafts, Number of visits, NFI purchases
            </td>
        </tr> 
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Queries-CO" <?php if(in_array("CO",$acqueries)) echo "checked=checked" ?> />
                Contracts; Contract expiration, Contracted Merchants vs MEPS listing of Merchants, Find Purchases
            </td>
        </tr>
    </table>
    <br />
<table width=100% border=1 style="border-collapse: collapse;">
        <tr bgcolor="#19498B">
            <td>
                <font size=3 color="white">
                <b>7. System Administration </b></font>
                &nbsp;<input type="checkbox" name="SY" <?php if(in_array("SY",$acmain)) echo "checked=checked" ?> />
            </td>
        </tr>
        <tr>
            <td>
				&nbsp;<input type="checkbox" name="Sys-US" <?php if(in_array("US",$acsys)) echo "checked=checked" ?> />
                User Management
            </td>
        </tr> 
        </table>
</table>
<?php } ?>
