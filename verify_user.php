<?php session_start();
  	include "fncLogin.inc.php";
      $user=$_POST["user"];
      $pass=$_POST["pass"];
      $usertype=fncGetUserType($user);
      $data=fncVerifyUser($user, $pass, $usertype);
      if($data[0]=='NOTFOUND'){
          $link="index.php?msg=nf";
          fncLogAccess2("Failed to login",$user);
      }
      elseif ($data[0]=='NOTFOUND2'){
          $link="index.php?msg=nf2";
          fncLogAccess2("Failed to login",$user);
      }
      elseif ($data[0]=='NOTFOUND3'){
          $link="index.php?msg=nf3";
          fncLogAccess2("Failed to login",$user);
      }
      elseif ($data[0]=='NOTFOUND4'){
          $link="index.php?msg=nf4";
          fncLogAccess2("Failed to login",$user);
      }
      else{
          $_SESSION['access']=trim($data[1]);
          $_SESSION['user']=trim($data[0]);
          $_SESSION['usertype']=$usertype;
          $_SESSION['name']=fncGetFullName($data[0]);
          $link="main.php";
          //Detailed user rights
          $_SESSION["acupload"]=$data["acupload"];
          $_SESSION["acdata"]=$data["acdata"];
          $_SESSION["acqueries"]=$data["acqueries"];
          $_SESSION["acsys"]=$data["acsys"];
          fncLogAccess2("Successfully logged in");
          if($usertype==3 || $usertype==4){
              $_SESSION["retailer_id"]=$user;
              $link="/retailer";
          }
          elseif(isset($_SESSION["returnurl"])){
              $link=$_SESSION["returnurl"];
              $_SESSION["returnurl"]=null;
          }
          else{
              $link="main.php";
          }
      }
      echo"<script>
            window.location=\"$link\";
        </script>";
?>